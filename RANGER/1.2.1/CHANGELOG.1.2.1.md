
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Ranger Changelog

## Release 1.2.1 - Unreleased (as of 2018-10-20)



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [RANGER-2209](https://issues.apache.org/jira/browse/RANGER-2209) | Service Definition for ABFS to support Ranger Authorization |  Major | Ranger | Yuan Gao | Yuan Gao |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [RANGER-2140](https://issues.apache.org/jira/browse/RANGER-2140) | Upgrade spring and guava libraries |  Major | Ranger | Pradeep Agrawal | Pradeep Agrawal |
| [RANGER-2207](https://issues.apache.org/jira/browse/RANGER-2207) | Allow resources to appear in column mask policies without being visible in access policies |  Major | Ranger | Eric Alton | Nitin Galave |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [RANGER-2241](https://issues.apache.org/jira/browse/RANGER-2241) | Fix release build scripts to conform to latest Apache release guidelines - Part 2 - Remove sha1 and mds |  Major | Ranger | Colm O hEigeartaigh | Velmurugan Periasamy |
| [RANGER-1958](https://issues.apache.org/jira/browse/RANGER-1958) | [HBase] Implement getUserPermissions API of AccessControlService.Interface to allow clients to access HBase permissions stored in Ranger |  Major | plugins | Ankit Singhal | Ankit Singhal |
| [RANGER-2238](https://issues.apache.org/jira/browse/RANGER-2238) | String comparison should not use ‘==’ in ServiceUtil.java |  Minor | admin | Qiang Zhang | Qiang Zhang |
| [RANGER-2245](https://issues.apache.org/jira/browse/RANGER-2245) | Exclude Jetty libraries |  Major | Ranger | Pradeep Agrawal | Pradeep Agrawal |
| [RANGER-2249](https://issues.apache.org/jira/browse/RANGER-2249) | Ranger Audit  not flushed immediately to hdfs |  Major | Ranger | Ramesh Mani | Ramesh Mani |
| [RANGER-2250](https://issues.apache.org/jira/browse/RANGER-2250) | Service configs  fields are not showing for atlas service form page |  Major | Ranger | Nitin Galave | Nitin Galave |
| [RANGER-2247](https://issues.apache.org/jira/browse/RANGER-2247) | Ranger Plugin for HDFS throws StringIndexOutOfBounds exception when policy resource is "\\" |  Major | Ranger | Abhay Kulkarni | Abhay Kulkarni |
| [RANGER-2243](https://issues.apache.org/jira/browse/RANGER-2243) | Provide option to ranger builds to specifically build a single plugin |  Major | Ranger | Sailaja Polavarapu | Sailaja Polavarapu |


