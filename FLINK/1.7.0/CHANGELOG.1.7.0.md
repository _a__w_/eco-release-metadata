
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Flink Changelog

## Release 1.7.0 - Unreleased (as of 2018-10-20)



### IMPORTANT ISSUES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [FLINK-9935](https://issues.apache.org/jira/browse/FLINK-9935) | Batch Table API: grouping by window and attribute causes java.lang.ClassCastException: |  Critical | Table API & SQL | Roman Wozniak | Fabian Hueske |


### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [FLINK-9688](https://issues.apache.org/jira/browse/FLINK-9688) | ATAN2 Sql Function support |  Minor | Table API & SQL | Sergey Nuyanzin | Sergey Nuyanzin |
| [FLINK-7812](https://issues.apache.org/jira/browse/FLINK-7812) | Log system resources as metrics |  Major | Metrics | Piotr Nowojski | Piotr Nowojski |
| [FLINK-10022](https://issues.apache.org/jira/browse/FLINK-10022) | Add metrics for input/output buffers |  Major | Metrics, Network | Nico Kruber | Nico Kruber |
| [FLINK-9850](https://issues.apache.org/jira/browse/FLINK-9850) | Add a string to the print method to identify output for DataStream |  Major | DataStream API | Hequn Cheng | vinoyang |
| [FLINK-10163](https://issues.apache.org/jira/browse/FLINK-10163) | Support CREATE VIEW in SQL Client |  Major | Table API & SQL | Timo Walther | Timo Walther |
| [FLINK-3875](https://issues.apache.org/jira/browse/FLINK-3875) | Add a TableSink for Elasticsearch |  Major | Streaming Connectors, Table API & SQL | Fabian Hueske | Timo Walther |
| [FLINK-9126](https://issues.apache.org/jira/browse/FLINK-9126) | Creation of the CassandraPojoInputFormat class to output data into a Custom Cassandra Annotated Pojo |  Minor | Cassandra Connector, DataSet API | Jeffrey Carter | Jeffrey Carter |
| [FLINK-10440](https://issues.apache.org/jira/browse/FLINK-10440) | Add CassandraPojoOutputFormat |  Major | Cassandra Connector | Benoit MERIAUX | Benoit MERIAUX |
| [FLINK-10423](https://issues.apache.org/jira/browse/FLINK-10423) | Expose RocksDB native metrics |  Major | Metrics, State Backends, Checkpointing | Seth Wiesman | Seth Wiesman |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [FLINK-9890](https://issues.apache.org/jira/browse/FLINK-9890) | Remove obsolete Class ResourceManagerConfiguration |  Major | Distributed Coordination | Gary Yao | Gary Yao |
| [FLINK-6222](https://issues.apache.org/jira/browse/FLINK-6222) | YARN: setting environment variables in an easier fashion |  Major | Startup Shell Scripts | Craig Foster | Dawid Wysakowicz |
| [FLINK-9236](https://issues.apache.org/jira/browse/FLINK-9236) | Use Apache Parent POM 20 |  Major | Build System | Ted Yu | jiayichao |
| [FLINK-9944](https://issues.apache.org/jira/browse/FLINK-9944) | Cleanup end-to-end test poms |  Major | Build System, Tests | Chesnay Schepler | Chesnay Schepler |
| [FLINK-9504](https://issues.apache.org/jira/browse/FLINK-9504) | Change checkpoint duration log level to debug to avoid too much log |  Minor | State Backends, Checkpointing | aitozi | aitozi |
| [FLINK-10055](https://issues.apache.org/jira/browse/FLINK-10055) | incorrect in-progress file suffix in BucketingSink's java doc |  Minor | Documentation | Bowen Li | Bowen Li |
| [FLINK-10073](https://issues.apache.org/jira/browse/FLINK-10073) | Allow setting a restart strategy in SQL Client |  Major | Table API & SQL | Timo Walther | Timo Walther |
| [FLINK-10094](https://issues.apache.org/jira/browse/FLINK-10094) | Always backup default config for end-to-end tests |  Major | Tests | Chesnay Schepler | Chesnay Schepler |
| [FLINK-9933](https://issues.apache.org/jira/browse/FLINK-9933) | Simplify taskmanager memory default values |  Minor | Documentation | Chesnay Schepler | vinoyang |
| [FLINK-10102](https://issues.apache.org/jira/browse/FLINK-10102) | EXECUTION\_FAILOVER\_STRATEGY docs are wrong |  Major | Distributed Coordination, Documentation | Chesnay Schepler | Chesnay Schepler |
| [FLINK-9240](https://issues.apache.org/jira/browse/FLINK-9240) | Avoid deprecated akka methods |  Minor | Client, Local Runtime, Mesos, Tests, Web Client, YARN | Arnout Engelen |  |
| [FLINK-10099](https://issues.apache.org/jira/browse/FLINK-10099) | Rework YarnResourceManagerTest |  Major | Tests | TisonKun | TisonKun |
| [FLINK-9795](https://issues.apache.org/jira/browse/FLINK-9795) | Update Mesos documentation for flip6 |  Major | Documentation | Leonid Ishimnikov | Gary Yao |
| [FLINK-9919](https://issues.apache.org/jira/browse/FLINK-9919) | Remove unit from fullRestarts metric docs |  Major | Documentation, Metrics | Chesnay Schepler | vinoyang |
| [FLINK-10006](https://issues.apache.org/jira/browse/FLINK-10006) | Improve logging in BarrierBuffer |  Major | Logging, Network | Nico Kruber | Nico Kruber |
| [FLINK-9977](https://issues.apache.org/jira/browse/FLINK-9977) | Refine the docs for Table/SQL built-in functions |  Minor | Documentation | Xingcan Cui | Xingcan Cui |
| [FLINK-8135](https://issues.apache.org/jira/browse/FLINK-8135) | Add description to MessageParameter |  Major | Documentation, REST | Chesnay Schepler | Andrei |
| [FLINK-9853](https://issues.apache.org/jira/browse/FLINK-9853) | add hex support in table api and sql |  Major | Table API & SQL | xueyu | xueyu |
| [FLINK-10123](https://issues.apache.org/jira/browse/FLINK-10123) | Use ExecutorThreadFactory instead of DefaultThreadFactory in RestServer/Client |  Major | REST | Till Rohrmann | Till Rohrmann |
| [FLINK-10056](https://issues.apache.org/jira/browse/FLINK-10056) | Add testRequestNextInputSplit |  Major | JobManager, Tests | TisonKun | TisonKun |
| [FLINK-8290](https://issues.apache.org/jira/browse/FLINK-8290) | Allow setting clientId in flink-connector-kafka-0.8 |  Major | . | xymaqingxiang | xymaqingxiang |
| [FLINK-10110](https://issues.apache.org/jira/browse/FLINK-10110) | Harden e2e Kafka shutdown |  Major | Tests | Till Rohrmann | Till Rohrmann |
| [FLINK-9013](https://issues.apache.org/jira/browse/FLINK-9013) | Document yarn.containers.vcores only being effective when adapting YARN config |  Major | Documentation, YARN | Nico Kruber | Dawid Wysakowicz |
| [FLINK-9446](https://issues.apache.org/jira/browse/FLINK-9446) | Compatibility table not up-to-date |  Major | Documentation | Razvan | Chesnay Schepler |
| [FLINK-9859](https://issues.apache.org/jira/browse/FLINK-9859) | More Akka config options |  Major | Local Runtime | TisonKun | TisonKun |
| [FLINK-10020](https://issues.apache.org/jira/browse/FLINK-10020) | Kinesis Consumer listShards should support more recoverable exceptions |  Major | Kinesis Connector | Thomas Weise | Thomas Weise |
| [FLINK-9899](https://issues.apache.org/jira/browse/FLINK-9899) | Add more metrics to the Kinesis source connector |  Major | Kinesis Connector | Lakshmi Rao | Lakshmi Rao |
| [FLINK-10001](https://issues.apache.org/jira/browse/FLINK-10001) | Improve Kubernetes documentation |  Major | Documentation, Kubernetes | Till Rohrmann | Till Rohrmann |
| [FLINK-6670](https://issues.apache.org/jira/browse/FLINK-6670) | Remove CommonTestUtils.createTempDirectory() |  Minor | Tests | Nico Kruber | Chesnay Schepler |
| [FLINK-10082](https://issues.apache.org/jira/browse/FLINK-10082) | Initialize StringBuilder in Slf4jReporter with estimated size |  Major | Metrics | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10181](https://issues.apache.org/jira/browse/FLINK-10181) | Add anchor link to individual rest requests |  Major | Documentation, REST | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10164](https://issues.apache.org/jira/browse/FLINK-10164) | Add support for resuming from savepoints to StandaloneJobClusterEntrypoint |  Major | Distributed Coordination | Till Rohrmann | Till Rohrmann |
| [FLINK-10233](https://issues.apache.org/jira/browse/FLINK-10233) | Undeprecate ConfigOption#withDescription(String) |  Major | Configuration, Core | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10137](https://issues.apache.org/jira/browse/FLINK-10137) | YARN: Log completed Containers |  Major | Distributed Coordination, ResourceManager, YARN | Gary Yao | Gary Yao |
| [FLINK-9642](https://issues.apache.org/jira/browse/FLINK-9642) | Add caching layer to SharedBuffer |  Major | CEP | aitozi | aitozi |
| [FLINK-10270](https://issues.apache.org/jira/browse/FLINK-10270) | Delete LegacyRestHandlerAdapter |  Major | REST | Gary Yao | Gary Yao |
| [FLINK-7551](https://issues.apache.org/jira/browse/FLINK-7551) | Add VERSION to the REST urls. |  Critical | REST | Kostas Kloudas | Chesnay Schepler |
| [FLINK-10186](https://issues.apache.org/jira/browse/FLINK-10186) | Use ThreadLocalRandom in BufferSpiller constructor |  Major | Streaming | Hiroaki Yoshida | Hiroaki Yoshida |
| [FLINK-10254](https://issues.apache.org/jira/browse/FLINK-10254) | Fix inappropriate checkNotNull in stateBackend |  Major | State Backends, Checkpointing | aitozi | aitozi |
| [FLINK-10131](https://issues.apache.org/jira/browse/FLINK-10131) | Improve logging around ResultSubpartition |  Major | Logging, Network | Nico Kruber | Nico Kruber |
| [FLINK-10170](https://issues.apache.org/jira/browse/FLINK-10170) | Support string representation for map and array types in descriptor-based Table API |  Major | Table API & SQL | Jun Zhang | Jun Zhang |
| [FLINK-10185](https://issues.apache.org/jira/browse/FLINK-10185) | Make ZooKeeperStateHandleStore#releaseAndTryRemove synchronous |  Major | Distributed Coordination | Till Rohrmann | Till Rohrmann |
| [FLINK-10301](https://issues.apache.org/jira/browse/FLINK-10301) | Allow a custom Configuration in StreamNetworkBenchmarkEnvironment |  Major | Network, Tests | Nico Kruber | Nico Kruber |
| [FLINK-10223](https://issues.apache.org/jira/browse/FLINK-10223) | TaskManagers should log their ResourceID during startup |  Major | Distributed Coordination | Konstantin Knauf | Gary Yao |
| [FLINK-10325](https://issues.apache.org/jira/browse/FLINK-10325) | [State TTL] Refactor TtlListState to use only loops, no java stream API for performance |  Major | State Backends, Checkpointing | Andrey Zagrebin | Andrey Zagrebin |
| [FLINK-10215](https://issues.apache.org/jira/browse/FLINK-10215) | Add configuration of java option  for historyserver |  Minor | History Server | liuxianjiao | liuxianjiao |
| [FLINK-9912](https://issues.apache.org/jira/browse/FLINK-9912) | Release TaskExecutors from SlotPool if all slots have been removed |  Major | Distributed Coordination | Till Rohrmann | Till Rohrmann |
| [FLINK-9917](https://issues.apache.org/jira/browse/FLINK-9917) | Remove superfluous lock from SlotSharingManager |  Major | JobManager | Till Rohrmann | Till Rohrmann |
| [FLINK-10365](https://issues.apache.org/jira/browse/FLINK-10365) | Consolidate shaded Hadoop classes for filesystems |  Major | FileSystem | Stephan Ewen | Stephan Ewen |
| [FLINK-10366](https://issues.apache.org/jira/browse/FLINK-10366) | Create a shared base for S3 file systems |  Major | FileSystem | Stephan Ewen | Stephan Ewen |
| [FLINK-8660](https://issues.apache.org/jira/browse/FLINK-8660) | Enable the user to provide custom HAServices implementation |  Major | Cluster Management, Configuration, Distributed Coordination | Krzysztof Białek | Krzysztof Białek |
| [FLINK-10050](https://issues.apache.org/jira/browse/FLINK-10050) | Support 'allowedLateness' in CoGroupedStreams |  Major | Streaming | eugen yushin |  |
| [FLINK-10260](https://issues.apache.org/jira/browse/FLINK-10260) | Confusing log messages during TaskManager registration |  Major | ResourceManager | Stephan Ewen | Andrey Zagrebin |
| [FLINK-10375](https://issues.apache.org/jira/browse/FLINK-10375) | ExceptionInChainedStubException hides wrapped exception in cause |  Minor | Core | Mike Pedersen | Mike Pedersen |
| [FLINK-9913](https://issues.apache.org/jira/browse/FLINK-9913) | Improve output serialization only once in RecordWriter |  Major | Network | zhijiang | zhijiang |
| [FLINK-10369](https://issues.apache.org/jira/browse/FLINK-10369) | Enable YARNITCase |  Minor | Tests | Till Rohrmann | Till Rohrmann |
| [FLINK-1960](https://issues.apache.org/jira/browse/FLINK-1960) | Add comments and docs for withForwardedFields and related operators |  Minor | Documentation | Theodore Vasiloudis | hzhuangzhenxi |
| [FLINK-10410](https://issues.apache.org/jira/browse/FLINK-10410) | Search for broken links on travis |  Major | Documentation, Travis | Chesnay Schepler | Chesnay Schepler |
| [FLINK-9455](https://issues.apache.org/jira/browse/FLINK-9455) | Make SlotManager aware of multi slot TaskManagers |  Major | Distributed Coordination, ResourceManager | Till Rohrmann | Till Rohrmann |
| [FLINK-10411](https://issues.apache.org/jira/browse/FLINK-10411) | Make ClusterEntrypoint more modular |  Minor | Distributed Coordination | Till Rohrmann | Till Rohrmann |
| [FLINK-10311](https://issues.apache.org/jira/browse/FLINK-10311) | HA end-to-end/Jepsen tests for standby Dispatchers |  Critical | Tests | Till Rohrmann | Gary Yao |
| [FLINK-10339](https://issues.apache.org/jira/browse/FLINK-10339) | SpillReadBufferPool cannot use off-heap memory |  Minor | Network | zhijiang | zhijiang |
| [FLINK-10291](https://issues.apache.org/jira/browse/FLINK-10291) | Generate JobGraph with fixed/configurable JobID in StandaloneJobClusterEntrypoint |  Critical | Distributed Coordination | Till Rohrmann | vinoyang |
| [FLINK-10371](https://issues.apache.org/jira/browse/FLINK-10371) | Allow to enable SSL mutual authentication on REST endpoints by configuration |  Major | Client, REST, Security | Johannes Dillmann | Johannes Dillmann |
| [FLINK-8532](https://issues.apache.org/jira/browse/FLINK-8532) | RebalancePartitioner should use Random value for its first partition |  Major | DataStream API | Yuta Morisawa | Guibo Pan |
| [FLINK-10514](https://issues.apache.org/jira/browse/FLINK-10514) | change Tachyon to Alluxio |  Minor | Streaming | yangxiaoshuo | Guibo Pan |
| [FLINK-10310](https://issues.apache.org/jira/browse/FLINK-10310) | Cassandra Sink - Handling failing requests |  Major | Cassandra Connector | Jayant Ameta |  |
| [FLINK-10399](https://issues.apache.org/jira/browse/FLINK-10399) | Refactor ParameterTool#fromArgs |  Major | Client | TisonKun | TisonKun |
| [FLINK-10282](https://issues.apache.org/jira/browse/FLINK-10282) | Provide separate thread-pool for REST endpoint |  Major | Local Runtime, REST | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10075](https://issues.apache.org/jira/browse/FLINK-10075) | HTTP connections to a secured REST endpoint flood the log |  Critical | REST | Stephan Ewen | Andrey Zagrebin |
| [FLINK-10554](https://issues.apache.org/jira/browse/FLINK-10554) | Bump flink-shaded dependency version |  Major | Build System | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10474](https://issues.apache.org/jira/browse/FLINK-10474) | Don't translate IN with Literals to JOIN with VALUES for streaming queries |  Major | Table API & SQL | Fabian Hueske | Hequn Cheng |
| [FLINK-9697](https://issues.apache.org/jira/browse/FLINK-9697) | Provide connector for modern Kafka |  Major | . | Ted Yu | vinoyang |
| [FLINK-9443](https://issues.apache.org/jira/browse/FLINK-9443) | Remove unused parameter in StreamGraphHasherV2 |  Major | Cluster Management | aitozi | aitozi |
| [FLINK-10312](https://issues.apache.org/jira/browse/FLINK-10312) | Wrong / missing exception when submitting job |  Critical | JobManager | Stephan Ewen | Andrey Zagrebin |
| [FLINK-9061](https://issues.apache.org/jira/browse/FLINK-9061) | Add entropy to s3 path for better scalability |  Critical | FileSystem, State Backends, Checkpointing | Jamie Grier | Indrajit Roychoudhury |
| [FLINK-10582](https://issues.apache.org/jira/browse/FLINK-10582) | Make REST executor thread priority configurable |  Major | Local Runtime, REST | Till Rohrmann | Till Rohrmann |
| [FLINK-10579](https://issues.apache.org/jira/browse/FLINK-10579) | Remove unused deploysettings.xml |  Major | Build System | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10511](https://issues.apache.org/jira/browse/FLINK-10511) | Code duplication of creating RPC service in ClusterEntrypoint and AkkaRpcServiceUtils |  Minor | Cluster Management | Shimin Yang | Shimin Yang |
| [FLINK-10057](https://issues.apache.org/jira/browse/FLINK-10057) | optimalize org.apache.flink.yarn.cli.FlinkYarnSessionCli.isYarnPropertiesFileMode |  Major | Client | liuzq |  |
| [FLINK-10563](https://issues.apache.org/jira/browse/FLINK-10563) | Expose shaded Presto S3 filesystem under "s3p" scheme |  Major | . | Aljoscha Krettek | Aljoscha Krettek |
| [FLINK-10608](https://issues.apache.org/jira/browse/FLINK-10608) | Add avro files generated by datastream-allround-test to RAT exclusions |  Major | Build System | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10614](https://issues.apache.org/jira/browse/FLINK-10614) | Update test\_batch\_allround.sh e2e to new testing infrastructure |  Minor | Tests | Till Rohrmann | Till Rohrmann |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [FLINK-9185](https://issues.apache.org/jira/browse/FLINK-9185) | Potential null dereference in PrioritizedOperatorSubtaskState#resolvePrioritizedAlternatives |  Minor | . | Ted Yu | Stephen Jason |
| [FLINK-5750](https://issues.apache.org/jira/browse/FLINK-5750) | Incorrect translation of n-ary Union |  Critical | Table API & SQL | Anton Mushin | Alexander Koltsov |
| [FLINK-9914](https://issues.apache.org/jira/browse/FLINK-9914) | Flink docker information in official website is out of date and should be update |  Minor | . | vinoyang | vinoyang |
| [FLINK-9927](https://issues.apache.org/jira/browse/FLINK-9927) | Error in Python Stream API example on website |  Minor | Documentation | Joe Malt |  |
| [FLINK-9562](https://issues.apache.org/jira/browse/FLINK-9562) | Wrong wording in flink-optimizer module |  Trivial | Optimizer | Alexandr Arkhipov | Alexandr Arkhipov |
| [FLINK-9972](https://issues.apache.org/jira/browse/FLINK-9972) | Debug memory logging not working |  Critical | TaskManager | Piotr Nowojski | Piotr Nowojski |
| [FLINK-10063](https://issues.apache.org/jira/browse/FLINK-10063) | Jepsen: Automatically restart Mesos Processes |  Critical | Tests | Gary Yao | Gary Yao |
| [FLINK-10105](https://issues.apache.org/jira/browse/FLINK-10105) | Test failure because of jobmanager.execution.failover-strategy is outdated |  Major | Tests | vinoyang | Dawid Wysakowicz |
| [FLINK-9693](https://issues.apache.org/jira/browse/FLINK-9693) | Possible memory leak in jobmanager retaining archived checkpoints |  Major | JobManager, State Backends, Checkpointing | Steven Zhen Wu | Till Rohrmann |
| [FLINK-9664](https://issues.apache.org/jira/browse/FLINK-9664) | FlinkML Quickstart Loading Data section example doesn't work as described |  Major | Documentation, Machine Learning Library | Mano Swerts | Rong Rong |
| [FLINK-9546](https://issues.apache.org/jira/browse/FLINK-9546) | The heartbeatTimeoutIntervalMs of HeartbeatMonitor should be larger than 0 |  Minor | Core | Sihua Zhou | Sihua Zhou |
| [FLINK-9289](https://issues.apache.org/jira/browse/FLINK-9289) | Parallelism of generated operators should have max parallism of input |  Major | DataSet API | Fabian Hueske | Xingcan Cui |
| [FLINK-10101](https://issues.apache.org/jira/browse/FLINK-10101) | Mesos web ui url is missing. |  Major | Mesos | Renjie Liu | Renjie Liu |
| [FLINK-10154](https://issues.apache.org/jira/browse/FLINK-10154) | Make sure we always read at least one record in KinesisConnector |  Minor | Kinesis Connector | Jamie Grier | Jamie Grier |
| [FLINK-10116](https://issues.apache.org/jira/browse/FLINK-10116) | createComparator fails on case class with Unit type fields prior to the join-key |  Major | DataSet API | Will | Fabian Hueske |
| [FLINK-10159](https://issues.apache.org/jira/browse/FLINK-10159) | TestHarness#initializeState(xyz) calls after TestHarness#open() are being silently ignored |  Critical | Tests | Piotr Nowojski | Piotr Nowojski |
| [FLINK-10169](https://issues.apache.org/jira/browse/FLINK-10169) | RowtimeValidator fails with custom TimestampExtractor |  Major | Table API & SQL | Timo Walther |  |
| [FLINK-10072](https://issues.apache.org/jira/browse/FLINK-10072) | Syntax and consistency issues in "The Broadcast State Pattern" |  Trivial | Documentation, Streaming | Rick Hofstede | Rick Hofstede |
| [FLINK-10151](https://issues.apache.org/jira/browse/FLINK-10151) | [State TTL] Fix false recursion call in TransformingStateTableKeyGroupPartitioner.tryAddToSource |  Major | State Backends, Checkpointing | Andrey Zagrebin | Andrey Zagrebin |
| [FLINK-10187](https://issues.apache.org/jira/browse/FLINK-10187) | Fix LogicalUnnestRule to match Correlate/Uncollect correctly |  Major | Table API & SQL | Shuyi Chen | Shuyi Chen |
| [FLINK-10172](https://issues.apache.org/jira/browse/FLINK-10172) | Inconsistentcy in ExpressionParser and ExpressionDsl for order by asc/desc |  Major | Table API & SQL | Rong Rong | Rong Rong |
| [FLINK-10175](https://issues.apache.org/jira/browse/FLINK-10175) | Fix concurrent access to shared buffer in map state / querable state |  Critical | State Backends, Checkpointing | Stefan Richter | Stefan Richter |
| [FLINK-10204](https://issues.apache.org/jira/browse/FLINK-10204) | StreamElementSerializer#copy broken for LatencyMarkers |  Major | Metrics, Streaming | Ben La Monica | Ben La Monica |
| [FLINK-10192](https://issues.apache.org/jira/browse/FLINK-10192) | SQL Client table visualization mode does not update correctly |  Major | Table API & SQL | Fabian Hueske | Timo Walther |
| [FLINK-10189](https://issues.apache.org/jira/browse/FLINK-10189) | FindBugs warnings: Inefficient use of keySet iterator instead of entrySet iterator |  Major | Streaming, Streaming Connectors | Hiroaki Yoshida | Hiroaki Yoshida |
| [FLINK-10138](https://issues.apache.org/jira/browse/FLINK-10138) | Queryable state (rocksdb) end-to-end test failed on Travis |  Blocker | Queryable State, Tests | Till Rohrmann |  |
| [FLINK-10142](https://issues.apache.org/jira/browse/FLINK-10142) | Reduce synchronization overhead for credit notifications |  Major | Network | Nico Kruber | Nico Kruber |
| [FLINK-10141](https://issues.apache.org/jira/browse/FLINK-10141) | Reduce lock contention introduced with 1.5 |  Major | Network | Nico Kruber | Nico Kruber |
| [FLINK-10115](https://issues.apache.org/jira/browse/FLINK-10115) | Content-length limit is also applied to FileUploads |  Major | REST, Webfrontend | Yazdan Shirvany | Chesnay Schepler |
| [FLINK-10150](https://issues.apache.org/jira/browse/FLINK-10150) | Chained batch operators interfere with each other other |  Blocker | Metrics, Webfrontend | Helmut Zechmann | Chesnay Schepler |
| [FLINK-10283](https://issues.apache.org/jira/browse/FLINK-10283) | FileCache logs unnecessary warnings |  Major | Local Runtime | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10261](https://issues.apache.org/jira/browse/FLINK-10261) | INSERT INTO does not work with ORDER BY clause |  Major | Table API & SQL | Timo Walther | xueyu |
| [FLINK-10193](https://issues.apache.org/jira/browse/FLINK-10193) | Default RPC timeout is used when triggering savepoint via JobMasterGateway |  Critical | Distributed Coordination | Gary Yao | Gary Yao |
| [FLINK-10174](https://issues.apache.org/jira/browse/FLINK-10174) | getbytes with no charsets test error for hex and toBase64 |  Major | Table API & SQL | xueyu | xueyu |
| [FLINK-10293](https://issues.apache.org/jira/browse/FLINK-10293) | RemoteStreamEnvironment does not forward port to RestClusterClient |  Major | Client, Streaming | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10281](https://issues.apache.org/jira/browse/FLINK-10281) | Fix string literal escaping throughout Table & SQL API |  Major | Table API & SQL | vinoyang | Timo Walther |
| [FLINK-10011](https://issues.apache.org/jira/browse/FLINK-10011) | Old job resurrected during HA failover |  Blocker | JobManager | Elias Levy | Till Rohrmann |
| [FLINK-10269](https://issues.apache.org/jira/browse/FLINK-10269) | Elasticsearch 6 UpdateRequest fail because of binary incompatibility |  Blocker | ElasticSearch Connector | Timo Walther | Timo Walther |
| [FLINK-10328](https://issues.apache.org/jira/browse/FLINK-10328) | Stopping the ZooKeeperSubmittedJobGraphStore should release all currently held locks |  Major | Distributed Coordination | Till Rohrmann | Till Rohrmann |
| [FLINK-10329](https://issues.apache.org/jira/browse/FLINK-10329) | Fail with exception if job cannot be removed by ZooKeeperSubmittedJobGraphStore#removeJobGraph |  Major | Distributed Coordination | Till Rohrmann | Till Rohrmann |
| [FLINK-10255](https://issues.apache.org/jira/browse/FLINK-10255) | Standby Dispatcher locks submitted JobGraphs |  Blocker | Distributed Coordination | Till Rohrmann | Till Rohrmann |
| [FLINK-10314](https://issues.apache.org/jira/browse/FLINK-10314) | Blocking calls in Execution Graph creation bring down cluster |  Blocker | Distributed Coordination, JobManager | Stephan Ewen | Till Rohrmann |
| [FLINK-9884](https://issues.apache.org/jira/browse/FLINK-9884) | Slot request may not be removed when it has already be assigned in slot manager |  Major | Cluster Management | shuai.xu | shuai.xu |
| [FLINK-10362](https://issues.apache.org/jira/browse/FLINK-10362) | Bundled S3 connectors load wrong Hadoop config |  Major | FileSystem | Stephan Ewen | Stephan Ewen |
| [FLINK-10363](https://issues.apache.org/jira/browse/FLINK-10363) | S3 FileSystem factory prints secrets into logs |  Critical | FileSystem | Stephan Ewen | Stephan Ewen |
| [FLINK-9567](https://issues.apache.org/jira/browse/FLINK-9567) | Flink does not release resource in Yarn Cluster mode |  Critical | Cluster Management, YARN | Shimin Yang | Shimin Yang |
| [FLINK-10157](https://issues.apache.org/jira/browse/FLINK-10157) | Allow \`null\` user values in map state with TTL |  Minor | State Backends, Checkpointing | chengjie.wu | Andrey Zagrebin |
| [FLINK-10376](https://issues.apache.org/jira/browse/FLINK-10376) | BlobCacheCleanupTest.testPermanentBlobCleanup failed on Travis |  Critical | Tests | Till Rohrmann | TisonKun |
| [FLINK-10389](https://issues.apache.org/jira/browse/FLINK-10389) | TaskManagerServicesConfiguration ctor contains self assignment |  Minor | . | Ted Yu | vinoyang |
| [FLINK-10383](https://issues.apache.org/jira/browse/FLINK-10383) | Hadoop configurations on the classpath seep into the S3 file system configs |  Major | FileSystem | Stephan Ewen | Stephan Ewen |
| [FLINK-10416](https://issues.apache.org/jira/browse/FLINK-10416) | Add files generated by jepsen tests to rat excludes |  Major | Build System, Tests | Dawid Wysakowicz | Dawid Wysakowicz |
| [FLINK-10263](https://issues.apache.org/jira/browse/FLINK-10263) | User-defined function with LITERAL paramters yields CompileException |  Major | Table API & SQL | Fabian Hueske | Timo Walther |
| [FLINK-10010](https://issues.apache.org/jira/browse/FLINK-10010) | Deprecate unused BaseAlignedWindowAssigner related components |  Major | DataStream API | Rong Rong | Rong Rong |
| [FLINK-10444](https://issues.apache.org/jira/browse/FLINK-10444) | Make S3 entropy injection work with FileSystem safety net |  Major | FileSystem | Stephan Ewen | Stephan Ewen |
| [FLINK-10400](https://issues.apache.org/jira/browse/FLINK-10400) | Return failed JobResult if job terminates in state FAILED or CANCELED |  Major | Client | Till Rohrmann | Till Rohrmann |
| [FLINK-10415](https://issues.apache.org/jira/browse/FLINK-10415) | RestClient does not react to lost connection |  Blocker | REST | Till Rohrmann | Till Rohrmann |
| [FLINK-10065](https://issues.apache.org/jira/browse/FLINK-10065) | InstantiationUtil.deserializeObject(InputStream in, ClassLoader cl, boolean isFailureTolerant) will close the inputStream |  Major | . | Congxian Qiu | Congxian Qiu |
| [FLINK-10450](https://issues.apache.org/jira/browse/FLINK-10450) | Broken links in the documentation |  Major | Documentation, Project Website | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10451](https://issues.apache.org/jira/browse/FLINK-10451) | TableFunctionCollector should handle the life cycle of ScalarFunction |  Major | Table API & SQL | Ruidong Li | Ruidong Li |
| [FLINK-10454](https://issues.apache.org/jira/browse/FLINK-10454) | Travis fails on ScheduleOrUpdateConsumersTest |  Critical | Tests | TisonKun | Till Rohrmann |
| [FLINK-10421](https://issues.apache.org/jira/browse/FLINK-10421) | Shaded Hadoop S3A end-to-end test failed on Travis |  Critical | Tests | Dawid Wysakowicz | Till Rohrmann |
| [FLINK-10487](https://issues.apache.org/jira/browse/FLINK-10487) | fix invalid Flink SQL example |  Major | Documentation, Table API & SQL | Bowen Li | Bowen Li |
| [FLINK-10513](https://issues.apache.org/jira/browse/FLINK-10513) | Replace TaskManagerActions#notifyFinalState with #updateTaskExecutionState |  Minor | TaskManager | Till Rohrmann | Till Rohrmann |
| [FLINK-10295](https://issues.apache.org/jira/browse/FLINK-10295) | Tokenisation of Program Args resulting in unexpected results |  Blocker | REST, Webfrontend | Gaurav Singhania | Andrey Zagrebin |
| [FLINK-10469](https://issues.apache.org/jira/browse/FLINK-10469) | FileChannel may not write the whole buffer in a single call to FileChannel.write(Buffer buffer) |  Blocker | Core, Network | Yun Gao | Nico Kruber |
| [FLINK-5542](https://issues.apache.org/jira/browse/FLINK-5542) | YARN client incorrectly uses local YARN config to check vcore capacity |  Major | YARN | Shannon Carey |  |
| [FLINK-10465](https://issues.apache.org/jira/browse/FLINK-10465) | Jepsen: runit supervised sshd is stopped on tear down |  Critical | Tests | Gary Yao | Gary Yao |
| [FLINK-10361](https://issues.apache.org/jira/browse/FLINK-10361) | Elasticsearch (v6.3.1) sink end-to-end test instable |  Critical | Tests | Till Rohrmann | Biao Liu |
| [FLINK-10316](https://issues.apache.org/jira/browse/FLINK-10316) | Add check to KinesisProducer that aws.region is set |  Minor | Kinesis Connector | Joseph Sims |  |
| [FLINK-10379](https://issues.apache.org/jira/browse/FLINK-10379) | Can not use Table Functions in Java Table API |  Critical | Table API & SQL | Piotr Nowojski | Hequn Cheng |
| [FLINK-9788](https://issues.apache.org/jira/browse/FLINK-9788) | ExecutionGraph Inconsistency prevents Job from recovering |  Blocker | Core | Gary Yao | Till Rohrmann |
| [FLINK-10529](https://issues.apache.org/jira/browse/FLINK-10529) | Add flink-s3-fs-base to the connectors in the travis stage file. |  Major | Build System | Kostas Kloudas | Kostas Kloudas |
| [FLINK-10524](https://issues.apache.org/jira/browse/FLINK-10524) | MemoryManagerConcurrentModReleaseTest.testConcurrentModificationWhileReleasing failed on travis |  Critical | Distributed Coordination, Tests | Chesnay Schepler | Till Rohrmann |
| [FLINK-10532](https://issues.apache.org/jira/browse/FLINK-10532) | Broken links in documentation |  Major | Documentation | Chesnay Schepler | Timo Walther |
| [FLINK-10544](https://issues.apache.org/jira/browse/FLINK-10544) | Remove custom settings.xml for snapshot deployments |  Major | Build System | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10530](https://issues.apache.org/jira/browse/FLINK-10530) | ProcessFailureCancelingITCase.testCancelingOnProcessFailure failed on Travis. |  Critical | Tests | Kostas Kloudas | Till Rohrmann |
| [FLINK-10516](https://issues.apache.org/jira/browse/FLINK-10516) | YarnApplicationMasterRunner does not initialize FileSystem with correct Flink Configuration during setup |  Major | YARN | Shuyi Chen | Shuyi Chen |
| [FLINK-10135](https://issues.apache.org/jira/browse/FLINK-10135) | Certain cluster-level metrics are no longer exposed |  Critical | JobManager, Metrics | Joey Echeverria | vinoyang |
| [FLINK-9932](https://issues.apache.org/jira/browse/FLINK-9932) | Timed-out TaskExecutor slot-offers to JobMaster leak the slot |  Blocker | Cluster Management | shuai.xu | shuai.xu |
| [FLINK-9891](https://issues.apache.org/jira/browse/FLINK-9891) | Flink cluster is not shutdown in YARN mode when Flink client is stopped |  Major | Client, YARN | Sergey Krasovskiy | Andrey Zagrebin |
| [FLINK-10390](https://issues.apache.org/jira/browse/FLINK-10390) | DataDog MetricReporter leaks connections |  Minor | Metrics | Elias Levy | Chesnay Schepler |
| [FLINK-10354](https://issues.apache.org/jira/browse/FLINK-10354) | Savepoints should be counted as retained checkpoints |  Major | State Backends, Checkpointing | Dawid Wysakowicz | Dawid Wysakowicz |
| [FLINK-10567](https://issues.apache.org/jira/browse/FLINK-10567) | Lost serialize fields when ttl state store with the mutable serializer |  Major | State Backends, Checkpointing | aitozi | aitozi |
| [FLINK-10580](https://issues.apache.org/jira/browse/FLINK-10580) | EventTimeWindowCheckpointingITCase fails on travis |  Blocker | Metrics, State Backends, Checkpointing, Streaming, Tests | Chesnay Schepler | Till Rohrmann |
| [FLINK-10537](https://issues.apache.org/jira/browse/FLINK-10537) | Network throughput performance regression after broadcast changes |  Major | Network | Piotr Nowojski | Piotr Nowojski |
| [FLINK-10259](https://issues.apache.org/jira/browse/FLINK-10259) | Key validation for GroupWindowAggregate is broken |  Major | Table API & SQL | Fabian Hueske | Fabian Hueske |
| [FLINK-4052](https://issues.apache.org/jira/browse/FLINK-4052) | Unstable test ConnectionUtilsTest |  Critical | Tests | Stephan Ewen | Stephan Ewen |
| [FLINK-10309](https://issues.apache.org/jira/browse/FLINK-10309) | Cancel flink job occurs java.net.ConnectException |  Critical | Distributed Coordination, REST | vinoyang | Gary Yao |
| [FLINK-10412](https://issues.apache.org/jira/browse/FLINK-10412) | toString field in AbstractID should be transient to avoid been serialized |  Major | Distributed Coordination | Zhu Zhu | vinoyang |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [FLINK-5860](https://issues.apache.org/jira/browse/FLINK-5860) | Replace all the file creating from java.io.tmpdir with TemporaryFolder |  Major | Tests | shijinkui | Mahesh Senniappan |
| [FLINK-10084](https://issues.apache.org/jira/browse/FLINK-10084) | Migration tests weren't updated for 1.5 |  Critical | Tests | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10139](https://issues.apache.org/jira/browse/FLINK-10139) | Update migration tests for 1.6 |  Major | Tests | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10201](https://issues.apache.org/jira/browse/FLINK-10201) | The batchTestUtil was mistakenly used in some stream sql tests |  Minor | Table API & SQL | Xingcan Cui | Xingcan Cui |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [FLINK-9951](https://issues.apache.org/jira/browse/FLINK-9951) | Update scm developerConnection |  Major | Build System | Chesnay Schepler | Chesnay Schepler |
| [FLINK-6846](https://issues.apache.org/jira/browse/FLINK-6846) | Add TIMESTAMPADD supported in TableAPI |  Major | Table API & SQL | sunjincheng | sunjincheng |
| [FLINK-9915](https://issues.apache.org/jira/browse/FLINK-9915) | Add TO\_BASE64 function for table/sql API |  Minor | Table API & SQL | vinoyang | vinoyang |
| [FLINK-9916](https://issues.apache.org/jira/browse/FLINK-9916) | Add FROM\_BASE64 function for table/sql API |  Minor | . | vinoyang | vinoyang |
| [FLINK-9928](https://issues.apache.org/jira/browse/FLINK-9928) | Add LOG2 function for table/sql API |  Minor | Table API & SQL | vinoyang | vinoyang |
| [FLINK-9135](https://issues.apache.org/jira/browse/FLINK-9135) | Remove AggregateReduceFunctionsRule once CALCITE-2216 is fixed |  Major | Table API & SQL | Fabian Hueske | Shuyi Chen |
| [FLINK-9637](https://issues.apache.org/jira/browse/FLINK-9637) | Add public user documentation for TTL feature |  Major | State Backends, Checkpointing | Andrey Zagrebin | Andrey Zagrebin |
| [FLINK-10087](https://issues.apache.org/jira/browse/FLINK-10087) | Update BucketingSinkMigrationTest |  Major | filesystem-connector | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10085](https://issues.apache.org/jira/browse/FLINK-10085) | Update AbstractOperatorRestoreTestBase |  Critical | State Backends, Checkpointing, Streaming, Tests | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10089](https://issues.apache.org/jira/browse/FLINK-10089) | Update FlinkKafkaConsumerBaseMigrationTest |  Major | Kafka Connector | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10090](https://issues.apache.org/jira/browse/FLINK-10090) | Update ContinuousFileProcessingMigrationTest |  Major | filesystem-connector | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10091](https://issues.apache.org/jira/browse/FLINK-10091) | Update WindowOperatorMigrationTest |  Major | Streaming | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10092](https://issues.apache.org/jira/browse/FLINK-10092) | Update StatefulJobSavepointMigrationITCase |  Major | State Backends, Checkpointing | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10109](https://issues.apache.org/jira/browse/FLINK-10109) | Add documentation for StreamingFileSink |  Major | Streaming Connectors | Aljoscha Krettek | Aljoscha Krettek |
| [FLINK-7205](https://issues.apache.org/jira/browse/FLINK-7205) | Add UUID supported in TableAPI/SQL |  Major | Table API & SQL | sunjincheng | Jiayi Liao |
| [FLINK-5232](https://issues.apache.org/jira/browse/FLINK-5232) | Add a Thread default uncaught exception handler on the JobManager |  Major | JobManager | Stephan Ewen | vinoyang |
| [FLINK-10059](https://issues.apache.org/jira/browse/FLINK-10059) | Add LTRIM supported in Table API and SQL |  Minor | Table API & SQL | vinoyang | vinoyang |
| [FLINK-10060](https://issues.apache.org/jira/browse/FLINK-10060) | Add RTRIM supported in Table API and SQL |  Minor | . | vinoyang | vinoyang |
| [FLINK-9781](https://issues.apache.org/jira/browse/FLINK-9781) | scala-maven-plugin fails on java 9 |  Major | Build System, Scala API | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10207](https://issues.apache.org/jira/browse/FLINK-10207) | Bump checkstyle-plugin to 8.9 |  Major | Build System | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10324](https://issues.apache.org/jira/browse/FLINK-10324) | Replace ZooKeeperStateHandleStore#getAllSortedByNameAndLock by getAllAndLock |  Minor | Distributed Coordination | Till Rohrmann | Till Rohrmann |
| [FLINK-10331](https://issues.apache.org/jira/browse/FLINK-10331) | Reduce number of flush requests to the network stack |  Major | Network | Nico Kruber | Nico Kruber |
| [FLINK-10332](https://issues.apache.org/jira/browse/FLINK-10332) | Move data available notification in PipelinedSubpartition out of the synchronized block |  Major | Network | Nico Kruber | Nico Kruber |
| [FLINK-9713](https://issues.apache.org/jira/browse/FLINK-9713) | Support versioned joins in planning phase |  Major | Table API & SQL | Piotr Nowojski | Piotr Nowojski |
| [FLINK-9714](https://issues.apache.org/jira/browse/FLINK-9714) | Support versioned joins with processing time |  Major | Table API & SQL | Piotr Nowojski | Piotr Nowojski |
| [FLINK-9738](https://issues.apache.org/jira/browse/FLINK-9738) | Provide a way to define Table Version Functions in Table API |  Major | Table API & SQL | Piotr Nowojski | Piotr Nowojski |
| [FLINK-10234](https://issues.apache.org/jira/browse/FLINK-10234) | Lambda usage in FutureUtils is ambigouos with Java 9 |  Major | Core | Chesnay Schepler | Chesnay Schepler |
| [FLINK-6847](https://issues.apache.org/jira/browse/FLINK-6847) | Add TIMESTAMPDIFF supported in TableAPI |  Major | Table API & SQL | sunjincheng |  |
| [FLINK-6813](https://issues.apache.org/jira/browse/FLINK-6813) | Add TIMESTAMPDIFF supported in SQL |  Major | Table API & SQL | sunjincheng | sunjincheng |
| [FLINK-10145](https://issues.apache.org/jira/browse/FLINK-10145) | Add replace supported in TableAPI and SQL |  Major | Table API & SQL | Guibo Pan | Guibo Pan |
| [FLINK-10393](https://issues.apache.org/jira/browse/FLINK-10393) | Remove legacy entrypoints from startup scripts |  Major | Startup Shell Scripts | Till Rohrmann | Till Rohrmann |
| [FLINK-10394](https://issues.apache.org/jira/browse/FLINK-10394) | Remove legacy mode testing profiles from Travis config |  Major | Build System | Till Rohrmann | Till Rohrmann |
| [FLINK-10395](https://issues.apache.org/jira/browse/FLINK-10395) | Remove legacy mode switch from parent pom |  Major | Build System | Till Rohrmann | Till Rohrmann |
| [FLINK-10396](https://issues.apache.org/jira/browse/FLINK-10396) | Remove codebase switch from MiniClusterResource |  Major | Tests | Till Rohrmann | Till Rohrmann |
| [FLINK-10397](https://issues.apache.org/jira/browse/FLINK-10397) | Remove CoreOptions#MODE |  Major | Configuration | Till Rohrmann | Till Rohrmann |
| [FLINK-10403](https://issues.apache.org/jira/browse/FLINK-10403) | Port JobManagerHAProcessFailureBatchRecoveryITCase to new code base |  Major | Tests | Till Rohrmann | Till Rohrmann |
| [FLINK-10402](https://issues.apache.org/jira/browse/FLINK-10402) | Port AbstractTaskManagerProcessFailureRecoveryTest to new code base |  Major | Tests | Till Rohrmann | Till Rohrmann |
| [FLINK-10401](https://issues.apache.org/jira/browse/FLINK-10401) | Port ProcessFailureCancelingITCase to new code base |  Major | Tests | Till Rohrmann | Till Rohrmann |
| [FLINK-10209](https://issues.apache.org/jira/browse/FLINK-10209) | Exclude jdk.tools dependency from hadoop when running with java 9 |  Major | Build System | Chesnay Schepler | Chesnay Schepler |
| [FLINK-8819](https://issues.apache.org/jira/browse/FLINK-8819) | Rework travis script to use build stages |  Trivial | Build System, Travis | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10470](https://issues.apache.org/jira/browse/FLINK-10470) | Add method to check if pattern can produce empty matches |  Major | CEP | Dawid Wysakowicz | Dawid Wysakowicz |
| [FLINK-10427](https://issues.apache.org/jira/browse/FLINK-10427) | Port JobSubmitTest to new code base |  Major | Tests | TisonKun | TisonKun |
| [FLINK-10406](https://issues.apache.org/jira/browse/FLINK-10406) | Port JobManagerTest to new code base |  Major | Tests | TisonKun | TisonKun |
| [FLINK-10453](https://issues.apache.org/jira/browse/FLINK-10453) | Move hadoop 2.4 travis profile into cron job |  Major | Travis | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10512](https://issues.apache.org/jira/browse/FLINK-10512) | Remove legacy REST API docs |  Major | Documentation, REST | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10208](https://issues.apache.org/jira/browse/FLINK-10208) | Bump mockito to 2.0+ |  Major | Build System, Tests | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10386](https://issues.apache.org/jira/browse/FLINK-10386) | Remove legacy class TaskExecutionStateListener |  Major | TaskManager | TisonKun | TisonKun |
| [FLINK-10227](https://issues.apache.org/jira/browse/FLINK-10227) | Remove usage of javax.xml.bind.DatatypeConverter |  Major | Core | Chesnay Schepler | Chesnay Schepler |
| [FLINK-9377](https://issues.apache.org/jira/browse/FLINK-9377) | Remove writing serializers as part of the checkpoint meta information |  Critical | State Backends, Checkpointing | Tzu-Li (Gordon) Tai | Tzu-Li (Gordon) Tai |
| [FLINK-10242](https://issues.apache.org/jira/browse/FLINK-10242) | Disable latency metrics by default |  Major | Configuration, Metrics | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10243](https://issues.apache.org/jira/browse/FLINK-10243) | Add option to reduce latency metrics granularity |  Major | Configuration, Metrics | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10426](https://issues.apache.org/jira/browse/FLINK-10426) | Port TaskTest to new code base |  Major | Tests | TisonKun | TisonKun |
| [FLINK-9752](https://issues.apache.org/jira/browse/FLINK-9752) | Add an S3 RecoverableWriter |  Major | Streaming Connectors | Stephan Ewen | Kostas Kloudas |
| [FLINK-10541](https://issues.apache.org/jira/browse/FLINK-10541) |  Removed unused legacy methods in TestBaseUtils |  Major | Tests | TisonKun | TisonKun |
| [FLINK-10549](https://issues.apache.org/jira/browse/FLINK-10549) | Remove Legacy\* tests |  Major | Tests | TisonKun | TisonKun |
| [FLINK-10545](https://issues.apache.org/jira/browse/FLINK-10545) | Remove JobManagerLeaderSessionIDITCase |  Major | Tests | TisonKun | TisonKun |
| [FLINK-10546](https://issues.apache.org/jira/browse/FLINK-10546) | Remove StandaloneMiniCluster |  Major | JobManager | TisonKun | TisonKun |
| [FLINK-10551](https://issues.apache.org/jira/browse/FLINK-10551) | Remove legacy REST handlers |  Major | REST | Chesnay Schepler | Chesnay Schepler |
| [FLINK-8865](https://issues.apache.org/jira/browse/FLINK-8865) | Add CLI query code completion in SQL Client |  Major | Table API & SQL | Timo Walther | xueyu |
| [FLINK-10547](https://issues.apache.org/jira/browse/FLINK-10547) | Remove LegacyCLI |  Minor | . | vinoyang | vinoyang |
| [FLINK-10559](https://issues.apache.org/jira/browse/FLINK-10559) | Remove LegacyLocalStreamEnvironment |  Major | Local Runtime | TisonKun | TisonKun |
| [FLINK-10565](https://issues.apache.org/jira/browse/FLINK-10565) | Refactor SchedulerTestBase |  Major | Tests | TisonKun | TisonKun |
| [FLINK-10405](https://issues.apache.org/jira/browse/FLINK-10405) | Port JobManagerFailsITCase to new code base |  Major | Tests | TisonKun | TisonKun |
| [FLINK-10247](https://issues.apache.org/jira/browse/FLINK-10247) | Run MetricQueryService in separate thread pool |  Critical | Metrics | Till Rohrmann | Shimin Yang |
| [FLINK-10253](https://issues.apache.org/jira/browse/FLINK-10253) | Run MetricQueryService with lower priority |  Critical | Metrics | Till Rohrmann | vinoyang |
| [FLINK-9715](https://issues.apache.org/jira/browse/FLINK-9715) | Support versioned joins with event time |  Major | Table API & SQL | Piotr Nowojski | Piotr Nowojski |
| [FLINK-10398](https://issues.apache.org/jira/browse/FLINK-10398) | Add Tanh math function supported in Table API and SQL |  Minor | Table API & SQL | vinoyang | vinoyang |
| [FLINK-10340](https://issues.apache.org/jira/browse/FLINK-10340) | Add Cosh math function supported in Table API and SQL |  Minor | Table API & SQL | Sergey Tsvetkov | vinoyang |
| [FLINK-10508](https://issues.apache.org/jira/browse/FLINK-10508) | Port JobManagerITCase to new code base |  Major | Tests | TisonKun | TisonKun |
| [FLINK-10602](https://issues.apache.org/jira/browse/FLINK-10602) | Run MetricFetcher in metrics ActorSystem |  Major | Metrics | Till Rohrmann | Till Rohrmann |


