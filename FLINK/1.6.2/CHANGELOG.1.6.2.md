
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Flink Changelog

## Release 1.6.2 - Unreleased (as of 2018-10-20)



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [FLINK-10260](https://issues.apache.org/jira/browse/FLINK-10260) | Confusing log messages during TaskManager registration |  Major | ResourceManager | Stephan Ewen | Andrey Zagrebin |
| [FLINK-10375](https://issues.apache.org/jira/browse/FLINK-10375) | ExceptionInChainedStubException hides wrapped exception in cause |  Minor | Core | Mike Pedersen | Mike Pedersen |
| [FLINK-10311](https://issues.apache.org/jira/browse/FLINK-10311) | HA end-to-end/Jepsen tests for standby Dispatchers |  Critical | Tests | Till Rohrmann | Gary Yao |
| [FLINK-10291](https://issues.apache.org/jira/browse/FLINK-10291) | Generate JobGraph with fixed/configurable JobID in StandaloneJobClusterEntrypoint |  Critical | Distributed Coordination | Till Rohrmann | vinoyang |
| [FLINK-10371](https://issues.apache.org/jira/browse/FLINK-10371) | Allow to enable SSL mutual authentication on REST endpoints by configuration |  Major | Client, REST, Security | Johannes Dillmann | Johannes Dillmann |
| [FLINK-10282](https://issues.apache.org/jira/browse/FLINK-10282) | Provide separate thread-pool for REST endpoint |  Major | Local Runtime, REST | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10075](https://issues.apache.org/jira/browse/FLINK-10075) | HTTP connections to a secured REST endpoint flood the log |  Critical | REST | Stephan Ewen | Andrey Zagrebin |
| [FLINK-10312](https://issues.apache.org/jira/browse/FLINK-10312) | Wrong / missing exception when submitting job |  Critical | JobManager | Stephan Ewen | Andrey Zagrebin |
| [FLINK-9061](https://issues.apache.org/jira/browse/FLINK-9061) | Add entropy to s3 path for better scalability |  Critical | FileSystem, State Backends, Checkpointing | Jamie Grier | Indrajit Roychoudhury |
| [FLINK-10582](https://issues.apache.org/jira/browse/FLINK-10582) | Make REST executor thread priority configurable |  Major | Local Runtime, REST | Till Rohrmann | Till Rohrmann |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [FLINK-10222](https://issues.apache.org/jira/browse/FLINK-10222) | Table scalar function expression parses error when function name equals the exists keyword suffix |  Major | Table API & SQL | vinoyang | vinoyang |
| [FLINK-9884](https://issues.apache.org/jira/browse/FLINK-9884) | Slot request may not be removed when it has already be assigned in slot manager |  Major | Cluster Management | shuai.xu | shuai.xu |
| [FLINK-10363](https://issues.apache.org/jira/browse/FLINK-10363) | S3 FileSystem factory prints secrets into logs |  Critical | FileSystem | Stephan Ewen | Stephan Ewen |
| [FLINK-9567](https://issues.apache.org/jira/browse/FLINK-9567) | Flink does not release resource in Yarn Cluster mode |  Critical | Cluster Management, YARN | Shimin Yang | Shimin Yang |
| [FLINK-10157](https://issues.apache.org/jira/browse/FLINK-10157) | Allow \`null\` user values in map state with TTL |  Minor | State Backends, Checkpointing | chengjie.wu | Andrey Zagrebin |
| [FLINK-10383](https://issues.apache.org/jira/browse/FLINK-10383) | Hadoop configurations on the classpath seep into the S3 file system configs |  Major | FileSystem | Stephan Ewen | Stephan Ewen |
| [FLINK-10263](https://issues.apache.org/jira/browse/FLINK-10263) | User-defined function with LITERAL paramters yields CompileException |  Major | Table API & SQL | Fabian Hueske | Timo Walther |
| [FLINK-10444](https://issues.apache.org/jira/browse/FLINK-10444) | Make S3 entropy injection work with FileSystem safety net |  Major | FileSystem | Stephan Ewen | Stephan Ewen |
| [FLINK-10400](https://issues.apache.org/jira/browse/FLINK-10400) | Return failed JobResult if job terminates in state FAILED or CANCELED |  Major | Client | Till Rohrmann | Till Rohrmann |
| [FLINK-10415](https://issues.apache.org/jira/browse/FLINK-10415) | RestClient does not react to lost connection |  Blocker | REST | Till Rohrmann | Till Rohrmann |
| [FLINK-10451](https://issues.apache.org/jira/browse/FLINK-10451) | TableFunctionCollector should handle the life cycle of ScalarFunction |  Major | Table API & SQL | Ruidong Li | Ruidong Li |
| [FLINK-10487](https://issues.apache.org/jira/browse/FLINK-10487) | fix invalid Flink SQL example |  Major | Documentation, Table API & SQL | Bowen Li | Bowen Li |
| [FLINK-10469](https://issues.apache.org/jira/browse/FLINK-10469) | FileChannel may not write the whole buffer in a single call to FileChannel.write(Buffer buffer) |  Blocker | Core, Network | Yun Gao | Nico Kruber |
| [FLINK-5542](https://issues.apache.org/jira/browse/FLINK-5542) | YARN client incorrectly uses local YARN config to check vcore capacity |  Major | YARN | Shannon Carey |  |
| [FLINK-10465](https://issues.apache.org/jira/browse/FLINK-10465) | Jepsen: runit supervised sshd is stopped on tear down |  Critical | Tests | Gary Yao | Gary Yao |
| [FLINK-10316](https://issues.apache.org/jira/browse/FLINK-10316) | Add check to KinesisProducer that aws.region is set |  Minor | Kinesis Connector | Joseph Sims |  |
| [FLINK-10379](https://issues.apache.org/jira/browse/FLINK-10379) | Can not use Table Functions in Java Table API |  Critical | Table API & SQL | Piotr Nowojski | Hequn Cheng |
| [FLINK-9788](https://issues.apache.org/jira/browse/FLINK-9788) | ExecutionGraph Inconsistency prevents Job from recovering |  Blocker | Core | Gary Yao | Till Rohrmann |
| [FLINK-10524](https://issues.apache.org/jira/browse/FLINK-10524) | MemoryManagerConcurrentModReleaseTest.testConcurrentModificationWhileReleasing failed on travis |  Critical | Distributed Coordination, Tests | Chesnay Schepler | Till Rohrmann |
| [FLINK-10532](https://issues.apache.org/jira/browse/FLINK-10532) | Broken links in documentation |  Major | Documentation | Chesnay Schepler | Timo Walther |
| [FLINK-10544](https://issues.apache.org/jira/browse/FLINK-10544) | Remove custom settings.xml for snapshot deployments |  Major | Build System | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10516](https://issues.apache.org/jira/browse/FLINK-10516) | YarnApplicationMasterRunner does not initialize FileSystem with correct Flink Configuration during setup |  Major | YARN | Shuyi Chen | Shuyi Chen |
| [FLINK-10135](https://issues.apache.org/jira/browse/FLINK-10135) | Certain cluster-level metrics are no longer exposed |  Critical | JobManager, Metrics | Joey Echeverria | vinoyang |
| [FLINK-9932](https://issues.apache.org/jira/browse/FLINK-9932) | Timed-out TaskExecutor slot-offers to JobMaster leak the slot |  Blocker | Cluster Management | shuai.xu | shuai.xu |
| [FLINK-9891](https://issues.apache.org/jira/browse/FLINK-9891) | Flink cluster is not shutdown in YARN mode when Flink client is stopped |  Major | Client, YARN | Sergey Krasovskiy | Andrey Zagrebin |
| [FLINK-10390](https://issues.apache.org/jira/browse/FLINK-10390) | DataDog MetricReporter leaks connections |  Minor | Metrics | Elias Levy | Chesnay Schepler |
| [FLINK-10354](https://issues.apache.org/jira/browse/FLINK-10354) | Savepoints should be counted as retained checkpoints |  Major | State Backends, Checkpointing | Dawid Wysakowicz | Dawid Wysakowicz |
| [FLINK-10259](https://issues.apache.org/jira/browse/FLINK-10259) | Key validation for GroupWindowAggregate is broken |  Major | Table API & SQL | Fabian Hueske | Fabian Hueske |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [FLINK-10331](https://issues.apache.org/jira/browse/FLINK-10331) | Reduce number of flush requests to the network stack |  Major | Network | Nico Kruber | Nico Kruber |
| [FLINK-10332](https://issues.apache.org/jira/browse/FLINK-10332) | Move data available notification in PipelinedSubpartition out of the synchronized block |  Major | Network | Nico Kruber | Nico Kruber |
| [FLINK-10242](https://issues.apache.org/jira/browse/FLINK-10242) | Disable latency metrics by default |  Major | Configuration, Metrics | Chesnay Schepler | Chesnay Schepler |
| [FLINK-10243](https://issues.apache.org/jira/browse/FLINK-10243) | Add option to reduce latency metrics granularity |  Major | Configuration, Metrics | Chesnay Schepler | Chesnay Schepler |
| [FLINK-9752](https://issues.apache.org/jira/browse/FLINK-9752) | Add an S3 RecoverableWriter |  Major | Streaming Connectors | Stephan Ewen | Kostas Kloudas |


