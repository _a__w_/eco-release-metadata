
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.9 - 2016-09-29



### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12436](https://issues.apache.org/jira/browse/CASSANDRA-12436) | Under some races commit log may incorrectly think it has unflushed data |  Major | . | Branimir Lambov | Branimir Lambov |
| [CASSANDRA-11195](https://issues.apache.org/jira/browse/CASSANDRA-11195) | paging may returns incomplete results on small page size |  Major | . | Jim Witschey | Benjamin Lerer |


