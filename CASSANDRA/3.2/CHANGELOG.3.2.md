
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.2 - 2016-01-11



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7392](https://issues.apache.org/jira/browse/CASSANDRA-7392) | Abort in-progress queries that time out |  Critical | Local Write-Read Paths | Jonathan Ellis | Stefania |
| [CASSANDRA-10852](https://issues.apache.org/jira/browse/CASSANDRA-10852) | Add requireAuthorization method to IAuthorizer |  Minor | . | Mike Adamson | Mike Adamson |
| [CASSANDRA-9303](https://issues.apache.org/jira/browse/CASSANDRA-9303) | Match cassandra-loader options in COPY FROM |  Critical | Tools | Jonathan Ellis | Stefania |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9945](https://issues.apache.org/jira/browse/CASSANDRA-9945) | Add transparent data encryption core classes |  Major | . | Jason Brown | Jason Brown |
| [CASSANDRA-10410](https://issues.apache.org/jira/browse/CASSANDRA-10410) | Minor random optimizations |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-7918](https://issues.apache.org/jira/browse/CASSANDRA-7918) | Provide graphing tool along with cassandra-stress |  Minor | Tools | Benedict | Ryan McGuire |
| [CASSANDRA-10616](https://issues.apache.org/jira/browse/CASSANDRA-10616) | add support for jacoco execfile merging and bump version |  Major | . | Russ Hatch | Russ Hatch |
| [CASSANDRA-10651](https://issues.apache.org/jira/browse/CASSANDRA-10651) | allow unit testing by defined list of files |  Major | . | Russ Hatch | Russ Hatch |
| [CASSANDRA-10310](https://issues.apache.org/jira/browse/CASSANDRA-10310) | Support type casting in selection clause |  Major | . | Jon Haddad | Benjamin Lerer |
| [CASSANDRA-10677](https://issues.apache.org/jira/browse/CASSANDRA-10677) | Improve performance of folderSize function |  Minor | Local Write-Read Paths | Briareus | Briareus |
| [CASSANDRA-10200](https://issues.apache.org/jira/browse/CASSANDRA-10200) | NetworkTopologyStrategy.calculateNaturalEndpoints is rather inefficient |  Minor | . | Branimir Lambov | Branimir Lambov |
| [CASSANDRA-10705](https://issues.apache.org/jira/browse/CASSANDRA-10705) | Fix typo in comment in cassandra.yaml |  Trivial | Configuration | Hobin Yoon |  |
| [CASSANDRA-9844](https://issues.apache.org/jira/browse/CASSANDRA-9844) | Reevaluate inspections in generate-idea-files target |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-10409](https://issues.apache.org/jira/browse/CASSANDRA-10409) | Specialize MultiCBuilder when building a single clustering |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-10225](https://issues.apache.org/jira/browse/CASSANDRA-10225) | Make compression ratio much more accurate |  Major | Tools | Jeremy Hanna | Brett Snyder |
| [CASSANDRA-7225](https://issues.apache.org/jira/browse/CASSANDRA-7225) | show CQL help in cqlsh in web browser |  Trivial | Documentation and Website, Tools | Robert Stupp | Robert Stupp |
| [CASSANDRA-10243](https://issues.apache.org/jira/browse/CASSANDRA-10243) | Warn or fail when changing cluster topology live |  Critical | Tools | Jonathan Ellis | Stefania |
| [CASSANDRA-10679](https://issues.apache.org/jira/browse/CASSANDRA-10679) | Normalize all the tools shell scripts |  Major | Tools | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-8639](https://issues.apache.org/jira/browse/CASSANDRA-8639) | Can OOM on CL replay with dense mutations |  Minor | Local Write-Read Paths | T Jake Luciani | Ariel Weisberg |
| [CASSANDRA-8142](https://issues.apache.org/jira/browse/CASSANDRA-8142) | prevent the command "cassandra start" from starting a cluster |  Major | Packaging | Steven Lowenthal | Robert Stupp |
| [CASSANDRA-9879](https://issues.apache.org/jira/browse/CASSANDRA-9879) | Add the name of the compressor in the output of sstablemetadata |  Trivial | Tools | Nicolas Lalevée | Nicolas Lalevée |
| [CASSANDRA-9748](https://issues.apache.org/jira/browse/CASSANDRA-9748) | Can't see other nodes when using multiple network interfaces |  Minor | Streaming and Messaging | Roman Bielik | Paulo Motta |
| [CASSANDRA-10718](https://issues.apache.org/jira/browse/CASSANDRA-10718) | Group pending compactions based on table |  Minor | . | Marcus Eriksson | Tushar Agrawal |
| [CASSANDRA-10149](https://issues.apache.org/jira/browse/CASSANDRA-10149) | Make nodetool cfstats and cfhistograms consistent |  Major | Tools | Jeremy Hanna |  |
| [CASSANDRA-10464](https://issues.apache.org/jira/browse/CASSANDRA-10464) | "nodetool compactionhistory" output should be sorted on compacted\_at column and the timestamp shown in human readable format |  Minor | Tools | Wei Deng | Michael Edge |
| [CASSANDRA-6992](https://issues.apache.org/jira/browse/CASSANDRA-6992) | Bootstrap on vnodes clusters can cause stampeding/storm behavior |  Minor | Coordination, Streaming and Messaging | Rick Branson | Paulo Motta |
| [CASSANDRA-9302](https://issues.apache.org/jira/browse/CASSANDRA-9302) | Optimize cqlsh COPY FROM, part 3 |  Critical | Tools | Jonathan Ellis | Stefania |
| [CASSANDRA-10750](https://issues.apache.org/jira/browse/CASSANDRA-10750) | Minor code improvements |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-10795](https://issues.apache.org/jira/browse/CASSANDRA-10795) | Improve Failure Detector Unknown EP message |  Minor | Observability | Anthony Cozzie | Anthony Cozzie |
| [CASSANDRA-9294](https://issues.apache.org/jira/browse/CASSANDRA-9294) | Streaming errors should log the root cause |  Major | Streaming and Messaging | Brandon Williams | Paulo Motta |
| [CASSANDRA-10708](https://issues.apache.org/jira/browse/CASSANDRA-10708) | Add forceUserDefinedCleanup to allow more flexible cleanup for operators |  Minor | Tools | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-10580](https://issues.apache.org/jira/browse/CASSANDRA-10580) | Add latency metrics for dropped messages |  Minor | Coordination, Observability | Anubhav Kale | Anubhav Kale |
| [CASSANDRA-10494](https://issues.apache.org/jira/browse/CASSANDRA-10494) | Move static JVM options to jvm.options file |  Minor | Configuration | Paulo Motta | Paulo Motta |
| [CASSANDRA-8755](https://issues.apache.org/jira/browse/CASSANDRA-8755) | Replace trivial uses of String.replace/replaceAll/split with StringUtils methods |  Trivial | . | Jaroslav Kamenik | Alexander Shopov |
| [CASSANDRA-6696](https://issues.apache.org/jira/browse/CASSANDRA-6696) | Partition sstables by token range |  Major | Compaction | sankalp kohli | Marcus Eriksson |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9241](https://issues.apache.org/jira/browse/CASSANDRA-9241) | ByteBuffer.array() without ByteBuffer.arrayOffset() + ByteBuffer.position() is a bug |  Minor | Testing | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-10555](https://issues.apache.org/jira/browse/CASSANDRA-10555) | ReadCommandTest should truncate between test cases |  Minor | Testing | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-10597](https://issues.apache.org/jira/browse/CASSANDRA-10597) | Error: unmappable character for encoding MS949 in ant build-test task. |  Trivial | Testing, Tools | Jaebin Lee |  |
| [CASSANDRA-10188](https://issues.apache.org/jira/browse/CASSANDRA-10188) | sstableloader does not use MAX\_HEAP\_SIZE env parameter |  Minor | . | Dan Hable | Michael Shuler |
| [CASSANDRA-10729](https://issues.apache.org/jira/browse/CASSANDRA-10729) | SELECT statement with IN restrictions on partition key + ORDER BY + LIMIT return wrong results |  Major | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-10816](https://issues.apache.org/jira/browse/CASSANDRA-10816) | Explicitly handle SSL handshake errors during connect() |  Major | Streaming and Messaging | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-10835](https://issues.apache.org/jira/browse/CASSANDRA-10835) | CqlInputFormat  creates too small splits for map Hadoop tasks |  Major | . | Artem Aliev |  |
| [CASSANDRA-10824](https://issues.apache.org/jira/browse/CASSANDRA-10824) | Cast functions do not work properly on Counter columns |  Major | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-8805](https://issues.apache.org/jira/browse/CASSANDRA-8805) | runWithCompactionsDisabled only cancels compactions, which is not the only source of markCompacted |  Major | Compaction | Benedict | Carl Yeksigian |
| [CASSANDRA-10851](https://issues.apache.org/jira/browse/CASSANDRA-10851) | deb file conflict on sstablemetadata |  Major | Packaging | Michael Shuler | Michael Shuler |
| [CASSANDRA-10822](https://issues.apache.org/jira/browse/CASSANDRA-10822) | SSTable data loss when upgrading with row tombstone present |  Critical | . | Andy Tolbert | Branimir Lambov |
| [CASSANDRA-9179](https://issues.apache.org/jira/browse/CASSANDRA-9179) | Unable to "point in time" restore if table/cf has been recreated |  Major | CQL, Distributed Metadata | Jon Moses | Branimir Lambov |
| [CASSANDRA-10806](https://issues.apache.org/jira/browse/CASSANDRA-10806) | sstableloader can't handle upper case keyspace |  Minor | Tools | Alex Liu | Alex Liu |
| [CASSANDRA-10837](https://issues.apache.org/jira/browse/CASSANDRA-10837) | Cluster/session should be closed in Cassandra Hadoop Input/Output classes |  Major | CQL | Alex Liu | Alex Liu |
| [CASSANDRA-10653](https://issues.apache.org/jira/browse/CASSANDRA-10653) | Remove dependency on jgrapht for UDT resolution |  Minor | Distributed Metadata | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-10541](https://issues.apache.org/jira/browse/CASSANDRA-10541) | cqlshlib tests cannot run on Windows |  Minor | Tools | Benjamin Lerer | Paulo Motta |
| [CASSANDRA-10873](https://issues.apache.org/jira/browse/CASSANDRA-10873) | Allow sstableloader to work with 3rd party authentication providers |  Major | Tools | Mike Adamson | Mike Adamson |
| [CASSANDRA-9813](https://issues.apache.org/jira/browse/CASSANDRA-9813) | cqlsh column header can be incorrect when no rows are returned |  Major | . | Aleksey Yeschenko | Adam Holmberg |
| [CASSANDRA-10593](https://issues.apache.org/jira/browse/CASSANDRA-10593) | Unintended interactions between commitlog archiving and commitlog recycling |  Major | Local Write-Read Paths | J.B. Langston | Ariel Weisberg |
| [CASSANDRA-10797](https://issues.apache.org/jira/browse/CASSANDRA-10797) | Bootstrap new node fails with OOM when streaming nodes contains thousands of sstables |  Major | Streaming and Messaging | Jose Martinez Poblete | Paulo Motta |
| [CASSANDRA-10921](https://issues.apache.org/jira/browse/CASSANDRA-10921) | Bump CQL version on 3.0 |  Major | Documentation and Website | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-10701](https://issues.apache.org/jira/browse/CASSANDRA-10701) | stop referring to batches as atomic |  Minor | . | Jon Haddad | Sylvain Lebresne |
| [CASSANDRA-10897](https://issues.apache.org/jira/browse/CASSANDRA-10897) | Avoid building PartitionUpdate in toString() |  Minor | Coordination | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-10711](https://issues.apache.org/jira/browse/CASSANDRA-10711) | NoSuchElementException when executing empty batch. |  Major | CQL | Jaroslav Kamenik | ZhaoYang |
| [CASSANDRA-10903](https://issues.apache.org/jira/browse/CASSANDRA-10903) | AssertionError while reading sstable when querying static column |  Major | Local Write-Read Paths | Tyler Hobbs | Sylvain Lebresne |
| [CASSANDRA-10854](https://issues.apache.org/jira/browse/CASSANDRA-10854) | cqlsh COPY FROM csv having line with more than one consecutive  ',' delimiter  is throwing 'list index out of range' |  Minor | Tools | Puspendu Banerjee | Stefania |
| [CASSANDRA-10949](https://issues.apache.org/jira/browse/CASSANDRA-10949) | SSTableMultiWriter streaming bug |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-10950](https://issues.apache.org/jira/browse/CASSANDRA-10950) | Fix HintsCatalogTest |  Minor | Testing | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-9309](https://issues.apache.org/jira/browse/CASSANDRA-9309) | Wrong interpretation of Config.getOutboundBindAny depending on using SSL or not |  Major | Streaming and Messaging | Casey Marshall | Yuki Morishita |
| [CASSANDRA-10931](https://issues.apache.org/jira/browse/CASSANDRA-10931) | CassandraVersion complains about 3.x version strings |  Major | Testing | Robert Stupp | Yuki Morishita |
| [CASSANDRA-10880](https://issues.apache.org/jira/browse/CASSANDRA-10880) | Paging state between 2.2 and 3.0 are incompatible on protocol v4 |  Critical | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-10951](https://issues.apache.org/jira/browse/CASSANDRA-10951) | Fix ReadCommandTest |  Blocker | Local Write-Read Paths, Testing | Yuki Morishita | Branimir Lambov |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10799](https://issues.apache.org/jira/browse/CASSANDRA-10799) | 2 cqlshlib tests still failing with cythonized driver installation |  Major | Testing | Stefania | Stefania |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10678](https://issues.apache.org/jira/browse/CASSANDRA-10678) | add SSTable flush observer |  Major | Local Write-Read Paths | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-10681](https://issues.apache.org/jira/browse/CASSANDRA-10681) | make index building pluggable via IndexBuildTask |  Minor | Local Write-Read Paths | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-9991](https://issues.apache.org/jira/browse/CASSANDRA-9991) | Implement efficient btree removal |  Major | . | Benedict | Piotr Jastrzebski |
| [CASSANDRA-10859](https://issues.apache.org/jira/browse/CASSANDRA-10859) | AssertionError in nodetool cfhistograms |  Major | Tools | Jim Witschey | Yuki Morishita |
| [CASSANDRA-9494](https://issues.apache.org/jira/browse/CASSANDRA-9494) | Need to set TTL with COPY command |  Major | CQL | Ed Chen | Stefania |
| [CASSANDRA-9428](https://issues.apache.org/jira/browse/CASSANDRA-9428) | Implement hints compression |  Major | Coordination | Aleksey Yeschenko | Blake Eggleston |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10904](https://issues.apache.org/jira/browse/CASSANDRA-10904) | Add upgrade procedure related to new role based access control in NEWS.txt |  Major | Documentation and Website | Reynald Bourtembourg |  |


