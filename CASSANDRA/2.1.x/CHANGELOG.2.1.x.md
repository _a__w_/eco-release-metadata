
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1.x - Unreleased (as of 2018-10-20)



### IMPORTANT ISSUES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10255](https://issues.apache.org/jira/browse/CASSANDRA-10255) | AttributeError: 'module' object has no attribute 'compress' |  Major | Configuration, Packaging | navithejesh |  |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10764](https://issues.apache.org/jira/browse/CASSANDRA-10764) | Cassandra Daemon should print JVM arguments |  Minor | Lifecycle, Observability | Anubhav Kale | Robert Stupp |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9798](https://issues.apache.org/jira/browse/CASSANDRA-9798) | Cassandra seems to have deadlocks during flush operations |  Major | . | Łukasz Mrożkiewicz |  |
| [CASSANDRA-9956](https://issues.apache.org/jira/browse/CASSANDRA-9956) | Stream failed during a rebuild |  Major | . | Boudigue |  |
| [CASSANDRA-9619](https://issues.apache.org/jira/browse/CASSANDRA-9619) | Read performance regression in tables with many columns on trunk and 2.2 vs. 2.1 |  Major | . | Jim Witschey | Benedict |
| [CASSANDRA-11047](https://issues.apache.org/jira/browse/CASSANDRA-11047) | native protocol will not bind ipv6 |  Major | CQL | Brandon Williams | Norman Maurer |
| [CASSANDRA-11121](https://issues.apache.org/jira/browse/CASSANDRA-11121) | cqlsh\_copy\_test failing on 2.1 |  Major | Tools | Philip Thompson | Stefania |
| [CASSANDRA-11549](https://issues.apache.org/jira/browse/CASSANDRA-11549) | cqlsh: COPY FROM ignores NULL values in conversion |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-10583](https://issues.apache.org/jira/browse/CASSANDRA-10583) | After bulk loading CQL query on timestamp column returns wrong result |  Major | . | Kai Wang |  |
| [CASSANDRA-10928](https://issues.apache.org/jira/browse/CASSANDRA-10928) | SSTableExportTest.testExportColumnsWithMetadata randomly fails |  Major | Testing | Michael Kjellman | sankalp kohli |
| [CASSANDRA-12406](https://issues.apache.org/jira/browse/CASSANDRA-12406) | dtest failure in pushed\_notifications\_test.TestPushedNotifications.move\_single\_node\_test |  Major | . | Sean McCarthy | Paulo Motta |
| [CASSANDRA-13847](https://issues.apache.org/jira/browse/CASSANDRA-13847) | test failure in cqlsh\_tests.cqlsh\_tests.CqlLoginTest.test\_list\_roles\_after\_login |  Major | Testing, Tools | Joel Knighton | Andrés de la Peña |


