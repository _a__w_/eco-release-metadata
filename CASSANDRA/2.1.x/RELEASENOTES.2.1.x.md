
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra  2.1.x Release Notes

These release notes cover new developer and user-facing incompatibilities, important issues, features, and major improvements.


---

* [CASSANDRA-10255](https://issues.apache.org/jira/browse/CASSANDRA-10255) | *Major* | **AttributeError: 'module' object has no attribute 'compress'**

Hi Recently i have upgrade my server from cassandra version 2.0.11 to 2.1.8 using tarball installation.And i have added one node to existing culster with the same version of 2.1.8. All the configuration settings are done but am not able to run cqlsh from bin directory.and this is the error
AttributeError: 'module' object has no attribute 'compress'

{code}
./cqlsh
Traceback (most recent call last):
 File "./cqlsh", line 111, in \<module\>
    from cassandra.cluster import Cluster, PagedResult
File "/home/cassandra/apache-cassandra-2.1.8/bin/../lib/cassandra-driver-internal-only-2.6.0c2.post.zip/cassandra-driver-2.6.0c2.post/cassandra/cluster.py", line 49, in \<module\>
 File "/home/cassandra/apache-cassandra-2.1.8/bin/../lib/cassandra-driver-internal-only-2.6.0c2.post.zip/cassandra-driver-2.6.0c2.post/cassandra/connection.py", line 88, in \<module\>
AttributeError: 'module' object has no attribute 'compress'
{code}



