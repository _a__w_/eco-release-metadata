
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.0.6 - 2011-12-14



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3541](https://issues.apache.org/jira/browse/CASSANDRA-3541) | Support timeuuid column names |  Minor | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-3419](https://issues.apache.org/jira/browse/CASSANDRA-3419) | CQL queries should allow CF names to be qualified by keyspace, part 2 |  Minor | CQL | paul cannon | Pavel Yaskevich |
| [CASSANDRA-3337](https://issues.apache.org/jira/browse/CASSANDRA-3337) | Create a way to obliterate nodes from the ring |  Major | . | Brandon Williams | Brandon Williams |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2799](https://issues.apache.org/jira/browse/CASSANDRA-2799) | Implement old style api support for ColumnFamilyInputFormat and ColumnFamilyRecordReader |  Minor | . | Jeremy Hanna | Steeve Morin |
| [CASSANDRA-3562](https://issues.apache.org/jira/browse/CASSANDRA-3562) | StorageProxy log improvement for RangeSlice (CL) |  Minor | . | Pedro Guilherme Rafael | Pedro Guilherme Rafael |
| [CASSANDRA-3572](https://issues.apache.org/jira/browse/CASSANDRA-3572) | make NodeCommand enum sorted to avoid constant merge conflicts |  Minor | . | Peter Schuller | Peter Schuller |
| [CASSANDRA-2268](https://issues.apache.org/jira/browse/CASSANDRA-2268) | CQL-enabled stress.java |  Minor | Tools | Eric Evans | Eric Evans |
| [CASSANDRA-3220](https://issues.apache.org/jira/browse/CASSANDRA-3220) | add describe\_ring to cli |  Minor | Tools | Jackson Chung | Jackson Chung |
| [CASSANDRA-3591](https://issues.apache.org/jira/browse/CASSANDRA-3591) | a better exception than "Keyspace names must be case-insensitively unique" |  Trivial | CQL | Dominique De Vito | Dominique De Vito |
| [CASSANDRA-3602](https://issues.apache.org/jira/browse/CASSANDRA-3602) | Remove test/distributed |  Trivial | . | Brandon Williams | Brandon Williams |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3489](https://issues.apache.org/jira/browse/CASSANDRA-3489) | EncryptionOptions should be instantiated |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3457](https://issues.apache.org/jira/browse/CASSANDRA-3457) | Make cqlsh look for a suitable python version |  Minor | Tools | paul cannon | paul cannon |
| [CASSANDRA-3546](https://issues.apache.org/jira/browse/CASSANDRA-3546) | Hinted handoffs isn't delivered if/when HintedHandOffManager ends up in invalid state. |  Major | . | Fredrik Larsson Stigbäck | Sylvain Lebresne |
| [CASSANDRA-3547](https://issues.apache.org/jira/browse/CASSANDRA-3547) | Race between cf flush and  its secondary indexes flush |  Major | Secondary Indexes | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3548](https://issues.apache.org/jira/browse/CASSANDRA-3548) | NPE in AntiEntropyService$RepairSession.completed() |  Minor | . | amorton | Sylvain Lebresne |
| [CASSANDRA-3553](https://issues.apache.org/jira/browse/CASSANDRA-3553) | value validator in the cli does not pick up |  Minor | Tools | Jackson Chung | Pavel Yaskevich |
| [CASSANDRA-3536](https://issues.apache.org/jira/browse/CASSANDRA-3536) | Assertion error during bootstraping cassandra |  Major | . | Ramesh Natarajan | Jonathan Ellis |
| [CASSANDRA-3556](https://issues.apache.org/jira/browse/CASSANDRA-3556) | nodetool info reports inaccurate datacenter/rack for localhost |  Minor | Tools | Rick Branson | Rick Branson |
| [CASSANDRA-3532](https://issues.apache.org/jira/browse/CASSANDRA-3532) | Compaction cleanupIfNecessary costly when many files in data dir |  Major | . | Eric Parusel | Eric Parusel |
| [CASSANDRA-3565](https://issues.apache.org/jira/browse/CASSANDRA-3565) | CQL CF creation skips most of the validation code |  Minor | CQL | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3573](https://issues.apache.org/jira/browse/CASSANDRA-3573) | When Snappy compression is not available on the platform, trying to enable it introduces problems |  Minor | . | Vitalii Tymchyshyn | Pavel Yaskevich |
| [CASSANDRA-3558](https://issues.apache.org/jira/browse/CASSANDRA-3558) | Compression chunk\_length\_kb is not correctly returned for thrift/avro |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3422](https://issues.apache.org/jira/browse/CASSANDRA-3422) | Can create a Column Family with comparator CounterColumnType which is subsequently unusable |  Minor | . | Kelley Reynolds | Sylvain Lebresne |
| [CASSANDRA-3551](https://issues.apache.org/jira/browse/CASSANDRA-3551) | Timeout exception for quorum reads after upgrade from 1.0.2 to 1.0.5 |  Critical | . | Zhong Li | Sylvain Lebresne |
| [CASSANDRA-3584](https://issues.apache.org/jira/browse/CASSANDRA-3584) | Check for 0.0.0.0 is incorrect |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3540](https://issues.apache.org/jira/browse/CASSANDRA-3540) | Wrong check of partitioner for secondary indexes |  Critical | Secondary Indexes | Sylvain Lebresne | Yuki Morishita |
| [CASSANDRA-3588](https://issues.apache.org/jira/browse/CASSANDRA-3588) | cqlsh: HELP for DELETE\_USING and DELETE\_COLUMNS broken |  Trivial | Tools | paul cannon | paul cannon |
| [CASSANDRA-2189](https://issues.apache.org/jira/browse/CASSANDRA-2189) | json2sstable fails due to OutOfMemory |  Minor | Tools | Shotaro Kamio | Jonathan Ellis |
| [CASSANDRA-3577](https://issues.apache.org/jira/browse/CASSANDRA-3577) | TimeoutException When using QuorumEach or ALL consistency on Multi-DC |  Major | . | Vijay | Vijay |
| [CASSANDRA-3563](https://issues.apache.org/jira/browse/CASSANDRA-3563) | Packaging should increase vm.max\_map\_count to accommodate leveled compaction |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3596](https://issues.apache.org/jira/browse/CASSANDRA-3596) | cqlsh: DESCRIBE output for a columnfamily does not work as input to same C\* instance |  Minor | Tools | paul cannon | paul cannon |
| [CASSANDRA-3601](https://issues.apache.org/jira/browse/CASSANDRA-3601) | get\_count NullPointerException with counters |  Major | . | Greg Hinkle |  |
| [CASSANDRA-3598](https://issues.apache.org/jira/browse/CASSANDRA-3598) | Index Scan's will span across multiple DC's |  Minor | . | Vijay | Vijay |
| [CASSANDRA-3485](https://issues.apache.org/jira/browse/CASSANDRA-3485) | Gossiper.addSavedEndpoint should never add itself |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3580](https://issues.apache.org/jira/browse/CASSANDRA-3580) | Don't assume the Table instance has been open when dropping a keyspace |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |


