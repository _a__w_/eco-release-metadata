
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.0.1 - 2011-10-31



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3140](https://issues.apache.org/jira/browse/CASSANDRA-3140) | Expose server, api versions to CQL |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3130](https://issues.apache.org/jira/browse/CASSANDRA-3130) | CQL queries should alow talbe names to be qualified by keyspace |  Minor | . | Edward Capriolo | Pavel Yaskevich |
| [CASSANDRA-2802](https://issues.apache.org/jira/browse/CASSANDRA-2802) | Enable encryption for data across the DC only. |  Minor | . | Vijay | Vijay |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2882](https://issues.apache.org/jira/browse/CASSANDRA-2882) | describe\_ring should include datacenter/topology information |  Minor | CQL | Mark Guzman | Mark Guzman |
| [CASSANDRA-3261](https://issues.apache.org/jira/browse/CASSANDRA-3261) | Thrift sockets are not buffered properly |  Major | . | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-3286](https://issues.apache.org/jira/browse/CASSANDRA-3286) | Performance issue in ByteBufferUtil |  Minor | . | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-3330](https://issues.apache.org/jira/browse/CASSANDRA-3330) | Improve CompactionTask extensibility |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2961](https://issues.apache.org/jira/browse/CASSANDRA-2961) | Expire dead gossip states based on time |  Minor | . | Brandon Williams | Jérémy Sevellec |
| [CASSANDRA-3333](https://issues.apache.org/jira/browse/CASSANDRA-3333) | remove more copies from read/write network path |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3124](https://issues.apache.org/jira/browse/CASSANDRA-3124) | java heap limit for nodetool |  Minor | Tools | Zenek Kraweznik | Zenek Kraweznik |
| [CASSANDRA-3185](https://issues.apache.org/jira/browse/CASSANDRA-3185) | Unify support across different map/reduce related classes for comma seperated list of hosts for initial thrift port connection. |  Minor | . | Eldon Stegall | Eldon Stegall |
| [CASSANDRA-3361](https://issues.apache.org/jira/browse/CASSANDRA-3361) | add logging to StorageProxy for mutations and CL |  Minor | . | Jackson Chung | Jackson Chung |
| [CASSANDRA-3280](https://issues.apache.org/jira/browse/CASSANDRA-3280) | Pig Storage Handler: Add \>=0.8.1 types, Guess right type for Key in Schema |  Major | . | Steeve Morin |  |
| [CASSANDRA-3366](https://issues.apache.org/jira/browse/CASSANDRA-3366) | CQL: Internal application error specifying 'using consistency ...' in lower case; must be upper case |  Trivial | CQL | Cathy Daw | Pavel Yaskevich |
| [CASSANDRA-3365](https://issues.apache.org/jira/browse/CASSANDRA-3365) | [patch] don't stutter exception messages |  Trivial | . | Dave Brosius |  |
| [CASSANDRA-3359](https://issues.apache.org/jira/browse/CASSANDRA-3359) | snapshot should include manifest under leveledcompaction |  Major | . | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-3170](https://issues.apache.org/jira/browse/CASSANDRA-3170) | Schema versions output should be on separate lines for separate versions |  Minor | . | Jeremy Hanna | Pavel Yaskevich |
| [CASSANDRA-3386](https://issues.apache.org/jira/browse/CASSANDRA-3386) | Avoid lock contention in hint rows |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3363](https://issues.apache.org/jira/browse/CASSANDRA-3363) | Allow one leveled compaction task to kick off another |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3372](https://issues.apache.org/jira/browse/CASSANDRA-3372) | Make HSHA cached threads. |  Minor | . | Vijay | Vijay |
| [CASSANDRA-2997](https://issues.apache.org/jira/browse/CASSANDRA-2997) | Enhance human-readability of snapshot names created by drop column family |  Minor | Tools | Eric Gilmore | Yuki Morishita |
| [CASSANDRA-3396](https://issues.apache.org/jira/browse/CASSANDRA-3396) | [patch] push down assignments to scopes where they are needed |  Trivial | Documentation and Website | Dave Brosius | Dave Brosius |
| [CASSANDRA-3214](https://issues.apache.org/jira/browse/CASSANDRA-3214) | Make CFIF use rpc\_endpoint prior to trying endpoint |  Minor | . | Eldon Stegall | Eldon Stegall |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3275](https://issues.apache.org/jira/browse/CASSANDRA-3275) | Make Cassandra compile under JDK 7 |  Major | . | Pavel Yaskevich | satish babu krishnamoorthy |
| [CASSANDRA-3299](https://issues.apache.org/jira/browse/CASSANDRA-3299) | clientutil depends on FBUtilities (bad) |  Major | CQL | Eric Evans | Eric Evans |
| [CASSANDRA-3349](https://issues.apache.org/jira/browse/CASSANDRA-3349) | NPE on malformed CQL |  Major | . | paul cannon | Pavel Yaskevich |
| [CASSANDRA-3350](https://issues.apache.org/jira/browse/CASSANDRA-3350) | Can't USE numeric keyspace names in CQL |  Minor | . | paul cannon | Pavel Yaskevich |
| [CASSANDRA-3358](https://issues.apache.org/jira/browse/CASSANDRA-3358) | 2GB row size limit in ColumnIndex offset calculation |  Major | . | Thomas Richter | Thomas Richter |
| [CASSANDRA-3357](https://issues.apache.org/jira/browse/CASSANDRA-3357) | SSTableImport/Export don't handle tombstone well if value validation != BytesType |  Trivial | Tools | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3353](https://issues.apache.org/jira/browse/CASSANDRA-3353) | misc CQL doc fixes |  Minor | Documentation and Website | Eric Evans | Eric Evans |
| [CASSANDRA-3344](https://issues.apache.org/jira/browse/CASSANDRA-3344) | Compaction throttling can be too slow |  Minor | . | Fabien Rousseau | Sylvain Lebresne |
| [CASSANDRA-3342](https://issues.apache.org/jira/browse/CASSANDRA-3342) | cassandra-cli allows setting min\_compaction\_threshold to 1 |  Major | Tools | Jason Baker | Pavel Yaskevich |
| [CASSANDRA-3126](https://issues.apache.org/jira/browse/CASSANDRA-3126) | unable to remove column metadata via CLI |  Minor | Tools | Radim Kolar | Pavel Yaskevich |
| [CASSANDRA-3368](https://issues.apache.org/jira/browse/CASSANDRA-3368) | 'show schema' in cli does not show compression\_options |  Trivial | Tools | Christian Spriegel | Pavel Yaskevich |
| [CASSANDRA-3364](https://issues.apache.org/jira/browse/CASSANDRA-3364) | [patch] use long math, if you want long results |  Trivial | . | Dave Brosius |  |
| [CASSANDRA-3373](https://issues.apache.org/jira/browse/CASSANDRA-3373) | ReadResponseSerializer doesn't compute serialized size correctly |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3375](https://issues.apache.org/jira/browse/CASSANDRA-3375) | Avoid clock drift on some Windows machines |  Minor | . | Flavio Baronti |  |
| [CASSANDRA-3292](https://issues.apache.org/jira/browse/CASSANDRA-3292) | creating column family sets durable\_writes to true |  Minor | . | Radim Kolar | Jonathan Ellis |
| [CASSANDRA-3314](https://issues.apache.org/jira/browse/CASSANDRA-3314) | Fail to delete -Index files if index is currently building |  Minor | . | Radim Kolar | Jonathan Ellis |
| [CASSANDRA-3377](https://issues.apache.org/jira/browse/CASSANDRA-3377) | correct dropped messages logging |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3384](https://issues.apache.org/jira/browse/CASSANDRA-3384) | it would be nice if "describe keyspace" in cli shows "Cache Provider" |  Minor | Tools | Vijay | Pavel Yaskevich |
| [CASSANDRA-3370](https://issues.apache.org/jira/browse/CASSANDRA-3370) | Deflate Compression corrupts SSTables |  Major | . | Christian Spriegel | Sylvain Lebresne |
| [CASSANDRA-3387](https://issues.apache.org/jira/browse/CASSANDRA-3387) | unnecessary locking in hint path due to InetAddress.getLocalhost() |  Minor | . | Yang Yang | Yang Yang |
| [CASSANDRA-3334](https://issues.apache.org/jira/browse/CASSANDRA-3334) | dropping index causes some inflight mutations to fail |  Minor | . | Radim Kolar | Jonathan Ellis |
| [CASSANDRA-3313](https://issues.apache.org/jira/browse/CASSANDRA-3313) | Cancelling index build throws assert error |  Minor | . | Radim Kolar | Jonathan Ellis |
| [CASSANDRA-3369](https://issues.apache.org/jira/browse/CASSANDRA-3369) | AssertionError when adding a node and doing repair, repair hangs |  Major | . | Christian Spriegel | Sylvain Lebresne |
| [CASSANDRA-3391](https://issues.apache.org/jira/browse/CASSANDRA-3391) | CFM.toAvro() incorrectly serialises key\_validation\_class defn |  Minor | . | amorton | amorton |
| [CASSANDRA-3394](https://issues.apache.org/jira/browse/CASSANDRA-3394) | AssertionError in PrecompactedRow.write via CommutativeRowIndexer during bootstrap |  Major | . | paul cannon | Sylvain Lebresne |
| [CASSANDRA-3351](https://issues.apache.org/jira/browse/CASSANDRA-3351) | If node fails to join a ring it will stay in joining state indefinately |  Trivial | . | Tuukka Luolamo | Brandon Williams |
| [CASSANDRA-3390](https://issues.apache.org/jira/browse/CASSANDRA-3390) | ReadResponseSerializer.serializedSize() calculation is wrong |  Major | . | Yang Yang | Jonathan Ellis |
| [CASSANDRA-3381](https://issues.apache.org/jira/browse/CASSANDRA-3381) | StorageProxy does not log correctly when schema is not in agreement |  Minor | . | Jackson Chung | Tommy Tynjä |
| [CASSANDRA-3400](https://issues.apache.org/jira/browse/CASSANDRA-3400) | ConcurrentModificationException during nodetool repair |  Minor | . | Scott Fines | Sylvain Lebresne |
| [CASSANDRA-2466](https://issues.apache.org/jira/browse/CASSANDRA-2466) | bloom filters should avoid huge array allocations to avoid fragmentation concerns |  Minor | . | Peter Schuller | Oleg Anastasyev |
| [CASSANDRA-3403](https://issues.apache.org/jira/browse/CASSANDRA-3403) | describe\_ring topology information is wrong/incomplete |  Major | . | Brandon Williams | Patricio Echague |
| [CASSANDRA-3302](https://issues.apache.org/jira/browse/CASSANDRA-3302) | stop Cassandra result in hang |  Major | . | Jackson Chung | Sylvain Lebresne |
| [CASSANDRA-3409](https://issues.apache.org/jira/browse/CASSANDRA-3409) | CFS reloading of the compaction strategy is done for every metadata update and is not thread safe |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3408](https://issues.apache.org/jira/browse/CASSANDRA-3408) | LeveledCompactionTask is too fragile and can block compactions |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3385](https://issues.apache.org/jira/browse/CASSANDRA-3385) | NPE in hinted handoff |  Minor | . | Yang Yang | Jonathan Ellis |
| [CASSANDRA-3331](https://issues.apache.org/jira/browse/CASSANDRA-3331) | Apache Daemon missing from the binary tarball |  Major | . | Benjamin Coverston | Benjamin Coverston |
| [CASSANDRA-2854](https://issues.apache.org/jira/browse/CASSANDRA-2854) | Java Build Path is broken in Eclipse after running generate-eclipse-files Ant target |  Minor | Packaging | David Allsopp | David Allsopp |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3196](https://issues.apache.org/jira/browse/CASSANDRA-3196) | Cassandra-CLI command should have --version option |  Minor | Tools | Mauri Tikka | Pavel Yaskevich |


