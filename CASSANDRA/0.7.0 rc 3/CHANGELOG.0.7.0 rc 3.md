
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.7.0 rc 3 - 2010-12-24



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1763](https://issues.apache.org/jira/browse/CASSANDRA-1763) | NodeCmd should be able to view Compaction Statistics |  Minor | . | Edward Capriolo | Edward Capriolo |
| [CASSANDRA-1812](https://issues.apache.org/jira/browse/CASSANDRA-1812) | Allow per-CF compaction, repair, and cleanup |  Minor | . | Tyler Hobbs | Jon Hermes |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1814](https://issues.apache.org/jira/browse/CASSANDRA-1814) | validation is inefficient |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-1811](https://issues.apache.org/jira/browse/CASSANDRA-1811) | Cleanup smallest CFs first |  Minor | . | Paul Querna | Jon Hermes |
| [CASSANDRA-1838](https://issues.apache.org/jira/browse/CASSANDRA-1838) | Add ability to set TTL on columns in cassandra-cli |  Minor | Tools | Eric Tamme | Pavel Yaskevich |
| [CASSANDRA-1689](https://issues.apache.org/jira/browse/CASSANDRA-1689) | avoid nuking system classpath |  Minor | Packaging | Jonathan Ellis | Nick Bailey |
| [CASSANDRA-1845](https://issues.apache.org/jira/browse/CASSANDRA-1845) | [patch] help out gc, by making inner classes static where possible |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-1844](https://issues.apache.org/jira/browse/CASSANDRA-1844) | [patch] equal objects should have equal hashcodes |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-1863](https://issues.apache.org/jira/browse/CASSANDRA-1863) | [patch] remove redundant unused code |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-1864](https://issues.apache.org/jira/browse/CASSANDRA-1864) | [patch] fix wasteful use of boxing when not needed |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-1867](https://issues.apache.org/jira/browse/CASSANDRA-1867) | sstable2json runs out of memory when trying to export huge rows |  Minor | Tools | Ilya Maykov | Ilya Maykov |
| [CASSANDRA-1874](https://issues.apache.org/jira/browse/CASSANDRA-1874) | stress.py needs an option to specify a file containing a list of cassandra nodes |  Minor | . | Matthew F. Dennis | Matthew F. Dennis |
| [CASSANDRA-1879](https://issues.apache.org/jira/browse/CASSANDRA-1879) | [patch] fix logging contexts |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-1884](https://issues.apache.org/jira/browse/CASSANDRA-1884) | sstable2json / sstablekeys should verify key order in -Data and -Index files |  Minor | Tools | Karl Mueller | Karl Mueller |
| [CASSANDRA-1878](https://issues.apache.org/jira/browse/CASSANDRA-1878) | Minimize Key Cache Invalidation by Compaction |  Major | . | Tyler Hobbs | Jonathan Ellis |
| [CASSANDRA-1880](https://issues.apache.org/jira/browse/CASSANDRA-1880) | make stress.py trigger python issue 3770 during import |  Minor | . | Peter Schuller | Peter Schuller |
| [CASSANDRA-1870](https://issues.apache.org/jira/browse/CASSANDRA-1870) | Speed up release target |  Minor | Tools | Stu Hood | Eric Evans |
| [CASSANDRA-1890](https://issues.apache.org/jira/browse/CASSANDRA-1890) | Updates and tweaks to spec file for easier support of candidate and beta releases |  Major | . | Nate McCall | Nate McCall |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1832](https://issues.apache.org/jira/browse/CASSANDRA-1832) | mx4j does not load |  Minor | Tools | Rustam Aliyev | Ran Tavory |
| [CASSANDRA-1829](https://issues.apache.org/jira/browse/CASSANDRA-1829) | Nodetool move is broken |  Blocker | . | Nick Bailey | Nick Bailey |
| [CASSANDRA-1835](https://issues.apache.org/jira/browse/CASSANDRA-1835) | "null" error creating CF from cli |  Minor | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1843](https://issues.apache.org/jira/browse/CASSANDRA-1843) | Indexes: CF MBeans for automatic indexes are never unregistered when they are deleted. |  Minor | . | Jon Hermes | Jon Hermes |
| [CASSANDRA-1783](https://issues.apache.org/jira/browse/CASSANDRA-1783) | spurious failures of o.a.c.db.NameSortTest:testNameSort100 |  Trivial | . | Eric Evans | Jonathan Ellis |
| [CASSANDRA-1834](https://issues.apache.org/jira/browse/CASSANDRA-1834) | hudson test failure: "Forked Java VM exited abnormally." |  Major | . | Eric Evans | Jonathan Ellis |
| [CASSANDRA-1842](https://issues.apache.org/jira/browse/CASSANDRA-1842) | ColumnFamilyOutputFormat only writes the first column |  Major | . | Brandon Williams | Jeremy Hanna |
| [CASSANDRA-1847](https://issues.apache.org/jira/browse/CASSANDRA-1847) | ByteBufferUtil.clone shouldn't mutate the passed bytebuffer |  Minor | . | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-1850](https://issues.apache.org/jira/browse/CASSANDRA-1850) | When the ANTLR code generation fails the build continues and ignores the failure |  Trivial | . | Stephen Connolly | Stephen Connolly |
| [CASSANDRA-1824](https://issues.apache.org/jira/browse/CASSANDRA-1824) | Schema only fully propagates from seeds |  Major | . | Brandon Williams | Gary Dusbabek |
| [CASSANDRA-1841](https://issues.apache.org/jira/browse/CASSANDRA-1841) | cassandra-cli formatted help width |  Trivial | Tools | Eric Evans | Eric Evans |
| [CASSANDRA-1837](https://issues.apache.org/jira/browse/CASSANDRA-1837) | Deleted columns are resurrected after a flush |  Blocker | . | Brandon Williams | Gary Dusbabek |
| [CASSANDRA-1830](https://issues.apache.org/jira/browse/CASSANDRA-1830) | ReadResponseResolver might miss an inconsistency |  Minor | . | Jonathan Ellis | Randall Leeds |
| [CASSANDRA-1862](https://issues.apache.org/jira/browse/CASSANDRA-1862) | StorageProxy throws an InvalidRequestException in readProtocol during bootstrap |  Minor | . | Nate McCall | Nate McCall |
| [CASSANDRA-1748](https://issues.apache.org/jira/browse/CASSANDRA-1748) | Flush before repair |  Major | . | Stu Hood | Tyler Hobbs |
| [CASSANDRA-1866](https://issues.apache.org/jira/browse/CASSANDRA-1866) | Internal error from malformed remove with supercolumns |  Minor | CQL | Jonathan Ellis | Tyler Hobbs |
| [CASSANDRA-1873](https://issues.apache.org/jira/browse/CASSANDRA-1873) | Read Repair behavior thwarts DynamicEndpointSnitch at CL.ONE |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1883](https://issues.apache.org/jira/browse/CASSANDRA-1883) | NPE in get\_slice quorum read |  Minor | . | Karl Mueller | Jonathan Ellis |
| [CASSANDRA-1853](https://issues.apache.org/jira/browse/CASSANDRA-1853) | changing row cache save interval is reflected in 'describe keyspace' on node it was submitted to, but not nodes it was propagated to |  Minor | . | Peter Schuller | Gary Dusbabek |
| [CASSANDRA-1833](https://issues.apache.org/jira/browse/CASSANDRA-1833) | clustertool get\_endpoints needs key as String, not ByteBuffer |  Trivial | Tools | Kelvin Kakugawa | Kelvin Kakugawa |


