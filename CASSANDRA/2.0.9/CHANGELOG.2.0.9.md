
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.0.9 - 2014-06-30



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6344](https://issues.apache.org/jira/browse/CASSANDRA-6344) | When running CQLSH with file input, exit with error status code if script fails |  Major | Tools | Branden Visser | Mikhail Stepura |
| [CASSANDRA-7172](https://issues.apache.org/jira/browse/CASSANDRA-7172) | cqlsh: Accept and execute CQL statement(s) from command-line parameter |  Trivial | Tools | PJ | Mikhail Stepura |
| [CASSANDRA-7264](https://issues.apache.org/jira/browse/CASSANDRA-7264) | Add conditional support for the creation/deletion of users |  Minor | . | Hamilton Tran | Aleksey Yeschenko |
| [CASSANDRA-7273](https://issues.apache.org/jira/browse/CASSANDRA-7273) | expose global ColumnFamily metrics |  Minor | . | Richard Wagner | Chris Lohfink |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7063](https://issues.apache.org/jira/browse/CASSANDRA-7063) | Raise the streaming phi threshold check in 1.2 |  Minor | . | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-7137](https://issues.apache.org/jira/browse/CASSANDRA-7137) | Check for Java \>= 1.7 in bin/cassandra |  Minor | . | Tyler Hobbs | Brandon Williams |
| [CASSANDRA-7191](https://issues.apache.org/jira/browse/CASSANDRA-7191) | reduce needless garbage/thrashing in pending range calculation |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-7133](https://issues.apache.org/jira/browse/CASSANDRA-7133) | yield in SlabAllocator$Region.allocate could cause starvation |  Minor | . | Jeremiah Jordan | Benedict |
| [CASSANDRA-7210](https://issues.apache.org/jira/browse/CASSANDRA-7210) | Add --resolve-ip option on 'nodetool ring' |  Trivial | Tools | Yasuharu Goto | Yasuharu Goto |
| [CASSANDRA-7244](https://issues.apache.org/jira/browse/CASSANDRA-7244) | Don't allocate a Codec.Flag enum value array on every read |  Minor | . | Benedict | Benedict |
| [CASSANDRA-7253](https://issues.apache.org/jira/browse/CASSANDRA-7253) | Improve javadoc for KeysIndex |  Trivial | . | Peter | Peter |
| [CASSANDRA-7274](https://issues.apache.org/jira/browse/CASSANDRA-7274) | Better display table organization on desc table via primary key list |  Trivial | . | G Gordon Worley III | Patrick McFadin |
| [CASSANDRA-6484](https://issues.apache.org/jira/browse/CASSANDRA-6484) | cassandra-shuffle not working with authentication |  Minor | Tools | Gibheer | Aleksey Yeschenko |
| [CASSANDRA-7320](https://issues.apache.org/jira/browse/CASSANDRA-7320) | Swap local and global default read repair chances |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-7333](https://issues.apache.org/jira/browse/CASSANDRA-7333) | gossipinfo should include the generation |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-6539](https://issues.apache.org/jira/browse/CASSANDRA-6539) | Track metrics at a keyspace level as well as column family level |  Minor | . | Nick Bailey | Brandon Williams |
| [CASSANDRA-7236](https://issues.apache.org/jira/browse/CASSANDRA-7236) | Extend connection backlog for MessageService |  Major | . | Oleg Anastasyev | Oleg Anastasyev |
| [CASSANDRA-7266](https://issues.apache.org/jira/browse/CASSANDRA-7266) | Allow cqlsh shell ignore .cassandra permission errors and not fail to open the cqlsh shell. |  Minor | Tools | Carolyn Jung |  |
| [CASSANDRA-7356](https://issues.apache.org/jira/browse/CASSANDRA-7356) | Add a more ops friendly replace\_address flag |  Major | . | Richard Low | Brandon Williams |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7122](https://issues.apache.org/jira/browse/CASSANDRA-7122) | Replacement nodes have null entries in system.peers |  Major | . | Richard Low | Brandon Williams |
| [CASSANDRA-6562](https://issues.apache.org/jira/browse/CASSANDRA-6562) | sstablemetadata{.bat} location inconsistency |  Trivial | Tools | Chris Burroughs | Brandon Williams |
| [CASSANDRA-7183](https://issues.apache.org/jira/browse/CASSANDRA-7183) | BackgroundActivityMonitor.readAndCompute only returns half of the values |  Minor | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-6162](https://issues.apache.org/jira/browse/CASSANDRA-6162) | Cassandra does not start on Ubuntu 13.04 RUssian |  Major | Packaging | Sergey Nagaytsev | Dave Brosius |
| [CASSANDRA-7088](https://issues.apache.org/jira/browse/CASSANDRA-7088) |  Zero length BigInteger exception causing processing to freeze |  Major | . | Jacek Furmankiewicz | Sylvain Lebresne |
| [CASSANDRA-7185](https://issues.apache.org/jira/browse/CASSANDRA-7185) | cqlsh can't tab-complete disabling compaction |  Trivial | . | Brandon Williams | Mikhail Stepura |
| [CASSANDRA-7092](https://issues.apache.org/jira/browse/CASSANDRA-7092) | Visibility issue in StreamSession |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-7198](https://issues.apache.org/jira/browse/CASSANDRA-7198) | CqlPagingRecordReader throws IllegalStateException |  Trivial | . | Brent Theisen | Brent Theisen |
| [CASSANDRA-7211](https://issues.apache.org/jira/browse/CASSANDRA-7211) | Triggers Demo seems incompatible with row markers |  Trivial | . | Anthony Cozzie | Anthony Cozzie |
| [CASSANDRA-6877](https://issues.apache.org/jira/browse/CASSANDRA-6877) | pig tests broken |  Major | . | Brandon Williams | Sam Tunnicliffe |
| [CASSANDRA-6973](https://issues.apache.org/jira/browse/CASSANDRA-6973) | timestamp data type does ISO 8601 formats with 'Z' as time zone. |  Trivial | . | Juho Mäkinen | Chander Pechetty |
| [CASSANDRA-7294](https://issues.apache.org/jira/browse/CASSANDRA-7294) | FileCache metrics incorrectly named |  Major | Tools | Nick Bailey | Nick Bailey |
| [CASSANDRA-7290](https://issues.apache.org/jira/browse/CASSANDRA-7290) | Compaction strategy is not reloaded when compaction strategy options is updated |  Major | Compaction | Paulo Motta | Paulo Motta |
| [CASSANDRA-6563](https://issues.apache.org/jira/browse/CASSANDRA-6563) | TTL histogram compactions not triggered at high "Estimated droppable tombstones" rate |  Major | Compaction | Chris Burroughs | Paulo Motta |
| [CASSANDRA-7288](https://issues.apache.org/jira/browse/CASSANDRA-7288) | Exception during compaction |  Major | . | Julien Anguenot | Marcus Eriksson |
| [CASSANDRA-7315](https://issues.apache.org/jira/browse/CASSANDRA-7315) | Hadoop Compatibility Broken |  Major | . | Benjamin Coverston | Benjamin Coverston |
| [CASSANDRA-7319](https://issues.apache.org/jira/browse/CASSANDRA-7319) | Fix availability validation for LOCAL\_ONE CL |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6875](https://issues.apache.org/jira/browse/CASSANDRA-6875) | CQL3: select multiple CQL rows in a single partition using IN |  Minor | CQL | Nicolas Favre-Felix | Tyler Hobbs |
| [CASSANDRA-7219](https://issues.apache.org/jira/browse/CASSANDRA-7219) | Hint streaming can cause decommission to fail |  Major | . | Jeremiah Jordan | Jason Brown |
| [CASSANDRA-7325](https://issues.apache.org/jira/browse/CASSANDRA-7325) | Cqlsh counts non-empty lines for "Blank lines" warning |  Minor | . | Mikhail Stepura | Mikhail Stepura |
| [CASSANDRA-7262](https://issues.apache.org/jira/browse/CASSANDRA-7262) | During streaming: java.lang.AssertionError: Reference counter -1 |  Minor | Streaming and Messaging | Duncan Sands | Joshua McKenzie |
| [CASSANDRA-7330](https://issues.apache.org/jira/browse/CASSANDRA-7330) | Infinite loop in StreamReader.read during exception condition while running repair |  Minor | Streaming and Messaging | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-7144](https://issues.apache.org/jira/browse/CASSANDRA-7144) | Fix handling of empty counter replication mutations |  Major | . | Maxime Lamothe-Brassard | Aleksey Yeschenko |
| [CASSANDRA-7336](https://issues.apache.org/jira/browse/CASSANDRA-7336) | RepairTask doesn't send a correct message in a JMX notifcation in case of IllegalArgumentException |  Minor | . | Mikhail Stepura | Mikhail Stepura |
| [CASSANDRA-6523](https://issues.apache.org/jira/browse/CASSANDRA-6523) | "Unable to contact any seeds!" with multi-DC cluster and listen != broadcast address |  Minor | . | Chris Burroughs | Brandon Williams |
| [CASSANDRA-7337](https://issues.apache.org/jira/browse/CASSANDRA-7337) | Protocol batches wrongly ignores conditions |  Major | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-7234](https://issues.apache.org/jira/browse/CASSANDRA-7234) | Post-compaction cache preheating can result in FileNotFoundExceptions when tables are dropped |  Minor | . | Tyler Hobbs | Marcus Eriksson |
| [CASSANDRA-7380](https://issues.apache.org/jira/browse/CASSANDRA-7380) | Native protocol needs keepalive, we should add it |  Major | . | Jose Martinez Poblete | Brandon Williams |
| [CASSANDRA-6397](https://issues.apache.org/jira/browse/CASSANDRA-6397) | removenode outputs confusing non-error |  Trivial | Tools | Ryan McGuire | Kirk True |
| [CASSANDRA-7268](https://issues.apache.org/jira/browse/CASSANDRA-7268) | Secondary Index can miss data without an error |  Major | Secondary Indexes | Jeremiah Jordan | Sam Tunnicliffe |
| [CASSANDRA-7401](https://issues.apache.org/jira/browse/CASSANDRA-7401) | Memtable.maybeUpdateLiveRatio goes into an endless loop when currentOperations is zero |  Major | . | Christian Spriegel | Christian Spriegel |
| [CASSANDRA-7318](https://issues.apache.org/jira/browse/CASSANDRA-7318) | Unable to truncate column family on node which has been decommissioned and re-bootstrapped |  Minor | . | Thomas Whiteway | Brandon Williams |
| [CASSANDRA-6449](https://issues.apache.org/jira/browse/CASSANDRA-6449) | Tools error out if they can't make ~/.cassandra |  Major | Tools | Jeremiah Jordan | Kirk True |
| [CASSANDRA-7397](https://issues.apache.org/jira/browse/CASSANDRA-7397) | BatchlogManagerTest unit test failing |  Major | Testing | Michael Shuler | Carl Yeksigian |
| [CASSANDRA-7364](https://issues.apache.org/jira/browse/CASSANDRA-7364) | assert error in StorageProxy.submitHint |  Minor | . | Radim Kolar | Aleksey Yeschenko |
| [CASSANDRA-7307](https://issues.apache.org/jira/browse/CASSANDRA-7307) | New nodes mark dead nodes as up for 10 minutes |  Major | . | Richard Low | Brandon Williams |
| [CASSANDRA-7235](https://issues.apache.org/jira/browse/CASSANDRA-7235) | ColumnStats min/max column names does not account for range thombstones, making deleted data resurrect at random |  Major | . | Oleg Anastasyev | Oleg Anastasyev |
| [CASSANDRA-7317](https://issues.apache.org/jira/browse/CASSANDRA-7317) | Repair range validation is too strict |  Major | . | Nick Bailey | Yuki Morishita |
| [CASSANDRA-7407](https://issues.apache.org/jira/browse/CASSANDRA-7407) | COPY command does not work properly with collections causing failure to import data |  Major | . | Jose Martinez Poblete | Mikhail Stepura |
| [CASSANDRA-7373](https://issues.apache.org/jira/browse/CASSANDRA-7373) | Commit logs no longer deleting and MemtablePostFlusher pending growing |  Major | . | Francois Richard | Mikhail Stepura |
| [CASSANDRA-6766](https://issues.apache.org/jira/browse/CASSANDRA-6766) | allow now() -\> uuid compatibility |  Minor | CQL | Jonathan Ellis | Tyler Hobbs |
| [CASSANDRA-7305](https://issues.apache.org/jira/browse/CASSANDRA-7305) | CQL3, Static columns not returning rows if values are not set |  Major | . | Patrick Callaghan | Sylvain Lebresne |
| [CASSANDRA-7399](https://issues.apache.org/jira/browse/CASSANDRA-7399) | cqlsh: describe table shows wrong data type for CompositeType |  Major | Tools | Robert Stupp | Mikhail Stepura |
| [CASSANDRA-7394](https://issues.apache.org/jira/browse/CASSANDRA-7394) | Fix CollationController#collectTimeOrderedData() mostRecentRowTombstone tracking |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-7059](https://issues.apache.org/jira/browse/CASSANDRA-7059) | Range query with strict bound on clustering column can return less results than required for compact tables |  Major | . | Sylvain Lebresne | Sylvain Lebresne |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7067](https://issues.apache.org/jira/browse/CASSANDRA-7067) | Refuse CAS batch that have a 'USING TIMESTAMP' |  Minor | . | Mikhail Stepura | Sylvain Lebresne |
| [CASSANDRA-7335](https://issues.apache.org/jira/browse/CASSANDRA-7335) | GossipingPropertyFileSnitchTest fails on Windows |  Minor | Testing | Joshua McKenzie | Joshua McKenzie |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7181](https://issues.apache.org/jira/browse/CASSANDRA-7181) | Remove unused method isLocalTask() in o.a.c.repair.StreamingRepairTask |  Trivial | . | Lyuben Todorov | Lyuben Todorov |


