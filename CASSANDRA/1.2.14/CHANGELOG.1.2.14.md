
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.2.14 - 2014-02-03



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6286](https://issues.apache.org/jira/browse/CASSANDRA-6286) | Add JMX auth password file location info to cassandra-env.sh |  Trivial | Configuration | Peter Halliday | Peter Halliday |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6404](https://issues.apache.org/jira/browse/CASSANDRA-6404) | Tools emit ERRORs and WARNINGs about missing javaagent |  Minor | Tools | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-6453](https://issues.apache.org/jira/browse/CASSANDRA-6453) | Improve error message for invalid property values during parsing. |  Trivial | . | Brian ONeill |  |
| [CASSANDRA-6491](https://issues.apache.org/jira/browse/CASSANDRA-6491) | Timeout can send confusing information as to what their cause is |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2238](https://issues.apache.org/jira/browse/CASSANDRA-2238) | Allow nodetool to print out hostnames given an option |  Trivial | Tools | Joaquin Casares | Daneel S. Yaitskov |
| [CASSANDRA-6585](https://issues.apache.org/jira/browse/CASSANDRA-6585) | Make node tool exit code non zero when it fails to create snapshot |  Major | Tools | Vishy Kasar | Brandon Williams |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6190](https://issues.apache.org/jira/browse/CASSANDRA-6190) | Cassandra 2.0 won't start up with Java 7u40 with Client JVM.  (works on Server JVM, and both JVMs 7u25) |  Major | Configuration | Steven Lowenthal | Brandon Williams |
| [CASSANDRA-6427](https://issues.apache.org/jira/browse/CASSANDRA-6427) | Counter writes shouldn't be resubmitted after timeouts |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6420](https://issues.apache.org/jira/browse/CASSANDRA-6420) | CassandraStorage should not assume all DataBags are DefaultDataBags |  Major | . | Mike Spertus | Mike Spertus |
| [CASSANDRA-6471](https://issues.apache.org/jira/browse/CASSANDRA-6471) | Executing a prepared CREATE KEYSPACE multiple times doesn't work |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6510](https://issues.apache.org/jira/browse/CASSANDRA-6510) | Don't drop local mutations without a hint |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6419](https://issues.apache.org/jira/browse/CASSANDRA-6419) | Setting max\_hint\_window\_in\_ms explicitly to null causes problems with JMX view |  Minor | Configuration | Nate McCall | Nate McCall |
| [CASSANDRA-6521](https://issues.apache.org/jira/browse/CASSANDRA-6521) | Thrift should validate SliceRange start and finish lengths |  Major | . | Ben Bromhead | Ben Bromhead |
| [CASSANDRA-6531](https://issues.apache.org/jira/browse/CASSANDRA-6531) | Failure to start after unclean shutdown - java.lang.IllegalArgumentException: bufferSize must be positive |  Major | . | Nikolai Grigoriev | Jonathan Ellis |
| [CASSANDRA-6535](https://issues.apache.org/jira/browse/CASSANDRA-6535) | Prepared Statement on Defunct CF Can Impact Cluster Availability |  Minor | . | Adam Holmberg | Adam Holmberg |
| [CASSANDRA-6550](https://issues.apache.org/jira/browse/CASSANDRA-6550) | C\* should be able to throttle batchlog processing |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6545](https://issues.apache.org/jira/browse/CASSANDRA-6545) | LOCAL\_QUORUM still doesn't work with SimpleStrategy but don't throw a meaningful error message anymore |  Major | . | Sylvain Lebresne | Alex Liu |
| [CASSANDRA-6053](https://issues.apache.org/jira/browse/CASSANDRA-6053) | system.peers table not updated after decommissioning nodes in C\* 2.0 |  Major | . | Guyon Moree | Tyler Hobbs |
| [CASSANDRA-6567](https://issues.apache.org/jira/browse/CASSANDRA-6567) | StackOverflowError with big IN list |  Minor | . | Dmitry Shohov | Sylvain Lebresne |
| [CASSANDRA-6598](https://issues.apache.org/jira/browse/CASSANDRA-6598) | upgradesstables does not upgrade indexes causing startup error. |  Major | . | Ryan McGuire | Brandon Williams |
| [CASSANDRA-6255](https://issues.apache.org/jira/browse/CASSANDRA-6255) | Exception count not incremented on OutOfMemoryError (HSHA) |  Minor | . | Dan Hendry | Mikhail Stepura |
| [CASSANDRA-6569](https://issues.apache.org/jira/browse/CASSANDRA-6569) | Batchlog replays copy the entire batchlog table into the heap |  Major | . | Rick Branson | Aleksey Yeschenko |
| [CASSANDRA-6603](https://issues.apache.org/jira/browse/CASSANDRA-6603) | "hung" repair results in drain hanging |  Minor | . | Chris Burroughs | Jason Brown |
| [CASSANDRA-6604](https://issues.apache.org/jira/browse/CASSANDRA-6604) | AssertionError: Verb COUNTER\_MUTATION should not legally be dropped |  Major | . | Bartłomiej Romański |  |
| [CASSANDRA-4375](https://issues.apache.org/jira/browse/CASSANDRA-4375) | FD incorrectly using RPC timeout to ignore gossip heartbeats |  Major | . | Peter Schuller | Brandon Williams |
| [CASSANDRA-6262](https://issues.apache.org/jira/browse/CASSANDRA-6262) | Nodetool compact throws an error after importing data with sstableloader |  Major | . | J.B. Langston | Sylvain Lebresne |
| [CASSANDRA-6564](https://issues.apache.org/jira/browse/CASSANDRA-6564) | Gossiper failed with ArrayIndexOutOfBoundsException |  Minor | . | Shao-Chuan Wang | Tyler Hobbs |
| [CASSANDRA-6607](https://issues.apache.org/jira/browse/CASSANDRA-6607) | Unable to prepare statement with batch and delete from collection |  Minor | . | Jan Chochol | Sylvain Lebresne |
| [CASSANDRA-6629](https://issues.apache.org/jira/browse/CASSANDRA-6629) | Coordinator's "java.lang.ArrayIndexOutOfBoundsException: -1" with CL \> 1 |  Major | CQL | Roman Skvazh | Mikhail Stepura |
| [CASSANDRA-6619](https://issues.apache.org/jira/browse/CASSANDRA-6619) | Race condition issue during upgrading 1.1 to 1.2 |  Minor | . | Minh Do | Minh Do |
| [CASSANDRA-6615](https://issues.apache.org/jira/browse/CASSANDRA-6615) | Changing the IP of a node on a live cluster leaves gossip infos and throws Exceptions |  Major | . | Fabien Rousseau | Brandon Williams |
| [CASSANDRA-6503](https://issues.apache.org/jira/browse/CASSANDRA-6503) | sstables from stalled repair sessions become live after a reboot and can resurrect deleted data |  Minor | . | Jeremiah Jordan | Jason Brown |
| [CASSANDRA-6592](https://issues.apache.org/jira/browse/CASSANDRA-6592) | IllegalArgumentException when Preparing Statements |  Critical | . | Tyler Hobbs | Sylvain Lebresne |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4899](https://issues.apache.org/jira/browse/CASSANDRA-4899) | add gitignore |  Trivial | . | Radim Kolar | Michael Shuler |


