
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.7.0 - 2011-01-09



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1885](https://issues.apache.org/jira/browse/CASSANDRA-1885) | Clean up ReadResponse |  Trivial | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1871](https://issues.apache.org/jira/browse/CASSANDRA-1871) | Expose index-building status in JMX + cli schema description |  Minor | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1370](https://issues.apache.org/jira/browse/CASSANDRA-1370) | TokenMetaData.getPendingRangesMM() is unnecessarily synchronized |  Minor | . | Jason Fager | Brandon Williams |
| [CASSANDRA-1369](https://issues.apache.org/jira/browse/CASSANDRA-1369) | FBUtilities.hash can result in thread contention on call to MessageDigest.getInstance() |  Minor | . | Jason Fager | Brandon Williams |
| [CASSANDRA-1914](https://issues.apache.org/jira/browse/CASSANDRA-1914) | No need to preserve tombstones in 2ary index ColumnFamilyStores |  Minor | . | Jonathan Ellis | Jonathan Ellis |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1875](https://issues.apache.org/jira/browse/CASSANDRA-1875) | backgrounding cli makes it crash |  Trivial | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1893](https://issues.apache.org/jira/browse/CASSANDRA-1893) | Timed out reads not counted in metrics |  Minor | . | Stu Hood | Stu Hood |
| [CASSANDRA-1899](https://issues.apache.org/jira/browse/CASSANDRA-1899) | Failed to get columns from super column in cassandra-cli (0.7-rc2) |  Minor | Tools | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-1892](https://issues.apache.org/jira/browse/CASSANDRA-1892) | heisenbug in SSTableExportTest |  Minor | . | Jonathan Ellis | T Jake Luciani |
| [CASSANDRA-1766](https://issues.apache.org/jira/browse/CASSANDRA-1766) | Streaming never makes progress |  Major | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-1905](https://issues.apache.org/jira/browse/CASSANDRA-1905) | count timeouts towards dynamicsnitch latencies |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1917](https://issues.apache.org/jira/browse/CASSANDRA-1917) | Insert performance regression |  Critical | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-1910](https://issues.apache.org/jira/browse/CASSANDRA-1910) | validation of time uuid is incorrect |  Minor | . | Dave | Gary Dusbabek |
| [CASSANDRA-1901](https://issues.apache.org/jira/browse/CASSANDRA-1901) | getRestrictedRanges bug where node owns minimum token |  Major | . | Jonathan Ellis | Stu Hood |
| [CASSANDRA-1904](https://issues.apache.org/jira/browse/CASSANDRA-1904) | Crash during startup: SSTable doesn't handle corrupt (empty) tmp files |  Major | . | Timo Nentwig | Gary Dusbabek |
| [CASSANDRA-1922](https://issues.apache.org/jira/browse/CASSANDRA-1922) | exceptions after cleanup |  Major | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-1939](https://issues.apache.org/jira/browse/CASSANDRA-1939) | Misuses of ByteBuffer absolute get (wrongfully adding arrayOffset to the index) |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |


