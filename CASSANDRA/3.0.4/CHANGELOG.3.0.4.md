
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.0.4 - 2016-03-08



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10622](https://issues.apache.org/jira/browse/CASSANDRA-10622) | Add all options in cqlshrc sample as commented out choices |  Minor | Documentation and Website | Lorina Poland | Tyler Hobbs |
| [CASSANDRA-11041](https://issues.apache.org/jira/browse/CASSANDRA-11041) | Make it clear what timestamp\_resolution is used for with DTCS |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-10397](https://issues.apache.org/jira/browse/CASSANDRA-10397) | Add local timezone support to cqlsh |  Minor | . | Suleman Rai | Stefan Podkowinski |
| [CASSANDRA-11124](https://issues.apache.org/jira/browse/CASSANDRA-11124) | Change default cqlsh encoding to utf-8 |  Trivial | Tools | Paulo Motta | Paulo Motta |
| [CASSANDRA-7464](https://issues.apache.org/jira/browse/CASSANDRA-7464) | Replace sstable2json |  Minor | . | Sylvain Lebresne | Chris Lohfink |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11001](https://issues.apache.org/jira/browse/CASSANDRA-11001) | Hadoop integration is incompatible with Cassandra Driver 3.0.0 |  Major | . | Jacek Lewandowski | Robert Stupp |
| [CASSANDRA-11116](https://issues.apache.org/jira/browse/CASSANDRA-11116) | Gossiper#isEnabled is not thread safe |  Critical | Core | Sergio Bossa | Ariel Weisberg |
| [CASSANDRA-11113](https://issues.apache.org/jira/browse/CASSANDRA-11113) | DateTieredCompactionStrategy.getMaximalTask compacts repaired and unrepaired sstables together |  Major | . | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-11030](https://issues.apache.org/jira/browse/CASSANDRA-11030) | utf-8 characters incorrectly displayed/inserted on cqlsh on Windows |  Minor | . | Paulo Motta | Paulo Motta |
| [CASSANDRA-11120](https://issues.apache.org/jira/browse/CASSANDRA-11120) | Leak Detected while bringing nodes up and down when under stress. |  Minor | Core | Jeremiah Jordan | Ariel Weisberg |
| [CASSANDRA-11090](https://issues.apache.org/jira/browse/CASSANDRA-11090) | Avoid creating empty hint files |  Minor | . | Blake Eggleston | Aleksey Yeschenko |
| [CASSANDRA-11139](https://issues.apache.org/jira/browse/CASSANDRA-11139) | New token allocator is broken |  Major | Configuration | Tommy Stendahl | Branimir Lambov |
| [CASSANDRA-7238](https://issues.apache.org/jira/browse/CASSANDRA-7238) | Nodetool Status performance is much slower with VNodes On |  Minor | Tools | Russell Spitzer | Yuki Morishita |
| [CASSANDRA-10697](https://issues.apache.org/jira/browse/CASSANDRA-10697) | Leak detected while running offline scrub |  Major | Tools | mlowicki | Marcus Eriksson |
| [CASSANDRA-11079](https://issues.apache.org/jira/browse/CASSANDRA-11079) | LeveledCompactionStrategyTest.testCompactionProgress unit test fails sometimes |  Major | Testing | Sylvain Lebresne | Marcus Eriksson |
| [CASSANDRA-11080](https://issues.apache.org/jira/browse/CASSANDRA-11080) | CompactionsCQLTest.testTriggerMinorCompactionSTCS is flaky on 2.2 |  Major | . | Sylvain Lebresne | Marcus Eriksson |
| [CASSANDRA-11156](https://issues.apache.org/jira/browse/CASSANDRA-11156) | AssertionError when listing sstable files on inconsistent disk state |  Major | Local Write-Read Paths | Stefania | Stefania |
| [CASSANDRA-7281](https://issues.apache.org/jira/browse/CASSANDRA-7281) | SELECT on tuple relations are broken for mixed ASC/DESC clustering order |  Major | . | Sylvain Lebresne | Marcin Szymaniuk |
| [CASSANDRA-11050](https://issues.apache.org/jira/browse/CASSANDRA-11050) | SSTables not loaded after dropping column |  Major | Distributed Metadata | amorton | amorton |
| [CASSANDRA-11048](https://issues.apache.org/jira/browse/CASSANDRA-11048) | JSON queries are not thread safe |  Critical | Coordination | Sergio Bossa | Tyler Hobbs |
| [CASSANDRA-10733](https://issues.apache.org/jira/browse/CASSANDRA-10733) | Inconsistencies in CQLSH auto-complete |  Trivial | CQL, Tools | Michael Edge | Michael Edge |
| [CASSANDRA-10512](https://issues.apache.org/jira/browse/CASSANDRA-10512) | We do not save an upsampled index summaries |  Major | Local Write-Read Paths | Benedict | Ariel Weisberg |
| [CASSANDRA-10736](https://issues.apache.org/jira/browse/CASSANDRA-10736) | TestTopology.simple\_decommission\_test failing due to assertion triggered by SizeEstimatesRecorder |  Minor | Distributed Metadata | Joel Knighton | Joel Knighton |
| [CASSANDRA-11084](https://issues.apache.org/jira/browse/CASSANDRA-11084) | cassandra-3.0 eclipse-warnings |  Minor | Testing | Michael Shuler | Benjamin Lerer |
| [CASSANDRA-10176](https://issues.apache.org/jira/browse/CASSANDRA-10176) | nodetool status says ' Non-system keyspaces don't have the same replication settings' when they do |  Minor | Distributed Metadata | Chris Burroughs | Sylvain Lebresne |
| [CASSANDRA-10721](https://issues.apache.org/jira/browse/CASSANDRA-10721) | Altering a UDT might break UDA deserialisation |  Major | CQL, Distributed Metadata | Aleksey Yeschenko | Robert Stupp |
| [CASSANDRA-11123](https://issues.apache.org/jira/browse/CASSANDRA-11123) | cqlsh pg-style-strings broken if line ends with ';' |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-9714](https://issues.apache.org/jira/browse/CASSANDRA-9714) | sstableloader appears to use the cassandra.yaml outgoing stream throttle |  Major | Tools | Jeremy Hanna | Yuki Morishita |
| [CASSANDRA-10767](https://issues.apache.org/jira/browse/CASSANDRA-10767) | Checking version of Cassandra command creates \`cassandra.logdir\_IS\_UNDEFINED/\` |  Trivial | Tools | Jens Rantil | Yuki Morishita |
| [CASSANDRA-10991](https://issues.apache.org/jira/browse/CASSANDRA-10991) | Cleanup OpsCenter keyspace fails - node thinks that didn't joined the ring yet |  Major | Observability | mlowicki | Marcus Eriksson |
| [CASSANDRA-11146](https://issues.apache.org/jira/browse/CASSANDRA-11146) | Adding field to UDT definition breaks SELECT JSON |  Major | CQL | Alexander | Tyler Hobbs |
| [CASSANDRA-11065](https://issues.apache.org/jira/browse/CASSANDRA-11065) | null pointer exception in CassandraDaemon.java:195 |  Minor | Streaming and Messaging | Vassil Lunchev | Paulo Motta |
| [CASSANDRA-10793](https://issues.apache.org/jira/browse/CASSANDRA-10793) | fix ohc and java-driver pom dependencies in build.xml |  Major | Packaging | Jeremiah Jordan | Robert Stupp |
| [CASSANDRA-11033](https://issues.apache.org/jira/browse/CASSANDRA-11033) | Prevent logging in sandboxed state |  Minor | CQL | Robert Stupp | Robert Stupp |
| [CASSANDRA-10625](https://issues.apache.org/jira/browse/CASSANDRA-10625) | Problem of year 10000: Dates too far in the future can be saved but not read back using cqlsh |  Minor | . | Piotr Kołaczkowski | Adam Holmberg |
| [CASSANDRA-10840](https://issues.apache.org/jira/browse/CASSANDRA-10840) | Replacing an aggregate with a new version doesn't reset INITCOND |  Major | CQL | Sandeep Tamhankar | Robert Stupp |
| [CASSANDRA-11172](https://issues.apache.org/jira/browse/CASSANDRA-11172) | Infinite loop bug adding high-level SSTableReader in compaction |  Major | Compaction | Jeff Ferland | Marcus Eriksson |
| [CASSANDRA-11158](https://issues.apache.org/jira/browse/CASSANDRA-11158) | AssertionError: null in Slice$Bound.create |  Critical | Compaction, Local Write-Read Paths | Samu Kallio | Branimir Lambov |
| [CASSANDRA-10972](https://issues.apache.org/jira/browse/CASSANDRA-10972) | File based hints don't implement backpressure and can OOM |  Minor | . | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-11167](https://issues.apache.org/jira/browse/CASSANDRA-11167) | NPE when creating serializing ErrorMessage for Exception with null message |  Minor | Coordination | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-10371](https://issues.apache.org/jira/browse/CASSANDRA-10371) | Decommissioned nodes can remain in gossip |  Minor | Distributed Metadata | Brandon Williams | Joel Knighton |
| [CASSANDRA-11212](https://issues.apache.org/jira/browse/CASSANDRA-11212) | cqlsh python version checking is out of date |  Major | Tools | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-11064](https://issues.apache.org/jira/browse/CASSANDRA-11064) | Failed aggregate creation breaks server permanently |  Major | . | Olivier Michallat | Robert Stupp |
| [CASSANDRA-11069](https://issues.apache.org/jira/browse/CASSANDRA-11069) | Materialised views require all collections to be selected. |  Major | Coordination, Materialized Views | Vassil Lunchev | Carl Yeksigian |
| [CASSANDRA-11216](https://issues.apache.org/jira/browse/CASSANDRA-11216) | Range.compareTo() violates the contract of Comparable |  Minor | Core | Jason Brown | Jason Brown |
| [CASSANDRA-11214](https://issues.apache.org/jira/browse/CASSANDRA-11214) | Adding Support for system-Z(s390x) architecture |  Minor | Observability, Testing | Nirav | Nirav |
| [CASSANDRA-11239](https://issues.apache.org/jira/browse/CASSANDRA-11239) | Deprecated repair methods cause NPE |  Major | . | Nick Bailey | Nick Bailey |
| [CASSANDRA-11062](https://issues.apache.org/jira/browse/CASSANDRA-11062) | BatchlogManager May Attempt to Flush Hints After HintsService is Shutdown |  Minor | Local Write-Read Paths | Caleb Rackliffe | Caleb Rackliffe |
| [CASSANDRA-11164](https://issues.apache.org/jira/browse/CASSANDRA-11164) | Order and filter cipher suites correctly |  Minor | . | Tom Petracca | Stefan Podkowinski |
| [CASSANDRA-11184](https://issues.apache.org/jira/browse/CASSANDRA-11184) | cqlsh\_tests.py:TestCqlsh.test\_sub\_second\_precision is failing |  Major | Testing | Michael Shuler | Jim Witschey |
| [CASSANDRA-11185](https://issues.apache.org/jira/browse/CASSANDRA-11185) | cqlsh\_copy\_tests.py:CqlshCopyTest.test\_round\_trip\_with\_sub\_second\_precision is failing |  Major | Testing | Michael Shuler | Jim Witschey |
| [CASSANDRA-11128](https://issues.apache.org/jira/browse/CASSANDRA-11128) | Unexpected exception during request; AssertionError: null |  Major | . | Russ Hatch | Sylvain Lebresne |
| [CASSANDRA-11043](https://issues.apache.org/jira/browse/CASSANDRA-11043) | Secondary indexes doesn't properly validate custom expressions |  Major | CQL, Local Write-Read Paths, Secondary Indexes | Andrés de la Peña | Sam Tunnicliffe |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5902](https://issues.apache.org/jira/browse/CASSANDRA-5902) | Dealing with hints after a topology change |  Minor | Coordination | Jonathan Ellis | Branimir Lambov |


