
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.0.12 - 2015-01-20



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6952](https://issues.apache.org/jira/browse/CASSANDRA-6952) | Cannot bind variables to USE statements |  Minor | CQL | Matt Stump | Benjamin Lerer |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7463](https://issues.apache.org/jira/browse/CASSANDRA-7463) | Update CQLSSTableWriter to allow parallel writing of SSTables on the same table within the same JVM |  Major | . | Johnny Miller | Carl Yeksigian |
| [CASSANDRA-4959](https://issues.apache.org/jira/browse/CASSANDRA-4959) | CQLSH insert help has typo |  Trivial | Documentation and Website | Edward Capriolo | Ricardo Devis Agullo |
| [CASSANDRA-8188](https://issues.apache.org/jira/browse/CASSANDRA-8188) | don't block SocketThread for MessagingService |  Major | . | yangwei | yangwei |
| [CASSANDRA-8125](https://issues.apache.org/jira/browse/CASSANDRA-8125) | nodetool statusgossip doesn't exist |  Minor | . | Connor Warrington | Jan Karlsson |
| [CASSANDRA-7979](https://issues.apache.org/jira/browse/CASSANDRA-7979) | Acceptable time skew for C\* |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-8183](https://issues.apache.org/jira/browse/CASSANDRA-8183) | improve PropertyFileSnitch output |  Trivial | Configuration | Liang Xie | Liang Xie |
| [CASSANDRA-8221](https://issues.apache.org/jira/browse/CASSANDRA-8221) | Specify keyspace in error message when streaming fails due to missing replicas |  Trivial | . | Tyler Hobbs | Rajanarayanan Thottuvaikkatumana |
| [CASSANDRA-7386](https://issues.apache.org/jira/browse/CASSANDRA-7386) | JBOD threshold to prevent unbalanced disk utilization |  Minor | . | Chris Lohfink | Robert Stupp |
| [CASSANDRA-8329](https://issues.apache.org/jira/browse/CASSANDRA-8329) | LeveledCompactionStrategy should split large files across data directories when compacting |  Major | Compaction | J.B. Langston | Marcus Eriksson |
| [CASSANDRA-7897](https://issues.apache.org/jira/browse/CASSANDRA-7897) | NodeTool command to display OffHeap memory usage |  Minor | Observability, Tools | Vijay | Benjamin Lerer |
| [CASSANDRA-8417](https://issues.apache.org/jira/browse/CASSANDRA-8417) | Default base\_time\_seconds in DTCS is almost always too large |  Major | . | Jonathan Ellis | Björn Hegerfors |
| [CASSANDRA-7947](https://issues.apache.org/jira/browse/CASSANDRA-7947) | Change error message when RR times out |  Minor | Observability | Brandon Williams | Sam Tunnicliffe |
| [CASSANDRA-8193](https://issues.apache.org/jira/browse/CASSANDRA-8193) | Multi-DC parallel snapshot repair |  Minor | . | Jimmy Mårdell | Jimmy Mårdell |
| [CASSANDRA-8194](https://issues.apache.org/jira/browse/CASSANDRA-8194) | Reading from Auth table should not be in the request path |  Minor | Distributed Metadata | Vishy Kasar | Sam Tunnicliffe |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7704](https://issues.apache.org/jira/browse/CASSANDRA-7704) | FileNotFoundException during STREAM-OUT triggers 100% CPU usage |  Major | . | Rick Branson | Benedict |
| [CASSANDRA-8166](https://issues.apache.org/jira/browse/CASSANDRA-8166) | Not all data is loaded to Pig using CqlNativeStorage |  Major | . | Oksana Danylyshyn | Alex Liu |
| [CASSANDRA-8116](https://issues.apache.org/jira/browse/CASSANDRA-8116) | HSHA fails with default rpc\_max\_threads setting |  Minor | . | Mike Adamson | Tyler Hobbs |
| [CASSANDRA-8139](https://issues.apache.org/jira/browse/CASSANDRA-8139) | The WRITETIME function returns null for negative timestamp values |  Minor | CQL | Richard Bremner | Sylvain Lebresne |
| [CASSANDRA-8205](https://issues.apache.org/jira/browse/CASSANDRA-8205) | ColumnFamilyMetrics#totalDiskSpaceUsed gets wrong value when SSTable is deleted |  Minor | Observability | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-7510](https://issues.apache.org/jira/browse/CASSANDRA-7510) | Notify clients that bootstrap is finished over binary protocol |  Minor | . | Joost Reuzel | Brandon Williams |
| [CASSANDRA-8247](https://issues.apache.org/jira/browse/CASSANDRA-8247) | HHOM creates repeated 'No files to compact for user defined compaction' messages |  Minor | . | Brandon Williams | Carl Yeksigian |
| [CASSANDRA-8206](https://issues.apache.org/jira/browse/CASSANDRA-8206) | Deleting columns breaks secondary index on clustering column |  Critical | Secondary Indexes | Tuukka Mustonen | Sylvain Lebresne |
| [CASSANDRA-8178](https://issues.apache.org/jira/browse/CASSANDRA-8178) | Column names are not converted correctly for non-text comparators |  Major | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-8164](https://issues.apache.org/jira/browse/CASSANDRA-8164) | OOM due to slow memory meter |  Major | . | Oleg Anastasyev | Oleg Anastasyev |
| [CASSANDRA-8211](https://issues.apache.org/jira/browse/CASSANDRA-8211) | Overlapping sstables in L1+ |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-8297](https://issues.apache.org/jira/browse/CASSANDRA-8297) | Milliseconds since epoch used for tracing tables |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-8265](https://issues.apache.org/jira/browse/CASSANDRA-8265) | Disable SSLv3 for POODLE |  Major | . | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-8286](https://issues.apache.org/jira/browse/CASSANDRA-8286) | Regression in ORDER BY |  Major | . | Philip Thompson | Tyler Hobbs |
| [CASSANDRA-8264](https://issues.apache.org/jira/browse/CASSANDRA-8264) | Problems with multicolumn relations and COMPACT STORAGE |  Major | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-8280](https://issues.apache.org/jira/browse/CASSANDRA-8280) | Cassandra crashing on inserting data over 64K into indexed strings |  Critical | CQL | Cristian Marinescu | Sam Tunnicliffe |
| [CASSANDRA-7538](https://issues.apache.org/jira/browse/CASSANDRA-7538) | Truncate of a CF should also delete Paxos CF |  Minor | Coordination | sankalp kohli | Sam Tunnicliffe |
| [CASSANDRA-8260](https://issues.apache.org/jira/browse/CASSANDRA-8260) | Replacing a node can leave the old node in system.peers on the replacement |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-8122](https://issues.apache.org/jira/browse/CASSANDRA-8122) | Undeclare throwable exception while executing 'nodetool netstats localhost' |  Minor | Tools | Vishal Mehta | Carl Yeksigian |
| [CASSANDRA-8401](https://issues.apache.org/jira/browse/CASSANDRA-8401) | dropping a CF doesn't remove the latency-sampling task |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-8416](https://issues.apache.org/jira/browse/CASSANDRA-8416) | AssertionError 'Incoherent new size -1' during hints compaction |  Major | . | Richard Low | Marcus Eriksson |
| [CASSANDRA-8346](https://issues.apache.org/jira/browse/CASSANDRA-8346) | Paxos operation can use stale data during multiple range movements |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-8332](https://issues.apache.org/jira/browse/CASSANDRA-8332) | Null pointer after droping keyspace |  Minor | Distributed Metadata | Chris Lohfink | T Jake Luciani |
| [CASSANDRA-8451](https://issues.apache.org/jira/browse/CASSANDRA-8451) | NPE when writetime() or ttl() are nested inside function call |  Minor | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-8410](https://issues.apache.org/jira/browse/CASSANDRA-8410) | Select with many IN values on clustering columns can result in a StackOverflowError |  Major | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-8156](https://issues.apache.org/jira/browse/CASSANDRA-8156) | Fix validation of indexes on composite column components of COMPACT tables |  Trivial | . | Denis Angilella | Sylvain Lebresne |
| [CASSANDRA-8373](https://issues.apache.org/jira/browse/CASSANDRA-8373) | MOVED\_NODE Topology Change event is never emitted |  Minor | . | Adam Holmberg | Adam Holmberg |
| [CASSANDRA-8408](https://issues.apache.org/jira/browse/CASSANDRA-8408) | limit appears to replace page size under certain conditions |  Minor | . | Russ Hatch | Tyler Hobbs |
| [CASSANDRA-8285](https://issues.apache.org/jira/browse/CASSANDRA-8285) | Move all hints related tasks to hints private executor |  Major | . | Pierre Laporte | Aleksey Yeschenko |
| [CASSANDRA-8087](https://issues.apache.org/jira/browse/CASSANDRA-8087) | Multiple non-DISTINCT rows returned when page\_size set |  Minor | . | Adam Holmberg | Tyler Hobbs |
| [CASSANDRA-8485](https://issues.apache.org/jira/browse/CASSANDRA-8485) | Move 2.0 metered flusher to its own thread |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-8525](https://issues.apache.org/jira/browse/CASSANDRA-8525) | Bloom Filter truePositive counter not updated on key cache hit |  Major | Local Write-Read Paths | Tyler Hobbs | Marcus Eriksson |
| [CASSANDRA-8541](https://issues.apache.org/jira/browse/CASSANDRA-8541) | References to non-existent/deprecated CqlPagingInputFormat in code |  Major | . | Rekha Joshi | Rekha Joshi |
| [CASSANDRA-8245](https://issues.apache.org/jira/browse/CASSANDRA-8245) | Cassandra nodes periodically die in 2-DC configuration |  Minor | . | Oleg Poleshuk | Brandon Williams |
| [CASSANDRA-8499](https://issues.apache.org/jira/browse/CASSANDRA-8499) | Ensure SSTableWriter cleans up properly after failure |  Major | . | Benedict | Benedict |
| [CASSANDRA-8462](https://issues.apache.org/jira/browse/CASSANDRA-8462) | Upgrading a 2.0 to 2.1 breaks CFMetaData on 2.0 nodes |  Major | . | Rick Branson | Aleksey Yeschenko |
| [CASSANDRA-6983](https://issues.apache.org/jira/browse/CASSANDRA-6983) | DirectoriesTest fails when run as root |  Minor | Testing | Brandon Williams | Alan Boudreault |
| [CASSANDRA-8490](https://issues.apache.org/jira/browse/CASSANDRA-8490) | DISTINCT queries with LIMITs or paging are incorrect when partitions are deleted |  Major | . | Frank Limstrand | Tyler Hobbs |
| [CASSANDRA-8587](https://issues.apache.org/jira/browse/CASSANDRA-8587) | Fix MessageOut's serializeSize calculation |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-8562](https://issues.apache.org/jira/browse/CASSANDRA-8562) | Fix checking available disk space before compaction starts |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-8579](https://issues.apache.org/jira/browse/CASSANDRA-8579) | sstablemetadata can't load org.apache.cassandra.tools.SSTableMetadataViewer |  Minor | Tools | Jimmy Mårdell | Jimmy Mårdell |
| [CASSANDRA-8548](https://issues.apache.org/jira/browse/CASSANDRA-8548) | Nodetool Cleanup - java.lang.AssertionError |  Major | . | Sebastian Estevez | Marcus Eriksson |
| [CASSANDRA-8558](https://issues.apache.org/jira/browse/CASSANDRA-8558) | deleted row still can be selected out |  Blocker | . | zhaoyan | Sylvain Lebresne |
| [CASSANDRA-8550](https://issues.apache.org/jira/browse/CASSANDRA-8550) | Internal pagination in CQL3 index queries creating substantial overhead |  Major | . | Samuel Klock | Tyler Hobbs |
| [CASSANDRA-8640](https://issues.apache.org/jira/browse/CASSANDRA-8640) | Paxos requires all nodes for CAS |  Major | . | Anthony Cozzie | Anthony Cozzie |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7713](https://issues.apache.org/jira/browse/CASSANDRA-7713) | CommitLogTest failure causes cascading unit test failures |  Major | . | Michael Shuler | Bogdan Kanivets |
| [CASSANDRA-8212](https://issues.apache.org/jira/browse/CASSANDRA-8212) | Archive Commitlog Test Failing |  Major | . | Philip Thompson | Philip Thompson |


