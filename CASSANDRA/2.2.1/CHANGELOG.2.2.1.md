
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.2.1 - 2015-09-01



### IMPORTANT ISSUES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9884](https://issues.apache.org/jira/browse/CASSANDRA-9884) | Error on encrypted node communication upgrading from 2.1.6 to 2.2.0 |  Critical | Configuration | Carlos Scheidecker | Yuki Morishita |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9723](https://issues.apache.org/jira/browse/CASSANDRA-9723) | UDF / UDA execution time in trace |  Minor | . | Christopher Batey | Christopher Batey |
| [CASSANDRA-9845](https://issues.apache.org/jira/browse/CASSANDRA-9845) | Fix build.xml to not download non-existent source jars, cleanup duplicate artifacts |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-9757](https://issues.apache.org/jira/browse/CASSANDRA-9757) | Cache maven-ant-tasks.jar locally |  Trivial | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-9648](https://issues.apache.org/jira/browse/CASSANDRA-9648) | Warn if power profile is not High Performance on Windows |  Minor | Packaging | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-9793](https://issues.apache.org/jira/browse/CASSANDRA-9793) | Log when messages are dropped due to cross\_node\_timeout |  Major | Streaming and Messaging | Brandon Williams | Stefania |
| [CASSANDRA-9827](https://issues.apache.org/jira/browse/CASSANDRA-9827) | Add consistency level to tracing ouput |  Minor | . | Alec Grieser |  |
| [CASSANDRA-9265](https://issues.apache.org/jira/browse/CASSANDRA-9265) | Add checksum to saved cache files |  Major | . | Ariel Weisberg | Daniel Chia |
| [CASSANDRA-9533](https://issues.apache.org/jira/browse/CASSANDRA-9533) | Make batch commitlog mode easier to tune |  Major | . | Jonathan Ellis | Benedict |
| [CASSANDRA-10015](https://issues.apache.org/jira/browse/CASSANDRA-10015) | Create tool to debug why expired sstables are not getting dropped |  Major | Tools | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9237](https://issues.apache.org/jira/browse/CASSANDRA-9237) | Gossip messages subject to head of line blocking by other intra-cluster traffic |  Major | Streaming and Messaging | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-10004](https://issues.apache.org/jira/browse/CASSANDRA-10004) | Allow changing default encoding on cqlsh |  Minor | Tools | Paulo Motta | Paulo Motta |
| [CASSANDRA-10056](https://issues.apache.org/jira/browse/CASSANDRA-10056) | Fix AggregationTest post-test error messages |  Trivial | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-10114](https://issues.apache.org/jira/browse/CASSANDRA-10114) | Allow count(\*) and count(1) to be use as normal aggregation |  Minor | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-10083](https://issues.apache.org/jira/browse/CASSANDRA-10083) | Revert AutoSavingCache.IStreamFactory to return OutputStream |  Major | . | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-9615](https://issues.apache.org/jira/browse/CASSANDRA-9615) | Speed up Windows launch scripts |  Minor | Packaging | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-10086](https://issues.apache.org/jira/browse/CASSANDRA-10086) | Add a "CLEAR" cqlsh command to clear the console |  Trivial | . | Paul O'Fallon | Paul O'Fallon |
| [CASSANDRA-10234](https://issues.apache.org/jira/browse/CASSANDRA-10234) | Add nodetool gettraceprobability command |  Trivial | . | sequoyha pelletier | sequoyha pelletier |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-8220](https://issues.apache.org/jira/browse/CASSANDRA-8220) | JDK bug prevents clean shutdown on OSX with Java 1.8.0\_20 |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-9858](https://issues.apache.org/jira/browse/CASSANDRA-9858) | SelectStatement.Parameters fields should be inspectable by custom indexes and query handlers |  Major | . | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-7357](https://issues.apache.org/jira/browse/CASSANDRA-7357) | Cleaning up snapshots on startup leftover from repairs that got interrupted |  Minor | Streaming and Messaging | Stephen Johnson | Paulo Motta |
| [CASSANDRA-9765](https://issues.apache.org/jira/browse/CASSANDRA-9765) | checkForEndpointCollision fails for legitimate collisions |  Major | Distributed Metadata | Richard Low | Stefania |
| [CASSANDRA-9869](https://issues.apache.org/jira/browse/CASSANDRA-9869) | windows - failed testClearEphemeralSnapshots |  Trivial | Testing | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-8735](https://issues.apache.org/jira/browse/CASSANDRA-8735) | Batch log replication is not randomized when there are only 2 racks |  Minor | . | Yuki Morishita | Mihai Suteu |
| [CASSANDRA-9696](https://issues.apache.org/jira/browse/CASSANDRA-9696) | nodetool stopdaemon exception |  Minor | Packaging | Andreas Schnitzerling | Joshua McKenzie |
| [CASSANDRA-9849](https://issues.apache.org/jira/browse/CASSANDRA-9849) | Paging changes to 2.1/2.2 for backward compatibilty with 3.0 |  Major | . | Sylvain Lebresne | Tyler Hobbs |
| [CASSANDRA-9382](https://issues.apache.org/jira/browse/CASSANDRA-9382) | Snapshot file descriptors not getting purged (possible fd leak) |  Major | Local Write-Read Paths | Mark Curtis | Yuki Morishita |
| [CASSANDRA-9899](https://issues.apache.org/jira/browse/CASSANDRA-9899) | If compaction is disabled in schema, you can't enable a single node through nodetool |  Major | Compaction | Jeremiah Jordan | Marcus Eriksson |
| [CASSANDRA-7757](https://issues.apache.org/jira/browse/CASSANDRA-7757) | Possible Atomicity Violations in StreamSession and ThriftSessionManager |  Minor | Streaming and Messaging | Diogo Sousa | Paulo Motta |
| [CASSANDRA-9900](https://issues.apache.org/jira/browse/CASSANDRA-9900) | Anticompaction can mix old and new data with DTCS in 2.2+ |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9871](https://issues.apache.org/jira/browse/CASSANDRA-9871) | Cannot replace token does not exist - DN node removed as Fat Client |  Major | Distributed Metadata | Sebastian Estevez | Stefania |
| [CASSANDRA-9959](https://issues.apache.org/jira/browse/CASSANDRA-9959) | Expected bloom filter size should not be an int |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9941](https://issues.apache.org/jira/browse/CASSANDRA-9941) | SSTableVerify has wrong return codes on windows |  Major | Tools | Philip Thompson | Paulo Motta |
| [CASSANDRA-9942](https://issues.apache.org/jira/browse/CASSANDRA-9942) | SStableofflinerevel and sstablelevelreset don't have windows versions |  Major | Tools | Philip Thompson | Paulo Motta |
| [CASSANDRA-9963](https://issues.apache.org/jira/browse/CASSANDRA-9963) | Compaction not starting for new tables |  Major | Compaction | Jeremiah Jordan | Marcus Eriksson |
| [CASSANDRA-9031](https://issues.apache.org/jira/browse/CASSANDRA-9031) | nodetool info -T throws ArrayOutOfBounds when the node has not joined the cluster |  Major | Tools | Ron Kuris | Yuki Morishita |
| [CASSANDRA-9962](https://issues.apache.org/jira/browse/CASSANDRA-9962) | WaitQueueTest is flakey |  Minor | . | Benedict | Benedict |
| [CASSANDRA-9998](https://issues.apache.org/jira/browse/CASSANDRA-9998) | LEAK DETECTED with snapshot/sequential repairs |  Major | Streaming and Messaging | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9777](https://issues.apache.org/jira/browse/CASSANDRA-9777) | If you have a ~/.cqlshrc and a ~/.cassandra/cqlshrc, cqlsh will overwrite the latter with the former |  Major | . | Jon Moses | David Kua |
| [CASSANDRA-10000](https://issues.apache.org/jira/browse/CASSANDRA-10000) | Dates before 1970-01-01 are not formatted correctly on cqlsh\\Windows |  Minor | Tools | Paulo Motta | Paulo Motta |
| [CASSANDRA-9891](https://issues.apache.org/jira/browse/CASSANDRA-9891) | AggregationTest.testAggregateWithWithWriteTimeOrTTL is fragile |  Minor | CQL | Sylvain Lebresne | Benjamin Lerer |
| [CASSANDRA-9965](https://issues.apache.org/jira/browse/CASSANDRA-9965) | Add new JMX methods to change local compaction strategy |  Minor | Compaction | Aleksey Yeschenko | Marcus Eriksson |
| [CASSANDRA-9997](https://issues.apache.org/jira/browse/CASSANDRA-9997) | Document removal of cold\_reads\_to\_omit in 2.2 and 3.0 NEWS.txt |  Minor | Documentation and Website | Tommy Stendahl | Marcus Eriksson |
| [CASSANDRA-10008](https://issues.apache.org/jira/browse/CASSANDRA-10008) | Upgrading SSTables fails on 2.2.0 (after upgrade from 2.1.2) |  Major | . | Chris Moos | Chris Moos |
| [CASSANDRA-10040](https://issues.apache.org/jira/browse/CASSANDRA-10040) | Fix CASSANDRA-9771 |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-9973](https://issues.apache.org/jira/browse/CASSANDRA-9973) | java.lang.IllegalStateException: Unable to compute when histogram overflowed |  Major | Observability | Mark Manley | T Jake Luciani |
| [CASSANDRA-10071](https://issues.apache.org/jira/browse/CASSANDRA-10071) | Unable to start Thrift RPC server in cassandra deployment created using cassandra-all |  Major | Packaging | Lukas Krejci |  |
| [CASSANDRA-9462](https://issues.apache.org/jira/browse/CASSANDRA-9462) | ViewTest.sstableInBounds is failing |  Major | Local Write-Read Paths, Streaming and Messaging | Benedict | Ariel Weisberg |
| [CASSANDRA-8515](https://issues.apache.org/jira/browse/CASSANDRA-8515) | Commit log stop policy not enforced correctly during startup |  Minor | Coordination, Lifecycle | Richard Low | Paulo Motta |
| [CASSANDRA-10087](https://issues.apache.org/jira/browse/CASSANDRA-10087) | Typo in CreateTableStatement error message |  Trivial | . | Sehrope Sarkuni |  |
| [CASSANDRA-9970](https://issues.apache.org/jira/browse/CASSANDRA-9970) | CQL 3.3 ordering in where clause produces different results when using "IN...ORDER BY" |  Minor | CQL | Marc Zbyszynski | Benjamin Lerer |
| [CASSANDRA-9882](https://issues.apache.org/jira/browse/CASSANDRA-9882) | DTCS (maybe other strategies) can block flushing when there are lots of sstables |  Major | Compaction | Jeremiah Jordan | Marcus Eriksson |
| [CASSANDRA-10094](https://issues.apache.org/jira/browse/CASSANDRA-10094) | Windows utest 2.2: testCommitLogFailureBeforeInitialization\_mustKillJVM failure |  Major | . | Joshua McKenzie | Paulo Motta |
| [CASSANDRA-10115](https://issues.apache.org/jira/browse/CASSANDRA-10115) | Windows utest 2.2: LeveledCompactionStrategyTest.testGrouperLevels flaky |  Major | Compaction, Testing | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-10043](https://issues.apache.org/jira/browse/CASSANDRA-10043) | A NullPointerException is thrown if the column name is unknown for an IN relation |  Major | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-9749](https://issues.apache.org/jira/browse/CASSANDRA-9749) | CommitLogReplayer continues startup after encountering errors |  Major | . | Blake Eggleston | Branimir Lambov |
| [CASSANDRA-10048](https://issues.apache.org/jira/browse/CASSANDRA-10048) | cassandra-stress - Decimal is a BigInt not a Double |  Major | . | Sebastian Estevez |  |
| [CASSANDRA-10152](https://issues.apache.org/jira/browse/CASSANDRA-10152) | dtest: user\_functions\_test.py:TestUserFunctions.udf\_scripting\_test fails |  Major | . | Joshua McKenzie | Robert Stupp |
| [CASSANDRA-10135](https://issues.apache.org/jira/browse/CASSANDRA-10135) | Quoting changed for username in GRANT statement |  Minor | CQL | Bernhard K. Weisshuhn | Sam Tunnicliffe |
| [CASSANDRA-10139](https://issues.apache.org/jira/browse/CASSANDRA-10139) | Windows utest 2.2: NanoTimeToCurrentTimeMillisTest.testTimestampOrdering intermittent failure |  Major | . | Joshua McKenzie | Paulo Motta |
| [CASSANDRA-10168](https://issues.apache.org/jira/browse/CASSANDRA-10168) | CassandraAuthorizer.authorize must throw exception when lookup of any auth table fails |  Major | . | Vishy Kasar | Vishy Kasar |
| [CASSANDRA-9898](https://issues.apache.org/jira/browse/CASSANDRA-9898) | cqlsh crashes if it load a utf-8 file. |  Minor | Tools | Yasuharu Goto | Yasuharu Goto |
| [CASSANDRA-9460](https://issues.apache.org/jira/browse/CASSANDRA-9460) | NullPointerException Creating Digest |  Major | Local Write-Read Paths | Tyler Hobbs | T Jake Luciani |
| [CASSANDRA-9232](https://issues.apache.org/jira/browse/CASSANDRA-9232) | "timestamp" is considered as a reserved keyword in cqlsh completion |  Trivial | Tools | Michaël Figuière | Stefania |
| [CASSANDRA-9142](https://issues.apache.org/jira/browse/CASSANDRA-9142) | DC Local repair or -hosts should only be allowed with -full repair |  Minor | Streaming and Messaging | sankalp kohli | Marcus Eriksson |
| [CASSANDRA-10203](https://issues.apache.org/jira/browse/CASSANDRA-10203) | UnbufferedDataOutputstreamPlus.writeUTF fails for 0 length and \> 8190 length strings |  Major | Compaction, Local Write-Read Paths, Streaming and Messaging | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-10049](https://issues.apache.org/jira/browse/CASSANDRA-10049) | Commitlog initialization failure |  Major | . | T Jake Luciani | Branimir Lambov |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9581](https://issues.apache.org/jira/browse/CASSANDRA-9581) | pig-tests spend time waiting on /dev/random for SecureRandom |  Major | Testing | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-10151](https://issues.apache.org/jira/browse/CASSANDRA-10151) | Fix TrackerTest.testTryModify |  Major | . | Stefania | Benedict |
| [CASSANDRA-9575](https://issues.apache.org/jira/browse/CASSANDRA-9575) | LeveledCompactionStrategyTest.testCompactionRace and testMutateLevel sometimes fail |  Major | Testing | Ariel Weisberg | Yuki Morishita |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9903](https://issues.apache.org/jira/browse/CASSANDRA-9903) | Windows dtest: read timeout in consistency\_test.py:TestConsistency.short\_read\_quorum\_delete\_test |  Minor | Testing | Joshua McKenzie | T Jake Luciani |
| [CASSANDRA-7342](https://issues.apache.org/jira/browse/CASSANDRA-7342) | CAS writes do not have hint functionality. |  Major | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-9994](https://issues.apache.org/jira/browse/CASSANDRA-9994) | super\_counter\_test.py failing on Windows |  Major | . | Philip Thompson | Paulo Motta |
| [CASSANDRA-9982](https://issues.apache.org/jira/browse/CASSANDRA-9982) | sstablerepairedset has no .bat version |  Major | Tools | Philip Thompson | Paulo Motta |
| [CASSANDRA-9897](https://issues.apache.org/jira/browse/CASSANDRA-9897) | Windows dtest: snapshot\_test.py failures |  Major | Testing | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-9904](https://issues.apache.org/jira/browse/CASSANDRA-9904) | Windows dtest: cqlsh\_tests\\cqlsh\_copy\_tests.py failing with Permission denied |  Minor | Testing | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-10029](https://issues.apache.org/jira/browse/CASSANDRA-10029) | Read repair dtest failing on Windows |  Major | . | Philip Thompson | Paulo Motta |
| [CASSANDRA-10051](https://issues.apache.org/jira/browse/CASSANDRA-10051) | Windows dtest:  json\_tools\_test.TestJson.json\_tools\_test |  Major | . | Paulo Motta | Paulo Motta |
| [CASSANDRA-10078](https://issues.apache.org/jira/browse/CASSANDRA-10078) | Windows dtest 2.2: rebuild\_test.py:TestRebuild.simple\_rebuild\_test |  Major | Testing | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-9980](https://issues.apache.org/jira/browse/CASSANDRA-9980) | test\_eat\_glass in cqlsh\_tests fails on windows |  Major | . | Philip Thompson | Paulo Motta |
| [CASSANDRA-10075](https://issues.apache.org/jira/browse/CASSANDRA-10075) | Windows dtest 2.2: sstablesplit\_test.py:TestSSTableSplit.split\_test |  Major | Testing | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-10077](https://issues.apache.org/jira/browse/CASSANDRA-10077) | Windows dtest 2.2: json\_test.py:JsonFullRowInsertSelect.pkey\_requirement\_test |  Major | Testing | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-10076](https://issues.apache.org/jira/browse/CASSANDRA-10076) | Windows dtest 2.2: thrift\_hsha\_test.py:ThriftHSHATest.test\_6285 |  Major | . | Joshua McKenzie | Paulo Motta |
| [CASSANDRA-10037](https://issues.apache.org/jira/browse/CASSANDRA-10037) | Auth upgrade test failing on Windows 2.2 |  Major | . | Philip Thompson | Paulo Motta |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9737](https://issues.apache.org/jira/browse/CASSANDRA-9737) | Log warning when using an aggregate without partition key |  Trivial | . | Christopher Batey | Robert Stupp |


