
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1.18 - 2017-06-26



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13230](https://issues.apache.org/jira/browse/CASSANDRA-13230) | Build RPM packages for release |  Major | Packaging | Michael Shuler | Stefan Podkowinski |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13388](https://issues.apache.org/jira/browse/CASSANDRA-13388) | Add hosted CI config files (such as Circle and TravisCI) for easier developer testing |  Major | Testing | Jeff Jirsa | Jeff Jirsa |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13108](https://issues.apache.org/jira/browse/CASSANDRA-13108) | Uncaught exeption stack traces not logged |  Trivial | Core | Christopher Batey | Christopher Batey |
| [CASSANDRA-13278](https://issues.apache.org/jira/browse/CASSANDRA-13278) | Update build.xml and build.properties.default maven repos |  Minor | Build | Michael Shuler | Robert Stupp |
| [CASSANDRA-13364](https://issues.apache.org/jira/browse/CASSANDRA-13364) | Cqlsh COPY fails importing Map\<String,List\<String\>\>, ParseError unhashable type list |  Major | Tools | Nicolae Namolovan | Stefania |
| [CASSANDRA-13413](https://issues.apache.org/jira/browse/CASSANDRA-13413) | Run more test targets on CircleCI |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-13147](https://issues.apache.org/jira/browse/CASSANDRA-13147) | Secondary index query on partition key columns might not return all the rows. |  Major | Secondary Indexes | Benjamin Lerer | Andrés de la Peña |
| [CASSANDRA-13092](https://issues.apache.org/jira/browse/CASSANDRA-13092) | Debian package does not install the hostpot\_compiler command file |  Trivial | Packaging | Jan Urbański | Michael Shuler |
| [CASSANDRA-13466](https://issues.apache.org/jira/browse/CASSANDRA-13466) | Set javac encoding to utf-8 in 2.1 and 2.2. |  Major | Build | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-13412](https://issues.apache.org/jira/browse/CASSANDRA-13412) | Update of column with TTL results in secondary index not returning row |  Major | Secondary Indexes | Enrique Bautista Barahona | Andrés de la Peña |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13440](https://issues.apache.org/jira/browse/CASSANDRA-13440) | Sign RPM artifacts |  Major | Packaging | Stefan Podkowinski | Michael Shuler |


