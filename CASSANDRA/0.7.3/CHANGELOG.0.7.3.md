
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.7.3 - 2011-03-04



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2222](https://issues.apache.org/jira/browse/CASSANDRA-2222) | Make it less easy for user to aim the schema change gun at his foot |  Major | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2217](https://issues.apache.org/jira/browse/CASSANDRA-2217) | nodetool scrub |  Major | Tools | Jonathan Ellis | Jonathan Ellis |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2115](https://issues.apache.org/jira/browse/CASSANDRA-2115) | Keep endpoint state until aVeryLongTime |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-2161](https://issues.apache.org/jira/browse/CASSANDRA-2161) | throttle hinted handoff delivery |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2137](https://issues.apache.org/jira/browse/CASSANDRA-2137) | stress.java should have a way to specify the replication strategy |  Trivial | . | Brandon Williams | Pavel Yaskevich |
| [CASSANDRA-2108](https://issues.apache.org/jira/browse/CASSANDRA-2108) | stress.py uses the same fmt string in several places, it should be centralized |  Minor | . | Matthew F. Dennis | Matthew F. Dennis |
| [CASSANDRA-2175](https://issues.apache.org/jira/browse/CASSANDRA-2175) | make key cache preheating use less memory |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2047](https://issues.apache.org/jira/browse/CASSANDRA-2047) | Stress --keep-going should become --keep-trying |  Trivial | . | T Jake Luciani | Pavel Yaskevich |
| [CASSANDRA-2186](https://issues.apache.org/jira/browse/CASSANDRA-2186) | Use get, put methods in ByteBufferUtil instead of looping over Direct buffers |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2107](https://issues.apache.org/jira/browse/CASSANDRA-2107) | MessageDigests are created in several places, centralize the creation and error handling |  Minor | . | Matthew F. Dennis | Matthew F. Dennis |
| [CASSANDRA-2020](https://issues.apache.org/jira/browse/CASSANDRA-2020) | stress.java performance falls off heavily towards the end |  Minor | . | Brandon Williams | Pavel Yaskevich |
| [CASSANDRA-2239](https://issues.apache.org/jira/browse/CASSANDRA-2239) | Avoid recreating index comparators at each call of indexFor |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2232](https://issues.apache.org/jira/browse/CASSANDRA-2232) | Clean up and document EstimatedHistogram |  Minor | . | Jonathan Ellis | Jonathan Ellis |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2069](https://issues.apache.org/jira/browse/CASSANDRA-2069) | Read repair causes tremendous GC pressure |  Major | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-2172](https://issues.apache.org/jira/browse/CASSANDRA-2172) | Saved-cache files are created for empty caches |  Minor | . | Jonathan Ellis | Matthew F. Dennis |
| [CASSANDRA-2174](https://issues.apache.org/jira/browse/CASSANDRA-2174) | saved caches written with BufferedRandomAccessFile cannot be read by ObjectInputStream |  Major | . | Matthew F. Dennis | Matthew F. Dennis |
| [CASSANDRA-2178](https://issues.apache.org/jira/browse/CASSANDRA-2178) | Memtable Flush writers doesn't actually flush in parallel |  Minor | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-2014](https://issues.apache.org/jira/browse/CASSANDRA-2014) | Can't delete whole row from Hadoop MapReduce |  Major | . | Patrik Modesto | Patrik Modesto |
| [CASSANDRA-2182](https://issues.apache.org/jira/browse/CASSANDRA-2182) | Cassandra doesn't startup on single core boxes. |  Minor | . | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-2162](https://issues.apache.org/jira/browse/CASSANDRA-2162) | cassandra-cli --keyspace option doesn't work properly when used with authentication |  Minor | Tools | Jim Ancona | Jim Ancona |
| [CASSANDRA-2147](https://issues.apache.org/jira/browse/CASSANDRA-2147) | stress.java doesn't read more unique rows than 2x the number of threads |  Minor | . | Brandon Williams | Pavel Yaskevich |
| [CASSANDRA-2184](https://issues.apache.org/jira/browse/CASSANDRA-2184) | Returning split length of 0 confuses Pig |  Minor | . | Jonathan Ellis | Brandon Williams |
| [CASSANDRA-2169](https://issues.apache.org/jira/browse/CASSANDRA-2169) | user created with debian packaging is unable to increase memlock |  Minor | Packaging | Jeremy Hanna | Jeremy Hanna |
| [CASSANDRA-2211](https://issues.apache.org/jira/browse/CASSANDRA-2211) | Cleanup can create sstables whose contents do not match their advertised version |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2216](https://issues.apache.org/jira/browse/CASSANDRA-2216) | Compaction can echo data which breaks upon sstable format changes |  Critical | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2188](https://issues.apache.org/jira/browse/CASSANDRA-2188) | sstable2json generates invalid json for "paged" rows |  Major | Tools | Shotaro Kamio | Pavel Yaskevich |
| [CASSANDRA-2213](https://issues.apache.org/jira/browse/CASSANDRA-2213) | A bug in BufferedRandomAccessFile |  Major | . | Leo Jay | Leo Jay |
| [CASSANDRA-2218](https://issues.apache.org/jira/browse/CASSANDRA-2218) | Performance regression caused by cache-avoiding code in BRAF |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2183](https://issues.apache.org/jira/browse/CASSANDRA-2183) | memtable\_flush\_after\_mins setting not working |  Minor | . | Ching-cheng | Jonathan Ellis |
| [CASSANDRA-2207](https://issues.apache.org/jira/browse/CASSANDRA-2207) | Error saving cache on Windows |  Minor | . | ruslan.usifov | Jonathan Ellis |
| [CASSANDRA-2212](https://issues.apache.org/jira/browse/CASSANDRA-2212) | Cannot get range slice of super columns in reversed order |  Major | . | Muga Nishizawa | Sylvain Lebresne |
| [CASSANDRA-2200](https://issues.apache.org/jira/browse/CASSANDRA-2200) | stress.java doesn't insert the correct amount of rows |  Minor | . | Brandon Williams | Pavel Yaskevich |
| [CASSANDRA-2223](https://issues.apache.org/jira/browse/CASSANDRA-2223) | ClientOnly mode is creating directories |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-2206](https://issues.apache.org/jira/browse/CASSANDRA-2206) | Startup fails due to cassandra trying to delete nonexisting file |  Major | . | Thibaut | Jonathan Ellis |
| [CASSANDRA-2104](https://issues.apache.org/jira/browse/CASSANDRA-2104) | IndexOutOfBoundsException during lazy row compaction of supercolumns |  Major | . | Daniel Lundin | Sylvain Lebresne |
| [CASSANDRA-2237](https://issues.apache.org/jira/browse/CASSANDRA-2237) | cassandra.bat does fail when CASSANDRA\_HOME contains a whitespace |  Major | Packaging | Norman Maurer | Norman Maurer |
| [CASSANDRA-2241](https://issues.apache.org/jira/browse/CASSANDRA-2241) | BRAF read can loop infinitely instead of detecting EOF |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2248](https://issues.apache.org/jira/browse/CASSANDRA-2248) | ant javadoc fails on windows |  Major | Packaging | Norman Maurer | Norman Maurer |
| [CASSANDRA-2100](https://issues.apache.org/jira/browse/CASSANDRA-2100) | Restart required to change cache\_save\_period |  Minor | . | Nick Bailey | Jon Hermes |
| [CASSANDRA-2196](https://issues.apache.org/jira/browse/CASSANDRA-2196) | Hyphenated index names cause problems |  Minor | . | Stephen Pope | Jonathan Ellis |
| [CASSANDRA-2243](https://issues.apache.org/jira/browse/CASSANDRA-2243) | fix "ant codecoverage" |  Major | . | Jonathan Ellis | Stephen Connolly |
| [CASSANDRA-2253](https://issues.apache.org/jira/browse/CASSANDRA-2253) | Gossiper Starvation |  Major | . | Mikael Sitruk | Mikael Sitruk |
| [CASSANDRA-2255](https://issues.apache.org/jira/browse/CASSANDRA-2255) | ColumnFamilyOutputFormat drops mutations when batches fill up. |  Major | . | Eldon Stegall | Jeremy Hanna |
| [CASSANDRA-2240](https://issues.apache.org/jira/browse/CASSANDRA-2240) | nodetool scrub hangs or throws an exception |  Major | Tools | Yaniv Kunda | Jonathan Ellis |
| [CASSANDRA-2187](https://issues.apache.org/jira/browse/CASSANDRA-2187) | Cassandra Cli hangs forever if schema does not settle within timeout window |  Minor | . | Chris Goffinet | Chris Goffinet |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2154](https://issues.apache.org/jira/browse/CASSANDRA-2154) | Update StreamingTransferTest to include multiple ColumnFamilies |  Major | Testing | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-2194](https://issues.apache.org/jira/browse/CASSANDRA-2194) | Add unit test for ByteBufferUtil |  Major | Testing | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2173](https://issues.apache.org/jira/browse/CASSANDRA-2173) | Unit test for row cache |  Minor | . | Jonathan Ellis | Pavel Yaskevich |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1143](https://issues.apache.org/jira/browse/CASSANDRA-1143) | Nodetool gives cryptic errors when given a nonexistent keyspace arg |  Trivial | Tools | Ian Soboroff | Joaquin Casares |


