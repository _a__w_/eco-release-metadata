
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.0.x - Unreleased (as of 2018-10-20)



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10942](https://issues.apache.org/jira/browse/CASSANDRA-10942) | guard against npe if no thrift supercolumn data |  Trivial | Local Write-Read Paths | Dave Brosius | Dave Brosius |
| [CASSANDRA-10563](https://issues.apache.org/jira/browse/CASSANDRA-10563) | Integrate new upgrade test into dtest upgrade suite |  Critical | Testing | Jim Witschey | Jim Witschey |
| [CASSANDRA-14302](https://issues.apache.org/jira/browse/CASSANDRA-14302) | Log when sstables are deleted |  Minor | . | Blake Eggleston | Blake Eggleston |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11018](https://issues.apache.org/jira/browse/CASSANDRA-11018) | Drop column in results in corrupted table or tables state (reversible) |  Minor | CQL | Jason Kania | Sylvain Lebresne |
| [CASSANDRA-10848](https://issues.apache.org/jira/browse/CASSANDRA-10848) | Upgrade paging dtests involving deletion flap on CassCI |  Major | . | Jim Witschey | Russ Hatch |
| [CASSANDRA-13130](https://issues.apache.org/jira/browse/CASSANDRA-13130) | Strange result of several list updates in a single request |  Trivial | . | Mikhail Krupitskiy | Benjamin Lerer |
| [CASSANDRA-13216](https://issues.apache.org/jira/browse/CASSANDRA-13216) | testall failure in org.apache.cassandra.net.MessagingServiceTest.testDroppedMessages |  Major | Testing | Sean McCarthy | Alex Petrov |
| [CASSANDRA-10965](https://issues.apache.org/jira/browse/CASSANDRA-10965) | Shadowable tombstones can continue to shadow view results when timestamps match |  Major | Local Write-Read Paths, Materialized Views | Carl Yeksigian |  |
| [CASSANDRA-12952](https://issues.apache.org/jira/browse/CASSANDRA-12952) | AlterTableStatement propagates base table and affected MV changes inconsistently |  Major | Distributed Metadata, Materialized Views | Aleksey Yeschenko | Andrés de la Peña |
| [CASSANDRA-13573](https://issues.apache.org/jira/browse/CASSANDRA-13573) | ColumnMetadata.cellValueType() doesn't return correct type for non-frozen collection |  Major | Core, CQL, Materialized Views, Tools | Stefano Ortolani | ZhaoYang |
| [CASSANDRA-13750](https://issues.apache.org/jira/browse/CASSANDRA-13750) | Counter digests include local data |  Minor | . | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-13640](https://issues.apache.org/jira/browse/CASSANDRA-13640) | CQLSH error when using 'login' to switch users |  Minor | CQL | Andrés de la Peña | Andrés de la Peña |
| [CASSANDRA-13622](https://issues.apache.org/jira/browse/CASSANDRA-13622) | Better config validation/documentation |  Minor | Configuration | Kurt Greaves | ZhaoYang |
| [CASSANDRA-13849](https://issues.apache.org/jira/browse/CASSANDRA-13849) | GossipStage blocks because of race in ActiveRepairService |  Major | . | Tom van der Woerdt | Sergey Lapukhov |
| [CASSANDRA-13121](https://issues.apache.org/jira/browse/CASSANDRA-13121) | repair progress message breaks legacy JMX support |  Minor | Streaming and Messaging | Scott Bale | Patrick Bannister |
| [CASSANDRA-14803](https://issues.apache.org/jira/browse/CASSANDRA-14803) | Rows that cross index block boundaries can cause incomplete reverse reads in some cases. |  Major | . | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-14823](https://issues.apache.org/jira/browse/CASSANDRA-14823) | Legacy sstables with range tombstones spanning multiple index blocks create invalid bound sequences on 3.0+ |  Major | . | Blake Eggleston | Blake Eggleston |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10632](https://issues.apache.org/jira/browse/CASSANDRA-10632) | sstableutil tests failing |  Major | . | Jim Witschey | Jim Witschey |
| [CASSANDRA-10913](https://issues.apache.org/jira/browse/CASSANDRA-10913) | netstats\_test dtest flaps |  Major | . | Jim Witschey |  |
| [CASSANDRA-10896](https://issues.apache.org/jira/browse/CASSANDRA-10896) | Fix skipping logic on upgrade tests in dtest |  Major | . | Jim Witschey | DS Test Eng |
| [CASSANDRA-10912](https://issues.apache.org/jira/browse/CASSANDRA-10912) | resumable\_bootstrap\_test dtest flaps |  Major | Testing | Jim Witschey | Yuki Morishita |


