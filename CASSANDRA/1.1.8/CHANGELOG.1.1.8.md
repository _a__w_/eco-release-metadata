
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.1.8 - 2012-12-20



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4790](https://issues.apache.org/jira/browse/CASSANDRA-4790) | Allow property override of available processors value |  Minor | . | Ahmed Bashir | Brandon Williams |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5026](https://issues.apache.org/jira/browse/CASSANDRA-5026) | Reduce log spam from counter shard warnings |  Minor | . | Jonathan Ellis | Sylvain Lebresne |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4919](https://issues.apache.org/jira/browse/CASSANDRA-4919) | StorageProxy.getRangeSlice sometimes returns incorrect number of columns |  Major | . | Piotr Kołaczkowski | Piotr Kołaczkowski |
| [CASSANDRA-4803](https://issues.apache.org/jira/browse/CASSANDRA-4803) | CFRR wide row iterators improvements |  Major | . | Piotr Kołaczkowski | Piotr Kołaczkowski |
| [CASSANDRA-5009](https://issues.apache.org/jira/browse/CASSANDRA-5009) | RangeStreamer has no way to report failures, allowing bootstrap/move etc to complete without data |  Critical | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-5008](https://issues.apache.org/jira/browse/CASSANDRA-5008) | "show schema" command in cassandra-cli generates wrong "index\_options" values. |  Minor | . | Thierry Boileau | Yuki Morishita |
| [CASSANDRA-5025](https://issues.apache.org/jira/browse/CASSANDRA-5025) | Schema push/pull race |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4984](https://issues.apache.org/jira/browse/CASSANDRA-4984) | error opening data file at startup |  Major | . | Zenek Kraweznik | Carl Yeksigian |
| [CASSANDRA-5046](https://issues.apache.org/jira/browse/CASSANDRA-5046) | cqlsh doesn't show correct timezone when SELECTing a column of type TIMESTAMP |  Minor | . | B. Todd Burruss | Aleksey Yeschenko |
| [CASSANDRA-5061](https://issues.apache.org/jira/browse/CASSANDRA-5061) | Upgraded cassandra loses all cfs on restart |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-4996](https://issues.apache.org/jira/browse/CASSANDRA-4996) | After changing the compaction strategy, compression\_strategy  always returning back to the "SnappyCompressor" through CQL 2.2.0 |  Trivial | . | Shamim Ahmed | Aleksey Yeschenko |
| [CASSANDRA-5066](https://issues.apache.org/jira/browse/CASSANDRA-5066) | Compression params validation assumes SnappyCompressor |  Trivial | . | Aleksey Yeschenko | Sylvain Lebresne |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4997](https://issues.apache.org/jira/browse/CASSANDRA-4997) | Remove multithreaded\_compaction from cassandra.yaml |  Trivial | . | Jeremy Hanna | Jonathan Ellis |


