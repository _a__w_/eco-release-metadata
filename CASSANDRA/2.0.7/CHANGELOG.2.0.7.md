
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.0.7 - 2014-04-18



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6882](https://issues.apache.org/jira/browse/CASSANDRA-6882) | Add triggers support to LWT operations |  Minor | . | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-6473](https://issues.apache.org/jira/browse/CASSANDRA-6473) | Add CQL function to generate random UUID |  Minor | . | Mikhail Mazurskiy | Carl Yeksigian |
| [CASSANDRA-6902](https://issues.apache.org/jira/browse/CASSANDRA-6902) | Make cqlsh prompt for a password if the user doesn't enter one |  Minor | Tools | J.B. Langston | J.B. Langston |
| [CASSANDRA-6311](https://issues.apache.org/jira/browse/CASSANDRA-6311) | Add CqlRecordReader to take advantage of native CQL pagination |  Major | . | Alex Liu | Alex Liu |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6331](https://issues.apache.org/jira/browse/CASSANDRA-6331) | Add LZ4Compressor in CQL document |  Trivial | Documentation and Website | Ray Wu | Mikhail Stepura |
| [CASSANDRA-6865](https://issues.apache.org/jira/browse/CASSANDRA-6865) | Logging of slices and requested count in SliceQueryFilter |  Minor | . | Oleg Anastasyev | Oleg Anastasyev |
| [CASSANDRA-5708](https://issues.apache.org/jira/browse/CASSANDRA-5708) | Add DELETE ... IF EXISTS to CQL3 |  Minor | . | Sylvain Lebresne | Tyler Hobbs |
| [CASSANDRA-6807](https://issues.apache.org/jira/browse/CASSANDRA-6807) | Thrift with CQL3 doesn't return key |  Major | . | Peter |  |
| [CASSANDRA-6876](https://issues.apache.org/jira/browse/CASSANDRA-6876) | Improve PerRowSecondaryIndex performance by removing O(N) complexity when computing indexes for a column |  Minor | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-6821](https://issues.apache.org/jira/browse/CASSANDRA-6821) | Cassandra can't delete snapshots for keyspaces that no longer exist. |  Major | . | Nick Bailey | Lyuben Todorov |
| [CASSANDRA-6867](https://issues.apache.org/jira/browse/CASSANDRA-6867) | MeteredFlusher should ignore memtables not affected by it |  Minor | . | Anthony Cozzie | Aleksey Yeschenko |
| [CASSANDRA-6659](https://issues.apache.org/jira/browse/CASSANDRA-6659) | Allow "intercepting" query by user provided custom classes |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6906](https://issues.apache.org/jira/browse/CASSANDRA-6906) | Skip Replica Calculation for Range Slice on LocalStrategy Keyspace |  Minor | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-6972](https://issues.apache.org/jira/browse/CASSANDRA-6972) | Throw an ERROR when auto\_bootstrap: true and bootstrapping node is listed in seeds |  Minor | . | Darla Baker | Brandon Williams |
| [CASSANDRA-6991](https://issues.apache.org/jira/browse/CASSANDRA-6991) | cql3 grammar generation can fail due to ANTLR timeout. |  Trivial | . | Ben Chan | Ben Chan |
| [CASSANDRA-6961](https://issues.apache.org/jira/browse/CASSANDRA-6961) | nodes should go into hibernate when join\_ring is false |  Major | . | Brandon Williams | Brandon Williams |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6789](https://issues.apache.org/jira/browse/CASSANDRA-6789) | Triggers can not be added from thrift |  Minor | . | Edward Capriolo | Sam Tunnicliffe |
| [CASSANDRA-6790](https://issues.apache.org/jira/browse/CASSANDRA-6790) | Triggers are broken in trunk because of imutable list |  Minor | . | Edward Capriolo | Sam Tunnicliffe |
| [CASSANDRA-6811](https://issues.apache.org/jira/browse/CASSANDRA-6811) | nodetool no longer shows node joining |  Minor | . | Brandon Williams | Vijay |
| [CASSANDRA-6838](https://issues.apache.org/jira/browse/CASSANDRA-6838) | FileCacheService overcounting its memoryUsage |  Minor | . | Benedict | Benedict |
| [CASSANDRA-6827](https://issues.apache.org/jira/browse/CASSANDRA-6827) | Using static with a counter column is broken |  Minor | . | Jeremiah Jordan | Aleksey Yeschenko |
| [CASSANDRA-6844](https://issues.apache.org/jira/browse/CASSANDRA-6844) | Expired cells not converted to deleted cells during cleanup/scrub/compaction (sometimes) |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6845](https://issues.apache.org/jira/browse/CASSANDRA-6845) | Cleanup fails with IndexOutOfBoundsException exception |  Major | . | Dmitrij Koniajev | Dmitrij Koniajev |
| [CASSANDRA-6779](https://issues.apache.org/jira/browse/CASSANDRA-6779) | BooleanType is not too boolean |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6803](https://issues.apache.org/jira/browse/CASSANDRA-6803) | nodetool getsstables converts key from String incorrectly |  Major | Tools | Nate McCall | Nate McCall |
| [CASSANDRA-6832](https://issues.apache.org/jira/browse/CASSANDRA-6832) | File handle leak in StreamWriter.java |  Minor | Local Write-Read Paths | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-6847](https://issues.apache.org/jira/browse/CASSANDRA-6847) | The binary transport doesn't load truststore file |  Minor | . | Mikhail Stepura | Mikhail Stepura |
| [CASSANDRA-6773](https://issues.apache.org/jira/browse/CASSANDRA-6773) | Delimiter not working for special characters in COPY command from CQLSH |  Trivial | . | Shiti Saxena | Shiti Saxena |
| [CASSANDRA-5833](https://issues.apache.org/jira/browse/CASSANDRA-5833) | Duplicate classes in Cassandra-all package. |  Major | CQL, Packaging | sam schumer | Dave Brosius |
| [CASSANDRA-6840](https://issues.apache.org/jira/browse/CASSANDRA-6840) | 2.0.{5,6} node throws EOFExceptions on the Row mutation forwarding path during rolling upgrade from 1.2.15. |  Major | . | Federico Piccinini | Marcus Eriksson |
| [CASSANDRA-6376](https://issues.apache.org/jira/browse/CASSANDRA-6376) | Pig tests are failing |  Minor | . | Sylvain Lebresne | Alex Liu |
| [CASSANDRA-6864](https://issues.apache.org/jira/browse/CASSANDRA-6864) | Proceed with truncate, if there are unreachable non storage nodes |  Major | . | Oleg Anastasyev | Oleg Anastasyev |
| [CASSANDRA-6800](https://issues.apache.org/jira/browse/CASSANDRA-6800) | ant codecoverage no longer works |  Minor | Testing | Edward Capriolo | Mikhail Stepura |
| [CASSANDRA-6837](https://issues.apache.org/jira/browse/CASSANDRA-6837) | Batch CAS does not support LOCAL\_SERIAL |  Major | . | Nicolas Favre-Felix | Sylvain Lebresne |
| [CASSANDRA-6613](https://issues.apache.org/jira/browse/CASSANDRA-6613) | Java 7u51 breaks internode encryption |  Major | . | Ray Sinnema | Brandon Williams |
| [CASSANDRA-6793](https://issues.apache.org/jira/browse/CASSANDRA-6793) | NPE in Hadoop Word count example |  Minor | . | Chander Pechetty | Chander Pechetty |
| [CASSANDRA-6646](https://issues.apache.org/jira/browse/CASSANDRA-6646) | Disk Failure Policy ignores CorruptBlockException |  Minor | . | sankalp kohli | Marcus Eriksson |
| [CASSANDRA-6514](https://issues.apache.org/jira/browse/CASSANDRA-6514) | Nodetool Refresh / CFS.loadNewSSTables() can Lose New SSTables |  Minor | Tools | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-6884](https://issues.apache.org/jira/browse/CASSANDRA-6884) | AbstractPaxosCallback throws write timeout exception with SERIAL. |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-6862](https://issues.apache.org/jira/browse/CASSANDRA-6862) | Schema versions mismatch on large ring |  Major | . | Oleg Anastasyev | Oleg Anastasyev |
| [CASSANDRA-6873](https://issues.apache.org/jira/browse/CASSANDRA-6873) | Static columns with IF NOT EXISTS don't always work as expected |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6104](https://issues.apache.org/jira/browse/CASSANDRA-6104) | Add additional limits in cassandra.conf provided by Debian package |  Trivial | Packaging | J.B. Langston | Brandon Williams |
| [CASSANDRA-6816](https://issues.apache.org/jira/browse/CASSANDRA-6816) | CQL3 docs don't mention BATCH UNLOGGED |  Minor | Documentation and Website | Duncan Sands | Tyler Hobbs |
| [CASSANDRA-6857](https://issues.apache.org/jira/browse/CASSANDRA-6857) | SELECT DISTINCT with a LIMIT is broken by paging |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6879](https://issues.apache.org/jira/browse/CASSANDRA-6879) | ConcurrentModificationException while doing range slice query. |  Major | . | Shao-Chuan Wang | Jonathan Ellis |
| [CASSANDRA-6820](https://issues.apache.org/jira/browse/CASSANDRA-6820) | NPE in MeteredFlusher.run |  Minor | . | Nicolas Favre-Felix | Nicolas Favre-Felix |
| [CASSANDRA-6168](https://issues.apache.org/jira/browse/CASSANDRA-6168) | nodetool status should issue a warning when no keyspace is specified |  Minor | Tools | Patricio Echague | Vijay |
| [CASSANDRA-6896](https://issues.apache.org/jira/browse/CASSANDRA-6896) | status output is confused when hostname resolution is enabled |  Minor | Tools | Brandon Williams | Vijay |
| [CASSANDRA-6923](https://issues.apache.org/jira/browse/CASSANDRA-6923) | Fix UnsupportedOperationException on CAS timeout |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6436](https://issues.apache.org/jira/browse/CASSANDRA-6436) | AbstractColumnFamilyInputFormat does not use start and end tokens configured via ConfigHelper.setInputRange() |  Major | Tools | Paulo Motta | Paulo Motta |
| [CASSANDRA-6907](https://issues.apache.org/jira/browse/CASSANDRA-6907) | ignore snapshot repair flag on Windows |  Major | Tools | Jonathan Ellis | Joshua McKenzie |
| [CASSANDRA-6945](https://issues.apache.org/jira/browse/CASSANDRA-6945) | Calculate liveRatio on per-memtable basis, non per-CF |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6931](https://issues.apache.org/jira/browse/CASSANDRA-6931) | BatchLogManager shouldn't serialize mutations with version 1.2 in 2.1. |  Major | . | Sylvain Lebresne | Aleksey Yeschenko |
| [CASSANDRA-6822](https://issues.apache.org/jira/browse/CASSANDRA-6822) | Never ending batch replay after dropping column family |  Major | . | Duncan Sands | Aleksey Yeschenko |
| [CASSANDRA-6958](https://issues.apache.org/jira/browse/CASSANDRA-6958) | upgradesstables does not maintain levels for existing SSTables |  Major | . | Wei Deng | Marcus Eriksson |
| [CASSANDRA-6956](https://issues.apache.org/jira/browse/CASSANDRA-6956) | SELECT ... LIMIT offset by 1 with static columns |  Major | . | Pavel Eremeev | Sylvain Lebresne |
| [CASSANDRA-6892](https://issues.apache.org/jira/browse/CASSANDRA-6892) | Cassandra 2.0.x validates Thrift columns incorrectly and causes InvalidRequestException |  Minor | CQL | Christian Spriegel | Sylvain Lebresne |
| [CASSANDRA-6966](https://issues.apache.org/jira/browse/CASSANDRA-6966) | Errors with Super Columns, mixup of 1.2 and 2.0 |  Major | . | Nicolas Lalevée | Sylvain Lebresne |
| [CASSANDRA-6893](https://issues.apache.org/jira/browse/CASSANDRA-6893) | Unintended update with conditional statement |  Major | . | Suguru Namura | Sylvain Lebresne |
| [CASSANDRA-6914](https://issues.apache.org/jira/browse/CASSANDRA-6914) | Map element is not allowed in CAS condition with DELETE/UPDATE query |  Major | . | Dmitriy Ukhlov | Sylvain Lebresne |
| [CASSANDRA-6913](https://issues.apache.org/jira/browse/CASSANDRA-6913) | Compaction of system keyspaces during startup can cause early loading of non-system keyspaces |  Minor | . | Benedict | Benedict |
| [CASSANDRA-6825](https://issues.apache.org/jira/browse/CASSANDRA-6825) | Slice Queries Can Skip Intersecting SSTables |  Major | . | Bill Mitchell | Tyler Hobbs |
| [CASSANDRA-6787](https://issues.apache.org/jira/browse/CASSANDRA-6787) | assassinate should continue when the endpoint vanishes |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-6818](https://issues.apache.org/jira/browse/CASSANDRA-6818) | SSTable references not released if stream session fails before it starts |  Major | . | Richard Low | Yuki Morishita |
| [CASSANDRA-5995](https://issues.apache.org/jira/browse/CASSANDRA-5995) | Cassandra-shuffle causes NumberFormatException |  Major | Tools | William Montaz | Lyuben Todorov |
| [CASSANDRA-6920](https://issues.apache.org/jira/browse/CASSANDRA-6920) | LatencyMetrics can return infinity |  Major | . | Nick Bailey | Benedict |
| [CASSANDRA-6948](https://issues.apache.org/jira/browse/CASSANDRA-6948) | After Bootstrap or Replace node startup, EXPIRING\_MAP\_REAPER is shutdown and cannot be restarted, causing callbacks to collect indefinitely |  Major | . | Keith Wright | Brandon Williams |
| [CASSANDRA-6984](https://issues.apache.org/jira/browse/CASSANDRA-6984) | NullPointerException in Streaming During Repair |  Blocker | . | Tyler Hobbs | Yuki Morishita |
| [CASSANDRA-6971](https://issues.apache.org/jira/browse/CASSANDRA-6971) | Schedule schema pulls onChange |  Major | . | Russ Hatch | Brandon Williams |
| [CASSANDRA-7007](https://issues.apache.org/jira/browse/CASSANDRA-7007) | Streaming Errors due to RejectedExecutionException |  Blocker | . | Tyler Hobbs | Yuki Morishita |
| [CASSANDRA-6980](https://issues.apache.org/jira/browse/CASSANDRA-6980) | Non-droppable verbs shouldn't be dropped from OTC |  Major | . | Richard Low | Jason Brown |
| [CASSANDRA-7025](https://issues.apache.org/jira/browse/CASSANDRA-7025) | RejectedExecutionException when stopping a node after drain |  Trivial | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-6841](https://issues.apache.org/jira/browse/CASSANDRA-6841) | ConcurrentModificationException in commit-log-writer after local schema reset |  Minor | . | Pas | Benedict |
| [CASSANDRA-6283](https://issues.apache.org/jira/browse/CASSANDRA-6283) | Windows 7 data files kept open / can't be deleted after compaction. |  Major | Compaction, Local Write-Read Paths | Andreas Schnitzerling | Joshua McKenzie |
| [CASSANDRA-6788](https://issues.apache.org/jira/browse/CASSANDRA-6788) | Race condition silently kills thrift server |  Major | . | Christian Rolf | Christian Rolf |


