
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1.15 - 2016-07-05



### IMPORTANT ISSUES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11574](https://issues.apache.org/jira/browse/CASSANDRA-11574) | clqsh: COPY FROM throws TypeError with Cython extensions enabled |  Major | Tools | Mahafuzur Rahman | Stefania |


### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11831](https://issues.apache.org/jira/browse/CASSANDRA-11831) | Ability to disable purgeable tombstone check via startup flag |  Major | . | Ryan Svihla | Marcus Eriksson |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11552](https://issues.apache.org/jira/browse/CASSANDRA-11552) | Reduce amount of logging calls from ColumnFamilyStore.selectAndReference |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-10828](https://issues.apache.org/jira/browse/CASSANDRA-10828) | Allow usage of multiplier in the start value of cassandra-stress population sequence |  Trivial | Tools | Eduard Tudenhoefner | Eduard Tudenhoefner |
| [CASSANDRA-10532](https://issues.apache.org/jira/browse/CASSANDRA-10532) | Allow LWT operation on static column with only partition keys |  Major | CQL | DOAN DuyHai | Carl Yeksigian |
| [CASSANDRA-11933](https://issues.apache.org/jira/browse/CASSANDRA-11933) | Cache local ranges when calculating repair neighbors |  Major | Core | Cyril Scetbon | Mahdi Mohammadinasab |
| [CASSANDRA-11576](https://issues.apache.org/jira/browse/CASSANDRA-11576) | Add support for JNA mlockall(2) on POWER |  Minor | Core | Rei Odaira | Rei Odaira |
| [CASSANDRA-11755](https://issues.apache.org/jira/browse/CASSANDRA-11755) | nodetool info should run with "readonly" jmx access |  Minor | Observability | Jérôme Mainaud | Jérôme Mainaud |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11633](https://issues.apache.org/jira/browse/CASSANDRA-11633) | cqlsh COPY FROM fails with []{} chars in UDT/tuple fields/values |  Minor | Tools | Robert Stupp | Robert Stupp |
| [CASSANDRA-11628](https://issues.apache.org/jira/browse/CASSANDRA-11628) | Fix the regression to CASSANDRA-3983 that got introduced by CASSANDRA-10679 |  Major | Tools | Wei Deng | Wei Deng |
| [CASSANDRA-9935](https://issues.apache.org/jira/browse/CASSANDRA-9935) | Repair fails with RuntimeException |  Major | . | mlowicki | Paulo Motta |
| [CASSANDRA-11630](https://issues.apache.org/jira/browse/CASSANDRA-11630) | Make cython optional in pylib/setup.py |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-11631](https://issues.apache.org/jira/browse/CASSANDRA-11631) | cqlsh COPY FROM fails for null values with non-prepared statements |  Minor | Tools | Robert Stupp | Stefania |
| [CASSANDRA-11661](https://issues.apache.org/jira/browse/CASSANDRA-11661) | Cassandra 2.0 and later require Java 7u25 or later - jdk 101 |  Critical | Testing | William Boutin | Eduard Tudenhoefner |
| [CASSANDRA-11737](https://issues.apache.org/jira/browse/CASSANDRA-11737) | Add a way to disable severity in DynamicEndpointSnitch |  Major | . | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-11644](https://issues.apache.org/jira/browse/CASSANDRA-11644) | gc\_warn\_threshold\_in\_ms is not applied when it's greater than MIN\_LOG\_DURATION(200ms) |  Minor | . | ZhaoYang | ZhaoYang |
| [CASSANDRA-11679](https://issues.apache.org/jira/browse/CASSANDRA-11679) | Cassandra Driver returns different number of results depending on fetchsize |  Major | CQL | Varun Barala | Benjamin Lerer |
| [CASSANDRA-11834](https://issues.apache.org/jira/browse/CASSANDRA-11834) | Don't compute expensive MaxPurgeableTimestamp until we've verified there's an expired tombstone |  Minor | Compaction | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-11855](https://issues.apache.org/jira/browse/CASSANDRA-11855) | MessagingService#getCommandDroppedTasks should be displayed in netstats |  Major | Tools | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-11739](https://issues.apache.org/jira/browse/CASSANDRA-11739) | Cache key references might cause OOM on incremental repair |  Major | Streaming and Messaging | Paulo Motta | Paulo Motta |
| [CASSANDRA-11848](https://issues.apache.org/jira/browse/CASSANDRA-11848) | replace address can "succeed" without actually streaming anything |  Major | Streaming and Messaging | Jeremiah Jordan | Paulo Motta |
| [CASSANDRA-11824](https://issues.apache.org/jira/browse/CASSANDRA-11824) | If repair fails no way to run repair again |  Major | . | T Jake Luciani | Marcus Eriksson |
| [CASSANDRA-11750](https://issues.apache.org/jira/browse/CASSANDRA-11750) | Offline scrub should not abort when it hits corruption |  Minor | Tools | Adam Hattrell | Yuki Morishita |
| [CASSANDRA-11152](https://issues.apache.org/jira/browse/CASSANDRA-11152) | SOURCE command in CQLSH 3.2 requires that "use keyspace" is in the cql file that you are sourcing |  Major | Tools | Francesco Animali | Stefania |
| [CASSANDRA-11055](https://issues.apache.org/jira/browse/CASSANDRA-11055) | C\*2.1 cqlsh DESCRIBE KEYSPACE ( or TABLE ) returns 'NoneType' object has no attribute 'replace' |  Major | Tools | Simon Ashley | Stefania |
| [CASSANDRA-11749](https://issues.apache.org/jira/browse/CASSANDRA-11749) | CQLSH gets SSL exception following a COPY FROM |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-11886](https://issues.apache.org/jira/browse/CASSANDRA-11886) | Streaming will miss sections for early opened sstables during compaction |  Critical | . | Stefan Podkowinski | Marcus Eriksson |
| [CASSANDRA-9842](https://issues.apache.org/jira/browse/CASSANDRA-9842) | Inconsistent behavior for '= null' conditions on static columns |  Major | CQL | Chandra Sekar | Alex Petrov |
| [CASSANDRA-11991](https://issues.apache.org/jira/browse/CASSANDRA-11991) | On clock skew, paxos may "corrupt" the node clock |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-11882](https://issues.apache.org/jira/browse/CASSANDRA-11882) | Clustering Key with ByteBuffer size \> 64k throws Assertion Error |  Major | CQL, Streaming and Messaging | Lerh Chuan Low | Lerh Chuan Low |
| [CASSANDRA-11696](https://issues.apache.org/jira/browse/CASSANDRA-11696) | Incremental repairs can mark too many ranges as repaired |  Critical | Streaming and Messaging | Joel Knighton | Marcus Eriksson |
| [CASSANDRA-12077](https://issues.apache.org/jira/browse/CASSANDRA-12077) | NPE when trying to get sstables for anticompaction |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-11854](https://issues.apache.org/jira/browse/CASSANDRA-11854) | Remove finished streaming connections from MessagingService |  Major | Streaming and Messaging | Paulo Motta | Paulo Motta |
| [CASSANDRA-12043](https://issues.apache.org/jira/browse/CASSANDRA-12043) | Syncing most recent commit in CAS across replicas can cause all CAS queries in the CQL partition to fail |  Major | . | sankalp kohli | Sylvain Lebresne |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11840](https://issues.apache.org/jira/browse/CASSANDRA-11840) | Set a more conservative default to streaming\_socket\_timeout\_in\_ms |  Major | Streaming and Messaging | Paulo Motta | Paulo Motta |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10853](https://issues.apache.org/jira/browse/CASSANDRA-10853) | deb package migration to dh\_python2 |  Major | Packaging | Michael Shuler | Michael Shuler |


