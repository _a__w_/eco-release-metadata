
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.1.11 - 2013-04-19



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5280](https://issues.apache.org/jira/browse/CASSANDRA-5280) | Provide a nodetool command to repair subranges |  Trivial | . | Ahmed Bashir | Brandon Williams |
| [CASSANDRA-5361](https://issues.apache.org/jira/browse/CASSANDRA-5361) | Enable ThreadLocal allocation in the JVM |  Major | . | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-5401](https://issues.apache.org/jira/browse/CASSANDRA-5401) | Pluggable security feature to prevent node from joining a cluster and running destructive commands |  Trivial | Configuration | Ahmed Bashir | Aleksey Yeschenko |
| [CASSANDRA-5459](https://issues.apache.org/jira/browse/CASSANDRA-5459) | Remove node from seeds list when it permanently leaves the cluster |  Minor | . | Jason Brown | Jason Brown |
| [CASSANDRA-5463](https://issues.apache.org/jira/browse/CASSANDRA-5463) | minor removal of dead code |  Trivial | . | Dave Brosius | Dave Brosius |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5080](https://issues.apache.org/jira/browse/CASSANDRA-5080) | cassandra-cli doesn't support JMX authentication. |  Major | Tools | Sergey Olefir | Michał Michalski |
| [CASSANDRA-5284](https://issues.apache.org/jira/browse/CASSANDRA-5284) | Possible assertion triggered in SliceFromReadCommand |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5236](https://issues.apache.org/jira/browse/CASSANDRA-5236) | Migration during shutdown can cause AE |  Trivial | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-5254](https://issues.apache.org/jira/browse/CASSANDRA-5254) | Nodes can be marked up after gossip sends the goodbye command |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-5315](https://issues.apache.org/jira/browse/CASSANDRA-5315) | Tests broken on Java7 |  Minor | Testing | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-4801](https://issues.apache.org/jira/browse/CASSANDRA-4801) | inet datatype does not work with cqlsh on windows |  Minor | Tools | Jonathan Ellis | Aleksey Yeschenko |
| [CASSANDRA-4021](https://issues.apache.org/jira/browse/CASSANDRA-4021) | CFS.scrubDataDirectories tries to delete nonexistent orphans |  Minor | . | Brandon Williams | Boris Yen |
| [CASSANDRA-5350](https://issues.apache.org/jira/browse/CASSANDRA-5350) | Fix ColumnFamily opening race |  Major | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-5195](https://issues.apache.org/jira/browse/CASSANDRA-5195) | Offline scrub does not migrate the directory structure on migration from 1.0.x to 1.1.x and causes the keyspace to disappear |  Major | . | Omid Aladini | Omid Aladini |
| [CASSANDRA-5052](https://issues.apache.org/jira/browse/CASSANDRA-5052) | cassandra-cli should escape keyspace name |  Trivial | Tools | Leonid Shalupov | Aleksey Yeschenko |
| [CASSANDRA-5372](https://issues.apache.org/jira/browse/CASSANDRA-5372) | Broken default values for min/max timestamp |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5242](https://issues.apache.org/jira/browse/CASSANDRA-5242) | Directories.migrateFile() does not handle -old or -tmp LDB manifests |  Minor | . | amorton | Marcus Eriksson |
| [CASSANDRA-5456](https://issues.apache.org/jira/browse/CASSANDRA-5456) | Large number of bootstrapping nodes cause gossip to stop working |  Major | . | Oleg Kibirev | Oleg Kibirev |
| [CASSANDRA-5464](https://issues.apache.org/jira/browse/CASSANDRA-5464) | use slf4j not commons-logging |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-4463](https://issues.apache.org/jira/browse/CASSANDRA-4463) | Nodes Don't Restart: Assertion Error on Serializing Cache provider |  Minor | . | Arya Goudarzi | Jonathan Ellis |


