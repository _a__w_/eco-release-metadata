
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.2.0 beta 3 - 2012-12-04



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4861](https://issues.apache.org/jira/browse/CASSANDRA-4861) | Consider separating tracing from log4j |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4731](https://issues.apache.org/jira/browse/CASSANDRA-4731) | Custom CQL transport needs to validate its framing |  Major | CQL | Tupshin Harper | Sylvain Lebresne |
| [CASSANDRA-4905](https://issues.apache.org/jira/browse/CASSANDRA-4905) | Repair should exclude gcable tombstones from merkle-tree computation |  Major | . | Christian Spriegel | Sylvain Lebresne |
| [CASSANDRA-4931](https://issues.apache.org/jira/browse/CASSANDRA-4931) | Better printing of AbstractBound in tracing |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4924](https://issues.apache.org/jira/browse/CASSANDRA-4924) | Make CQL 3 data accessible via thrift. |  Major | . | amorton | Jonathan Ellis |
| [CASSANDRA-4906](https://issues.apache.org/jira/browse/CASSANDRA-4906) | Avoid flushing other columnfamilies on truncate |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4883](https://issues.apache.org/jira/browse/CASSANDRA-4883) | Optimize mostRecentTomstone vs maxTimestamp check in CollationController.collectAllData |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4957](https://issues.apache.org/jira/browse/CASSANDRA-4957) | When bulk loading, validate SSTable's partitioner as much as possible |  Minor | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-4953](https://issues.apache.org/jira/browse/CASSANDRA-4953) | nodetool help works, but shows error first |  Trivial | Tools | Dave Brosius | Dave Brosius |
| [CASSANDRA-3151](https://issues.apache.org/jira/browse/CASSANDRA-3151) | CLI documentation should explain how to create column families with CompositeType's |  Minor | . | Ryan King | Dave Brosius |
| [CASSANDRA-3042](https://issues.apache.org/jira/browse/CASSANDRA-3042) | Implement authentication in Pig loadFunc |  Minor | . | Nate McCall | Aleksey Yeschenko |
| [CASSANDRA-4829](https://issues.apache.org/jira/browse/CASSANDRA-4829) | Make consistency level configurable in cqlsh |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-4915](https://issues.apache.org/jira/browse/CASSANDRA-4915) | CQL should prevent or warn about inefficient queries |  Major | . | Edward Capriolo | Sylvain Lebresne |
| [CASSANDRA-4994](https://issues.apache.org/jira/browse/CASSANDRA-4994) | cassandra.yaml - actually separate server\_encryption\_options and client\_encryption\_options |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-4874](https://issues.apache.org/jira/browse/CASSANDRA-4874) | Possible authorizaton handling impovements |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4879](https://issues.apache.org/jira/browse/CASSANDRA-4879) | CQL help in trunk/doc/cql3/CQL.textile outdated |  Major | . | K. B. Hahn | Eric Evans |
| [CASSANDRA-4928](https://issues.apache.org/jira/browse/CASSANDRA-4928) | CQL3 SelectStatement should not share slice query filters amongst ReadCommand |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4930](https://issues.apache.org/jira/browse/CASSANDRA-4930) | typo in ConfigHelper.java |  Trivial | . | Michael Kjellman |  |
| [CASSANDRA-4934](https://issues.apache.org/jira/browse/CASSANDRA-4934) | assertion error in offheap bloom filter |  Major | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-4935](https://issues.apache.org/jira/browse/CASSANDRA-4935) | occasional TableTest failure |  Minor | . | Brandon Williams | Yuki Morishita |
| [CASSANDRA-4940](https://issues.apache.org/jira/browse/CASSANDRA-4940) | Truncate doesn't clear row cache |  Minor | . | Jeremiah Jordan | Jonathan Ellis |
| [CASSANDRA-4919](https://issues.apache.org/jira/browse/CASSANDRA-4919) | StorageProxy.getRangeSlice sometimes returns incorrect number of columns |  Major | . | Piotr Kołaczkowski | Piotr Kołaczkowski |
| [CASSANDRA-4813](https://issues.apache.org/jira/browse/CASSANDRA-4813) | Problem using BulkOutputFormat while streaming several SSTables simultaneously from a given node. |  Minor | . | Ralph Romanos | Yuki Morishita |
| [CASSANDRA-4956](https://issues.apache.org/jira/browse/CASSANDRA-4956) | exclude system\_traces from repair |  Trivial | . | Brandon Williams | Yuki Morishita |
| [CASSANDRA-4384](https://issues.apache.org/jira/browse/CASSANDRA-4384) | HintedHandoff can begin before SS knows the hostID |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-4929](https://issues.apache.org/jira/browse/CASSANDRA-4929) | Deal with broken ALTER DROP support in CQL3 |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4969](https://issues.apache.org/jira/browse/CASSANDRA-4969) | Set max frame size in CLI to avoid OOM when SSL is enabled |  Trivial | Tools | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-4968](https://issues.apache.org/jira/browse/CASSANDRA-4968) | CQL3 Descrepancies |  Minor | . | T Jake Luciani |  |
| [CASSANDRA-4970](https://issues.apache.org/jira/browse/CASSANDRA-4970) | cqlsh renders bytes fields as strings |  Minor | . | Jonathan Ellis | Aleksey Yeschenko |
| [CASSANDRA-4965](https://issues.apache.org/jira/browse/CASSANDRA-4965) | in cqlsh, alter table with compaction\_strategy\_class does nothing |  Trivial | . | Jeremy Hanna | Aleksey Yeschenko |
| [CASSANDRA-4918](https://issues.apache.org/jira/browse/CASSANDRA-4918) | Remove CQL3 arbitrary select limit |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4945](https://issues.apache.org/jira/browse/CASSANDRA-4945) | CQL3 does handle List append or prepend with a "Prepared" list |  Minor | . | Rick Shaw | Sylvain Lebresne |
| [CASSANDRA-4877](https://issues.apache.org/jira/browse/CASSANDRA-4877) | Range queries return fewer result after a lot of delete |  Major | . | julien campan | Sylvain Lebresne |
| [CASSANDRA-4979](https://issues.apache.org/jira/browse/CASSANDRA-4979) | Stress for cql3 is broken on 1.2/trunk |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4893](https://issues.apache.org/jira/browse/CASSANDRA-4893) | Don't throw internal exceptions over JMX |  Major | . | Nick Bailey | Yuki Morishita |
| [CASSANDRA-4982](https://issues.apache.org/jira/browse/CASSANDRA-4982) | cqlsh: alter table add column with table that has collection fails |  Major | . | Dave Brosius | Sylvain Lebresne |
| [CASSANDRA-4990](https://issues.apache.org/jira/browse/CASSANDRA-4990) | Do not allow use of collection with compact storage |  Minor | . | Yuki Morishita | Sylvain Lebresne |
| [CASSANDRA-4992](https://issues.apache.org/jira/browse/CASSANDRA-4992) | TTL/WRITETIME function against collection column returns invalid value |  Minor | . | Yuki Morishita | Sylvain Lebresne |
| [CASSANDRA-4975](https://issues.apache.org/jira/browse/CASSANDRA-4975) | AE in Bounds when running word count |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-4843](https://issues.apache.org/jira/browse/CASSANDRA-4843) | When upgrading from 1.1.6 to 1.20 change in partitioner causes nodes not to start |  Major | . | Edward Capriolo | Jonathan Ellis |
| [CASSANDRA-5001](https://issues.apache.org/jira/browse/CASSANDRA-5001) | Generated time-based UUID don't conform to the spec |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4955](https://issues.apache.org/jira/browse/CASSANDRA-4955) | some confusion around KEY pseudocolumn from Thrift tables |  Minor | . | Jonathan Ellis | Aleksey Yeschenko |
| [CASSANDRA-5009](https://issues.apache.org/jira/browse/CASSANDRA-5009) | RangeStreamer has no way to report failures, allowing bootstrap/move etc to complete without data |  Critical | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-4860](https://issues.apache.org/jira/browse/CASSANDRA-4860) | Estimated Row Cache Entry size incorrect (always 24?) |  Major | . | Chris Burroughs | Vijay |
| [CASSANDRA-4880](https://issues.apache.org/jira/browse/CASSANDRA-4880) | Endless loop flushing+compacting system/schema\_keyspaces and system/schema\_columnfamilies |  Major | . | Mina Naguib | Pavel Yaskevich |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4941](https://issues.apache.org/jira/browse/CASSANDRA-4941) | Move CompressionMetadata off-heap |  Minor | . | Jonathan Ellis | Jonathan Ellis |


