
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.8.3 - 2011-08-08



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2924](https://issues.apache.org/jira/browse/CASSANDRA-2924) | Consolidate JDBC driver classes: Connection and CassandraConnection in advance of feature additions for 1.1 |  Minor | . | Rick Shaw | Rick Shaw |
| [CASSANDRA-2943](https://issues.apache.org/jira/browse/CASSANDRA-2943) | add ability to drop local tasks that will timeout anyway |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2953](https://issues.apache.org/jira/browse/CASSANDRA-2953) | Use lazy initialization instead of class initialization in NodeId |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-1405](https://issues.apache.org/jira/browse/CASSANDRA-1405) | Switch to THsHaServer, redux |  Minor | CQL | Jonathan Ellis | Vijay |
| [CASSANDRA-2850](https://issues.apache.org/jira/browse/CASSANDRA-2850) | Converting bytes to hex string is unnecessarily slow |  Minor | . | David Allsopp | David Allsopp |
| [CASSANDRA-2971](https://issues.apache.org/jira/browse/CASSANDRA-2971) | Append (not add new) InetAddress info logging when starting MessagingService |  Minor | . | Jackson Chung | Jackson Chung |
| [CASSANDRA-2965](https://issues.apache.org/jira/browse/CASSANDRA-2965) | Allow cassandra to start on a Solaris machine. |  Trivial | Packaging | Kjell Andreassen | Kjell Andreassen |
| [CASSANDRA-2980](https://issues.apache.org/jira/browse/CASSANDRA-2980) | Replace "system" in StorageService with Table.SYSTEM\_TABLE |  Trivial | . | Norman Maurer | Norman Maurer |
| [CASSANDRA-2981](https://issues.apache.org/jira/browse/CASSANDRA-2981) | Provide Hadoop read access to Counter Columns. |  Minor | . | amorton | amorton |
| [CASSANDRA-2979](https://issues.apache.org/jira/browse/CASSANDRA-2979) | Skip system table when repair |  Major | . | Norman Maurer | Norman Maurer |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2929](https://issues.apache.org/jira/browse/CASSANDRA-2929) | Don't include tmp files as sstable when create column families |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2496](https://issues.apache.org/jira/browse/CASSANDRA-2496) | Gossip should handle 'dead' states |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-2946](https://issues.apache.org/jira/browse/CASSANDRA-2946) | HintedHandoff fails with could not reach schema agreement |  Major | . | Richard Low | Jonathan Ellis |
| [CASSANDRA-2933](https://issues.apache.org/jira/browse/CASSANDRA-2933) | nodetool hangs (doesn't return prompt) if you specify a table that doesn't exist or a KS that has no CF's |  Minor | Tools | Cathy Daw | Yuki Morishita |
| [CASSANDRA-2717](https://issues.apache.org/jira/browse/CASSANDRA-2717) | duplicate rows returned from SELECT where KEY term is duplicated |  Minor | . | amorton | Jim Ancona |
| [CASSANDRA-2951](https://issues.apache.org/jira/browse/CASSANDRA-2951) | FreeableMemory can be accessed after it is invalid |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2948](https://issues.apache.org/jira/browse/CASSANDRA-2948) | Nodetool move fails to stream out data from moved node to new endpoint. |  Critical | . | Sam Overton | Sam Overton |
| [CASSANDRA-2829](https://issues.apache.org/jira/browse/CASSANDRA-2829) | memtable with no post-flush activity can leave commitlog permanently dirty |  Critical | . | amorton | Sylvain Lebresne |
| [CASSANDRA-2949](https://issues.apache.org/jira/browse/CASSANDRA-2949) | Batch mutation of counters in multiple supercolumns throws an exception during replication. |  Critical | . | Sam Overton | Sylvain Lebresne |
| [CASSANDRA-2825](https://issues.apache.org/jira/browse/CASSANDRA-2825) | Auto bootstrapping the 4th node in a 4 node cluster doesn't work, when no token explicitly assigned in config. |  Major | . | Michael Allen | Brandon Williams |
| [CASSANDRA-2960](https://issues.apache.org/jira/browse/CASSANDRA-2960) | replication\_factor \> 1 always causes cassandra to return null |  Minor | . | Steve Corona | Jonathan Ellis |
| [CASSANDRA-2958](https://issues.apache.org/jira/browse/CASSANDRA-2958) | Flush memtables on shutdown when durable writes are disabled |  Major | . | David Phillips | Jonathan Ellis |
| [CASSANDRA-2983](https://issues.apache.org/jira/browse/CASSANDRA-2983) | CliClient print memtable threshold in incorrect order |  Minor | . | Jackson Chung |  |
| [CASSANDRA-2968](https://issues.apache.org/jira/browse/CASSANDRA-2968) | AssertionError during compaction of CF with counter columns |  Major | . | Taras Puchko | Sylvain Lebresne |
| [CASSANDRA-2867](https://issues.apache.org/jira/browse/CASSANDRA-2867) | Starting 0.8.1 after upgrade from 0.7.6-2 fails |  Major | . | Yaniv Kunda | Jonathan Ellis |


