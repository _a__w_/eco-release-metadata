
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.2.0 rc2 - 2015-07-09



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9229](https://issues.apache.org/jira/browse/CASSANDRA-9229) | Add functions to convert timeuuid to date or time |  Major | CQL | Michaël Figuière | Benjamin Lerer |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9529](https://issues.apache.org/jira/browse/CASSANDRA-9529) | Standardize quoting in Unix scripts |  Minor | Tools | Stefania | Stefania |
| [CASSANDRA-9571](https://issues.apache.org/jira/browse/CASSANDRA-9571) | Set HAS\_MORE\_PAGES flag when PagingState is null |  Minor | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-9378](https://issues.apache.org/jira/browse/CASSANDRA-9378) | Instrument the logger with error count metrics by level |  Minor | Tools | Jonathan Shook | Yuki Morishita |
| [CASSANDRA-9496](https://issues.apache.org/jira/browse/CASSANDRA-9496) | ArrivalWindow should use primitives |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-9475](https://issues.apache.org/jira/browse/CASSANDRA-9475) | Duplicate compilation of UDFs on coordinator |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-9563](https://issues.apache.org/jira/browse/CASSANDRA-9563) | Rename class for DATE type in Java driver |  Minor | . | Olivier Michallat | Robert Stupp |
| [CASSANDRA-9532](https://issues.apache.org/jira/browse/CASSANDRA-9532) | Provide access to select statement's real column definitions |  Major | CQL | mck | Sam Tunnicliffe |
| [CASSANDRA-9634](https://issues.apache.org/jira/browse/CASSANDRA-9634) | Set kernel timer resolution on Windows |  Major | Lifecycle, Local Write-Read Paths | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-7556](https://issues.apache.org/jira/browse/CASSANDRA-7556) | Update cqlsh for UDFs |  Major | Tools | Tyler Hobbs | Robert Stupp |
| [CASSANDRA-9090](https://issues.apache.org/jira/browse/CASSANDRA-9090) | Allow JMX over SSL directly from nodetool |  Major | Tools | Philip Thompson |  |
| [CASSANDRA-7814](https://issues.apache.org/jira/browse/CASSANDRA-7814) | enable describe on indices |  Minor | Tools | radha | Stefania |
| [CASSANDRA-9601](https://issues.apache.org/jira/browse/CASSANDRA-9601) | Allow an initial connection timeout to be set in cqlsh |  Major | Tools | Mike Adamson | Stefania |
| [CASSANDRA-9603](https://issues.apache.org/jira/browse/CASSANDRA-9603) | Expose private listen\_address in system.local |  Major | Distributed Metadata | Piotr Kołaczkowski | Carl Yeksigian |
| [CASSANDRA-9658](https://issues.apache.org/jira/browse/CASSANDRA-9658) | Re-enable memory-mapped index file reads on Windows |  Major | Local Write-Read Paths | Joshua McKenzie | Joshua McKenzie |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9240](https://issues.apache.org/jira/browse/CASSANDRA-9240) | Performance issue after a restart |  Minor | . | Alan Boudreault | Benedict |
| [CASSANDRA-9566](https://issues.apache.org/jira/browse/CASSANDRA-9566) | Running upgradesstables on 2.2 fails |  Major | . | Carl Yeksigian | Marcus Eriksson |
| [CASSANDRA-9569](https://issues.apache.org/jira/browse/CASSANDRA-9569) | nodetool should exit with status code != 0 if NodeProbe fails |  Major | Tools | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9570](https://issues.apache.org/jira/browse/CASSANDRA-9570) | Deprecated forceRepairAsync methods in StorageService do not work |  Major | Observability | Mike Adamson | Yuki Morishita |
| [CASSANDRA-9573](https://issues.apache.org/jira/browse/CASSANDRA-9573) | OOM when loading sstables (system.hints) |  Critical | Coordination | Alan Boudreault | Sam Tunnicliffe |
| [CASSANDRA-9572](https://issues.apache.org/jira/browse/CASSANDRA-9572) | DateTieredCompactionStrategy fails to combine SSTables correctly when TTL is used. |  Major | Compaction | Antti Nissinen | Marcus Eriksson |
| [CASSANDRA-9567](https://issues.apache.org/jira/browse/CASSANDRA-9567) | Windows does not handle ipv6 addresses |  Major | . | Philip Thompson | Philip Thompson |
| [CASSANDRA-9576](https://issues.apache.org/jira/browse/CASSANDRA-9576) | Connection leak in CQLRecordWriter |  Critical | . | T Meyarivan | Philip Thompson |
| [CASSANDRA-9119](https://issues.apache.org/jira/browse/CASSANDRA-9119) | Nodetool rebuild creates an additional rebuild session even if there is one already running |  Major | Tools | Jose Martinez Poblete | Yuki Morishita |
| [CASSANDRA-9622](https://issues.apache.org/jira/browse/CASSANDRA-9622) | count/max/min aggregates not created for the blob type |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-9621](https://issues.apache.org/jira/browse/CASSANDRA-9621) | Repair of the SystemDistributed keyspace creates a non-trivial amount of memory pressure |  Minor | . | Sylvain Lebresne | Marcus Eriksson |
| [CASSANDRA-9565](https://issues.apache.org/jira/browse/CASSANDRA-9565) | 'WITH WITH' in alter keyspace statements causes NPE |  Major | . | Jim Witschey | Benjamin Lerer |
| [CASSANDRA-9503](https://issues.apache.org/jira/browse/CASSANDRA-9503) | Update CQL doc reflecting current keywords |  Trivial | Documentation and Website | Adam Holmberg | Benjamin Lerer |
| [CASSANDRA-9540](https://issues.apache.org/jira/browse/CASSANDRA-9540) | Cql IN query wrong on rows with values bigger than 64kb |  Major | CQL | Mathijs Vogelzang | Carl Yeksigian |
| [CASSANDRA-9558](https://issues.apache.org/jira/browse/CASSANDRA-9558) | Cassandra-stress regression in 2.2 |  Major | . | Alan Boudreault | Andy Tolbert |
| [CASSANDRA-9559](https://issues.apache.org/jira/browse/CASSANDRA-9559) | IndexOutOfBoundsException inserting into TupleType |  Major | CQL | dan jatnieks | Benjamin Lerer |
| [CASSANDRA-9527](https://issues.apache.org/jira/browse/CASSANDRA-9527) | Cannot create secondary index on a table WITH COMPACT STORAGE |  Minor | CQL, Secondary Indexes | fuggy\_yama | Benjamin Lerer |
| [CASSANDRA-9385](https://issues.apache.org/jira/browse/CASSANDRA-9385) | cqlsh autocomplete does not work for DTCS min\_threshold |  Trivial | . | Sebastian Estevez | Alex Buck |
| [CASSANDRA-9631](https://issues.apache.org/jira/browse/CASSANDRA-9631) | Unnecessary required filtering for query on indexed clustering key |  Major | CQL | Kevin Deldycke | Benjamin Lerer |
| [CASSANDRA-9649](https://issues.apache.org/jira/browse/CASSANDRA-9649) | Paxos ballot in StorageProxy could clash |  Minor | Coordination | Stefania | Stefania |
| [CASSANDRA-9675](https://issues.apache.org/jira/browse/CASSANDRA-9675) | BulkLoader has --transport-factory option but does not use it |  Minor | Tools | Mike Adamson | Mike Adamson |
| [CASSANDRA-9680](https://issues.apache.org/jira/browse/CASSANDRA-9680) | Update CQL version |  Major | . | Sylvain Lebresne | Benjamin Lerer |
| [CASSANDRA-9064](https://issues.apache.org/jira/browse/CASSANDRA-9064) | [LeveledCompactionStrategy] cqlsh can't run cql produced by its own describe table statement |  Major | CQL | Sujeet Gholap | Benjamin Lerer |
| [CASSANDRA-9688](https://issues.apache.org/jira/browse/CASSANDRA-9688) | Wrong CQL version in cqlsh prevent the client to connect to the server |  Major | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-9560](https://issues.apache.org/jira/browse/CASSANDRA-9560) | Changing durable\_writes on a keyspace is only applied after restart of node |  Major | Distributed Metadata | Fred A | Carl Yeksigian |
| [CASSANDRA-9693](https://issues.apache.org/jira/browse/CASSANDRA-9693) | cqlsh DESCRIBE KEYSPACES reporting error 'None not found in keyspaces' after keyspaces |  Major | Tools | Mike Adamson | Mike Adamson |
| [CASSANDRA-9681](https://issues.apache.org/jira/browse/CASSANDRA-9681) | Memtable heap size grows and many long GC pauses are triggered |  Critical | . | mlowicki | Benedict |
| [CASSANDRA-9636](https://issues.apache.org/jira/browse/CASSANDRA-9636) | Duplicate columns in selection causes AssertionError |  Major | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-9687](https://issues.apache.org/jira/browse/CASSANDRA-9687) | Wrong partitioner after upgrading sstables (secondary indexes are not handled correctly after CASSANDRA-6962) |  Blocker | Local Write-Read Paths, Secondary Indexes | Andreas Schnitzerling | Yuki Morishita |
| [CASSANDRA-9643](https://issues.apache.org/jira/browse/CASSANDRA-9643) | Warn when an extra-large partition is compacted |  Major | Compaction | Jonathan Ellis | Stefania |
| [CASSANDRA-9726](https://issues.apache.org/jira/browse/CASSANDRA-9726) | Built in aggregate docs do not display examples due to whitespace error |  Major | Documentation and Website | Christopher Batey | Christopher Batey |
| [CASSANDRA-9725](https://issues.apache.org/jira/browse/CASSANDRA-9725) | CQL docs do not build due to duplicate name |  Major | Documentation and Website | Christopher Batey | Christopher Batey |
| [CASSANDRA-9727](https://issues.apache.org/jira/browse/CASSANDRA-9727) | AuthSuccess NPE |  Major | . | Pierre N. |  |
| [CASSANDRA-9647](https://issues.apache.org/jira/browse/CASSANDRA-9647) | Tables created by cassandra-stress are omitted in DESCRIBE KEYSPACE |  Minor | . | Ryan McGuire | Tyler Hobbs |
| [CASSANDRA-9542](https://issues.apache.org/jira/browse/CASSANDRA-9542) | Create UDA does not recognize built-in functions without specifying system keyspace |  Minor | . | Zachary Kurey | Robert Stupp |
| [CASSANDRA-7973](https://issues.apache.org/jira/browse/CASSANDRA-7973) | cqlsh connect error "member\_descriptor' object is not callable" |  Minor | Tools | Digant Modha | Stefania |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9583](https://issues.apache.org/jira/browse/CASSANDRA-9583) | test-compression could run multiple unit tests in parallel like test |  Major | Testing | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-9523](https://issues.apache.org/jira/browse/CASSANDRA-9523) | RangeTombstoneTest.testOverwritesToDeletedColumns failed in 2.2 |  Major | Testing | Michael Shuler | Ariel Weisberg |
| [CASSANDRA-9346](https://issues.apache.org/jira/browse/CASSANDRA-9346) | Expand upgrade testing for commitlog changes |  Major | Testing | Philip Thompson | Branimir Lambov |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9618](https://issues.apache.org/jira/browse/CASSANDRA-9618) | Consider deprecating sstable2json/json2sstable in 2.2 |  Major | . | Sylvain Lebresne | Sylvain Lebresne |


