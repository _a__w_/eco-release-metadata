
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.6.9 - 2011-01-09



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1698](https://issues.apache.org/jira/browse/CASSANDRA-1698) | nodetool cfhistograms |  Major | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1812](https://issues.apache.org/jira/browse/CASSANDRA-1812) | Allow per-CF compaction, repair, and cleanup |  Minor | . | Tyler Hobbs | Jon Hermes |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1723](https://issues.apache.org/jira/browse/CASSANDRA-1723) | Add missing .bat files for tools |  Minor | . | Vladimir Loncar | Vladimir Loncar |
| [CASSANDRA-1553](https://issues.apache.org/jira/browse/CASSANDRA-1553) | warn of imbalanced ranges in nodetool ring |  Minor | Tools | Jonathan Ellis | Jon Hermes |
| [CASSANDRA-1789](https://issues.apache.org/jira/browse/CASSANDRA-1789) | Clean up (and make sane) key/row cache loading logspam |  Minor | . | Jon Hermes | Matthew F. Dennis |
| [CASSANDRA-1811](https://issues.apache.org/jira/browse/CASSANDRA-1811) | Cleanup smallest CFs first |  Minor | . | Paul Querna | Jon Hermes |
| [CASSANDRA-1867](https://issues.apache.org/jira/browse/CASSANDRA-1867) | sstable2json runs out of memory when trying to export huge rows |  Minor | Tools | Ilya Maykov | Ilya Maykov |
| [CASSANDRA-1878](https://issues.apache.org/jira/browse/CASSANDRA-1878) | Minimize Key Cache Invalidation by Compaction |  Major | . | Tyler Hobbs | Jonathan Ellis |
| [CASSANDRA-1370](https://issues.apache.org/jira/browse/CASSANDRA-1370) | TokenMetaData.getPendingRangesMM() is unnecessarily synchronized |  Minor | . | Jason Fager | Brandon Williams |
| [CASSANDRA-1926](https://issues.apache.org/jira/browse/CASSANDRA-1926) | Add Ant target to generate the project description files for running Cassandra in Eclipse |  Minor | . | Zhijie Shen | T Jake Luciani |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1590](https://issues.apache.org/jira/browse/CASSANDRA-1590) | Pig loadfunc fails with java.io.FileNotFoundException: ...:.../job.jar!/storage-conf.xml |  Major | . | Tommi Virtanen |  |
| [CASSANDRA-1739](https://issues.apache.org/jira/browse/CASSANDRA-1739) | bootstrapping nodes do not reject range slice requests |  Major | . | Randall Leeds | Randall Leeds |
| [CASSANDRA-1700](https://issues.apache.org/jira/browse/CASSANDRA-1700) | mapreduce support is broken |  Major | . | Jeremy Hanna | Stu Hood |
| [CASSANDRA-1674](https://issues.apache.org/jira/browse/CASSANDRA-1674) | Repair using abnormally large amounts of disk space |  Major | . | Jonathan Ellis | Stu Hood |
| [CASSANDRA-1729](https://issues.apache.org/jira/browse/CASSANDRA-1729) | Fix misuse of DataOutputBuffer.getData in AntiEntropyService |  Minor | . | Stu Hood | Stu Hood |
| [CASSANDRA-1760](https://issues.apache.org/jira/browse/CASSANDRA-1760) | JNA Native check can throw a NoSuchMethodError |  Minor | . | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-1591](https://issues.apache.org/jira/browse/CASSANDRA-1591) | get\_range\_slices always returns super columns that's been removed/restored, regardless of count value in slicerange |  Minor | . | Jianing hu | Jonathan Ellis |
| [CASSANDRA-1752](https://issues.apache.org/jira/browse/CASSANDRA-1752) | repair leaving FDs unclosed |  Major | . | Jonathan Ellis | Tyler Hobbs |
| [CASSANDRA-1781](https://issues.apache.org/jira/browse/CASSANDRA-1781) | Internal error processing get\_range\_slices |  Major | . | Patrik Modesto | Stu Hood |
| [CASSANDRA-1730](https://issues.apache.org/jira/browse/CASSANDRA-1730) | Fat clients are never removed |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-1408](https://issues.apache.org/jira/browse/CASSANDRA-1408) | nodetool drain attempts to delete a deleted file |  Minor | . | Jon Hermes | Brandon Williams |
| [CASSANDRA-1830](https://issues.apache.org/jira/browse/CASSANDRA-1830) | ReadResponseResolver might miss an inconsistency |  Minor | . | Jonathan Ellis | Randall Leeds |
| [CASSANDRA-1866](https://issues.apache.org/jira/browse/CASSANDRA-1866) | Internal error from malformed remove with supercolumns |  Minor | CQL | Jonathan Ellis | Tyler Hobbs |
| [CASSANDRA-1873](https://issues.apache.org/jira/browse/CASSANDRA-1873) | Read Repair behavior thwarts DynamicEndpointSnitch at CL.ONE |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1883](https://issues.apache.org/jira/browse/CASSANDRA-1883) | NPE in get\_slice quorum read |  Minor | . | Karl Mueller | Jonathan Ellis |
| [CASSANDRA-1766](https://issues.apache.org/jira/browse/CASSANDRA-1766) | Streaming never makes progress |  Major | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-1905](https://issues.apache.org/jira/browse/CASSANDRA-1905) | count timeouts towards dynamicsnitch latencies |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1910](https://issues.apache.org/jira/browse/CASSANDRA-1910) | validation of time uuid is incorrect |  Minor | . | Dave | Gary Dusbabek |
| [CASSANDRA-1901](https://issues.apache.org/jira/browse/CASSANDRA-1901) | getRestrictedRanges bug where node owns minimum token |  Major | . | Jonathan Ellis | Stu Hood |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1265](https://issues.apache.org/jira/browse/CASSANDRA-1265) | make getBuckets deterministic |  Minor | . | Jonathan Ellis | Tyler Hobbs |


