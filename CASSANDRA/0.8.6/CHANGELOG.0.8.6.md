
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.8.6 - 2011-09-20



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2890](https://issues.apache.org/jira/browse/CASSANDRA-2890) | Randomize (to some extend) the choice of the first replica for counter increment |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3041](https://issues.apache.org/jira/browse/CASSANDRA-3041) | Move streams data to too many nodes. |  Minor | . | Nick Bailey | Nick Bailey |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3138](https://issues.apache.org/jira/browse/CASSANDRA-3138) | PropertyFileSnitch's ResourceWatcher fails because it uses FBUtilities.resourceToFile(..) while PropertyFileSnitch uses classloader.getResourceAsStream(..) |  Minor | . | mck | mck |
| [CASSANDRA-3139](https://issues.apache.org/jira/browse/CASSANDRA-3139) | Prevent users from creating keyspaces with LocalStrategy replication |  Minor | CQL | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-3129](https://issues.apache.org/jira/browse/CASSANDRA-3129) | "show schema" in CLI outputs invalid text structure that cannot be replayed (easily tweakable though) |  Minor | Tools | Evaldo Gardenali | Pavel Yaskevich |
| [CASSANDRA-3142](https://issues.apache.org/jira/browse/CASSANDRA-3142) | CustomTThreadPoolServer should log TTransportException at DEBUG level |  Minor | . | Jim Ancona | Jim Ancona |
| [CASSANDRA-3152](https://issues.apache.org/jira/browse/CASSANDRA-3152) | Logic of AbstractNetworkTopologySnitch.compareEndpoints is wrong |  Minor | . | Vijay | Vijay |
| [CASSANDRA-3158](https://issues.apache.org/jira/browse/CASSANDRA-3158) | Improve caching of same-version Messages on digest and repair paths |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3160](https://issues.apache.org/jira/browse/CASSANDRA-3160) | PIG\_OPTS bash variable interpolation doesn't work correctly when PIG\_OPTS is set in the environment. |  Minor | . | Eldon Stegall |  |
| [CASSANDRA-3166](https://issues.apache.org/jira/browse/CASSANDRA-3166) | Rolling upgrades from 0.7 to 0.8 not possible |  Major | . | Marcus Eriksson |  |
| [CASSANDRA-3202](https://issues.apache.org/jira/browse/CASSANDRA-3202) | CFMetata.getMergeShardChance returns readRepairChance |  Trivial | . | Sylvain Lebresne |  |
| [CASSANDRA-3187](https://issues.apache.org/jira/browse/CASSANDRA-3187) | Return both listen\_address and rpc\_address through describe\_ring |  Minor | . | Nick Bailey | Nick Bailey |
| [CASSANDRA-3164](https://issues.apache.org/jira/browse/CASSANDRA-3164) | GCInspector still not avoiding divide by zero |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3195](https://issues.apache.org/jira/browse/CASSANDRA-3195) | Cassandra-CLI does not allow "Config" as column family name |  Minor | Tools | Mauri Tikka | Pavel Yaskevich |
| [CASSANDRA-3212](https://issues.apache.org/jira/browse/CASSANDRA-3212) | Accomodate missing encryption\_options in IncomingTcpConnection.stream |  Trivial | . | Jonathan Ellis | Jonathan Ellis |


