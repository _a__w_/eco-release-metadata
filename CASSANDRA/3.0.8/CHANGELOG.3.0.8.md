
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.0.8 - 2016-07-05



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11327](https://issues.apache.org/jira/browse/CASSANDRA-11327) | Maintain a histogram of times when writes are blocked due to no available memory |  Major | Core | Ariel Weisberg | Ariel Weisberg |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9666](https://issues.apache.org/jira/browse/CASSANDRA-9666) | Provide an alternative to DTCS |  Major | Compaction | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-10532](https://issues.apache.org/jira/browse/CASSANDRA-10532) | Allow LWT operation on static column with only partition keys |  Major | CQL | DOAN DuyHai | Carl Yeksigian |
| [CASSANDRA-11933](https://issues.apache.org/jira/browse/CASSANDRA-11933) | Cache local ranges when calculating repair neighbors |  Major | Core | Cyril Scetbon | Mahdi Mohammadinasab |
| [CASSANDRA-11576](https://issues.apache.org/jira/browse/CASSANDRA-11576) | Add support for JNA mlockall(2) on POWER |  Minor | Core | Rei Odaira | Rei Odaira |
| [CASSANDRA-11798](https://issues.apache.org/jira/browse/CASSANDRA-11798) | Allow specification of 'time' column value as number in CQL query. |  Minor | CQL | Andy Tolbert | Alex Petrov |
| [CASSANDRA-11755](https://issues.apache.org/jira/browse/CASSANDRA-11755) | nodetool info should run with "readonly" jmx access |  Minor | Observability | Jérôme Mainaud | Jérôme Mainaud |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11984](https://issues.apache.org/jira/browse/CASSANDRA-11984) | StorageService shutdown hook should use a volatile variable |  Major | Core | Edward Capriolo | Edward Capriolo |
| [CASSANDRA-11749](https://issues.apache.org/jira/browse/CASSANDRA-11749) | CQLSH gets SSL exception following a COPY FROM |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-11886](https://issues.apache.org/jira/browse/CASSANDRA-11886) | Streaming will miss sections for early opened sstables during compaction |  Critical | . | Stefan Podkowinski | Marcus Eriksson |
| [CASSANDRA-11604](https://issues.apache.org/jira/browse/CASSANDRA-11604) | select on table fails after changing user defined type in map |  Major | CQL | Andreas Jaekle | Alex Petrov |
| [CASSANDRA-11481](https://issues.apache.org/jira/browse/CASSANDRA-11481) | Example metrics config has DroppedMetrics |  Minor | Documentation and Website | Christopher Batey | Christopher Batey |
| [CASSANDRA-11948](https://issues.apache.org/jira/browse/CASSANDRA-11948) | Wrong ByteBuffer comparisons in TimeTypeTest |  Minor | Testing | Rei Odaira | Rei Odaira |
| [CASSANDRA-11038](https://issues.apache.org/jira/browse/CASSANDRA-11038) | Is node being restarted treated as node joining? |  Minor | Distributed Metadata | cheng ren | Sam Tunnicliffe |
| [CASSANDRA-12023](https://issues.apache.org/jira/browse/CASSANDRA-12023) | Schema upgrade bug with super columns |  Critical | Distributed Metadata | Jeremiah Jordan | Aleksey Yeschenko |
| [CASSANDRA-9842](https://issues.apache.org/jira/browse/CASSANDRA-9842) | Inconsistent behavior for '= null' conditions on static columns |  Major | CQL | Chandra Sekar | Alex Petrov |
| [CASSANDRA-11920](https://issues.apache.org/jira/browse/CASSANDRA-11920) | bloom\_filter\_fp\_chance needs to be validated up front |  Minor | Lifecycle, Local Write-Read Paths | ADARSH KUMAR | Arindam Gupta |
| [CASSANDRA-11991](https://issues.apache.org/jira/browse/CASSANDRA-11991) | On clock skew, paxos may "corrupt" the node clock |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-11913](https://issues.apache.org/jira/browse/CASSANDRA-11913) | BufferUnderFlowException in CompressorTest |  Minor | Testing | Rei Odaira | Rei Odaira |
| [CASSANDRA-11882](https://issues.apache.org/jira/browse/CASSANDRA-11882) | Clustering Key with ByteBuffer size \> 64k throws Assertion Error |  Major | CQL, Streaming and Messaging | Lerh Chuan Low | Lerh Chuan Low |
| [CASSANDRA-11696](https://issues.apache.org/jira/browse/CASSANDRA-11696) | Incremental repairs can mark too many ranges as repaired |  Critical | Streaming and Messaging | Joel Knighton | Marcus Eriksson |
| [CASSANDRA-12077](https://issues.apache.org/jira/browse/CASSANDRA-12077) | NPE when trying to get sstables for anticompaction |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-11878](https://issues.apache.org/jira/browse/CASSANDRA-11878) | dtest failure in upgrade\_tests.cql\_tests.TestCQLNodes2RF1\_Upgrade\_current\_3\_x\_To\_indev\_3\_x.select\_key\_in\_test |  Major | . | Sean McCarthy | Marcus Eriksson |
| [CASSANDRA-12083](https://issues.apache.org/jira/browse/CASSANDRA-12083) | Race condition during system.roles column family creation |  Major | Core | Sharvanath Pathak | Sam Tunnicliffe |
| [CASSANDRA-11854](https://issues.apache.org/jira/browse/CASSANDRA-11854) | Remove finished streaming connections from MessagingService |  Major | Streaming and Messaging | Paulo Motta | Paulo Motta |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12070](https://issues.apache.org/jira/browse/CASSANDRA-12070) | dtest failure in cqlsh\_tests.cqlsh\_copy\_tests.CqlshCopyTest.test\_data\_validation\_on\_read\_template |  Major | Tools | Sean McCarthy | Tyler Hobbs |


