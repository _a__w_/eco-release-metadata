
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.4 - 2016-03-08



### IMPORTANT ISSUES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10428](https://issues.apache.org/jira/browse/CASSANDRA-10428) | cqlsh: Include sub-second precision in timestamps by default |  Major | Tools | Chandran Anjur Narasimhan | Stefania |


### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10660](https://issues.apache.org/jira/browse/CASSANDRA-10660) | Support user-defined compactions through nodetool |  Minor | Tools | Tyler Hobbs | Jeff Jirsa |
| [CASSANDRA-6018](https://issues.apache.org/jira/browse/CASSANDRA-6018) | Add option to encrypt commitlog |  Major | . | Jason Brown | Jason Brown |
| [CASSANDRA-8103](https://issues.apache.org/jira/browse/CASSANDRA-8103) | Secondary Indices for Static Columns |  Major | CQL | Ron Cohen | Taiyuan Zhang |
| [CASSANDRA-7715](https://issues.apache.org/jira/browse/CASSANDRA-7715) | Add a credentials cache to the PasswordAuthenticator |  Minor | CQL | Mike Adamson | Sam Tunnicliffe |
| [CASSANDRA-11173](https://issues.apache.org/jira/browse/CASSANDRA-11173) | Add extension points in storage and streaming classes |  Major | . | Blake Eggleston | Blake Eggleston |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10838](https://issues.apache.org/jira/browse/CASSANDRA-10838) | print 3.0 statistics in sstablemetadata command output |  Minor | Tools | Shogo Hoshii | Shogo Hoshii |
| [CASSANDRA-10981](https://issues.apache.org/jira/browse/CASSANDRA-10981) | Consider striping view locks by key and cfid |  Minor | Coordination, Materialized Views | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-10926](https://issues.apache.org/jira/browse/CASSANDRA-10926) | Improve error message when removenode called on nonmember node |  Trivial | Distributed Metadata, Tools | Kai Wang | Joel Knighton |
| [CASSANDRA-10396](https://issues.apache.org/jira/browse/CASSANDRA-10396) | Simplify row cache invalidation code |  Trivial | . | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-10866](https://issues.apache.org/jira/browse/CASSANDRA-10866) | Column Family should expose count metrics for dropped mutations. |  Minor | Observability, Tools | Anubhav Kale | Anubhav Kale |
| [CASSANDRA-10657](https://issues.apache.org/jira/browse/CASSANDRA-10657) | Re-enable/improve value skipping |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-10907](https://issues.apache.org/jira/browse/CASSANDRA-10907) | Nodetool snapshot should provide an option to skip flushing |  Minor | Configuration | Anubhav Kale | Anubhav Kale |
| [CASSANDRA-10661](https://issues.apache.org/jira/browse/CASSANDRA-10661) | Integrate SASI to Cassandra |  Major | Local Write-Read Paths | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-9974](https://issues.apache.org/jira/browse/CASSANDRA-9974) | Improve debuggability |  Major | . | Benedict |  |
| [CASSANDRA-11054](https://issues.apache.org/jira/browse/CASSANDRA-11054) | Added support for IBM zSystems architecture (s390x) |  Minor | Observability, Testing | Meerabo Shah |  |
| [CASSANDRA-10819](https://issues.apache.org/jira/browse/CASSANDRA-10819) | Generic Java UDF types |  Minor | CQL | Robert Stupp | Robert Stupp |
| [CASSANDRA-10622](https://issues.apache.org/jira/browse/CASSANDRA-10622) | Add all options in cqlshrc sample as commented out choices |  Minor | Documentation and Website | Lorina Poland | Tyler Hobbs |
| [CASSANDRA-11041](https://issues.apache.org/jira/browse/CASSANDRA-11041) | Make it clear what timestamp\_resolution is used for with DTCS |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-8180](https://issues.apache.org/jira/browse/CASSANDRA-8180) | Optimize disk seek using min/max column name meta data when the LIMIT clause is used |  Minor | Local Write-Read Paths | DOAN DuyHai | Stefania |
| [CASSANDRA-11040](https://issues.apache.org/jira/browse/CASSANDRA-11040) | Encrypted hints |  Minor | . | Jason Brown | Jason Brown |
| [CASSANDRA-7950](https://issues.apache.org/jira/browse/CASSANDRA-7950) | Output of nodetool compactionstats and compactionhistory does not work well with long keyspace and column family names. |  Minor | Tools | Eugene | Yuki Morishita |
| [CASSANDRA-11148](https://issues.apache.org/jira/browse/CASSANDRA-11148) | Remove duplicate offline compaction tracking |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-11151](https://issues.apache.org/jira/browse/CASSANDRA-11151) | Remove unused method introduced in CASSANDRA-6696 |  Minor | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-10397](https://issues.apache.org/jira/browse/CASSANDRA-10397) | Add local timezone support to cqlsh |  Minor | . | Suleman Rai | Stefan Podkowinski |
| [CASSANDRA-11124](https://issues.apache.org/jira/browse/CASSANDRA-11124) | Change default cqlsh encoding to utf-8 |  Trivial | Tools | Paulo Motta | Paulo Motta |
| [CASSANDRA-10637](https://issues.apache.org/jira/browse/CASSANDRA-10637) | Extract LoaderOptions and refactor BulkLoader to be able to be used from within existing Java code instead of just through main() |  Minor | Tools | Eric Fenderbosch | Eric Fenderbosch |
| [CASSANDRA-7464](https://issues.apache.org/jira/browse/CASSANDRA-7464) | Replace sstable2json |  Minor | . | Sylvain Lebresne | Chris Lohfink |
| [CASSANDRA-10392](https://issues.apache.org/jira/browse/CASSANDRA-10392) | Allow Cassandra to trace to custom tracing implementations |  Major | . | mck | mck |
| [CASSANDRA-10458](https://issues.apache.org/jira/browse/CASSANDRA-10458) | cqlshrc: add option to always use ssl |  Major | . | Matt Wringe | Stefan Podkowinski |
| [CASSANDRA-9472](https://issues.apache.org/jira/browse/CASSANDRA-9472) | Reintroduce off heap memtables |  Major | . | Benedict | Benedict |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10953](https://issues.apache.org/jira/browse/CASSANDRA-10953) | Make all timeouts configurable via nodetool and jmx |  Major | Configuration | Sebastian Estevez | Jeremy Hanna |
| [CASSANDRA-11015](https://issues.apache.org/jira/browse/CASSANDRA-11015) | CommitLogUpgradeTestMaker creates broken commit logs for \> 3.0 |  Minor | Testing | Jason Brown | Branimir Lambov |
| [CASSANDRA-10958](https://issues.apache.org/jira/browse/CASSANDRA-10958) | Range query with filtering interacts badly with static columns |  Minor | . | Taiyuan Zhang | Taiyuan Zhang |
| [CASSANDRA-11077](https://issues.apache.org/jira/browse/CASSANDRA-11077) | Set javac encoding to utf-8 |  Trivial | Packaging | Paulo Motta | Paulo Motta |
| [CASSANDRA-11001](https://issues.apache.org/jira/browse/CASSANDRA-11001) | Hadoop integration is incompatible with Cassandra Driver 3.0.0 |  Major | . | Jacek Lewandowski | Robert Stupp |
| [CASSANDRA-11116](https://issues.apache.org/jira/browse/CASSANDRA-11116) | Gossiper#isEnabled is not thread safe |  Critical | Core | Sergio Bossa | Ariel Weisberg |
| [CASSANDRA-11122](https://issues.apache.org/jira/browse/CASSANDRA-11122) | SASI does not find term when indexing non-ascii character |  Major | CQL | DOAN DuyHai |  |
| [CASSANDRA-11113](https://issues.apache.org/jira/browse/CASSANDRA-11113) | DateTieredCompactionStrategy.getMaximalTask compacts repaired and unrepaired sstables together |  Major | . | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-11030](https://issues.apache.org/jira/browse/CASSANDRA-11030) | utf-8 characters incorrectly displayed/inserted on cqlsh on Windows |  Minor | . | Paulo Motta | Paulo Motta |
| [CASSANDRA-11120](https://issues.apache.org/jira/browse/CASSANDRA-11120) | Leak Detected while bringing nodes up and down when under stress. |  Minor | Core | Jeremiah Jordan | Ariel Weisberg |
| [CASSANDRA-11090](https://issues.apache.org/jira/browse/CASSANDRA-11090) | Avoid creating empty hint files |  Minor | . | Blake Eggleston | Aleksey Yeschenko |
| [CASSANDRA-11136](https://issues.apache.org/jira/browse/CASSANDRA-11136) | SASI index options validation |  Major | Core | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-11139](https://issues.apache.org/jira/browse/CASSANDRA-11139) | New token allocator is broken |  Major | Configuration | Tommy Stendahl | Branimir Lambov |
| [CASSANDRA-7238](https://issues.apache.org/jira/browse/CASSANDRA-7238) | Nodetool Status performance is much slower with VNodes On |  Minor | Tools | Russell Spitzer | Yuki Morishita |
| [CASSANDRA-10697](https://issues.apache.org/jira/browse/CASSANDRA-10697) | Leak detected while running offline scrub |  Major | Tools | mlowicki | Marcus Eriksson |
| [CASSANDRA-11079](https://issues.apache.org/jira/browse/CASSANDRA-11079) | LeveledCompactionStrategyTest.testCompactionProgress unit test fails sometimes |  Major | Testing | Sylvain Lebresne | Marcus Eriksson |
| [CASSANDRA-11080](https://issues.apache.org/jira/browse/CASSANDRA-11080) | CompactionsCQLTest.testTriggerMinorCompactionSTCS is flaky on 2.2 |  Major | . | Sylvain Lebresne | Marcus Eriksson |
| [CASSANDRA-11156](https://issues.apache.org/jira/browse/CASSANDRA-11156) | AssertionError when listing sstable files on inconsistent disk state |  Major | Local Write-Read Paths | Stefania | Stefania |
| [CASSANDRA-7281](https://issues.apache.org/jira/browse/CASSANDRA-7281) | SELECT on tuple relations are broken for mixed ASC/DESC clustering order |  Major | . | Sylvain Lebresne | Marcin Szymaniuk |
| [CASSANDRA-11050](https://issues.apache.org/jira/browse/CASSANDRA-11050) | SSTables not loaded after dropping column |  Major | Distributed Metadata | amorton | amorton |
| [CASSANDRA-11048](https://issues.apache.org/jira/browse/CASSANDRA-11048) | JSON queries are not thread safe |  Critical | Coordination | Sergio Bossa | Tyler Hobbs |
| [CASSANDRA-11130](https://issues.apache.org/jira/browse/CASSANDRA-11130) | [SASI Pre-QA] = semantics not respected when using StandardAnalyzer |  Major | CQL | DOAN DuyHai | Pavel Yaskevich |
| [CASSANDRA-10733](https://issues.apache.org/jira/browse/CASSANDRA-10733) | Inconsistencies in CQLSH auto-complete |  Trivial | CQL, Tools | Michael Edge | Michael Edge |
| [CASSANDRA-10512](https://issues.apache.org/jira/browse/CASSANDRA-10512) | We do not save an upsampled index summaries |  Major | Local Write-Read Paths | Benedict | Ariel Weisberg |
| [CASSANDRA-10736](https://issues.apache.org/jira/browse/CASSANDRA-10736) | TestTopology.simple\_decommission\_test failing due to assertion triggered by SizeEstimatesRecorder |  Minor | Distributed Metadata | Joel Knighton | Joel Knighton |
| [CASSANDRA-11159](https://issues.apache.org/jira/browse/CASSANDRA-11159) | SASI indexes don't switch memtable on flush |  Critical | Local Write-Read Paths | Sam Tunnicliffe | Pavel Yaskevich |
| [CASSANDRA-10176](https://issues.apache.org/jira/browse/CASSANDRA-10176) | nodetool status says ' Non-system keyspaces don't have the same replication settings' when they do |  Minor | Distributed Metadata | Chris Burroughs | Sylvain Lebresne |
| [CASSANDRA-10721](https://issues.apache.org/jira/browse/CASSANDRA-10721) | Altering a UDT might break UDA deserialisation |  Major | CQL, Distributed Metadata | Aleksey Yeschenko | Robert Stupp |
| [CASSANDRA-11123](https://issues.apache.org/jira/browse/CASSANDRA-11123) | cqlsh pg-style-strings broken if line ends with ';' |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-9714](https://issues.apache.org/jira/browse/CASSANDRA-9714) | sstableloader appears to use the cassandra.yaml outgoing stream throttle |  Major | Tools | Jeremy Hanna | Yuki Morishita |
| [CASSANDRA-10767](https://issues.apache.org/jira/browse/CASSANDRA-10767) | Checking version of Cassandra command creates \`cassandra.logdir\_IS\_UNDEFINED/\` |  Trivial | Tools | Jens Rantil | Yuki Morishita |
| [CASSANDRA-11169](https://issues.apache.org/jira/browse/CASSANDRA-11169) | add SASI validation for partitioner and complex columns |  Major | sasi | Jon Haddad | Pavel Yaskevich |
| [CASSANDRA-11178](https://issues.apache.org/jira/browse/CASSANDRA-11178) | TrackerTest is failing on trunk after CASSANDRA-11159 |  Major | Testing | Yuki Morishita | Pavel Yaskevich |
| [CASSANDRA-11135](https://issues.apache.org/jira/browse/CASSANDRA-11135) | test\_timestamp\_output in the cqlshlib tests is failing |  Major | Testing | Stefania | Stefania |
| [CASSANDRA-10991](https://issues.apache.org/jira/browse/CASSANDRA-10991) | Cleanup OpsCenter keyspace fails - node thinks that didn't joined the ring yet |  Major | Observability | mlowicki | Marcus Eriksson |
| [CASSANDRA-11146](https://issues.apache.org/jira/browse/CASSANDRA-11146) | Adding field to UDT definition breaks SELECT JSON |  Major | CQL | Alexander | Tyler Hobbs |
| [CASSANDRA-11065](https://issues.apache.org/jira/browse/CASSANDRA-11065) | null pointer exception in CassandraDaemon.java:195 |  Minor | Streaming and Messaging | Vassil Lunchev | Paulo Motta |
| [CASSANDRA-10793](https://issues.apache.org/jira/browse/CASSANDRA-10793) | fix ohc and java-driver pom dependencies in build.xml |  Major | Packaging | Jeremiah Jordan | Robert Stupp |
| [CASSANDRA-11033](https://issues.apache.org/jira/browse/CASSANDRA-11033) | Prevent logging in sandboxed state |  Minor | CQL | Robert Stupp | Robert Stupp |
| [CASSANDRA-10625](https://issues.apache.org/jira/browse/CASSANDRA-10625) | Problem of year 10000: Dates too far in the future can be saved but not read back using cqlsh |  Minor | . | Piotr Kołaczkowski | Adam Holmberg |
| [CASSANDRA-10840](https://issues.apache.org/jira/browse/CASSANDRA-10840) | Replacing an aggregate with a new version doesn't reset INITCOND |  Major | CQL | Sandeep Tamhankar | Robert Stupp |
| [CASSANDRA-11205](https://issues.apache.org/jira/browse/CASSANDRA-11205) | SASI OnDiskIndexTest.testSuperBlockRetrieval flaky on CassCI |  Major | Testing | Joel Knighton | Pavel Yaskevich |
| [CASSANDRA-11172](https://issues.apache.org/jira/browse/CASSANDRA-11172) | Infinite loop bug adding high-level SSTableReader in compaction |  Major | Compaction | Jeff Ferland | Marcus Eriksson |
| [CASSANDRA-11158](https://issues.apache.org/jira/browse/CASSANDRA-11158) | AssertionError: null in Slice$Bound.create |  Critical | Compaction, Local Write-Read Paths | Samu Kallio | Branimir Lambov |
| [CASSANDRA-10972](https://issues.apache.org/jira/browse/CASSANDRA-10972) | File based hints don't implement backpressure and can OOM |  Minor | . | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-11167](https://issues.apache.org/jira/browse/CASSANDRA-11167) | NPE when creating serializing ErrorMessage for Exception with null message |  Minor | Coordination | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-10371](https://issues.apache.org/jira/browse/CASSANDRA-10371) | Decommissioned nodes can remain in gossip |  Minor | Distributed Metadata | Brandon Williams | Joel Knighton |
| [CASSANDRA-11212](https://issues.apache.org/jira/browse/CASSANDRA-11212) | cqlsh python version checking is out of date |  Major | Tools | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-11064](https://issues.apache.org/jira/browse/CASSANDRA-11064) | Failed aggregate creation breaks server permanently |  Major | . | Olivier Michallat | Robert Stupp |
| [CASSANDRA-11069](https://issues.apache.org/jira/browse/CASSANDRA-11069) | Materialised views require all collections to be selected. |  Major | Coordination, Materialized Views | Vassil Lunchev | Carl Yeksigian |
| [CASSANDRA-11216](https://issues.apache.org/jira/browse/CASSANDRA-11216) | Range.compareTo() violates the contract of Comparable |  Minor | Core | Jason Brown | Jason Brown |
| [CASSANDRA-11214](https://issues.apache.org/jira/browse/CASSANDRA-11214) | Adding Support for system-Z(s390x) architecture |  Minor | Observability, Testing | Nirav | Nirav |
| [CASSANDRA-11239](https://issues.apache.org/jira/browse/CASSANDRA-11239) | Deprecated repair methods cause NPE |  Major | . | Nick Bailey | Nick Bailey |
| [CASSANDRA-11062](https://issues.apache.org/jira/browse/CASSANDRA-11062) | BatchlogManager May Attempt to Flush Hints After HintsService is Shutdown |  Minor | Local Write-Read Paths | Caleb Rackliffe | Caleb Rackliffe |
| [CASSANDRA-11164](https://issues.apache.org/jira/browse/CASSANDRA-11164) | Order and filter cipher suites correctly |  Minor | . | Tom Petracca | Stefan Podkowinski |
| [CASSANDRA-11184](https://issues.apache.org/jira/browse/CASSANDRA-11184) | cqlsh\_tests.py:TestCqlsh.test\_sub\_second\_precision is failing |  Major | Testing | Michael Shuler | Jim Witschey |
| [CASSANDRA-11185](https://issues.apache.org/jira/browse/CASSANDRA-11185) | cqlsh\_copy\_tests.py:CqlshCopyTest.test\_round\_trip\_with\_sub\_second\_precision is failing |  Major | Testing | Michael Shuler | Jim Witschey |
| [CASSANDRA-11128](https://issues.apache.org/jira/browse/CASSANDRA-11128) | Unexpected exception during request; AssertionError: null |  Major | . | Russ Hatch | Sylvain Lebresne |
| [CASSANDRA-11043](https://issues.apache.org/jira/browse/CASSANDRA-11043) | Secondary indexes doesn't properly validate custom expressions |  Major | CQL, Local Write-Read Paths, Secondary Indexes | Andrés de la Peña | Sam Tunnicliffe |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9986](https://issues.apache.org/jira/browse/CASSANDRA-9986) | Remove SliceableUnfilteredRowIterator |  Minor | . | Benedict | Benedict |
| [CASSANDRA-11132](https://issues.apache.org/jira/browse/CASSANDRA-11132) | [SASI Pre-QA] Creating SPARSE index with literal type leaves the index in a stalled state |  Major | CQL | DOAN DuyHai | Pavel Yaskevich |
| [CASSANDRA-11133](https://issues.apache.org/jira/browse/CASSANDRA-11133) | [SASI Pre-QA] Creating SPARSE index with Analyzer fails silently when using = |  Major | CQL | DOAN DuyHai | Pavel Yaskevich |
| [CASSANDRA-11134](https://issues.apache.org/jira/browse/CASSANDRA-11134) | [SASI Pre-QA] Index creation should respect IF NOT EXISTS |  Major | CQL | DOAN DuyHai | Pavel Yaskevich |
| [CASSANDRA-11129](https://issues.apache.org/jira/browse/CASSANDRA-11129) | [SASI Pre-QA] Index mode validation throws exception but do not cleanup index meta data |  Major | CQL | DOAN DuyHai | Pavel Yaskevich |
| [CASSANDRA-5902](https://issues.apache.org/jira/browse/CASSANDRA-5902) | Dealing with hints after a topology change |  Minor | Coordination | Jonathan Ellis | Branimir Lambov |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11067](https://issues.apache.org/jira/browse/CASSANDRA-11067) | Improve SASI syntax |  Major | CQL | Jonathan Ellis | Pavel Yaskevich |


