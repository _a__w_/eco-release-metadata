
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra  3.4 Release Notes

These release notes cover new developer and user-facing incompatibilities, important issues, features, and major improvements.


---

* [CASSANDRA-10428](https://issues.apache.org/jira/browse/CASSANDRA-10428) | *Major* | **cqlsh: Include sub-second precision in timestamps by default**

Query with \>= timestamp works. But the exact timestamp value is not working.

{noformat}
NCHAN-M-D0LZ:bin nchan$ ./cqlsh
Connected to CCC Multi-Region Cassandra Cluster at \<host\>:\<port\>.
[cqlsh 5.0.1 \| Cassandra 2.1.7 \| CQL spec 3.2.0 \| Native protocol v3]
Use HELP for help.
cqlsh\>
{noformat}

{panel:title=Schema\|borderStyle=dashed\|borderColor=#ccc\|titleBGColor=#F7D6C1\|bgColor=#FFFFCE}
cqlsh:ccc\> desc COLUMNFAMILY ez\_task\_result ;

CREATE TABLE ccc.ez\_task\_result (
    submissionid text,
    ezid text,
    name text,
    time timestamp,
    analyzed\_index\_root text,
    ...
    ...
    PRIMARY KEY (submissionid, ezid, name, time)
{panel}

{panel:title=Working\|borderStyle=dashed\|borderColor=#ccc\|titleBGColor=#F7D6C1\|bgColor=#FFFFCE}
cqlsh:ccc\> select submissionid, ezid, name, time, state, status, translated\_criteria\_status from ez\_task\_result where submissionid='760dd154670811e58c04005056bb6ff0' and ezid='760dd6de670811e594fc005056bb6ff0' and name='run-sanities' and time\>='2015-09-29 20:54:23-0700';

 submissionid                     \| ezid                             \| name         \| time                     \| state     \| status      \| translated\_criteria\_status
----------------------------------+----------------------------------+--------------+--------------------------+-----------+-------------+----------------------------
 760dd154670811e58c04005056bb6ff0 \| 760dd6de670811e594fc005056bb6ff0 \| run-sanities \| 2015-09-29 20:54:23-0700 \| EXECUTING \| IN\_PROGRESS \|       run-sanities started

(1 rows)
cqlsh:ccc\>
{panel}
{panel:title=Not working\|borderStyle=dashed\|borderColor=#ccc\|titleBGColor=#F7D6C1\|bgColor=#FFFFCE}
cqlsh:ccc\> select submissionid, ezid, name, time, state, status, translated\_criteria\_status from ez\_task\_result where submissionid='760dd154670811e58c04005056bb6ff0' and ezid='760dd6de670811e594fc005056bb6ff0' and name='run-sanities' and time='2015-09-29 20:54:23-0700';

 submissionid \| ezid \| name \| time \| analyzed\_index\_root \| analyzed\_log\_path \| clientid \| end\_time \| jenkins\_path \| log\_file\_path \| path\_available \| path\_to\_task \| required\_for\_overall\_status \| start\_time \| state \| status \| translated\_criteria\_status \| type
--------------+------+------+------+---------------------+-------------------+----------+----------+--------------+---------------+----------------+--------------+-----------------------------+------------+-------+--------+----------------------------+------

(0 rows)
cqlsh:ccc\>
{panel}



