
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1.20 - 2018-02-16



### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-14021](https://issues.apache.org/jira/browse/CASSANDRA-14021) | test\_pycodestyle\_compliance - cqlsh\_tests.cqlsh\_tests.TestCqlsh code style errors |  Major | . | Michael Kjellman | Michael Kjellman |
| [CASSANDRA-14181](https://issues.apache.org/jira/browse/CASSANDRA-14181) | RPM package has too many executable files |  Minor | Packaging | Troels Arvin | Troels Arvin |
| [CASSANDRA-14092](https://issues.apache.org/jira/browse/CASSANDRA-14092) | Max ttl of 20 years will overflow localDeletionTime |  Blocker | Core | Paulo Motta | Paulo Motta |


