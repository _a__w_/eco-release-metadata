
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.8.2 - 2011-07-26



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2770](https://issues.apache.org/jira/browse/CASSANDRA-2770) | Expose data\_dir though jmx |  Trivial | . | Mike Bulman | Mike Bulman |
| [CASSANDRA-2911](https://issues.apache.org/jira/browse/CASSANDRA-2911) | Simplified classes to write SSTables (for bulk loading usage) |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-1125](https://issues.apache.org/jira/browse/CASSANDRA-1125) | Filter out ColumnFamily rows that aren't part of the query (using a KeyRange) |  Minor | . | Jeremy Hanna | mck |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2754](https://issues.apache.org/jira/browse/CASSANDRA-2754) | Consolidating Ticket for JDBC Semantic Improvements |  Minor | . | Rick Shaw | Rick Shaw |
| [CASSANDRA-2808](https://issues.apache.org/jira/browse/CASSANDRA-2808) | add java vendor/versoin to cassandra startup logging |  Minor | . | Jackson Chung | Jackson Chung |
| [CASSANDRA-2803](https://issues.apache.org/jira/browse/CASSANDRA-2803) | Cassandra deb should depend on libjna-java |  Minor | Packaging | paul cannon |  |
| [CASSANDRA-2813](https://issues.apache.org/jira/browse/CASSANDRA-2813) | more info on logging when SSTable cannot create the builder due to version mismatch |  Minor | . | Jackson Chung | Jackson Chung |
| [CASSANDRA-2776](https://issues.apache.org/jira/browse/CASSANDRA-2776) | add ability to return  "endpoints" to nodetool |  Minor | Tools | Patricio Echague | Patricio Echague |
| [CASSANDRA-2832](https://issues.apache.org/jira/browse/CASSANDRA-2832) | reduce variance in HH impact between wide and narrow rows |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2836](https://issues.apache.org/jira/browse/CASSANDRA-2836) | Add CFS.estimatedKeys to cfstats output |  Trivial | . | Brandon Williams | Joe Stein |
| [CASSANDRA-2841](https://issues.apache.org/jira/browse/CASSANDRA-2841) | Always use even distribution for merkle tree with RandomPartitionner |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2844](https://issues.apache.org/jira/browse/CASSANDRA-2844) | grep friendly nodetool compactionstats output |  Trivial | Tools | Wojciech Meler | Wojciech Meler |
| [CASSANDRA-2804](https://issues.apache.org/jira/browse/CASSANDRA-2804) | expose dropped messages, exceptions over JMX |  Minor | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2763](https://issues.apache.org/jira/browse/CASSANDRA-2763) | When dropping a keyspace you're currently authenticated to, might be nice to de-authenticate upon completion |  Trivial | Tools | Jeremy Hanna | Joe Stein |
| [CASSANDRA-2888](https://issues.apache.org/jira/browse/CASSANDRA-2888) | CQL support for JDBC DatabaseMetaData |  Minor | . | Dave Carlson | Rick Shaw |
| [CASSANDRA-2903](https://issues.apache.org/jira/browse/CASSANDRA-2903) | Default behavior of generating index\_name for columns might need to be improved. |  Minor | CQL | Boris Yen | Jonathan Ellis |
| [CASSANDRA-2895](https://issues.apache.org/jira/browse/CASSANDRA-2895) | add java classpath to cassandra startup logging |  Minor | . | Jackson Chung | Jackson Chung |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2780](https://issues.apache.org/jira/browse/CASSANDRA-2780) | sstable2json needs to escape quotes |  Minor | . | Timo Nentwig | Pavel Yaskevich |
| [CASSANDRA-2769](https://issues.apache.org/jira/browse/CASSANDRA-2769) | Cannot Create Duplicate Compaction Marker |  Major | . | Benjamin Coverston | Sylvain Lebresne |
| [CASSANDRA-2727](https://issues.apache.org/jira/browse/CASSANDRA-2727) | examples/hadoop\_word\_count reducer to cassandra doesn't output into the output\_words cf |  Minor | . | Jeremy Hanna | T Jake Luciani |
| [CASSANDRA-2164](https://issues.apache.org/jira/browse/CASSANDRA-2164) | debian build dep on ant-optional is missing |  Minor | . | Peter Schuller | Peter Schuller |
| [CASSANDRA-2817](https://issues.apache.org/jira/browse/CASSANDRA-2817) | Expose number of threads blocked on submitting a memtable for flush |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2822](https://issues.apache.org/jira/browse/CASSANDRA-2822) | NullPointerException after upgrade to 0.8.0 |  Trivial | . | Viliam Holub | Jonathan Ellis |
| [CASSANDRA-2823](https://issues.apache.org/jira/browse/CASSANDRA-2823) | NPE during range slices with rowrepairs |  Major | . | Terje Marthinussen | Sylvain Lebresne |
| [CASSANDRA-2824](https://issues.apache.org/jira/browse/CASSANDRA-2824) | assert err on SystemTable.getCurrentLocalNodeId during a cleanup |  Minor | . | Jackson Chung | Sylvain Lebresne |
| [CASSANDRA-2755](https://issues.apache.org/jira/browse/CASSANDRA-2755) | ColumnFamilyRecordWriter fails to throw a write exception encountered after the user begins to close the writer |  Minor | . | Greg Katz | mck |
| [CASSANDRA-2835](https://issues.apache.org/jira/browse/CASSANDRA-2835) | CFMetadata don't set the default for Replicate\_on\_write correctly |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2818](https://issues.apache.org/jira/browse/CASSANDRA-2818) | 0.8.0 is unable to participate with nodes using a \_newer\_ protocol version |  Minor | . | Michael Allen | Brandon Williams |
| [CASSANDRA-2653](https://issues.apache.org/jira/browse/CASSANDRA-2653) | index scan errors out when zero columns are requested |  Minor | . | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-2773](https://issues.apache.org/jira/browse/CASSANDRA-2773) | "Index manager cannot support deleting and inserting into a row in the same mutation" |  Critical | . | Boris Yen | Jonathan Ellis |
| [CASSANDRA-2853](https://issues.apache.org/jira/browse/CASSANDRA-2853) | cassandra-cli has backwards index status message |  Trivial | . | Matt Kennedy | Matt Kennedy |
| [CASSANDRA-2846](https://issues.apache.org/jira/browse/CASSANDRA-2846) | Changing replication\_factor using "update keyspace" not working |  Minor | . | Jonas Borgström | Jonathan Ellis |
| [CASSANDRA-2852](https://issues.apache.org/jira/browse/CASSANDRA-2852) | Cassandra CLI - Import Keyspace Definitions from File - Comments do partitially interpret characters/commands |  Trivial | Tools | jens mueller | Pavel Yaskevich |
| [CASSANDRA-2746](https://issues.apache.org/jira/browse/CASSANDRA-2746) | CliClient does not log root cause exception when catch it from executeCLIStatement |  Trivial | Tools | Jackson Chung | Jackson Chung |
| [CASSANDRA-2800](https://issues.apache.org/jira/browse/CASSANDRA-2800) | OPP#describeOwnership reports incorrect ownership |  Minor | . | Jon Hermes | Jon Hermes |
| [CASSANDRA-2762](https://issues.apache.org/jira/browse/CASSANDRA-2762) | Token cannot contain comma (possibly non-alpha/non-numeric too?) in OrderPreservingPartitioner |  Minor | . | Jackson Chung | Jonathan Ellis |
| [CASSANDRA-2870](https://issues.apache.org/jira/browse/CASSANDRA-2870) | dynamic snitch + read repair off can cause LOCAL\_QUORUM reads to return spurious UnavailableException |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2873](https://issues.apache.org/jira/browse/CASSANDRA-2873) | Typo in src/java/org/apache/cassandra/cli/CliClient |  Trivial | Tools | Michał Bartoszewski | Jonathan Ellis |
| [CASSANDRA-2880](https://issues.apache.org/jira/browse/CASSANDRA-2880) | example configuration of commitlog\_sync: batch |  Trivial | . | Wojciech Meler | Wojciech Meler |
| [CASSANDRA-2129](https://issues.apache.org/jira/browse/CASSANDRA-2129) | removetoken after removetoken rf error fails to work |  Minor | . | Mike Bulman | Brandon Williams |
| [CASSANDRA-2899](https://issues.apache.org/jira/browse/CASSANDRA-2899) | cli silently fails when classes are quoted |  Minor | . | Brandon Williams | Pavel Yaskevich |
| [CASSANDRA-2821](https://issues.apache.org/jira/browse/CASSANDRA-2821) | CLI remove ascii column |  Minor | Tools | Sasha Dolgy | Pavel Yaskevich |
| [CASSANDRA-2902](https://issues.apache.org/jira/browse/CASSANDRA-2902) | "count" doesn't accept UUIDs in CLI even though "get" does |  Minor | . | Matthew F. Dennis | Pavel Yaskevich |
| [CASSANDRA-2809](https://issues.apache.org/jira/browse/CASSANDRA-2809) | In the Cli, update column family \<cf\> with comparator; create Column metadata |  Minor | Tools | Silvère Lestang | Pavel Yaskevich |
| [CASSANDRA-2908](https://issues.apache.org/jira/browse/CASSANDRA-2908) | Fix bulkload JMX call |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2907](https://issues.apache.org/jira/browse/CASSANDRA-2907) | durable\_writes flag cannot be changed via the CLI (system does not process KsDef.durable\_writes option properly). |  Minor | . | Benjamin Schrauwen | Pavel Yaskevich |
| [CASSANDRA-2872](https://issues.apache.org/jira/browse/CASSANDRA-2872) | While dropping and recreating an index, incremental snapshotting can hang |  Minor | . | Sylvain Lebresne | Jonathan Ellis |
| [CASSANDRA-2860](https://issues.apache.org/jira/browse/CASSANDRA-2860) | Versioning works \*too\* well |  Major | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-2912](https://issues.apache.org/jira/browse/CASSANDRA-2912) | CQL ignores client timestamp for full row deletion |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2928](https://issues.apache.org/jira/browse/CASSANDRA-2928) | Fix Hinted Handoff replay |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2816](https://issues.apache.org/jira/browse/CASSANDRA-2816) | Repair doesn't synchronize merkle tree creation properly |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2869](https://issues.apache.org/jira/browse/CASSANDRA-2869) | CassandraStorage does not function properly when used multiple times in a single pig script due to UDFContext sharing issues |  Major | . | Grant Ingersoll | Jeremy Hanna |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2728](https://issues.apache.org/jira/browse/CASSANDRA-2728) | Update the JDBC semantics contained in the class CResultSet |  Minor | . | Rick Shaw | Rick Shaw |
| [CASSANDRA-2725](https://issues.apache.org/jira/browse/CASSANDRA-2725) | Update the JDBC semantics contained in the class AbstractResultSet |  Minor | CQL | Rick Shaw | Rick Shaw |
| [CASSANDRA-2720](https://issues.apache.org/jira/browse/CASSANDRA-2720) | The current implementation of CassandraConnection does not always follow documented semantics for a JDBC Connection interface |  Minor | CQL | Rick Shaw | Rick Shaw |
| [CASSANDRA-2729](https://issues.apache.org/jira/browse/CASSANDRA-2729) | Update the JDBC semantics contained in the interface CassandraResultSet |  Minor | CQL | Rick Shaw | Rick Shaw |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2934](https://issues.apache.org/jira/browse/CASSANDRA-2934) | log broken incoming connections at DEBUG |  Trivial | . | Jonathan Ellis | Jonathan Ellis |


