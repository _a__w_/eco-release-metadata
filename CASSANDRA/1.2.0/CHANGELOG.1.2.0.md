
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.2.0 - 2013-01-02



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3761](https://issues.apache.org/jira/browse/CASSANDRA-3761) | CQL 3.0 |  Major | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5060](https://issues.apache.org/jira/browse/CASSANDRA-5060) | select keyspace\_name from system.schema\_keyspaces |  Minor | CQL | Tupshin Harper | Aleksey Yeschenko |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4009](https://issues.apache.org/jira/browse/CASSANDRA-4009) | Increase usage of Metrics and flesh out o.a.c.metrics |  Minor | . | Brandon Williams | Yuki Morishita |
| [CASSANDRA-4606](https://issues.apache.org/jira/browse/CASSANDRA-4606) | Add Recommends: ntp to Debian packages |  Minor | Packaging | paul cannon | paul cannon |
| [CASSANDRA-4474](https://issues.apache.org/jira/browse/CASSANDRA-4474) | Respect five-minute flush moratorium after initial CL replay |  Minor | . | Jonathan Ellis | Yuki Morishita |
| [CASSANDRA-2293](https://issues.apache.org/jira/browse/CASSANDRA-2293) | Rewrite nodetool help |  Minor | Documentation and Website | amorton | Jason Brown |
| [CASSANDRA-4406](https://issues.apache.org/jira/browse/CASSANDRA-4406) | Update stress for CQL3 |  Major | Tools | Sylvain Lebresne | David Alves |
| [CASSANDRA-4208](https://issues.apache.org/jira/browse/CASSANDRA-4208) | ColumnFamilyOutputFormat should support writing to multiple column families |  Major | . | Robbie Strickland | Robbie Strickland |
| [CASSANDRA-4806](https://issues.apache.org/jira/browse/CASSANDRA-4806) | Consistency of Append/Prepend on Lists need to be improved or clarified |  Minor | . | Michaël Figuière | Sylvain Lebresne |
| [CASSANDRA-3920](https://issues.apache.org/jira/browse/CASSANDRA-3920) | tests for cqlsh |  Minor | Tools | paul cannon | paul cannon |
| [CASSANDRA-4119](https://issues.apache.org/jira/browse/CASSANDRA-4119) | Support multiple non-consecutive tokens per host (virtual nodes) |  Major | . | Sam Overton | Sam Overton |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4620](https://issues.apache.org/jira/browse/CASSANDRA-4620) | Avoid special characters which might yield to a build fail |  Major | . | Pierre-Yves Ritschard |  |
| [CASSANDRA-4566](https://issues.apache.org/jira/browse/CASSANDRA-4566) | test-clientutil target is failing |  Blocker | Packaging, Tools | Eric Evans | Dave Brosius |
| [CASSANDRA-4622](https://issues.apache.org/jira/browse/CASSANDRA-4622) | corrupted saved caches |  Major | . | Brandon Williams | Vijay |
| [CASSANDRA-4633](https://issues.apache.org/jira/browse/CASSANDRA-4633) | cassandra-stress:  --enable-cql does not work with COUNTER\_ADD |  Minor | Tools | Cathy Daw | Aleksey Yeschenko |
| [CASSANDRA-4576](https://issues.apache.org/jira/browse/CASSANDRA-4576) | Error in non-upgraded node's log when upgrading another node to trunk |  Major | . | Tyler Patterson | Pavel Yaskevich |
| [CASSANDRA-4799](https://issues.apache.org/jira/browse/CASSANDRA-4799) | assertion failure in leveled compaction test |  Major | . | Brandon Williams | Yuki Morishita |
| [CASSANDRA-4802](https://issues.apache.org/jira/browse/CASSANDRA-4802) | Regular startup log has confusing "Bootstrap/Replace/Move completed!" without boostrap, replace, or move |  Trivial | . | Karl Mueller | Vijay |
| [CASSANDRA-4842](https://issues.apache.org/jira/browse/CASSANDRA-4842) | DateType in Column MetaData causes server crash |  Minor | . | Russell Bradberry | Sylvain Lebresne |
| [CASSANDRA-4797](https://issues.apache.org/jira/browse/CASSANDRA-4797) | range queries return incorrect results |  Major | . | Brandon Williams |  |
| [CASSANDRA-4719](https://issues.apache.org/jira/browse/CASSANDRA-4719) | binary protocol: when an invalid event type is watched via a REGISTER message, the response message does not have an associated stream id |  Minor | CQL | paul cannon | Sylvain Lebresne |
| [CASSANDRA-4402](https://issues.apache.org/jira/browse/CASSANDRA-4402) | Atomicity violation bugs because of misusing concurrent collections |  Trivial | . | Yu Lin | Yu Lin |
| [CASSANDRA-4913](https://issues.apache.org/jira/browse/CASSANDRA-4913) | DESC KEYSPACE \<ks\> from cqlsh won't show cql3 cfs |  Major | . | Nick Bailey | Aleksey Yeschenko |
| [CASSANDRA-4996](https://issues.apache.org/jira/browse/CASSANDRA-4996) | After changing the compaction strategy, compression\_strategy  always returning back to the "SnappyCompressor" through CQL 2.2.0 |  Trivial | . | Shamim Ahmed | Aleksey Yeschenko |
| [CASSANDRA-5066](https://issues.apache.org/jira/browse/CASSANDRA-5066) | Compression params validation assumes SnappyCompressor |  Trivial | . | Aleksey Yeschenko | Sylvain Lebresne |
| [CASSANDRA-5070](https://issues.apache.org/jira/browse/CASSANDRA-5070) | LOCAL\_QUORUM consistency causes Tracing to fail |  Minor | . | T Jake Luciani | Aleksey Yeschenko |
| [CASSANDRA-4492](https://issues.apache.org/jira/browse/CASSANDRA-4492) | HintsColumnFamily compactions hang when using multithreaded compaction |  Minor | . | Jason Harvey | Carl Yeksigian |
| [CASSANDRA-5082](https://issues.apache.org/jira/browse/CASSANDRA-5082) | Disallow counters in collection (CQL3) |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5093](https://issues.apache.org/jira/browse/CASSANDRA-5093) | Wrong bloom\_filter\_fp\_chance for newly created CFs with LeveledCompactionStrategy |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5089](https://issues.apache.org/jira/browse/CASSANDRA-5089) | get\_range\_slices does not validate end\_token |  Trivial | . | Aleksey Pesternikov | Jonathan Ellis |
| [CASSANDRA-5450](https://issues.apache.org/jira/browse/CASSANDRA-5450) | cluster : Host ID collision |  Major | Configuration | nivance |  |


