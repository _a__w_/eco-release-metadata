
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.6.1 - 2010-04-18



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-978](https://issues.apache.org/jira/browse/CASSANDRA-978) | add nodetool drain |  Minor | Tools | Jonathan Ellis | Roger Schildmeijer |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-955](https://issues.apache.org/jira/browse/CASSANDRA-955) | Hadoop doesn't schedule the tasks close to the data |  Major | . | Johan Oskarsson | Johan Oskarsson |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-934](https://issues.apache.org/jira/browse/CASSANDRA-934) | NPE in sstable2json |  Minor | Tools | Brandon Williams | Brandon Williams |
| [CASSANDRA-937](https://issues.apache.org/jira/browse/CASSANDRA-937) | Invalid Response count 4 |  Major | . | Dan Di Spaltro | Jonathan Ellis |
| [CASSANDRA-933](https://issues.apache.org/jira/browse/CASSANDRA-933) | OutOfBoundException in StorageService.getAllRanges |  Minor | . | gabriele renzi | gabriele renzi |
| [CASSANDRA-942](https://issues.apache.org/jira/browse/CASSANDRA-942) | Command line arguments inversion in clustertool |  Trivial | Tools | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-936](https://issues.apache.org/jira/browse/CASSANDRA-936) | Discard Commitlog Exception |  Major | . | Dan Di Spaltro | Jonathan Ellis |
| [CASSANDRA-924](https://issues.apache.org/jira/browse/CASSANDRA-924) | incorrect neighbor calculation in repair |  Major | . | Jianing hu | Stu Hood |
| [CASSANDRA-920](https://issues.apache.org/jira/browse/CASSANDRA-920) | Deleting and re-inserting row causes error in get\_slice count parameter |  Minor | . | Bob Florian | Brandon Williams |
| [CASSANDRA-866](https://issues.apache.org/jira/browse/CASSANDRA-866) | AssertionError SSTableSliceIterator.java:126 |  Major | . | B. Todd Burruss | Jonathan Ellis |
| [CASSANDRA-969](https://issues.apache.org/jira/browse/CASSANDRA-969) | Server fails to join cluster if IPv6 only |  Minor | . | Cody Lerum | Gary Dusbabek |


