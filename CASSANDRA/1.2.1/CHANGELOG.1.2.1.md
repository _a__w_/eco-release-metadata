
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.2.1 - 2013-01-28



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5044](https://issues.apache.org/jira/browse/CASSANDRA-5044) | Allow resizing of threadpools via JMX |  Minor | Tools | Carl Yeksigian | Carl Yeksigian |
| [CASSANDRA-5031](https://issues.apache.org/jira/browse/CASSANDRA-5031) | Add ssl support to binary protocol |  Major | CQL | Jonathan Ellis | Jason Brown |
| [CASSANDRA-4750](https://issues.apache.org/jira/browse/CASSANDRA-4750) | Add jmx/nodetool methods to enable/disable hinted handoff |  Minor | . | Brandon Williams | Alexey Zotov |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3977](https://issues.apache.org/jira/browse/CASSANDRA-3977) | [patch] choose a constant for 'no compression ration' that is outside the valid range of compression ratios |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-4848](https://issues.apache.org/jira/browse/CASSANDRA-4848) | Expose black-listed directories via JMX |  Minor | . | Kirk True | Kirk True |
| [CASSANDRA-5005](https://issues.apache.org/jira/browse/CASSANDRA-5005) | Add a latency histogram option to stress |  Minor | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-5041](https://issues.apache.org/jira/browse/CASSANDRA-5041) | Change LeveledManifest to try to load old and tmp json files |  Major | . | Carl Yeksigian | Carl Yeksigian |
| [CASSANDRA-5043](https://issues.apache.org/jira/browse/CASSANDRA-5043) | Small CollationController refactoring and CFS.getRawCachedRow(DecoratedKey) fix. |  Minor | . | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-5057](https://issues.apache.org/jira/browse/CASSANDRA-5057) | Cache reads don't need to be mutable if we're using the off-heap (copying) cache |  Trivial | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5090](https://issues.apache.org/jira/browse/CASSANDRA-5090) | Minimize byte array allocation by AbstractData{Input, Output}. |  Minor | . | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-4671](https://issues.apache.org/jira/browse/CASSANDRA-4671) | Improve removal of gcable tomstones during minor compaction |  Minor | . | Sylvain Lebresne | Vijay |
| [CASSANDRA-5077](https://issues.apache.org/jira/browse/CASSANDRA-5077) | Simplify CompactionIterable.getReduced |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4972](https://issues.apache.org/jira/browse/CASSANDRA-4972) | Move "add a default limit" logic to cqlsh |  Minor | Tools | Jonathan Ellis | Aleksey Yeschenko |
| [CASSANDRA-4767](https://issues.apache.org/jira/browse/CASSANDRA-4767) | Need some indication of node repair success or failure |  Minor | Tools | Ahmed Bashir | Yuki Morishita |
| [CASSANDRA-5095](https://issues.apache.org/jira/browse/CASSANDRA-5095) | change NEWS.txt in 1.2 to make it clear that a repair is required post-upgrade |  Minor | . | Robert Coli | Robert Coli |
| [CASSANDRA-5155](https://issues.apache.org/jira/browse/CASSANDRA-5155) | Make Ec2Region's datacenter name configurable |  Minor | . | Jason Brown | Jason Brown |
| [CASSANDRA-5109](https://issues.apache.org/jira/browse/CASSANDRA-5109) | convert default marshallers list to map for better readability |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-5159](https://issues.apache.org/jira/browse/CASSANDRA-5159) | Expose droppable tombstone ratio stats over JMX |  Minor | Tools | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-4927](https://issues.apache.org/jira/browse/CASSANDRA-4927) | E notation is not implemented for floating point numbers. |  Minor | . | Krzysztof Cieslinski Cognitum | Michał Michalski |
| [CASSANDRA-4858](https://issues.apache.org/jira/browse/CASSANDRA-4858) | Coverage analysis for low-CL queries |  Major | . | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-5170](https://issues.apache.org/jira/browse/CASSANDRA-5170) | ConcurrentModificationException in getBootstrapSource |  Major | . | Jonathan Ellis | Kirk True |
| [CASSANDRA-5172](https://issues.apache.org/jira/browse/CASSANDRA-5172) | Detect (and warn) unintentional use of the cql2 thrift methods when cql3 was the intention |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4894](https://issues.apache.org/jira/browse/CASSANDRA-4894) | log number of combined/merged rows during a compaction |  Minor | . | Matthew F. Dennis | Yuki Morishita |
| [CASSANDRA-5156](https://issues.apache.org/jira/browse/CASSANDRA-5156) | CQL: loosen useless versioning constraint |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5128](https://issues.apache.org/jira/browse/CASSANDRA-5128) | Stream hints on decommission |  Major | . | Jason Brown | Jason Brown |
| [CASSANDRA-5120](https://issues.apache.org/jira/browse/CASSANDRA-5120) | Add support for SSL sockets to use client certificate authentication. |  Minor | . | Steven Franklin | Aleksey Yeschenko |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4939](https://issues.apache.org/jira/browse/CASSANDRA-4939) | Add debug logging to list filenames processed by o.a.c.db.Directories.migrateFile method |  Minor | . | J.B. Langston | Dave Brosius |
| [CASSANDRA-4847](https://issues.apache.org/jira/browse/CASSANDRA-4847) | Bad disk causes death of node despite disk\_failure\_policy |  Major | . | Kirk True | Kirk True |
| [CASSANDRA-4753](https://issues.apache.org/jira/browse/CASSANDRA-4753) | Timeout reporter writes hints for the local node |  Minor | . | Aleksey Yeschenko | Jonathan Ellis |
| [CASSANDRA-5087](https://issues.apache.org/jira/browse/CASSANDRA-5087) | Changing from higher to lower compaction throughput causes long (multi hour) pause in large compactions |  Minor | . | J.B. Langston | J.B. Langston |
| [CASSANDRA-5097](https://issues.apache.org/jira/browse/CASSANDRA-5097) | cassandra-shuffle fails as system keyspace is not user-modifiable |  Major | Tools | Jouni Hartikainen | Jouni Hartikainen |
| [CASSANDRA-4564](https://issues.apache.org/jira/browse/CASSANDRA-4564) | MoveTest madness |  Minor | Testing | Eric Evans | Liu Yu |
| [CASSANDRA-5101](https://issues.apache.org/jira/browse/CASSANDRA-5101) | describe commands fail in cql3 when previously created with cql2 |  Minor | . | Michael Kjellman | Aleksey Yeschenko |
| [CASSANDRA-5098](https://issues.apache.org/jira/browse/CASSANDRA-5098) | CassandraStorage doesn't decode name in widerow mode |  Major | . | Justen Walker | Brandon Williams |
| [CASSANDRA-5102](https://issues.apache.org/jira/browse/CASSANDRA-5102) | upgrading from 1.1.7 to 1.2.0 caused upgraded nodes to only know about other 1.2.0 nodes |  Blocker | . | Michael Kjellman | Brandon Williams |
| [CASSANDRA-5113](https://issues.apache.org/jira/browse/CASSANDRA-5113) | RepairCallback breaks CL guarantees |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5122](https://issues.apache.org/jira/browse/CASSANDRA-5122) | Select on composite partition keys are not validated correctly |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5013](https://issues.apache.org/jira/browse/CASSANDRA-5013) | disallow bloom filter false positive chance of 0 |  Minor | Configuration | Matthew F. Dennis | Jonathan Ellis |
| [CASSANDRA-5088](https://issues.apache.org/jira/browse/CASSANDRA-5088) | Major compaction IOException in 1.1.8 |  Major | . | Karl Mueller | Jonathan Ellis |
| [CASSANDRA-5117](https://issues.apache.org/jira/browse/CASSANDRA-5117) | CQL 3 ALTER TABLE ... ADD causes OOB |  Major | . | K. B. Hahn | Sylvain Lebresne |
| [CASSANDRA-5127](https://issues.apache.org/jira/browse/CASSANDRA-5127) | unsafeAssassinateEndpoint throws NullPointerException and fails to remove node from gossip |  Major | . | Michael Kjellman | Brandon Williams |
| [CASSANDRA-5126](https://issues.apache.org/jira/browse/CASSANDRA-5126) | Ensure Jackson dependency matches lib |  Minor | . | Nate McCall | Nate McCall |
| [CASSANDRA-5132](https://issues.apache.org/jira/browse/CASSANDRA-5132) | unfriendly error message during create table map collection |  Trivial | . | K. B. Hahn | Sylvain Lebresne |
| [CASSANDRA-5134](https://issues.apache.org/jira/browse/CASSANDRA-5134) | sstable2json always returns default value validator |  Minor | Tools | Christophe Angeli | Sylvain Lebresne |
| [CASSANDRA-5136](https://issues.apache.org/jira/browse/CASSANDRA-5136) | repair -pr triggers an Assertion |  Major | . | Michael Kjellman | Carl Yeksigian |
| [CASSANDRA-5118](https://issues.apache.org/jira/browse/CASSANDRA-5118) | user defined compaction is broken |  Minor | . | Michael Kjellman | Yuki Morishita |
| [CASSANDRA-5106](https://issues.apache.org/jira/browse/CASSANDRA-5106) | Stock example for using pig throws InvalidRequestException(why:Start token sorts after end token) |  Major | . | janwar dinata | Jonathan Ellis |
| [CASSANDRA-5144](https://issues.apache.org/jira/browse/CASSANDRA-5144) | Validate login for Thrift describe\_keyspace, describe\_keyspaces and set\_keyspace methods |  Trivial | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5145](https://issues.apache.org/jira/browse/CASSANDRA-5145) | CQL3 BATCH authorization caching bug |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5141](https://issues.apache.org/jira/browse/CASSANDRA-5141) | Can not insert an empty map. |  Minor | . | Krzysztof Cieslinski Cognitum | Sylvain Lebresne |
| [CASSANDRA-5099](https://issues.apache.org/jira/browse/CASSANDRA-5099) | Since 1.1, get\_count sometimes returns value smaller than actual column count |  Major | . | Jason Harvey | Yuki Morishita |
| [CASSANDRA-5137](https://issues.apache.org/jira/browse/CASSANDRA-5137) | Make sure SSTables left over from compaction get deleted and logged |  Minor | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-5148](https://issues.apache.org/jira/browse/CASSANDRA-5148) | Add option to disable tcp\_nodelay |  Minor | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-5160](https://issues.apache.org/jira/browse/CASSANDRA-5160) | CFS.allUserDefined() doesn't exclude system\_auth and system\_traces keysapces |  Trivial | . | Jason Brown | Jason Brown |
| [CASSANDRA-4446](https://issues.apache.org/jira/browse/CASSANDRA-4446) | nodetool drain sometimes doesn't mark commitlog fully flushed |  Minor | Tools | Robert Coli | Jonathan Ellis |
| [CASSANDRA-5121](https://issues.apache.org/jira/browse/CASSANDRA-5121) | system.peers.tokens is empty after node restart |  Minor | . | Pierre Chalamet | Sylvain Lebresne |
| [CASSANDRA-5130](https://issues.apache.org/jira/browse/CASSANDRA-5130) | streaming from another node results in a bogus % streamed on that sstable |  Major | . | Michael Kjellman | Yuki Morishita |
| [CASSANDRA-5133](https://issues.apache.org/jira/browse/CASSANDRA-5133) | Nodes can't rejoin after stopping, when using GossipingPropertyFileSnitch |  Major | . | Matt Jurik | Brandon Williams |
| [CASSANDRA-5165](https://issues.apache.org/jira/browse/CASSANDRA-5165) | Need to trim DC and RACK names in cassandra-topology.properties file |  Minor | . | Alexey Zotov | Alexey Zotov |
| [CASSANDRA-4936](https://issues.apache.org/jira/browse/CASSANDRA-4936) | Less than operator when comparing timeuuids behaves as less than equal. |  Major | . | Cesar Lopez-Nataren | Sylvain Lebresne |
| [CASSANDRA-5167](https://issues.apache.org/jira/browse/CASSANDRA-5167) | Node isn't removed from system.peers after 'nodetool removenode' |  Minor | . | Nicolai Gylling | Brandon Williams |
| [CASSANDRA-5175](https://issues.apache.org/jira/browse/CASSANDRA-5175) | Unbounded (?) thread growth connecting to an removed node |  Minor | . | Janne Jalkanen | Vijay |
| [CASSANDRA-5153](https://issues.apache.org/jira/browse/CASSANDRA-5153) | max client timestamp |  Major | . | yangwei | Jonathan Ellis |
| [CASSANDRA-4795](https://issues.apache.org/jira/browse/CASSANDRA-4795) | replication, compaction, compression? options are not validated |  Minor | . | Brandon Williams | Dave Brosius |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4942](https://issues.apache.org/jira/browse/CASSANDRA-4942) | Pool [Compressed]RandomAccessReader on the partitioned read path |  Minor | . | Jonathan Ellis | Jonathan Ellis |


