
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.2.19 - 2014-09-18



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7595](https://issues.apache.org/jira/browse/CASSANDRA-7595) | EmbeddedCassandraService class should provide a stop method |  Minor | . | Mirko Tschäni | Lyuben Todorov |
| [CASSANDRA-7788](https://issues.apache.org/jira/browse/CASSANDRA-7788) | Improve PasswordAuthenticator default super user setup |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7470](https://issues.apache.org/jira/browse/CASSANDRA-7470) | java.lang.AssertionError are causing cql responses to always go to streamId = 0 instead of the request sending streamId |  Trivial | CQL | Dominic Letz | Tyler Hobbs |
| [CASSANDRA-7543](https://issues.apache.org/jira/browse/CASSANDRA-7543) | Assertion error when compacting large row with map//list field or range tombstone |  Major | . | Matt Byrd | Yuki Morishita |
| [CASSANDRA-5481](https://issues.apache.org/jira/browse/CASSANDRA-5481) | CQLSH exception handling could leave a session in a bad state |  Minor | . | Jordan Pittier | Jordan Pittier |
| [CASSANDRA-7668](https://issues.apache.org/jira/browse/CASSANDRA-7668) | Make gc\_grace\_seconds 7 days for system tables |  Minor | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-7669](https://issues.apache.org/jira/browse/CASSANDRA-7669) | nodetool fails to connect when ipv6 host is specified |  Trivial | Tools | Adam Holmberg | Adam Holmberg |
| [CASSANDRA-7663](https://issues.apache.org/jira/browse/CASSANDRA-7663) | Removing a seed causes previously removed seeds to reappear |  Major | . | Richard Low | Brandon Williams |
| [CASSANDRA-7478](https://issues.apache.org/jira/browse/CASSANDRA-7478) | StorageService.getJoiningNodes returns duplicate ips |  Major | . | Nick Bailey | Jonathan Ellis |
| [CASSANDRA-7745](https://issues.apache.org/jira/browse/CASSANDRA-7745) | Background LCS compactions stall with pending compactions remaining |  Minor | Compaction | J.B. Langston | Yuki Morishita |
| [CASSANDRA-7758](https://issues.apache.org/jira/browse/CASSANDRA-7758) | Some gossip messages are very slow to process on vnode clusters |  Major | . | Rick Branson | Rick Branson |
| [CASSANDRA-7798](https://issues.apache.org/jira/browse/CASSANDRA-7798) | Empty clustering column not caught for CQL3 update to compact storage counter table |  Major | . | Jeremiah Jordan | Aleksey Yeschenko |
| [CASSANDRA-7808](https://issues.apache.org/jira/browse/CASSANDRA-7808) | LazilyCompactedRow incorrectly handles row tombstones |  Major | . | Richard Low | Richard Low |
| [CASSANDRA-7810](https://issues.apache.org/jira/browse/CASSANDRA-7810) | tombstones gc'd before being locally applied |  Major | Compaction | Jonathan Halliday | Marcus Eriksson |
| [CASSANDRA-7145](https://issues.apache.org/jira/browse/CASSANDRA-7145) | FileNotFoundException during compaction |  Major | Compaction | PJ | Marcus Eriksson |
| [CASSANDRA-7601](https://issues.apache.org/jira/browse/CASSANDRA-7601) | Data loss after nodetool taketoken |  Minor | Testing | Philip Thompson | Brandon Williams |
| [CASSANDRA-7828](https://issues.apache.org/jira/browse/CASSANDRA-7828) | New node cannot be joined if a value in composite type column is dropped (description updated) |  Major | . | Igor Zubchenok | Mikhail Stepura |


