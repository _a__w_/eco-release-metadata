
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.7.7 - 2011-07-16



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2733](https://issues.apache.org/jira/browse/CASSANDRA-2733) | nodetool ring with EC2Snitch, NPE checking for the zone and dc |  Minor | . | Vijay | Vijay |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2646](https://issues.apache.org/jira/browse/CASSANDRA-2646) | Cli should be able to specify a limit for get\_slice |  Minor | Tools | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2690](https://issues.apache.org/jira/browse/CASSANDRA-2690) | Make the release build fail if the publish to central repository also fails |  Major | Packaging | Stephen Connolly | Stephen Connolly |
| [CASSANDRA-2693](https://issues.apache.org/jira/browse/CASSANDRA-2693) | Renaming/Dropping Keyspace shouldn't touch hints |  Trivial | . | Nicholas Telford | Nicholas Telford |
| [CASSANDRA-2714](https://issues.apache.org/jira/browse/CASSANDRA-2714) | Throttle migration replay |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2716](https://issues.apache.org/jira/browse/CASSANDRA-2716) | avoid allocating a new serializer per ColumnFamily (row) |  Trivial | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2654](https://issues.apache.org/jira/browse/CASSANDRA-2654) | Work around native heap leak in sun.nio.ch.Util affecting IncomingTcpConnection |  Major | . | Hannes Schmidt | Hannes Schmidt |
| [CASSANDRA-2832](https://issues.apache.org/jira/browse/CASSANDRA-2832) | reduce variance in HH impact between wide and narrow rows |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2841](https://issues.apache.org/jira/browse/CASSANDRA-2841) | Always use even distribution for merkle tree with RandomPartitionner |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2652](https://issues.apache.org/jira/browse/CASSANDRA-2652) | Hinted handoff needs to adjust page size for lage columns to avoid OOM |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2660](https://issues.apache.org/jira/browse/CASSANDRA-2660) | BRAF.sync() bug can cause massive commit log write magnification |  Minor | . | Peter Schuller | Peter Schuller |
| [CASSANDRA-2668](https://issues.apache.org/jira/browse/CASSANDRA-2668) | don't perform HH to client-mode nodes |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2684](https://issues.apache.org/jira/browse/CASSANDRA-2684) | IntergerType uses Thrift method that attempts to unsafely access backing array of ByteBuffer and fails |  Minor | . | Ed Anuff | Ed Anuff |
| [CASSANDRA-2685](https://issues.apache.org/jira/browse/CASSANDRA-2685) | NPE in Table.createReplicationStrategy during sends from HintedHandOffManager |  Minor | . | Marko Mikulicic | Jonathan Ellis |
| [CASSANDRA-2680](https://issues.apache.org/jira/browse/CASSANDRA-2680) | range scan doesn't repair missing rows |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2675](https://issues.apache.org/jira/browse/CASSANDRA-2675) | java.io.IOError: java.io.EOFException with version 0.7.6 |  Minor | . | rene kochen | Sylvain Lebresne |
| [CASSANDRA-2673](https://issues.apache.org/jira/browse/CASSANDRA-2673) | AssertionError post truncate |  Minor | . | Marko Mikulicic | Jonathan Ellis |
| [CASSANDRA-2590](https://issues.apache.org/jira/browse/CASSANDRA-2590) | row delete breaks read repair |  Minor | . | amorton | amorton |
| [CASSANDRA-2669](https://issues.apache.org/jira/browse/CASSANDRA-2669) | Scrub does not close files |  Minor | Tools | Daniel Doubleday | Jonathan Ellis |
| [CASSANDRA-2164](https://issues.apache.org/jira/browse/CASSANDRA-2164) | debian build dep on ant-optional is missing |  Minor | . | Peter Schuller | Peter Schuller |
| [CASSANDRA-2792](https://issues.apache.org/jira/browse/CASSANDRA-2792) | Bootstrapping node stalls. Bootstrapper thinks it is still streaming some sstables. The source nodes do not. Caused by IllegalStateException on source nodes. |  Major | . | Dominic Williams | Sylvain Lebresne |
| [CASSANDRA-2817](https://issues.apache.org/jira/browse/CASSANDRA-2817) | Expose number of threads blocked on submitting a memtable for flush |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2755](https://issues.apache.org/jira/browse/CASSANDRA-2755) | ColumnFamilyRecordWriter fails to throw a write exception encountered after the user begins to close the writer |  Minor | . | Greg Katz | mck |
| [CASSANDRA-2837](https://issues.apache.org/jira/browse/CASSANDRA-2837) | Setting RR Chance via CliClient results in chance being too low |  Minor | . | Bill de hOra | Jon Hermes |
| [CASSANDRA-2653](https://issues.apache.org/jira/browse/CASSANDRA-2653) | index scan errors out when zero columns are requested |  Minor | . | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-2773](https://issues.apache.org/jira/browse/CASSANDRA-2773) | "Index manager cannot support deleting and inserting into a row in the same mutation" |  Critical | . | Boris Yen | Jonathan Ellis |
| [CASSANDRA-2852](https://issues.apache.org/jira/browse/CASSANDRA-2852) | Cassandra CLI - Import Keyspace Definitions from File - Comments do partitially interpret characters/commands |  Trivial | Tools | jens mueller | Pavel Yaskevich |
| [CASSANDRA-2800](https://issues.apache.org/jira/browse/CASSANDRA-2800) | OPP#describeOwnership reports incorrect ownership |  Minor | . | Jon Hermes | Jon Hermes |
| [CASSANDRA-2762](https://issues.apache.org/jira/browse/CASSANDRA-2762) | Token cannot contain comma (possibly non-alpha/non-numeric too?) in OrderPreservingPartitioner |  Minor | . | Jackson Chung | Jonathan Ellis |
| [CASSANDRA-2839](https://issues.apache.org/jira/browse/CASSANDRA-2839) | Small typos in the cli |  Minor | . | Joaquin Casares | Joaquin Casares |


