
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.2.8 - 2016-09-28



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12030](https://issues.apache.org/jira/browse/CASSANDRA-12030) | Range tombstones that are masked by row tombstones should not be written out |  Minor | . | Nachiket Patil | Nachiket Patil |
| [CASSANDRA-11715](https://issues.apache.org/jira/browse/CASSANDRA-11715) | Make GCInspector's MIN\_LOG\_DURATION configurable |  Minor | . | Brandon Williams | Jeff Jirsa |
| [CASSANDRA-12040](https://issues.apache.org/jira/browse/CASSANDRA-12040) |   If a level compaction fails due to no space it should schedule the next one |  Minor | . | sankalp kohli | sankalp kohli |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11973](https://issues.apache.org/jira/browse/CASSANDRA-11973) | Is MemoryUtil.getShort() supposed to return a sign-extended or non-sign-extended value? |  Minor | Core | Rei Odaira | Rei Odaira |
| [CASSANDRA-12112](https://issues.apache.org/jira/browse/CASSANDRA-12112) | Tombstone histogram not accounting for partition deletions |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-11349](https://issues.apache.org/jira/browse/CASSANDRA-11349) | MerkleTree mismatch when multiple range tombstones exists for the same partition and interval |  Major | . | Fabien Rousseau | Branimir Lambov |
| [CASSANDRA-11414](https://issues.apache.org/jira/browse/CASSANDRA-11414) | dtest failure in bootstrap\_test.TestBootstrap.resumable\_bootstrap\_test |  Major | Testing | Philip Thompson | Paulo Motta |
| [CASSANDRA-12143](https://issues.apache.org/jira/browse/CASSANDRA-12143) | NPE when trying to remove purgable tombstones from result |  Critical | . | mck | mck |
| [CASSANDRA-12146](https://issues.apache.org/jira/browse/CASSANDRA-12146) | Use dedicated executor for sending JMX notifications |  Major | Observability | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-12105](https://issues.apache.org/jira/browse/CASSANDRA-12105) | ThriftServer.stop is not thread safe |  Minor | . | Brian Wawok | Brian Wawok |
| [CASSANDRA-12214](https://issues.apache.org/jira/browse/CASSANDRA-12214) |  cqlshlib test failure: cqlshlib.test.remove\_test\_db |  Major | Testing | Craig Kodman | Stefania |
| [CASSANDRA-11850](https://issues.apache.org/jira/browse/CASSANDRA-11850) | cannot use cql since upgrading python to 2.7.11+ |  Major | Tools | Andrew Madison | Stefania |
| [CASSANDRA-11979](https://issues.apache.org/jira/browse/CASSANDRA-11979) | cqlsh copyutil should get host metadata by connected address |  Minor | Tools | Adam Holmberg | Stefania |
| [CASSANDRA-11465](https://issues.apache.org/jira/browse/CASSANDRA-11465) | dtest failure in cql\_tracing\_test.TestCqlTracing.tracing\_unknown\_impl\_test |  Major | Observability | Philip Thompson | Stefania |
| [CASSANDRA-12351](https://issues.apache.org/jira/browse/CASSANDRA-12351) | IllegalStateException: empty rows returned when reading system.schema\_keyspaces |  Major | Local Write-Read Paths | mck | Sylvain Lebresne |
| [CASSANDRA-11828](https://issues.apache.org/jira/browse/CASSANDRA-11828) | Commit log needs to track unflushed intervals rather than positions |  Major | Local Write-Read Paths | Branimir Lambov | Branimir Lambov |
| [CASSANDRA-12312](https://issues.apache.org/jira/browse/CASSANDRA-12312) | Restore JVM metric export for metric reporters |  Major | . | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-12371](https://issues.apache.org/jira/browse/CASSANDRA-12371) | INSERT JSON - numbers not accepted for smallint and tinyint |  Minor | CQL | Paweł Rychlik | Paweł Rychlik |
| [CASSANDRA-10992](https://issues.apache.org/jira/browse/CASSANDRA-10992) | Hanging streaming sessions |  Major | Streaming and Messaging | mlowicki | Paulo Motta |
| [CASSANDRA-11752](https://issues.apache.org/jira/browse/CASSANDRA-11752) | histograms/metrics in 2.2 do not appear recency biased |  Major | Core | Chris Burroughs | Per Otterström |
| [CASSANDRA-9507](https://issues.apache.org/jira/browse/CASSANDRA-9507) | range metrics are not updated for timeout and unavailable in StorageProxy |  Minor | Observability | sankalp kohli | Nachiket Patil |
| [CASSANDRA-11356](https://issues.apache.org/jira/browse/CASSANDRA-11356) | EC2MRS ignores broadcast\_rpc\_address setting in cassandra.yaml |  Major | Core | Thanh | Paulo Motta |
| [CASSANDRA-12445](https://issues.apache.org/jira/browse/CASSANDRA-12445) | StreamingTransferTest.testTransferRangeTombstones failure |  Major | Streaming and Messaging, Testing | Joel Knighton | Paulo Motta |
| [CASSANDRA-11345](https://issues.apache.org/jira/browse/CASSANDRA-11345) | Assertion Errors "Memory was freed" during streaming |  Major | Streaming and Messaging | Jean-Francois Gosselin | Paulo Motta |
| [CASSANDRA-12127](https://issues.apache.org/jira/browse/CASSANDRA-12127) | Queries with empty ByteBuffer values in clustering column restrictions fail for non-composite compact tables |  Major | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-12251](https://issues.apache.org/jira/browse/CASSANDRA-12251) | Move migration tasks to non-periodic queue, assure flush executor shutdown after non-periodic executor |  Major | Lifecycle | Philip Thompson | Alex Petrov |
| [CASSANDRA-12476](https://issues.apache.org/jira/browse/CASSANDRA-12476) | SyntaxException when COPY FROM Counter Table with Null value |  Minor | Tools | Ashraful Islam | Stefania |
| [CASSANDRA-12528](https://issues.apache.org/jira/browse/CASSANDRA-12528) | Fix eclipse-warning problems |  Major | Core | Joel Knighton | Sam Tunnicliffe |
| [CASSANDRA-12279](https://issues.apache.org/jira/browse/CASSANDRA-12279) | nodetool repair hangs on non-existant table |  Minor | . | Benjamin Roth | Masataka Yamaguchi |
| [CASSANDRA-12522](https://issues.apache.org/jira/browse/CASSANDRA-12522) | nodetool repair -pr and -local option rejected |  Major | Tools | Jérôme Mainaud | Jérôme Mainaud |
| [CASSANDRA-8523](https://issues.apache.org/jira/browse/CASSANDRA-8523) | Writes should be sent to a replacement node which has a new IP while it is streaming in data |  Major | . | Richard Wagner | Paulo Motta |
| [CASSANDRA-12481](https://issues.apache.org/jira/browse/CASSANDRA-12481) | dtest failure in cqlshlib.test.test\_cqlsh\_output.TestCqlshOutput.test\_describe\_keyspace\_output |  Major | Testing | Craig Kodman | Stefania |
| [CASSANDRA-11332](https://issues.apache.org/jira/browse/CASSANDRA-11332) | nodes connect to themselves when NTS is used |  Major | Core | Brandon Williams | Branimir Lambov |
| [CASSANDRA-11363](https://issues.apache.org/jira/browse/CASSANDRA-11363) | High Blocked NTR When Connecting |  Major | Coordination | Russell Bradberry | T Jake Luciani |
| [CASSANDRA-12554](https://issues.apache.org/jira/browse/CASSANDRA-12554) | updateJobs in PendingRangeCalculatorService should be decremented in finally block |  Minor | Distributed Metadata | sankalp kohli | sankalp kohli |
| [CASSANDRA-12642](https://issues.apache.org/jira/browse/CASSANDRA-12642) | cqlsh NoHostsAvailable/AuthenticationFailure when sourcing a file with COPY commands |  Minor | Tools | Adam Holmberg | Stefania |
| [CASSANDRA-12253](https://issues.apache.org/jira/browse/CASSANDRA-12253) | Fix exceptions when enabling gossip on proxy nodes. |  Minor | Distributed Metadata | Dikang Gu | Dikang Gu |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12375](https://issues.apache.org/jira/browse/CASSANDRA-12375) | dtest failure in read\_repair\_test.TestReadRepair.test\_gcable\_tombstone\_resurrection\_on\_range\_slice\_query |  Major | Testing | Craig Kodman | Sylvain Lebresne |
| [CASSANDRA-11701](https://issues.apache.org/jira/browse/CASSANDRA-11701) | [windows] dtest failure in cqlsh\_tests.cqlsh\_copy\_tests.CqlshCopyTest.test\_reading\_with\_skip\_and\_max\_rows |  Major | Tools | Russ Hatch | Stefania |


