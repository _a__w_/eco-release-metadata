
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1.7 - 2015-06-22



### IMPORTANT ISSUES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9549](https://issues.apache.org/jira/browse/CASSANDRA-9549) | Memory leak in Ref.GlobalState due to pathological ConcurrentLinkedQueue.remove behaviour |  Critical | . | Ivar Thorson | Benedict |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9529](https://issues.apache.org/jira/browse/CASSANDRA-9529) | Standardize quoting in Unix scripts |  Minor | Tools | Stefania | Stefania |
| [CASSANDRA-9571](https://issues.apache.org/jira/browse/CASSANDRA-9571) | Set HAS\_MORE\_PAGES flag when PagingState is null |  Minor | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-9592](https://issues.apache.org/jira/browse/CASSANDRA-9592) | \`Periodically attempt to submit background compaction tasks |  Trivial | . | Benedict | Benedict |
| [CASSANDRA-9496](https://issues.apache.org/jira/browse/CASSANDRA-9496) | ArrivalWindow should use primitives |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-9532](https://issues.apache.org/jira/browse/CASSANDRA-9532) | Provide access to select statement's real column definitions |  Major | CQL | mck | Sam Tunnicliffe |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9569](https://issues.apache.org/jira/browse/CASSANDRA-9569) | nodetool should exit with status code != 0 if NodeProbe fails |  Major | Tools | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9572](https://issues.apache.org/jira/browse/CASSANDRA-9572) | DateTieredCompactionStrategy fails to combine SSTables correctly when TTL is used. |  Major | Compaction | Antti Nissinen | Marcus Eriksson |
| [CASSANDRA-9119](https://issues.apache.org/jira/browse/CASSANDRA-9119) | Nodetool rebuild creates an additional rebuild session even if there is one already running |  Major | Tools | Jose Martinez Poblete | Yuki Morishita |
| [CASSANDRA-9580](https://issues.apache.org/jira/browse/CASSANDRA-9580) | Cardinality check broken during incremental compaction re-opening |  Minor | Compaction | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-9565](https://issues.apache.org/jira/browse/CASSANDRA-9565) | 'WITH WITH' in alter keyspace statements causes NPE |  Major | . | Jim Witschey | Benjamin Lerer |


