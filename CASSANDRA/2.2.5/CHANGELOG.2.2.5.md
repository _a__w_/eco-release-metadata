
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.2.5 - 2016-02-08



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9303](https://issues.apache.org/jira/browse/CASSANDRA-9303) | Match cassandra-loader options in COPY FROM |  Critical | Tools | Jonathan Ellis | Stefania |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9474](https://issues.apache.org/jira/browse/CASSANDRA-9474) | Validate dc information on startup |  Major | . | Marcus Olsson | Marcus Olsson |
| [CASSANDRA-9748](https://issues.apache.org/jira/browse/CASSANDRA-9748) | Can't see other nodes when using multiple network interfaces |  Minor | Streaming and Messaging | Roman Bielik | Paulo Motta |
| [CASSANDRA-9302](https://issues.apache.org/jira/browse/CASSANDRA-9302) | Optimize cqlsh COPY FROM, part 3 |  Critical | Tools | Jonathan Ellis | Stefania |
| [CASSANDRA-9556](https://issues.apache.org/jira/browse/CASSANDRA-9556) | Add newer data types to cassandra stress |  Minor | Tools | Jeremy Hanna | ZhaoYang |
| [CASSANDRA-9294](https://issues.apache.org/jira/browse/CASSANDRA-9294) | Streaming errors should log the root cause |  Major | Streaming and Messaging | Brandon Williams | Paulo Motta |
| [CASSANDRA-9977](https://issues.apache.org/jira/browse/CASSANDRA-9977) | Support counter-columns for native aggregates (sum,avg,max,min) |  Major | CQL | Noam Liran | Robert Stupp |
| [CASSANDRA-10140](https://issues.apache.org/jira/browse/CASSANDRA-10140) | Enable GC logging by default |  Minor | Configuration | Chris Lohfink | Chris Lohfink |
| [CASSANDRA-10847](https://issues.apache.org/jira/browse/CASSANDRA-10847) | Log a message when refusing a major compaction due to incremental repairedAt status |  Trivial | Compaction | Brandon Williams | Marcus Eriksson |
| [CASSANDRA-7925](https://issues.apache.org/jira/browse/CASSANDRA-7925) | TimeUUID LSB should be unique per process, not just per machine |  Major | . | Peter Mädel | Sylvain Lebresne |
| [CASSANDRA-10025](https://issues.apache.org/jira/browse/CASSANDRA-10025) | Allow compaction throttle to be real time |  Minor | . | sankalp kohli | Soumava Ghosh |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10018](https://issues.apache.org/jira/browse/CASSANDRA-10018) | Stats for several pools removed from nodetool tpstats output |  Major | Observability | Sam Tunnicliffe | Joel Knighton |
| [CASSANDRA-10585](https://issues.apache.org/jira/browse/CASSANDRA-10585) | SSTablesPerReadHistogram seems wrong when row cache hit happend |  Minor | Observability | Ivan Burmistrov | Ivan Burmistrov |
| [CASSANDRA-10816](https://issues.apache.org/jira/browse/CASSANDRA-10816) | Explicitly handle SSL handshake errors during connect() |  Major | Streaming and Messaging | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-10835](https://issues.apache.org/jira/browse/CASSANDRA-10835) | CqlInputFormat  creates too small splits for map Hadoop tasks |  Major | . | Artem Aliev |  |
| [CASSANDRA-8805](https://issues.apache.org/jira/browse/CASSANDRA-8805) | runWithCompactionsDisabled only cancels compactions, which is not the only source of markCompacted |  Major | Compaction | Benedict | Carl Yeksigian |
| [CASSANDRA-9179](https://issues.apache.org/jira/browse/CASSANDRA-9179) | Unable to "point in time" restore if table/cf has been recreated |  Major | CQL, Distributed Metadata | Jon Moses | Branimir Lambov |
| [CASSANDRA-10541](https://issues.apache.org/jira/browse/CASSANDRA-10541) | cqlshlib tests cannot run on Windows |  Minor | Tools | Benjamin Lerer | Paulo Motta |
| [CASSANDRA-9813](https://issues.apache.org/jira/browse/CASSANDRA-9813) | cqlsh column header can be incorrect when no rows are returned |  Major | . | Aleksey Yeschenko | Adam Holmberg |
| [CASSANDRA-10593](https://issues.apache.org/jira/browse/CASSANDRA-10593) | Unintended interactions between commitlog archiving and commitlog recycling |  Major | Local Write-Read Paths | J.B. Langston | Ariel Weisberg |
| [CASSANDRA-10856](https://issues.apache.org/jira/browse/CASSANDRA-10856) | Upgrading section in NEWS for 2.2 is misplaced |  Major | . | Jim Witschey | Sylvain Lebresne |
| [CASSANDRA-10850](https://issues.apache.org/jira/browse/CASSANDRA-10850) | v4 spec has tons of grammatical mistakes |  Major | Documentation and Website | Sandeep Tamhankar | Sandeep Tamhankar |
| [CASSANDRA-10701](https://issues.apache.org/jira/browse/CASSANDRA-10701) | stop referring to batches as atomic |  Minor | . | Jon Haddad | Sylvain Lebresne |
| [CASSANDRA-10854](https://issues.apache.org/jira/browse/CASSANDRA-10854) | cqlsh COPY FROM csv having line with more than one consecutive  ',' delimiter  is throwing 'list index out of range' |  Minor | Tools | Puspendu Banerjee | Stefania |
| [CASSANDRA-10875](https://issues.apache.org/jira/browse/CASSANDRA-10875) | cqlsh fails to decode utf-8 characters for text typed columns. |  Minor | Tools | Yasuharu Goto | Yasuharu Goto |
| [CASSANDRA-9309](https://issues.apache.org/jira/browse/CASSANDRA-9309) | Wrong interpretation of Config.getOutboundBindAny depending on using SSL or not |  Major | Streaming and Messaging | Casey Marshall | Yuki Morishita |
| [CASSANDRA-10817](https://issues.apache.org/jira/browse/CASSANDRA-10817) | DROP USER is not case-sensitive |  Minor | CQL | Mike Adamson | Marcus Eriksson |
| [CASSANDRA-10902](https://issues.apache.org/jira/browse/CASSANDRA-10902) | Skip saved cache directory when checking SSTables at startup |  Major | Configuration | Carl Yeksigian | Carl Yeksigian |
| [CASSANDRA-9258](https://issues.apache.org/jira/browse/CASSANDRA-9258) | Range movement causes CPU & performance impact |  Major | . | Rick Branson | Dikang Gu |
| [CASSANDRA-8708](https://issues.apache.org/jira/browse/CASSANDRA-8708) | inter\_dc\_stream\_throughput\_outbound\_megabits\_per\_sec to defaults to unlimited |  Major | Streaming and Messaging | Adam Hattrell | Jeremy Hanna |
| [CASSANDRA-10975](https://issues.apache.org/jira/browse/CASSANDRA-10975) | Histogram buckets exposed in jmx are sorted by count |  Major | Observability | Chris Lohfink | Chris Lohfink |
| [CASSANDRA-10887](https://issues.apache.org/jira/browse/CASSANDRA-10887) | Pending range calculator gives wrong pending ranges for moves |  Critical | Coordination | Richard Low | sankalp kohli |
| [CASSANDRA-8072](https://issues.apache.org/jira/browse/CASSANDRA-8072) | Exception during startup: Unable to gossip with any seeds |  Major | Lifecycle | Ryan Springer | Stefania |
| [CASSANDRA-10676](https://issues.apache.org/jira/browse/CASSANDRA-10676) | AssertionError in CompactionExecutor |  Major | Compaction | mlowicki | Carl Yeksigian |
| [CASSANDRA-10959](https://issues.apache.org/jira/browse/CASSANDRA-10959) | missing timeout option propagation in cqlsh (cqlsh.py) |  Minor | Tools | Julien Blondeau | Julien Blondeau |
| [CASSANDRA-10477](https://issues.apache.org/jira/browse/CASSANDRA-10477) | java.lang.AssertionError in StorageProxy.submitHint |  Major | Local Write-Read Paths | Severin Leonhardt | Ariel Weisberg |
| [CASSANDRA-10686](https://issues.apache.org/jira/browse/CASSANDRA-10686) | cqlsh schema refresh on timeout dtest is flaky |  Minor | Testing, Tools | Joel Knighton | Paulo Motta |
| [CASSANDRA-10961](https://issues.apache.org/jira/browse/CASSANDRA-10961) | Not enough bytes error when add nodes to cluster |  Major | Streaming and Messaging | xiaost | Paulo Motta |
| [CASSANDRA-9465](https://issues.apache.org/jira/browse/CASSANDRA-9465) | No client warning on tombstone threshold |  Minor | Observability | Adam Holmberg | Carl Yeksigian |
| [CASSANDRA-10980](https://issues.apache.org/jira/browse/CASSANDRA-10980) | nodetool scrub NPEs when keyspace isn't specified |  Trivial | Compaction, Tools | Will Hayworth | Yuki Morishita |
| [CASSANDRA-10997](https://issues.apache.org/jira/browse/CASSANDRA-10997) | cqlsh\_copy\_tests failing en mass when vnodes are disabled |  Major | Tools | Philip Thompson | Stefania |
| [CASSANDRA-11005](https://issues.apache.org/jira/browse/CASSANDRA-11005) | Split consistent range movement flag |  Trivial | Configuration | sankalp kohli | sankalp kohli |
| [CASSANDRA-10829](https://issues.apache.org/jira/browse/CASSANDRA-10829) | cleanup + repair generates a lot of logs |  Major | Streaming and Messaging | Fabien Rousseau | Marcus Eriksson |
| [CASSANDRA-10909](https://issues.apache.org/jira/browse/CASSANDRA-10909) | NPE in ActiveRepairService |  Major | Streaming and Messaging | Eduard Tudenhoefner | Marcus Eriksson |
| [CASSANDRA-10979](https://issues.apache.org/jira/browse/CASSANDRA-10979) | LCS doesn't do L0 STC on new tables while an L0-\>L1 compaction is in progress |  Major | Compaction | Jeff Ferland | Carl Yeksigian |
| [CASSANDRA-8521](https://issues.apache.org/jira/browse/CASSANDRA-8521) | RangeSliceQueryPager may fetch too much data in the first partition |  Minor | . | Tyler Hobbs | Sylvain Lebresne |
| [CASSANDRA-10969](https://issues.apache.org/jira/browse/CASSANDRA-10969) | long-running cluster sees bad gossip generation when a node restarts |  Major | Coordination | T. David Hudson | Joel Knighton |
| [CASSANDRA-10955](https://issues.apache.org/jira/browse/CASSANDRA-10955) | Multi-partitions queries with ORDER BY can result in a NPE |  Major | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-10948](https://issues.apache.org/jira/browse/CASSANDRA-10948) | CQLSH error when trying to insert non-ascii statement |  Minor | Tools | Matthieu Nantern | Matthieu Nantern |
| [CASSANDRA-11007](https://issues.apache.org/jira/browse/CASSANDRA-11007) | Exception when running nodetool info during bootstrap |  Minor | Tools | T Jake Luciani | Yuki Morishita |
| [CASSANDRA-9949](https://issues.apache.org/jira/browse/CASSANDRA-9949) | maxPurgeableTimestamp needs to check memtables too |  Major | Local Write-Read Paths | Jonathan Ellis | Stefania |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10799](https://issues.apache.org/jira/browse/CASSANDRA-10799) | 2 cqlshlib tests still failing with cythonized driver installation |  Major | Testing | Stefania | Stefania |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10761](https://issues.apache.org/jira/browse/CASSANDRA-10761) | Possible regression of CASSANDRA-9201 |  Major | Distributed Metadata | Philip Thompson | Sam Tunnicliffe |
| [CASSANDRA-10935](https://issues.apache.org/jira/browse/CASSANDRA-10935) | sstableloader\_uppercase\_keyspace\_name\_test is failing on 2.2 |  Major | Tools | Philip Thompson | Marcus Eriksson |
| [CASSANDRA-10938](https://issues.apache.org/jira/browse/CASSANDRA-10938) | test\_bulk\_round\_trip\_blogposts is failing occasionally |  Major | Tools | Stefania | Stefania |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10904](https://issues.apache.org/jira/browse/CASSANDRA-10904) | Add upgrade procedure related to new role based access control in NEWS.txt |  Major | Documentation and Website | Reynald Bourtembourg |  |


