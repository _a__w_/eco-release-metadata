
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.0 beta 1 - 2015-08-24



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6434](https://issues.apache.org/jira/browse/CASSANDRA-6434) | Repair-aware gc grace period |  Major | . | sankalp kohli | Marcus Eriksson |
| [CASSANDRA-7771](https://issues.apache.org/jira/browse/CASSANDRA-7771) | Allow multiple 2ndary index on the same column |  Major | CQL | Sylvain Lebresne | Sam Tunnicliffe |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9827](https://issues.apache.org/jira/browse/CASSANDRA-9827) | Add consistency level to tracing ouput |  Minor | . | Alec Grieser |  |
| [CASSANDRA-9853](https://issues.apache.org/jira/browse/CASSANDRA-9853) | loadConfig() called twice on startup |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-9890](https://issues.apache.org/jira/browse/CASSANDRA-9890) | Bytecode inspection for Java-UDFs |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-9889](https://issues.apache.org/jira/browse/CASSANDRA-9889) | Disable scripted UDFs by default |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-9792](https://issues.apache.org/jira/browse/CASSANDRA-9792) | Reduce Merkle tree serialized size |  Minor | . | Bharatendra Boddu | Bharatendra Boddu |
| [CASSANDRA-9471](https://issues.apache.org/jira/browse/CASSANDRA-9471) | Columns should be backed by a BTree, not an array |  Major | . | Benedict | Benedict |
| [CASSANDRA-7237](https://issues.apache.org/jira/browse/CASSANDRA-7237) | Optimize batchlog manager to avoid full scans |  Minor | . | Aleksey Yeschenko | Branimir Lambov |
| [CASSANDRA-9265](https://issues.apache.org/jira/browse/CASSANDRA-9265) | Add checksum to saved cache files |  Major | . | Ariel Weisberg | Daniel Chia |
| [CASSANDRA-9533](https://issues.apache.org/jira/browse/CASSANDRA-9533) | Make batch commitlog mode easier to tune |  Major | . | Jonathan Ellis | Benedict |
| [CASSANDRA-8684](https://issues.apache.org/jira/browse/CASSANDRA-8684) | Replace usage of Adler32 with CRC32 |  Major | Local Write-Read Paths | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-10015](https://issues.apache.org/jira/browse/CASSANDRA-10015) | Create tool to debug why expired sstables are not getting dropped |  Major | Tools | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-5220](https://issues.apache.org/jira/browse/CASSANDRA-5220) | Repair improvements when using vnodes |  Major | . | Brandon Williams | Marcus Olsson |
| [CASSANDRA-9926](https://issues.apache.org/jira/browse/CASSANDRA-9926) | Remove AbstractRow.isEmpty |  Trivial | . | Benedict | Benedict |
| [CASSANDRA-9932](https://issues.apache.org/jira/browse/CASSANDRA-9932) | Make all partitions btree backed |  Major | . | Benedict | Benedict |
| [CASSANDRA-9237](https://issues.apache.org/jira/browse/CASSANDRA-9237) | Gossip messages subject to head of line blocking by other intra-cluster traffic |  Major | Streaming and Messaging | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-10004](https://issues.apache.org/jira/browse/CASSANDRA-10004) | Allow changing default encoding on cqlsh |  Minor | Tools | Paulo Motta | Paulo Motta |
| [CASSANDRA-9500](https://issues.apache.org/jira/browse/CASSANDRA-9500) | SequentialWriter should extend BufferedDataOutputStreamPlus |  Minor | Compaction, Local Write-Read Paths, Streaming and Messaging | Benedict | Ariel Weisberg |
| [CASSANDRA-10056](https://issues.apache.org/jira/browse/CASSANDRA-10056) | Fix AggregationTest post-test error messages |  Trivial | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-10114](https://issues.apache.org/jira/browse/CASSANDRA-10114) | Allow count(\*) and count(1) to be use as normal aggregation |  Minor | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-8671](https://issues.apache.org/jira/browse/CASSANDRA-8671) | Give compaction strategy more control over where sstables are created, including for flushing and streaming. |  Major | . | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-10083](https://issues.apache.org/jira/browse/CASSANDRA-10083) | Revert AutoSavingCache.IStreamFactory to return OutputStream |  Major | . | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-9459](https://issues.apache.org/jira/browse/CASSANDRA-9459) | SecondaryIndex API redesign |  Major | CQL, Local Write-Read Paths | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-8143](https://issues.apache.org/jira/browse/CASSANDRA-8143) | Partitioner should not be accessed through StorageService |  Major | . | Branimir Lambov | Branimir Lambov |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9880](https://issues.apache.org/jira/browse/CASSANDRA-9880) | ScrubTest.testScrubOutOfOrder should generate test file on the fly |  Blocker | Testing | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-9871](https://issues.apache.org/jira/browse/CASSANDRA-9871) | Cannot replace token does not exist - DN node removed as Fat Client |  Major | Distributed Metadata | Sebastian Estevez | Stefania |
| [CASSANDRA-9959](https://issues.apache.org/jira/browse/CASSANDRA-9959) | Expected bloom filter size should not be an int |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9963](https://issues.apache.org/jira/browse/CASSANDRA-9963) | Compaction not starting for new tables |  Major | Compaction | Jeremiah Jordan | Marcus Eriksson |
| [CASSANDRA-9968](https://issues.apache.org/jira/browse/CASSANDRA-9968) | cqlsh describe bug |  Major | . | T Jake Luciani | Adam Holmberg |
| [CASSANDRA-9031](https://issues.apache.org/jira/browse/CASSANDRA-9031) | nodetool info -T throws ArrayOutOfBounds when the node has not joined the cluster |  Major | Tools | Ron Kuris | Yuki Morishita |
| [CASSANDRA-9962](https://issues.apache.org/jira/browse/CASSANDRA-9962) | WaitQueueTest is flakey |  Minor | . | Benedict | Benedict |
| [CASSANDRA-9992](https://issues.apache.org/jira/browse/CASSANDRA-9992) | Sending batchlog verb to previous versions |  Major | Coordination | Carl Yeksigian | Carl Yeksigian |
| [CASSANDRA-9993](https://issues.apache.org/jira/browse/CASSANDRA-9993) | Unused verb for MV |  Minor | . | Carl Yeksigian | Aleksey Yeschenko |
| [CASSANDRA-9998](https://issues.apache.org/jira/browse/CASSANDRA-9998) | LEAK DETECTED with snapshot/sequential repairs |  Major | Streaming and Messaging | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9978](https://issues.apache.org/jira/browse/CASSANDRA-9978) | Split/Scrub tools no longer remove original sstable |  Minor | Tools | T Jake Luciani | Stefania |
| [CASSANDRA-10001](https://issues.apache.org/jira/browse/CASSANDRA-10001) | Bug in merging of collections |  Blocker | Local Write-Read Paths | T Jake Luciani | Stefania |
| [CASSANDRA-10002](https://issues.apache.org/jira/browse/CASSANDRA-10002) | Repeated slices on RowSearchers are incorrect |  Major | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-9777](https://issues.apache.org/jira/browse/CASSANDRA-9777) | If you have a ~/.cqlshrc and a ~/.cassandra/cqlshrc, cqlsh will overwrite the latter with the former |  Major | . | Jon Moses | David Kua |
| [CASSANDRA-10000](https://issues.apache.org/jira/browse/CASSANDRA-10000) | Dates before 1970-01-01 are not formatted correctly on cqlsh\\Windows |  Minor | Tools | Paulo Motta | Paulo Motta |
| [CASSANDRA-9965](https://issues.apache.org/jira/browse/CASSANDRA-9965) | Add new JMX methods to change local compaction strategy |  Minor | Compaction | Aleksey Yeschenko | Marcus Eriksson |
| [CASSANDRA-9997](https://issues.apache.org/jira/browse/CASSANDRA-9997) | Document removal of cold\_reads\_to\_omit in 2.2 and 3.0 NEWS.txt |  Minor | Documentation and Website | Tommy Stendahl | Marcus Eriksson |
| [CASSANDRA-9913](https://issues.apache.org/jira/browse/CASSANDRA-9913) | Select \* is only returning the first page of data on trunk |  Major | CQL | Philip Thompson | Benjamin Lerer |
| [CASSANDRA-10014](https://issues.apache.org/jira/browse/CASSANDRA-10014) | Deletions using clustering keys not reflected in MV |  Major | Coordination, Materialized Views | Stefan Podkowinski | Carl Yeksigian |
| [CASSANDRA-10026](https://issues.apache.org/jira/browse/CASSANDRA-10026) | AccessControlException in UFPureScriptTest |  Major | Testing | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-10006](https://issues.apache.org/jira/browse/CASSANDRA-10006) | 2.1 format sstable filenames with "tmp" are not handled by 3.0 |  Major | Local Write-Read Paths | Tyler Hobbs | Stefania |
| [CASSANDRA-9908](https://issues.apache.org/jira/browse/CASSANDRA-9908) | Potential race caused by async cleanup of transaction log files |  Major | Local Write-Read Paths | Sam Tunnicliffe | Stefania |
| [CASSANDRA-10019](https://issues.apache.org/jira/browse/CASSANDRA-10019) | UFPureScriptTest.testJavascriptTupleType fails on uninitialized thread id |  Major | Local Write-Read Paths, Testing | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-10003](https://issues.apache.org/jira/browse/CASSANDRA-10003) | RowAndDeletionMergeIteratorTest is failing on 3.0 and trunk |  Blocker | . | Yuki Morishita | Daniel Chia |
| [CASSANDRA-10040](https://issues.apache.org/jira/browse/CASSANDRA-10040) | Fix CASSANDRA-9771 |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-9462](https://issues.apache.org/jira/browse/CASSANDRA-9462) | ViewTest.sstableInBounds is failing |  Major | Local Write-Read Paths, Streaming and Messaging | Benedict | Ariel Weisberg |
| [CASSANDRA-8515](https://issues.apache.org/jira/browse/CASSANDRA-8515) | Commit log stop policy not enforced correctly during startup |  Minor | Coordination, Lifecycle | Richard Low | Paulo Motta |
| [CASSANDRA-9970](https://issues.apache.org/jira/browse/CASSANDRA-9970) | CQL 3.3 ordering in where clause produces different results when using "IN...ORDER BY" |  Minor | CQL | Marc Zbyszynski | Benjamin Lerer |
| [CASSANDRA-10073](https://issues.apache.org/jira/browse/CASSANDRA-10073) | Counter mutation serialization bug |  Critical | . | T Jake Luciani | Sylvain Lebresne |
| [CASSANDRA-10093](https://issues.apache.org/jira/browse/CASSANDRA-10093) | Invalid internal query for static compact tables |  Major | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-9882](https://issues.apache.org/jira/browse/CASSANDRA-9882) | DTCS (maybe other strategies) can block flushing when there are lots of sstables |  Major | Compaction | Jeremiah Jordan | Marcus Eriksson |
| [CASSANDRA-10121](https://issues.apache.org/jira/browse/CASSANDRA-10121) | Fix \*NEW\* failing pig unit tests |  Major | . | Aleksey Yeschenko | Blake Eggleston |
| [CASSANDRA-9111](https://issues.apache.org/jira/browse/CASSANDRA-9111) | SSTables originated from the same incremental repair session have different repairedAt timestamps |  Major | . | prmg |  |
| [CASSANDRA-9749](https://issues.apache.org/jira/browse/CASSANDRA-9749) | CommitLogReplayer continues startup after encountering errors |  Major | . | Blake Eggleston | Branimir Lambov |
| [CASSANDRA-10016](https://issues.apache.org/jira/browse/CASSANDRA-10016) | Materialized view metrics pushes out tpstats formatting |  Minor | Materialized Views, Tools | Sam Tunnicliffe | Paulo Motta |
| [CASSANDRA-10125](https://issues.apache.org/jira/browse/CASSANDRA-10125) | ReadFailure is thrown instead of ReadTimeout for range queries |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-10152](https://issues.apache.org/jira/browse/CASSANDRA-10152) | dtest: user\_functions\_test.py:TestUserFunctions.udf\_scripting\_test fails |  Major | . | Joshua McKenzie | Robert Stupp |
| [CASSANDRA-10049](https://issues.apache.org/jira/browse/CASSANDRA-10049) | Commitlog initialization failure |  Major | . | T Jake Luciani | Branimir Lambov |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9764](https://issues.apache.org/jira/browse/CASSANDRA-9764) | dtest for many UPDATE batches, low contention fails on trunk |  Blocker | . | Jim Witschey | Sylvain Lebresne |
| [CASSANDRA-9868](https://issues.apache.org/jira/browse/CASSANDRA-9868) | Archive commitlogs tests failing |  Blocker | Testing | Shawn Kumar | Ariel Weisberg |
| [CASSANDRA-9775](https://issues.apache.org/jira/browse/CASSANDRA-9775) | some paging dtests fail/flap on trunk |  Blocker | CQL | Jim Witschey | Sylvain Lebresne |
| [CASSANDRA-9704](https://issues.apache.org/jira/browse/CASSANDRA-9704) | On-wire backward compatibility for 8099 |  Major | . | Sylvain Lebresne | Tyler Hobbs |
| [CASSANDRA-9985](https://issues.apache.org/jira/browse/CASSANDRA-9985) | Introduce our own AbstractIterator |  Trivial | . | Benedict | Benedict |
| [CASSANDRA-7342](https://issues.apache.org/jira/browse/CASSANDRA-7342) | CAS writes do not have hint functionality. |  Major | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-9894](https://issues.apache.org/jira/browse/CASSANDRA-9894) | Serialize the header only once per message |  Major | . | Sylvain Lebresne | Benedict |
| [CASSANDRA-9717](https://issues.apache.org/jira/browse/CASSANDRA-9717) | TestCommitLog segment size dtests fail on trunk |  Blocker | . | Jim Witschey | Jim Witschey |
| [CASSANDRA-9426](https://issues.apache.org/jira/browse/CASSANDRA-9426) | Provide a per-table text-\>blob map for storing extra metadata |  Major | CQL, Distributed Metadata | Aleksey Yeschenko | Benjamin Lerer |
| [CASSANDRA-6230](https://issues.apache.org/jira/browse/CASSANDRA-6230) | Write hints to flat files instead of the system.hints |  Major | . | Jonathan Ellis | Aleksey Yeschenko |
| [CASSANDRA-10128](https://issues.apache.org/jira/browse/CASSANDRA-10128) | Windows dtest 3.0: consistent\_reads\_after\_write\_test (materialized\_views\_test.TestMaterializedViewsConsistency) timing out |  Major | Materialized Views, Testing | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-10100](https://issues.apache.org/jira/browse/CASSANDRA-10100) | Windows dtest 3.0: commitlog\_test.py:TestCommitLog.stop\_failure\_policy\_test fails |  Major | . | Joshua McKenzie | Paulo Motta |
| [CASSANDRA-10106](https://issues.apache.org/jira/browse/CASSANDRA-10106) | Windows dtest 3.0: TestRepair multiple failures |  Major | Testing | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-10098](https://issues.apache.org/jira/browse/CASSANDRA-10098) | Windows dtest 3.0: commitlog\_test.py:TestCommitLog.small\_segment\_size\_test fails |  Major | Testing | Joshua McKenzie | Paulo Motta |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9737](https://issues.apache.org/jira/browse/CASSANDRA-9737) | Log warning when using an aggregate without partition key |  Trivial | . | Christopher Batey | Robert Stupp |
| [CASSANDRA-9483](https://issues.apache.org/jira/browse/CASSANDRA-9483) | Document incompatibilities with -XX:+PerfDisableSharedMem |  Minor | Configuration, Documentation and Website | Tyler Hobbs | T Jake Luciani |
| [CASSANDRA-9927](https://issues.apache.org/jira/browse/CASSANDRA-9927) | Security for MaterializedViews |  Major | Coordination, Materialized Views | T Jake Luciani | Paulo Motta |
| [CASSANDRA-9416](https://issues.apache.org/jira/browse/CASSANDRA-9416) | 3.x should refuse to start on JVM\_VERSION \< 1.8 |  Minor | . | Michael Shuler | Philip Thompson |


