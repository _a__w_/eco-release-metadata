
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1.2 - 2014-11-10



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7927](https://issues.apache.org/jira/browse/CASSANDRA-7927) | Kill daemon on any disk error |  Major | . | John Sumsion | John Sumsion |
| [CASSANDRA-7579](https://issues.apache.org/jira/browse/CASSANDRA-7579) | File descriptor exhaustion can lead to unreliable state in exception condition |  Minor | Observability | Joshua McKenzie | Joshua McKenzie |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-8109](https://issues.apache.org/jira/browse/CASSANDRA-8109) | Avoid constant boxing in ColumnStats.{Min/Max}Tracker |  Minor | . | Sylvain Lebresne | Rajanarayanan Thottuvaikkatumana |
| [CASSANDRA-7463](https://issues.apache.org/jira/browse/CASSANDRA-7463) | Update CQLSSTableWriter to allow parallel writing of SSTables on the same table within the same JVM |  Major | . | Johnny Miller | Carl Yeksigian |
| [CASSANDRA-4959](https://issues.apache.org/jira/browse/CASSANDRA-4959) | CQLSH insert help has typo |  Trivial | Documentation and Website | Edward Capriolo | Ricardo Devis Agullo |
| [CASSANDRA-7852](https://issues.apache.org/jira/browse/CASSANDRA-7852) | Refactor/document how we track live size |  Minor | Observability | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-8171](https://issues.apache.org/jira/browse/CASSANDRA-8171) | Clean up generics |  Minor | . | Branimir Lambov | Branimir Lambov |
| [CASSANDRA-8188](https://issues.apache.org/jira/browse/CASSANDRA-8188) | don't block SocketThread for MessagingService |  Major | . | yangwei | yangwei |
| [CASSANDRA-8125](https://issues.apache.org/jira/browse/CASSANDRA-8125) | nodetool statusgossip doesn't exist |  Minor | . | Connor Warrington | Jan Karlsson |
| [CASSANDRA-6482](https://issues.apache.org/jira/browse/CASSANDRA-6482) | Add junitreport to ant test target |  Trivial | Testing | Michael Shuler | Mikhail Stepura |
| [CASSANDRA-8240](https://issues.apache.org/jira/browse/CASSANDRA-8240) | Attach existing sources to "Referenced libraries" in Eclipse project |  Trivial | . | Mikhail Stepura | Mikhail Stepura |
| [CASSANDRA-8250](https://issues.apache.org/jira/browse/CASSANDRA-8250) | Eclipse project recompiles Thrift classes again |  Trivial | Tools | Mikhail Stepura | Mikhail Stepura |
| [CASSANDRA-7979](https://issues.apache.org/jira/browse/CASSANDRA-7979) | Acceptable time skew for C\* |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-8096](https://issues.apache.org/jira/browse/CASSANDRA-8096) | Make cache serializers pluggable |  Minor | . | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-8230](https://issues.apache.org/jira/browse/CASSANDRA-8230) | LongToken no longer needs to use a boxed Long |  Minor | . | Branimir Lambov | Branimir Lambov |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-8084](https://issues.apache.org/jira/browse/CASSANDRA-8084) | GossipFilePropertySnitch and EC2MultiRegionSnitch when used in AWS/GCE clusters doesnt use the PRIVATE IPS for Intra-DC communications - When running nodetool repair |  Major | Configuration | Jana | Yuki Morishita |
| [CASSANDRA-8144](https://issues.apache.org/jira/browse/CASSANDRA-8144) | Creating CQL2 tables fails in C\* 2.1 |  Major | . | Sam Tunnicliffe | Aleksey Yeschenko |
| [CASSANDRA-8166](https://issues.apache.org/jira/browse/CASSANDRA-8166) | Not all data is loaded to Pig using CqlNativeStorage |  Major | . | Oksana Danylyshyn | Alex Liu |
| [CASSANDRA-8115](https://issues.apache.org/jira/browse/CASSANDRA-8115) | Windows install scripts fail to set logdir and datadir |  Major | Packaging | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-8116](https://issues.apache.org/jira/browse/CASSANDRA-8116) | HSHA fails with default rpc\_max\_threads setting |  Minor | . | Mike Adamson | Tyler Hobbs |
| [CASSANDRA-8196](https://issues.apache.org/jira/browse/CASSANDRA-8196) | Anticompaction live size bug |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-8182](https://issues.apache.org/jira/browse/CASSANDRA-8182) | Comparison method violates its general contract in IndexSummaryManager.redistributeSummaries |  Minor | . | Juho Mäkinen | Tyler Hobbs |
| [CASSANDRA-8139](https://issues.apache.org/jira/browse/CASSANDRA-8139) | The WRITETIME function returns null for negative timestamp values |  Minor | CQL | Richard Bremner | Sylvain Lebresne |
| [CASSANDRA-8176](https://issues.apache.org/jira/browse/CASSANDRA-8176) | Intermittent NPE from RecoveryManagerTest RecoverPIT unit test |  Major | Testing | Michael Shuler | Sam Tunnicliffe |
| [CASSANDRA-8205](https://issues.apache.org/jira/browse/CASSANDRA-8205) | ColumnFamilyMetrics#totalDiskSpaceUsed gets wrong value when SSTable is deleted |  Minor | Observability | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-8031](https://issues.apache.org/jira/browse/CASSANDRA-8031) | Custom Index describe broken again |  Major | Tools | Jeremiah Jordan | Tyler Hobbs |
| [CASSANDRA-8179](https://issues.apache.org/jira/browse/CASSANDRA-8179) | No error message shown when starting Cassandra on Windows if Cassandra is already running |  Minor | Packaging | Philip Thompson | Joshua McKenzie |
| [CASSANDRA-8136](https://issues.apache.org/jira/browse/CASSANDRA-8136) | Windows Service never finishes shutting down |  Minor | Lifecycle | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-8124](https://issues.apache.org/jira/browse/CASSANDRA-8124) | Stopping a node during compaction can make already written files stay around |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-7510](https://issues.apache.org/jira/browse/CASSANDRA-7510) | Notify clients that bootstrap is finished over binary protocol |  Minor | . | Joost Reuzel | Brandon Williams |
| [CASSANDRA-8247](https://issues.apache.org/jira/browse/CASSANDRA-8247) | HHOM creates repeated 'No files to compact for user defined compaction' messages |  Minor | . | Brandon Williams | Carl Yeksigian |
| [CASSANDRA-8246](https://issues.apache.org/jira/browse/CASSANDRA-8246) | Default timestamp for QueryOptions should be Long.MIN\_VALUE |  Major | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-8239](https://issues.apache.org/jira/browse/CASSANDRA-8239) | NamesQueryFilters do not update SSTable read rate metrics |  Blocker | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-8147](https://issues.apache.org/jira/browse/CASSANDRA-8147) | Secondary indexing of map keys does not work properly when mixing contains and contains\_key |  Minor | CQL, Secondary Indexes | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-8206](https://issues.apache.org/jira/browse/CASSANDRA-8206) | Deleting columns breaks secondary index on clustering column |  Critical | Secondary Indexes | Tuukka Mustonen | Sylvain Lebresne |
| [CASSANDRA-8178](https://issues.apache.org/jira/browse/CASSANDRA-8178) | Column names are not converted correctly for non-text comparators |  Major | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-8004](https://issues.apache.org/jira/browse/CASSANDRA-8004) | Run LCS for both repaired and unrepaired data |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-8209](https://issues.apache.org/jira/browse/CASSANDRA-8209) | Cassandra crashes when running on JDK8 update 40 |  Critical | Configuration, Packaging | Jaroslav Kamenik | T Jake Luciani |
| [CASSANDRA-8258](https://issues.apache.org/jira/browse/CASSANDRA-8258) | SELECT ... TOKEN() function broken in C\* 2.1.1 |  Major | . | Lex Lythius | Philip Thompson |
| [CASSANDRA-8262](https://issues.apache.org/jira/browse/CASSANDRA-8262) | parse\_for\_table\_meta errors out on queries with undefined grammars |  Major | . | Philip Thompson | Philip Thompson |
| [CASSANDRA-8213](https://issues.apache.org/jira/browse/CASSANDRA-8213) | Grant Permission fails if permission had been revoked previously |  Major | . | Philip Thompson | Aleksey Yeschenko |


