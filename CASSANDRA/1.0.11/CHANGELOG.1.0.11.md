
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.0.11 - 2012-07-31



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4010](https://issues.apache.org/jira/browse/CASSANDRA-4010) | Report thrift status in nodetool info |  Minor | Tools | Radim Kolar | Dave Brosius |
| [CASSANDRA-4240](https://issues.apache.org/jira/browse/CASSANDRA-4240) | Only check the size of indexed column values when they are of type KEYS |  Minor | . | Joaquin Casares | Alex Liu |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4061](https://issues.apache.org/jira/browse/CASSANDRA-4061) | Decommission should take a token |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-4113](https://issues.apache.org/jira/browse/CASSANDRA-4113) | cqlsh does not work with piping |  Major | . | Pavel Matveyev | paul cannon |
| [CASSANDRA-4223](https://issues.apache.org/jira/browse/CASSANDRA-4223) | Non Unique Streaming session ID's |  Major | . | amorton | amorton |
| [CASSANDRA-4255](https://issues.apache.org/jira/browse/CASSANDRA-4255) | concurrent modif ex when repair is run on LCS |  Minor | . | Jackson Chung | Jonathan Ellis |
| [CASSANDRA-4266](https://issues.apache.org/jira/browse/CASSANDRA-4266) | Validate compression parameters |  Minor | . | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-4270](https://issues.apache.org/jira/browse/CASSANDRA-4270) | long-test broken due to incorrect config option |  Minor | Testing | Tyler Patterson | Tyler Patterson |
| [CASSANDRA-4279](https://issues.apache.org/jira/browse/CASSANDRA-4279) | kick off background compaction when min/max changed |  Trivial | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4287](https://issues.apache.org/jira/browse/CASSANDRA-4287) | SizeTieredCompactionStrategy.getBuckets is quadradic in the number of sstables |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4291](https://issues.apache.org/jira/browse/CASSANDRA-4291) | Oversize integer in CQL throws NumberFormatException |  Minor | CQL | Scott Sadler | Dave Brosius |
| [CASSANDRA-4314](https://issues.apache.org/jira/browse/CASSANDRA-4314) | Index CF tombstones can cause OOM |  Critical | . | Wade Poziombka | Jonathan Ellis |
| [CASSANDRA-4396](https://issues.apache.org/jira/browse/CASSANDRA-4396) | Subcolumns not removed when compacting tombstoned super column |  Minor | . | Nick Bailey | Jonathan Ellis |
| [CASSANDRA-4195](https://issues.apache.org/jira/browse/CASSANDRA-4195) | error in log when upgrading multi-node cluster to 1.1 |  Major | . | Tyler Patterson | Pavel Yaskevich |
| [CASSANDRA-4419](https://issues.apache.org/jira/browse/CASSANDRA-4419) | leveled compaction generates too small sstables |  Minor | . | Noa Resare | Sylvain Lebresne |
| [CASSANDRA-3636](https://issues.apache.org/jira/browse/CASSANDRA-3636) | cassandra 1.0.x breakes APT on debian OpenVZ |  Minor | Packaging | Zenek Kraweznik | paul cannon |
| [CASSANDRA-4462](https://issues.apache.org/jira/browse/CASSANDRA-4462) | upgradesstables strips active data from sstables |  Major | . | Mike Heffner | Sylvain Lebresne |


