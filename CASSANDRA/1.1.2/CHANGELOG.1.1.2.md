
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.1.2 - 2012-07-02



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4012](https://issues.apache.org/jira/browse/CASSANDRA-4012) | Load-from-flat-file data import tool |  Major | Tools | Jonathan Ellis | paul cannon |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4108](https://issues.apache.org/jira/browse/CASSANDRA-4108) | Add an option to cqlsh to authenticate to a keyspace by default |  Minor | . | Jeremy Hanna | paul cannon |
| [CASSANDRA-4306](https://issues.apache.org/jira/browse/CASSANDRA-4306) | enforce 1m min keycache for auto |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-4319](https://issues.apache.org/jira/browse/CASSANDRA-4319) | ex msg for cql3 order by constraints says primary filter can be an IN clause which is misleading |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-4341](https://issues.apache.org/jira/browse/CASSANDRA-4341) | Small SSTable Segments Can Hurt Leveling Process |  Minor | . | Benjamin Coverston | Jonathan Ellis |
| [CASSANDRA-4248](https://issues.apache.org/jira/browse/CASSANDRA-4248) | it'd be great to print the strategyOptions map in the KSMetatData.toString |  Minor | . | Jackson Chung | Vladimir Sverzhinsky |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3865](https://issues.apache.org/jira/browse/CASSANDRA-3865) | Cassandra-cli returns 'command not found' instead of syntax error |  Trivial | . | Eric Lubow | Dave Brosius |
| [CASSANDRA-4307](https://issues.apache.org/jira/browse/CASSANDRA-4307) | isMarkedForDelete can return false if it is a few seconds in the future |  Major | . | Wade Simmons | Sylvain Lebresne |
| [CASSANDRA-4314](https://issues.apache.org/jira/browse/CASSANDRA-4314) | Index CF tombstones can cause OOM |  Critical | . | Wade Poziombka | Jonathan Ellis |
| [CASSANDRA-4318](https://issues.apache.org/jira/browse/CASSANDRA-4318) | Nodetool compactionstats fails with NullPointerException |  Minor | . | David B | Jonathan Ellis |
| [CASSANDRA-4328](https://issues.apache.org/jira/browse/CASSANDRA-4328) | CQL client timeout when inserting data after creating index |  Major | . | Haralds Ulmanis | Sylvain Lebresne |
| [CASSANDRA-4320](https://issues.apache.org/jira/browse/CASSANDRA-4320) | Assertion error while delivering the hints. |  Minor | . | Vijay | Vijay |
| [CASSANDRA-4343](https://issues.apache.org/jira/browse/CASSANDRA-4343) | fix sstable blacklisting for LCS |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4344](https://issues.apache.org/jira/browse/CASSANDRA-4344) | Windows tools don't work and litter the environment |  Major | Tools | Holger Hoffstätte | Holger Hoffstätte |
| [CASSANDRA-4352](https://issues.apache.org/jira/browse/CASSANDRA-4352) | cqlsh: ASSUME functionality broken by CASSANDRA-4198 fix |  Minor | Tools | paul cannon | paul cannon |
| [CASSANDRA-4349](https://issues.apache.org/jira/browse/CASSANDRA-4349) | PFS should give a friendlier error message when a node has not been configured |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4353](https://issues.apache.org/jira/browse/CASSANDRA-4353) | no error propagated to client when updating a column family with an invalid column def |  Minor | CQL | Sam Overton | Sam Overton |
| [CASSANDRA-3942](https://issues.apache.org/jira/browse/CASSANDRA-3942) | ColumnFamilyRecordReader can report progress \> 100% |  Minor | . | T Jake Luciani | Brandon Williams |
| [CASSANDRA-4368](https://issues.apache.org/jira/browse/CASSANDRA-4368) | bulkLoad() method in StorageService throws AssertionError |  Major | Tools | Danny Wong | Brandon Williams |
| [CASSANDRA-4370](https://issues.apache.org/jira/browse/CASSANDRA-4370) | hsha server may stop responding and will not close selectors |  Major | . | Viktor Kuzmin | Viktor Kuzmin |
| [CASSANDRA-2760](https://issues.apache.org/jira/browse/CASSANDRA-2760) | NPE in sstable2json |  Minor | Tools | Danny Wang | Jason Brown |
| [CASSANDRA-4331](https://issues.apache.org/jira/browse/CASSANDRA-4331) | sstable2json error |  Major | . | ganghuang | Jonathan Ellis |
| [CASSANDRA-4372](https://issues.apache.org/jira/browse/CASSANDRA-4372) | CQL3 Range Query contains unwanted results with composite columns |  Major | CQL | Reinhard Buchinger | Sylvain Lebresne |
| [CASSANDRA-4379](https://issues.apache.org/jira/browse/CASSANDRA-4379) | cleanup optimization can delete data but not corresponding index entries |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4193](https://issues.apache.org/jira/browse/CASSANDRA-4193) | cql delete does not delete |  Major | . | Jackson Chung | Sylvain Lebresne |
| [CASSANDRA-4321](https://issues.apache.org/jira/browse/CASSANDRA-4321) | stackoverflow building interval tree & possible sstable corruptions |  Major | . | Anton Winter | Sylvain Lebresne |
| [CASSANDRA-4365](https://issues.apache.org/jira/browse/CASSANDRA-4365) | Use CF comparator to sort indexed columns in SecondaryIndexManager |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4275](https://issues.apache.org/jira/browse/CASSANDRA-4275) | Oracle Java 1.7 u4 does not allow Xss128k |  Major | . | Edward Capriolo | Sylvain Lebresne |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4217](https://issues.apache.org/jira/browse/CASSANDRA-4217) | Easy access to column timestamps (and maybe ttl) during queries |  Major | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3779](https://issues.apache.org/jira/browse/CASSANDRA-3779) | need forked language document |  Major | CQL | Eric Evans | Sylvain Lebresne |
| [CASSANDRA-4041](https://issues.apache.org/jira/browse/CASSANDRA-4041) | Allow updating column\_alias types |  Minor | CQL | Sylvain Lebresne | Pavel Yaskevich |


