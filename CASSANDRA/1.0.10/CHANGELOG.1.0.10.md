
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.0.10 - 2012-05-08



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3710](https://issues.apache.org/jira/browse/CASSANDRA-3710) | Add a configuration option to disable snapshots |  Minor | . | Brandon Williams | Dave Brosius |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4107](https://issues.apache.org/jira/browse/CASSANDRA-4107) | fix broken link in cassandra-env.sh |  Major | . | Ilya Shipitsin | Ilya Shipitsin |
| [CASSANDRA-3951](https://issues.apache.org/jira/browse/CASSANDRA-3951) | make thrift interface "backwards compat" guarantee more specific |  Minor | CQL | paul cannon | paul cannon |
| [CASSANDRA-4130](https://issues.apache.org/jira/browse/CASSANDRA-4130) | update snitch documentation |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3665](https://issues.apache.org/jira/browse/CASSANDRA-3665) | [patch] allow for clientutil.jar to be used without the base cassandra.jar for client applications |  Minor | . | Dave Brosius | Eric Evans |
| [CASSANDRA-4188](https://issues.apache.org/jira/browse/CASSANDRA-4188) | stress tool return a non-zero value when it fails |  Minor | Tools | Tyler Patterson | Tyler Patterson |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4078](https://issues.apache.org/jira/browse/CASSANDRA-4078) | StackOverflowError when upgrading to 1.0.8 from 0.8.10 |  Minor | . | Wenjun | paul cannon |
| [CASSANDRA-4090](https://issues.apache.org/jira/browse/CASSANDRA-4090) | cqlsh can't handle python being a python3 |  Trivial | Tools | Andrew Ash | Andrew Ash |
| [CASSANDRA-3732](https://issues.apache.org/jira/browse/CASSANDRA-3732) | Update POM generation after migration to git |  Minor | Packaging | Sylvain Lebresne | Stephen Connolly |
| [CASSANDRA-4129](https://issues.apache.org/jira/browse/CASSANDRA-4129) | Cannot create keyspace with specific keywords through cli |  Minor | . | Manoj Mainali | Pavel Yaskevich |
| [CASSANDRA-4145](https://issues.apache.org/jira/browse/CASSANDRA-4145) | NullPointerException when using sstableloader with PropertyFileSnitch configured |  Minor | Tools | Ji Cheng | Ji Cheng |
| [CASSANDRA-4159](https://issues.apache.org/jira/browse/CASSANDRA-4159) | isReadyForBootstrap doesn't compare schema UUID by timestamp as it should |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3946](https://issues.apache.org/jira/browse/CASSANDRA-3946) | BulkRecordWriter shouldn't stream any empty data/index files that might be created at end of flush |  Minor | . | Chris Goffinet | Yuki Morishita |
| [CASSANDRA-4190](https://issues.apache.org/jira/browse/CASSANDRA-4190) | Apparent data loss using super columns and row cache via ConcurrentLinkedHashCacheProvider |  Major | . | Mina Naguib | Sylvain Lebresne |
| [CASSANDRA-4204](https://issues.apache.org/jira/browse/CASSANDRA-4204) | Pig does not work on DateType |  Major | . | Jackson Chung | Brandon Williams |
| [CASSANDRA-4168](https://issues.apache.org/jira/browse/CASSANDRA-4168) | "Setup" section of tools/stress/README.txt needs update |  Minor | Tools | Tyler Patterson | Brandon Williams |
| [CASSANDRA-3985](https://issues.apache.org/jira/browse/CASSANDRA-3985) | Ensure a directory is selected for Compaction |  Minor | . | amorton | Jonathan Ellis |
| [CASSANDRA-4205](https://issues.apache.org/jira/browse/CASSANDRA-4205) | SSTables are not updated with max timestamp on upgradesstables/compaction leading to non-optimal performance. |  Critical | . | Thorkild Stray | Jonathan Ellis |


