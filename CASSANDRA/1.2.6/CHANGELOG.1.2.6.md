
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.2.6 - 2013-06-26



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2308](https://issues.apache.org/jira/browse/CASSANDRA-2308) | add tracing of cell name index effectiveness |  Minor | . | Jonathan Ellis | Jonathan Ellis |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5398](https://issues.apache.org/jira/browse/CASSANDRA-5398) | Remove localTimestamp from merkle-tree calculation (for tombstones) |  Minor | . | Christian Spriegel | Christian Spriegel |
| [CASSANDRA-5539](https://issues.apache.org/jira/browse/CASSANDRA-5539) | add custom prompt for cqlsh |  Trivial | Tools | Cyril Scetbon | Cyril Scetbon |
| [CASSANDRA-5594](https://issues.apache.org/jira/browse/CASSANDRA-5594) | CassandraAuthorizer.authorize(AuthenticatedUser, IResource) should use prepared statement |  Major | . | John Sanda | Aleksey Yeschenko |
| [CASSANDRA-5596](https://issues.apache.org/jira/browse/CASSANDRA-5596) | CREATE TABLE error message swaps table and keyspace |  Trivial | . | Lex Lythius | Jonathan Ellis |
| [CASSANDRA-5597](https://issues.apache.org/jira/browse/CASSANDRA-5597) | Allow switching to vertical output for SELECTs in cqlsh |  Trivial | . | Michał Michalski | Michał Michalski |
| [CASSANDRA-5272](https://issues.apache.org/jira/browse/CASSANDRA-5272) | Hinted Handoff Throttle based on cluster size |  Minor | . | Rick Branson | Jonathan Ellis |
| [CASSANDRA-5004](https://issues.apache.org/jira/browse/CASSANDRA-5004) | Add a rate limit option to stress |  Minor | Tools | Brandon Williams | Jason Brown |
| [CASSANDRA-5615](https://issues.apache.org/jira/browse/CASSANDRA-5615) | Allow Custom Indexes to Index Collections |  Major | . | Benjamin Coverston | Benjamin Coverston |
| [CASSANDRA-5618](https://issues.apache.org/jira/browse/CASSANDRA-5618) | Add metrics for read repair |  Minor | . | Rick Branson | Jingsi Zhu |
| [CASSANDRA-5638](https://issues.apache.org/jira/browse/CASSANDRA-5638) | Improve StorageProxy tracing |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5639](https://issues.apache.org/jira/browse/CASSANDRA-5639) | Update CREATE CUSTOM INDEX syntax to match new CREATE TRIGGER syntax |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5678](https://issues.apache.org/jira/browse/CASSANDRA-5678) | Avoid over reconnecting in EC2MRS |  Minor | . | Jason Brown | Jason Brown |
| [CASSANDRA-5694](https://issues.apache.org/jira/browse/CASSANDRA-5694) | Allow sstable2json to dump SecondaryIndex SSTables |  Minor | Tools | Michał Michalski | Michał Michalski |
| [CASSANDRA-4421](https://issues.apache.org/jira/browse/CASSANDRA-4421) | Support cql3 table definitions in Hadoop InputFormat |  Major | CQL | bert Passek | Alex Liu |
| [CASSANDRA-5630](https://issues.apache.org/jira/browse/CASSANDRA-5630) | config option to allow GossipingPropertyFileSnitch to use the "reconnect trick" |  Minor | . | Chris Burroughs | Chris Burroughs |
| [CASSANDRA-5524](https://issues.apache.org/jira/browse/CASSANDRA-5524) | Allow upgradesstables to be run against a specified directory |  Minor | Tools | Tyler Hobbs | Nick Bailey |
| [CASSANDRA-5492](https://issues.apache.org/jira/browse/CASSANDRA-5492) | Backport row-level bloom filter removal to 1.2 |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5681](https://issues.apache.org/jira/browse/CASSANDRA-5681) | Refactor IESCS in Snitches |  Minor | . | Jason Brown | Jason Brown |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5572](https://issues.apache.org/jira/browse/CASSANDRA-5572) | Write row markers when serializing columnfamilies and columns schema |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5569](https://issues.apache.org/jira/browse/CASSANDRA-5569) | Every stream operation requires checking indexes in every SSTable |  Minor | . | Rick Branson | Rick Branson |
| [CASSANDRA-4958](https://issues.apache.org/jira/browse/CASSANDRA-4958) | Snappy 1.0.4 doesn't work on OSX / Java 7 |  Minor | . | Colin Taylor | Yuki Morishita |
| [CASSANDRA-5529](https://issues.apache.org/jira/browse/CASSANDRA-5529) | thrift\_max\_message\_length\_in\_mb makes long-lived connections error out |  Major | CQL | Rob Timpe | Jonathan Ellis |
| [CASSANDRA-5584](https://issues.apache.org/jira/browse/CASSANDRA-5584) | Incorrect use of System.nanoTime() |  Trivial | . | Mikhail Mazurskiy | Mikhail Mazurskiy |
| [CASSANDRA-5314](https://issues.apache.org/jira/browse/CASSANDRA-5314) | Replaying old batches can 'undo' deletes |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5273](https://issues.apache.org/jira/browse/CASSANDRA-5273) | Hanging system after OutOfMemory. Server cannot die due to uncaughtException handling |  Minor | . | Ignace Desimpel | Jonathan Ellis |
| [CASSANDRA-5488](https://issues.apache.org/jira/browse/CASSANDRA-5488) | CassandraStorage throws NullPointerException (NPE) when widerows is set to 'true' |  Minor | . | Sheetal Gosrani | Sheetal Gosrani |
| [CASSANDRA-4655](https://issues.apache.org/jira/browse/CASSANDRA-4655) | Truncate operation doesn't delete rows from HintsColumnFamily. |  Minor | . | Alexey Zotov | Jonathan Ellis |
| [CASSANDRA-5589](https://issues.apache.org/jira/browse/CASSANDRA-5589) | ArrayIndexOutOfBoundsException in LeveledManifest |  Minor | . | Jeremy Hanna | Jonathan Ellis |
| [CASSANDRA-5478](https://issues.apache.org/jira/browse/CASSANDRA-5478) | Nodetool clearsnapshot incorrectly reports to have requested a snapshot |  Trivial | Tools | Geert Schuring | Dave Brosius |
| [CASSANDRA-5536](https://issues.apache.org/jira/browse/CASSANDRA-5536) | ColumnFamilyInputFormat demands OrderPreservingPartitioner when specifying InputRange with tokens |  Major | . | Lanny Ripple | Jonathan Ellis |
| [CASSANDRA-5588](https://issues.apache.org/jira/browse/CASSANDRA-5588) | Add get commands to nodetool for things with set |  Minor | Tools | Jeremiah Jordan | Michał Michalski |
| [CASSANDRA-5544](https://issues.apache.org/jira/browse/CASSANDRA-5544) | Hadoop jobs assigns only one mapper in task |  Major | . | Shamim Ahmed | Alex Liu |
| [CASSANDRA-5587](https://issues.apache.org/jira/browse/CASSANDRA-5587) | BulkLoader fails with NoSuchElementException |  Major | Tools | Julien Aymé | Julien Aymé |
| [CASSANDRA-5602](https://issues.apache.org/jira/browse/CASSANDRA-5602) | SnitchProperties uses GossipingPropertyFileSnitch log factory |  Trivial | . | Chris Burroughs | Chris Burroughs |
| [CASSANDRA-5610](https://issues.apache.org/jira/browse/CASSANDRA-5610) | ORDER BY desc breaks cqlsh COPY |  Minor | Tools | Jeremiah Jordan | Aleksey Yeschenko |
| [CASSANDRA-5616](https://issues.apache.org/jira/browse/CASSANDRA-5616) | CQL now() on prepared statements is evaluated at prepare time and not query execution time |  Major | CQL | Max Penet | Sylvain Lebresne |
| [CASSANDRA-5629](https://issues.apache.org/jira/browse/CASSANDRA-5629) | Incorrect handling of blob literals when the blob column is in reverse clustering order |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5643](https://issues.apache.org/jira/browse/CASSANDRA-5643) | error in help text of cassandra-cli (--tspw option shows SSL: full path to truststore) |  Trivial | Tools | Harry Metske | Harry Metske |
| [CASSANDRA-5644](https://issues.apache.org/jira/browse/CASSANDRA-5644) | Exception swallowing in ..net.MessagingService |  Minor | Configuration | Harry Metske | Harry Metske |
| [CASSANDRA-5647](https://issues.apache.org/jira/browse/CASSANDRA-5647) | Typo in error message in cqlsh (CQL3) when using more than one relation with a IN |  Trivial | Tools | Rui Vieira |  |
| [CASSANDRA-5622](https://issues.apache.org/jira/browse/CASSANDRA-5622) | Update CqlRecordWriter to conform to RecordWriter API |  Major | . | Jonathan Ellis | Alex Liu |
| [CASSANDRA-5652](https://issues.apache.org/jira/browse/CASSANDRA-5652) | Suppress custom exceptions thru jmx |  Trivial | Tools | Dave Brosius | Dave Brosius |
| [CASSANDRA-5655](https://issues.apache.org/jira/browse/CASSANDRA-5655) | Equals method in PermissionDetails causes StackOverflowException |  Minor | . | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-5422](https://issues.apache.org/jira/browse/CASSANDRA-5422) | Native protocol sanity check |  Major | CQL | Jonathan Ellis | Daniel Norberg |
| [CASSANDRA-5634](https://issues.apache.org/jira/browse/CASSANDRA-5634) | relocatingTokens should be ConcurrentMap |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5660](https://issues.apache.org/jira/browse/CASSANDRA-5660) | Gossiper incorrectly drops AppState for an upgrading node |  Minor | . | Jason Brown | Jason Brown |
| [CASSANDRA-5666](https://issues.apache.org/jira/browse/CASSANDRA-5666) | CQL3 should not allow ranges on the partition key without the token() method, even for byte ordered partitioner. |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5669](https://issues.apache.org/jira/browse/CASSANDRA-5669) | Connection thrashing in multi-region ec2 during upgrade, due to messaging version |  Minor | . | Jason Brown | Jason Brown |
| [CASSANDRA-5632](https://issues.apache.org/jira/browse/CASSANDRA-5632) | Cross-DC bandwidth-saving broken |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5668](https://issues.apache.org/jira/browse/CASSANDRA-5668) | NPE in net.OutputTcpConnection when tracing is enabled |  Major | . | Ryan McGuire | Jonathan Ellis |
| [CASSANDRA-5476](https://issues.apache.org/jira/browse/CASSANDRA-5476) | Exceptions in 1.1 nodes with 1.2 nodes in ring |  Major | . | John Watson |  |
| [CASSANDRA-5498](https://issues.apache.org/jira/browse/CASSANDRA-5498) | Possible NPE on EACH\_QUORUM writes |  Minor | . | Jason Brown | Jason Brown |
| [CASSANDRA-5673](https://issues.apache.org/jira/browse/CASSANDRA-5673) | NullPointerException on running instances |  Major | . | Sanjay |  |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5508](https://issues.apache.org/jira/browse/CASSANDRA-5508) | Expose whether jna is enabled and memory is locked via JMX |  Trivial | . | Jeremy Hanna | Dave Brosius |
| [CASSANDRA-5662](https://issues.apache.org/jira/browse/CASSANDRA-5662) | Wasted work in StorageService.initClient() |  Trivial | . | Adrian Nistor | Adrian Nistor |


