
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.6.10 - 2011-01-23



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1896](https://issues.apache.org/jira/browse/CASSANDRA-1896) | Improve throughput by adding buffering to the inter-node TCP communication |  Major | . | Tsiki | Brandon Williams |
| [CASSANDRA-2004](https://issues.apache.org/jira/browse/CASSANDRA-2004) | dynamic snitch doesn't collect local latencies |  Major | . | Brandon Williams | Brandon Williams |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1943](https://issues.apache.org/jira/browse/CASSANDRA-1943) | Addition of internode buffering broke Streaming |  Critical | . | Stu Hood |  |
| [CASSANDRA-1959](https://issues.apache.org/jira/browse/CASSANDRA-1959) | java.lang.ArrayIndexOutOfBoundsException while executing repair on a freshly added node (0.7.0) |  Major | . | Thibaut | Jonathan Ellis |


