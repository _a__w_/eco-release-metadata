
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.8.9 - 2011-12-14



### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3556](https://issues.apache.org/jira/browse/CASSANDRA-3556) | nodetool info reports inaccurate datacenter/rack for localhost |  Minor | Tools | Rick Branson | Rick Branson |
| [CASSANDRA-3422](https://issues.apache.org/jira/browse/CASSANDRA-3422) | Can create a Column Family with comparator CounterColumnType which is subsequently unusable |  Minor | . | Kelley Reynolds | Sylvain Lebresne |
| [CASSANDRA-2189](https://issues.apache.org/jira/browse/CASSANDRA-2189) | json2sstable fails due to OutOfMemory |  Minor | Tools | Shotaro Kamio | Jonathan Ellis |
| [CASSANDRA-3577](https://issues.apache.org/jira/browse/CASSANDRA-3577) | TimeoutException When using QuorumEach or ALL consistency on Multi-DC |  Major | . | Vijay | Vijay |
| [CASSANDRA-3598](https://issues.apache.org/jira/browse/CASSANDRA-3598) | Index Scan's will span across multiple DC's |  Minor | . | Vijay | Vijay |
| [CASSANDRA-3485](https://issues.apache.org/jira/browse/CASSANDRA-3485) | Gossiper.addSavedEndpoint should never add itself |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3580](https://issues.apache.org/jira/browse/CASSANDRA-3580) | Don't assume the Table instance has been open when dropping a keyspace |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |


