
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.2.0 rc2 - 2012-12-21



### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5064](https://issues.apache.org/jira/browse/CASSANDRA-5064) | 'Alter table' when it includes collections makes cqlsh hang |  Critical | . | Ryan McGuire | Sylvain Lebresne |
| [CASSANDRA-5072](https://issues.apache.org/jira/browse/CASSANDRA-5072) | Bug in creating EnumSet in SimpleAuthorizer example |  Minor | . | John Sanda | Aleksey Yeschenko |
| [CASSANDRA-5058](https://issues.apache.org/jira/browse/CASSANDRA-5058) | debian packaging should include shuffle |  Major | Packaging | Brandon Williams | Sylvain Lebresne |
| [CASSANDRA-5065](https://issues.apache.org/jira/browse/CASSANDRA-5065) | nodetool ownership is incorrect with vnodes |  Major | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-5053](https://issues.apache.org/jira/browse/CASSANDRA-5053) | not possible to change crc\_check\_chance |  Minor | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-5076](https://issues.apache.org/jira/browse/CASSANDRA-5076) | Murmur3Partitioner#describeOwnership calculates ownership% wrong |  Minor | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-4598](https://issues.apache.org/jira/browse/CASSANDRA-4598) | describeOwnership() in Murmur3Partitioner.java doesn't work for close tokens |  Minor | . | Julien Lambert |  |
| [CASSANDRA-5079](https://issues.apache.org/jira/browse/CASSANDRA-5079) | Compaction deletes ExpiringColumns in Secondary Indexes |  Major | Secondary Indexes | amorton | amorton |


