
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.7 beta 2 - 2010-10-01



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-891](https://issues.apache.org/jira/browse/CASSANDRA-891) | Add ValidateWith to CF definitions |  Minor | . | Jonathan Ellis | Jon Hermes |
| [CASSANDRA-1285](https://issues.apache.org/jira/browse/CASSANDRA-1285) | Have a way to increase the replication factor |  Major | . | Arya Goudarzi | Gary Dusbabek |
| [CASSANDRA-1368](https://issues.apache.org/jira/browse/CASSANDRA-1368) | Add output support for Hadoop Streaming |  Major | . | Stu Hood | Stu Hood |
| [CASSANDRA-1469](https://issues.apache.org/jira/browse/CASSANDRA-1469) | adaptive default heap size |  Major | Packaging | Jonathan Ellis | Brandon Williams |
| [CASSANDRA-1314](https://issues.apache.org/jira/browse/CASSANDRA-1314) | snitch that prefers a single replica for all reads to a given key |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1415](https://issues.apache.org/jira/browse/CASSANDRA-1415) | Allow creating indexes on existing data |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1485](https://issues.apache.org/jira/browse/CASSANDRA-1485) | Add a weighted request scheduler |  Minor | . | Jeremy Hanna | Jeremy Hanna |
| [CASSANDRA-1488](https://issues.apache.org/jira/browse/CASSANDRA-1488) | Allow index sampling ratio to be configured in storage-conf |  Minor | . | Jonathan Ellis | Jon Hermes |
| [CASSANDRA-1490](https://issues.apache.org/jira/browse/CASSANDRA-1490) | Expose partitioner and snitch |  Minor | CQL, Tools | paul cannon | Jon Hermes |
| [CASSANDRA-1489](https://issues.apache.org/jira/browse/CASSANDRA-1489) | Expose progress made on PendingStreams through JMX |  Minor | Tools | paul cannon | Nick Bailey |
| [CASSANDRA-1511](https://issues.apache.org/jira/browse/CASSANDRA-1511) | log when a "repair" operation completes |  Critical | . | Robert Coli | Stu Hood |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1307](https://issues.apache.org/jira/browse/CASSANDRA-1307) | Get the 'system' keyspace info |  Minor | . | Ching-Shen Chen | Gary Dusbabek |
| [CASSANDRA-1347](https://issues.apache.org/jira/browse/CASSANDRA-1347) | Move JVM\_OPTS from cassandra.in.sh to the conf directory |  Minor | Packaging | Nick Bailey | Nick Bailey |
| [CASSANDRA-1189](https://issues.apache.org/jira/browse/CASSANDRA-1189) | Refactor streaming |  Critical | . | Gary Dusbabek | Nirmal Ranganathan |
| [CASSANDRA-1363](https://issues.apache.org/jira/browse/CASSANDRA-1363) | describe\_keyspace should expose ColumDefs |  Minor | . | Tyler Hobbs | Jon Hermes |
| [CASSANDRA-1322](https://issues.apache.org/jira/browse/CASSANDRA-1322) | Remove cassandra.yaml from Hadoop/Client Code |  Major | . | Jonathan Ellis | Stu Hood |
| [CASSANDRA-1386](https://issues.apache.org/jira/browse/CASSANDRA-1386) | Even faster UUID comparisons |  Minor | . | Folke Behrens | Folke Behrens |
| [CASSANDRA-1396](https://issues.apache.org/jira/browse/CASSANDRA-1396) | Merge SP.mutate() and SP.mutateBlocking() (but using a writeResponseHandler that is a noop) |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-1393](https://issues.apache.org/jira/browse/CASSANDRA-1393) | Faster LongType comparison |  Major | . | Folke Behrens | Folke Behrens |
| [CASSANDRA-1319](https://issues.apache.org/jira/browse/CASSANDRA-1319) | TimestampReconciler should be a singleton |  Trivial | . | Gary Dusbabek | Folke Behrens |
| [CASSANDRA-1401](https://issues.apache.org/jira/browse/CASSANDRA-1401) | allow all operators in secondary clauses to index query |  Minor | Secondary Indexes | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1395](https://issues.apache.org/jira/browse/CASSANDRA-1395) | NEWS.txt could use an "Upgrading" section for 0.7.0 |  Minor | Documentation and Website | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-1389](https://issues.apache.org/jira/browse/CASSANDRA-1389) | Schema migrations are noisy |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-1388](https://issues.apache.org/jira/browse/CASSANDRA-1388) | Allow column\_metadata w/ standard columns in SuperColFamily |  Major | . | Tyler Hobbs | Jon Hermes |
| [CASSANDRA-1214](https://issues.apache.org/jira/browse/CASSANDRA-1214) | Force linux to not swap the JVM |  Major | . | James Golick | Jonathan Ellis |
| [CASSANDRA-1414](https://issues.apache.org/jira/browse/CASSANDRA-1414) | Upgrade from 0.6 to 0.7 is bumpy if you have hints |  Major | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-1410](https://issues.apache.org/jira/browse/CASSANDRA-1410) | allow index expressions on columns that are not part of the resultset |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1126](https://issues.apache.org/jira/browse/CASSANDRA-1126) | Allow loading of cassandra.yaml from a location given on the commandline |  Trivial | . | Erik Onnen | Jonathan Ellis |
| [CASSANDRA-1320](https://issues.apache.org/jira/browse/CASSANDRA-1320) | Convert AccessLevel to Set\<Permission\> |  Major | . | Stu Hood | Stu Hood |
| [CASSANDRA-1440](https://issues.apache.org/jira/browse/CASSANDRA-1440) | Improve auth story for Avro |  Major | . | Stu Hood | Stu Hood |
| [CASSANDRA-1390](https://issues.apache.org/jira/browse/CASSANDRA-1390) | Add back the missing options in cassandra.yaml |  Minor | . | Roger Schildmeijer | Brandon Williams |
| [CASSANDRA-1465](https://issues.apache.org/jira/browse/CASSANDRA-1465) | optimize OutboundTcpConnectionPool by removing synchronization, string comparisons |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1471](https://issues.apache.org/jira/browse/CASSANDRA-1471) | Improve SSTable discovery and allow for more components |  Major | . | Stu Hood | Stu Hood |
| [CASSANDRA-1372](https://issues.apache.org/jira/browse/CASSANDRA-1372) | StageManager.LOADBALANCE\_STAGE is unnecessary. |  Major | . | Gary Dusbabek | Jonathan Ellis |
| [CASSANDRA-1476](https://issues.apache.org/jira/browse/CASSANDRA-1476) | avro/CassandraServer has illegal @Override tags for methods implementing an interface |  Major | . | Matthew F. Dennis | Matthew F. Dennis |
| [CASSANDRA-1478](https://issues.apache.org/jira/browse/CASSANDRA-1478) | rename check\_schema\_agreement to describe\_schema\_versions |  Minor | CQL | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1500](https://issues.apache.org/jira/browse/CASSANDRA-1500) | Consolidated identical code in CassandraDaemon.setup() into super class |  Trivial | . | Amol Deshpande | Amol Deshpande |
| [CASSANDRA-1499](https://issues.apache.org/jira/browse/CASSANDRA-1499) | clean up ApplicationState |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1504](https://issues.apache.org/jira/browse/CASSANDRA-1504) | clean up streaming, part VII |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1371](https://issues.apache.org/jira/browse/CASSANDRA-1371) | replace createHardLink with JNA wrapper to link (posix) / CreateHardLink (windows) |  Minor | . | Jonathan Ellis | Brandon Williams |
| [CASSANDRA-1503](https://issues.apache.org/jira/browse/CASSANDRA-1503) | Modify bootstrap to use streaming callbacks |  Minor | . | Nick Bailey | Nick Bailey |
| [CASSANDRA-1506](https://issues.apache.org/jira/browse/CASSANDRA-1506) | combine "initiated" and requested streaming paths |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1424](https://issues.apache.org/jira/browse/CASSANDRA-1424) | Allow fine-tuning of Thrift TCP behavior for Thrift-managed sockets |  Minor | . | Erik Onnen | Erik Onnen |
| [CASSANDRA-1120](https://issues.apache.org/jira/browse/CASSANDRA-1120) | build.xml should document commonly used targets such as gen-thrift-java |  Trivial | . | Erik Onnen | Erik Onnen |
| [CASSANDRA-1074](https://issues.apache.org/jira/browse/CASSANDRA-1074) | check bloom filters to make minor compaction able to delete (some) tombstones |  Major | . | Robert Coli | Sylvain Lebresne |
| [CASSANDRA-1226](https://issues.apache.org/jira/browse/CASSANDRA-1226) | make DTPE handle exceptions the same way as CassandraDaemon |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1468](https://issues.apache.org/jira/browse/CASSANDRA-1468) | allow setting CompactionManager.min\|max compactionThreshold in configuration file |  Major | . | Jonathan Ellis | Jon Hermes |
| [CASSANDRA-1517](https://issues.apache.org/jira/browse/CASSANDRA-1517) | avoid allocating a mostly-unused HashMap in write path inner loop |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-786](https://issues.apache.org/jira/browse/CASSANDRA-786) | RPM Packages |  Minor | . | Daniel Lundin | Nick Bailey |
| [CASSANDRA-1342](https://issues.apache.org/jira/browse/CASSANDRA-1342) | Have the word\_count contrib example use the new baked in hadoop outputformat |  Minor | . | Jeremy Hanna | Jeremy Hanna |
| [CASSANDRA-1523](https://issues.apache.org/jira/browse/CASSANDRA-1523) | make cli comparator-aware |  Major | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1449](https://issues.apache.org/jira/browse/CASSANDRA-1449) | consistent nodetool blocking behavior |  Minor | Tools | Brandon Williams | Nirmal Ranganathan |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1350](https://issues.apache.org/jira/browse/CASSANDRA-1350) | DynamicEndpointSnitch is defeated by the caching done in Strategy |  Major | . | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-1377](https://issues.apache.org/jira/browse/CASSANDRA-1377) | NPE aborts streaming operations for keyspaces with hyphens ('-') in their names |  Major | . | Ben Hoyt | Gary Dusbabek |
| [CASSANDRA-1313](https://issues.apache.org/jira/browse/CASSANDRA-1313) | make cache sizes in CfDef Strings again so we can use %s of rows in the CF as in 0.6 |  Minor | . | Jonathan Ellis | Jon Hermes |
| [CASSANDRA-1241](https://issues.apache.org/jira/browse/CASSANDRA-1241) | config file option DiskAccessMode has no-op option "mmap\_index\_only" |  Minor | Documentation and Website | Robert Coli | Jonathan Ellis |
| [CASSANDRA-1378](https://issues.apache.org/jira/browse/CASSANDRA-1378) | add then drop Keyspace without putting anything in it causes exception |  Minor | . | Jeremiah Jordan | Gary Dusbabek |
| [CASSANDRA-1394](https://issues.apache.org/jira/browse/CASSANDRA-1394) | Blank listen\_address/rpc\_address no longer binds based on hostname |  Major | . | Brandon Williams |  |
| [CASSANDRA-1384](https://issues.apache.org/jira/browse/CASSANDRA-1384) | ERROR [MIGRATION-STAGE:1] Previous Version Mistmatch |  Major | . | Arya Goudarzi | Gary Dusbabek |
| [CASSANDRA-1399](https://issues.apache.org/jira/browse/CASSANDRA-1399) | CLI addColumnFamily - setting read\_repair\_chance modifies the keys\_cache\_size instead |  Trivial | Tools | Nirmal Ranganathan | Nirmal Ranganathan |
| [CASSANDRA-1373](https://issues.apache.org/jira/browse/CASSANDRA-1373) | OrderPreservingPartitioner with type validated indexed columns causes ClassCastException |  Major | . | Tyler Hobbs | Jonathan Ellis |
| [CASSANDRA-1382](https://issues.apache.org/jira/browse/CASSANDRA-1382) | Race condition leads to FileNotFoundException on startup |  Minor | . | Robert Zotter | Gary Dusbabek |
| [CASSANDRA-1403](https://issues.apache.org/jira/browse/CASSANDRA-1403) | CFMetaData id gets out of sync |  Major | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-1385](https://issues.apache.org/jira/browse/CASSANDRA-1385) | nodetool cfstats does not update after adding new cfs through API |  Minor | . | Arya Goudarzi | Gary Dusbabek |
| [CASSANDRA-1407](https://issues.apache.org/jira/browse/CASSANDRA-1407) | debian init script could be more consistent w/ bin/cassandra |  Major | Packaging | Eric Evans | Eric Evans |
| [CASSANDRA-1411](https://issues.apache.org/jira/browse/CASSANDRA-1411) | FBUtilities.hexToBytes() doesn't accommodate odd-length strings. |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-1416](https://issues.apache.org/jira/browse/CASSANDRA-1416) | SStableSliceIterator leaks FDs |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1238](https://issues.apache.org/jira/browse/CASSANDRA-1238) | Cassandra AVRO api is missing system\_add\_column\_family |  Major | . | Christian van der Leeden | Antoine Toulme |
| [CASSANDRA-1422](https://issues.apache.org/jira/browse/CASSANDRA-1422) | multiget\_count() should not take a keyspace arg |  Minor | . | Christopher Gist | Jon Hermes |
| [CASSANDRA-1429](https://issues.apache.org/jira/browse/CASSANDRA-1429) | The dynamic snitch can't be used with network topology strategies |  Major | . | Sylvain Lebresne | Jonathan Ellis |
| [CASSANDRA-1413](https://issues.apache.org/jira/browse/CASSANDRA-1413) | EstimatedHistogram.max is buggy |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1237](https://issues.apache.org/jira/browse/CASSANDRA-1237) | Store AccessLevels externally to IAuthenticator |  Major | . | Stu Hood | Stu Hood |
| [CASSANDRA-1315](https://issues.apache.org/jira/browse/CASSANDRA-1315) | ColumnFamilyOutputFormat should use client API objects |  Major | . | Stu Hood | Stu Hood |
| [CASSANDRA-1432](https://issues.apache.org/jira/browse/CASSANDRA-1432) | java.util.NoSuchElementException when returning a node to the cluster |  Minor | . | amorton | Jonathan Ellis |
| [CASSANDRA-1439](https://issues.apache.org/jira/browse/CASSANDRA-1439) | OPP makes HH unhappy |  Major | . | Jonathan Ellis | Brandon Williams |
| [CASSANDRA-1406](https://issues.apache.org/jira/browse/CASSANDRA-1406) | Dropping column families doesn't clean up secondary indexes |  Major | Secondary Indexes | Gary Dusbabek | Jonathan Ellis |
| [CASSANDRA-1446](https://issues.apache.org/jira/browse/CASSANDRA-1446) | cassandra-cli still relies on cassandra.in.sh instead of cassandra-env.sh |  Trivial | . | Brandon Williams | Eric Evans |
| [CASSANDRA-1435](https://issues.apache.org/jira/browse/CASSANDRA-1435) | CommitLogHeader raises an AssertionError during  startup |  Major | . | amorton | Jonathan Ellis |
| [CASSANDRA-1454](https://issues.apache.org/jira/browse/CASSANDRA-1454) | avronateSubcolumns was assuming an avro array. |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-1433](https://issues.apache.org/jira/browse/CASSANDRA-1433) | nodetool cfstats broken on TRUNK |  Minor | Tools | Jeremiah Jordan | Jonathan Ellis |
| [CASSANDRA-1463](https://issues.apache.org/jira/browse/CASSANDRA-1463) | Failed bootstrap can cause NPE in batch\_mutate on every node, taking down the entire cluster |  Major | . | David King | Jonathan Ellis |
| [CASSANDRA-1450](https://issues.apache.org/jira/browse/CASSANDRA-1450) | Memtable flush causes bad "reversed" get\_slice |  Major | . | Tyler Hobbs | Jonathan Ellis |
| [CASSANDRA-1190](https://issues.apache.org/jira/browse/CASSANDRA-1190) | Remove automatic repair sessions |  Major | . | Stu Hood | Stu Hood |
| [CASSANDRA-1430](https://issues.apache.org/jira/browse/CASSANDRA-1430) | SSTable statistics causing intermittent CL test failures in trunk. |  Major | . | Gary Dusbabek | Brandon Williams |
| [CASSANDRA-1348](https://issues.apache.org/jira/browse/CASSANDRA-1348) | Failed to delete commitlog when restarting service |  Minor | . | Viktor Jevdokimov | Jonathan Ellis |
| [CASSANDRA-1458](https://issues.apache.org/jira/browse/CASSANDRA-1458) | SSTable cleanup killed by IllegalStateException |  Minor | . | Christopher Gist | Jonathan Ellis |
| [CASSANDRA-1484](https://issues.apache.org/jira/browse/CASSANDRA-1484) | API Version Mismatch |  Minor | CQL | Arya Goudarzi | Jonathan Ellis |
| [CASSANDRA-1487](https://issues.apache.org/jira/browse/CASSANDRA-1487) | Bug in calculating QUORUM |  Major | . | Jignesh Dhruv | Jignesh Dhruv |
| [CASSANDRA-1493](https://issues.apache.org/jira/browse/CASSANDRA-1493) | AssertionError in MessagingService.receive for READ\_REPAIR verb |  Major | . | Johan Oskarsson | Johan Oskarsson |
| [CASSANDRA-1402](https://issues.apache.org/jira/browse/CASSANDRA-1402) | indexed writes fail with exception |  Major | . | Eric Evans | Jonathan Ellis |
| [CASSANDRA-1467](https://issues.apache.org/jira/browse/CASSANDRA-1467) | replication factor exceeds number of endpoints, when attempting to join a new node (but cluster has enough running nodes to fulfill RF) |  Major | . | Josep M. Blanquer | Gary Dusbabek |
| [CASSANDRA-1480](https://issues.apache.org/jira/browse/CASSANDRA-1480) | CFMetaData.convertToThrift makes subcomparator\_type empty string instead of null |  Major | CQL | Jeremy Hanna | Jon Hermes |
| [CASSANDRA-1507](https://issues.apache.org/jira/browse/CASSANDRA-1507) | make adaptive heap size calculation portable |  Minor | . | Peter Schuller | Peter Schuller |
| [CASSANDRA-1477](https://issues.apache.org/jira/browse/CASSANDRA-1477) | drop/recreate column family race condition |  Blocker | . | B. Todd Burruss | Gary Dusbabek |
| [CASSANDRA-1508](https://issues.apache.org/jira/browse/CASSANDRA-1508) | only attempt to set size on Linux (for portability) |  Minor | Packaging | Peter Schuller | Peter Schuller |
| [CASSANDRA-1509](https://issues.apache.org/jira/browse/CASSANDRA-1509) | CassandraServiceDataCleaner doesn't remove subdirectories properly |  Trivial | . | B. Todd Burruss | B. Todd Burruss |
| [CASSANDRA-1520](https://issues.apache.org/jira/browse/CASSANDRA-1520) | stress.py's multiget option sends increasingly inefficient queries as more test data is inserted |  Minor | . | Nate McCall | Brandon Williams |
| [CASSANDRA-1522](https://issues.apache.org/jira/browse/CASSANDRA-1522) | Methods removed from FileUtils break CassandraServiceDataCleaner in contrib/javautils |  Major | . | Nate McCall | Nate McCall |
| [CASSANDRA-1512](https://issues.apache.org/jira/browse/CASSANDRA-1512) | cassandra will replay the last mutation in a commitlog when it shouldn't |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1528](https://issues.apache.org/jira/browse/CASSANDRA-1528) | Cassandra holds a socket in CLOSE\_WAIT on the storage port |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-1529](https://issues.apache.org/jira/browse/CASSANDRA-1529) | word count contrib module broken as a result of removing Clock interface |  Trivial | . | Jeremy Hanna | Jeremy Hanna |
| [CASSANDRA-1535](https://issues.apache.org/jira/browse/CASSANDRA-1535) | Repair doesn't do anything when a CF isn't specified |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-1534](https://issues.apache.org/jira/browse/CASSANDRA-1534) | errors reading while bootstrapping |  Major | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-1540](https://issues.apache.org/jira/browse/CASSANDRA-1540) | cfstats is broken |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-1539](https://issues.apache.org/jira/browse/CASSANDRA-1539) | ExpiringColumn wrongly inherits Column.getMarkedForDeleteAt |  Critical | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-1544](https://issues.apache.org/jira/browse/CASSANDRA-1544) | Compacted SSTables not properly removed |  Major | . | Stu Hood | Stu Hood |
| [CASSANDRA-1527](https://issues.apache.org/jira/browse/CASSANDRA-1527) | ensure compaction thresholds are sane |  Minor | CQL | Jonathan Ellis | Jon Hermes |
| [CASSANDRA-1434](https://issues.apache.org/jira/browse/CASSANDRA-1434) | ColumnFamilyOutputFormat performs blocking writes for large batches |  Major | . | Stu Hood | Jonathan Ellis |
| [CASSANDRA-1545](https://issues.apache.org/jira/browse/CASSANDRA-1545) | NullPointerException on startup after upgrade |  Critical | . | JCF | Jon Hermes |
| [CASSANDRA-1549](https://issues.apache.org/jira/browse/CASSANDRA-1549) | cassandra-cli craps its pants and dies when 'gc\_grace\_seconds' is used in cf creation |  Minor | Tools | paul cannon | paul cannon |
| [CASSANDRA-1536](https://issues.apache.org/jira/browse/CASSANDRA-1536) | Detect use of secondary indexes with TTL'd columns |  Major | Secondary Indexes | Stu Hood | Stu Hood |
| [CASSANDRA-1542](https://issues.apache.org/jira/browse/CASSANDRA-1542) | Data directories not being properly scrubbed |  Minor | . | Stu Hood | Gary Dusbabek |
| [CASSANDRA-1216](https://issues.apache.org/jira/browse/CASSANDRA-1216) | removetoken drops node from ring before re-replicating its data is finished |  Major | . | Jonathan Ellis | Nick Bailey |
| [CASSANDRA-1548](https://issues.apache.org/jira/browse/CASSANDRA-1548) | renaming a keyspace, then trying to use original name again makes errorations |  Minor | . | paul cannon | Gary Dusbabek |
| [CASSANDRA-1447](https://issues.apache.org/jira/browse/CASSANDRA-1447) | SimpleAuthenticator MD5 support |  Minor | . | Stu Hood | Nirmal Ranganathan |
| [CASSANDRA-1408](https://issues.apache.org/jira/browse/CASSANDRA-1408) | nodetool drain attempts to delete a deleted file |  Minor | . | Jon Hermes | Brandon Williams |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1420](https://issues.apache.org/jira/browse/CASSANDRA-1420) | Using config-converter, some fields are not converted |  Minor | . | Ron Ratovsky | Jon Hermes |
| [CASSANDRA-1419](https://issues.apache.org/jira/browse/CASSANDRA-1419) | Improper file extension on output of config-converter |  Trivial | . | Ron Ratovsky | Jonathan Ellis |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1491](https://issues.apache.org/jira/browse/CASSANDRA-1491) | Allow queries to the snitch via JMX |  Minor | Tools | paul cannon | Nirmal Ranganathan |
| [CASSANDRA-1269](https://issues.apache.org/jira/browse/CASSANDRA-1269) | improve test coverage of BufferedRandomAccessFile |  Minor | . | Jonathan Ellis | Jon Hermes |
| [CASSANDRA-1392](https://issues.apache.org/jira/browse/CASSANDRA-1392) | rename RackAwareStrategy to OldNetworkTopologyStrategy, RackUnawareStrategy to SimpleStrategy |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1425](https://issues.apache.org/jira/browse/CASSANDRA-1425) | get input splits thrift method still takes a KS as an arg |  Minor | . | Jeremy Hanna | Jeremy Hanna |
| [CASSANDRA-1501](https://issues.apache.org/jira/browse/CASSANDRA-1501) | remove Clock from Thrift API |  Major | CQL | Jonathan Ellis | Jonathan Ellis |


