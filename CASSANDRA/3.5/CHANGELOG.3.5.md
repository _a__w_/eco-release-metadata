
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.5 - 2016-04-13



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6377](https://issues.apache.org/jira/browse/CASSANDRA-6377) | ALLOW FILTERING should allow seq scan filtering |  Major | CQL | Jonathan Ellis | Benjamin Lerer |
| [CASSANDRA-11330](https://issues.apache.org/jira/browse/CASSANDRA-11330) | Enable sstabledump to be used on 2i tables |  Minor | Tools | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-10900](https://issues.apache.org/jira/browse/CASSANDRA-10900) | CQL3 spec should clarify that now() function is calculated on the coordinator node |  Trivial | Documentation and Website | Wei Deng | Benjamin Lerer |
| [CASSANDRA-10809](https://issues.apache.org/jira/browse/CASSANDRA-10809) | Create a -D option to prevent gossip startup |  Major | Distributed Metadata | Brandon Williams | Sylvain Lebresne |
| [CASSANDRA-11411](https://issues.apache.org/jira/browse/CASSANDRA-11411) | Reduce the amount of logging during repair |  Minor | Streaming and Messaging | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-11179](https://issues.apache.org/jira/browse/CASSANDRA-11179) | Parallel cleanup can lead to disk space exhaustion |  Major | Compaction, Tools | Tyler Hobbs | Marcus Eriksson |
| [CASSANDRA-11444](https://issues.apache.org/jira/browse/CASSANDRA-11444) | Upgrade ohc to 0.4.3 |  Trivial | Core | Robert Stupp | Robert Stupp |
| [CASSANDRA-9325](https://issues.apache.org/jira/browse/CASSANDRA-9325) | cassandra-stress requires keystore for SSL but provides no way to configure it |  Major | Tools | J.B. Langston | Stefan Podkowinski |
| [CASSANDRA-11312](https://issues.apache.org/jira/browse/CASSANDRA-11312) | DatabaseDescriptor#applyConfig should log stacktrace in case of Eception during seed provider creation |  Minor | Configuration | Andrzej Ludwikowski | Andrzej Ludwikowski |
| [CASSANDRA-11320](https://issues.apache.org/jira/browse/CASSANDRA-11320) | Improve backoff policy for cqlsh COPY FROM |  Major | Tools | Stefania | Stefania |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11215](https://issues.apache.org/jira/browse/CASSANDRA-11215) | Reference leak with parallel repairs on the same table |  Major | . | Marcus Olsson | Marcus Olsson |
| [CASSANDRA-11176](https://issues.apache.org/jira/browse/CASSANDRA-11176) | SSTableRewriter.InvalidateKeys should have a weak reference to cache |  Major | Core | Jeremiah Jordan | Marcus Eriksson |
| [CASSANDRA-11217](https://issues.apache.org/jira/browse/CASSANDRA-11217) | Only log yaml config once, at startup |  Minor | Configuration, Core | Jason Brown | Jason Brown |
| [CASSANDRA-11275](https://issues.apache.org/jira/browse/CASSANDRA-11275) | sstableloader fails with java.lang.IllegalArgumentException: flags is not a column defined in this metadata |  Major | Materialized Views, Tools | Sergey Kirillov | Sergey Kirillov |
| [CASSANDRA-11210](https://issues.apache.org/jira/browse/CASSANDRA-11210) | Unresolved hostname in replace address |  Minor | . | sankalp kohli | Jan Karlsson |
| [CASSANDRA-11302](https://issues.apache.org/jira/browse/CASSANDRA-11302) | Invalid time unit conversion causing write timeouts |  Major | Core | Mike Heffner | Sylvain Lebresne |
| [CASSANDRA-11316](https://issues.apache.org/jira/browse/CASSANDRA-11316) | BTreeRow.hashCode() is broken |  Minor | . | Branimir Lambov | Branimir Lambov |
| [CASSANDRA-11325](https://issues.apache.org/jira/browse/CASSANDRA-11325) | Missing newline at end of bin/cqlsh causes it to be changed by the fixcrlf Ant plugin |  Trivial | Tools | Joel Knighton | Joel Knighton |
| [CASSANDRA-11286](https://issues.apache.org/jira/browse/CASSANDRA-11286) | streaming socket never times out |  Major | Streaming and Messaging | Paulo Motta | Paulo Motta |
| [CASSANDRA-11321](https://issues.apache.org/jira/browse/CASSANDRA-11321) | 'sstabledump -d' omits static rows and partition level deletions |  Minor | . | Andy Tolbert | Andy Tolbert |
| [CASSANDRA-11092](https://issues.apache.org/jira/browse/CASSANDRA-11092) | EXPAND breaks when the result has 0 rows in cqlsh |  Trivial | Tools | Kishan Karunaratne | Yuki Morishita |
| [CASSANDRA-11168](https://issues.apache.org/jira/browse/CASSANDRA-11168) | Hint Metrics are updated even if hinted\_hand-offs=false |  Minor | Coordination, Observability | Anubhav Kale | Anubhav Kale |
| [CASSANDRA-11344](https://issues.apache.org/jira/browse/CASSANDRA-11344) | Fix bloom filter sizing with LCS |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-10342](https://issues.apache.org/jira/browse/CASSANDRA-10342) | Read defragmentation can cause unnecessary repairs |  Minor | . | Marcus Olsson | Marcus Eriksson |
| [CASSANDRA-10971](https://issues.apache.org/jira/browse/CASSANDRA-10971) | Compressed commit log has no backpressure and can OOM |  Major | Local Write-Read Paths | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-10752](https://issues.apache.org/jira/browse/CASSANDRA-10752) | CQL.textile wasn't updated for CASSANDRA-6839 |  Major | Documentation and Website | Jeremiah Jordan | Sylvain Lebresne |
| [CASSANDRA-10748](https://issues.apache.org/jira/browse/CASSANDRA-10748) | UTF8Validator.validate() wrong ?? |  Minor | Core | Robert Stupp | Benjamin Lerer |
| [CASSANDRA-10990](https://issues.apache.org/jira/browse/CASSANDRA-10990) | Support streaming of older version sstables in 3.0 |  Major | Streaming and Messaging | Jeremy Hanna | Paulo Motta |
| [CASSANDRA-11333](https://issues.apache.org/jira/browse/CASSANDRA-11333) | cqlsh: COPY FROM should check that explicit column names are valid |  Minor | Tools | Stefania | Stefania |
| [CASSANDRA-11390](https://issues.apache.org/jira/browse/CASSANDRA-11390) | Too big MerkleTrees allocated during repair |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-11397](https://issues.apache.org/jira/browse/CASSANDRA-11397) | LIKE query on clustering column index returns incorrect results |  Major | . | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-11383](https://issues.apache.org/jira/browse/CASSANDRA-11383) | Avoid index segment stitching in RAM which lead to OOM on big SSTable files |  Major | CQL | DOAN DuyHai | Jordan West |
| [CASSANDRA-11435](https://issues.apache.org/jira/browse/CASSANDRA-11435) | PrepareCallback#response should not have DEBUG output |  Major | Core | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-11053](https://issues.apache.org/jira/browse/CASSANDRA-11053) | COPY FROM on large datasets: fix progress report and optimize performance part 4 |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-11373](https://issues.apache.org/jira/browse/CASSANDRA-11373) | Cancelled compaction leading to infinite loop in compaction strategy getNextBackgroundTask |  Major | Compaction | Eduard Tudenhoefner | Marcus Eriksson |
| [CASSANDRA-11451](https://issues.apache.org/jira/browse/CASSANDRA-11451) | Don't mark sstables as repairing when doing sub range repair |  Major | Streaming and Messaging | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-11453](https://issues.apache.org/jira/browse/CASSANDRA-11453) | CustomIndexTest fails intermittently |  Major | Testing | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-11331](https://issues.apache.org/jira/browse/CASSANDRA-11331) | Create Index IF NOT EXISTS throws error when index already exists |  Major | CQL | Philip Thompson | Sam Tunnicliffe |
| [CASSANDRA-11448](https://issues.apache.org/jira/browse/CASSANDRA-11448) | Running OOS should trigger the disk failure policy |  Major | . | Brandon Williams | Branimir Lambov |
| [CASSANDRA-10587](https://issues.apache.org/jira/browse/CASSANDRA-10587) | sstablemetadata NPE on cassandra 2.2 |  Minor | Tools | Tiago Batista | Yuki Morishita |
| [CASSANDRA-10943](https://issues.apache.org/jira/browse/CASSANDRA-10943) | NullPointer during LegacySchemaMigrator |  Minor | . | Stephan Wienczny | Aleksey Yeschenko |
| [CASSANDRA-11353](https://issues.apache.org/jira/browse/CASSANDRA-11353) | ERROR [CompactionExecutor] CassandraDaemon.java Exception in thread |  Major | Compaction, Local Write-Read Paths | Alexey Ivanchin | Sylvain Lebresne |
| [CASSANDRA-11525](https://issues.apache.org/jira/browse/CASSANDRA-11525) | StaticTokenTreeBuilder should respect posibility of duplicate tokens |  Major | sasi | DOAN DuyHai | Jordan West |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10331](https://issues.apache.org/jira/browse/CASSANDRA-10331) | Establish and implement canonical bulk reading workload(s) |  Major | . | Ariel Weisberg | Stefania |


