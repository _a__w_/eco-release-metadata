
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra  3.0.9 Release Notes

These release notes cover new developer and user-facing incompatibilities, important issues, features, and major improvements.


---

* [CASSANDRA-12107](https://issues.apache.org/jira/browse/CASSANDRA-12107) | *Major* | **Fix range scans for table with live static rows**

We were seeing some weird behaviour with limit based scan queries. In particular, we see the following:
{noformat}
$ cqlsh -k sd -e "consistency local\_quorum; SELECT uuid, token(uuid) FROM files WHERE token(uuid) \>= token('6b470c3e43ee06d1') limit 2"
Consistency level set to LOCAL\_QUORUM.

 uuid             \| system.token(uuid)
------------------+----------------------
 6b470c3e43ee06d1 \| -9218823070349964862
 484b091ca97803cd \| -8954822859271125729

(2 rows)
$ cqlsh -k sd -e "consistency local\_quorum; SELECT uuid, token(uuid) FROM files WHERE token(uuid) \> token('6b470c3e43ee06d1') limit 1"
Consistency level set to LOCAL\_QUORUM.

 uuid             \| system.token(uuid)
------------------+----------------------
 c348aaec2f1e4b85 \| -9218781105444826588
{noformat}
In the table uuid is partition key, and it has a clustering key as well.
So the uuid "c348aaec2f1e4b85" should be the second one in the limit query. After some investigation, it seems to me like the issue is in the way DataLimits handles static rows. Here is a patch for trunk (https://github.com/sharvanath/cassandra/commit/9a460d40e55bd7e3604d987ed4df5c8c2e03ffdc) which seems to fix it for me. Please take a look, seems like a pretty critical issue to me.

I have forked the dtests for it as well. However, since trunk has some failures already, I'm not fully sure how to infer the results.
http://cassci.datastax.com/view/Dev/view/sharvanath/job/sharvanath-fixScan-dtest/
http://cassci.datastax.com/view/Dev/view/sharvanath/job/sharvanath-fixScan-testall/



