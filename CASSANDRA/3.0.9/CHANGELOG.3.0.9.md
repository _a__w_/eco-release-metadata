
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.0.9 - 2016-09-20



### IMPORTANT ISSUES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12107](https://issues.apache.org/jira/browse/CASSANDRA-12107) | Fix range scans for table with live static rows |  Major | CQL | Sharvanath Pathak | Sharvanath Pathak |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11715](https://issues.apache.org/jira/browse/CASSANDRA-11715) | Make GCInspector's MIN\_LOG\_DURATION configurable |  Minor | . | Brandon Williams | Jeff Jirsa |
| [CASSANDRA-12114](https://issues.apache.org/jira/browse/CASSANDRA-12114) | Cassandra startup takes an hour because of N\*N operation |  Major | Core | Tom van der Woerdt | Jeff Jirsa |
| [CASSANDRA-12180](https://issues.apache.org/jira/browse/CASSANDRA-12180) | Should be able to override compaction space check |  Trivial | Compaction | sankalp kohli | sankalp kohli |
| [CASSANDRA-12181](https://issues.apache.org/jira/browse/CASSANDRA-12181) | Include table name in "Cannot get comparator" exception |  Trivial | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-12040](https://issues.apache.org/jira/browse/CASSANDRA-12040) |   If a level compaction fails due to no space it should schedule the next one |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-7190](https://issues.apache.org/jira/browse/CASSANDRA-7190) | Add schema to snapshot manifest |  Minor | Materialized Views, Tools | Jonathan Ellis | Alex Petrov |
| [CASSANDRA-12425](https://issues.apache.org/jira/browse/CASSANDRA-12425) | Log at DEBUG when running unit tests |  Minor | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-12385](https://issues.apache.org/jira/browse/CASSANDRA-12385) | Disk failure policy should not be invoked on out of space |  Minor | . | sankalp kohli | sankalp kohli |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12043](https://issues.apache.org/jira/browse/CASSANDRA-12043) | Syncing most recent commit in CAS across replicas can cause all CAS queries in the CQL partition to fail |  Major | . | sankalp kohli | Sylvain Lebresne |
| [CASSANDRA-11973](https://issues.apache.org/jira/browse/CASSANDRA-11973) | Is MemoryUtil.getShort() supposed to return a sign-extended or non-sign-extended value? |  Minor | Core | Rei Odaira | Rei Odaira |
| [CASSANDRA-11820](https://issues.apache.org/jira/browse/CASSANDRA-11820) | Altering a column's type causes EOF |  Major | Local Write-Read Paths | Carl Yeksigian | Sylvain Lebresne |
| [CASSANDRA-12090](https://issues.apache.org/jira/browse/CASSANDRA-12090) | Digest mismatch if static column is NULL |  Major | Streaming and Messaging | Tommy Stendahl | Tommy Stendahl |
| [CASSANDRA-12044](https://issues.apache.org/jira/browse/CASSANDRA-12044) | Materialized view definition regression in clustering key |  Major | CQL, Materialized Views | Michael Mior | Carl Yeksigian |
| [CASSANDRA-11944](https://issues.apache.org/jira/browse/CASSANDRA-11944) | sstablesInBounds might not actually give all sstables within the bounds due to having start positions moved in sstables |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-11996](https://issues.apache.org/jira/browse/CASSANDRA-11996) | SSTableSet.CANONICAL can miss sstables |  Critical | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-12098](https://issues.apache.org/jira/browse/CASSANDRA-12098) | dtest failure in secondary\_indexes\_test.TestSecondaryIndexes.test\_only\_coordinator\_chooses\_index\_for\_query |  Major | Secondary Indexes | Sean McCarthy | Sam Tunnicliffe |
| [CASSANDRA-11414](https://issues.apache.org/jira/browse/CASSANDRA-11414) | dtest failure in bootstrap\_test.TestBootstrap.resumable\_bootstrap\_test |  Major | Testing | Philip Thompson | Paulo Motta |
| [CASSANDRA-11733](https://issues.apache.org/jira/browse/CASSANDRA-11733) | SSTableReversedIterator ignores range tombstones |  Major | Local Write-Read Paths | Dave Brosius | Sylvain Lebresne |
| [CASSANDRA-12146](https://issues.apache.org/jira/browse/CASSANDRA-12146) | Use dedicated executor for sending JMX notifications |  Major | Observability | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-11315](https://issues.apache.org/jira/browse/CASSANDRA-11315) | Fix upgrading sparse tables that are incorrectly marked as dense |  Major | Distributed Metadata | Dominik Keil | Aleksey Yeschenko |
| [CASSANDRA-12147](https://issues.apache.org/jira/browse/CASSANDRA-12147) | Static thrift tables with non UTF8Type comparators can have column names converted incorrectly |  Major | Distributed Metadata | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-12105](https://issues.apache.org/jira/browse/CASSANDRA-12105) | ThriftServer.stop is not thread safe |  Minor | . | Brian Wawok | Brian Wawok |
| [CASSANDRA-11393](https://issues.apache.org/jira/browse/CASSANDRA-11393) | dtest failure in upgrade\_tests.upgrade\_through\_versions\_test.ProtoV3Upgrade\_2\_1\_UpTo\_3\_0\_HEAD.rolling\_upgrade\_test |  Major | Coordination, Streaming and Messaging | Philip Thompson | Benjamin Lerer |
| [CASSANDRA-12193](https://issues.apache.org/jira/browse/CASSANDRA-12193) | dtest failure in upgrade\_tests.cql\_tests.TestCQLNodes3RF3\_Upgrade\_current\_2\_1\_x\_To\_indev\_3\_0\_x.noncomposite\_static\_cf\_test |  Major | Coordination | Sean McCarthy | Alex Petrov |
| [CASSANDRA-12144](https://issues.apache.org/jira/browse/CASSANDRA-12144) | Undeletable / duplicate rows after upgrading from 2.2.4 to 3.0.7 |  Major | . | Stanislav Vishnevskiy | Alex Petrov |
| [CASSANDRA-12214](https://issues.apache.org/jira/browse/CASSANDRA-12214) |  cqlshlib test failure: cqlshlib.test.remove\_test\_db |  Major | Testing | Craig Kodman | Stefania |
| [CASSANDRA-11988](https://issues.apache.org/jira/browse/CASSANDRA-11988) | NullPointerException when reading/compacting table |  Major | Compaction | Nimi Wariboko Jr. | Sylvain Lebresne |
| [CASSANDRA-11464](https://issues.apache.org/jira/browse/CASSANDRA-11464) | C\* doesn't respond to OPTIONS request containing low protocol number |  Major | Core | Sandeep Tamhankar | Tyler Hobbs |
| [CASSANDRA-11850](https://issues.apache.org/jira/browse/CASSANDRA-11850) | cannot use cql since upgrading python to 2.7.11+ |  Major | Tools | Andrew Madison | Stefania |
| [CASSANDRA-11979](https://issues.apache.org/jira/browse/CASSANDRA-11979) | cqlsh copyutil should get host metadata by connected address |  Minor | Tools | Adam Holmberg | Stefania |
| [CASSANDRA-11980](https://issues.apache.org/jira/browse/CASSANDRA-11980) | Reads at EACH\_QUORUM not respecting the level with read repair or speculative retry active |  Major | Coordination | Aleksey Yeschenko | Carl Yeksigian |
| [CASSANDRA-12247](https://issues.apache.org/jira/browse/CASSANDRA-12247) | AssertionError with MVs on updating a row that isn't indexed due to a null value |  Critical | Local Write-Read Paths | Benjamin Roth | Sylvain Lebresne |
| [CASSANDRA-12278](https://issues.apache.org/jira/browse/CASSANDRA-12278) | Cassandra not working with Java 8u102 on Windows |  Major | Core | Thomas Atwood | Paulo Motta |
| [CASSANDRA-10939](https://issues.apache.org/jira/browse/CASSANDRA-10939) | Add missing jvm options to cassandra-env.ps1 |  Minor | Configuration | Paulo Motta | Paulo Motta |
| [CASSANDRA-12219](https://issues.apache.org/jira/browse/CASSANDRA-12219) | Lost counter writes in compact table and static columns |  Major | . | Brian Wawok | Sylvain Lebresne |
| [CASSANDRA-12263](https://issues.apache.org/jira/browse/CASSANDRA-12263) | Exception when computing read-repair for range tombstones |  Major | Coordination | Artem Rokhin | Sylvain Lebresne |
| [CASSANDRA-12277](https://issues.apache.org/jira/browse/CASSANDRA-12277) | Extend testing infrastructure to handle expected intermittent flaky tests - see ReplicationAwareTokenAllocatorTest.testNewCluster |  Minor | . | Joshua McKenzie | Branimir Lambov |
| [CASSANDRA-11465](https://issues.apache.org/jira/browse/CASSANDRA-11465) | dtest failure in cql\_tracing\_test.TestCqlTracing.tracing\_unknown\_impl\_test |  Major | Observability | Philip Thompson | Stefania |
| [CASSANDRA-12359](https://issues.apache.org/jira/browse/CASSANDRA-12359) | BlacklistingCompactionsTest.testBlacklistingWithSizeTieredCompactionStrategy is flaky |  Major | Testing | Joel Knighton | Stefania |
| [CASSANDRA-11823](https://issues.apache.org/jira/browse/CASSANDRA-11823) | Creating a table leads to a race with GraphiteReporter |  Minor | Observability | Stefano Ortolani | Edward Ribeiro |
| [CASSANDRA-12336](https://issues.apache.org/jira/browse/CASSANDRA-12336) | NullPointerException during compaction on table with static columns |  Major | Compaction | Evan Prothro | Sylvain Lebresne |
| [CASSANDRA-11828](https://issues.apache.org/jira/browse/CASSANDRA-11828) | Commit log needs to track unflushed intervals rather than positions |  Major | Local Write-Read Paths | Branimir Lambov | Branimir Lambov |
| [CASSANDRA-12312](https://issues.apache.org/jira/browse/CASSANDRA-12312) | Restore JVM metric export for metric reporters |  Major | . | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-12371](https://issues.apache.org/jira/browse/CASSANDRA-12371) | INSERT JSON - numbers not accepted for smallint and tinyint |  Minor | CQL | Paweł Rychlik | Paweł Rychlik |
| [CASSANDRA-12335](https://issues.apache.org/jira/browse/CASSANDRA-12335) | Super columns are broken after upgrading to 3.0 on thrift |  Major | Core | Jeremiah Jordan | Sylvain Lebresne |
| [CASSANDRA-10992](https://issues.apache.org/jira/browse/CASSANDRA-10992) | Hanging streaming sessions |  Major | Streaming and Messaging | mlowicki | Paulo Motta |
| [CASSANDRA-12249](https://issues.apache.org/jira/browse/CASSANDRA-12249) | dtest failure in upgrade\_tests.paging\_test.TestPagingDataNodes3RF3\_Upgrade\_current\_3\_0\_x\_To\_indev\_3\_x.basic\_paging\_test |  Major | Coordination | Philip Thompson | Tyler Hobbs |
| [CASSANDRA-11752](https://issues.apache.org/jira/browse/CASSANDRA-11752) | histograms/metrics in 2.2 do not appear recency biased |  Major | Core | Chris Burroughs | Per Otterström |
| [CASSANDRA-12100](https://issues.apache.org/jira/browse/CASSANDRA-12100) | Compactions are stuck after TRUNCATE |  Major | Compaction | Stefano Ortolani | Stefania |
| [CASSANDRA-12177](https://issues.apache.org/jira/browse/CASSANDRA-12177) | sstabledump fails if sstable path includes dot |  Major | Tools | Keith Wansbrough | Yuki Morishita |
| [CASSANDRA-9507](https://issues.apache.org/jira/browse/CASSANDRA-9507) | range metrics are not updated for timeout and unavailable in StorageProxy |  Minor | Observability | sankalp kohli | Nachiket Patil |
| [CASSANDRA-11356](https://issues.apache.org/jira/browse/CASSANDRA-11356) | EC2MRS ignores broadcast\_rpc\_address setting in cassandra.yaml |  Major | Core | Thanh | Paulo Motta |
| [CASSANDRA-12445](https://issues.apache.org/jira/browse/CASSANDRA-12445) | StreamingTransferTest.testTransferRangeTombstones failure |  Major | Streaming and Messaging, Testing | Joel Knighton | Paulo Motta |
| [CASSANDRA-11345](https://issues.apache.org/jira/browse/CASSANDRA-11345) | Assertion Errors "Memory was freed" during streaming |  Major | Streaming and Messaging | Jean-Francois Gosselin | Paulo Motta |
| [CASSANDRA-12127](https://issues.apache.org/jira/browse/CASSANDRA-12127) | Queries with empty ByteBuffer values in clustering column restrictions fail for non-composite compact tables |  Major | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-12436](https://issues.apache.org/jira/browse/CASSANDRA-12436) | Under some races commit log may incorrectly think it has unflushed data |  Major | . | Branimir Lambov | Branimir Lambov |
| [CASSANDRA-12251](https://issues.apache.org/jira/browse/CASSANDRA-12251) | Move migration tasks to non-periodic queue, assure flush executor shutdown after non-periodic executor |  Major | Lifecycle | Philip Thompson | Alex Petrov |
| [CASSANDRA-12476](https://issues.apache.org/jira/browse/CASSANDRA-12476) | SyntaxException when COPY FROM Counter Table with Null value |  Minor | Tools | Ashraful Islam | Stefania |
| [CASSANDRA-12504](https://issues.apache.org/jira/browse/CASSANDRA-12504) | BatchlogManager is shut down twice during drain |  Minor | Lifecycle | Alex Petrov | Stefania |
| [CASSANDRA-12528](https://issues.apache.org/jira/browse/CASSANDRA-12528) | Fix eclipse-warning problems |  Major | Core | Joel Knighton | Sam Tunnicliffe |
| [CASSANDRA-12508](https://issues.apache.org/jira/browse/CASSANDRA-12508) | nodetool repair returns status code 0 for some errors |  Major | Tools | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-11594](https://issues.apache.org/jira/browse/CASSANDRA-11594) | Too many open files on directories |  Critical | Core | n0rad | Stefania |
| [CASSANDRA-11889](https://issues.apache.org/jira/browse/CASSANDRA-11889) | LogRecord: file system race condition may cause verify() to fail |  Major | Local Write-Read Paths | Stefania | Stefania |
| [CASSANDRA-12279](https://issues.apache.org/jira/browse/CASSANDRA-12279) | nodetool repair hangs on non-existant table |  Minor | . | Benjamin Roth | Masataka Yamaguchi |
| [CASSANDRA-12522](https://issues.apache.org/jira/browse/CASSANDRA-12522) | nodetool repair -pr and -local option rejected |  Major | Tools | Jérôme Mainaud | Jérôme Mainaud |
| [CASSANDRA-12208](https://issues.apache.org/jira/browse/CASSANDRA-12208) | Estimated droppable tombstones given by sstablemetadata counts tombstones that aren't actually "droppable" |  Minor | Tools | Thanh | Marcus Eriksson |
| [CASSANDRA-8523](https://issues.apache.org/jira/browse/CASSANDRA-8523) | Writes should be sent to a replacement node which has a new IP while it is streaming in data |  Major | . | Richard Wagner | Paulo Motta |
| [CASSANDRA-12565](https://issues.apache.org/jira/browse/CASSANDRA-12565) | Node may not start when upgrading from 2.2 to 3.0 if drain is not run |  Major | Local Write-Read Paths | Stefania | Stefania |
| [CASSANDRA-12481](https://issues.apache.org/jira/browse/CASSANDRA-12481) | dtest failure in cqlshlib.test.test\_cqlsh\_output.TestCqlshOutput.test\_describe\_keyspace\_output |  Major | Testing | Craig Kodman | Stefania |
| [CASSANDRA-12527](https://issues.apache.org/jira/browse/CASSANDRA-12527) | Stack Overflow returned to queries while upgrading |  Major | Local Write-Read Paths | Steve Severance | Sylvain Lebresne |
| [CASSANDRA-11126](https://issues.apache.org/jira/browse/CASSANDRA-11126) | select\_distinct\_with\_deletions\_test failing on non-vnode environments |  Major | . | Ryan McGuire | Sylvain Lebresne |
| [CASSANDRA-11332](https://issues.apache.org/jira/browse/CASSANDRA-11332) | nodes connect to themselves when NTS is used |  Major | Core | Brandon Williams | Branimir Lambov |
| [CASSANDRA-12418](https://issues.apache.org/jira/browse/CASSANDRA-12418) | sstabledump JSON fails after row tombstone |  Major | Tools | Keith Wansbrough | Dave Brosius |
| [CASSANDRA-11195](https://issues.apache.org/jira/browse/CASSANDRA-11195) | paging may returns incomplete results on small page size |  Major | . | Jim Witschey | Benjamin Lerer |
| [CASSANDRA-12423](https://issues.apache.org/jira/browse/CASSANDRA-12423) | Cells missing from compact storage table after upgrading from 2.1.9 to 3.7 |  Major | Local Write-Read Paths | Tomasz Grabiec | Stefania |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12123](https://issues.apache.org/jira/browse/CASSANDRA-12123) | dtest failure in upgrade\_tests.cql\_tests.TestCQLNodes3RF3\_Upgrade\_next\_2\_1\_x\_To\_current\_3\_x.cql3\_non\_compound\_range\_tombstones\_test |  Major | Coordination | Philip Thompson | Tyler Hobbs |
| [CASSANDRA-12353](https://issues.apache.org/jira/browse/CASSANDRA-12353) | CustomIndexTest can still fail due to async cleanup |  Minor | . | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-11701](https://issues.apache.org/jira/browse/CASSANDRA-11701) | [windows] dtest failure in cqlsh\_tests.cqlsh\_copy\_tests.CqlshCopyTest.test\_reading\_with\_skip\_and\_max\_rows |  Major | Tools | Russ Hatch | Stefania |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12331](https://issues.apache.org/jira/browse/CASSANDRA-12331) | Unreleased Resource: Sockets |  Major | Streaming and Messaging | Eduardo Aguinaga | Arunkumar M |
| [CASSANDRA-6216](https://issues.apache.org/jira/browse/CASSANDRA-6216) | Level Compaction should persist last compacted key per level |  Minor | . | sankalp kohli | Dikang Gu |


