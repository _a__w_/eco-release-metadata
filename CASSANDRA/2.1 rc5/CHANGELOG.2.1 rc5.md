
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1 rc5 - 2014-08-04



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7619](https://issues.apache.org/jira/browse/CASSANDRA-7619) | Normalize windows tool batch file environments |  Minor | Tools | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-7651](https://issues.apache.org/jira/browse/CASSANDRA-7651) | Tolerate min/max column names of different lengths for slice intersections |  Minor | . | Tyler Hobbs | Jonathan Ellis |
| [CASSANDRA-7638](https://issues.apache.org/jira/browse/CASSANDRA-7638) | Revisit GCInspector |  Minor | . | Brandon Williams | Yuki Morishita |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7571](https://issues.apache.org/jira/browse/CASSANDRA-7571) | DefsTables#dropType() calls MM#notifyUpdateUserType() instead of MM#notifyDropUserType(). |  Trivial | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-7572](https://issues.apache.org/jira/browse/CASSANDRA-7572) | cassandra-env.sh fails to find Java version if JAVA\_TOOL\_OPTIONS in environment |  Major | Configuration | Gregory Ramsperger | Gregory Ramsperger |
| [CASSANDRA-7578](https://issues.apache.org/jira/browse/CASSANDRA-7578) | Give read access to system.schema\_usertypes to all authenticated users |  Minor | Tools | Mike Adamson | Mike Adamson |
| [CASSANDRA-7576](https://issues.apache.org/jira/browse/CASSANDRA-7576) | DateType columns not properly converted to TimestampType when in ReversedType columns. |  Major | . | Karl Rieb | Karl Rieb |
| [CASSANDRA-7580](https://issues.apache.org/jira/browse/CASSANDRA-7580) | Super user cannot list permissions with cqlsh |  Minor | Tools | Mike Adamson | Brandon Williams |
| [CASSANDRA-7590](https://issues.apache.org/jira/browse/CASSANDRA-7590) | java.lang.AssertionError when using DROP INDEX IF EXISTS on non-existing index |  Minor | . | Hanh Dang | Robert Stupp |
| [CASSANDRA-6930](https://issues.apache.org/jira/browse/CASSANDRA-6930) | Dynamic Snitch isWorthMergingForRangeQuery Doesn't Handle Some Cases Optimally |  Minor | . | Tyler Hobbs | Ala' Alkhaldi |
| [CASSANDRA-7614](https://issues.apache.org/jira/browse/CASSANDRA-7614) | sstablelevelreset hangs when no sstables are found, Errors on bad ks/table |  Minor | Tools | Tyler Hobbs | Yuki Morishita |
| [CASSANDRA-7603](https://issues.apache.org/jira/browse/CASSANDRA-7603) | cqlsh fails to decode blobs |  Major | Tools | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-7617](https://issues.apache.org/jira/browse/CASSANDRA-7617) | UDT schema change messages are indistinguishable from table schema changes |  Major | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-7587](https://issues.apache.org/jira/browse/CASSANDRA-7587) | sstablekeys does not work with default configuration |  Minor | Tools | K. B. Hahn | Tyler Hobbs |
| [CASSANDRA-7627](https://issues.apache.org/jira/browse/CASSANDRA-7627) | Change line-endings for all .ps1 files to be CRLF |  Trivial | Packaging | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-7596](https://issues.apache.org/jira/browse/CASSANDRA-7596) | Don't swap min/max column names when mutating level or repairedAt |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-7625](https://issues.apache.org/jira/browse/CASSANDRA-7625) | cqlsh not paging results |  Major | Tools | Ryan McGuire | Tyler Hobbs |
| [CASSANDRA-7626](https://issues.apache.org/jira/browse/CASSANDRA-7626) | Tracing problems with 2.1 cqlsh |  Minor | Tools | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-7649](https://issues.apache.org/jira/browse/CASSANDRA-7649) | remove ability to change num\_tokens |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-7652](https://issues.apache.org/jira/browse/CASSANDRA-7652) | Native protocol v3 spec - minor typo |  Trivial | Documentation and Website | Robert Stupp | Robert Stupp |
| [CASSANDRA-7647](https://issues.apache.org/jira/browse/CASSANDRA-7647) | Track min/max timestamps of range tombstones |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-7644](https://issues.apache.org/jira/browse/CASSANDRA-7644) | tracing does not log commitlog/memtable ops when the coordinator is a replica |  Minor | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-7636](https://issues.apache.org/jira/browse/CASSANDRA-7636) | Data is not filtered out when using WHERE clause on cluster column on Column Family with row cache on. |  Major | . | Krzysztof Zarzycki | Marcus Eriksson |
| [CASSANDRA-7511](https://issues.apache.org/jira/browse/CASSANDRA-7511) | Always flush on TRUNCATE |  Minor | . | Viktor Jevdokimov | Jeremiah Jordan |
| [CASSANDRA-7593](https://issues.apache.org/jira/browse/CASSANDRA-7593) | Min/max column name collection broken with range tombstones |  Critical | . | Russ Hatch | Tyler Hobbs |
| [CASSANDRA-7668](https://issues.apache.org/jira/browse/CASSANDRA-7668) | Make gc\_grace\_seconds 7 days for system tables |  Minor | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-7665](https://issues.apache.org/jira/browse/CASSANDRA-7665) | nodetool scrub fails on system schema with UDTs |  Major | . | Jonathan Halliday | Yuki Morishita |
| [CASSANDRA-7656](https://issues.apache.org/jira/browse/CASSANDRA-7656) | UDT with null/missing attributes |  Minor | CQL | Jaroslav Kamenik | Tyler Hobbs |
| [CASSANDRA-7672](https://issues.apache.org/jira/browse/CASSANDRA-7672) | user types allow counters |  Major | . | Russ Hatch | Aleksey Yeschenko |
| [CASSANDRA-7601](https://issues.apache.org/jira/browse/CASSANDRA-7601) | Data loss after nodetool taketoken |  Minor | Testing | Philip Thompson | Brandon Williams |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7568](https://issues.apache.org/jira/browse/CASSANDRA-7568) | Replacing a dead node using replace\_address fails |  Minor | Testing | Ala' Alkhaldi | Brandon Williams |


