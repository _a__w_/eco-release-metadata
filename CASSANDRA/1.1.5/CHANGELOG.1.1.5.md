
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.1.5 - 2012-09-09



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4558](https://issues.apache.org/jira/browse/CASSANDRA-4558) | Configurable transport in CF RecordReader / RecordWriter |  Major | . | Piotr Kołaczkowski | Piotr Kołaczkowski |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4547](https://issues.apache.org/jira/browse/CASSANDRA-4547) | Log(info) schema changes. |  Minor | . | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-4606](https://issues.apache.org/jira/browse/CASSANDRA-4606) | Add Recommends: ntp to Debian packages |  Minor | Packaging | paul cannon | paul cannon |
| [CASSANDRA-4474](https://issues.apache.org/jira/browse/CASSANDRA-4474) | Respect five-minute flush moratorium after initial CL replay |  Minor | . | Jonathan Ellis | Yuki Morishita |
| [CASSANDRA-4581](https://issues.apache.org/jira/browse/CASSANDRA-4581) | Any SecondaryIndex implementation can be reloaded following config changes |  Minor | . | Sam Tunnicliffe | Sam Tunnicliffe |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4533](https://issues.apache.org/jira/browse/CASSANDRA-4533) | Multithreaded cache saving can skip caches |  Trivial | . | Zhu Han | Yuki Morishita |
| [CASSANDRA-4574](https://issues.apache.org/jira/browse/CASSANDRA-4574) | Missing String.format() in AntiEntropyService.java logs |  Trivial | . | Julien Lambert | Dave Brosius |
| [CASSANDRA-4568](https://issues.apache.org/jira/browse/CASSANDRA-4568) | countPendingHints JMX operation is returning garbage for the key |  Minor | . | J.B. Langston | Brandon Williams |
| [CASSANDRA-4587](https://issues.apache.org/jira/browse/CASSANDRA-4587) | StackOverflowError in LeveledCompactionStrategy$LeveledScanner.computeNext |  Minor | . | Christian Schnidrig | Jonathan Ellis |
| [CASSANDRA-4572](https://issues.apache.org/jira/browse/CASSANDRA-4572) | lost+found directory in the data dir causes problems again |  Major | . | Brandon Williams | Yuki Morishita |
| [CASSANDRA-4597](https://issues.apache.org/jira/browse/CASSANDRA-4597) | Impossible to set LeveledCompactionStrategy to a column family. |  Major | . | jal | Pavel Yaskevich |
| [CASSANDRA-4571](https://issues.apache.org/jira/browse/CASSANDRA-4571) | Strange permament socket descriptors increasing leads to "Too many open files" |  Critical | . | Serg Shnerson | Jonathan Ellis |
| [CASSANDRA-4601](https://issues.apache.org/jira/browse/CASSANDRA-4601) | Ensure unique commit log file names |  Critical | . | amorton | amorton |
| [CASSANDRA-4561](https://issues.apache.org/jira/browse/CASSANDRA-4561) | update column family fails |  Major | . | Zenek Kraweznik | Pavel Yaskevich |
| [CASSANDRA-4532](https://issues.apache.org/jira/browse/CASSANDRA-4532) | NPE when trying to select a slice from a composite table |  Minor | CQL | basanth gowda | Sylvain Lebresne |
| [CASSANDRA-4624](https://issues.apache.org/jira/browse/CASSANDRA-4624) | ORDER BY validation is not restrictive enough |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4612](https://issues.apache.org/jira/browse/CASSANDRA-4612) | cql error with ORDER BY when using IN |  Major | . | Tyler Patterson | Pavel Yaskevich |
| [CASSANDRA-4631](https://issues.apache.org/jira/browse/CASSANDRA-4631) | minimum stack size for u34 and later is 180k |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-4626](https://issues.apache.org/jira/browse/CASSANDRA-4626) | Multiple values for CurrentLocal Node ID |  Major | . | amorton | Sylvain Lebresne |
| [CASSANDRA-4590](https://issues.apache.org/jira/browse/CASSANDRA-4590) | "The system cannot find the path specified" when creating hard link on Windows |  Trivial | . | Allen Servedio | Jonathan Ellis |
| [CASSANDRA-4602](https://issues.apache.org/jira/browse/CASSANDRA-4602) | Stack Size on Sun JVM 1.6.0\_33 must be at least 160k |  Major | . | amorton | Jonathan Ellis |
| [CASSANDRA-4578](https://issues.apache.org/jira/browse/CASSANDRA-4578) | Dead lock in mutation stage when many concurrent writes to few columns |  Minor | . | Suguru Namura | Sylvain Lebresne |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4563](https://issues.apache.org/jira/browse/CASSANDRA-4563) | Change nodetool setcachecapcity to manipulate global caches |  Minor | . | Jeremy Hanna | Pavel Yaskevich |


