
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.2.9 - 2013-08-30



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5823](https://issues.apache.org/jira/browse/CASSANDRA-5823) | nodetool history logging |  Minor | Tools | Jason Brown | Jason Brown |
| [CASSANDRA-5868](https://issues.apache.org/jira/browse/CASSANDRA-5868) | Add key cache hit rate to CFMBean |  Minor | Tools | Jonathan Ellis | Yuki Morishita |
| [CASSANDRA-4766](https://issues.apache.org/jira/browse/CASSANDRA-4766) | ReverseCompaction |  Minor | . | Edward Capriolo | Sylvain Lebresne |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5825](https://issues.apache.org/jira/browse/CASSANDRA-5825) | StatusLogger should print out the "All time blocked" stat like tpstats does |  Minor | . | Jeremiah Jordan | Tyler Hobbs |
| [CASSANDRA-5845](https://issues.apache.org/jira/browse/CASSANDRA-5845) | Don't pull schema from higher major nodes; don't push schema to lower major nodes |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5798](https://issues.apache.org/jira/browse/CASSANDRA-5798) | Cqlsh should support multiline comments |  Trivial | Tools | Michaël Figuière | Aleksey Yeschenko |
| [CASSANDRA-5816](https://issues.apache.org/jira/browse/CASSANDRA-5816) | [PATCH] Debian packaging: also recommend chrony and ptpd in addition to ntp |  Minor | Packaging | Blair Zajac | Blair Zajac |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5827](https://issues.apache.org/jira/browse/CASSANDRA-5827) | Expose setters for consistency level in Hadoop config helper |  Trivial | . | Manoj Mainali | Manoj Mainali |
| [CASSANDRA-5824](https://issues.apache.org/jira/browse/CASSANDRA-5824) | Fix quoting in CqlPagingRecordReader and CqlRecordWriter |  Major | . | Alex Liu | Alex Liu |
| [CASSANDRA-5820](https://issues.apache.org/jira/browse/CASSANDRA-5820) | sstableloader broken in 1.2.7/1.2.8 |  Major | Tools | Nick Bailey | Tyler Hobbs |
| [CASSANDRA-5793](https://issues.apache.org/jira/browse/CASSANDRA-5793) | OPP seems completely unsupported |  Minor | . | Vara Kumar | Vara Kumar |
| [CASSANDRA-5670](https://issues.apache.org/jira/browse/CASSANDRA-5670) | running compact on an index did not compact two index files into one |  Minor | Tools | Cathy Daw | Jason Brown |
| [CASSANDRA-4774](https://issues.apache.org/jira/browse/CASSANDRA-4774) | IndexOutOfBoundsException in org.apache.cassandra.gms.Gossiper.sendGossip |  Minor | . | Benjamin Coverston | Chris Lohfink |
| [CASSANDRA-5718](https://issues.apache.org/jira/browse/CASSANDRA-5718) | Cql3 reader returns duplicate rows if the cluster column is reversed |  Major | . | Alex Liu | Alex Liu |
| [CASSANDRA-5378](https://issues.apache.org/jira/browse/CASSANDRA-5378) | Fat Client: No longer works in 1.2 |  Major | . | Carl Yeksigian | Carl Yeksigian |
| [CASSANDRA-5831](https://issues.apache.org/jira/browse/CASSANDRA-5831) | Running sstableupgrade on C\* 1.0 data dir, before starting C\* 1.2 for the first time breaks stuff |  Minor | Tools | Jeremiah Jordan | Tyler Hobbs |
| [CASSANDRA-5857](https://issues.apache.org/jira/browse/CASSANDRA-5857) | NumberFormatException during decommission |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-5855](https://issues.apache.org/jira/browse/CASSANDRA-5855) | Scrub does not understand compound primary key created in CQL 3 beta |  Major | Tools | J.B. Langston | Tyler Hobbs |
| [CASSANDRA-5800](https://issues.apache.org/jira/browse/CASSANDRA-5800) | Support pre-1.2 release CQL3 tables in CqlPagingRecordReader |  Minor | . | Alex Liu | Aleksey Yeschenko |
| [CASSANDRA-5856](https://issues.apache.org/jira/browse/CASSANDRA-5856) | AE in ArrayBackedSortedColumns |  Critical | . | Brandon Williams | Aleksey Yeschenko |
| [CASSANDRA-5900](https://issues.apache.org/jira/browse/CASSANDRA-5900) | Setting bloom filter fp chance to 1.0 causes ClassCastExceptions |  Minor | Tools | J.B. Langston | Jonathan Ellis |
| [CASSANDRA-5904](https://issues.apache.org/jira/browse/CASSANDRA-5904) | Don't announce schema version until we've loaded the changes locally |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5891](https://issues.apache.org/jira/browse/CASSANDRA-5891) | Scrub disk footprint needs to be reduced |  Minor | Tools | sankalp kohli | Lyuben Todorov |
| [CASSANDRA-5893](https://issues.apache.org/jira/browse/CASSANDRA-5893) | CqlParser throws StackOverflowError on bigger batch operation |  Major | . | Vincent Mallet | Aleksey Yeschenko |
| [CASSANDRA-5903](https://issues.apache.org/jira/browse/CASSANDRA-5903) | Integer overflow in OffHeapBitSet when bloomfilter \> 2GB |  Major | . | Taylan Develioglu | Vijay |
| [CASSANDRA-5907](https://issues.apache.org/jira/browse/CASSANDRA-5907) | Leveled compaction may cause overlap in L1 when L0 compaction get behind |  Minor | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-4551](https://issues.apache.org/jira/browse/CASSANDRA-4551) | Nodetool getendpoints keys do not work with spaces, key\_validation=ascii value of key =\> "a test"  no delimiter |  Trivial | Tools | Mark Valdez | Greg DeAngelis |
| [CASSANDRA-5882](https://issues.apache.org/jira/browse/CASSANDRA-5882) | Changing column type from int to bigint or vice versa causes decoding errors. |  Major | . | J.B. Langston | Sylvain Lebresne |
| [CASSANDRA-5926](https://issues.apache.org/jira/browse/CASSANDRA-5926) | The native protocol server can deadlock |  Major | . | Sylvain Lebresne | Sylvain Lebresne |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5828](https://issues.apache.org/jira/browse/CASSANDRA-5828) | add counters coverage to upgrade tests |  Major | Testing | Cathy Daw | Daniel Meyer |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5727](https://issues.apache.org/jira/browse/CASSANDRA-5727) | Evaluate default LCS sstable size |  Major | . | Jonathan Ellis | Daniel Meyer |


