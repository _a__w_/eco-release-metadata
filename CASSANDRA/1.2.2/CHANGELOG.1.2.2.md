
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.2.2 - 2013-02-25



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3378](https://issues.apache.org/jira/browse/CASSANDRA-3378) | Allow configuration of storage protocol socket buffer |  Minor | . | Brandon Williams | Michał Michalski |
| [CASSANDRA-4554](https://issues.apache.org/jira/browse/CASSANDRA-4554) | Log when a node is down longer than the hint window and we stop saving hints |  Minor | . | Jonathan Ellis | Vijay |
| [CASSANDRA-4464](https://issues.apache.org/jira/browse/CASSANDRA-4464) | expose 2I CFs to the rest of nodetool |  Minor | Tools | Jonathan Ellis | Jason Brown |
| [CASSANDRA-5038](https://issues.apache.org/jira/browse/CASSANDRA-5038) | LZ4Compressor |  Minor | . | T Jake Luciani | Adrien Grand |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4694](https://issues.apache.org/jira/browse/CASSANDRA-4694) | populate\_io\_cache\_on\_flush option should be configurable for each column family independently |  Minor | . | Alexey Zotov | Alexey Zotov |
| [CASSANDRA-5227](https://issues.apache.org/jira/browse/CASSANDRA-5227) | Binary protocol: avoid sending notification for 0.0.0.0 |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5226](https://issues.apache.org/jira/browse/CASSANDRA-5226) | CQL3 refactor to allow conversion function |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4295](https://issues.apache.org/jira/browse/CASSANDRA-4295) | Implement caching of authorization results |  Minor | CQL | Jonathan Ellis | Aleksey Yeschenko |
| [CASSANDRA-5112](https://issues.apache.org/jira/browse/CASSANDRA-5112) | Setting up authentication tables with custom authentication plugin |  Minor | CQL | Dirkjan Bussink | Aleksey Yeschenko |
| [CASSANDRA-4898](https://issues.apache.org/jira/browse/CASSANDRA-4898) | Authentication provider in Cassandra itself |  Major | . | Dirkjan Bussink | Aleksey Yeschenko |
| [CASSANDRA-5241](https://issues.apache.org/jira/browse/CASSANDRA-5241) | Fix forceBlockingFlush |  Major | . | Sylvain Lebresne | Marcus Eriksson |
| [CASSANDRA-5231](https://issues.apache.org/jira/browse/CASSANDRA-5231) | Add username autocompletion to cqlsh |  Trivial | Tools | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5233](https://issues.apache.org/jira/browse/CASSANDRA-5233) | Add cqlsh help for auth statements |  Trivial | Tools | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-4366](https://issues.apache.org/jira/browse/CASSANDRA-4366) | add UseCondCardMark XX jvm settings on jdk 1.7 |  Trivial | Packaging | Dave Brosius | Dave Brosius |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4916](https://issues.apache.org/jira/browse/CASSANDRA-4916) | Starting Cassandra throws EOF while reading saved cache |  Minor | . | Michael Kjellman | Dave Brosius |
| [CASSANDRA-5168](https://issues.apache.org/jira/browse/CASSANDRA-5168) | word count example fails with InvalidRequestException(why:Start key's token sorts after end token) |  Minor | . | Jeremy Hanna | Brandon Williams |
| [CASSANDRA-5185](https://issues.apache.org/jira/browse/CASSANDRA-5185) | symlinks to data directories are broken in 1.2.0 |  Major | . | Jeremiah Jordan | Yuki Morishita |
| [CASSANDRA-5188](https://issues.apache.org/jira/browse/CASSANDRA-5188) | o.a.c.hadoop.ConfigHelper should support setting Thrift frame and max message sizes. |  Minor | . | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-5189](https://issues.apache.org/jira/browse/CASSANDRA-5189) | compact storage metadata is broken |  Major | CQL | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-5207](https://issues.apache.org/jira/browse/CASSANDRA-5207) | Validate login for USE queries |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5208](https://issues.apache.org/jira/browse/CASSANDRA-5208) | cli shouldn't set default username and password |  Trivial | Tools | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5203](https://issues.apache.org/jira/browse/CASSANDRA-5203) | Repair command should report error when replica node is dead |  Trivial | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-5193](https://issues.apache.org/jira/browse/CASSANDRA-5193) | Memtable flushwriter can pick a blacklisted directory |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5198](https://issues.apache.org/jira/browse/CASSANDRA-5198) | Fix CQL3 loose type validation of constants |  Minor | . | Edward Capriolo | Sylvain Lebresne |
| [CASSANDRA-5214](https://issues.apache.org/jira/browse/CASSANDRA-5214) | AE in DataTracker.markCompacting |  Major | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-5216](https://issues.apache.org/jira/browse/CASSANDRA-5216) | fix bug on decommission and removeNode |  Major | . | yangwei | yangwei |
| [CASSANDRA-5196](https://issues.apache.org/jira/browse/CASSANDRA-5196) | IllegalStateException thrown when running new installation with old data directories |  Minor | . | Robbie Strickland | Aleksey Yeschenko |
| [CASSANDRA-5232](https://issues.apache.org/jira/browse/CASSANDRA-5232) | ALTER TABLE ADD - data loss |  Major | . | Vasily V.D. | Sylvain Lebresne |
| [CASSANDRA-5235](https://issues.apache.org/jira/browse/CASSANDRA-5235) | Error when executing a file contains CQL statement in cqlsh |  Minor | Tools | Shamim Ahmed | Aleksey Yeschenko |
| [CASSANDRA-5068](https://issues.apache.org/jira/browse/CASSANDRA-5068) | CLONE - Once a host has been hinted to, log messages for it repeat every 10 mins even if no hints are delivered |  Minor | . | Peter Haggerty | Brandon Williams |
| [CASSANDRA-5211](https://issues.apache.org/jira/browse/CASSANDRA-5211) | Migrating Clusters with gossip tables that have old dead nodes causes NPE, inability to join cluster |  Major | . | Rick Branson | Brandon Williams |
| [CASSANDRA-5225](https://issues.apache.org/jira/browse/CASSANDRA-5225) | Missing columns, errors when requesting specific columns from wide rows |  Critical | . | Tyler Hobbs | Sylvain Lebresne |
| [CASSANDRA-5197](https://issues.apache.org/jira/browse/CASSANDRA-5197) | Loading persisted ring state in a mixed cluster can throw AE |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-5244](https://issues.apache.org/jira/browse/CASSANDRA-5244) | Compactions don't work while node is bootstrapping |  Critical | . | Jouni Hartikainen | Brandon Williams |
| [CASSANDRA-5129](https://issues.apache.org/jira/browse/CASSANDRA-5129) | newly bootstrapping nodes hang indefinitely in STATUS:BOOT while JOINING cluster |  Major | . | Michael Kjellman | Brandon Williams |
| [CASSANDRA-5105](https://issues.apache.org/jira/browse/CASSANDRA-5105) | repair -pr throws EOFException |  Minor | . | Michael Kjellman | Yuki Morishita |
| [CASSANDRA-5248](https://issues.apache.org/jira/browse/CASSANDRA-5248) | Fix timestamp-based tomstone removal logic |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5080](https://issues.apache.org/jira/browse/CASSANDRA-5080) | cassandra-cli doesn't support JMX authentication. |  Major | Tools | Sergey Olefir | Michał Michalski |
| [CASSANDRA-5256](https://issues.apache.org/jira/browse/CASSANDRA-5256) | "Memory was freed" AssertionError During Major Compaction |  Critical | . | C. Scott Andreas | Jonathan Ellis |
| [CASSANDRA-5230](https://issues.apache.org/jira/browse/CASSANDRA-5230) | cql3 doesn't support multiple clauses on primary key components |  Major | . | Brandon Williams | Sylvain Lebresne |
| [CASSANDRA-5240](https://issues.apache.org/jira/browse/CASSANDRA-5240) | CQL3 has error with Compund row keys when secondray index involved |  Major | . | Shahryar Sedghi | Sylvain Lebresne |
| [CASSANDRA-5107](https://issues.apache.org/jira/browse/CASSANDRA-5107) | node fails to start because host id is missing |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-5146](https://issues.apache.org/jira/browse/CASSANDRA-5146) | repair -pr hangs |  Major | . | Michael Kjellman | Sylvain Lebresne |
| [CASSANDRA-5252](https://issues.apache.org/jira/browse/CASSANDRA-5252) | Starting Cassandra throws EOF while reading saved cache |  Minor | . | Drew Kutcharian | Dave Brosius |


