
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.7 beta 3 - 2010-11-01



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1533](https://issues.apache.org/jira/browse/CASSANDRA-1533) | expose MessagingService (OutboundTcpConnection) queue lengths in JMX |  Minor | Tools | Jonathan Ellis | Nirmal Ranganathan |
| [CASSANDRA-1518](https://issues.apache.org/jira/browse/CASSANDRA-1518) | remember ring state between restarts |  Major | . | Jonathan Ellis | Brandon Williams |
| [CASSANDRA-1580](https://issues.apache.org/jira/browse/CASSANDRA-1580) | make cli less crashy |  Major | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1417](https://issues.apache.org/jira/browse/CASSANDRA-1417) | add cache save/load |  Major | . | Jonathan Ellis | Matthew F. Dennis |
| [CASSANDRA-1579](https://issues.apache.org/jira/browse/CASSANDRA-1579) | tuneable column size for stress.py |  Major | . | Jonathan Ellis | Tyler Hobbs |
| [CASSANDRA-1583](https://issues.apache.org/jira/browse/CASSANDRA-1583) | add ColumnDef support to cli |  Major | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1578](https://issues.apache.org/jira/browse/CASSANDRA-1578) | Add support for column and row validators to cli |  Major | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1584](https://issues.apache.org/jira/browse/CASSANDRA-1584) | Add support for updating KS, CF definitions |  Major | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1519](https://issues.apache.org/jira/browse/CASSANDRA-1519) | Add threshold to dynamic snitch |  Minor | . | Jonathan Ellis | Brandon Williams |
| [CASSANDRA-1088](https://issues.apache.org/jira/browse/CASSANDRA-1088) | Enable cassandra-cli to list rows, and page through rows in a column family |  Major | Tools | Frank Du | Jim Ancona |
| [CASSANDRA-1582](https://issues.apache.org/jira/browse/CASSANDRA-1582) | create tests for cli |  Major | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1615](https://issues.apache.org/jira/browse/CASSANDRA-1615) | cli index scan support |  Major | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1616](https://issues.apache.org/jira/browse/CASSANDRA-1616) | cli run from file |  Minor | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1653](https://issues.apache.org/jira/browse/CASSANDRA-1653) | add truncate support to cli |  Minor | Tools | Jonathan Ellis | Pavel Yaskevich |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1481](https://issues.apache.org/jira/browse/CASSANDRA-1481) | PropertyFileEndPointSnitch synchronization is a bottleneck |  Minor | . | Jonathan Ellis | Chris Goffinet |
| [CASSANDRA-1531](https://issues.apache.org/jira/browse/CASSANDRA-1531) | Add secondary index ops to stress.py |  Major | Secondary Indexes | Stu Hood | Stu Hood |
| [CASSANDRA-1271](https://issues.apache.org/jira/browse/CASSANDRA-1271) | Improve permissions to allow control over creation/removal/listing of Keyspaces |  Minor | . | Stu Hood | Eric Evans |
| [CASSANDRA-1558](https://issues.apache.org/jira/browse/CASSANDRA-1558) | add memtable, cache to what GCInspector logs |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1552](https://issues.apache.org/jira/browse/CASSANDRA-1552) | log warning when using randomly generated token |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1217](https://issues.apache.org/jira/browse/CASSANDRA-1217) | jmx: differentiate between thread pools on the request paths and internal ones |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1570](https://issues.apache.org/jira/browse/CASSANDRA-1570) | sort is not necessary for getMaxEndpointStateVersion() in Gossiper |  Minor | . | iryoung jeong | iryoung jeong |
| [CASSANDRA-1436](https://issues.apache.org/jira/browse/CASSANDRA-1436) | Take advantage of Avro IDL includes |  Major | . | Stu Hood | Gary Dusbabek |
| [CASSANDRA-1532](https://issues.apache.org/jira/browse/CASSANDRA-1532) | Expose dynamic secondary indexes via system\_update\_column\_family |  Minor | Secondary Indexes | Gary Dusbabek | Jonathan Ellis |
| [CASSANDRA-1593](https://issues.apache.org/jira/browse/CASSANDRA-1593) | Clarify NetworkTopologyStrategy |  Major | . | Stu Hood | Jonathan Ellis |
| [CASSANDRA-1595](https://issues.apache.org/jira/browse/CASSANDRA-1595) | log auto-guessed memtable size |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1572](https://issues.apache.org/jira/browse/CASSANDRA-1572) | Faster index sampling |  Minor | . | Jonathan Ellis | Stu Hood |
| [CASSANDRA-1596](https://issues.apache.org/jira/browse/CASSANDRA-1596) | failed compactions should not leave tmp files behind |  Minor | . | Peter Schuller | Gary Dusbabek |
| [CASSANDRA-1607](https://issues.apache.org/jira/browse/CASSANDRA-1607) | Remove ConsistencyLevel.ZERO |  Major | CQL | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-1007](https://issues.apache.org/jira/browse/CASSANDRA-1007) | Make memtable flush thresholds per-CF instead of global |  Minor | . | Jonathan Ellis | Jon Hermes |
| [CASSANDRA-1603](https://issues.apache.org/jira/browse/CASSANDRA-1603) | cli: allow entering binary data to columns with no metadata |  Major | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1626](https://issues.apache.org/jira/browse/CASSANDRA-1626) | Allow creating keyspace with no replicas configured |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1624](https://issues.apache.org/jira/browse/CASSANDRA-1624) | Track number of sstables accessed per read |  Minor | . | Stu Hood | Stu Hood |
| [CASSANDRA-1622](https://issues.apache.org/jira/browse/CASSANDRA-1622) | optimize quorum read |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1619](https://issues.apache.org/jira/browse/CASSANDRA-1619) | improve cli list command |  Major | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1636](https://issues.apache.org/jira/browse/CASSANDRA-1636) | Default JVM settings in cassandra.bat need some TLC |  Major | . | Nate McCall | Nate McCall |
| [CASSANDRA-1367](https://issues.apache.org/jira/browse/CASSANDRA-1367) | Upgrade to Thrift 0.5.0 |  Minor | . | Johan Oskarsson | T Jake Luciani |
| [CASSANDRA-1637](https://issues.apache.org/jira/browse/CASSANDRA-1637) | expose per-CF memtable settings in JMX |  Minor | Tools | Jonathan Ellis | Jon Hermes |
| [CASSANDRA-1634](https://issues.apache.org/jira/browse/CASSANDRA-1634) | Allow for cassandra-cli to update memtable\_\* settings on a ColumnFamily |  Major | Tools | Nate McCall | Jonathan Ellis |
| [CASSANDRA-615](https://issues.apache.org/jira/browse/CASSANDRA-615) | refactor o.a.c.cli.CliClient |  Minor | Tools | Eric Evans | Pavel Yaskevich |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1560](https://issues.apache.org/jira/browse/CASSANDRA-1560) | describe\_keyspace() should include strategy\_options |  Minor | . | Tyler Hobbs | Jon Hermes |
| [CASSANDRA-1550](https://issues.apache.org/jira/browse/CASSANDRA-1550) | enable/disable HH via JMX |  Minor | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1541](https://issues.apache.org/jira/browse/CASSANDRA-1541) | KEYS indexes recreated after first restart |  Major | . | Stu Hood | Jonathan Ellis |
| [CASSANDRA-1564](https://issues.apache.org/jira/browse/CASSANDRA-1564) | incomplete sstables for System keyspace are not scrubbed before opening it |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1293](https://issues.apache.org/jira/browse/CASSANDRA-1293) | add locking around row cache accesses |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1330](https://issues.apache.org/jira/browse/CASSANDRA-1330) | AssertionError: discard at CommitLogContext(file=...) is not after last flush at  ... |  Major | . | Viliam Holub | Jonathan Ellis |
| [CASSANDRA-1568](https://issues.apache.org/jira/browse/CASSANDRA-1568) | nodeprobe help message is missing option to compact specific keyspace |  Trivial | Tools | Edward Capriolo | Edward Capriolo |
| [CASSANDRA-1573](https://issues.apache.org/jira/browse/CASSANDRA-1573) | StreamOut fails to start an empty stream |  Blocker | . | Nick Bailey | Jonathan Ellis |
| [CASSANDRA-1574](https://issues.apache.org/jira/browse/CASSANDRA-1574) | Bootstrapping is broken |  Blocker | . | Nick Bailey | Nick Bailey |
| [CASSANDRA-1554](https://issues.apache.org/jira/browse/CASSANDRA-1554) | extend authorization to column families |  Major | . | Eric Evans | Eric Evans |
| [CASSANDRA-1571](https://issues.apache.org/jira/browse/CASSANDRA-1571) | Secondary Indexes aren't updated when removing whole row |  Major | Secondary Indexes | Petr Odut | Jonathan Ellis |
| [CASSANDRA-1556](https://issues.apache.org/jira/browse/CASSANDRA-1556) | InvalidRequestException(why='') returned from system\_add\_keyspace when strategy\_class not found |  Trivial | CQL | amorton | amorton |
| [CASSANDRA-1428](https://issues.apache.org/jira/browse/CASSANDRA-1428) | Rejecting keyspace creation when RF \> N is incorrect |  Minor | . | Cliff Moon | Jonathan Ellis |
| [CASSANDRA-1575](https://issues.apache.org/jira/browse/CASSANDRA-1575) | suggest avoiding broken openjdk6 on Debian as build-dep |  Minor | Packaging | Peter Schuller | Eric Evans |
| [CASSANDRA-1561](https://issues.apache.org/jira/browse/CASSANDRA-1561) | Disallow bootstrapping to a token that is already owned by a live node |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1557](https://issues.apache.org/jira/browse/CASSANDRA-1557) | ColumnFamilyStore masking IOException from FileUtils as IOError |  Minor | CQL | amorton | Jonathan Ellis |
| [CASSANDRA-1442](https://issues.apache.org/jira/browse/CASSANDRA-1442) | Get Range Slices is broken |  Critical | . | Moleza Moleza | Stu Hood |
| [CASSANDRA-1538](https://issues.apache.org/jira/browse/CASSANDRA-1538) | 0.7 on Windows |  Minor | Packaging | Jonathan Ellis | Vladimir Loncar |
| [CASSANDRA-1604](https://issues.apache.org/jira/browse/CASSANDRA-1604) | Database Descriptor has log message that mashes words |  Trivial | . | Edward Capriolo | Edward Capriolo |
| [CASSANDRA-1605](https://issues.apache.org/jira/browse/CASSANDRA-1605) | RemoveToken waits for dead nodes |  Major | Tools | Nick Bailey | Nick Bailey |
| [CASSANDRA-1612](https://issues.apache.org/jira/browse/CASSANDRA-1612) | cli support for strategy\_options |  Major | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1609](https://issues.apache.org/jira/browse/CASSANDRA-1609) | Cluster restart re-adds removed tokens |  Major | . | Nick Bailey | Jonathan Ellis |
| [CASSANDRA-1606](https://issues.apache.org/jira/browse/CASSANDRA-1606) | Thrift CfDef incomplete; missing row/key\_save\_period\_in\_seconds |  Major | . | Jon Hermes | Jon Hermes |
| [CASSANDRA-1613](https://issues.apache.org/jira/browse/CASSANDRA-1613) | index created from cli does not show up in keyspace metadata |  Major | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1620](https://issues.apache.org/jira/browse/CASSANDRA-1620) | statistics not created after streaming data |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1623](https://issues.apache.org/jira/browse/CASSANDRA-1623) | BUG: secondaryIndexes AND multiple index expressions can cause timesouts |  Major | . | Jason Tanner | Jonathan Ellis |
| [CASSANDRA-1628](https://issues.apache.org/jira/browse/CASSANDRA-1628) | RedHat init script status is a no-op |  Minor | . | Adam Gray | Adam Gray |
| [CASSANDRA-1630](https://issues.apache.org/jira/browse/CASSANDRA-1630) | system\_rename\_\* methods need to be removed until we can solve compaction and flush races. |  Major | CQL | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-1617](https://issues.apache.org/jira/browse/CASSANDRA-1617) | BufferUnderflowException occurs in RowMutationVerbHandler |  Major | . | Michael Moores | Brandon Williams |
| [CASSANDRA-1635](https://issues.apache.org/jira/browse/CASSANDRA-1635) | cli not picking up type annotation on set |  Major | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1642](https://issues.apache.org/jira/browse/CASSANDRA-1642) | JOIN verb was removed, breaking serialization by ordinal. |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-1641](https://issues.apache.org/jira/browse/CASSANDRA-1641) | auto-guessed memtable sizes are too high |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1643](https://issues.apache.org/jira/browse/CASSANDRA-1643) | Endpoint cache for a token should be part of AbstractReplicationStrategy and not Snitch |  Major | . | Rishi Bhardwaj | Jonathan Ellis |
| [CASSANDRA-1644](https://issues.apache.org/jira/browse/CASSANDRA-1644) | Commitlog.recover can delete the commitlog segment instance() just created |  Blocker | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1646](https://issues.apache.org/jira/browse/CASSANDRA-1646) | Using QUORUM and replication factor of 1 now causes a timeout exception |  Critical | . | Todd Nine | Jonathan Ellis |
| [CASSANDRA-1639](https://issues.apache.org/jira/browse/CASSANDRA-1639) | NPE in StorageService when cluster is first being created |  Minor | Tools | Dave Woldrich |  |
| [CASSANDRA-1650](https://issues.apache.org/jira/browse/CASSANDRA-1650) | removetoken is broken |  Major | . | Brandon Williams | Nick Bailey |
| [CASSANDRA-1649](https://issues.apache.org/jira/browse/CASSANDRA-1649) | Queries on system keyspace over thrift api all fail |  Minor | CQL | paul cannon | Jonathan Ellis |
| [CASSANDRA-1655](https://issues.apache.org/jira/browse/CASSANDRA-1655) | Config converter fails |  Critical | Tools | Stu Hood | Stu Hood |
| [CASSANDRA-1659](https://issues.apache.org/jira/browse/CASSANDRA-1659) | Config converter should set framed transport by default |  Major | . | Stu Hood | Stu Hood |
| [CASSANDRA-1661](https://issues.apache.org/jira/browse/CASSANDRA-1661) | Use of ByteBuffer limit() must account for arrayOffset() |  Major | . | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-1648](https://issues.apache.org/jira/browse/CASSANDRA-1648) | CliTest crashing intermittently |  Minor | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1672](https://issues.apache.org/jira/browse/CASSANDRA-1672) | Hinted HandOff are not delivered correctly following change to ByteBuffer |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-1666](https://issues.apache.org/jira/browse/CASSANDRA-1666) | logging error on C\* startup: Nodes /10.194.241.188 and /10.194.241.188 have the same token 85070591730234615865843651857942052863 |  Trivial | . | Matthew F. Dennis | Jonathan Ellis |
| [CASSANDRA-1631](https://issues.apache.org/jira/browse/CASSANDRA-1631) | dropping column families and keyspaces races with compaction and flushing |  Major | . | Gary Dusbabek | Gary Dusbabek |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1588](https://issues.apache.org/jira/browse/CASSANDRA-1588) | Explicitly expose ongoing per-node drain status and est. % done |  Minor | Tools | paul cannon | Nick Bailey |
| [CASSANDRA-1586](https://issues.apache.org/jira/browse/CASSANDRA-1586) | Explicitly expose ongoing per-node compaction tasks and est. % done |  Minor | Tools | paul cannon | Jonathan Ellis |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1516](https://issues.apache.org/jira/browse/CASSANDRA-1516) | Explicitly expose ongoing per-node tasks (like compactions, repair, etc) and est. % done |  Minor | . | paul cannon |  |
| [CASSANDRA-1288](https://issues.apache.org/jira/browse/CASSANDRA-1288) | replace one-off Timers with a central Timer in StorageService |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1502](https://issues.apache.org/jira/browse/CASSANDRA-1502) | remove IClock from internals |  Major | . | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-1647](https://issues.apache.org/jira/browse/CASSANDRA-1647) | remove preload\_row\_cache |  Minor | . | Jonathan Ellis | Jonathan Ellis |


