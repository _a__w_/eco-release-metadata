
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.0.2 - 2011-11-07



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3326](https://issues.apache.org/jira/browse/CASSANDRA-3326) | Add timing information to cassandra-cli queries |  Minor | Tools | Radim Kolar | satish babu krishnamoorthy |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3425](https://issues.apache.org/jira/browse/CASSANDRA-3425) | Enhance BUILD.XML to be able to Install and Deploy APACHE-CASSANDRA-CLIENTUTIL.JAR  into Maven Repos for use by JDBC Driver Build processes |  Minor | Packaging | Rick Shaw | Rick Shaw |
| [CASSANDRA-3432](https://issues.apache.org/jira/browse/CASSANDRA-3432) | Avoid large array allocation for compressed chunk offsets |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3316](https://issues.apache.org/jira/browse/CASSANDRA-3316) | Add a JMX call to force cleaning repair sessions (in case they are hang up) |  Minor | . | Sylvain Lebresne | Yuki Morishita |
| [CASSANDRA-3420](https://issues.apache.org/jira/browse/CASSANDRA-3420) | test run from ant |  Minor | Testing | Eric Evans | Eric Evans |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3388](https://issues.apache.org/jira/browse/CASSANDRA-3388) | StorageService.setMode() is used inconsistently |  Trivial | . | Tyler Hobbs | Sylvain Lebresne |
| [CASSANDRA-3427](https://issues.apache.org/jira/browse/CASSANDRA-3427) | CompressionMetadata is not shared across threads, we create a new one for each read |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3433](https://issues.apache.org/jira/browse/CASSANDRA-3433) | Describe ring is broken |  Major | CQL, Tools | Nick Bailey | Nick Bailey |
| [CASSANDRA-3399](https://issues.apache.org/jira/browse/CASSANDRA-3399) | Truncate disregards running compactions when deleting sstables |  Major | . | Sylvain Lebresne | Jonathan Ellis |
| [CASSANDRA-3436](https://issues.apache.org/jira/browse/CASSANDRA-3436) | CQL Metadata has inconsistent schema nomenclature |  Trivial | . | Kelley Reynolds | Jonathan Ellis |
| [CASSANDRA-3441](https://issues.apache.org/jira/browse/CASSANDRA-3441) | PerRowSecondaryIndexes skip the first column on update |  Major | . | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-3421](https://issues.apache.org/jira/browse/CASSANDRA-3421) | Can create a table with DecimalType comparator but CQL explodes trying to actually use it. |  Major | . | Kelley Reynolds | Rick Shaw |
| [CASSANDRA-3438](https://issues.apache.org/jira/browse/CASSANDRA-3438) | sstableloader fails |  Major | . | Jeremy Pinkham | Sylvain Lebresne |
| [CASSANDRA-3451](https://issues.apache.org/jira/browse/CASSANDRA-3451) | estimated row sizes regression |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3303](https://issues.apache.org/jira/browse/CASSANDRA-3303) | Short reads protection results in returning more columns than asked for |  Minor | . | Sylvain Lebresne | Byron Clark |
| [CASSANDRA-3395](https://issues.apache.org/jira/browse/CASSANDRA-3395) | Quorum returns incorrect results during hinted handoff |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3449](https://issues.apache.org/jira/browse/CASSANDRA-3449) | Missing null check in digest retry part of read path |  Major | . | Jonathan Ellis | Jonathan Ellis |


