
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1 rc1 - 2014-06-02



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6344](https://issues.apache.org/jira/browse/CASSANDRA-6344) | When running CQLSH with file input, exit with error status code if script fails |  Major | Tools | Branden Visser | Mikhail Stepura |
| [CASSANDRA-7172](https://issues.apache.org/jira/browse/CASSANDRA-7172) | cqlsh: Accept and execute CQL statement(s) from command-line parameter |  Trivial | Tools | PJ | Mikhail Stepura |
| [CASSANDRA-6421](https://issues.apache.org/jira/browse/CASSANDRA-6421) | Add bash completion to nodetool |  Trivial | Tools | Cyril Scetbon | Cyril Scetbon |
| [CASSANDRA-7248](https://issues.apache.org/jira/browse/CASSANDRA-7248) | Tuple type |  Major | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-7264](https://issues.apache.org/jira/browse/CASSANDRA-7264) | Add conditional support for the creation/deletion of users |  Minor | . | Hamilton Tran | Aleksey Yeschenko |
| [CASSANDRA-6357](https://issues.apache.org/jira/browse/CASSANDRA-6357) | Flush memtables to separate directory |  Minor | . | Patrick McFadin | Jonathan Ellis |
| [CASSANDRA-6757](https://issues.apache.org/jira/browse/CASSANDRA-6757) | SliceQueryFilter can accept multiple ColumnSlice but not exposed to thrift |  Major | . | Edward Capriolo | Edward Capriolo |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7116](https://issues.apache.org/jira/browse/CASSANDRA-7116) | Use ConcurrentLinkedQueue instead of NonBlockingHashSet in AbstractRowResolver |  Trivial | . | Benedict | Benedict |
| [CASSANDRA-7132](https://issues.apache.org/jira/browse/CASSANDRA-7132) | Add a new Snitch for Google Cloud Platform |  Minor | . | Brian Lynch | Brian Lynch |
| [CASSANDRA-3668](https://issues.apache.org/jira/browse/CASSANDRA-3668) | Parallel streaming for sstableloader |  Minor | Tools | Manish Zope | Joshua McKenzie |
| [CASSANDRA-6551](https://issues.apache.org/jira/browse/CASSANDRA-6551) | Rack-aware batchlog replication |  Minor | . | Rick Branson | Mikhail Stepura |
| [CASSANDRA-7142](https://issues.apache.org/jira/browse/CASSANDRA-7142) | Suggest CTRL-C/D or semicolon after three blank lines in cqlsh |  Trivial | Tools | Tyler Hobbs | Mikhail Stepura |
| [CASSANDRA-7107](https://issues.apache.org/jira/browse/CASSANDRA-7107) | General minor tidying of CollationController path |  Minor | . | Benedict | Benedict |
| [CASSANDRA-6861](https://issues.apache.org/jira/browse/CASSANDRA-6861) | Optimise our Netty 4 integration |  Minor | . | Benedict | T Jake Luciani |
| [CASSANDRA-7137](https://issues.apache.org/jira/browse/CASSANDRA-7137) | Check for Java \>= 1.7 in bin/cassandra |  Minor | . | Tyler Hobbs | Brandon Williams |
| [CASSANDRA-6326](https://issues.apache.org/jira/browse/CASSANDRA-6326) | Snapshot should create manifest file |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-7191](https://issues.apache.org/jira/browse/CASSANDRA-7191) | reduce needless garbage/thrashing in pending range calculation |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-7128](https://issues.apache.org/jira/browse/CASSANDRA-7128) | Upgrade NBHM |  Trivial | . | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-7001](https://issues.apache.org/jira/browse/CASSANDRA-7001) | Windows launch feature parity - augment launch process using PowerShell to match capabilities of \*nix launching |  Major | Packaging | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-5663](https://issues.apache.org/jira/browse/CASSANDRA-5663) | Add server side write batching to the native transport |  Major | . | Sylvain Lebresne | Benedict |
| [CASSANDRA-6134](https://issues.apache.org/jira/browse/CASSANDRA-6134) | Asynchronous batchlog replay |  Minor | . | Oleg Anastasyev | Oleg Anastasyev |
| [CASSANDRA-7210](https://issues.apache.org/jira/browse/CASSANDRA-7210) | Add --resolve-ip option on 'nodetool ring' |  Trivial | Tools | Yasuharu Goto | Yasuharu Goto |
| [CASSANDRA-7253](https://issues.apache.org/jira/browse/CASSANDRA-7253) | Improve javadoc for KeysIndex |  Trivial | . | Peter | Peter |
| [CASSANDRA-7242](https://issues.apache.org/jira/browse/CASSANDRA-7242) | More compaction visibility into thread pool and per CF |  Minor | . | Chris Lohfink | Chris Lohfink |
| [CASSANDRA-7139](https://issues.apache.org/jira/browse/CASSANDRA-7139) | Default concurrent\_compactors is probably too high |  Minor | . | Benedict | Jonathan Ellis |
| [CASSANDRA-7284](https://issues.apache.org/jira/browse/CASSANDRA-7284) | ClassCastException in HintedHandoffManager.pagingFinished |  Minor | . | Benedict | Benedict |
| [CASSANDRA-6975](https://issues.apache.org/jira/browse/CASSANDRA-6975) | Allow usage of QueryOptions in CQLStatement.executeInternal |  Minor | CQL | Mikhail Stepura | Sylvain Lebresne |
| [CASSANDRA-7206](https://issues.apache.org/jira/browse/CASSANDRA-7206) | UDT - allow null / non-existant attributes |  Major | CQL | Robert Stupp | Sylvain Lebresne |
| [CASSANDRA-6484](https://issues.apache.org/jira/browse/CASSANDRA-6484) | cassandra-shuffle not working with authentication |  Minor | Tools | Gibheer | Aleksey Yeschenko |
| [CASSANDRA-7231](https://issues.apache.org/jira/browse/CASSANDRA-7231) | Support more concurrent requests per native transport connection |  Minor | . | Benedict | Aleksey Yeschenko |
| [CASSANDRA-7160](https://issues.apache.org/jira/browse/CASSANDRA-7160) | clean up tools/bin |  Minor | Packaging | Brandon Williams | Michael Shuler |
| [CASSANDRA-7320](https://issues.apache.org/jira/browse/CASSANDRA-7320) | Swap local and global default read repair chances |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-4718](https://issues.apache.org/jira/browse/CASSANDRA-4718) | More-efficient ExecutorService for improved throughput |  Minor | . | Jonathan Ellis | Benedict |
| [CASSANDRA-7147](https://issues.apache.org/jira/browse/CASSANDRA-7147) | Add a new snitch for Apache Cloudstack platforms |  Minor | . | Pierre-Yves Ritschard | Pierre-Yves Ritschard |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7073](https://issues.apache.org/jira/browse/CASSANDRA-7073) | Plug holes in resource release when wiring up StreamSession |  Minor | . | Benedict | Benedict |
| [CASSANDRA-6546](https://issues.apache.org/jira/browse/CASSANDRA-6546) | disablethrift results in unclosed file descriptors |  Minor | . | Jason Harvey | Mikhail Stepura |
| [CASSANDRA-7126](https://issues.apache.org/jira/browse/CASSANDRA-7126) | relocate doesn't update system.peers correctly |  Blocker | . | T Jake Luciani | Brandon Williams |
| [CASSANDRA-7138](https://issues.apache.org/jira/browse/CASSANDRA-7138) | Thrift super columns filter conversion is broken on 2.1 |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-7112](https://issues.apache.org/jira/browse/CASSANDRA-7112) | ava.lang.ClassCastException: org.apache.cassandra.db.composites.CompoundComposite cannot be cast to org.apache.cassandra.db.composites.CellName |  Major | . | Tyler Hobbs | Sylvain Lebresne |
| [CASSANDRA-7122](https://issues.apache.org/jira/browse/CASSANDRA-7122) | Replacement nodes have null entries in system.peers |  Major | . | Richard Low | Brandon Williams |
| [CASSANDRA-6562](https://issues.apache.org/jira/browse/CASSANDRA-6562) | sstablemetadata{.bat} location inconsistency |  Trivial | Tools | Chris Burroughs | Brandon Williams |
| [CASSANDRA-7183](https://issues.apache.org/jira/browse/CASSANDRA-7183) | BackgroundActivityMonitor.readAndCompute only returns half of the values |  Minor | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-6162](https://issues.apache.org/jira/browse/CASSANDRA-6162) | Cassandra does not start on Ubuntu 13.04 RUssian |  Major | Packaging | Sergey Nagaytsev | Dave Brosius |
| [CASSANDRA-6959](https://issues.apache.org/jira/browse/CASSANDRA-6959) | Reusing Keyspace and CF names raises assertion errors |  Major | . | Ryan McGuire | Benedict |
| [CASSANDRA-7185](https://issues.apache.org/jira/browse/CASSANDRA-7185) | cqlsh can't tab-complete disabling compaction |  Trivial | . | Brandon Williams | Mikhail Stepura |
| [CASSANDRA-6974](https://issues.apache.org/jira/browse/CASSANDRA-6974) | Replaying archived commitlogs isn't working |  Major | . | Ryan McGuire | Benedict |
| [CASSANDRA-7092](https://issues.apache.org/jira/browse/CASSANDRA-7092) | Visibility issue in StreamSession |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-7196](https://issues.apache.org/jira/browse/CASSANDRA-7196) | "Select" query with "IN" restriction times out in CQLSH |  Major | . | Mikhail Stepura | T Jake Luciani |
| [CASSANDRA-5635](https://issues.apache.org/jira/browse/CASSANDRA-5635) | ThriftServer.stop() hangs forever |  Trivial | . | Robert Stupp | Dave Brosius |
| [CASSANDRA-7189](https://issues.apache.org/jira/browse/CASSANDRA-7189) | Repair hang when specified CF does not exist |  Minor | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-7211](https://issues.apache.org/jira/browse/CASSANDRA-7211) | Triggers Demo seems incompatible with row markers |  Trivial | . | Anthony Cozzie | Anthony Cozzie |
| [CASSANDRA-7218](https://issues.apache.org/jira/browse/CASSANDRA-7218) | cassandra-all:2.1.0-beta1 maven dependency failing |  Major | CQL | Prasanth Gullapalli | Dave Brosius |
| [CASSANDRA-7227](https://issues.apache.org/jira/browse/CASSANDRA-7227) | Fix bugs in CellName comparison |  Major | Local Write-Read Paths | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-6877](https://issues.apache.org/jira/browse/CASSANDRA-6877) | pig tests broken |  Major | . | Brandon Williams | Sam Tunnicliffe |
| [CASSANDRA-7197](https://issues.apache.org/jira/browse/CASSANDRA-7197) | Dead code in trunk |  Minor | . | Daniel Shelepov | Daniel Shelepov |
| [CASSANDRA-6643](https://issues.apache.org/jira/browse/CASSANDRA-6643) | Limit user types to the keyspace they are defined in |  Major | CQL | Russ Hatch | Sylvain Lebresne |
| [CASSANDRA-7208](https://issues.apache.org/jira/browse/CASSANDRA-7208) | CollectionType validate method should have a proper implementation |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-7100](https://issues.apache.org/jira/browse/CASSANDRA-7100) | Regression in 2.0.x - Cql3 reader returns duplicate rows if the cluster column is reversed |  Major | . | Rohit Rai | Alex Liu |
| [CASSANDRA-7093](https://issues.apache.org/jira/browse/CASSANDRA-7093) | ConfigHelper.setInputColumnFamily incompatible with upper-cased keyspaces since 2.0.7 |  Major | . | Maxime Nay | Alex Liu |
| [CASSANDRA-7209](https://issues.apache.org/jira/browse/CASSANDRA-7209) | Consider changing UDT serialization format before 2.1 release. |  Major | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-7270](https://issues.apache.org/jira/browse/CASSANDRA-7270) | nodetool netstats broken |  Minor | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-7194](https://issues.apache.org/jira/browse/CASSANDRA-7194) | CliTest unit test fails on Windows |  Trivial | Testing | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-7201](https://issues.apache.org/jira/browse/CASSANDRA-7201) | Regression: ColumnFamilyStoreTest, NativeCellTest, SSTableMetadataTest unit tests on 2.1 |  Major | Testing | Michael Shuler | Benedict |
| [CASSANDRA-7285](https://issues.apache.org/jira/browse/CASSANDRA-7285) | Full Checksum does not include full stable |  Major | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-7010](https://issues.apache.org/jira/browse/CASSANDRA-7010) | bootstrap\_test simple\_bootstrap\_test dtest fails in 2.1 |  Major | Testing | Michael Shuler | Marcus Eriksson |
| [CASSANDRA-7120](https://issues.apache.org/jira/browse/CASSANDRA-7120) | Bad paging state returned for prepared statements for last page |  Major | CQL | Tyler Hobbs | Sylvain Lebresne |
| [CASSANDRA-7287](https://issues.apache.org/jira/browse/CASSANDRA-7287) | Pig CqlStorage test fails with IAE |  Major | Testing | Brandon Williams | Sylvain Lebresne |
| [CASSANDRA-7294](https://issues.apache.org/jira/browse/CASSANDRA-7294) | FileCache metrics incorrectly named |  Major | Tools | Nick Bailey | Nick Bailey |
| [CASSANDRA-7300](https://issues.apache.org/jira/browse/CASSANDRA-7300) | BatchlogManager#countAllBatches doesn't swap strings for the count query. |  Trivial | . | Lyuben Todorov | Lyuben Todorov |
| [CASSANDRA-7315](https://issues.apache.org/jira/browse/CASSANDRA-7315) | Hadoop Compatibility Broken |  Major | . | Benjamin Coverston | Benjamin Coverston |
| [CASSANDRA-7279](https://issues.apache.org/jira/browse/CASSANDRA-7279) | MultiSliceTest.test\_with\_overlap\* unit tests failing in trunk |  Minor | Testing | Michael Shuler | Sylvain Lebresne |
| [CASSANDRA-7027](https://issues.apache.org/jira/browse/CASSANDRA-7027) | Back port MultiSliceRequest to 2.1 |  Major | . | Edward Capriolo | Edward Capriolo |
| [CASSANDRA-7267](https://issues.apache.org/jira/browse/CASSANDRA-7267) | Embedded sets in user defined data-types are not updating |  Major | . | Thomas Zimmer | Mikhail Stepura |
| [CASSANDRA-7291](https://issues.apache.org/jira/browse/CASSANDRA-7291) | java.lang.AssertionError when adding a collection to a UDT |  Major | CQL | Mikhail Stepura | Sylvain Lebresne |
| [CASSANDRA-7319](https://issues.apache.org/jira/browse/CASSANDRA-7319) | Fix availability validation for LOCAL\_ONE CL |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6875](https://issues.apache.org/jira/browse/CASSANDRA-6875) | CQL3: select multiple CQL rows in a single partition using IN |  Minor | CQL | Nicolas Favre-Felix | Tyler Hobbs |
| [CASSANDRA-7241](https://issues.apache.org/jira/browse/CASSANDRA-7241) | Pig test fails on 2.1 branch |  Major | . | Alex Liu | Brandon Williams |
| [CASSANDRA-7322](https://issues.apache.org/jira/browse/CASSANDRA-7322) | CqlPagingRecordReader returns partial or no data |  Major | . | Brandon Williams | Tyler Hobbs |
| [CASSANDRA-7219](https://issues.apache.org/jira/browse/CASSANDRA-7219) | Hint streaming can cause decommission to fail |  Major | . | Jeremiah Jordan | Jason Brown |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7164](https://issues.apache.org/jira/browse/CASSANDRA-7164) | o.a.c.service.RemoveTest unit test failing in 2.1 |  Minor | Testing | Michael Shuler | Joshua McKenzie |
| [CASSANDRA-7175](https://issues.apache.org/jira/browse/CASSANDRA-7175) | Dtests should have an option for offheap memtables |  Major | . | Ryan McGuire | Ryan McGuire |
| [CASSANDRA-7161](https://issues.apache.org/jira/browse/CASSANDRA-7161) | o.a.c.db.ColumnFamilyTest.testDigest failing in 2.1 |  Minor | . | Michael Shuler | Jonathan Ellis |
| [CASSANDRA-7165](https://issues.apache.org/jira/browse/CASSANDRA-7165) | o.a.c.streaming.StreamingTransferTest unit test failing in 2.1 |  Minor | Testing | Michael Shuler | Yuki Morishita |
| [CASSANDRA-7162](https://issues.apache.org/jira/browse/CASSANDRA-7162) | o.a.c.db.marshal.CollectionTypeTest unit test failing in 2.1 |  Minor | Testing | Michael Shuler | Sylvain Lebresne |
| [CASSANDRA-7036](https://issues.apache.org/jira/browse/CASSANDRA-7036) | counter\_tests.py:TestCounters.upgrade\_test dtest hangs in 1.2, 2.0, 2.1 |  Major | Testing | Michael Shuler | Michael Shuler |
| [CASSANDRA-7167](https://issues.apache.org/jira/browse/CASSANDRA-7167) | o.a.c.db.KeyCacheTest unit test failing intermittently in 2.1 |  Minor | Testing | Michael Shuler | Benedict |
| [CASSANDRA-7199](https://issues.apache.org/jira/browse/CASSANDRA-7199) | [dtest] snapshot\_test hung on 2.1 |  Minor | Testing | Michael Shuler | Benedict |
| [CASSANDRA-7013](https://issues.apache.org/jira/browse/CASSANDRA-7013) | sstable\_generation\_loading\_test sstableloader\_compression\_\* dtests fail in 1.2, 2.0, and 2.1 |  Major | Testing | Michael Shuler | Marcus Eriksson |
| [CASSANDRA-7141](https://issues.apache.org/jira/browse/CASSANDRA-7141) | Expand secondary\_indexes\_test for secondary indexes on sets and maps |  Minor | Secondary Indexes | Shawn Kumar | Shawn Kumar |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7251](https://issues.apache.org/jira/browse/CASSANDRA-7251) | testDiskFailurePolicy\_best\_effort assertion error |  Minor | Testing | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-7261](https://issues.apache.org/jira/browse/CASSANDRA-7261) | cqlsh support for new UDT format |  Major | . | Mikhail Stepura | Mikhail Stepura |
| [CASSANDRA-7289](https://issues.apache.org/jira/browse/CASSANDRA-7289) | cqlsh support for null values in UDT |  Major | Tools | Mikhail Stepura | Mikhail Stepura |
| [CASSANDRA-7265](https://issues.apache.org/jira/browse/CASSANDRA-7265) | DataOutputTest unit test failure on Windows |  Minor | . | Joshua McKenzie | Ala' Alkhaldi |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7181](https://issues.apache.org/jira/browse/CASSANDRA-7181) | Remove unused method isLocalTask() in o.a.c.repair.StreamingRepairTask |  Trivial | . | Lyuben Todorov | Lyuben Todorov |
| [CASSANDRA-7123](https://issues.apache.org/jira/browse/CASSANDRA-7123) | BATCH documentation should be explicit about ordering guarantees |  Minor | Documentation and Website | Tyler Hobbs | Tyler Hobbs |


