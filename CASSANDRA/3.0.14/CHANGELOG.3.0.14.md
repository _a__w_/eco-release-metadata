
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.0.14 - 2017-06-23



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13518](https://issues.apache.org/jira/browse/CASSANDRA-13518) | sstableloader doesn't support non default storage\_port and ssl\_storage\_port. |  Minor | Tools | Zhiyan Shao | Zhiyan Shao |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12728](https://issues.apache.org/jira/browse/CASSANDRA-12728) | Handling partially written hint files |  Major | Hints | Sharvanath Pathak | Garvit Juniwal |
| [CASSANDRA-13308](https://issues.apache.org/jira/browse/CASSANDRA-13308) | Gossip breaks, Hint files not being deleted on nodetool decommission |  Major | Hints, Streaming and Messaging | Arijit Banerjee | Jeff Jirsa |
| [CASSANDRA-13276](https://issues.apache.org/jira/browse/CASSANDRA-13276) | Regression on CASSANDRA-11416: can't load snapshots of tables with dropped columns |  Major | . | Matt Kopit | Andrés de la Peña |
| [CASSANDRA-13397](https://issues.apache.org/jira/browse/CASSANDRA-13397) | Return value of CountDownLatch.await() not being checked in Repair |  Minor | . | Simon Zhou | Simon Zhou |
| [CASSANDRA-13441](https://issues.apache.org/jira/browse/CASSANDRA-13441) | Schema version changes for each upgraded node in a rolling upgrade, causing migration storms |  Major | Distributed Metadata | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-12841](https://issues.apache.org/jira/browse/CASSANDRA-12841) | testall failure in org.apache.cassandra.db.compaction.NeverPurgeTest.minorNeverPurgeTombstonesTest-compression |  Major | . | Sean McCarthy | Marcus Eriksson |
| [CASSANDRA-13265](https://issues.apache.org/jira/browse/CASSANDRA-13265) | Expiration in OutboundTcpConnection can block the reader Thread |  Major | . | Christian Esken | Christian Esken |
| [CASSANDRA-13163](https://issues.apache.org/jira/browse/CASSANDRA-13163) | NPE in StorageService.excise |  Major | Core | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-13236](https://issues.apache.org/jira/browse/CASSANDRA-13236) | corrupt flag error after upgrade from 2.2 to 3.0.10 |  Critical | . | ingard mevåg | Sam Tunnicliffe |
| [CASSANDRA-12847](https://issues.apache.org/jira/browse/CASSANDRA-12847) | cqlsh DESCRIBE output doesn't properly quote index names |  Major | . | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-13052](https://issues.apache.org/jira/browse/CASSANDRA-13052) | Repair process is violating the start/end token limits for small ranges |  Major | Streaming and Messaging | Cristian P | Stefan Podkowinski |
| [CASSANDRA-13525](https://issues.apache.org/jira/browse/CASSANDRA-13525) | ReverseIndexedReader may drop rows during 2.1 to 3.0 upgrade |  Critical | Local Write-Read Paths | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-13533](https://issues.apache.org/jira/browse/CASSANDRA-13533) | ColumnIdentifier object size wrong when tables are not flushed |  Major | Core | Eduard Tudenhoefner | Eduard Tudenhoefner |
| [CASSANDRA-13559](https://issues.apache.org/jira/browse/CASSANDRA-13559) | Schema version id mismatch while upgrading to 3.0.13 |  Blocker | Core | Jay Zhuang | Jay Zhuang |
| [CASSANDRA-13120](https://issues.apache.org/jira/browse/CASSANDRA-13120) | Trace and Histogram output misleading |  Minor | Core | Adam Hattrell | Benjamin Lerer |
| [CASSANDRA-13542](https://issues.apache.org/jira/browse/CASSANDRA-13542) | nodetool scrub/cleanup/upgradesstables exit code |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-13209](https://issues.apache.org/jira/browse/CASSANDRA-13209) | test failure in cqlsh\_tests.cqlsh\_copy\_tests.CqlshCopyTest.test\_bulk\_round\_trip\_blogposts\_with\_max\_connections |  Major | . | Michael Shuler | Kurt Greaves |
| [CASSANDRA-13346](https://issues.apache.org/jira/browse/CASSANDRA-13346) | Failed unregistering mbean during drop keyspace |  Minor | Materialized Views | Gábor Auth | Lerh Chuan Low |
| [CASSANDRA-11381](https://issues.apache.org/jira/browse/CASSANDRA-11381) | Node running with join\_ring=false and authentication can not serve requests |  Major | . | mck | mck |
| [CASSANDRA-13004](https://issues.apache.org/jira/browse/CASSANDRA-13004) | Corruption while adding/removing a column to/from the table |  Blocker | Distributed Metadata | Stanislav Vishnevskiy | Alex Petrov |
| [CASSANDRA-13172](https://issues.apache.org/jira/browse/CASSANDRA-13172) | compaction\_large\_partition\_warning\_threshold\_mb not working properly when set to high value |  Minor | Configuration | Vladimir Vavro | Kurt Greaves |
| [CASSANDRA-13562](https://issues.apache.org/jira/browse/CASSANDRA-13562) | Cassandra removenode makes Gossiper Thread hang forever |  Major | Core | Jaydeepkumar Chovatia |  |
| [CASSANDRA-13072](https://issues.apache.org/jira/browse/CASSANDRA-13072) | Cassandra failed to run on Linux-aarch64 |  Major | Core | Jun He | Benjamin Lerer |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13434](https://issues.apache.org/jira/browse/CASSANDRA-13434) | Add PID file directive in /etc/init.d/cassandra |  Major | Packaging | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-13435](https://issues.apache.org/jira/browse/CASSANDRA-13435) | Incorrect status check use when stopping Cassandra |  Major | Packaging | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-13490](https://issues.apache.org/jira/browse/CASSANDRA-13490) | RPM Spec - disable binary check, improve readme instructions |  Major | Packaging | martin a langhoff | Stefan Podkowinski |
| [CASSANDRA-13493](https://issues.apache.org/jira/browse/CASSANDRA-13493) | RPM Init: Service startup ordering |  Major | Packaging | martin a langhoff | Stefan Podkowinski |
| [CASSANDRA-13440](https://issues.apache.org/jira/browse/CASSANDRA-13440) | Sign RPM artifacts |  Major | Packaging | Stefan Podkowinski | Michael Shuler |


