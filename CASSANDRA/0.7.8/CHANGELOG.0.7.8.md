
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.7.8 - 2011-07-22



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2770](https://issues.apache.org/jira/browse/CASSANDRA-2770) | Expose data\_dir though jmx |  Trivial | . | Mike Bulman | Mike Bulman |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2856](https://issues.apache.org/jira/browse/CASSANDRA-2856) | log unavailableexception details at debug level |  Trivial | . | Jonathan Ellis | Jonathan Ellis |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2870](https://issues.apache.org/jira/browse/CASSANDRA-2870) | dynamic snitch + read repair off can cause LOCAL\_QUORUM reads to return spurious UnavailableException |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2383](https://issues.apache.org/jira/browse/CASSANDRA-2383) | log4j unable to load properties file from classpath |  Minor | Tools | david lee | David Allsopp |
| [CASSANDRA-2740](https://issues.apache.org/jira/browse/CASSANDRA-2740) | nodetool decommission should throw an error when there are extra params |  Trivial | . | Brandon Williams | Jon Hermes |
| [CASSANDRA-2899](https://issues.apache.org/jira/browse/CASSANDRA-2899) | cli silently fails when classes are quoted |  Minor | . | Brandon Williams | Pavel Yaskevich |
| [CASSANDRA-2821](https://issues.apache.org/jira/browse/CASSANDRA-2821) | CLI remove ascii column |  Minor | Tools | Sasha Dolgy | Pavel Yaskevich |
| [CASSANDRA-2809](https://issues.apache.org/jira/browse/CASSANDRA-2809) | In the Cli, update column family \<cf\> with comparator; create Column metadata |  Minor | Tools | Silvère Lestang | Pavel Yaskevich |
| [CASSANDRA-2872](https://issues.apache.org/jira/browse/CASSANDRA-2872) | While dropping and recreating an index, incremental snapshotting can hang |  Minor | . | Sylvain Lebresne | Jonathan Ellis |
| [CASSANDRA-2928](https://issues.apache.org/jira/browse/CASSANDRA-2928) | Fix Hinted Handoff replay |  Major | . | Jonathan Ellis | Jonathan Ellis |


