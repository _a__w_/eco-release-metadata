
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.2.0 beta 1 - 2012-09-23



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3936](https://issues.apache.org/jira/browse/CASSANDRA-3936) | Gossip should have a 'goodbye' command to indicate shutdown |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-2819](https://issues.apache.org/jira/browse/CASSANDRA-2819) | Split rpc timeout for read and write ops |  Major | . | Stu Hood | Jonathan Ellis |
| [CASSANDRA-2478](https://issues.apache.org/jira/browse/CASSANDRA-2478) | Custom CQL protocol/transport |  Minor | CQL | Eric Evans | Sylvain Lebresne |
| [CASSANDRA-3647](https://issues.apache.org/jira/browse/CASSANDRA-3647) | Support collection (list, set, and map) value types in CQL |  Major | CQL | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-3772](https://issues.apache.org/jira/browse/CASSANDRA-3772) | Evaluate Murmur3-based partitioner |  Major | . | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1123](https://issues.apache.org/jira/browse/CASSANDRA-1123) | Allow tracing query details |  Major | . | Jonathan Ellis | David Alves |
| [CASSANDRA-4448](https://issues.apache.org/jira/browse/CASSANDRA-4448) | CQL3: allow to define a per-cf default consistency level |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4611](https://issues.apache.org/jira/browse/CASSANDRA-4611) | Add AlterKeyspace statement |  Minor | CQL | Sylvain Lebresne | Sylvain Lebresne |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3836](https://issues.apache.org/jira/browse/CASSANDRA-3836) | [patch] reduce workload for converting super columns to tuples |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-3442](https://issues.apache.org/jira/browse/CASSANDRA-3442) | TTL histogram for sstable metadata |  Minor | . | Jonathan Ellis | Yuki Morishita |
| [CASSANDRA-3910](https://issues.apache.org/jira/browse/CASSANDRA-3910) | make phi\_convict\_threshold Float |  Major | . | Radim Kolar | Harish Doddi |
| [CASSANDRA-3555](https://issues.apache.org/jira/browse/CASSANDRA-3555) | Bootstrapping to handle more failure |  Major | . | Vijay | Vijay |
| [CASSANDRA-2319](https://issues.apache.org/jira/browse/CASSANDRA-2319) | Promote row index |  Major | . | Stu Hood | Sylvain Lebresne |
| [CASSANDRA-4057](https://issues.apache.org/jira/browse/CASSANDRA-4057) | [patch] use double math for double results |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-2975](https://issues.apache.org/jira/browse/CASSANDRA-2975) | Upgrade MurmurHash to version 3 |  Trivial | . | Brian Lindauer | Vijay |
| [CASSANDRA-4060](https://issues.apache.org/jira/browse/CASSANDRA-4060) | Track time elapsed for deletes in cassandra-cli |  Minor | Tools | Radim Kolar | Radim Kolar |
| [CASSANDRA-4059](https://issues.apache.org/jira/browse/CASSANDRA-4059) | no need to fill a newly created array with 0s |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-4062](https://issues.apache.org/jira/browse/CASSANDRA-4062) | minor removal of deadcode and redundancy in hashing fn |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-4082](https://issues.apache.org/jira/browse/CASSANDRA-4082) | minor reduction in serializer fetching |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-3722](https://issues.apache.org/jira/browse/CASSANDRA-3722) | Send Hints to Dynamic Snitch when Compaction or repair is going on for a node. |  Minor | . | Vijay | Vijay |
| [CASSANDRA-4132](https://issues.apache.org/jira/browse/CASSANDRA-4132) | [patch] fix cli delete key bug by upgrading to jline 1.0 |  Trivial | Tools | Dave Brosius | Dave Brosius |
| [CASSANDRA-2635](https://issues.apache.org/jira/browse/CASSANDRA-2635) | make cache skipping optional |  Minor | . | Peter Schuller | Harish Doddi |
| [CASSANDRA-2392](https://issues.apache.org/jira/browse/CASSANDRA-2392) | Saving IndexSummaries to disk |  Minor | . | Chris Goffinet | Vijay |
| [CASSANDRA-3617](https://issues.apache.org/jira/browse/CASSANDRA-3617) | Clean up and optimize Message |  Major | . | Jonathan Ellis | Yuki Morishita |
| [CASSANDRA-4226](https://issues.apache.org/jira/browse/CASSANDRA-4226) | make sure MessageIn skips the entire payload size |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-4215](https://issues.apache.org/jira/browse/CASSANDRA-4215) | clarify distinction between in-memory size() method and on-disk serializedSize() for CF, Column classes |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4260](https://issues.apache.org/jira/browse/CASSANDRA-4260) | use platform agnostic new lines in printfs |  Trivial | Tools | Dave Brosius |  |
| [CASSANDRA-4247](https://issues.apache.org/jira/browse/CASSANDRA-4247) | More-efficient serialization for IndexScanCommand and RangeSliceCommand |  Minor | . | Jonathan Ellis | Yuki Morishita |
| [CASSANDRA-3794](https://issues.apache.org/jira/browse/CASSANDRA-3794) | Avoid ID conflicts from concurrent schema changes |  Major | . | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-4293](https://issues.apache.org/jira/browse/CASSANDRA-4293) | Stop serializing twice the cf id in RowMutation |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3762](https://issues.apache.org/jira/browse/CASSANDRA-3762) | AutoSaving KeyCache and System load time improvements. |  Minor | . | Vijay | Vijay |
| [CASSANDRA-3127](https://issues.apache.org/jira/browse/CASSANDRA-3127) | Message (inter-node) compression |  Minor | . | Sylvain Lebresne | Marcus Eriksson |
| [CASSANDRA-4308](https://issues.apache.org/jira/browse/CASSANDRA-4308) | Promote the use of IFilter for internal commands |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4297](https://issues.apache.org/jira/browse/CASSANDRA-4297) | Use java NIO as much as possible when streaming compressed SSTables |  Minor | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-4189](https://issues.apache.org/jira/browse/CASSANDRA-4189) | Improve hints replay |  Minor | . | Vijay | Vijay |
| [CASSANDRA-4315](https://issues.apache.org/jira/browse/CASSANDRA-4315) | Use EntryWeigher instead of Weigher to Measuring the cache. |  Minor | . | Vijay | Vijay |
| [CASSANDRA-4018](https://issues.apache.org/jira/browse/CASSANDRA-4018) | Add column metadata to system columnfamilies |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2479](https://issues.apache.org/jira/browse/CASSANDRA-2479) | remove CollatingOrderPreservingPartitioner |  Trivial | . | Eric Evans | Jonathan Ellis |
| [CASSANDRA-4357](https://issues.apache.org/jira/browse/CASSANDRA-4357) | Add Commitlog Versioning |  Minor | . | Vijay | Vijay |
| [CASSANDRA-4393](https://issues.apache.org/jira/browse/CASSANDRA-4393) | Reduce default BF size |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4329](https://issues.apache.org/jira/browse/CASSANDRA-4329) | CQL3: Always use composite types by default |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4433](https://issues.apache.org/jira/browse/CASSANDRA-4433) | remove redundant "name" column from schema\_keyspaces |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4414](https://issues.apache.org/jira/browse/CASSANDRA-4414) | Ship the exact cause for timeout and unavailable exception back to the client |  Major | . | Sylvain Lebresne | Jonathan Ellis |
| [CASSANDRA-4234](https://issues.apache.org/jira/browse/CASSANDRA-4234) | Add tombstone-removal compaction to LCS |  Minor | . | Jonathan Ellis | Yuki Morishita |
| [CASSANDRA-4361](https://issues.apache.org/jira/browse/CASSANDRA-4361) | CQL3: allow definition with only a PK |  Major | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4038](https://issues.apache.org/jira/browse/CASSANDRA-4038) | Investigate improving the dynamic snitch with reservoir sampling |  Major | . | Brandon Williams | Pavel Yaskevich |
| [CASSANDRA-2116](https://issues.apache.org/jira/browse/CASSANDRA-2116) | Separate out filesystem errors from generic IOErrors |  Major | . | Chris Goffinet | Aleksey Yeschenko |
| [CASSANDRA-4473](https://issues.apache.org/jira/browse/CASSANDRA-4473) | Binary protocol: handle asynchronous execution (better) |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4485](https://issues.apache.org/jira/browse/CASSANDRA-4485) | cqlsh: support collections |  Minor | . | Sylvain Lebresne | paul cannon |
| [CASSANDRA-4453](https://issues.apache.org/jira/browse/CASSANDRA-4453) | Better support of collections in the binary protocol |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4282](https://issues.apache.org/jira/browse/CASSANDRA-4282) | Improve startup time by making row cache population multi threaded |  Minor | . | Marcus Eriksson | Jonathan Ellis |
| [CASSANDRA-4292](https://issues.apache.org/jira/browse/CASSANDRA-4292) | Improve JBOD loadbalancing and reduce contention |  Major | . | Jonathan Ellis | Yuki Morishita |
| [CASSANDRA-2897](https://issues.apache.org/jira/browse/CASSANDRA-2897) | Secondary indexes without read-before-write |  Minor | Secondary Indexes | Sylvain Lebresne | Sam Tunnicliffe |
| [CASSANDRA-4548](https://issues.apache.org/jira/browse/CASSANDRA-4548) | Mutation response(WriteResponse.java) could be smaller and not contain keyspace and key |  Minor | . | sankalp kohli | Jonathan Ellis |
| [CASSANDRA-4500](https://issues.apache.org/jira/browse/CASSANDRA-4500) | cqlsh: understand updated encodings for collections |  Major | Tools | paul cannon | paul cannon |
| [CASSANDRA-4539](https://issues.apache.org/jira/browse/CASSANDRA-4539) | potential backwards incompatibility in native protocol |  Minor | CQL | paul cannon | Sylvain Lebresne |
| [CASSANDRA-4480](https://issues.apache.org/jira/browse/CASSANDRA-4480) | Binary protocol: adds events push |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4632](https://issues.apache.org/jira/browse/CASSANDRA-4632) | Increase default max\_hint\_window\_in\_ms to 3h |  Trivial | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4541](https://issues.apache.org/jira/browse/CASSANDRA-4541) | Replace Throttle with Guava's RateLimiter |  Minor | . | Michaël Figuière | Aleksey Yeschenko |
| [CASSANDRA-4647](https://issues.apache.org/jira/browse/CASSANDRA-4647) | Rename NodeId to CounterId |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3515](https://issues.apache.org/jira/browse/CASSANDRA-3515) | track tombstones skipped on read path |  Minor | . | Jackson Chung | Jonathan Ellis |
| [CASSANDRA-2858](https://issues.apache.org/jira/browse/CASSANDRA-2858) | make request dropping more accurate |  Minor | . | Ryan King | Jonathan Ellis |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3835](https://issues.apache.org/jira/browse/CASSANDRA-3835) | FB.broadcastAddress fixes and Soft reset on Ec2MultiRegionSnitch.reconnect |  Minor | . | Vijay | Vijay |
| [CASSANDRA-4022](https://issues.apache.org/jira/browse/CASSANDRA-4022) | Compaction of hints can get stuck in a loop |  Critical | . | Brandon Williams | Yuki Morishita |
| [CASSANDRA-4100](https://issues.apache.org/jira/browse/CASSANDRA-4100) | Make scrub and cleanup operations throttled |  Minor | . | Vijay | Vijay |
| [CASSANDRA-4141](https://issues.apache.org/jira/browse/CASSANDRA-4141) | Looks like Serializing cache broken in 1.1 |  Major | . | Vijay | Vijay |
| [CASSANDRA-4203](https://issues.apache.org/jira/browse/CASSANDRA-4203) | Fix descriptor versioning for bloom filter changes |  Major | . | Jonathan Ellis | Vijay |
| [CASSANDRA-4213](https://issues.apache.org/jira/browse/CASSANDRA-4213) | DynamicEndpointSnitch calculates score incorrectly |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-4227](https://issues.apache.org/jira/browse/CASSANDRA-4227) | StorageProxy throws NPEs for when there's no hostids for a target |  Trivial | . | Dave Brosius | Eric Evans |
| [CASSANDRA-4277](https://issues.apache.org/jira/browse/CASSANDRA-4277) | hsha default thread limits make no sense, and yaml comments look confused |  Major | . | Peter Schuller | Peter Schuller |
| [CASSANDRA-4302](https://issues.apache.org/jira/browse/CASSANDRA-4302) | cassandra-stress scripts should be executable |  Trivial | Tools | Tyler Hobbs | Yuki Morishita |
| [CASSANDRA-4289](https://issues.apache.org/jira/browse/CASSANDRA-4289) | Secondary Indexes fail following a system restart |  Major | Secondary Indexes | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-4313](https://issues.apache.org/jira/browse/CASSANDRA-4313) | CFS always try to load key cache |  Trivial | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-4312](https://issues.apache.org/jira/browse/CASSANDRA-4312) | fix OOM with ReadMessageTest.testNoCommitLog |  Trivial | Testing | Dave Brosius | Dave Brosius |
| [CASSANDRA-4311](https://issues.apache.org/jira/browse/CASSANDRA-4311) | clean up messagingservice protocol limitations |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4320](https://issues.apache.org/jira/browse/CASSANDRA-4320) | Assertion error while delivering the hints. |  Minor | . | Vijay | Vijay |
| [CASSANDRA-4317](https://issues.apache.org/jira/browse/CASSANDRA-4317) | AssertionError in handleStateNormal in a mixed cluster |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-4360](https://issues.apache.org/jira/browse/CASSANDRA-4360) | Fix broken streaming after CASSANDRA-4311 |  Minor | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-4395](https://issues.apache.org/jira/browse/CASSANDRA-4395) | SSTableNamesIterator misses some tombstones |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4364](https://issues.apache.org/jira/browse/CASSANDRA-4364) | Compaction invalidates row cache |  Minor | . | Marcus Eriksson | Jonathan Ellis |
| [CASSANDRA-3881](https://issues.apache.org/jira/browse/CASSANDRA-3881) | reduce computational complexity of processing topology changes |  Major | . | Peter Schuller | Sam Overton |
| [CASSANDRA-4404](https://issues.apache.org/jira/browse/CASSANDRA-4404) | tombstone estimation needs to avoid using global partitioner against index sstables |  Major | . | Jonathan Ellis | Yuki Morishita |
| [CASSANDRA-4403](https://issues.apache.org/jira/browse/CASSANDRA-4403) | cleanup uses global partitioner to estimate ranges in index sstables |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4409](https://issues.apache.org/jira/browse/CASSANDRA-4409) | Only consider whole row tombstone in collation controller |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4054](https://issues.apache.org/jira/browse/CASSANDRA-4054) | SStableImport and SStableExport does not serialize row level deletion |  Minor | Tools | Zhu Han | David Alves |
| [CASSANDRA-4441](https://issues.apache.org/jira/browse/CASSANDRA-4441) | Fix validation of dates |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4435](https://issues.apache.org/jira/browse/CASSANDRA-4435) | hints compaction loop over same sstable |  Major | . | Brandon Williams | Yuki Morishita |
| [CASSANDRA-3855](https://issues.apache.org/jira/browse/CASSANDRA-3855) | RemoveDeleted dominates compaction time for large sstable counts |  Major | . | Stu Hood | Yuki Morishita |
| [CASSANDRA-4475](https://issues.apache.org/jira/browse/CASSANDRA-4475) | Using 'key' as primary key throws exception when using CQL2 |  Trivial | . | Yuki Morishita | Sylvain Lebresne |
| [CASSANDRA-4477](https://issues.apache.org/jira/browse/CASSANDRA-4477) | cql3: defining more than one pk should be invalid |  Major | . | Brandon Williams | Sylvain Lebresne |
| [CASSANDRA-3687](https://issues.apache.org/jira/browse/CASSANDRA-3687) | Local range scans are not run on the read stage |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4546](https://issues.apache.org/jira/browse/CASSANDRA-4546) | cqlsh: handle when full cassandra type class names are given |  Major | Tools | paul cannon | paul cannon |
| [CASSANDRA-4487](https://issues.apache.org/jira/browse/CASSANDRA-4487) | remove uses of SchemaDisagreementException |  Minor | CQL | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-4553](https://issues.apache.org/jira/browse/CASSANDRA-4553) | NPE while loading Saved KeyCache |  Minor | . | Vijay | Vijay |
| [CASSANDRA-4555](https://issues.apache.org/jira/browse/CASSANDRA-4555) | select statement with indexed column causes node to OOM |  Major | . | David Alves | Jonathan Ellis |
| [CASSANDRA-4479](https://issues.apache.org/jira/browse/CASSANDRA-4479) | JMX attribute setters not consistent with cassandra.yaml |  Trivial | . | Eric Dong | Chris Merrill |
| [CASSANDRA-4567](https://issues.apache.org/jira/browse/CASSANDRA-4567) | Error in log related to Murmur3Partitioner |  Major | . | Tyler Patterson | Vijay |
| [CASSANDRA-4599](https://issues.apache.org/jira/browse/CASSANDRA-4599) | Event tracing always records thread name as 'TracingStage' |  Trivial | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-4497](https://issues.apache.org/jira/browse/CASSANDRA-4497) | Update CQL pseudo-maps to real map syntax |  Major | . | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-4577](https://issues.apache.org/jira/browse/CASSANDRA-4577) | Allow preparing queries without parameters |  Major | CQL | Christoph Hack | Christoph Hack |
| [CASSANDRA-4624](https://issues.apache.org/jira/browse/CASSANDRA-4624) | ORDER BY validation is not restrictive enough |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4493](https://issues.apache.org/jira/browse/CASSANDRA-4493) | Fix update of CF comparator (including adding new collections) |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4628](https://issues.apache.org/jira/browse/CASSANDRA-4628) | CQL/JDBC: date vs. timestamp issues |  Minor | . | Michael Krumpholz | Michael Krumpholz |
| [CASSANDRA-4640](https://issues.apache.org/jira/browse/CASSANDRA-4640) | Make CQL3 the default |  Minor | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4377](https://issues.apache.org/jira/browse/CASSANDRA-4377) | CQL3 column value validation bug |  Major | CQL | Nick Bailey | Pavel Yaskevich |
| [CASSANDRA-4537](https://issues.apache.org/jira/browse/CASSANDRA-4537) | We should emit number of sstables in each level from JMX |  Minor | . | sankalp kohli | Yuki Morishita |
| [CASSANDRA-4659](https://issues.apache.org/jira/browse/CASSANDRA-4659) | Fix consistency ALL parsing in CQL3 |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4627](https://issues.apache.org/jira/browse/CASSANDRA-4627) | support inet data type |  Major | Tools | paul cannon | paul cannon |
| [CASSANDRA-4579](https://issues.apache.org/jira/browse/CASSANDRA-4579) | CQL queries using LIMIT sometimes missing results |  Major | . | paul cannon | Sylvain Lebresne |
| [CASSANDRA-4491](https://issues.apache.org/jira/browse/CASSANDRA-4491) | cqlsh needs to use system.local instead of system.Versions |  Minor | Tools | paul cannon | paul cannon |
| [CASSANDRA-4621](https://issues.apache.org/jira/browse/CASSANDRA-4621) | AssertionError in StorageProxy.getRestrictedRange |  Major | . | Pierre-Yves Ritschard | Sylvain Lebresne |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4138](https://issues.apache.org/jira/browse/CASSANDRA-4138) | Add varint encoding to Serializing Cache |  Minor | . | Vijay | Vijay |
| [CASSANDRA-4120](https://issues.apache.org/jira/browse/CASSANDRA-4120) | Gossip identifies hosts by UUID |  Major | . | Sam Overton | Eric Evans |
| [CASSANDRA-3885](https://issues.apache.org/jira/browse/CASSANDRA-3885) | Support multiple ranges in SliceQueryFilter |  Major | . | Jonathan Ellis | David Alves |
| [CASSANDRA-3708](https://issues.apache.org/jira/browse/CASSANDRA-3708) | Support "composite prefix" tombstones |  Major | . | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-4121](https://issues.apache.org/jira/browse/CASSANDRA-4121) | TokenMetadata supports multiple tokens per host |  Major | . | Sam Overton | Sam Overton |
| [CASSANDRA-4122](https://issues.apache.org/jira/browse/CASSANDRA-4122) | Bootstrap and decommission with multiple ranges for vnodes |  Major | . | Sam Overton | Sam Overton |
| [CASSANDRA-4125](https://issues.apache.org/jira/browse/CASSANDRA-4125) | Update nodetool for vnodes |  Major | . | Sam Overton | Eric Evans |
| [CASSANDRA-4127](https://issues.apache.org/jira/browse/CASSANDRA-4127) | migration support for vnodes |  Major | . | Sam Overton | Sam Overton |
| [CASSANDRA-4179](https://issues.apache.org/jira/browse/CASSANDRA-4179) | Add more general support for composites (to row key, column value) |  Minor | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3680](https://issues.apache.org/jira/browse/CASSANDRA-3680) | Add Support for Composite Secondary Indexes |  Major | Secondary Indexes | T Jake Luciani | Sylvain Lebresne |
| [CASSANDRA-2118](https://issues.apache.org/jira/browse/CASSANDRA-2118) | Provide failure modes if issues with the underlying filesystem of a node |  Major | . | Chris Goffinet | Aleksey Yeschenko |
| [CASSANDRA-4383](https://issues.apache.org/jira/browse/CASSANDRA-4383) | Binary encoding of vnode tokens |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3979](https://issues.apache.org/jira/browse/CASSANDRA-3979) | Consider providing error code with exceptions (and documenting them) |  Major | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4542](https://issues.apache.org/jira/browse/CASSANDRA-4542) | add atomic\_batch\_mutate method |  Major | CQL | Jonathan Ellis | Aleksey Yeschenko |
| [CASSANDRA-4635](https://issues.apache.org/jira/browse/CASSANDRA-4635) | BatchlogManagerMBean |  Minor | . | Jonathan Ellis | Aleksey Yeschenko |
| [CASSANDRA-4559](https://issues.apache.org/jira/browse/CASSANDRA-4559) | implement token relocation |  Major | Tools | Eric Evans | Eric Evans |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4413](https://issues.apache.org/jira/browse/CASSANDRA-4413) | upgrade guava to 12.0 |  Trivial | . | David Alves | David Alves |


