
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.2.9 - 2017-02-21



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12967](https://issues.apache.org/jira/browse/CASSANDRA-12967) | Spec for Cassandra RPM Build |  Major | Packaging | Bhuvan Rawal | Michael Shuler |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12883](https://issues.apache.org/jira/browse/CASSANDRA-12883) | Remove support for non-JavaScript UDFs |  Minor | CQL | Robert Stupp | Robert Stupp |
| [CASSANDRA-12981](https://issues.apache.org/jira/browse/CASSANDRA-12981) | Refactor ColumnCondition |  Major | CQL | Benjamin Lerer | Benjamin Lerer |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12618](https://issues.apache.org/jira/browse/CASSANDRA-12618) | Out of memory bug with one insert |  Critical | . | Eduardo Alonso de Blas | Benjamin Lerer |
| [CASSANDRA-12580](https://issues.apache.org/jira/browse/CASSANDRA-12580) | Fix merkle tree size calculation |  Major | Streaming and Messaging | Paulo Motta | Paulo Motta |
| [CASSANDRA-12457](https://issues.apache.org/jira/browse/CASSANDRA-12457) | dtest failure in upgrade\_tests.cql\_tests.TestCQLNodes2RF1\_Upgrade\_current\_2\_1\_x\_To\_indev\_2\_2\_x.bug\_5732\_test |  Major | Lifecycle | Craig Kodman | Stefania |
| [CASSANDRA-12700](https://issues.apache.org/jira/browse/CASSANDRA-12700) | During writing data into Cassandra 3.7.0 using Python driver 3.7 sometimes Connection get lost, because of Server NullPointerException |  Major | Core | Rajesh Radhakrishnan | Jeff Jirsa |
| [CASSANDRA-11117](https://issues.apache.org/jira/browse/CASSANDRA-11117) | ColUpdateTimeDeltaHistogram histogram overflow |  Minor | Observability | Chris Lohfink | Joel Knighton |
| [CASSANDRA-12720](https://issues.apache.org/jira/browse/CASSANDRA-12720) | Permissions on aggregate functions are not removed on drop |  Major | Distributed Metadata | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-12765](https://issues.apache.org/jira/browse/CASSANDRA-12765) | SSTable ignored incorrectly with partition level tombstone |  Major | Local Write-Read Paths | Cameron Zemek | Cameron Zemek |
| [CASSANDRA-12786](https://issues.apache.org/jira/browse/CASSANDRA-12786) | Fix a bug in CASSANDRA-11005(Split consisten range movement flag) |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-12754](https://issues.apache.org/jira/browse/CASSANDRA-12754) | Change cassandra.wait\_for\_tracing\_events\_timeout\_secs default to -1 so C\* doesn't wait on trace events to be written before responding to request by default |  Major | Observability | Andy Tolbert | Stefania |
| [CASSANDRA-12462](https://issues.apache.org/jira/browse/CASSANDRA-12462) | NullPointerException in CompactionInfo.getId(CompactionInfo.java:65) |  Major | Compaction | Jonathan DePrizio | Simon Zhou |
| [CASSANDRA-12808](https://issues.apache.org/jira/browse/CASSANDRA-12808) | testall failure inorg.apache.cassandra.io.sstable.IndexSummaryManagerTest.testCancelIndex |  Major | . | Sean McCarthy | Sam Tunnicliffe |
| [CASSANDRA-12863](https://issues.apache.org/jira/browse/CASSANDRA-12863) | cqlsh COPY FROM cannot parse timestamp in partition key if table contains a counter value |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-12813](https://issues.apache.org/jira/browse/CASSANDRA-12813) | NPE in auth for bootstrapping node |  Major | . | Charles Mims | Alex Petrov |
| [CASSANDRA-12531](https://issues.apache.org/jira/browse/CASSANDRA-12531) | dtest failure in read\_failures\_test.TestReadFailures.test\_tombstone\_failure\_v3 |  Major | . | Craig Kodman | Sam Tunnicliffe |
| [CASSANDRA-12901](https://issues.apache.org/jira/browse/CASSANDRA-12901) | Repair can hang if node dies during sync or anticompaction |  Major | Streaming and Messaging | Paulo Motta | Paulo Motta |
| [CASSANDRA-12792](https://issues.apache.org/jira/browse/CASSANDRA-12792) | delete with timestamp long.MAX\_VALUE for the whole key creates tombstone that cannot be removed. |  Major | Compaction | Ian Ilsley | Joel Knighton |
| [CASSANDRA-12281](https://issues.apache.org/jira/browse/CASSANDRA-12281) | Gossip blocks on startup when there are pending range movements |  Major | Core | Eric Evans | Stefan Podkowinski |
| [CASSANDRA-12899](https://issues.apache.org/jira/browse/CASSANDRA-12899) | stand alone sstableupgrade prints LEAK DETECTED warnings some times |  Major | . | Jeremiah Jordan | Yuki Morishita |
| [CASSANDRA-12914](https://issues.apache.org/jira/browse/CASSANDRA-12914) | cqlsh describe fails with "list[i] not a string for i in..." |  Minor | Tools | Adam Holmberg | Adam Holmberg |
| [CASSANDRA-12935](https://issues.apache.org/jira/browse/CASSANDRA-12935) | Use saved tokens when setting local tokens on StorageService.joinRing() |  Minor | Coordination | Paulo Motta | Paulo Motta |
| [CASSANDRA-12817](https://issues.apache.org/jira/browse/CASSANDRA-12817) | testall failure in org.apache.cassandra.cql3.validation.entities.UFTest.testAllNativeTypes |  Major | . | Sean McCarthy | Robert Stupp |
| [CASSANDRA-12673](https://issues.apache.org/jira/browse/CASSANDRA-12673) | Nodes cannot see each other in multi-DC, non-EC2 environment with two-interface nodes due to outbound node-to-node connection binding to private interface |  Minor | Core | Milan Majercik | Milan Majercik |
| [CASSANDRA-12796](https://issues.apache.org/jira/browse/CASSANDRA-12796) | Heap exhaustion when rebuilding secondary index over a table with wide partitions |  Critical | Core, Secondary Indexes | Milan Majercik | Sam Tunnicliffe |
| [CASSANDRA-8616](https://issues.apache.org/jira/browse/CASSANDRA-8616) | sstable tools may result in commit log segments be written |  Major | Tools | Tyler Hobbs | Yuki Morishita |
| [CASSANDRA-12959](https://issues.apache.org/jira/browse/CASSANDRA-12959) | copy from csv import wrong values with udt having set when fields are not specified in correct order in csv |  Major | Tools | Quentin Ambard | Stefania |
| [CASSANDRA-12909](https://issues.apache.org/jira/browse/CASSANDRA-12909) | cqlsh copy cannot parse strings when counters are present |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-13074](https://issues.apache.org/jira/browse/CASSANDRA-13074) | DynamicEndpointSnitch frequently no-ops through early exit in multi-datacenter situations |  Major | Coordination | Joel Knighton | Joel Knighton |
| [CASSANDRA-12453](https://issues.apache.org/jira/browse/CASSANDRA-12453) | AutoSavingCache does not store required keys making RowCacheTests Flaky |  Minor | Core | sankalp kohli | Jay Zhuang |
| [CASSANDRA-13046](https://issues.apache.org/jira/browse/CASSANDRA-13046) | Missing PID file directory when installing from RPM |  Minor | Packaging | Marcel Dopita | Michael Shuler |
| [CASSANDRA-12979](https://issues.apache.org/jira/browse/CASSANDRA-12979) | checkAvailableDiskSpace doesn't update expectedWriteSize when reducing thread scope |  Major | . | Jon Haddad | Jon Haddad |
| [CASSANDRA-12856](https://issues.apache.org/jira/browse/CASSANDRA-12856) | dtest failure in replication\_test.SnitchConfigurationUpdateTest.test\_cannot\_restart\_with\_different\_rack |  Major | . | Sean McCarthy | Stefania |
| [CASSANDRA-13017](https://issues.apache.org/jira/browse/CASSANDRA-13017) | DISTINCT queries on partition keys and static column might not return all the results |  Major | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-13009](https://issues.apache.org/jira/browse/CASSANDRA-13009) | Speculative retry bugs |  Major | . | Simon Zhou | Simon Zhou |
| [CASSANDRA-13114](https://issues.apache.org/jira/browse/CASSANDRA-13114) | Upgrade netty to 4.0.44 to fix memory leak with client encryption |  Blocker | . | Tom van der Woerdt | Stefan Podkowinski |
| [CASSANDRA-12539](https://issues.apache.org/jira/browse/CASSANDRA-12539) | Empty CommitLog prevents restart |  Major | . | Stefano Ortolani | Benjamin Lerer |
| [CASSANDRA-13159](https://issues.apache.org/jira/browse/CASSANDRA-13159) | Coalescing strategy can enter infinite loop |  Major | Streaming and Messaging | Corentin Chary | Corentin Chary |
| [CASSANDRA-13204](https://issues.apache.org/jira/browse/CASSANDRA-13204) | Thread Leak in OutboundTcpConnection |  Major | . | sankalp kohli | Jason Brown |
| [CASSANDRA-12876](https://issues.apache.org/jira/browse/CASSANDRA-12876) | Negative mean write latency |  Major | Observability | Kévin LOVATO | Per Otterström |
| [CASSANDRA-13219](https://issues.apache.org/jira/browse/CASSANDRA-13219) | Cassandra.yaml now unicode instead of ascii after 13090 |  Minor | Configuration | Philip Thompson | Ariel Weisberg |
| [CASSANDRA-13211](https://issues.apache.org/jira/browse/CASSANDRA-13211) | Use portable stderr for java error in startup |  Major | . | Max Bowsher | Michael Shuler |


