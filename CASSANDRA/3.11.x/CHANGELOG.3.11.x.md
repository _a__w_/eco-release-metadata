
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.11.x - Unreleased (as of 2018-10-20)



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10917](https://issues.apache.org/jira/browse/CASSANDRA-10917) | better validator randomness |  Trivial | Local Write-Read Paths | Dave Brosius | Dave Brosius |
| [CASSANDRA-10918](https://issues.apache.org/jira/browse/CASSANDRA-10918) | remove leftover code from refactor |  Trivial | Local Write-Read Paths | Dave Brosius | Dave Brosius |
| [CASSANDRA-10941](https://issues.apache.org/jira/browse/CASSANDRA-10941) | Lists: only fetch cell path if you have a valid value to add |  Trivial | Local Write-Read Paths | Dave Brosius | Dave Brosius |
| [CASSANDRA-10966](https://issues.apache.org/jira/browse/CASSANDRA-10966) | guard against legacy migration failure due to non-existent index name |  Trivial | Distributed Metadata | Dave Brosius | Dave Brosius |
| [CASSANDRA-14302](https://issues.apache.org/jira/browse/CASSANDRA-14302) | Log when sstables are deleted |  Minor | . | Blake Eggleston | Blake Eggleston |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11018](https://issues.apache.org/jira/browse/CASSANDRA-11018) | Drop column in results in corrupted table or tables state (reversible) |  Minor | CQL | Jason Kania | Sylvain Lebresne |
| [CASSANDRA-11047](https://issues.apache.org/jira/browse/CASSANDRA-11047) | native protocol will not bind ipv6 |  Major | CQL | Brandon Williams | Norman Maurer |
| [CASSANDRA-11085](https://issues.apache.org/jira/browse/CASSANDRA-11085) | cassandra-3.3 eclipse-warnings |  Minor | Testing | Michael Shuler | Benjamin Lerer |
| [CASSANDRA-11210](https://issues.apache.org/jira/browse/CASSANDRA-11210) | Unresolved hostname in replace address |  Minor | . | sankalp kohli | Jan Karlsson |
| [CASSANDRA-11339](https://issues.apache.org/jira/browse/CASSANDRA-11339) | WHERE clause in SELECT DISTINCT can be ignored |  Major | CQL | Philip Thompson | Alex Petrov |
| [CASSANDRA-10583](https://issues.apache.org/jira/browse/CASSANDRA-10583) | After bulk loading CQL query on timestamp column returns wrong result |  Major | . | Kai Wang |  |
| [CASSANDRA-10848](https://issues.apache.org/jira/browse/CASSANDRA-10848) | Upgrade paging dtests involving deletion flap on CassCI |  Major | . | Jim Witschey | Russ Hatch |
| [CASSANDRA-13174](https://issues.apache.org/jira/browse/CASSANDRA-13174) | Indexing is allowed on Duration type when it should not be |  Major | CQL | Kishan Karunaratne | Benjamin Lerer |
| [CASSANDRA-13130](https://issues.apache.org/jira/browse/CASSANDRA-13130) | Strange result of several list updates in a single request |  Trivial | . | Mikhail Krupitskiy | Benjamin Lerer |
| [CASSANDRA-13370](https://issues.apache.org/jira/browse/CASSANDRA-13370) | unittest CipherFactoryTest failed on MacOS |  Minor | Testing | Jay Zhuang | Jay Zhuang |
| [CASSANDRA-12962](https://issues.apache.org/jira/browse/CASSANDRA-12962) | SASI: Index are rebuilt on restart |  Minor | sasi | Corentin Chary | Alex Petrov |
| [CASSANDRA-13229](https://issues.apache.org/jira/browse/CASSANDRA-13229) | dtest failure in topology\_test.TestTopology.size\_estimates\_multidc\_test |  Major | Testing | Sean McCarthy | Alex Petrov |
| [CASSANDRA-13302](https://issues.apache.org/jira/browse/CASSANDRA-13302) | last row of previous page == first row of next page while querying data using SASI index |  Major | . | Andy Tolbert | Alex Petrov |
| [CASSANDRA-13216](https://issues.apache.org/jira/browse/CASSANDRA-13216) | testall failure in org.apache.cassandra.net.MessagingServiceTest.testDroppedMessages |  Major | Testing | Sean McCarthy | Alex Petrov |
| [CASSANDRA-12617](https://issues.apache.org/jira/browse/CASSANDRA-12617) | dtest failure in offline\_tools\_test.TestOfflineTools.sstableofflinerelevel\_test |  Major | . | Sean McCarthy | Ariel Weisberg |
| [CASSANDRA-12952](https://issues.apache.org/jira/browse/CASSANDRA-12952) | AlterTableStatement propagates base table and affected MV changes inconsistently |  Major | Distributed Metadata, Materialized Views | Aleksey Yeschenko | Andrés de la Peña |
| [CASSANDRA-13573](https://issues.apache.org/jira/browse/CASSANDRA-13573) | ColumnMetadata.cellValueType() doesn't return correct type for non-frozen collection |  Major | Core, CQL, Materialized Views, Tools | Stefano Ortolani | ZhaoYang |
| [CASSANDRA-13750](https://issues.apache.org/jira/browse/CASSANDRA-13750) | Counter digests include local data |  Minor | . | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-13640](https://issues.apache.org/jira/browse/CASSANDRA-13640) | CQLSH error when using 'login' to switch users |  Minor | CQL | Andrés de la Peña | Andrés de la Peña |
| [CASSANDRA-13622](https://issues.apache.org/jira/browse/CASSANDRA-13622) | Better config validation/documentation |  Minor | Configuration | Kurt Greaves | ZhaoYang |
| [CASSANDRA-13849](https://issues.apache.org/jira/browse/CASSANDRA-13849) | GossipStage blocks because of race in ActiveRepairService |  Major | . | Tom van der Woerdt | Sergey Lapukhov |
| [CASSANDRA-14205](https://issues.apache.org/jira/browse/CASSANDRA-14205) | ReservedKeywords class is missing some reserved CQL keywords |  Major | CQL | Andrés de la Peña | Andrés de la Peña |
| [CASSANDRA-13121](https://issues.apache.org/jira/browse/CASSANDRA-13121) | repair progress message breaks legacy JMX support |  Minor | Streaming and Messaging | Scott Bale | Patrick Bannister |
| [CASSANDRA-9652](https://issues.apache.org/jira/browse/CASSANDRA-9652) | Nodetool cleanup does not work for nodes taken out of replication |  Minor | . | Erick Ramirez |  |
| [CASSANDRA-11811](https://issues.apache.org/jira/browse/CASSANDRA-11811) | dtest failure in snapshot\_test.TestArchiveCommitlog.test\_archive\_commitlog |  Major | . | Philip Thompson | Jim Witschey |
| [CASSANDRA-14803](https://issues.apache.org/jira/browse/CASSANDRA-14803) | Rows that cross index block boundaries can cause incomplete reverse reads in some cases. |  Major | . | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-14823](https://issues.apache.org/jira/browse/CASSANDRA-14823) | Legacy sstables with range tombstones spanning multiple index blocks create invalid bound sequences on 3.0+ |  Major | . | Blake Eggleston | Blake Eggleston |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9464](https://issues.apache.org/jira/browse/CASSANDRA-9464) | test-clientutil-jar broken |  Major | . | Michael Shuler |  |
| [CASSANDRA-11539](https://issues.apache.org/jira/browse/CASSANDRA-11539) | dtest failure in topology\_test.TestTopology.movement\_test |  Major | Testing | Michael Shuler | Russ Hatch |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10334](https://issues.apache.org/jira/browse/CASSANDRA-10334) | Generate flame graphs from canonical bulk reading workload running in CI |  Major | . | Ariel Weisberg | Alan Boudreault |
| [CASSANDRA-10335](https://issues.apache.org/jira/browse/CASSANDRA-10335) | Collect flight recordings of canonical bulk read workload in CI |  Major | . | Ariel Weisberg | Ryan McGuire |
| [CASSANDRA-10526](https://issues.apache.org/jira/browse/CASSANDRA-10526) | Add dtest for CASSANDRA-10406 |  Minor | . | Yuki Morishita | Yuki Morishita |


