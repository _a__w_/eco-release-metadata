
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.2.4 - 2013-04-11



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5302](https://issues.apache.org/jira/browse/CASSANDRA-5302) | Fix nodetool ring and status output format for IPv6 addresses |  Trivial | Tools | Michał Michalski | Michał Michalski |
| [CASSANDRA-5361](https://issues.apache.org/jira/browse/CASSANDRA-5361) | Enable ThreadLocal allocation in the JVM |  Major | . | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-5373](https://issues.apache.org/jira/browse/CASSANDRA-5373) | Collection of min/max timestamp during compaction seems unnecessarily imprecise |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5366](https://issues.apache.org/jira/browse/CASSANDRA-5366) | UpgradeSSTables Optimisation |  Major | . | Brooke Bryan | Sylvain Lebresne |
| [CASSANDRA-5413](https://issues.apache.org/jira/browse/CASSANDRA-5413) | cqlsh returns map entries in wrong order |  Trivial | CQL, Tools | Swav Swiac | Aleksey Yeschenko |
| [CASSANDRA-5401](https://issues.apache.org/jira/browse/CASSANDRA-5401) | Pluggable security feature to prevent node from joining a cluster and running destructive commands |  Trivial | Configuration | Ahmed Bashir | Aleksey Yeschenko |
| [CASSANDRA-5430](https://issues.apache.org/jira/browse/CASSANDRA-5430) | Make Auth.SUPERUSER\_SETUP\_DELAY configurable |  Minor | . | John Sanda | Aleksey Yeschenko |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5350](https://issues.apache.org/jira/browse/CASSANDRA-5350) | Fix ColumnFamily opening race |  Major | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-5352](https://issues.apache.org/jira/browse/CASSANDRA-5352) | PreparedStatements get mixed up between Keyspaces |  Major | . | David Sauer | David Sauer |
| [CASSANDRA-5354](https://issues.apache.org/jira/browse/CASSANDRA-5354) | CL regression in the presence of bootstrapping nodes |  Major | . | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-5362](https://issues.apache.org/jira/browse/CASSANDRA-5362) | Transposed KS/CF arguments |  Trivial | . | Joaquin Casares | Sylvain Lebresne |
| [CASSANDRA-5368](https://issues.apache.org/jira/browse/CASSANDRA-5368) | ClosedChannelException on shutdown |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-5179](https://issues.apache.org/jira/browse/CASSANDRA-5179) | Hinted handoff sends over 1000 rows for one column change |  Major | . | Antti Koivisto | Aleksey Yeschenko |
| [CASSANDRA-5363](https://issues.apache.org/jira/browse/CASSANDRA-5363) | Missing files in debian /etc/cassandra/conf folder |  Minor | Packaging | Sven Delmas | Brandon Williams |
| [CASSANDRA-5364](https://issues.apache.org/jira/browse/CASSANDRA-5364) | Guava should be bumped to 13.0.1 in maven dependency declaration. |  Major | . | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-5355](https://issues.apache.org/jira/browse/CASSANDRA-5355) | Collection values size is not validated. |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5356](https://issues.apache.org/jira/browse/CASSANDRA-5356) | realclean does not show up with ant -p |  Trivial | Packaging | Chris Burroughs | Chris Burroughs |
| [CASSANDRA-5187](https://issues.apache.org/jira/browse/CASSANDRA-5187) | be consistent in visible messages about DOWN versus dead state |  Trivial | . | Robert Coli |  |
| [CASSANDRA-5036](https://issues.apache.org/jira/browse/CASSANDRA-5036) | Wrong description of 'setstreamthroughput' option |  Trivial | Configuration, Documentation and Website | Alexey Zotov | Alexey Zotov |
| [CASSANDRA-5372](https://issues.apache.org/jira/browse/CASSANDRA-5372) | Broken default values for min/max timestamp |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5164](https://issues.apache.org/jira/browse/CASSANDRA-5164) | Invalid streamId in cql binary protocol when using invalid CL |  Minor | CQL | Pierre Chalamet | Sylvain Lebresne |
| [CASSANDRA-5382](https://issues.apache.org/jira/browse/CASSANDRA-5382) | ConcurrentModificantionException on server when multiple CQL3 read requests received on single column family simultaneously. |  Minor | . | Jason Reber | Sylvain Lebresne |
| [CASSANDRA-5376](https://issues.apache.org/jira/browse/CASSANDRA-5376) | CQL3: IN clause on last key not working when schema includes set,list or map |  Minor | . | Christiaan Willemsen | Sylvain Lebresne |
| [CASSANDRA-5386](https://issues.apache.org/jira/browse/CASSANDRA-5386) | CQL Not Handling Descending Clustering Order On A timeuuid Correctly |  Major | . | Gareth Collins | Sylvain Lebresne |
| [CASSANDRA-5392](https://issues.apache.org/jira/browse/CASSANDRA-5392) | cassandra-all 1.2.0 pom missing netty dependency |  Major | Packaging | Sean Bridges | Sean Bridges |
| [CASSANDRA-5341](https://issues.apache.org/jira/browse/CASSANDRA-5341) | Select writetime  Exception when  data doesn't exist |  Minor | CQL | julien campan | Sylvain Lebresne |
| [CASSANDRA-5404](https://issues.apache.org/jira/browse/CASSANDRA-5404) | NPE during cql3 select with token() |  Major | . | Brandon Williams | Sylvain Lebresne |
| [CASSANDRA-5385](https://issues.apache.org/jira/browse/CASSANDRA-5385) | IndexHelper.skipBloomFilters won't skip non-SHA filters |  Major | . | Carl Yeksigian | Carl Yeksigian |
| [CASSANDRA-5391](https://issues.apache.org/jira/browse/CASSANDRA-5391) | SSL problems with inter-DC communication |  Blocker | . | Ondřej Černoš | Yuki Morishita |
| [CASSANDRA-5395](https://issues.apache.org/jira/browse/CASSANDRA-5395) | Compaction doesn't remove index entries as designed |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5397](https://issues.apache.org/jira/browse/CASSANDRA-5397) | Updates to PerRowSecondaryIndex don't use most current values |  Minor | . | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-5423](https://issues.apache.org/jira/browse/CASSANDRA-5423) | PasswordAuthenticator is incompatible with various Cassandra clients |  Minor | . | Sven Delmas | Aleksey Yeschenko |
| [CASSANDRA-5410](https://issues.apache.org/jira/browse/CASSANDRA-5410) | incremental backups race |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5425](https://issues.apache.org/jira/browse/CASSANDRA-5425) | disablebinary nodetool command |  Minor | . | Joaquin Casares | Michał Michalski |
| [CASSANDRA-5415](https://issues.apache.org/jira/browse/CASSANDRA-5415) | Batch with timestamp failed |  Minor | . | Alexey Tereschenko | Aleksey Yeschenko |
| [CASSANDRA-5431](https://issues.apache.org/jira/browse/CASSANDRA-5431) | cassandra-shuffle with JMX usernames and passwords |  Major | . | Eric Dong | Michał Michalski |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3783](https://issues.apache.org/jira/browse/CASSANDRA-3783) | Add 'null' support to CQL 3.0 |  Minor | CQL | Sylvain Lebresne | Michał Michalski |


