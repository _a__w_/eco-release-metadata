
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.8.0 - 2011-06-02



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2602](https://issues.apache.org/jira/browse/CASSANDRA-2602) | Add check in SSTable.componentsFor() for temp files |  Minor | . | amorton | amorton |
| [CASSANDRA-2612](https://issues.apache.org/jira/browse/CASSANDRA-2612) | Disable compaction throttling during bootstrap |  Minor | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-2667](https://issues.apache.org/jira/browse/CASSANDRA-2667) | Rewrite ColumnFamilyOutputFormat to use thrift (not arvo) |  Minor | . | mck | mck |
| [CASSANDRA-2690](https://issues.apache.org/jira/browse/CASSANDRA-2690) | Make the release build fail if the publish to central repository also fails |  Major | Packaging | Stephen Connolly | Stephen Connolly |
| [CASSANDRA-2689](https://issues.apache.org/jira/browse/CASSANDRA-2689) | output version string w/ bin/cassandra -v |  Minor | Tools | Eric Evans | Eric Evans |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2581](https://issues.apache.org/jira/browse/CASSANDRA-2581) | Rebuffer called excessively during seeks |  Minor | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-2593](https://issues.apache.org/jira/browse/CASSANDRA-2593) | CQL: Errors when running unqualified "select column" statement (no where clause) |  Minor | CQL | Cathy Daw | Pavel Yaskevich |
| [CASSANDRA-2599](https://issues.apache.org/jira/browse/CASSANDRA-2599) | Command Line Client has Memtable thresholds: (millions of ops/minutes/MB) last two parameters reversed |  Minor | . | Joe Siegrist |  |
| [CASSANDRA-2483](https://issues.apache.org/jira/browse/CASSANDRA-2483) | Stress.java fails to run if the keyspace already exists |  Minor | Tools | Nick Bailey |  |
| [CASSANDRA-2605](https://issues.apache.org/jira/browse/CASSANDRA-2605) | Merkle tree splitting can exit early |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2596](https://issues.apache.org/jira/browse/CASSANDRA-2596) | include indexes in snapshots |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2598](https://issues.apache.org/jira/browse/CASSANDRA-2598) | incremental\_backups and snapshot\_before\_compaction duplicate hard links |  Minor | . | mck | Jonathan Ellis |
| [CASSANDRA-2604](https://issues.apache.org/jira/browse/CASSANDRA-2604) | EOFException on commitlogs |  Major | . | Terje Marthinussen | Sylvain Lebresne |
| [CASSANDRA-2613](https://issues.apache.org/jira/browse/CASSANDRA-2613) | CQL test failures |  Critical | CQL | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-2419](https://issues.apache.org/jira/browse/CASSANDRA-2419) | Risk of counter over-count when recovering commit log |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2619](https://issues.apache.org/jira/browse/CASSANDRA-2619) | secondary index not dropped until restart |  Major | Secondary Indexes | Jackson Chung | Jonathan Ellis |
| [CASSANDRA-2600](https://issues.apache.org/jira/browse/CASSANDRA-2600) | CQL: Range query throws errors when run thru cqlsh but passes in system test |  Minor | . | Cathy Daw | Pavel Yaskevich |
| [CASSANDRA-2627](https://issues.apache.org/jira/browse/CASSANDRA-2627) | Don't allow {LOCAL\|EACH}\_QUORUM unless strategy is NTS |  Trivial | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2622](https://issues.apache.org/jira/browse/CASSANDRA-2622) | Select \* doesn't include row key |  Major | CQL | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2624](https://issues.apache.org/jira/browse/CASSANDRA-2624) | SimpleStrategy w/o replication\_factor |  Major | CQL, Tools | Eric Evans | Jonathan Ellis |
| [CASSANDRA-2638](https://issues.apache.org/jira/browse/CASSANDRA-2638) | Migrations announce on startup attempts to set local gossip state before gossiper is started. |  Major | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-2628](https://issues.apache.org/jira/browse/CASSANDRA-2628) | Empty Result with Secondary Index Queries with "limit 1" |  Major | Secondary Indexes | Muga Nishizawa | Sylvain Lebresne |
| [CASSANDRA-2642](https://issues.apache.org/jira/browse/CASSANDRA-2642) | CounterColumn Increments lost after restart |  Critical | . | Utku Can Topcu | Sylvain Lebresne |
| [CASSANDRA-2623](https://issues.apache.org/jira/browse/CASSANDRA-2623) | CLI escaped single quote parsing gives errors |  Minor | Tools | rday | Pavel Yaskevich |
| [CASSANDRA-2615](https://issues.apache.org/jira/browse/CASSANDRA-2615) | in cassandra-cli, the help command output on validation types should be updated |  Minor | . | Jackson Chung | Pavel Yaskevich |
| [CASSANDRA-2566](https://issues.apache.org/jira/browse/CASSANDRA-2566) | CQL: Batch Updates: some consistency levels not working |  Minor | CQL | Cathy Daw | Pavel Yaskevich |
| [CASSANDRA-2647](https://issues.apache.org/jira/browse/CASSANDRA-2647) | Cassandra can't find jamm on startup |  Minor | Packaging | Marcus Bointon | Eric Evans |
| [CASSANDRA-2672](https://issues.apache.org/jira/browse/CASSANDRA-2672) | The class o.a.c.cql.jdbc.TypedColumn needs to be declared public |  Trivial | CQL | Rick Shaw | Rick Shaw |
| [CASSANDRA-2592](https://issues.apache.org/jira/browse/CASSANDRA-2592) | CQL greater-than and less-than operators (\> and \<) result in key ranges that are inclusive of the terms |  Major | CQL | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-2682](https://issues.apache.org/jira/browse/CASSANDRA-2682) | UUIDType assumes ByteBuffer has an accessible backing array |  Major | . | Ed Anuff | Ed Anuff |
| [CASSANDRA-2684](https://issues.apache.org/jira/browse/CASSANDRA-2684) | IntergerType uses Thrift method that attempts to unsafely access backing array of ByteBuffer and fails |  Minor | . | Ed Anuff | Ed Anuff |
| [CASSANDRA-2481](https://issues.apache.org/jira/browse/CASSANDRA-2481) | C\* .deb installs C\* init.d scripts such that C\* comes up before mdadm and related |  Minor | Packaging | Matthew F. Dennis | paul cannon |
| [CASSANDRA-2687](https://issues.apache.org/jira/browse/CASSANDRA-2687) | generate-eclipse-files ant target throws StackOverflowError in eclipse |  Minor | . | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-2678](https://issues.apache.org/jira/browse/CASSANDRA-2678) | Incorrect NetworkTopolgyStrategy Options on upgrade from 0.7.5 |  Minor | . | Chris Trahman | Jonathan Ellis |
| [CASSANDRA-2696](https://issues.apache.org/jira/browse/CASSANDRA-2696) | Exception adding validators to non-string columns |  Minor | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-2694](https://issues.apache.org/jira/browse/CASSANDRA-2694) | stop JDBC driver from needing access to cassandra.yaml |  Minor | . | amorton | amorton |
| [CASSANDRA-2706](https://issues.apache.org/jira/browse/CASSANDRA-2706) | Pig output not working with 0.8.0 branch |  Major | . | Jeremy Hanna | Brandon Williams |
| [CASSANDRA-2626](https://issues.apache.org/jira/browse/CASSANDRA-2626) | stack overflow while compacting |  Major | . | Terje Marthinussen | Shotaro Kamio |
| [CASSANDRA-2713](https://issues.apache.org/jira/browse/CASSANDRA-2713) | Null strategy\_options on a KsDef leads to an NPE. |  Minor | . | Jon Hermes | Jon Hermes |
| [CASSANDRA-2649](https://issues.apache.org/jira/browse/CASSANDRA-2649) | work-around schema disagreements from cqlsh |  Minor | Tools | Eric Evans | Eric Evans |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2607](https://issues.apache.org/jira/browse/CASSANDRA-2607) | remove clustertool |  Minor | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2526](https://issues.apache.org/jira/browse/CASSANDRA-2526) | Add ant support to generate CQL documentation from textile source |  Minor | Documentation and Website | Jonathan Ellis | Nate McCall |


