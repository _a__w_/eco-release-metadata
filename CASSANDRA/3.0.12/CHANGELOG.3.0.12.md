
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.0.12 - 2017-03-10



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13230](https://issues.apache.org/jira/browse/CASSANDRA-13230) | Build RPM packages for release |  Major | Packaging | Michael Shuler | Stefan Podkowinski |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13179](https://issues.apache.org/jira/browse/CASSANDRA-13179) | Remove offheap\_buffer as option for memtable\_allocation\_type in cassandra.yaml |  Major | Configuration | Brad Vernon |  |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13185](https://issues.apache.org/jira/browse/CASSANDRA-13185) | cqlsh COPY doesn't support dates before 1900 or after 9999 |  Major | Tools | Tyler Hobbs | Stefania |
| [CASSANDRA-13108](https://issues.apache.org/jira/browse/CASSANDRA-13108) | Uncaught exeption stack traces not logged |  Trivial | Core | Christopher Batey | Christopher Batey |
| [CASSANDRA-12497](https://issues.apache.org/jira/browse/CASSANDRA-12497) | COPY ... TO STDOUT regression in 2.2.7 |  Major | Tools | Max Bowsher | Márton Salomváry |
| [CASSANDRA-13070](https://issues.apache.org/jira/browse/CASSANDRA-13070) | unittest antiCompactionSizeTest is flaky |  Major | Testing | Jay Zhuang | Jay Zhuang |
| [CASSANDRA-12202](https://issues.apache.org/jira/browse/CASSANDRA-12202) | LongLeveledCompactionStrategyTest flapping in 2.1, 2.2, 3.0 |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-13237](https://issues.apache.org/jira/browse/CASSANDRA-13237) | Legacy deserializer can create unexpected boundary range tombstones |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-13090](https://issues.apache.org/jira/browse/CASSANDRA-13090) | Coalescing strategy sleeps too much |  Major | Streaming and Messaging | Corentin Chary | Corentin Chary |
| [CASSANDRA-13232](https://issues.apache.org/jira/browse/CASSANDRA-13232) | "multiple versions of ant detected in path for junit" printed for every junit test case spawned by "ant test" |  Major | Build | Michael Kjellman | Michael Kjellman |
| [CASSANDRA-13278](https://issues.apache.org/jira/browse/CASSANDRA-13278) | Update build.xml and build.properties.default maven repos |  Minor | Build | Michael Shuler | Robert Stupp |
| [CASSANDRA-12886](https://issues.apache.org/jira/browse/CASSANDRA-12886) | Streaming failed due to SSL Socket connection reset |  Major | . | Bing Wu | Paulo Motta |
| [CASSANDRA-13071](https://issues.apache.org/jira/browse/CASSANDRA-13071) | cqlsh copy-from should error out when csv contains invalid data for collections |  Minor | Tools | Stefania | Stefania |
| [CASSANDRA-13038](https://issues.apache.org/jira/browse/CASSANDRA-13038) | 33% of compaction time spent in StreamingHistogram.update() |  Major | Compaction | Corentin Chary | Jeff Jirsa |
| [CASSANDRA-13231](https://issues.apache.org/jira/browse/CASSANDRA-13231) | org.apache.cassandra.db.DirectoriesTest(testStandardDirs) unit test failing |  Major | . | Michael Kjellman | Michael Kjellman |
| [CASSANDRA-13294](https://issues.apache.org/jira/browse/CASSANDRA-13294) | Possible data loss on upgrade 2.1 - 3.0 |  Blocker | Local Write-Read Paths | Marcus Eriksson | Stefania |
| [CASSANDRA-13233](https://issues.apache.org/jira/browse/CASSANDRA-13233) | Improve testing on macOS by eliminating sigar logging |  Major | . | Michael Kjellman | Michael Kjellman |


