
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.2.0 - 2015-07-20



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9544](https://issues.apache.org/jira/browse/CASSANDRA-9544) | Allow specification of TLS protocol to use for cqlsh |  Major | Tools | Jesse Szwedko | Jesse Szwedko |
| [CASSANDRA-9791](https://issues.apache.org/jira/browse/CASSANDRA-9791) | Adjust stop-server.ps1 to behave similarly to stop behavior in cassandra init script |  Minor | Configuration | Andy Tolbert | Andy Tolbert |
| [CASSANDRA-9682](https://issues.apache.org/jira/browse/CASSANDRA-9682) | setting log4j.logger.org.apache.cassandra=DEBUG causes keyspace username/password to show up in system.log |  Major | Observability | Victor Chen | Sam Tunnicliffe |
| [CASSANDRA-9797](https://issues.apache.org/jira/browse/CASSANDRA-9797) | Don't wrap byte arrays in SequentialWriter |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-7731](https://issues.apache.org/jira/browse/CASSANDRA-7731) | Get max values for live/tombstone cells per slice |  Minor | . | Cyril Scetbon | Robert Stupp |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9740](https://issues.apache.org/jira/browse/CASSANDRA-9740) | Can't transition from write survey to normal mode |  Trivial | . | Jason Brown | Jason Brown |
| [CASSANDRA-9750](https://issues.apache.org/jira/browse/CASSANDRA-9750) | hashCode in UDFunction broken |  Trivial | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-9671](https://issues.apache.org/jira/browse/CASSANDRA-9671) | sum() and avg() functions missing for smallint and tinyint types |  Major | . | Aleksey Yeschenko | Robert Stupp |
| [CASSANDRA-9686](https://issues.apache.org/jira/browse/CASSANDRA-9686) | FSReadError and LEAK DETECTED after upgrading |  Major | Local Write-Read Paths | Andreas Schnitzerling | Stefania |
| [CASSANDRA-9519](https://issues.apache.org/jira/browse/CASSANDRA-9519) | CASSANDRA-8448 Doesn't seem to be fixed |  Major | . | Jeremiah Jordan | Sylvain Lebresne |
| [CASSANDRA-9837](https://issues.apache.org/jira/browse/CASSANDRA-9837) | Fix broken logging for "empty" flushes in Memtable |  Trivial | . | Michael McKibben |  |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9795](https://issues.apache.org/jira/browse/CASSANDRA-9795) | Fix cqlsh dtests on windows |  Major | Testing, Tools | T Jake Luciani | T Jake Luciani |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9767](https://issues.apache.org/jira/browse/CASSANDRA-9767) | Allow the selection of columns together with aggregates |  Minor | CQL | Ajay | Benjamin Lerer |
| [CASSANDRA-9771](https://issues.apache.org/jira/browse/CASSANDRA-9771) | Revert CASSANDRA-9542 (allow native functions in UDA) |  Blocker | . | Robert Stupp | Robert Stupp |


