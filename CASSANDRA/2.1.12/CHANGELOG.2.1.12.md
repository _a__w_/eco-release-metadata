
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1.12 - 2015-12-07



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10242](https://issues.apache.org/jira/browse/CASSANDRA-10242) | Validate rack information on startup |  Major | Configuration | Jonathan Ellis | Carl Yeksigian |
| [CASSANDRA-10544](https://issues.apache.org/jira/browse/CASSANDRA-10544) | Make cqlsh tests work when authentication is configured |  Trivial | Testing, Tools | Adam Holmberg | Stefania |
| [CASSANDRA-10559](https://issues.apache.org/jira/browse/CASSANDRA-10559) | Support encrypted and plain traffic on the same port |  Major | . | Norman Maurer | Norman Maurer |
| [CASSANDRA-10092](https://issues.apache.org/jira/browse/CASSANDRA-10092) | Generalize PerRowSecondaryIndex validation |  Minor | . | Andrés de la Peña | Andrés de la Peña |
| [CASSANDRA-8970](https://issues.apache.org/jira/browse/CASSANDRA-8970) | Allow custom time\_format on cqlsh COPY TO |  Trivial | Tools | Aaron Ploetz | Aaron Ploetz |
| [CASSANDRA-9579](https://issues.apache.org/jira/browse/CASSANDRA-9579) | Add JMX / nodetool command to refresh system.size\_estimates |  Minor | CQL | Piotr Kołaczkowski | Carl Yeksigian |
| [CASSANDRA-10249](https://issues.apache.org/jira/browse/CASSANDRA-10249) | Make buffered read size configurable |  Major | Local Write-Read Paths | Albert P Tobey | Albert P Tobey |
| [CASSANDRA-9043](https://issues.apache.org/jira/browse/CASSANDRA-9043) | Improve COPY command to work with Counter columns |  Minor | . | Sebastian Estevez | ZhaoYang |
| [CASSANDRA-10719](https://issues.apache.org/jira/browse/CASSANDRA-10719) | Inconsistencies within CQL 'describe', and CQL docs/'help describe' |  Minor | CQL, Documentation and Website, Tools | Michael Edge | Michael Edge |
| [CASSANDRA-8935](https://issues.apache.org/jira/browse/CASSANDRA-8935) | cqlsh: Make the column order in COPY FROM more apparent |  Trivial | Tools | Tyler Hobbs | Michael Edge |
| [CASSANDRA-9304](https://issues.apache.org/jira/browse/CASSANDRA-9304) | COPY TO improvements |  Minor | Tools | Jonathan Ellis | Stefania |
| [CASSANDRA-10243](https://issues.apache.org/jira/browse/CASSANDRA-10243) | Warn or fail when changing cluster topology live |  Critical | Tools | Jonathan Ellis | Stefania |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10233](https://issues.apache.org/jira/browse/CASSANDRA-10233) | IndexOutOfBoundsException in HintedHandOffManager |  Minor | . | Omri Iluz | J.P. Eiti Kimura |
| [CASSANDRA-10523](https://issues.apache.org/jira/browse/CASSANDRA-10523) | Distinguish Infinity and -Infinity in cqlsh result formatting |  Minor | Tools | Adam Holmberg | Adam Holmberg |
| [CASSANDRA-10363](https://issues.apache.org/jira/browse/CASSANDRA-10363) | NullPointerException returned with select ttl(value), IN, ORDER BY and paging off |  Minor | CQL | Sucwinder Bassi | Benjamin Lerer |
| [CASSANDRA-10377](https://issues.apache.org/jira/browse/CASSANDRA-10377) | AssertionError: attempted to delete non-existing file CommitLog |  Critical | . | Vovodroid | Vovodroid |
| [CASSANDRA-10545](https://issues.apache.org/jira/browse/CASSANDRA-10545) | JDK bug from CASSANDRA-8220 makes drain die early also |  Trivial | . | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-10264](https://issues.apache.org/jira/browse/CASSANDRA-10264) | Unable to use conditions on static columns for DELETE |  Major | CQL | DOAN DuyHai | Benjamin Lerer |
| [CASSANDRA-10381](https://issues.apache.org/jira/browse/CASSANDRA-10381) | NullPointerException in cqlsh paging through CF with static columns |  Major | CQL | Michael Keeney | Benjamin Lerer |
| [CASSANDRA-10550](https://issues.apache.org/jira/browse/CASSANDRA-10550) | NPE on null 'highestSelectivityIndex()' |  Major | . | Berenguer Blasi | Berenguer Blasi |
| [CASSANDRA-10251](https://issues.apache.org/jira/browse/CASSANDRA-10251) | JVM\_OPTS repetition when started from init script |  Major | Packaging | Eric Evans | Eric Evans |
| [CASSANDRA-10079](https://issues.apache.org/jira/browse/CASSANDRA-10079) | LEAK DETECTED, after nodetool drain |  Major | Lifecycle | Sebastian Estevez | Yuki Morishita |
| [CASSANDRA-10501](https://issues.apache.org/jira/browse/CASSANDRA-10501) | Failure to start up Cassandra when temporary compaction files are not all renamed after kill/crash (FSReadError) |  Major | Local Write-Read Paths | Mathieu Roy | Marcus Eriksson |
| [CASSANDRA-10579](https://issues.apache.org/jira/browse/CASSANDRA-10579) | IndexOutOfBoundsException during memtable flushing at startup (with offheap\_objects) |  Major | . | Jeff Griffith | Benedict |
| [CASSANDRA-10557](https://issues.apache.org/jira/browse/CASSANDRA-10557) | Streaming can throw exception when trying to retry |  Minor | Streaming and Messaging | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-9937](https://issues.apache.org/jira/browse/CASSANDRA-9937) | logback-tools.xml is incorrectly configured for outputing to System.err |  Minor | Tools | Mike Adamson | Mike Adamson |
| [CASSANDRA-10298](https://issues.apache.org/jira/browse/CASSANDRA-10298) | Replaced dead node stayed in gossip forever |  Major | . | Dikang Gu | Dikang Gu |
| [CASSANDRA-9912](https://issues.apache.org/jira/browse/CASSANDRA-9912) | SizeEstimatesRecorder has assertions after decommission sometimes |  Major | Coordination, Distributed Metadata | Jeremiah Jordan | Paulo Motta |
| [CASSANDRA-10633](https://issues.apache.org/jira/browse/CASSANDRA-10633) | cqlsh copy uses wrong variable name for time\_format |  Major | Tools | Jeremiah Jordan | Stefania |
| [CASSANDRA-10401](https://issues.apache.org/jira/browse/CASSANDRA-10401) | Improve json2sstable error reporting on nonexistent column |  Major | Tools | Jose Martinez Poblete | Paulo Motta |
| [CASSANDRA-10605](https://issues.apache.org/jira/browse/CASSANDRA-10605) | Remove superfluous COUNTER\_MUTATION stage mapping |  Major | . | Anubhav Kale | Anubhav Kale |
| [CASSANDRA-10258](https://issues.apache.org/jira/browse/CASSANDRA-10258) | Reject counter writes in CQLSSTableWriter |  Major | Tools | Guillaume VIEL | Paulo Motta |
| [CASSANDRA-10089](https://issues.apache.org/jira/browse/CASSANDRA-10089) | NullPointerException in Gossip handleStateNormal |  Major | Distributed Metadata | Stefania | Stefania |
| [CASSANDRA-10341](https://issues.apache.org/jira/browse/CASSANDRA-10341) | Streaming does not guarantee cache invalidation |  Major | Streaming and Messaging | Benedict | Paulo Motta |
| [CASSANDRA-10485](https://issues.apache.org/jira/browse/CASSANDRA-10485) | Missing host ID on hinted handoff write |  Major | Coordination | Paulo Motta | Paulo Motta |
| [CASSANDRA-10534](https://issues.apache.org/jira/browse/CASSANDRA-10534) | CompressionInfo not being fsynced on close |  Major | Local Write-Read Paths | Sharvanath Pathak | Stefania |
| [CASSANDRA-10422](https://issues.apache.org/jira/browse/CASSANDRA-10422) | Avoid anticompaction when doing subrange repair |  Major | Compaction | Marcus Eriksson | Ariel Weisberg |
| [CASSANDRA-8879](https://issues.apache.org/jira/browse/CASSANDRA-8879) | Alter table on compact storage broken |  Minor | . | Nick Bailey | Aleksey Yeschenko |
| [CASSANDRA-10680](https://issues.apache.org/jira/browse/CASSANDRA-10680) | Deal with small compression chunk size better during streaming plan setup |  Major | Streaming and Messaging | Jeff Jirsa | Yuki Morishita |
| [CASSANDRA-10582](https://issues.apache.org/jira/browse/CASSANDRA-10582) | CorruptSSTableException should print the SS Table Name |  Minor | . | Anubhav Kale | Jeremiah Jordan |
| [CASSANDRA-10692](https://issues.apache.org/jira/browse/CASSANDRA-10692) | Don't remove level info when doing upgradesstables |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-10188](https://issues.apache.org/jira/browse/CASSANDRA-10188) | sstableloader does not use MAX\_HEAP\_SIZE env parameter |  Minor | . | Dan Hable | Michael Shuler |
| [CASSANDRA-2388](https://issues.apache.org/jira/browse/CASSANDRA-2388) | ColumnFamilyRecordReader fails for a given split because a host is down, even if records could reasonably be read from other replica. |  Minor | Tools | Eldon Stegall | Paulo Motta |
| [CASSANDRA-9355](https://issues.apache.org/jira/browse/CASSANDRA-9355) | RecoveryManagerTruncateTest fails in test-compression |  Major | Testing | Michael Shuler | Ariel Weisberg |
| [CASSANDRA-10012](https://issues.apache.org/jira/browse/CASSANDRA-10012) | Deadlock when session streaming is retried after exception |  Major | Streaming and Messaging | Chris Moos | Yuki Morishita |
| [CASSANDRA-10683](https://issues.apache.org/jira/browse/CASSANDRA-10683) | Internal pagination of CQL index queries with compact storage is suboptimal |  Major | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-10044](https://issues.apache.org/jira/browse/CASSANDRA-10044) | Native-Transport-Requests is missing from the nodetool tpstats output in Cassandra 2.1 |  Minor | . | Wei Deng | Wei Deng |
| [CASSANDRA-10740](https://issues.apache.org/jira/browse/CASSANDRA-10740) | Incorrect condition causes cleanup of SSTables which might not need it |  Major | . | Jakub Janecek | Jakub Janecek |
| [CASSANDRA-10760](https://issues.apache.org/jira/browse/CASSANDRA-10760) | Counters are erroneously allowed as map key type |  Minor | CQL | Aleksey Yeschenko | Michael Edge |
| [CASSANDRA-10755](https://issues.apache.org/jira/browse/CASSANDRA-10755) | PreparedStatement is the same id for different Japanese katakana characters with same length |  Major | CQL | ZhaoYang | Stefan Podkowinski |
| [CASSANDRA-10749](https://issues.apache.org/jira/browse/CASSANDRA-10749) | DeletionTime.compareTo wrong in rare cases |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-10658](https://issues.apache.org/jira/browse/CASSANDRA-10658) | Some DROP ... IF EXISTS incorrectly result in exceptions on non-existing KS |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-10213](https://issues.apache.org/jira/browse/CASSANDRA-10213) | Status command in debian/ubuntu init script doesn't work |  Minor | . | Ruggero Marchei | Ruggero Marchei |
| [CASSANDRA-7953](https://issues.apache.org/jira/browse/CASSANDRA-7953) | RangeTombstones not merging during compaction |  Critical | . | Marcus Olsson | Branimir Lambov |
| [CASSANDRA-10774](https://issues.apache.org/jira/browse/CASSANDRA-10774) | Fail stream session if receiver cannot process data |  Critical | Streaming and Messaging | Paulo Motta | Paulo Motta |
| [CASSANDRA-10768](https://issues.apache.org/jira/browse/CASSANDRA-10768) | Optimize the way we check if a token is repaired in anticompaction |  Major | Compaction, Streaming and Messaging | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-10791](https://issues.apache.org/jira/browse/CASSANDRA-10791) | RangeTombstones can be written after END\_OF\_ROW markers when streaming |  Critical | . | Branimir Lambov | Branimir Lambov |
| [CASSANDRA-10288](https://issues.apache.org/jira/browse/CASSANDRA-10288) | Incremental repair can hang if replica aren't all up (was: Inconsistent behaviours on repair when a node in RF is missing) |  Major | Streaming and Messaging | Alan Boudreault | Yuki Morishita |
| [CASSANDRA-10808](https://issues.apache.org/jira/browse/CASSANDRA-10808) | Cannot start Stress on Windows |  Major | Tools | Benjamin Lerer | Benjamin Lerer |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10415](https://issues.apache.org/jira/browse/CASSANDRA-10415) | Fix cqlsh bugs |  Major | Tools | Jim Witschey | Stefania |
| [CASSANDRA-10577](https://issues.apache.org/jira/browse/CASSANDRA-10577) | Fix cqlsh COPY commands that use NULL |  Major | Tools | Jim Witschey | Stefania |
| [CASSANDRA-10276](https://issues.apache.org/jira/browse/CASSANDRA-10276) | Do STCS in DTCS-windows |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-10280](https://issues.apache.org/jira/browse/CASSANDRA-10280) | Make DTCS work well with old data |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |


