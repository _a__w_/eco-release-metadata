
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.11.0 - 2017-06-23



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13230](https://issues.apache.org/jira/browse/CASSANDRA-13230) | Build RPM packages for release |  Major | Packaging | Michael Shuler | Stefan Podkowinski |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12983](https://issues.apache.org/jira/browse/CASSANDRA-12983) | NoReplicationTokenAllocator should work with zero replication factor as well. |  Major | . | Dikang Gu | Dikang Gu |
| [CASSANDRA-13160](https://issues.apache.org/jira/browse/CASSANDRA-13160) | batch documentation should note the single partition optimization |  Minor | Documentation and Website | Jon Haddad | Jon Haddad |
| [CASSANDRA-13180](https://issues.apache.org/jira/browse/CASSANDRA-13180) | Better handling of missing entries in system\_schema.columns during startup |  Major | Distributed Metadata | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-13205](https://issues.apache.org/jira/browse/CASSANDRA-13205) | Hint related logging should include the IP address of the destination in addition to host ID |  Trivial | . | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-12233](https://issues.apache.org/jira/browse/CASSANDRA-12233) | Cassandra stress should obfuscate password in cmd in graph |  Minor | Tools | Christopher Batey | Christopher Batey |
| [CASSANDRA-13008](https://issues.apache.org/jira/browse/CASSANDRA-13008) | Add vm.max\_map\_count StartupCheck |  Minor | Configuration | Jay Zhuang | Jay Zhuang |
| [CASSANDRA-13238](https://issues.apache.org/jira/browse/CASSANDRA-13238) | Add actual row output to assertEmpty error message |  Trivial | . | Alex Petrov | Alex Petrov |
| [CASSANDRA-12915](https://issues.apache.org/jira/browse/CASSANDRA-12915) | SASI: Index intersection with an empty range really inefficient |  Major | sasi | Corentin Chary | Corentin Chary |
| [CASSANDRA-13326](https://issues.apache.org/jira/browse/CASSANDRA-13326) |  Support unaligned memory access for AArch64 |  Minor | Observability, Testing | yuqi | yuqi |
| [CASSANDRA-13388](https://issues.apache.org/jira/browse/CASSANDRA-13388) | Add hosted CI config files (such as Circle and TravisCI) for easier developer testing |  Major | Testing | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-13060](https://issues.apache.org/jira/browse/CASSANDRA-13060) | NPE in StorageService.java while bootstrapping |  Trivial | . | Jay Zhuang | Jay Zhuang |
| [CASSANDRA-13518](https://issues.apache.org/jira/browse/CASSANDRA-13518) | sstableloader doesn't support non default storage\_port and ssl\_storage\_port. |  Minor | Tools | Zhiyan Shao | Zhiyan Shao |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12954](https://issues.apache.org/jira/browse/CASSANDRA-12954) | system\_distributed.view\_build\_status uses gc\_gs of 0, but issues distributed deletes |  Trivial | Distributed Metadata | Aleksey Yeschenko | Jeff Jirsa |
| [CASSANDRA-12910](https://issues.apache.org/jira/browse/CASSANDRA-12910) | SASI: calculatePrimary() always returns null |  Minor | sasi | Corentin Chary | Corentin Chary |
| [CASSANDRA-12998](https://issues.apache.org/jira/browse/CASSANDRA-12998) | Remove a silly hint.isLive() check in HintsService.write() |  Trivial | Local Write-Read Paths | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-13030](https://issues.apache.org/jira/browse/CASSANDRA-13030) | nodetool stopdaemon errors out |  Minor | Tools | Corentin Chary | Corentin Chary |
| [CASSANDRA-13034](https://issues.apache.org/jira/browse/CASSANDRA-13034) | Move to FastThreadLocalThread and FastThreadLocal |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-12990](https://issues.apache.org/jira/browse/CASSANDRA-12990) | More fixes to the TokenAllocator |  Major | . | Dikang Gu | Dikang Gu |
| [CASSANDRA-12997](https://issues.apache.org/jira/browse/CASSANDRA-12997) | Use timestamp from ClientState by default in AlterTableStatement |  Minor | . | Sean McCarthy | Sylvain Lebresne |
| [CASSANDRA-13117](https://issues.apache.org/jira/browse/CASSANDRA-13117) | Dump threads when unit test times out |  Minor | Testing | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-13177](https://issues.apache.org/jira/browse/CASSANDRA-13177) | sstabledump doesn't handle non-empty partitions with a partition-level deletion correctly |  Major | Tools | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-13152](https://issues.apache.org/jira/browse/CASSANDRA-13152) | UPDATE on counter columns with empty list as argument in IN disables cluster |  Major | . | jorge collinet | Benjamin Lerer |
| [CASSANDRA-13125](https://issues.apache.org/jira/browse/CASSANDRA-13125) | Duplicate rows after upgrading from 2.1.16 to 3.0.10/3.9 |  Critical | . | Zhongxiang Zheng | Sylvain Lebresne |
| [CASSANDRA-13109](https://issues.apache.org/jira/browse/CASSANDRA-13109) | Lightweight transactions temporarily fail after upgrade from 2.1 to 3.0 |  Major | . | Samuel Klock | Samuel Klock |
| [CASSANDRA-13114](https://issues.apache.org/jira/browse/CASSANDRA-13114) | Upgrade netty to 4.0.44 to fix memory leak with client encryption |  Blocker | . | Tom van der Woerdt | Stefan Podkowinski |
| [CASSANDRA-13173](https://issues.apache.org/jira/browse/CASSANDRA-13173) | Reloading logback.xml does not work |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-12539](https://issues.apache.org/jira/browse/CASSANDRA-12539) | Empty CommitLog prevents restart |  Major | . | Stefano Ortolani | Benjamin Lerer |
| [CASSANDRA-13159](https://issues.apache.org/jira/browse/CASSANDRA-13159) | Coalescing strategy can enter infinite loop |  Major | Streaming and Messaging | Corentin Chary | Corentin Chary |
| [CASSANDRA-13204](https://issues.apache.org/jira/browse/CASSANDRA-13204) | Thread Leak in OutboundTcpConnection |  Major | . | sankalp kohli | Jason Brown |
| [CASSANDRA-12876](https://issues.apache.org/jira/browse/CASSANDRA-12876) | Negative mean write latency |  Major | Observability | Kévin LOVATO | Per Otterström |
| [CASSANDRA-13219](https://issues.apache.org/jira/browse/CASSANDRA-13219) | Cassandra.yaml now unicode instead of ascii after 13090 |  Minor | Configuration | Philip Thompson | Ariel Weisberg |
| [CASSANDRA-13211](https://issues.apache.org/jira/browse/CASSANDRA-13211) | Use portable stderr for java error in startup |  Major | . | Max Bowsher | Michael Shuler |
| [CASSANDRA-13018](https://issues.apache.org/jira/browse/CASSANDRA-13018) | Exceptions encountered calling getSeeds() breaks OTC thread |  Major | Streaming and Messaging | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-13185](https://issues.apache.org/jira/browse/CASSANDRA-13185) | cqlsh COPY doesn't support dates before 1900 or after 9999 |  Major | Tools | Tyler Hobbs | Stefania |
| [CASSANDRA-13108](https://issues.apache.org/jira/browse/CASSANDRA-13108) | Uncaught exeption stack traces not logged |  Trivial | Core | Christopher Batey | Christopher Batey |
| [CASSANDRA-12497](https://issues.apache.org/jira/browse/CASSANDRA-12497) | COPY ... TO STDOUT regression in 2.2.7 |  Major | Tools | Max Bowsher | Márton Salomváry |
| [CASSANDRA-13070](https://issues.apache.org/jira/browse/CASSANDRA-13070) | unittest antiCompactionSizeTest is flaky |  Major | Testing | Jay Zhuang | Jay Zhuang |
| [CASSANDRA-12202](https://issues.apache.org/jira/browse/CASSANDRA-12202) | LongLeveledCompactionStrategyTest flapping in 2.1, 2.2, 3.0 |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-13237](https://issues.apache.org/jira/browse/CASSANDRA-13237) | Legacy deserializer can create unexpected boundary range tombstones |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-13090](https://issues.apache.org/jira/browse/CASSANDRA-13090) | Coalescing strategy sleeps too much |  Major | Streaming and Messaging | Corentin Chary | Corentin Chary |
| [CASSANDRA-13232](https://issues.apache.org/jira/browse/CASSANDRA-13232) | "multiple versions of ant detected in path for junit" printed for every junit test case spawned by "ant test" |  Major | Build | Michael Kjellman | Michael Kjellman |
| [CASSANDRA-13278](https://issues.apache.org/jira/browse/CASSANDRA-13278) | Update build.xml and build.properties.default maven repos |  Minor | Build | Michael Shuler | Robert Stupp |
| [CASSANDRA-13071](https://issues.apache.org/jira/browse/CASSANDRA-13071) | cqlsh copy-from should error out when csv contains invalid data for collections |  Minor | Tools | Stefania | Stefania |
| [CASSANDRA-13038](https://issues.apache.org/jira/browse/CASSANDRA-13038) | 33% of compaction time spent in StreamingHistogram.update() |  Major | Compaction | Corentin Chary | Jeff Jirsa |
| [CASSANDRA-12676](https://issues.apache.org/jira/browse/CASSANDRA-12676) | Message coalescing regression |  Major | . | T Jake Luciani | Jeff Jirsa |
| [CASSANDRA-13231](https://issues.apache.org/jira/browse/CASSANDRA-13231) | org.apache.cassandra.db.DirectoriesTest(testStandardDirs) unit test failing |  Major | . | Michael Kjellman | Michael Kjellman |
| [CASSANDRA-13294](https://issues.apache.org/jira/browse/CASSANDRA-13294) | Possible data loss on upgrade 2.1 - 3.0 |  Blocker | Local Write-Read Paths | Marcus Eriksson | Stefania |
| [CASSANDRA-13053](https://issues.apache.org/jira/browse/CASSANDRA-13053) | GRANT/REVOKE on table without keyspace performs permissions check incorrectly |  Minor | CQL | Sam Tunnicliffe | Aleksey Yeschenko |
| [CASSANDRA-12513](https://issues.apache.org/jira/browse/CASSANDRA-12513) | IOException (No such file or directory) closing MessagingService's server socket (locally) |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-13305](https://issues.apache.org/jira/browse/CASSANDRA-13305) | Slice.isEmpty() returns false for some empty slices |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-13282](https://issues.apache.org/jira/browse/CASSANDRA-13282) | Commitlog replay may fail if last mutation is within 4 bytes of end of segment |  Major | Core | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-13343](https://issues.apache.org/jira/browse/CASSANDRA-13343) | Wrong logger name in AnticompactionTask |  Trivial | . | Simon Zhou | Simon Zhou |
| [CASSANDRA-13233](https://issues.apache.org/jira/browse/CASSANDRA-13233) | Improve testing on macOS by eliminating sigar logging |  Major | . | Michael Kjellman | Michael Kjellman |
| [CASSANDRA-13320](https://issues.apache.org/jira/browse/CASSANDRA-13320) | upgradesstables fails after upgrading from 2.1.x to 3.0.11 |  Major | . | Zhongxiang Zheng | Sam Tunnicliffe |
| [CASSANDRA-13246](https://issues.apache.org/jira/browse/CASSANDRA-13246) | Querying by secondary index on collection column returns NullPointerException sometimes |  Major | Local Write-Read Paths, Secondary Indexes | hochung |  |
| [CASSANDRA-13153](https://issues.apache.org/jira/browse/CASSANDRA-13153) | Reappeared Data when Mixing Incremental and Full Repairs |  Major | Compaction, Tools | Amanda Debrot | Stefan Podkowinski |
| [CASSANDRA-12653](https://issues.apache.org/jira/browse/CASSANDRA-12653) | In-flight shadow round requests |  Minor | Distributed Metadata | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-13366](https://issues.apache.org/jira/browse/CASSANDRA-13366) | Possible AssertionError in UnfilteredRowIteratorWithLowerBound |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-13340](https://issues.apache.org/jira/browse/CASSANDRA-13340) | Bugs handling range tombstones in the sstable iterators |  Critical | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-13317](https://issues.apache.org/jira/browse/CASSANDRA-13317) | Default logging we ship will incorrectly print "?:?" for "%F:%L" pattern due to includeCallerData being false by default no appender |  Major | Core | Michael Kjellman | Michael Kjellman |
| [CASSANDRA-13247](https://issues.apache.org/jira/browse/CASSANDRA-13247) | index on udt built failed and no data could be inserted |  Critical | CQL | mashudong | Andrés de la Peña |
| [CASSANDRA-12773](https://issues.apache.org/jira/browse/CASSANDRA-12773) | cassandra-stress error for one way SSL |  Major | Tools | Jane Deng | Stefan Podkowinski |
| [CASSANDRA-13337](https://issues.apache.org/jira/browse/CASSANDRA-13337) | Dropping column results in "corrupt" SSTable |  Major | Compaction | Jonas Borgström | Sylvain Lebresne |
| [CASSANDRA-13316](https://issues.apache.org/jira/browse/CASSANDRA-13316) | Build error because of dependent jar (byteman-install-3.0.3.jar) currupted |  Major | Testing | Sam Ding |  |
| [CASSANDRA-13274](https://issues.apache.org/jira/browse/CASSANDRA-13274) | Fix code to not exchange schema across major versions |  Critical | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-13298](https://issues.apache.org/jira/browse/CASSANDRA-13298) | DataOutputBuffer.asNewBuffer broken |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-13277](https://issues.apache.org/jira/browse/CASSANDRA-13277) | Duplicate results with secondary index on static column |  Major | Local Write-Read Paths, Secondary Indexes | Romain Hardouin | Andrés de la Peña |
| [CASSANDRA-13382](https://issues.apache.org/jira/browse/CASSANDRA-13382) | cdc column addition strikes again |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-13384](https://issues.apache.org/jira/browse/CASSANDRA-13384) | Legacy caching options can prevent 3.0 upgrade |  Minor | Distributed Metadata | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-13341](https://issues.apache.org/jira/browse/CASSANDRA-13341) | Legacy deserializer can create empty range tombstones |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-13389](https://issues.apache.org/jira/browse/CASSANDRA-13389) | Possible NPE on upgrade to 3.0/3.X in case of IO errors |  Major | Lifecycle | Stefania | Stefania |
| [CASSANDRA-12825](https://issues.apache.org/jira/browse/CASSANDRA-12825) | testall failure in org.apache.cassandra.db.compaction.CompactionsCQLTest.testTriggerMinorCompactionDTCS-compression |  Major | . | Sean McCarthy | Marcus Eriksson |
| [CASSANDRA-13103](https://issues.apache.org/jira/browse/CASSANDRA-13103) | incorrect jvm metric names |  Minor | Observability | Alain Rastoul | Alain Rastoul |
| [CASSANDRA-13020](https://issues.apache.org/jira/browse/CASSANDRA-13020) | Hint delivery fails when prefer\_local enabled |  Major | Streaming and Messaging | Aleksandr Ivanov | Stefan Podkowinski |
| [CASSANDRA-13395](https://issues.apache.org/jira/browse/CASSANDRA-13395) | Expired rows without regular column data can crash upgradesstables |  Major | Local Write-Read Paths | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-13405](https://issues.apache.org/jira/browse/CASSANDRA-13405) | ViewBuilder can miss data due to sstable generation filter |  Major | Local Write-Read Paths, Materialized Views | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-13410](https://issues.apache.org/jira/browse/CASSANDRA-13410) | nodetool upgradesstables/scrub/compact ignores system tables |  Minor | . | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-13417](https://issues.apache.org/jira/browse/CASSANDRA-13417) | Illegal unicode character breaks compilation on Chinese env OS |  Minor | . | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-12213](https://issues.apache.org/jira/browse/CASSANDRA-12213) | dtest failure in write\_failures\_test.TestWriteFailures.test\_paxos\_any |  Major | Distributed Metadata | Craig Kodman | Stefania |
| [CASSANDRA-13364](https://issues.apache.org/jira/browse/CASSANDRA-13364) | Cqlsh COPY fails importing Map\<String,List\<String\>\>, ParseError unhashable type list |  Major | Tools | Nicolae Namolovan | Stefania |
| [CASSANDRA-13413](https://issues.apache.org/jira/browse/CASSANDRA-13413) | Run more test targets on CircleCI |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-13147](https://issues.apache.org/jira/browse/CASSANDRA-13147) | Secondary index query on partition key columns might not return all the rows. |  Major | Secondary Indexes | Benjamin Lerer | Andrés de la Peña |
| [CASSANDRA-13329](https://issues.apache.org/jira/browse/CASSANDRA-13329) | max\_hints\_delivery\_threads does not work |  Major | . | Fuud | Aleksandr Sorokoumov |
| [CASSANDRA-13393](https://issues.apache.org/jira/browse/CASSANDRA-13393) | Fix weightedSize() for row-cache reported by JMX and NodeTool |  Minor | Tools | Fuud | Fuud |
| [CASSANDRA-13092](https://issues.apache.org/jira/browse/CASSANDRA-13092) | Debian package does not install the hostpot\_compiler command file |  Trivial | Packaging | Jan Urbański | Michael Shuler |
| [CASSANDRA-13422](https://issues.apache.org/jira/browse/CASSANDRA-13422) | CompactionStrategyManager should take write not read lock when handling remove notifications |  Major | Local Write-Read Paths | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-13443](https://issues.apache.org/jira/browse/CASSANDRA-13443) | V5 protocol flags decoding broken |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-12728](https://issues.apache.org/jira/browse/CASSANDRA-12728) | Handling partially written hint files |  Major | Hints | Sharvanath Pathak | Garvit Juniwal |
| [CASSANDRA-13307](https://issues.apache.org/jira/browse/CASSANDRA-13307) | The specification of protocol version in cqlsh means the python driver doesn't automatically downgrade protocol version. |  Minor | Tools | Matt Byrd | Matt Byrd |
| [CASSANDRA-13308](https://issues.apache.org/jira/browse/CASSANDRA-13308) | Gossip breaks, Hint files not being deleted on nodetool decommission |  Major | Hints, Streaming and Messaging | Arijit Banerjee | Jeff Jirsa |
| [CASSANDRA-13276](https://issues.apache.org/jira/browse/CASSANDRA-13276) | Regression on CASSANDRA-11416: can't load snapshots of tables with dropped columns |  Major | . | Matt Kopit | Andrés de la Peña |
| [CASSANDRA-13397](https://issues.apache.org/jira/browse/CASSANDRA-13397) | Return value of CountDownLatch.await() not being checked in Repair |  Minor | . | Simon Zhou | Simon Zhou |
| [CASSANDRA-12835](https://issues.apache.org/jira/browse/CASSANDRA-12835) | Tracing payload not passed from QueryMessage to tracing session |  Critical | . | Hannu Kröger | mck |
| [CASSANDRA-13441](https://issues.apache.org/jira/browse/CASSANDRA-13441) | Schema version changes for each upgraded node in a rolling upgrade, causing migration storms |  Major | Distributed Metadata | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-13188](https://issues.apache.org/jira/browse/CASSANDRA-13188) | compaction-stress AssertionError from getMemtableFor() |  Major | Testing, Tools | Jay Zhuang | Jay Zhuang |
| [CASSANDRA-12841](https://issues.apache.org/jira/browse/CASSANDRA-12841) | testall failure in org.apache.cassandra.db.compaction.NeverPurgeTest.minorNeverPurgeTombstonesTest-compression |  Major | . | Sean McCarthy | Marcus Eriksson |
| [CASSANDRA-13399](https://issues.apache.org/jira/browse/CASSANDRA-13399) | UDA fails without input rows |  Minor | CQL | PAF | Robert Stupp |
| [CASSANDRA-13265](https://issues.apache.org/jira/browse/CASSANDRA-13265) | Expiration in OutboundTcpConnection can block the reader Thread |  Major | . | Christian Esken | Christian Esken |
| [CASSANDRA-13163](https://issues.apache.org/jira/browse/CASSANDRA-13163) | NPE in StorageService.excise |  Major | Core | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-13369](https://issues.apache.org/jira/browse/CASSANDRA-13369) | If there are multiple values for a key, CQL grammar choses last value. This should not be silent or should not be allowed. |  Minor | CQL | Nachiket Patil | Nachiket Patil |
| [CASSANDRA-13228](https://issues.apache.org/jira/browse/CASSANDRA-13228) | SASI index on partition key part doesn't match |  Major | sasi | Hannu Kröger | Andrés de la Peña |
| [CASSANDRA-13236](https://issues.apache.org/jira/browse/CASSANDRA-13236) | corrupt flag error after upgrade from 2.2 to 3.0.10 |  Critical | . | ingard mevåg | Sam Tunnicliffe |
| [CASSANDRA-13218](https://issues.apache.org/jira/browse/CASSANDRA-13218) | Duration validation error is unclear in case of overflow. |  Major | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-12847](https://issues.apache.org/jira/browse/CASSANDRA-12847) | cqlsh DESCRIBE output doesn't properly quote index names |  Major | . | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-13052](https://issues.apache.org/jira/browse/CASSANDRA-13052) | Repair process is violating the start/end token limits for small ranges |  Major | Streaming and Messaging | Cristian P | Stefan Podkowinski |
| [CASSANDRA-13525](https://issues.apache.org/jira/browse/CASSANDRA-13525) | ReverseIndexedReader may drop rows during 2.1 to 3.0 upgrade |  Critical | Local Write-Read Paths | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-13533](https://issues.apache.org/jira/browse/CASSANDRA-13533) | ColumnIdentifier object size wrong when tables are not flushed |  Major | Core | Eduard Tudenhoefner | Eduard Tudenhoefner |
| [CASSANDRA-13549](https://issues.apache.org/jira/browse/CASSANDRA-13549) | Cqlsh throws and error when querying a duration data type |  Major | CQL | Akhil Mehra | Akhil Mehra |
| [CASSANDRA-13559](https://issues.apache.org/jira/browse/CASSANDRA-13559) | Schema version id mismatch while upgrading to 3.0.13 |  Blocker | Core | Jay Zhuang | Jay Zhuang |
| [CASSANDRA-13120](https://issues.apache.org/jira/browse/CASSANDRA-13120) | Trace and Histogram output misleading |  Minor | Core | Adam Hattrell | Benjamin Lerer |
| [CASSANDRA-13542](https://issues.apache.org/jira/browse/CASSANDRA-13542) | nodetool scrub/cleanup/upgradesstables exit code |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-13209](https://issues.apache.org/jira/browse/CASSANDRA-13209) | test failure in cqlsh\_tests.cqlsh\_copy\_tests.CqlshCopyTest.test\_bulk\_round\_trip\_blogposts\_with\_max\_connections |  Major | . | Michael Shuler | Kurt Greaves |
| [CASSANDRA-13346](https://issues.apache.org/jira/browse/CASSANDRA-13346) | Failed unregistering mbean during drop keyspace |  Minor | Materialized Views | Gábor Auth | Lerh Chuan Low |
| [CASSANDRA-11381](https://issues.apache.org/jira/browse/CASSANDRA-11381) | Node running with join\_ring=false and authentication can not serve requests |  Major | . | mck | mck |
| [CASSANDRA-13004](https://issues.apache.org/jira/browse/CASSANDRA-13004) | Corruption while adding/removing a column to/from the table |  Blocker | Distributed Metadata | Stanislav Vishnevskiy | Alex Petrov |
| [CASSANDRA-13172](https://issues.apache.org/jira/browse/CASSANDRA-13172) | compaction\_large\_partition\_warning\_threshold\_mb not working properly when set to high value |  Minor | Configuration | Vladimir Vavro | Kurt Greaves |
| [CASSANDRA-13072](https://issues.apache.org/jira/browse/CASSANDRA-13072) | Cassandra failed to run on Linux-aarch64 |  Major | Core | Jun He | Benjamin Lerer |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13059](https://issues.apache.org/jira/browse/CASSANDRA-13059) | dtest failure in upgrade\_tests.upgrade\_through\_versions\_test.TestUpgrade\_current\_2\_1\_x\_To\_indev\_3\_x.rolling\_upgrade\_test |  Major | Testing | Sean McCarthy | Benjamin Lerer |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13434](https://issues.apache.org/jira/browse/CASSANDRA-13434) | Add PID file directive in /etc/init.d/cassandra |  Major | Packaging | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-13435](https://issues.apache.org/jira/browse/CASSANDRA-13435) | Incorrect status check use when stopping Cassandra |  Major | Packaging | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-13490](https://issues.apache.org/jira/browse/CASSANDRA-13490) | RPM Spec - disable binary check, improve readme instructions |  Major | Packaging | martin a langhoff | Stefan Podkowinski |
| [CASSANDRA-13493](https://issues.apache.org/jira/browse/CASSANDRA-13493) | RPM Init: Service startup ordering |  Major | Packaging | martin a langhoff | Stefan Podkowinski |
| [CASSANDRA-13440](https://issues.apache.org/jira/browse/CASSANDRA-13440) | Sign RPM artifacts |  Major | Packaging | Stefan Podkowinski | Michael Shuler |


