
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.0.7 - 2012-01-16



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3600](https://issues.apache.org/jira/browse/CASSANDRA-3600) | Allow overriding RING\_DELAY |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3497](https://issues.apache.org/jira/browse/CASSANDRA-3497) | BloomFilter FP ratio should be configurable or size-restricted some other way |  Minor | . | Brandon Williams | Yuki Morishita |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3654](https://issues.apache.org/jira/browse/CASSANDRA-3654) | Warn when the stored Gossip Generation is from the future |  Trivial | . | amorton | amorton |
| [CASSANDRA-3661](https://issues.apache.org/jira/browse/CASSANDRA-3661) | Re-introduce timeout debug messages in CassandraServer |  Trivial | CQL | Jonathan Ellis | Brandon Williams |
| [CASSANDRA-3571](https://issues.apache.org/jira/browse/CASSANDRA-3571) | make stream throttling configurable at runtime with nodetool |  Minor | Tools | Peter Schuller | Peter Schuller |
| [CASSANDRA-3669](https://issues.apache.org/jira/browse/CASSANDRA-3669) | [patch] Word count sample has a flawed addToMutationMap, fix |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-2940](https://issues.apache.org/jira/browse/CASSANDRA-2940) | Make rpc\_timeout\_in\_ms into a jmx mbean property |  Major | . | Jeremy Hanna | Ruben Terrazas |
| [CASSANDRA-3625](https://issues.apache.org/jira/browse/CASSANDRA-3625) | Do something about DynamicCompositeType |  Major | . | Sylvain Lebresne | Sylvain Lebresne |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3614](https://issues.apache.org/jira/browse/CASSANDRA-3614) | Fatal exception in thread Thread[MigrationStage:1,5,main] (LeveledCompaction) |  Major | . | Andrew Suffield | Jonathan Ellis |
| [CASSANDRA-3618](https://issues.apache.org/jira/browse/CASSANDRA-3618) | OpenBitSet can allocate more bytes than it needs |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3629](https://issues.apache.org/jira/browse/CASSANDRA-3629) | Bootstrapping nodes don't ensure schema is ready before continuing |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3101](https://issues.apache.org/jira/browse/CASSANDRA-3101) | Should check for errors when calling /bin/ln |  Minor | . | paul cannon | Vijay |
| [CASSANDRA-3626](https://issues.apache.org/jira/browse/CASSANDRA-3626) | Nodes can get stuck in UP state forever, despite being DOWN |  Major | . | Peter Schuller | Brandon Williams |
| [CASSANDRA-3544](https://issues.apache.org/jira/browse/CASSANDRA-3544) | NPE on startup when there are permissions issues with directories |  Minor | . | Tyler Hobbs | Yuki Morishita |
| [CASSANDRA-3603](https://issues.apache.org/jira/browse/CASSANDRA-3603) | CounterColumn and CounterContext use a log4j logger instead of using slf4j like the rest of the code base |  Minor | . | Peter Schuller | Peter Schuller |
| [CASSANDRA-3616](https://issues.apache.org/jira/browse/CASSANDRA-3616) | Temp SSTable and file descriptor leak |  Major | . | Eric Parusel | Sylvain Lebresne |
| [CASSANDRA-3327](https://issues.apache.org/jira/browse/CASSANDRA-3327) | Support TimeUUID in CassandraStorage |  Major | . | Manuel Kreutz | Brandon Williams |
| [CASSANDRA-3415](https://issues.apache.org/jira/browse/CASSANDRA-3415) | show schema fails |  Minor | Tools | Radim Kolar | Jonathan Ellis |
| [CASSANDRA-3335](https://issues.apache.org/jira/browse/CASSANDRA-3335) | ThreadPoolExecutor creates threads as non-daemon and will block on shutdown by default |  Minor | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-3659](https://issues.apache.org/jira/browse/CASSANDRA-3659) | Flush non-cfs backed secondary indexes along with CF |  Minor | Secondary Indexes | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-3652](https://issues.apache.org/jira/browse/CASSANDRA-3652) | correct and improve stream protocol mismatch error |  Minor | . | Peter Schuller | Peter Schuller |
| [CASSANDRA-3644](https://issues.apache.org/jira/browse/CASSANDRA-3644) | parsing of chunk\_length\_kb silently overflows |  Minor | . | Peter Schuller | Peter Schuller |
| [CASSANDRA-3656](https://issues.apache.org/jira/browse/CASSANDRA-3656) | GC can take 0 ms |  Trivial | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3554](https://issues.apache.org/jira/browse/CASSANDRA-3554) | Hints are not replayed unless node was marked down |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3155](https://issues.apache.org/jira/browse/CASSANDRA-3155) | Secondary index should report it's memory consumption |  Minor | Secondary Indexes | Jason Rutherglen | T Jake Luciani |
| [CASSANDRA-3397](https://issues.apache.org/jira/browse/CASSANDRA-3397) | Problem markers don't show up in Eclipse |  Minor | Packaging | David Allsopp | David Allsopp |
| [CASSANDRA-3686](https://issues.apache.org/jira/browse/CASSANDRA-3686) | Streaming retry is no longer performed |  Minor | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-3655](https://issues.apache.org/jira/browse/CASSANDRA-3655) | NPE when running upgradesstables |  Major | . | Tupshin Harper | Jonathan Ellis |
| [CASSANDRA-3666](https://issues.apache.org/jira/browse/CASSANDRA-3666) | Changing compaction strategy from Leveled to SizeTiered logs millions of messages about nothing to compact |  Major | . | Viktor Jevdokimov | Jonathan Ellis |
| [CASSANDRA-3531](https://issues.apache.org/jira/browse/CASSANDRA-3531) | Fix crack-smoking in ConsistencyLevelTest |  Minor | Testing | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3624](https://issues.apache.org/jira/browse/CASSANDRA-3624) | Hinted Handoff - related OOM |  Major | . | Marcus Eriksson | Jonathan Ellis |
| [CASSANDRA-3696](https://issues.apache.org/jira/browse/CASSANDRA-3696) | Adding another datacenter's node results in 0 rows returned on first datacenter |  Major | . | Joaquin Casares | Jonathan Ellis |
| [CASSANDRA-3681](https://issues.apache.org/jira/browse/CASSANDRA-3681) | Multiple threads can attempt hint handoff to the same target |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3711](https://issues.apache.org/jira/browse/CASSANDRA-3711) | Unsustainable Thread Accumulation in ParallelCompactionIterable.Reducer ThreadPoolExecutor |  Minor | . | Caleb Rackliffe | Jonathan Ellis |
| [CASSANDRA-3714](https://issues.apache.org/jira/browse/CASSANDRA-3714) | Show Schema; inserts an extra comma in column\_metadata |  Trivial | . | Joaquin Casares | Yuki Morishita |
| [CASSANDRA-3700](https://issues.apache.org/jira/browse/CASSANDRA-3700) | SelectStatement start/end key are not set correctly when a key alias is involved |  Major | CQL | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-3718](https://issues.apache.org/jira/browse/CASSANDRA-3718) | cqlsh missing help for INSERT |  Minor | Tools | paul cannon | paul cannon |
| [CASSANDRA-3693](https://issues.apache.org/jira/browse/CASSANDRA-3693) | strange values of pending tasks with compactionstats (below 0) |  Minor | Tools | Zenek Kraweznik | Jonathan Ellis |
| [CASSANDRA-3579](https://issues.apache.org/jira/browse/CASSANDRA-3579) | AssertionError in hintedhandoff - 1.0.5 |  Major | . | Ramesh Natarajan | Sylvain Lebresne |
| [CASSANDRA-3727](https://issues.apache.org/jira/browse/CASSANDRA-3727) | Fix unit tests failure |  Blocker | Testing | Sylvain Lebresne |  |
| [CASSANDRA-3733](https://issues.apache.org/jira/browse/CASSANDRA-3733) | Once a host has been hinted to, log messages for it repeat every 10 mins even if no hints are delivered |  Minor | . | Brandon Williams | Brandon Williams |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3426](https://issues.apache.org/jira/browse/CASSANDRA-3426) | clientutil -javadoc and -sources jars |  Minor | Packaging, Tools | Eric Evans | satish babu krishnamoorthy |


