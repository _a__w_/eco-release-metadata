
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.2.3 - 2013-03-18



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5280](https://issues.apache.org/jira/browse/CASSANDRA-5280) | Provide a nodetool command to repair subranges |  Trivial | . | Ahmed Bashir | Brandon Williams |
| [CASSANDRA-5291](https://issues.apache.org/jira/browse/CASSANDRA-5291) | Document how setting internode\_{send\|recv}\_buff\_size\_in\_bytes might give you worse performance |  Trivial | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-5249](https://issues.apache.org/jira/browse/CASSANDRA-5249) | Avoid allocateding SSTableBoundedScanner when the range does not intersect the sstable |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4993](https://issues.apache.org/jira/browse/CASSANDRA-4993) | Add binary protocol support to stress |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5081](https://issues.apache.org/jira/browse/CASSANDRA-5081) | Support null values in PreparedStatements parameters |  Major | . | Michaël Figuière | Sylvain Lebresne |
| [CASSANDRA-4570](https://issues.apache.org/jira/browse/CASSANDRA-4570) | Minor logging additions |  Trivial | . | Joaquin Casares | Joaquin Casares |
| [CASSANDRA-5310](https://issues.apache.org/jira/browse/CASSANDRA-5310) | New authentication module does not wok in multi datacenters in case of network outage |  Minor | . | jal | Aleksey Yeschenko |
| [CASSANDRA-5319](https://issues.apache.org/jira/browse/CASSANDRA-5319) | Add new GC Log Rotation options to cassandra-env.sh |  Trivial | Packaging | Jeremiah Jordan | Jeremiah Jordan |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5292](https://issues.apache.org/jira/browse/CASSANDRA-5292) | CQL3 shouldn't lowercase DC names for NTS |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5287](https://issues.apache.org/jira/browse/CASSANDRA-5287) | Composite comparators for super CFs broken in 1.2 |  Major | . | Tyler Hobbs | Sylvain Lebresne |
| [CASSANDRA-5299](https://issues.apache.org/jira/browse/CASSANDRA-5299) | isLocalDc() in OutboundTcpConnection class retrieves local IP in wrong way |  Major | . | Michał Michalski | Marcus Eriksson |
| [CASSANDRA-5300](https://issues.apache.org/jira/browse/CASSANDRA-5300) | Insufficient validation of UPDATE queries against counter cfs |  Major | . | Aleksey Yeschenko | Sylvain Lebresne |
| [CASSANDRA-5285](https://issues.apache.org/jira/browse/CASSANDRA-5285) | PropertyFileSnitch default DC/Rack behavior is broken in 1.2 |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5255](https://issues.apache.org/jira/browse/CASSANDRA-5255) | dsnitch severity is not correctly set for compaction info |  Minor | . | Brandon Williams | Vijay |
| [CASSANDRA-5298](https://issues.apache.org/jira/browse/CASSANDRA-5298) | MalformedObjectNameException in ConnectionMetrics for IPv6 nodes because of ":" characters |  Minor | . | Michał Michalski | Michał Michalski |
| [CASSANDRA-5181](https://issues.apache.org/jira/browse/CASSANDRA-5181) | cassandra-all 1.2.0 pom missing netty dependency |  Minor | Packaging | Carl Lerche | Sylvain Lebresne |
| [CASSANDRA-5311](https://issues.apache.org/jira/browse/CASSANDRA-5311) | Thrift CQLPreparedResult don't include type arguments (for collection in particular) |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5182](https://issues.apache.org/jira/browse/CASSANDRA-5182) | Deletable rows are sometimes not removed during compaction |  Major | . | Binh Van Nguyen | Yuki Morishita |
| [CASSANDRA-5309](https://issues.apache.org/jira/browse/CASSANDRA-5309) | Add a note to cassandra-cli "show schema" explaining that it is cql-oblivious |  Minor | Tools | Jayadevan | Aleksey Yeschenko |
| [CASSANDRA-4801](https://issues.apache.org/jira/browse/CASSANDRA-4801) | inet datatype does not work with cqlsh on windows |  Minor | Tools | Jonathan Ellis | Aleksey Yeschenko |
| [CASSANDRA-5327](https://issues.apache.org/jira/browse/CASSANDRA-5327) | Backport on-startup manifest repair to 1.2 |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5305](https://issues.apache.org/jira/browse/CASSANDRA-5305) | clqsh COPY is broken after strictening validation in 1.2.2 (quotes ints) |  Minor | Tools | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5329](https://issues.apache.org/jira/browse/CASSANDRA-5329) | Repair -pr with vnodes incompatibilty |  Major | . | julien campan | Yuki Morishita |
| [CASSANDRA-4021](https://issues.apache.org/jira/browse/CASSANDRA-4021) | CFS.scrubDataDirectories tries to delete nonexistent orphans |  Minor | . | Brandon Williams | Boris Yen |
| [CASSANDRA-5245](https://issues.apache.org/jira/browse/CASSANDRA-5245) | AnitEntropy/MerkleTree Error |  Minor | . | David Röhr | Sylvain Lebresne |
| [CASSANDRA-5334](https://issues.apache.org/jira/browse/CASSANDRA-5334) | Don't announce migrations to pre-1.2 nodes |  Major | . | Ryan McGuire | Aleksey Yeschenko |


