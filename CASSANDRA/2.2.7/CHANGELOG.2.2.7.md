
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.2.7 - 2016-07-05



### IMPORTANT ISSUES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11574](https://issues.apache.org/jira/browse/CASSANDRA-11574) | clqsh: COPY FROM throws TypeError with Cython extensions enabled |  Major | Tools | Mahafuzur Rahman | Stefania |


### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11831](https://issues.apache.org/jira/browse/CASSANDRA-11831) | Ability to disable purgeable tombstone check via startup flag |  Major | . | Ryan Svihla | Marcus Eriksson |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11552](https://issues.apache.org/jira/browse/CASSANDRA-11552) | Reduce amount of logging calls from ColumnFamilyStore.selectAndReference |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-11541](https://issues.apache.org/jira/browse/CASSANDRA-11541) | correct the java documentation for SlabAllocator and NativeAllocator |  Trivial | . | ZhaoYang | ZhaoYang |
| [CASSANDRA-10828](https://issues.apache.org/jira/browse/CASSANDRA-10828) | Allow usage of multiplier in the start value of cassandra-stress population sequence |  Trivial | Tools | Eduard Tudenhoefner | Eduard Tudenhoefner |
| [CASSANDRA-10532](https://issues.apache.org/jira/browse/CASSANDRA-10532) | Allow LWT operation on static column with only partition keys |  Major | CQL | DOAN DuyHai | Carl Yeksigian |
| [CASSANDRA-11933](https://issues.apache.org/jira/browse/CASSANDRA-11933) | Cache local ranges when calculating repair neighbors |  Major | Core | Cyril Scetbon | Mahdi Mohammadinasab |
| [CASSANDRA-11576](https://issues.apache.org/jira/browse/CASSANDRA-11576) | Add support for JNA mlockall(2) on POWER |  Minor | Core | Rei Odaira | Rei Odaira |
| [CASSANDRA-12013](https://issues.apache.org/jira/browse/CASSANDRA-12013) | Optimize the isEOF() checking in RAR |  Minor | Compaction, Local Write-Read Paths | Dikang Gu | Dikang Gu |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11553](https://issues.apache.org/jira/browse/CASSANDRA-11553) | hadoop.cql3.CqlRecordWriter does not close cluster on reconnect |  Major | Tools | Artem Aliev | Artem Aliev |
| [CASSANDRA-11474](https://issues.apache.org/jira/browse/CASSANDRA-11474) | cqlsh: COPY FROM should use regular inserts for single statement batches |  Minor | Tools | Stefania | Stefania |
| [CASSANDRA-11549](https://issues.apache.org/jira/browse/CASSANDRA-11549) | cqlsh: COPY FROM ignores NULL values in conversion |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-11621](https://issues.apache.org/jira/browse/CASSANDRA-11621) | Stack Overflow inserting value with many columns |  Major | . | Andrew Jefferson | Alex Petrov |
| [CASSANDRA-11633](https://issues.apache.org/jira/browse/CASSANDRA-11633) | cqlsh COPY FROM fails with []{} chars in UDT/tuple fields/values |  Minor | Tools | Robert Stupp | Robert Stupp |
| [CASSANDRA-11628](https://issues.apache.org/jira/browse/CASSANDRA-11628) | Fix the regression to CASSANDRA-3983 that got introduced by CASSANDRA-10679 |  Major | Tools | Wei Deng | Wei Deng |
| [CASSANDRA-9935](https://issues.apache.org/jira/browse/CASSANDRA-9935) | Repair fails with RuntimeException |  Major | . | mlowicki | Paulo Motta |
| [CASSANDRA-11642](https://issues.apache.org/jira/browse/CASSANDRA-11642) | sstabledump and sstableverify need to be added to deb packages |  Major | Tools | Attila Szucs | T Jake Luciani |
| [CASSANDRA-11630](https://issues.apache.org/jira/browse/CASSANDRA-11630) | Make cython optional in pylib/setup.py |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-11631](https://issues.apache.org/jira/browse/CASSANDRA-11631) | cqlsh COPY FROM fails for null values with non-prepared statements |  Minor | Tools | Robert Stupp | Stefania |
| [CASSANDRA-11660](https://issues.apache.org/jira/browse/CASSANDRA-11660) | Dubious call to remove in CompactionManager.java |  Minor | Compaction | Max Schaefer | Marcus Eriksson |
| [CASSANDRA-11137](https://issues.apache.org/jira/browse/CASSANDRA-11137) | JSON datetime formatting needs timezone |  Major | CQL | Stefania | Alex Petrov |
| [CASSANDRA-11502](https://issues.apache.org/jira/browse/CASSANDRA-11502) | Fix denseness and column metadata updates coming from Thrift |  Minor | Distributed Metadata | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-11510](https://issues.apache.org/jira/browse/CASSANDRA-11510) | Clustering key and secondary index |  Major | CQL, Secondary Indexes | Julien Muller | Sam Tunnicliffe |
| [CASSANDRA-11661](https://issues.apache.org/jira/browse/CASSANDRA-11661) | Cassandra 2.0 and later require Java 7u25 or later - jdk 101 |  Critical | Testing | William Boutin | Eduard Tudenhoefner |
| [CASSANDRA-9861](https://issues.apache.org/jira/browse/CASSANDRA-9861) | When forcibly exiting due to OOM, we should produce a heap dump |  Minor | Lifecycle | Benedict | Benjamin Lerer |
| [CASSANDRA-11626](https://issues.apache.org/jira/browse/CASSANDRA-11626) | cqlsh fails and exits on non-ascii chars |  Minor | Tools | Robert Stupp | Tyler Hobbs |
| [CASSANDRA-10962](https://issues.apache.org/jira/browse/CASSANDRA-10962) | Cassandra should not create snapshot at restart for compactions\_in\_progress |  Minor | Lifecycle | FACORAT | Alex Petrov |
| [CASSANDRA-11540](https://issues.apache.org/jira/browse/CASSANDRA-11540) | The JVM should exit if jmx fails to bind |  Major | Core | Brandon Williams | Alex Petrov |
| [CASSANDRA-9395](https://issues.apache.org/jira/browse/CASSANDRA-9395) | Prohibit Counter type as part of the PK |  Major | CQL | Sebastian Estevez | Brett Snyder |
| [CASSANDRA-11737](https://issues.apache.org/jira/browse/CASSANDRA-11737) | Add a way to disable severity in DynamicEndpointSnitch |  Major | . | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-11644](https://issues.apache.org/jira/browse/CASSANDRA-11644) | gc\_warn\_threshold\_in\_ms is not applied when it's greater than MIN\_LOG\_DURATION(200ms) |  Minor | . | ZhaoYang | ZhaoYang |
| [CASSANDRA-11753](https://issues.apache.org/jira/browse/CASSANDRA-11753) | cqlsh show sessions truncates time\_elapsed values \> 999999 |  Major | CQL, Observability, Testing, Tools | Jonathan Shook | Stefania |
| [CASSANDRA-11834](https://issues.apache.org/jira/browse/CASSANDRA-11834) | Don't compute expensive MaxPurgeableTimestamp until we've verified there's an expired tombstone |  Minor | Compaction | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-11855](https://issues.apache.org/jira/browse/CASSANDRA-11855) | MessagingService#getCommandDroppedTasks should be displayed in netstats |  Major | Tools | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-11272](https://issues.apache.org/jira/browse/CASSANDRA-11272) | NullPointerException (NPE) during bootstrap startup in StorageService.java |  Major | Lifecycle | Jason Kania | Alex Petrov |
| [CASSANDRA-11739](https://issues.apache.org/jira/browse/CASSANDRA-11739) | Cache key references might cause OOM on incremental repair |  Major | Streaming and Messaging | Paulo Motta | Paulo Motta |
| [CASSANDRA-11867](https://issues.apache.org/jira/browse/CASSANDRA-11867) | Possible memory leak in NIODataInputStream |  Minor | Local Write-Read Paths | Robert Stupp | Robert Stupp |
| [CASSANDRA-11708](https://issues.apache.org/jira/browse/CASSANDRA-11708) | sstableloader not work with ssl options |  Minor | Tools | Bin Chen | Yuki Morishita |
| [CASSANDRA-11848](https://issues.apache.org/jira/browse/CASSANDRA-11848) | replace address can "succeed" without actually streaming anything |  Major | Streaming and Messaging | Jeremiah Jordan | Paulo Motta |
| [CASSANDRA-11824](https://issues.apache.org/jira/browse/CASSANDRA-11824) | If repair fails no way to run repair again |  Major | . | T Jake Luciani | Marcus Eriksson |
| [CASSANDRA-11750](https://issues.apache.org/jira/browse/CASSANDRA-11750) | Offline scrub should not abort when it hits corruption |  Minor | Tools | Adam Hattrell | Yuki Morishita |
| [CASSANDRA-11743](https://issues.apache.org/jira/browse/CASSANDRA-11743) | Race condition in CommitLog.recover can prevent startup |  Major | Lifecycle | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-9669](https://issues.apache.org/jira/browse/CASSANDRA-9669) | If sstable flushes complete out of order, on restart we can fail to replay necessary commit log records |  Critical | Local Write-Read Paths | Benedict | Branimir Lambov |
| [CASSANDRA-11711](https://issues.apache.org/jira/browse/CASSANDRA-11711) | testJsonThreadSafety is failing / flapping |  Minor | Testing | Alex Petrov | Tyler Hobbs |
| [CASSANDRA-11587](https://issues.apache.org/jira/browse/CASSANDRA-11587) | Cfstats estimate number of keys should return 0 for empty table |  Trivial | Tools | Jane Deng | Mahdi Mohammadinasab |
| [CASSANDRA-11152](https://issues.apache.org/jira/browse/CASSANDRA-11152) | SOURCE command in CQLSH 3.2 requires that "use keyspace" is in the cql file that you are sourcing |  Major | Tools | Francesco Animali | Stefania |
| [CASSANDRA-11664](https://issues.apache.org/jira/browse/CASSANDRA-11664) | Tab completion in cqlsh doesn't work for capitalized letters |  Minor | Tools | J.B. Langston | Mahdi Mohammadinasab |
| [CASSANDRA-11742](https://issues.apache.org/jira/browse/CASSANDRA-11742) | Failed bootstrap results in exception when node is restarted |  Minor | Lifecycle | Tommy Stendahl | Joel Knighton |
| [CASSANDRA-11884](https://issues.apache.org/jira/browse/CASSANDRA-11884) | dtest failure in secondary\_indexes\_test.TestSecondaryIndexesOnCollections.test\_tuple\_indexes |  Major | Secondary Indexes | Sean McCarthy | Branimir Lambov |
| [CASSANDRA-11984](https://issues.apache.org/jira/browse/CASSANDRA-11984) | StorageService shutdown hook should use a volatile variable |  Major | Core | Edward Capriolo | Edward Capriolo |
| [CASSANDRA-11749](https://issues.apache.org/jira/browse/CASSANDRA-11749) | CQLSH gets SSL exception following a COPY FROM |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-11886](https://issues.apache.org/jira/browse/CASSANDRA-11886) | Streaming will miss sections for early opened sstables during compaction |  Critical | . | Stefan Podkowinski | Marcus Eriksson |
| [CASSANDRA-11948](https://issues.apache.org/jira/browse/CASSANDRA-11948) | Wrong ByteBuffer comparisons in TimeTypeTest |  Minor | Testing | Rei Odaira | Rei Odaira |
| [CASSANDRA-11038](https://issues.apache.org/jira/browse/CASSANDRA-11038) | Is node being restarted treated as node joining? |  Minor | Distributed Metadata | cheng ren | Sam Tunnicliffe |
| [CASSANDRA-9842](https://issues.apache.org/jira/browse/CASSANDRA-9842) | Inconsistent behavior for '= null' conditions on static columns |  Major | CQL | Chandra Sekar | Alex Petrov |
| [CASSANDRA-11920](https://issues.apache.org/jira/browse/CASSANDRA-11920) | bloom\_filter\_fp\_chance needs to be validated up front |  Minor | Lifecycle, Local Write-Read Paths | ADARSH KUMAR | Arindam Gupta |
| [CASSANDRA-11991](https://issues.apache.org/jira/browse/CASSANDRA-11991) | On clock skew, paxos may "corrupt" the node clock |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-11913](https://issues.apache.org/jira/browse/CASSANDRA-11913) | BufferUnderFlowException in CompressorTest |  Minor | Testing | Rei Odaira | Rei Odaira |
| [CASSANDRA-11882](https://issues.apache.org/jira/browse/CASSANDRA-11882) | Clustering Key with ByteBuffer size \> 64k throws Assertion Error |  Major | CQL, Streaming and Messaging | Lerh Chuan Low | Lerh Chuan Low |
| [CASSANDRA-11696](https://issues.apache.org/jira/browse/CASSANDRA-11696) | Incremental repairs can mark too many ranges as repaired |  Critical | Streaming and Messaging | Joel Knighton | Marcus Eriksson |
| [CASSANDRA-12077](https://issues.apache.org/jira/browse/CASSANDRA-12077) | NPE when trying to get sstables for anticompaction |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-11854](https://issues.apache.org/jira/browse/CASSANDRA-11854) | Remove finished streaming connections from MessagingService |  Major | Streaming and Messaging | Paulo Motta | Paulo Motta |
| [CASSANDRA-12043](https://issues.apache.org/jira/browse/CASSANDRA-12043) | Syncing most recent commit in CAS across replicas can cause all CAS queries in the CQL partition to fail |  Major | . | sankalp kohli | Sylvain Lebresne |
| [CASSANDRA-10391](https://issues.apache.org/jira/browse/CASSANDRA-10391) | sstableloader fails with client SSL enabled with non-standard keystore/truststore location |  Major | Tools | Jon Moses | Andrew Hust |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11505](https://issues.apache.org/jira/browse/CASSANDRA-11505) | dtest failure in cqlsh\_tests.cqlsh\_copy\_tests.CqlshCopyTest.test\_reading\_max\_parse\_errors |  Major | Tools | Michael Shuler | Stefania |
| [CASSANDRA-11799](https://issues.apache.org/jira/browse/CASSANDRA-11799) | dtest failure in cqlsh\_tests.cqlsh\_tests.TestCqlsh.test\_unicode\_syntax\_error |  Major | Testing | Philip Thompson | Tyler Hobbs |
| [CASSANDRA-9039](https://issues.apache.org/jira/browse/CASSANDRA-9039) | CommitLog compressed configuration not run in several unit tests |  Major | Local Write-Read Paths | Ariel Weisberg | Benjamin Lerer |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11840](https://issues.apache.org/jira/browse/CASSANDRA-11840) | Set a more conservative default to streaming\_socket\_timeout\_in\_ms |  Major | Streaming and Messaging | Paulo Motta | Paulo Motta |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10853](https://issues.apache.org/jira/browse/CASSANDRA-10853) | deb package migration to dh\_python2 |  Major | Packaging | Michael Shuler | Michael Shuler |


