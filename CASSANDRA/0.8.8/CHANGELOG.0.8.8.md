
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.8.8 - 2011-12-02



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3185](https://issues.apache.org/jira/browse/CASSANDRA-3185) | Unify support across different map/reduce related classes for comma seperated list of hosts for initial thrift port connection. |  Minor | . | Eldon Stegall | Eldon Stegall |
| [CASSANDRA-3280](https://issues.apache.org/jira/browse/CASSANDRA-3280) | Pig Storage Handler: Add \>=0.8.1 types, Guess right type for Key in Schema |  Major | . | Steeve Morin |  |
| [CASSANDRA-3170](https://issues.apache.org/jira/browse/CASSANDRA-3170) | Schema versions output should be on separate lines for separate versions |  Minor | . | Jeremy Hanna | Pavel Yaskevich |
| [CASSANDRA-3386](https://issues.apache.org/jira/browse/CASSANDRA-3386) | Avoid lock contention in hint rows |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2855](https://issues.apache.org/jira/browse/CASSANDRA-2855) | Skip rows with empty columns when slicing entire row |  Minor | CQL | Jeremy Hanna | T Jake Luciani |
| [CASSANDRA-3214](https://issues.apache.org/jira/browse/CASSANDRA-3214) | Make CFIF use rpc\_endpoint prior to trying endpoint |  Minor | . | Eldon Stegall | Eldon Stegall |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3297](https://issues.apache.org/jira/browse/CASSANDRA-3297) | truncate can still result in data being replayed after a restart |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2863](https://issues.apache.org/jira/browse/CASSANDRA-2863) | NPE when writing SSTable generated via repair |  Major | . | Héctor Izquierdo | Sylvain Lebresne |
| [CASSANDRA-3349](https://issues.apache.org/jira/browse/CASSANDRA-3349) | NPE on malformed CQL |  Major | . | paul cannon | Pavel Yaskevich |
| [CASSANDRA-3350](https://issues.apache.org/jira/browse/CASSANDRA-3350) | Can't USE numeric keyspace names in CQL |  Minor | . | paul cannon | Pavel Yaskevich |
| [CASSANDRA-3320](https://issues.apache.org/jira/browse/CASSANDRA-3320) | pig\_cassandra script errors when running against pig 0.9.1 tar ball because there are multiple jars. |  Minor | . | Brian ONeill | Brian ONeill |
| [CASSANDRA-3358](https://issues.apache.org/jira/browse/CASSANDRA-3358) | 2GB row size limit in ColumnIndex offset calculation |  Major | . | Thomas Richter | Thomas Richter |
| [CASSANDRA-3357](https://issues.apache.org/jira/browse/CASSANDRA-3357) | SSTableImport/Export don't handle tombstone well if value validation != BytesType |  Trivial | Tools | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3344](https://issues.apache.org/jira/browse/CASSANDRA-3344) | Compaction throttling can be too slow |  Minor | . | Fabien Rousseau | Sylvain Lebresne |
| [CASSANDRA-3342](https://issues.apache.org/jira/browse/CASSANDRA-3342) | cassandra-cli allows setting min\_compaction\_threshold to 1 |  Major | Tools | Jason Baker | Pavel Yaskevich |
| [CASSANDRA-3126](https://issues.apache.org/jira/browse/CASSANDRA-3126) | unable to remove column metadata via CLI |  Minor | Tools | Radim Kolar | Pavel Yaskevich |
| [CASSANDRA-3292](https://issues.apache.org/jira/browse/CASSANDRA-3292) | creating column family sets durable\_writes to true |  Minor | . | Radim Kolar | Jonathan Ellis |
| [CASSANDRA-3377](https://issues.apache.org/jira/browse/CASSANDRA-3377) | correct dropped messages logging |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3384](https://issues.apache.org/jira/browse/CASSANDRA-3384) | it would be nice if "describe keyspace" in cli shows "Cache Provider" |  Minor | Tools | Vijay | Pavel Yaskevich |
| [CASSANDRA-3369](https://issues.apache.org/jira/browse/CASSANDRA-3369) | AssertionError when adding a node and doing repair, repair hangs |  Major | . | Christian Spriegel | Sylvain Lebresne |
| [CASSANDRA-3391](https://issues.apache.org/jira/browse/CASSANDRA-3391) | CFM.toAvro() incorrectly serialises key\_validation\_class defn |  Minor | . | amorton | amorton |
| [CASSANDRA-3394](https://issues.apache.org/jira/browse/CASSANDRA-3394) | AssertionError in PrecompactedRow.write via CommutativeRowIndexer during bootstrap |  Major | . | paul cannon | Sylvain Lebresne |
| [CASSANDRA-3351](https://issues.apache.org/jira/browse/CASSANDRA-3351) | If node fails to join a ring it will stay in joining state indefinately |  Trivial | . | Tuukka Luolamo | Brandon Williams |
| [CASSANDRA-3390](https://issues.apache.org/jira/browse/CASSANDRA-3390) | ReadResponseSerializer.serializedSize() calculation is wrong |  Major | . | Yang Yang | Jonathan Ellis |
| [CASSANDRA-3400](https://issues.apache.org/jira/browse/CASSANDRA-3400) | ConcurrentModificationException during nodetool repair |  Minor | . | Scott Fines | Sylvain Lebresne |
| [CASSANDRA-3414](https://issues.apache.org/jira/browse/CASSANDRA-3414) | Not possible to change row\_cache\_provider on existing cf |  Minor | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-3405](https://issues.apache.org/jira/browse/CASSANDRA-3405) | Row cache provider reported wrong in cassandra-cli |  Minor | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-3399](https://issues.apache.org/jira/browse/CASSANDRA-3399) | Truncate disregards running compactions when deleting sstables |  Major | . | Sylvain Lebresne | Jonathan Ellis |
| [CASSANDRA-3450](https://issues.apache.org/jira/browse/CASSANDRA-3450) | maybeInit in ColumnFamilyRecordReader can cause rows to be empty but not null |  Major | . | Lanny Ripple | Lanny Ripple |
| [CASSANDRA-3178](https://issues.apache.org/jira/browse/CASSANDRA-3178) | Counter shard merging is not thread safe |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3472](https://issues.apache.org/jira/browse/CASSANDRA-3472) | Actually uses efficient cross DC writes |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3186](https://issues.apache.org/jira/browse/CASSANDRA-3186) | nodetool should not NPE when rack/dc info is not yet available |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3519](https://issues.apache.org/jira/browse/CASSANDRA-3519) | ConcurrentModificationException in FailureDetector |  Minor | . | amorton | amorton |
| [CASSANDRA-3514](https://issues.apache.org/jira/browse/CASSANDRA-3514) | CounterColumnFamily Compaction error (ArrayIndexOutOfBoundsException) |  Major | . | Eric Falcao | Sylvain Lebresne |
| [CASSANDRA-2786](https://issues.apache.org/jira/browse/CASSANDRA-2786) | After a minor compaction, deleted key-slices are visible again |  Major | . | rene kochen | Sylvain Lebresne |
| [CASSANDRA-3520](https://issues.apache.org/jira/browse/CASSANDRA-3520) | Unit test are hanging on 0.8 branch |  Major | Testing | Sylvain Lebresne | Sylvain Lebresne |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3196](https://issues.apache.org/jira/browse/CASSANDRA-3196) | Cassandra-CLI command should have --version option |  Minor | Tools | Mauri Tikka | Pavel Yaskevich |


