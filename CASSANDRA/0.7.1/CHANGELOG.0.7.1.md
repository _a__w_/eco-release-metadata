
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.7.1 - 2011-02-14



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1453](https://issues.apache.org/jira/browse/CASSANDRA-1453) | stress.java |  Minor | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-982](https://issues.apache.org/jira/browse/CASSANDRA-982) | read repair on quorum consistencylevel |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1963](https://issues.apache.org/jira/browse/CASSANDRA-1963) | Allow user to specify files to compact through JMX |  Minor | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-1654](https://issues.apache.org/jira/browse/CASSANDRA-1654) | EC2Snitch |  Major | . | Jonathan Ellis | Brandon Williams |
| [CASSANDRA-2012](https://issues.apache.org/jira/browse/CASSANDRA-2012) | handshake protocol version during connection setup. |  Major | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-1108](https://issues.apache.org/jira/browse/CASSANDRA-1108) | ability to forcibly mark machines failed |  Minor | Tools | Jonathan Ellis | Brandon Williams |
| [CASSANDRA-1551](https://issues.apache.org/jira/browse/CASSANDRA-1551) | create "tell me what nodes you have hints for" jmx api |  Minor | Tools | Jonathan Ellis | Jon Hermes |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1886](https://issues.apache.org/jira/browse/CASSANDRA-1886) | [patch] Make sure sterams get closed |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-1525](https://issues.apache.org/jira/browse/CASSANDRA-1525) | Improve log4j configuration to provide the ability to modify logging levels dynamically via JMX |  Major | . | Nate McCall | T Jake Luciani |
| [CASSANDRA-1470](https://issues.apache.org/jira/browse/CASSANDRA-1470) | Avoid polluting page cache during compaction |  Major | . | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1555](https://issues.apache.org/jira/browse/CASSANDRA-1555) | Considerations for larger bloom filters |  Major | . | Stu Hood | Ryan King |
| [CASSANDRA-1822](https://issues.apache.org/jira/browse/CASSANDRA-1822) | Row level coverage in LegacySSTableTest |  Minor | . | Stu Hood | Stu Hood |
| [CASSANDRA-1921](https://issues.apache.org/jira/browse/CASSANDRA-1921) | Add jmx password authentication to nodetool |  Major | Tools | mck | mck |
| [CASSANDRA-1374](https://issues.apache.org/jira/browse/CASSANDRA-1374) | Make snitches configurable at runtime |  Major | . | Jeremy Hanna | Jon Hermes |
| [CASSANDRA-1928](https://issues.apache.org/jira/browse/CASSANDRA-1928) | implement ByteOrderedPartitioner.describeOwnership(..) |  Major | . | mck | mck |
| [CASSANDRA-1926](https://issues.apache.org/jira/browse/CASSANDRA-1926) | Add Ant target to generate the project description files for running Cassandra in Eclipse |  Minor | . | Zhijie Shen | T Jake Luciani |
| [CASSANDRA-1897](https://issues.apache.org/jira/browse/CASSANDRA-1897) | Replace lib/guava-r05.jar with version from maven central repository |  Major | Packaging | Stephen Connolly | Stephen Connolly |
| [CASSANDRA-1896](https://issues.apache.org/jira/browse/CASSANDRA-1896) | Improve throughput by adding buffering to the inter-node TCP communication |  Major | . | Tsiki | Brandon Williams |
| [CASSANDRA-1888](https://issues.apache.org/jira/browse/CASSANDRA-1888) | Replace lib/high-scale-lib.jar with equivalent from maven central repository |  Major | . | Stephen Connolly | Stephen Connolly |
| [CASSANDRA-1930](https://issues.apache.org/jira/browse/CASSANDRA-1930) | db.Table flusherLock write lock fairness policy is sub-optimal |  Major | . | Kelvin Kakugawa | Kelvin Kakugawa |
| [CASSANDRA-1891](https://issues.apache.org/jira/browse/CASSANDRA-1891) | large supercolumn deserialization invokes CSLM worst case scenario |  Minor | . | Cliff Moon | Cliff Moon |
| [CASSANDRA-1857](https://issues.apache.org/jira/browse/CASSANDRA-1857) | nodetool has "invalidaterowcache" but no "invalidatekeycache" |  Trivial | Tools | Robert Coli | Jon Hermes |
| [CASSANDRA-1714](https://issues.apache.org/jira/browse/CASSANDRA-1714) | zero-copy reads |  Major | . | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1935](https://issues.apache.org/jira/browse/CASSANDRA-1935) | Refuse to open SSTables from the future |  Minor | . | Stu Hood | Ryan King |
| [CASSANDRA-1977](https://issues.apache.org/jira/browse/CASSANDRA-1977) | Remove contention around MD5 generation in GuidGenerator |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-2009](https://issues.apache.org/jira/browse/CASSANDRA-2009) | Move relevant methods to ByteBufferUtil (and normalize on names) |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-1968](https://issues.apache.org/jira/browse/CASSANDRA-1968) | Increase JVM young generation size |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-1565](https://issues.apache.org/jira/browse/CASSANDRA-1565) | cassandra-cli use consistant flags and improve error messages |  Minor | Tools | Ian Rogers | Christoph Hack |
| [CASSANDRA-2011](https://issues.apache.org/jira/browse/CASSANDRA-2011) | Add some buffer verbs to StorageService |  Major | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-1459](https://issues.apache.org/jira/browse/CASSANDRA-1459) | Allow modification of HintedHandoff configuration to be changed on the fly per node. |  Major | Tools | Jeff Davey | Brandon Williams |
| [CASSANDRA-2029](https://issues.apache.org/jira/browse/CASSANDRA-2029) | describe\_keyspace in CLI should default to the currently set keyspace if there is one, otherwise keep current behavior |  Minor | Tools | Matthew F. Dennis | Pavel Yaskevich |
| [CASSANDRA-526](https://issues.apache.org/jira/browse/CASSANDRA-526) | Operations: Allow the ability to start a node without joining the ring |  Minor | . | Chris Goffinet | Sylvain Lebresne |
| [CASSANDRA-2038](https://issues.apache.org/jira/browse/CASSANDRA-2038) | Add local read path back to StorageProxy |  Major | . | T Jake Luciani | Jonathan Ellis |
| [CASSANDRA-2030](https://issues.apache.org/jira/browse/CASSANDRA-2030) | cassandra-cli batch mode does not provide an option to display output |  Minor | Tools | Matthew F. Dennis | Pavel Yaskevich |
| [CASSANDRA-2032](https://issues.apache.org/jira/browse/CASSANDRA-2032) | CLI should accept # (and/or some other char) as a comment character in files passed with -f |  Minor | Tools | Matthew F. Dennis | Pavel Yaskevich |
| [CASSANDRA-1898](https://issues.apache.org/jira/browse/CASSANDRA-1898) | json2sstable should support streaming |  Major | Tools | Nick Bailey | Pavel Yaskevich |
| [CASSANDRA-2017](https://issues.apache.org/jira/browse/CASSANDRA-2017) | Replace ivy withmaven-ant-tasks |  Major | . | Stephen Connolly | Stephen Connolly |
| [CASSANDRA-1900](https://issues.apache.org/jira/browse/CASSANDRA-1900) | Make removetoken force always work |  Major | Tools | Nick Bailey | Brandon Williams |
| [CASSANDRA-1919](https://issues.apache.org/jira/browse/CASSANDRA-1919) | Add shutdownhook to flush commitlog |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1951](https://issues.apache.org/jira/browse/CASSANDRA-1951) | offline local nodes |  Minor | . | Gary Dusbabek | Sylvain Lebresne |
| [CASSANDRA-2098](https://issues.apache.org/jira/browse/CASSANDRA-2098) | Add description to nodetool commands |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2114](https://issues.apache.org/jira/browse/CASSANDRA-2114) | make it easier to have Cassandra JVM listen for remote debuggers |  Minor | . | Matthew F. Dennis | Matthew F. Dennis |
| [CASSANDRA-2091](https://issues.apache.org/jira/browse/CASSANDRA-2091) | Make BBU.string validate input for the desired Charset |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1530](https://issues.apache.org/jira/browse/CASSANDRA-1530) | More-efficient cross-DC replication |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2135](https://issues.apache.org/jira/browse/CASSANDRA-2135) | Add the ability to enable/disable Thrift through nodetool |  Trivial | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-2138](https://issues.apache.org/jira/browse/CASSANDRA-2138) | add option to enable efficient cross-dc replication |  Minor | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-2142](https://issues.apache.org/jira/browse/CASSANDRA-2142) | Add "reduce memory usage because I tuned things poorly" feature |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2127](https://issues.apache.org/jira/browse/CASSANDRA-2127) | CLI should support describe\_schema\_versions() and other informative Thrift calls |  Minor | Tools | Tyler Hobbs | Pavel Yaskevich |
| [CASSANDRA-2149](https://issues.apache.org/jira/browse/CASSANDRA-2149) | Allow a file of hosts to be passed to stress.java |  Trivial | . | Nick Bailey | Matthew F. Dennis |
| [CASSANDRA-2041](https://issues.apache.org/jira/browse/CASSANDRA-2041) | add paging of large rows to sstable2json |  Minor | Tools | Jonathan Ellis | Pavel Yaskevich |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1907](https://issues.apache.org/jira/browse/CASSANDRA-1907) | AbstractCassandraDaemon blows up when log4j config is specified using a physical file. |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-1915](https://issues.apache.org/jira/browse/CASSANDRA-1915) | stress.java doesn't actually read its data |  Major | . | Brandon Williams | Pavel Yaskevich |
| [CASSANDRA-1916](https://issues.apache.org/jira/browse/CASSANDRA-1916) | Cleanup needs to remove secondary index entries |  Major | Secondary Indexes | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1927](https://issues.apache.org/jira/browse/CASSANDRA-1927) | Hadoop Integration doesn't work when one node is down |  Major | . | Utku Can Topcu | mck |
| [CASSANDRA-1931](https://issues.apache.org/jira/browse/CASSANDRA-1931) | Internal error processing insert java.lang.AssertionError  at org.apache.cassandra.service.StorageProxy.sendMessages(StorageProxy.java:219) |  Major | . | Karl Mueller | T Jake Luciani |
| [CASSANDRA-1943](https://issues.apache.org/jira/browse/CASSANDRA-1943) | Addition of internode buffering broke Streaming |  Critical | . | Stu Hood |  |
| [CASSANDRA-1962](https://issues.apache.org/jira/browse/CASSANDRA-1962) | Equality problem in schema updates |  Minor | . | Stu Hood | Stu Hood |
| [CASSANDRA-1959](https://issues.apache.org/jira/browse/CASSANDRA-1959) | java.lang.ArrayIndexOutOfBoundsException while executing repair on a freshly added node (0.7.0) |  Major | . | Thibaut | Jonathan Ellis |
| [CASSANDRA-1972](https://issues.apache.org/jira/browse/CASSANDRA-1972) | Default concurrency values are improperly proportioned |  Minor | . | Stu Hood | Stu Hood |
| [CASSANDRA-1986](https://issues.apache.org/jira/browse/CASSANDRA-1986) | write CL \> ONE regression |  Major | . | Kelvin Kakugawa | Kelvin Kakugawa |
| [CASSANDRA-1985](https://issues.apache.org/jira/browse/CASSANDRA-1985) | read repair on CL.ONE regression |  Major | . | Kelvin Kakugawa | Jonathan Ellis |
| [CASSANDRA-1993](https://issues.apache.org/jira/browse/CASSANDRA-1993) | Word count example doesn't output the words correctly to cassandra.  It outputs spurious data past the length of the byte array. |  Minor | . | Jesse Shieh | Jesse Shieh |
| [CASSANDRA-1895](https://issues.apache.org/jira/browse/CASSANDRA-1895) | Loadbalance during gossip issues leaves cluster in bad state |  Minor | . | Stu Hood | Brandon Williams |
| [CASSANDRA-1995](https://issues.apache.org/jira/browse/CASSANDRA-1995) | cassandra-cli doesn't accept 'name' as a column name in column metadata when creating a column family |  Minor | Tools | Tyler Hobbs | Pavel Yaskevich |
| [CASSANDRA-1979](https://issues.apache.org/jira/browse/CASSANDRA-1979) | CassandraServiceDataCleaner.prepare() fails with IOException. |  Major | . | Chris Ng | Nate McCall |
| [CASSANDRA-1999](https://issues.apache.org/jira/browse/CASSANDRA-1999) | Fix misuses of ByteBufferUtil.string() |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2000](https://issues.apache.org/jira/browse/CASSANDRA-2000) | Table comments indicates expiry checking happens 10x times per minimum interval, but doesn't |  Trivial | . | Peter Schuller | Peter Schuller |
| [CASSANDRA-1973](https://issues.apache.org/jira/browse/CASSANDRA-1973) | stress.java -k doesn't keep going |  Trivial | . | Brandon Williams | Pavel Yaskevich |
| [CASSANDRA-1976](https://issues.apache.org/jira/browse/CASSANDRA-1976) | SSTableWriter.writeStatistics is serializing incorrect data. |  Minor | . | Gary Dusbabek | Jonathan Ellis |
| [CASSANDRA-1992](https://issues.apache.org/jira/browse/CASSANDRA-1992) | Bootstrap breaks data stored (missing rows, extra rows, column values modified) |  Major | . | Mateusz Korniak | Brandon Williams |
| [CASSANDRA-1964](https://issues.apache.org/jira/browse/CASSANDRA-1964) | MutationTest of the distributed-test suite fails |  Minor | . | Michael Allen | Stu Hood |
| [CASSANDRA-2010](https://issues.apache.org/jira/browse/CASSANDRA-2010) | Error when read repair is disabled |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2016](https://issues.apache.org/jira/browse/CASSANDRA-2016) | Files added with missing license headers |  Blocker | . | Stephen Connolly | Stephen Connolly |
| [CASSANDRA-1718](https://issues.apache.org/jira/browse/CASSANDRA-1718) | cassandra should chdir / when daemonizing |  Minor | Packaging | paul cannon | Eric Evans |
| [CASSANDRA-1934](https://issues.apache.org/jira/browse/CASSANDRA-1934) | Update token metadata for NORMAL state |  Minor | . | Nick Bailey | Brandon Williams |
| [CASSANDRA-2031](https://issues.apache.org/jira/browse/CASSANDRA-2031) | CLI chokes on whitespace after semicolon when using -f |  Minor | Tools | Matthew F. Dennis | Pavel Yaskevich |
| [CASSANDRA-2036](https://issues.apache.org/jira/browse/CASSANDRA-2036) | cassandra-topology.properties cannot reside inside jar file |  Major | . | mck | mck |
| [CASSANDRA-2023](https://issues.apache.org/jira/browse/CASSANDRA-2023) | fix regression in 1968 (young gen sizing logic) |  Major | Packaging | Peter Schuller | Peter Schuller |
| [CASSANDRA-2046](https://issues.apache.org/jira/browse/CASSANDRA-2046) | ivy.jar is included in the binary distribution |  Major | . | Stephen Connolly | Eric Evans |
| [CASSANDRA-2044](https://issues.apache.org/jira/browse/CASSANDRA-2044) | CLI should loop on describe\_schema until agreement or fatel exit with stacktrace/message if no agreement after X seconds |  Major | . | Matthew F. Dennis | Pavel Yaskevich |
| [CASSANDRA-2051](https://issues.apache.org/jira/browse/CASSANDRA-2051) | Fixes for multi-datacenter writes |  Major | . | Jonathan Ellis | ivan |
| [CASSANDRA-2057](https://issues.apache.org/jira/browse/CASSANDRA-2057) | overflow in NodeCmd |  Minor | Tools | Ryan King | Ryan King |
| [CASSANDRA-2049](https://issues.apache.org/jira/browse/CASSANDRA-2049) | On the CLI, creating or updating a keyspace to use the NetworkTopologyStrategy breaks "show keyspaces;" |  Major | . | Jeremy Hanna | Pavel Yaskevich |
| [CASSANDRA-2066](https://issues.apache.org/jira/browse/CASSANDRA-2066) | 2 (more) Misuses of ByteBuffer relative gets |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2063](https://issues.apache.org/jira/browse/CASSANDRA-2063) | bug with test |  Minor | . | Amit Cahanovich | Eric Evans |
| [CASSANDRA-2071](https://issues.apache.org/jira/browse/CASSANDRA-2071) | RP.describeOwnership() does some bad math |  Minor | . | Jon Hermes | Jon Hermes |
| [CASSANDRA-2059](https://issues.apache.org/jira/browse/CASSANDRA-2059) | SSTableDeletingReference only deletes data files |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2073](https://issues.apache.org/jira/browse/CASSANDRA-2073) | Streaming occasionally makes gossip back up |  Minor | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-2094](https://issues.apache.org/jira/browse/CASSANDRA-2094) | fix regression in CL.ALL read |  Major | . | Kelvin Kakugawa | Kelvin Kakugawa |
| [CASSANDRA-2072](https://issues.apache.org/jira/browse/CASSANDRA-2072) | Race condition during decommission |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-2083](https://issues.apache.org/jira/browse/CASSANDRA-2083) | Hinted Handoff and schema race |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-2053](https://issues.apache.org/jira/browse/CASSANDRA-2053) | Make cache saving less contentious |  Major | . | Nick Bailey | Jonathan Ellis |
| [CASSANDRA-2081](https://issues.apache.org/jira/browse/CASSANDRA-2081) | Consistency QUORUM does not work anymore (hector:Could not fullfill request on this host) |  Critical | . | Thibaut | amorton |
| [CASSANDRA-2131](https://issues.apache.org/jira/browse/CASSANDRA-2131) | Illegal file mode when saving caches |  Major | . | amorton | amorton |
| [CASSANDRA-2096](https://issues.apache.org/jira/browse/CASSANDRA-2096) | InternalException on system\_update\_column\_family if column\_metadata is not assigned |  Minor | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-2134](https://issues.apache.org/jira/browse/CASSANDRA-2134) | Error in ThreadPoolExecutor |  Major | . | Patrik Modesto | Sylvain Lebresne |
| [CASSANDRA-2121](https://issues.apache.org/jira/browse/CASSANDRA-2121) | stress.java cardinality option parsing typo |  Minor | . | Chris Burroughs | Chris Burroughs |
| [CASSANDRA-2123](https://issues.apache.org/jira/browse/CASSANDRA-2123) | nodetool cfhistograms write/read latency columns are reversed |  Minor | Tools | Chris Burroughs | Jonathan Ellis |
| [CASSANDRA-2148](https://issues.apache.org/jira/browse/CASSANDRA-2148) | system CFs default to large memtable throughputs on large heaps |  Major | . | Matthew F. Dennis | Matthew F. Dennis |
| [CASSANDRA-2111](https://issues.apache.org/jira/browse/CASSANDRA-2111) | cassandra-cli 'use Keyspace user pass' breaks with SimpleAuth |  Trivial | Tools | Tyler Hobbs | Pavel Yaskevich |
| [CASSANDRA-2076](https://issues.apache.org/jira/browse/CASSANDRA-2076) | Not restarting due to Invalid saved cache |  Critical | . | Thibaut | Matthew F. Dennis |
| [CASSANDRA-2150](https://issues.apache.org/jira/browse/CASSANDRA-2150) | sstablekeys silently ignores extra arguments |  Trivial | Tools | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-2102](https://issues.apache.org/jira/browse/CASSANDRA-2102) | saved row cache doesn't save the cache |  Major | . | Matthew F. Dennis | Matthew F. Dennis |
| [CASSANDRA-2058](https://issues.apache.org/jira/browse/CASSANDRA-2058) | Load spikes due to MessagingService-generated garbage collection |  Major | . | David King | Jonathan Ellis |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1859](https://issues.apache.org/jira/browse/CASSANDRA-1859) | distributed test harness |  Major | Tools | Kelvin Kakugawa | Kelvin Kakugawa |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1923](https://issues.apache.org/jira/browse/CASSANDRA-1923) | unit tests that validate that message serialization isn't broken in the current version. |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-1970](https://issues.apache.org/jira/browse/CASSANDRA-1970) | Message version resolution |  Minor | . | Gary Dusbabek | Gary Dusbabek |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2028](https://issues.apache.org/jira/browse/CASSANDRA-2028) | Cassandra should log, at info level, when a keyspace and/or column family is created and/or updated |  Minor | . | Matthew F. Dennis | Gary Dusbabek |


