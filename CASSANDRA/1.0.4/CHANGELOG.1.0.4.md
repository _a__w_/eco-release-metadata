
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.0.4 - 2011-11-29



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3347](https://issues.apache.org/jira/browse/CASSANDRA-3347) | include bloomfilter stats in nodeCmd output |  Minor | Tools | Yang Yang | Vijay |
| [CASSANDRA-3406](https://issues.apache.org/jira/browse/CASSANDRA-3406) | Create a nodetool upgrade\_sstables to avoid using scrubs for tasks it wasn't intended to. |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3495](https://issues.apache.org/jira/browse/CASSANDRA-3495) | capture BloomFilter memory size in Cassandra (JMX) |  Minor | Tools | Jackson Chung | Vijay |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3504](https://issues.apache.org/jira/browse/CASSANDRA-3504) | Storage paths exposed in JMX should be absolute/canonical |  Trivial | . | Tyler Hobbs | Tyler Hobbs |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3496](https://issues.apache.org/jira/browse/CASSANDRA-3496) | Load from \`nodetool ring\` does not update after cleanup. |  Major | . | Benjamin Coverston | Jackson Chung |
| [CASSANDRA-3407](https://issues.apache.org/jira/browse/CASSANDRA-3407) | Changing partitioner causes interval tree build failure before the change can be detected |  Minor | . | Zhong Li | Yuki Morishita |
| [CASSANDRA-3506](https://issues.apache.org/jira/browse/CASSANDRA-3506) | cassandra.bat does not use SETLOCAL, can cause classpath issues |  Minor | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-3501](https://issues.apache.org/jira/browse/CASSANDRA-3501) | Move allows you to move to tokens \> 2\*\*127 |  Minor | Tools | Nick Bailey | Jonathan Ellis |
| [CASSANDRA-3508](https://issues.apache.org/jira/browse/CASSANDRA-3508) | requiring --debug to see stack traces for failures in cassandra-cli is a terrible idea (aka silent failure is never a valid option) |  Minor | Tools | Matthew F. Dennis | Pavel Yaskevich |
| [CASSANDRA-3510](https://issues.apache.org/jira/browse/CASSANDRA-3510) | Incorrect query results due to invalid SSTable.maxTimestamp |  Critical | . | amorton | Sylvain Lebresne |
| [CASSANDRA-3519](https://issues.apache.org/jira/browse/CASSANDRA-3519) | ConcurrentModificationException in FailureDetector |  Minor | . | amorton | amorton |
| [CASSANDRA-3514](https://issues.apache.org/jira/browse/CASSANDRA-3514) | CounterColumnFamily Compaction error (ArrayIndexOutOfBoundsException) |  Major | . | Eric Falcao | Sylvain Lebresne |
| [CASSANDRA-2786](https://issues.apache.org/jira/browse/CASSANDRA-2786) | After a minor compaction, deleted key-slices are visible again |  Major | . | rene kochen | Sylvain Lebresne |
| [CASSANDRA-3440](https://issues.apache.org/jira/browse/CASSANDRA-3440) | local writes timing out cause attempt to hint to self |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3529](https://issues.apache.org/jira/browse/CASSANDRA-3529) | ConcurrentModificationException in Table.all() |  Trivial | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3521](https://issues.apache.org/jira/browse/CASSANDRA-3521) | sstableloader throwing exceptions when loading snapshot data from compressed CFs |  Major | . | Peter Velas | Yuki Morishita |
| [CASSANDRA-3530](https://issues.apache.org/jira/browse/CASSANDRA-3530) | Header class not thread safe, but mutated by multiple threads |  Major | . | Sean Bridges | Jonathan Ellis |


