
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.6.3 - 2010-06-29



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1141](https://issues.apache.org/jira/browse/CASSANDRA-1141) | remove HintedHandOffManager.deliverAllHints |  Minor | . | Jonathan Ellis | Brandon Williams |
| [CASSANDRA-1184](https://issues.apache.org/jira/browse/CASSANDRA-1184) | clarify the units of sleep in user-visible messages within StorageService |  Trivial | . | Robert Coli | Robert Coli |
| [CASSANDRA-1027](https://issues.apache.org/jira/browse/CASSANDRA-1027) | Allow Null Predicate in Deletion |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-1212](https://issues.apache.org/jira/browse/CASSANDRA-1212) | JMX stats for bloomfilters |  Major | . | Jeff Hodges | Jeff Hodges |
| [CASSANDRA-1112](https://issues.apache.org/jira/browse/CASSANDRA-1112) | improve bootstrap token generation when insufficient data is present to estimate from keys |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1226](https://issues.apache.org/jira/browse/CASSANDRA-1226) | make DTPE handle exceptions the same way as CassandraDaemon |  Minor | . | Jonathan Ellis | Jonathan Ellis |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-997](https://issues.apache.org/jira/browse/CASSANDRA-997) | Javadoc for thrift interface is not generated |  Trivial | Documentation and Website | Hannes Wallnoefer | Hannes Wallnoefer |
| [CASSANDRA-1019](https://issues.apache.org/jira/browse/CASSANDRA-1019) | "java.net.ConnectException: Connection timed out" in MESSAGE-STREAMING-POOL:1 |  Major | . | B. Todd Burruss | Stu Hood |
| [CASSANDRA-1111](https://issues.apache.org/jira/browse/CASSANDRA-1111) | describe\_ring() throws on single node clusters and/or probably clusters without a replication factor |  Major | . | Dominic Williams | Gary Dusbabek |
| [CASSANDRA-1129](https://issues.apache.org/jira/browse/CASSANDRA-1129) | Using KeysCached="xx%" results in Key cache capacity: 1 |  Minor | . | Ran Tavory |  |
| [CASSANDRA-1152](https://issues.apache.org/jira/browse/CASSANDRA-1152) | Read operation with ConsistencyLevel.ALL throws exception |  Major | . | Yuki Morishita | Gary Dusbabek |
| [CASSANDRA-1139](https://issues.apache.org/jira/browse/CASSANDRA-1139) | batch\_mutate Deletion with column family type mismatch causes RuntimeException |  Minor | . | tholzer | Matthew F. Dennis |
| [CASSANDRA-1057](https://issues.apache.org/jira/browse/CASSANDRA-1057) | Login information stored in threads may be reused. |  Minor | . | Kenny | Kenny |
| [CASSANDRA-1146](https://issues.apache.org/jira/browse/CASSANDRA-1146) | "Expected both token and generation columns" |  Major | . | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-1118](https://issues.apache.org/jira/browse/CASSANDRA-1118) | allow overriding existing token owner with a new IP |  Minor | . | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-1160](https://issues.apache.org/jira/browse/CASSANDRA-1160) | race with insufficiently constructed Gossiper |  Minor | . | Jonathan Ellis | Matthew F. Dennis |
| [CASSANDRA-1119](https://issues.apache.org/jira/browse/CASSANDRA-1119) | detect incomplete commitlogheader |  Minor | . | Jonathan Ellis | Matthew F. Dennis |
| [CASSANDRA-1174](https://issues.apache.org/jira/browse/CASSANDRA-1174) | Debian packaging should auto-detect the JVM, not require OpenJDK |  Minor | . | Coda Hale | Coda Hale |
| [CASSANDRA-1150](https://issues.apache.org/jira/browse/CASSANDRA-1150) | pig contrib module not building, other errors |  Minor | . | Jeremy Hanna | Jeremy Hanna |
| [CASSANDRA-1169](https://issues.apache.org/jira/browse/CASSANDRA-1169) | AES makes Streaming unhappy |  Critical | . | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-1173](https://issues.apache.org/jira/browse/CASSANDRA-1173) | Debian packaging refers to now nonexistent DISCLAIMER.txt |  Trivial | . | Manish Singh | Manish Singh |
| [CASSANDRA-1172](https://issues.apache.org/jira/browse/CASSANDRA-1172) | update gc options for debian package |  Minor | . | Jonathan Ellis | Eric Evans |
| [CASSANDRA-1198](https://issues.apache.org/jira/browse/CASSANDRA-1198) | In a cluster, get\_range\_slices() does not return all the keys it should |  Major | . | Christopher Gist | Jonathan Ellis |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1181](https://issues.apache.org/jira/browse/CASSANDRA-1181) | kinder gentler compaction |  Major | . | Jonathan Ellis | Brandon Williams |


