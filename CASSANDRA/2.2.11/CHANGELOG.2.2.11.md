
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.2.11 - 2017-10-05



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13809](https://issues.apache.org/jira/browse/CASSANDRA-13809) | Make BatchlogManagerMBean.forceBatchlogReplay() blocking |  Trivial | Coordination | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-13844](https://issues.apache.org/jira/browse/CASSANDRA-13844) | sstableloader doesn't support non default storage\_port and ssl\_storage\_port |  Major | Tools | Eduard Tudenhoefner | Eduard Tudenhoefner |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13625](https://issues.apache.org/jira/browse/CASSANDRA-13625) | Remove unused cassandra.yaml setting, max\_value\_size\_in\_mb, from 2.2.9 |  Major | . | Joaquin Casares | Joaquin Casares |
| [CASSANDRA-13592](https://issues.apache.org/jira/browse/CASSANDRA-13592) | Null Pointer exception at SELECT JSON statement |  Major | CQL | Wyss Philipp | ZhaoYang |
| [CASSANDRA-13646](https://issues.apache.org/jira/browse/CASSANDRA-13646) | Bind parameters of collection types are not properly validated |  Major | . | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-13272](https://issues.apache.org/jira/browse/CASSANDRA-13272) | "nodetool bootstrap resume" does not exit |  Major | Lifecycle, Streaming and Messaging | Tom van der Woerdt | Tim Lamballais |
| [CASSANDRA-13700](https://issues.apache.org/jira/browse/CASSANDRA-13700) | Heartbeats can cause gossip information to go permanently missing on certain nodes |  Critical | Distributed Metadata | Joel Knighton | Joel Knighton |
| [CASSANDRA-11223](https://issues.apache.org/jira/browse/CASSANDRA-11223) | Queries with LIMIT filtering on clustering columns can return less rows than expected |  Major | Local Write-Read Paths | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-13067](https://issues.apache.org/jira/browse/CASSANDRA-13067) | Integer overflows with file system size reported by Amazon Elastic File System (EFS) |  Major | Core | Michael Hanselmann | Benjamin Lerer |
| [CASSANDRA-13775](https://issues.apache.org/jira/browse/CASSANDRA-13775) | CircleCI tests fail because \*stress-test\* isn't a valid target |  Major | Build | Eduard Tudenhoefner | Eduard Tudenhoefner |
| [CASSANDRA-13649](https://issues.apache.org/jira/browse/CASSANDRA-13649) | Uncaught exceptions in Netty pipeline |  Major | Streaming and Messaging, Testing | Stefan Podkowinski | Norman Maurer |
| [CASSANDRA-13807](https://issues.apache.org/jira/browse/CASSANDRA-13807) | CircleCI fix - only collect the xml file from containers where it exists |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-13833](https://issues.apache.org/jira/browse/CASSANDRA-13833) | Failed compaction is not captured |  Major | Compaction | Jay Zhuang | Jay Zhuang |
| [CASSANDRA-13738](https://issues.apache.org/jira/browse/CASSANDRA-13738) | Load is over calculated after each IndexSummaryRedistribution |  Major | Core | Jay Zhuang | Jay Zhuang |
| [CASSANDRA-13868](https://issues.apache.org/jira/browse/CASSANDRA-13868) | Safely handle empty buffers when outputting to JSON |  Minor | . | Jason Brown | Jason Brown |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13782](https://issues.apache.org/jira/browse/CASSANDRA-13782) | Cassandra RPM has wrong owner for /usr/share directories |  Major | Packaging | Hannu Kröger | Sasatani Takenobu |


