
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.0.3 - 2011-11-18



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3265](https://issues.apache.org/jira/browse/CASSANDRA-3265) | Display server version when connecting with cli, cqlsh |  Trivial | Tools | Jonathan Ellis | paul cannon |
| [CASSANDRA-3456](https://issues.apache.org/jira/browse/CASSANDRA-3456) | Automatically create SHA1 of new sstables |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3464](https://issues.apache.org/jira/browse/CASSANDRA-3464) | Remove renameSSTables from ColumnFamilyStore.java |  Trivial | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-3474](https://issues.apache.org/jira/browse/CASSANDRA-3474) | SSTableMetadata deserialization works but looks broken |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3470](https://issues.apache.org/jira/browse/CASSANDRA-3470) | Add a second letter to Descriptor version |  Minor | . | Jonathan Ellis | Yuki Morishita |
| [CASSANDRA-3478](https://issues.apache.org/jira/browse/CASSANDRA-3478) | Add support for sstable forwards-compatibility |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3393](https://issues.apache.org/jira/browse/CASSANDRA-3393) | report compression ratio in CFSMBean |  Minor | Tools | Jonathan Ellis | Vijay |
| [CASSANDRA-3188](https://issues.apache.org/jira/browse/CASSANDRA-3188) | cqlsh 2.0 |  Major | . | Jonathan Ellis | paul cannon |
| [CASSANDRA-3471](https://issues.apache.org/jira/browse/CASSANDRA-3471) | Less verbose cassandra-cli startup |  Trivial | Tools | Radim Kolar | Radim Kolar |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3450](https://issues.apache.org/jira/browse/CASSANDRA-3450) | maybeInit in ColumnFamilyRecordReader can cause rows to be empty but not null |  Major | . | Lanny Ripple | Lanny Ripple |
| [CASSANDRA-3178](https://issues.apache.org/jira/browse/CASSANDRA-3178) | Counter shard merging is not thread safe |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3437](https://issues.apache.org/jira/browse/CASSANDRA-3437) | invalidate / unregisterSSTables is confused |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3472](https://issues.apache.org/jira/browse/CASSANDRA-3472) | Actually uses efficient cross DC writes |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3446](https://issues.apache.org/jira/browse/CASSANDRA-3446) | Problem SliceByNamesReadCommand on super column family after flush operation |  Critical | . | Mike Smith | Jonathan Ellis |
| [CASSANDRA-3473](https://issues.apache.org/jira/browse/CASSANDRA-3473) | Missing null check in CQL QueryProcessor |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3467](https://issues.apache.org/jira/browse/CASSANDRA-3467) | get\_slice, super column family with UUIDType as column comparator |  Minor | CQL | alesl | Rick Branson |
| [CASSANDRA-3475](https://issues.apache.org/jira/browse/CASSANDRA-3475) | LoadBroadcaster never removes endpoints |  Trivial | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3418](https://issues.apache.org/jira/browse/CASSANDRA-3418) | Counter decrements require a space around the minus sign but not around the plus sign |  Minor | . | Kelley Reynolds | Pavel Yaskevich |
| [CASSANDRA-3482](https://issues.apache.org/jira/browse/CASSANDRA-3482) | Flush Assertion Error - CF size changed during serialization |  Critical | . | Dan Hendry | Jonathan Ellis |
| [CASSANDRA-3466](https://issues.apache.org/jira/browse/CASSANDRA-3466) | Hinted handoff not working after rolling upgrade from 0.8.7 to 1.0.2 |  Major | . | Jonas Borgström | Jonathan Ellis |
| [CASSANDRA-3484](https://issues.apache.org/jira/browse/CASSANDRA-3484) | Bizarre Compaction Manager Behaviour |  Major | . | Dan Hendry |  |
| [CASSANDRA-3131](https://issues.apache.org/jira/browse/CASSANDRA-3131) | cqlsh doesn't work on windows (no readline) |  Minor | . | Eric Evans | paul cannon |
| [CASSANDRA-3491](https://issues.apache.org/jira/browse/CASSANDRA-3491) | Recursion bug in CollationController |  Major | . | T Jake Luciani | Jonathan Ellis |
| [CASSANDRA-3493](https://issues.apache.org/jira/browse/CASSANDRA-3493) | cqlsh complains when you try to do UPDATE with counter columns |  Minor | Tools | paul cannon | paul cannon |
| [CASSANDRA-3492](https://issues.apache.org/jira/browse/CASSANDRA-3492) | Compression option chunk\_length is not converted into KB as it should |  Critical | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3481](https://issues.apache.org/jira/browse/CASSANDRA-3481) | During repair, "incorrect data size" & "Connection reset" errors. Repair unable to complete. |  Major | . | Eric Falcao | Sylvain Lebresne |
| [CASSANDRA-3522](https://issues.apache.org/jira/browse/CASSANDRA-3522) | Fatal exception in thread Thread |  Minor | . | Gabriel Rosendorf | Brandon Williams |


