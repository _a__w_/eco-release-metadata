
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.7.9 - 2011-09-06



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2895](https://issues.apache.org/jira/browse/CASSANDRA-2895) | add java classpath to cassandra startup logging |  Minor | . | Jackson Chung | Jackson Chung |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2869](https://issues.apache.org/jira/browse/CASSANDRA-2869) | CassandraStorage does not function properly when used multiple times in a single pig script due to UDFContext sharing issues |  Major | . | Grant Ingersoll | Jeremy Hanna |
| [CASSANDRA-2929](https://issues.apache.org/jira/browse/CASSANDRA-2929) | Don't include tmp files as sstable when create column families |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2662](https://issues.apache.org/jira/browse/CASSANDRA-2662) | Nodes get ignored by dynamic snitch when read repair chance is zero |  Trivial | . | Daniel Doubleday | Brandon Williams |
| [CASSANDRA-2708](https://issues.apache.org/jira/browse/CASSANDRA-2708) | memory leak in CompactionManager's estimatedCompactions |  Minor | . | Dan LaRocque | Dan LaRocque |
| [CASSANDRA-3071](https://issues.apache.org/jira/browse/CASSANDRA-3071) | Gossip state is not removed after a new IP takes over a token |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-2868](https://issues.apache.org/jira/browse/CASSANDRA-2868) | Native Memory Leak |  Minor | . | Daniel Doubleday | Brandon Williams |
| [CASSANDRA-3082](https://issues.apache.org/jira/browse/CASSANDRA-3082) | Operation with CL=EACH\_QUORUM doesn't succeed when a replica is down (RF=3) |  Minor | . | Patricio Echague | Patricio Echague |
| [CASSANDRA-3076](https://issues.apache.org/jira/browse/CASSANDRA-3076) | AssertionError in new GCInspector log |  Minor | . | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-2964](https://issues.apache.org/jira/browse/CASSANDRA-2964) | IndexRangeSliceQuery results include index column even though it is not in SliceRange |  Major | . | Roland Gude | Jonathan Ellis |


