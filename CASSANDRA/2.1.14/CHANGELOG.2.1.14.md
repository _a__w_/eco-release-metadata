
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1.14 - 2016-04-26



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10622](https://issues.apache.org/jira/browse/CASSANDRA-10622) | Add all options in cqlshrc sample as commented out choices |  Minor | Documentation and Website | Lorina Poland | Tyler Hobbs |
| [CASSANDRA-11041](https://issues.apache.org/jira/browse/CASSANDRA-11041) | Make it clear what timestamp\_resolution is used for with DTCS |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-10888](https://issues.apache.org/jira/browse/CASSANDRA-10888) | Tombstone error warning does not log partition key |  Major | Local Write-Read Paths | Ole Pedersen | Brett Snyder |
| [CASSANDRA-11179](https://issues.apache.org/jira/browse/CASSANDRA-11179) | Parallel cleanup can lead to disk space exhaustion |  Major | Compaction, Tools | Tyler Hobbs | Marcus Eriksson |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10979](https://issues.apache.org/jira/browse/CASSANDRA-10979) | LCS doesn't do L0 STC on new tables while an L0-\>L1 compaction is in progress |  Major | Compaction | Jeff Ferland | Carl Yeksigian |
| [CASSANDRA-11116](https://issues.apache.org/jira/browse/CASSANDRA-11116) | Gossiper#isEnabled is not thread safe |  Critical | Core | Sergio Bossa | Ariel Weisberg |
| [CASSANDRA-11113](https://issues.apache.org/jira/browse/CASSANDRA-11113) | DateTieredCompactionStrategy.getMaximalTask compacts repaired and unrepaired sstables together |  Major | . | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-7238](https://issues.apache.org/jira/browse/CASSANDRA-7238) | Nodetool Status performance is much slower with VNodes On |  Minor | Tools | Russell Spitzer | Yuki Morishita |
| [CASSANDRA-10697](https://issues.apache.org/jira/browse/CASSANDRA-10697) | Leak detected while running offline scrub |  Major | Tools | mlowicki | Marcus Eriksson |
| [CASSANDRA-11079](https://issues.apache.org/jira/browse/CASSANDRA-11079) | LeveledCompactionStrategyTest.testCompactionProgress unit test fails sometimes |  Major | Testing | Sylvain Lebresne | Marcus Eriksson |
| [CASSANDRA-11080](https://issues.apache.org/jira/browse/CASSANDRA-11080) | CompactionsCQLTest.testTriggerMinorCompactionSTCS is flaky on 2.2 |  Major | . | Sylvain Lebresne | Marcus Eriksson |
| [CASSANDRA-10176](https://issues.apache.org/jira/browse/CASSANDRA-10176) | nodetool status says ' Non-system keyspaces don't have the same replication settings' when they do |  Minor | Distributed Metadata | Chris Burroughs | Sylvain Lebresne |
| [CASSANDRA-9714](https://issues.apache.org/jira/browse/CASSANDRA-9714) | sstableloader appears to use the cassandra.yaml outgoing stream throttle |  Major | Tools | Jeremy Hanna | Yuki Morishita |
| [CASSANDRA-10767](https://issues.apache.org/jira/browse/CASSANDRA-10767) | Checking version of Cassandra command creates \`cassandra.logdir\_IS\_UNDEFINED/\` |  Trivial | Tools | Jens Rantil | Yuki Morishita |
| [CASSANDRA-11172](https://issues.apache.org/jira/browse/CASSANDRA-11172) | Infinite loop bug adding high-level SSTableReader in compaction |  Major | Compaction | Jeff Ferland | Marcus Eriksson |
| [CASSANDRA-10371](https://issues.apache.org/jira/browse/CASSANDRA-10371) | Decommissioned nodes can remain in gossip |  Minor | Distributed Metadata | Brandon Williams | Joel Knighton |
| [CASSANDRA-11176](https://issues.apache.org/jira/browse/CASSANDRA-11176) | SSTableRewriter.InvalidateKeys should have a weak reference to cache |  Major | Core | Jeremiah Jordan | Marcus Eriksson |
| [CASSANDRA-11302](https://issues.apache.org/jira/browse/CASSANDRA-11302) | Invalid time unit conversion causing write timeouts |  Major | Core | Mike Heffner | Sylvain Lebresne |
| [CASSANDRA-11286](https://issues.apache.org/jira/browse/CASSANDRA-11286) | streaming socket never times out |  Major | Streaming and Messaging | Paulo Motta | Paulo Motta |
| [CASSANDRA-10342](https://issues.apache.org/jira/browse/CASSANDRA-10342) | Read defragmentation can cause unnecessary repairs |  Minor | . | Marcus Olsson | Marcus Eriksson |
| [CASSANDRA-10752](https://issues.apache.org/jira/browse/CASSANDRA-10752) | CQL.textile wasn't updated for CASSANDRA-6839 |  Major | Documentation and Website | Jeremiah Jordan | Sylvain Lebresne |
| [CASSANDRA-9598](https://issues.apache.org/jira/browse/CASSANDRA-9598) | bad classapth for 'sstablerepairedset' in 'cassandra-tools' package |  Minor | Tools | Clément Lardeur | Yuki Morishita |
| [CASSANDRA-11053](https://issues.apache.org/jira/browse/CASSANDRA-11053) | COPY FROM on large datasets: fix progress report and optimize performance part 4 |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-11375](https://issues.apache.org/jira/browse/CASSANDRA-11375) | COPY FROM fails when importing blob |  Major | CQL | Philippe Thibaudeau | Stefania |
| [CASSANDRA-11415](https://issues.apache.org/jira/browse/CASSANDRA-11415) | dtest failure in jmx\_test.TestJMX.cfhistograms\_test |  Major | Testing | Philip Thompson | Yuki Morishita |
| [CASSANDRA-11448](https://issues.apache.org/jira/browse/CASSANDRA-11448) | Running OOS should trigger the disk failure policy |  Major | . | Brandon Williams | Branimir Lambov |
| [CASSANDRA-11467](https://issues.apache.org/jira/browse/CASSANDRA-11467) | Paging loses rows in certain conditions |  Major | CQL | Ian McMahon | Benjamin Lerer |
| [CASSANDRA-11529](https://issues.apache.org/jira/browse/CASSANDRA-11529) | Checking if an unlogged batch is local is inefficient |  Critical | Coordination | Paulo Motta | Stefania |
| [CASSANDRA-11548](https://issues.apache.org/jira/browse/CASSANDRA-11548) | Anticompaction not removing old sstables |  Major | . | Ruoran Wang | Ruoran Wang |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11505](https://issues.apache.org/jira/browse/CASSANDRA-11505) | dtest failure in cqlsh\_tests.cqlsh\_copy\_tests.CqlshCopyTest.test\_reading\_max\_parse\_errors |  Major | Tools | Michael Shuler | Stefania |


