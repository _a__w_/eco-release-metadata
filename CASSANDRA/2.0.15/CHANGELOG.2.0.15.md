
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.0.15 - 2015-05-18



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-8734](https://issues.apache.org/jira/browse/CASSANDRA-8734) | Expose commit log archive status |  Minor | Configuration | Philip S Doctor | Chris Lohfink |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-8086](https://issues.apache.org/jira/browse/CASSANDRA-8086) | Cassandra should have ability to limit the number of native connections |  Major | . | Vishy Kasar | Norman Maurer |
| [CASSANDRA-8909](https://issues.apache.org/jira/browse/CASSANDRA-8909) | Replication Strategy creation errors are lost in try/catch |  Trivial | . | Alan Boudreault | Alan Boudreault |
| [CASSANDRA-8085](https://issues.apache.org/jira/browse/CASSANDRA-8085) | Make PasswordAuthenticator number of hashing rounds configurable |  Major | Configuration | Tyler Hobbs | Sam Tunnicliffe |
| [CASSANDRA-7533](https://issues.apache.org/jira/browse/CASSANDRA-7533) | Let MAX\_OUTSTANDING\_REPLAY\_COUNT be configurable |  Minor | . | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-9032](https://issues.apache.org/jira/browse/CASSANDRA-9032) | Reduce logging level for MigrationTask abort due to down node from ERROR to INFO |  Minor | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-9050](https://issues.apache.org/jira/browse/CASSANDRA-9050) | Add debug level logging to Directories.getWriteableLocation() |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-8056](https://issues.apache.org/jira/browse/CASSANDRA-8056) | nodetool snapshot \<keyspace\> -cf \<table\> -t \<sametagname\> does not work on multiple tabes of the same keyspace |  Trivial | . | Esha Pathak |  |
| [CASSANDRA-8360](https://issues.apache.org/jira/browse/CASSANDRA-8360) | In DTCS, always compact SSTables in the same time window, even if they are fewer than min\_threshold |  Minor | . | Björn Hegerfors | Björn Hegerfors |
| [CASSANDRA-8359](https://issues.apache.org/jira/browse/CASSANDRA-8359) | Make DTCS consider removing SSTables much more frequently |  Minor | . | Björn Hegerfors | Björn Hegerfors |
| [CASSANDRA-9128](https://issues.apache.org/jira/browse/CASSANDRA-9128) | Flush system.IndexInfo after index state changed |  Minor | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-9196](https://issues.apache.org/jira/browse/CASSANDRA-9196) | Do not rebuild indexes if no columns are actually indexed |  Major | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-9262](https://issues.apache.org/jira/browse/CASSANDRA-9262) | IncomingTcpConnection thread is not named |  Trivial | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-9140](https://issues.apache.org/jira/browse/CASSANDRA-9140) | Scrub should handle corrupted compressed chunks |  Major | Tools | Tyler Hobbs | Stefania |
| [CASSANDRA-9314](https://issues.apache.org/jira/browse/CASSANDRA-9314) | Overload SecondaryIndex#indexes to accept the column definition |  Major | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-9322](https://issues.apache.org/jira/browse/CASSANDRA-9322) | Possible overlap with LCS and including non-compacting sstables |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9286](https://issues.apache.org/jira/browse/CASSANDRA-9286) | Add Keyspace/Table details to CollectionType.java error message |  Minor | Observability | sequoyha pelletier | Carl Yeksigian |
| [CASSANDRA-9364](https://issues.apache.org/jira/browse/CASSANDRA-9364) | Add a warning when a high setting for rpc\_max\_threads is specified for HSHA |  Major | . | Brandon Williams | Brandon Williams |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-8243](https://issues.apache.org/jira/browse/CASSANDRA-8243) | DTCS can leave time-overlaps, limiting ability to expire entire SSTables |  Minor | . | Björn Hegerfors | Björn Hegerfors |
| [CASSANDRA-8808](https://issues.apache.org/jira/browse/CASSANDRA-8808) | CQLSSTableWriter: close does not work + more than one table throws ex |  Major | CQL | Sebastian YEPES FERNANDEZ | Benjamin Lerer |
| [CASSANDRA-8613](https://issues.apache.org/jira/browse/CASSANDRA-8613) | Regression in mixed single and multi-column relation support |  Major | CQL | Tyler Hobbs | Benjamin Lerer |
| [CASSANDRA-8238](https://issues.apache.org/jira/browse/CASSANDRA-8238) | NPE in SizeTieredCompactionStrategy.filterColdSSTables |  Major | Compaction | Tyler Hobbs | Marcus Eriksson |
| [CASSANDRA-8904](https://issues.apache.org/jira/browse/CASSANDRA-8904) | Compaction pending tasks is 9 but no compaction is running |  Minor | . | Zdenek Ott | Carl Yeksigian |
| [CASSANDRA-7816](https://issues.apache.org/jira/browse/CASSANDRA-7816) | Duplicate DOWN/UP Events Pushed with Native Protocol |  Minor | CQL | Michael Penick | Stefania |
| [CASSANDRA-8991](https://issues.apache.org/jira/browse/CASSANDRA-8991) | CQL3 DropIndexStatement should expose getColumnFamily like the CQL2 version does. |  Minor | . | Ulises Cervino Beresi | Ulises Cervino Beresi |
| [CASSANDRA-8950](https://issues.apache.org/jira/browse/CASSANDRA-8950) | NullPointerException in nodetool getendpoints with non-existent keyspace or table |  Minor | Tools | Tyler Hobbs | Stefania |
| [CASSANDRA-8559](https://issues.apache.org/jira/browse/CASSANDRA-8559) | OOM caused by large tombstone warning. |  Major | . | Dominic Letz | Aleksey Yeschenko |
| [CASSANDRA-8934](https://issues.apache.org/jira/browse/CASSANDRA-8934) | COPY command has inherent 128KB field size limit |  Major | Tools |  Brian Hess | Philip Thompson |
| [CASSANDRA-8949](https://issues.apache.org/jira/browse/CASSANDRA-8949) | CompressedSequentialWriter.resetAndTruncate can lose data |  Critical | Local Write-Read Paths | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-9027](https://issues.apache.org/jira/browse/CASSANDRA-9027) | Error processing org.apache.cassandra.metrics:type=HintedHandOffManager,name=Hints\_created-\<IPv6 address\> |  Major | . | Erik Forsberg | Erik Forsberg |
| [CASSANDRA-8516](https://issues.apache.org/jira/browse/CASSANDRA-8516) | NEW\_NODE topology event emitted instead of MOVED\_NODE by moving node |  Minor | CQL | Tyler Hobbs | Stefania |
| [CASSANDRA-9036](https://issues.apache.org/jira/browse/CASSANDRA-9036) | "disk full" when running cleanup (on a far from full disk) |  Major | . | Erik Forsberg | Robert Stupp |
| [CASSANDRA-8740](https://issues.apache.org/jira/browse/CASSANDRA-8740) | java.lang.AssertionError when reading saved cache |  Major | . | Nikolai Grigoriev | Dave Brosius |
| [CASSANDRA-7976](https://issues.apache.org/jira/browse/CASSANDRA-7976) | Changes to index\_interval table properties revert after subsequent modifications |  Major | Configuration | Andrew Lenards | Stefania |
| [CASSANDRA-9070](https://issues.apache.org/jira/browse/CASSANDRA-9070) | Race in cancelling compactions |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-8672](https://issues.apache.org/jira/browse/CASSANDRA-8672) | Ambiguous WriteTimeoutException while completing pending CAS commits |  Minor | . | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-8979](https://issues.apache.org/jira/browse/CASSANDRA-8979) | MerkleTree mismatch for deleted and non-existing rows |  Major | . | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-7491](https://issues.apache.org/jira/browse/CASSANDRA-7491) | Incorrect thrift-server dependency in 2.0 poms |  Major | Packaging | Sam Tunnicliffe |  |
| [CASSANDRA-9088](https://issues.apache.org/jira/browse/CASSANDRA-9088) | Don't include tmp files in SSTableOfflineRelevel |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-8989](https://issues.apache.org/jira/browse/CASSANDRA-8989) | Reading from table which contains collection type using token function and with CL \> ONE causes overwhelming writes to replicas |  Major | . | Miroslaw Partyka | Carl Yeksigian |
| [CASSANDRA-8102](https://issues.apache.org/jira/browse/CASSANDRA-8102) | cassandra-cli and cqlsh report two different values for a setting, partially update it and partially report it |  Minor | Tools | Peter Haggerty | Stefania |
| [CASSANDRA-9180](https://issues.apache.org/jira/browse/CASSANDRA-9180) | Failed bootstrap/replace attempts persist entries in system.peers |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-9137](https://issues.apache.org/jira/browse/CASSANDRA-9137) | nodetool doesn't work when Cassandra run with the property java.net.preferIPv6Addresses=true |  Major | . | Andrey Trubachev | Andrey Trubachev |
| [CASSANDRA-9139](https://issues.apache.org/jira/browse/CASSANDRA-9139) | LeveledCompactionStrategyTest failing in test-compression target |  Major | Testing | Philip Thompson | Marcus Eriksson |
| [CASSANDRA-8336](https://issues.apache.org/jira/browse/CASSANDRA-8336) | Add shutdown gossip state to prevent timeouts during rolling restarts |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-9135](https://issues.apache.org/jira/browse/CASSANDRA-9135) | testScrubCorruptedCounterRow is failing under test-compression target |  Major | Testing | Philip Thompson | Tyler Hobbs |
| [CASSANDRA-7292](https://issues.apache.org/jira/browse/CASSANDRA-7292) | Can't seed new node into ring with (public) ip of an old node |  Major | . | Juho Mäkinen | Brandon Williams |
| [CASSANDRA-9138](https://issues.apache.org/jira/browse/CASSANDRA-9138) | BlacklistingCompactionsTest failing on test-compression target |  Major | Testing | Philip Thompson | Marcus Eriksson |
| [CASSANDRA-9235](https://issues.apache.org/jira/browse/CASSANDRA-9235) | Max sstable size in leveled manifest is an int, creating large sstables overflows this and breaks LCS |  Major | Compaction | Sergey Maznichenko | Marcus Eriksson |
| [CASSANDRA-9238](https://issues.apache.org/jira/browse/CASSANDRA-9238) | Race condition after shutdown gossip message |  Minor | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-9234](https://issues.apache.org/jira/browse/CASSANDRA-9234) | Disable single-sstable tombstone compactions for DTCS |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9249](https://issues.apache.org/jira/browse/CASSANDRA-9249) | Resetting local schema can cause assertion error |  Minor | Distributed Metadata | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-9194](https://issues.apache.org/jira/browse/CASSANDRA-9194) | Delete-only workloads crash Cassandra |  Major | . | Robert Wille | Benedict |
| [CASSANDRA-9281](https://issues.apache.org/jira/browse/CASSANDRA-9281) | Index selection during rebuild fails with certain table layouts. |  Major | Streaming and Messaging | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-9290](https://issues.apache.org/jira/browse/CASSANDRA-9290) | Fix possible NPE in Scrubber |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-8051](https://issues.apache.org/jira/browse/CASSANDRA-8051) | Add SERIAL and LOCAL\_SERIAL consistency levels to cqlsh |  Minor | Tools | Nicolas Favre-Felix | Carl Yeksigian |
| [CASSANDRA-9136](https://issues.apache.org/jira/browse/CASSANDRA-9136) | Improve error handling when table is queried before the schema has fully propagated |  Major | . | Russell Spitzer | Tyler Hobbs |
| [CASSANDRA-6702](https://issues.apache.org/jira/browse/CASSANDRA-6702) | Upgrading node uses the wrong port in gossiping |  Minor | . | Minh Do | Blake Eggleston |
| [CASSANDRA-9299](https://issues.apache.org/jira/browse/CASSANDRA-9299) | Fix counting of tombstones towards TombstoneOverwhelmingException |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-9132](https://issues.apache.org/jira/browse/CASSANDRA-9132) | resumable\_bootstrap\_test can hang |  Major | Testing | Tyler Hobbs | Yuki Morishita |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7712](https://issues.apache.org/jira/browse/CASSANDRA-7712) | temporary files need to be cleaned by unit tests |  Minor | Testing | Michael Shuler | Michael Shuler |


