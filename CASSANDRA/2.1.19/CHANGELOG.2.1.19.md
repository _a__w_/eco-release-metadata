
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1.19 - 2017-10-05



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13844](https://issues.apache.org/jira/browse/CASSANDRA-13844) | sstableloader doesn't support non default storage\_port and ssl\_storage\_port |  Major | Tools | Eduard Tudenhoefner | Eduard Tudenhoefner |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13700](https://issues.apache.org/jira/browse/CASSANDRA-13700) | Heartbeats can cause gossip information to go permanently missing on certain nodes |  Critical | Distributed Metadata | Joel Knighton | Joel Knighton |
| [CASSANDRA-13775](https://issues.apache.org/jira/browse/CASSANDRA-13775) | CircleCI tests fail because \*stress-test\* isn't a valid target |  Major | Build | Eduard Tudenhoefner | Eduard Tudenhoefner |
| [CASSANDRA-13807](https://issues.apache.org/jira/browse/CASSANDRA-13807) | CircleCI fix - only collect the xml file from containers where it exists |  Major | . | Marcus Eriksson | Marcus Eriksson |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13782](https://issues.apache.org/jira/browse/CASSANDRA-13782) | Cassandra RPM has wrong owner for /usr/share directories |  Major | Packaging | Hannu Kröger | Sasatani Takenobu |


