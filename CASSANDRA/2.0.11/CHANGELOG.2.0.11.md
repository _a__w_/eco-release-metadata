
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.0.11 - 2014-10-24



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6602](https://issues.apache.org/jira/browse/CASSANDRA-6602) | Compaction improvements to optimize time series data |  Major | . | Tupshin Harper | Björn Hegerfors |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7864](https://issues.apache.org/jira/browse/CASSANDRA-7864) | Repair should do less work when RF=1 |  Minor | Streaming and Messaging | Tyler Hobbs | Yuki Morishita |
| [CASSANDRA-7851](https://issues.apache.org/jira/browse/CASSANDRA-7851) | C\* PID file should be readable by mere users |  Minor | Packaging | Michael Shuler | Michael Shuler |
| [CASSANDRA-7968](https://issues.apache.org/jira/browse/CASSANDRA-7968) | permissions\_validity\_in\_ms should be settable via JMX |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-7849](https://issues.apache.org/jira/browse/CASSANDRA-7849) | Server logged error messages (in binary protocol) for unexpected exceptions could be more helpful |  Major | . | graham sanderson | graham sanderson |
| [CASSANDRA-7977](https://issues.apache.org/jira/browse/CASSANDRA-7977) | Allow invalidating permissions cache |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-7978](https://issues.apache.org/jira/browse/CASSANDRA-7978) | CrcCheckChance should not be stored as part of an sstable |  Minor | Distributed Metadata, Local Write-Read Paths | sankalp kohli | T Jake Luciani |
| [CASSANDRA-8046](https://issues.apache.org/jira/browse/CASSANDRA-8046) | Set base C\* version in debs and strip -N, ~textN, +textN |  Trivial | Packaging | Michael Shuler | Michael Shuler |
| [CASSANDRA-7909](https://issues.apache.org/jira/browse/CASSANDRA-7909) | Do not exit nodetool repair when receiving JMX NOTIF\_LOST |  Trivial | Tools | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-7676](https://issues.apache.org/jira/browse/CASSANDRA-7676) | bin/cassandra should complain if $JAVA is empty or not an executable |  Trivial | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-8088](https://issues.apache.org/jira/browse/CASSANDRA-8088) | Notify subscribers when a column family is truncated |  Minor | Observability | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-8111](https://issues.apache.org/jira/browse/CASSANDRA-8111) | Create backup directories for commitlog archiving during startup |  Trivial | . | Jan Karlsson |  |
| [CASSANDRA-8021](https://issues.apache.org/jira/browse/CASSANDRA-8021) | Improve cqlsh autocomplete for alter keyspace |  Minor | . | Philip Thompson | Rajanarayanan Thottuvaikkatumana |
| [CASSANDRA-7777](https://issues.apache.org/jira/browse/CASSANDRA-7777) | Ability to clean up local sstable files after they've been loaded by the CqlBulkRecordWriter |  Minor | . | Paul Pak | Paul Pak |
| [CASSANDRA-7341](https://issues.apache.org/jira/browse/CASSANDRA-7341) | Emit metrics related to CAS/Paxos |  Minor | . | sankalp kohli | sankalp kohli |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7479](https://issues.apache.org/jira/browse/CASSANDRA-7479) | Consistency level ANY does not send Commit to all endpoints for LOCAL\_SERIAL |  Major | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-7722](https://issues.apache.org/jira/browse/CASSANDRA-7722) | IndexOutOfBoundsException when combining token() with PK inequality in where clause |  Minor | . | Mike Adamson | Benjamin Lerer |
| [CASSANDRA-7808](https://issues.apache.org/jira/browse/CASSANDRA-7808) | LazilyCompactedRow incorrectly handles row tombstones |  Major | . | Richard Low | Richard Low |
| [CASSANDRA-7802](https://issues.apache.org/jira/browse/CASSANDRA-7802) | Need to export JVM\_OPTS from init.d script |  Minor | Packaging | Matt Robenolt | Matt Robenolt |
| [CASSANDRA-7832](https://issues.apache.org/jira/browse/CASSANDRA-7832) | A partition key-only CQL3 table mistakenly considered to be thrift compatible |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-7810](https://issues.apache.org/jira/browse/CASSANDRA-7810) | tombstones gc'd before being locally applied |  Major | Compaction | Jonathan Halliday | Marcus Eriksson |
| [CASSANDRA-7799](https://issues.apache.org/jira/browse/CASSANDRA-7799) | cassandra-env.sh doesn't detect openjdk version numbers |  Minor | . | Ryan McGuire | Brandon Williams |
| [CASSANDRA-7831](https://issues.apache.org/jira/browse/CASSANDRA-7831) | recreating a counter column after dropping it leaves in unusable state |  Major | . | Peter Mädel | Aleksey Yeschenko |
| [CASSANDRA-7145](https://issues.apache.org/jira/browse/CASSANDRA-7145) | FileNotFoundException during compaction |  Major | Compaction | PJ | Marcus Eriksson |
| [CASSANDRA-7711](https://issues.apache.org/jira/browse/CASSANDRA-7711) | composite column not sliced when using IN clause on (other) composite columns |  Minor | CQL | Frens Jan Rumph | Benjamin Lerer |
| [CASSANDRA-7594](https://issues.apache.org/jira/browse/CASSANDRA-7594) | Disruptor Thrift server worker thread pool not adjustable |  Major | . | Rick Branson | Pavel Yaskevich |
| [CASSANDRA-7828](https://issues.apache.org/jira/browse/CASSANDRA-7828) | New node cannot be joined if a value in composite type column is dropped (description updated) |  Major | . | Igor Zubchenok | Mikhail Stepura |
| [CASSANDRA-7906](https://issues.apache.org/jira/browse/CASSANDRA-7906) | CqlConfigHelper disables the connection pool for remote hosts while the remote hosts can be used |  Major | . | Jacek Lewandowski | Jacek Lewandowski |
| [CASSANDRA-7360](https://issues.apache.org/jira/browse/CASSANDRA-7360) | CQLSSTableWriter consumes all memory for table with compound primary key |  Major | . | Xu Zhongxing | Sylvain Lebresne |
| [CASSANDRA-7856](https://issues.apache.org/jira/browse/CASSANDRA-7856) | Xss in test |  Minor | Testing | Cyril Scetbon | Tyler Hobbs |
| [CASSANDRA-7912](https://issues.apache.org/jira/browse/CASSANDRA-7912) | YamlFileNetworkTopologySnitch doesn't switch to dc-internal ip's anymore |  Major | . | Holger Bornträger | Holger Bornträger |
| [CASSANDRA-7889](https://issues.apache.org/jira/browse/CASSANDRA-7889) | Fix "java.lang.IllegalStateException: No configured daemon" in tests |  Trivial | . | Sylvain Lebresne | Steven Nelson |
| [CASSANDRA-7940](https://issues.apache.org/jira/browse/CASSANDRA-7940) | Gossip only node removal can race with FD.convict |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-7948](https://issues.apache.org/jira/browse/CASSANDRA-7948) | RowIndexEntry incorrectly reports serializedSize. |  Minor | . | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-7967](https://issues.apache.org/jira/browse/CASSANDRA-7967) | Include schema\_triggers CF in readable system resources |  Major | . | Adam Holmberg | Adam Holmberg |
| [CASSANDRA-6125](https://issues.apache.org/jira/browse/CASSANDRA-6125) | Race condition in Gossip propagation |  Major | . | Sergio Bossa | Brandon Williams |
| [CASSANDRA-7946](https://issues.apache.org/jira/browse/CASSANDRA-7946) | NPE when streaming data to a joining node and dropping table in cluster |  Major | Streaming and Messaging | DOAN DuyHai | Yuki Morishita |
| [CASSANDRA-6075](https://issues.apache.org/jira/browse/CASSANDRA-6075) | The token function should allow column identifiers in the correct order only |  Minor | CQL | Michaël Figuière | Benjamin Lerer |
| [CASSANDRA-7878](https://issues.apache.org/jira/browse/CASSANDRA-7878) | Fix wrong progress reporting when streaming uncompressed SSTable w/ CRC check |  Trivial | Streaming and Messaging, Tools | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-7939](https://issues.apache.org/jira/browse/CASSANDRA-7939) | checkForEndpointCollision should ignore joining nodes |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-6898](https://issues.apache.org/jira/browse/CASSANDRA-6898) | describing a table with compression should expand the compression options |  Minor | CQL | Brandon Williams |  |
| [CASSANDRA-7983](https://issues.apache.org/jira/browse/CASSANDRA-7983) | nodetool repair triggers OOM |  Major | Tools | Jose Martinez Poblete | Jimmy Mårdell |
| [CASSANDRA-6904](https://issues.apache.org/jira/browse/CASSANDRA-6904) | commitlog segments may not be archived after restart |  Major | Lifecycle, Local Write-Read Paths | Jonathan Ellis | Sam Tunnicliffe |
| [CASSANDRA-7956](https://issues.apache.org/jira/browse/CASSANDRA-7956) | "nodetool compactionhistory" crashes because of low heap size (GC overhead limit exceeded) |  Trivial | . | Nikolai Grigoriev | Michael Shuler |
| [CASSANDRA-7992](https://issues.apache.org/jira/browse/CASSANDRA-7992) | Arithmetic overflow sorting commit log segments on replay |  Major | . | Oleg Anastasyev | Oleg Anastasyev |
| [CASSANDRA-7833](https://issues.apache.org/jira/browse/CASSANDRA-7833) | Collection types validation is incomplete |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-7969](https://issues.apache.org/jira/browse/CASSANDRA-7969) | Properly track min/max timestamps and maxLocalDeletionTimes for range and row tombstones |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-7898](https://issues.apache.org/jira/browse/CASSANDRA-7898) | IndexOutOfBoundsException comparing incompatible reversed types in DynamicCompositeType |  Major | . | Tim Whittington | Tim Whittington |
| [CASSANDRA-7375](https://issues.apache.org/jira/browse/CASSANDRA-7375) | nodetool units wrong for streamthroughput |  Minor | . | Mike Heffner |  |
| [CASSANDRA-7899](https://issues.apache.org/jira/browse/CASSANDRA-7899) | SSL does not work in cassandra-cli |  Major | Tools | Zdenek Ott | Jason Brown |
| [CASSANDRA-8057](https://issues.apache.org/jira/browse/CASSANDRA-8057) | Record the real messaging version in all cases in OutboundTcpConnection |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-7734](https://issues.apache.org/jira/browse/CASSANDRA-7734) | Schema pushes (seemingly) randomly not happening |  Major | . | graham sanderson | Aleksey Yeschenko |
| [CASSANDRA-7993](https://issues.apache.org/jira/browse/CASSANDRA-7993) | Fat client nodes dont schedule schema pull on connect |  Major | . | Oleg Anastasyev | Oleg Anastasyev |
| [CASSANDRA-8083](https://issues.apache.org/jira/browse/CASSANDRA-8083) | OpenJDK 6 Dependency in dsc20 RPM |  Minor | Packaging | Timo Beckers | Michael Shuler |
| [CASSANDRA-7188](https://issues.apache.org/jira/browse/CASSANDRA-7188) | Wrong class type: class org.apache.cassandra.db.Column in CounterColumn.reconcile |  Major | . | Nicolas Lalevée | Aleksey Yeschenko |
| [CASSANDRA-7256](https://issues.apache.org/jira/browse/CASSANDRA-7256) | Error when dropping keyspace. |  Major | . | Steven Lowenthal | Aleksey Yeschenko |
| [CASSANDRA-8058](https://issues.apache.org/jira/browse/CASSANDRA-8058) | local consistency level during boostrap (may cause a write timeout on each write request) |  Major | . | Nicolas DOUILLET | Nicolas DOUILLET |
| [CASSANDRA-8101](https://issues.apache.org/jira/browse/CASSANDRA-8101) | Invalid ASCII and UTF-8 chars not rejected in CQL string literals |  Critical | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-8108](https://issues.apache.org/jira/browse/CASSANDRA-8108) | Errors paging DISTINCT queries on static columns |  Major | . | David Hearnden | Tyler Hobbs |
| [CASSANDRA-6998](https://issues.apache.org/jira/browse/CASSANDRA-6998) | HintedHandoff - expired hints may block future hints deliveries |  Major | . | Scooletz | Aleksey Yeschenko |
| [CASSANDRA-7446](https://issues.apache.org/jira/browse/CASSANDRA-7446) | Batchlog should be streamed to a different node on decom |  Major | . | Aleksey Yeschenko | Branimir Lambov |
| [CASSANDRA-7747](https://issues.apache.org/jira/browse/CASSANDRA-7747) | CQL token(id) does not work in DELETE statements |  Minor | Tools | Taylor Gronka | Sylvain Lebresne |
| [CASSANDRA-6430](https://issues.apache.org/jira/browse/CASSANDRA-6430) | DELETE with IF \<field\>=\<value\> clause doesn't work properly if more then one row are going to be deleted |  Major | . | Dmitriy Ukhlov | Tyler Hobbs |
| [CASSANDRA-8084](https://issues.apache.org/jira/browse/CASSANDRA-8084) | GossipFilePropertySnitch and EC2MultiRegionSnitch when used in AWS/GCE clusters doesnt use the PRIVATE IPS for Intra-DC communications - When running nodetool repair |  Major | Configuration | Jana | Yuki Morishita |
| [CASSANDRA-8027](https://issues.apache.org/jira/browse/CASSANDRA-8027) | Assertion error in CompressionParameters |  Major | Distributed Metadata | Carl Yeksigian | T Jake Luciani |


