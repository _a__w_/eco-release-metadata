
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1 beta2 - 2014-05-05



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6882](https://issues.apache.org/jira/browse/CASSANDRA-6882) | Add triggers support to LWT operations |  Minor | . | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-6902](https://issues.apache.org/jira/browse/CASSANDRA-6902) | Make cqlsh prompt for a password if the user doesn't enter one |  Minor | Tools | J.B. Langston | J.B. Langston |
| [CASSANDRA-6689](https://issues.apache.org/jira/browse/CASSANDRA-6689) | Partially Off Heap Memtables |  Major | . | Benedict | Benedict |
| [CASSANDRA-6855](https://issues.apache.org/jira/browse/CASSANDRA-6855) | Native protocol V3 |  Major | CQL | Sylvain Lebresne | Sylvain Lebresne |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6446](https://issues.apache.org/jira/browse/CASSANDRA-6446) | Faster range tombstones on wide partitions |  Major | . | Oleg Anastasyev | Oleg Anastasyev |
| [CASSANDRA-5899](https://issues.apache.org/jira/browse/CASSANDRA-5899) | Sends all interface in native protocol notification when rpc\_address=0.0.0.0 |  Minor | . | Sylvain Lebresne | Tyler Hobbs |
| [CASSANDRA-6451](https://issues.apache.org/jira/browse/CASSANDRA-6451) | Allow Cassandra-Stress to Set Compaction Strategy Options |  Minor | Tools | Russell Spitzer | Benedict |
| [CASSANDRA-6767](https://issues.apache.org/jira/browse/CASSANDRA-6767) | sstablemetadata should print minimum timestamp |  Trivial | Tools | Matt Stump |  |
| [CASSANDRA-4165](https://issues.apache.org/jira/browse/CASSANDRA-4165) | Generate Digest file for compressed SSTables |  Minor | . | Marcus Eriksson | Jonathan Ellis |
| [CASSANDRA-6759](https://issues.apache.org/jira/browse/CASSANDRA-6759) | CommitLogSegment should provide madvise DONTNEED hint after syncing a segment |  Minor | . | Benedict | Benedict |
| [CASSANDRA-6307](https://issues.apache.org/jira/browse/CASSANDRA-6307) | Switch cqlsh from cassandra-dbapi2 to python-driver |  Minor | . | Aleksey Yeschenko | Mikhail Stepura |
| [CASSANDRA-6692](https://issues.apache.org/jira/browse/CASSANDRA-6692) | AtomicBTreeColumns Improvements |  Minor | . | Benedict | Benedict |
| [CASSANDRA-6835](https://issues.apache.org/jira/browse/CASSANDRA-6835) | cassandra-stress should support a variable number of counter columns |  Minor | . | Benedict | Benedict |
| [CASSANDRA-6331](https://issues.apache.org/jira/browse/CASSANDRA-6331) | Add LZ4Compressor in CQL document |  Trivial | Documentation and Website | Ray Wu | Mikhail Stepura |
| [CASSANDRA-6849](https://issues.apache.org/jira/browse/CASSANDRA-6849) | Add verbose logging to cassandra-stress |  Minor | Tools | Benedict | Benedict |
| [CASSANDRA-6865](https://issues.apache.org/jira/browse/CASSANDRA-6865) | Logging of slices and requested count in SliceQueryFilter |  Minor | . | Oleg Anastasyev | Oleg Anastasyev |
| [CASSANDRA-6518](https://issues.apache.org/jira/browse/CASSANDRA-6518) | Add in forgotten MX4J variables to cassandra-env.sh |  Trivial | Configuration | Lewis John McGibbney |  |
| [CASSANDRA-5708](https://issues.apache.org/jira/browse/CASSANDRA-5708) | Add DELETE ... IF EXISTS to CQL3 |  Minor | . | Sylvain Lebresne | Tyler Hobbs |
| [CASSANDRA-6854](https://issues.apache.org/jira/browse/CASSANDRA-6854) | TServerArgs have unsed reference |  Minor | . | Edward Capriolo | Edward Capriolo |
| [CASSANDRA-6781](https://issues.apache.org/jira/browse/CASSANDRA-6781) | ByteBuffer write() methods for serializing sstables |  Minor | . | Benedict | Benedict |
| [CASSANDRA-6876](https://issues.apache.org/jira/browse/CASSANDRA-6876) | Improve PerRowSecondaryIndex performance by removing O(N) complexity when computing indexes for a column |  Minor | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-6236](https://issues.apache.org/jira/browse/CASSANDRA-6236) | Update native protocol server to Netty 4 |  Minor | . | Sylvain Lebresne | Benedict |
| [CASSANDRA-6901](https://issues.apache.org/jira/browse/CASSANDRA-6901) | Make OpOrder AutoCloseable |  Minor | . | Benedict | Benedict |
| [CASSANDRA-6821](https://issues.apache.org/jira/browse/CASSANDRA-6821) | Cassandra can't delete snapshots for keyspaces that no longer exist. |  Major | . | Nick Bailey | Lyuben Todorov |
| [CASSANDRA-6921](https://issues.apache.org/jira/browse/CASSANDRA-6921) | Remove "adjusted op rate" from cassandra-stress stats logging |  Trivial | Tools | Benedict | Benedict |
| [CASSANDRA-6941](https://issues.apache.org/jira/browse/CASSANDRA-6941) | Optimized CF.hasColumns() implementation |  Minor | . | Benedict | Aleksey Yeschenko |
| [CASSANDRA-6953](https://issues.apache.org/jira/browse/CASSANDRA-6953) | Optimise CounterCell#reconcile |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6906](https://issues.apache.org/jira/browse/CASSANDRA-6906) | Skip Replica Calculation for Range Slice on LocalStrategy Keyspace |  Minor | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-6972](https://issues.apache.org/jira/browse/CASSANDRA-6972) | Throw an ERROR when auto\_bootstrap: true and bootstrapping node is listed in seeds |  Minor | . | Darla Baker | Brandon Williams |
| [CASSANDRA-6880](https://issues.apache.org/jira/browse/CASSANDRA-6880) | counters++ lock on cells, not partitions |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6888](https://issues.apache.org/jira/browse/CASSANDRA-6888) | Store whether a counter sstable still use some local/remote shards in the sstable metadata |  Major | . | Sylvain Lebresne | Aleksey Yeschenko |
| [CASSANDRA-6991](https://issues.apache.org/jira/browse/CASSANDRA-6991) | cql3 grammar generation can fail due to ANTLR timeout. |  Trivial | . | Ben Chan | Ben Chan |
| [CASSANDRA-6961](https://issues.apache.org/jira/browse/CASSANDRA-6961) | nodes should go into hibernate when join\_ring is false |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-6747](https://issues.apache.org/jira/browse/CASSANDRA-6747) | MessagingService should handle failures on remote nodes. |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-7024](https://issues.apache.org/jira/browse/CASSANDRA-7024) | Create snapshot selectively during sequential repair |  Minor | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-6487](https://issues.apache.org/jira/browse/CASSANDRA-6487) | Log WARN on large batch sizes |  Minor | . | Patrick McFadin | Lyuben Todorov |
| [CASSANDRA-6764](https://issues.apache.org/jira/browse/CASSANDRA-6764) | Using Batch commitlog\_sync is slow and doesn't actually batch writes |  Major | . | John Carrino | John Carrino |
| [CASSANDRA-6919](https://issues.apache.org/jira/browse/CASSANDRA-6919) | Use OpOrder to guard sstable references for reads, instead of acquiring/releasing references |  Minor | . | Benedict | Benedict |
| [CASSANDRA-7031](https://issues.apache.org/jira/browse/CASSANDRA-7031) | Increase default commit log total space + segment size |  Trivial | . | Benedict | Benedict |
| [CASSANDRA-7047](https://issues.apache.org/jira/browse/CASSANDRA-7047) | TriggerExecutor should group mutations by row key |  Major | . | Sergio Bossa | Aleksey Yeschenko |
| [CASSANDRA-7078](https://issues.apache.org/jira/browse/CASSANDRA-7078) | Make sstable2json output more readable |  Minor | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-7087](https://issues.apache.org/jira/browse/CASSANDRA-7087) | Use JMX\_PORT for the RMI port to simplify nodetool connectivity |  Minor | Configuration | Chris Lohfink | Chris Lohfink |
| [CASSANDRA-5547](https://issues.apache.org/jira/browse/CASSANDRA-5547) | Multi-threaded scrub, cleanup and upgradesstables |  Major | Tools | Benjamin Coverston | Russell Spitzer |
| [CASSANDRA-7090](https://issues.apache.org/jira/browse/CASSANDRA-7090) | Add ability to set/get logging levels to nodetool |  Minor | Tools | Jackson Chung | Jackson Chung |
| [CASSANDRA-7072](https://issues.apache.org/jira/browse/CASSANDRA-7072) | Allow better specification of rack properties file |  Minor | Configuration | Patrick Ziegler | Patrick Ziegler |
| [CASSANDRA-7119](https://issues.apache.org/jira/browse/CASSANDRA-7119) | Optimise isLive(System.currentTimeMillis()) + minor Cell cleanup |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6694](https://issues.apache.org/jira/browse/CASSANDRA-6694) | Slightly More Off-Heap Memtables |  Major | . | Benedict | Benedict |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6741](https://issues.apache.org/jira/browse/CASSANDRA-6741) | cqlsh DESCRIBE KEYSPACE returns "'NoneType' object has no attribute 'get\_usertypes\_names'" |  Major | . | Ryan McGuire | Mikhail Stepura |
| [CASSANDRA-6739](https://issues.apache.org/jira/browse/CASSANDRA-6739) | Exception on nodetool cfstats |  Major | . | Mateusz Gajewski | Mikhail Stepura |
| [CASSANDRA-6742](https://issues.apache.org/jira/browse/CASSANDRA-6742) | Make it safe to concurrently access ABSC after its construction |  Critical | . | Ryan McGuire | Aleksey Yeschenko |
| [CASSANDRA-6753](https://issues.apache.org/jira/browse/CASSANDRA-6753) | Cassandra2.1~beta1 Stall at Boot |  Major | . | David Chia | Benedict |
| [CASSANDRA-6705](https://issues.apache.org/jira/browse/CASSANDRA-6705) | ALTER TYPE \<type\> RENAME \<field\> fails sometime with java.lang.AssertionError: null |  Minor | CQL | Mikhail Stepura | Sylvain Lebresne |
| [CASSANDRA-6683](https://issues.apache.org/jira/browse/CASSANDRA-6683) | BADNESS\_THRESHOLD does not working correctly with DynamicEndpointSnitch |  Major | . | Kirill Bogdanov | Tyler Hobbs |
| [CASSANDRA-6770](https://issues.apache.org/jira/browse/CASSANDRA-6770) | failure to decode multiple UDT |  Major | Tools | Jonathan Ellis | Mikhail Stepura |
| [CASSANDRA-6762](https://issues.apache.org/jira/browse/CASSANDRA-6762) | 2.1-beta1 missing cassandra-driver-core jar in -bin.tar.gz |  Minor | Packaging | Michael Shuler | Michael Shuler |
| [CASSANDRA-6785](https://issues.apache.org/jira/browse/CASSANDRA-6785) | clean out populate\_io\_cache\_on\_flush option |  Minor | Configuration | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-6791](https://issues.apache.org/jira/browse/CASSANDRA-6791) | CompressedSequentialWriter can write zero-length segments during scrub |  Minor | . | Jonathan Ellis | Viktor Kuzmin |
| [CASSANDRA-6797](https://issues.apache.org/jira/browse/CASSANDRA-6797) | compaction and scrub data directories race on startup |  Minor | Compaction, Lifecycle | Matt Byrd | Joshua McKenzie |
| [CASSANDRA-5201](https://issues.apache.org/jira/browse/CASSANDRA-5201) | Cassandra/Hadoop does not support current Hadoop releases |  Major | . | Brian Jeltema | Benjamin Coverston |
| [CASSANDRA-6808](https://issues.apache.org/jira/browse/CASSANDRA-6808) | Possibly repairing with verbose nodes |  Minor | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-6838](https://issues.apache.org/jira/browse/CASSANDRA-6838) | FileCacheService overcounting its memoryUsage |  Minor | . | Benedict | Benedict |
| [CASSANDRA-6745](https://issues.apache.org/jira/browse/CASSANDRA-6745) | Require specifying rows\_per\_partition\_to\_cache |  Trivial | . | Jonathan Ellis | Marcus Eriksson |
| [CASSANDRA-6844](https://issues.apache.org/jira/browse/CASSANDRA-6844) | Expired cells not converted to deleted cells during cleanup/scrub/compaction (sometimes) |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6779](https://issues.apache.org/jira/browse/CASSANDRA-6779) | BooleanType is not too boolean |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6847](https://issues.apache.org/jira/browse/CASSANDRA-6847) | The binary transport doesn't load truststore file |  Minor | . | Mikhail Stepura | Mikhail Stepura |
| [CASSANDRA-6773](https://issues.apache.org/jira/browse/CASSANDRA-6773) | Delimiter not working for special characters in COPY command from CQLSH |  Trivial | . | Shiti Saxena | Shiti Saxena |
| [CASSANDRA-6850](https://issues.apache.org/jira/browse/CASSANDRA-6850) | cqlsh won't startup |  Minor | Tools | Chander Pechetty | Mikhail Stepura |
| [CASSANDRA-6848](https://issues.apache.org/jira/browse/CASSANDRA-6848) | stress (2.1) spams console with java.util.NoSuchElementException when run against nodes recently created |  Major | . | Russ Hatch | Benedict |
| [CASSANDRA-6834](https://issues.apache.org/jira/browse/CASSANDRA-6834) | cassandra-stress should fail if the same option is provided multiple times |  Minor | . | Benedict | Benedict |
| [CASSANDRA-6557](https://issues.apache.org/jira/browse/CASSANDRA-6557) | CommitLogSegment may be duplicated in unlikely race scenario |  Minor | . | Benedict | Benedict |
| [CASSANDRA-6860](https://issues.apache.org/jira/browse/CASSANDRA-6860) | Race condition in Batch CLE |  Major | . | Benedict | Benedict |
| [CASSANDRA-6856](https://issues.apache.org/jira/browse/CASSANDRA-6856) | StorageProxy registers new instance with JMX not this instance |  Minor | . | Edward Capriolo | Edward Capriolo |
| [CASSANDRA-6376](https://issues.apache.org/jira/browse/CASSANDRA-6376) | Pig tests are failing |  Minor | . | Sylvain Lebresne | Alex Liu |
| [CASSANDRA-6864](https://issues.apache.org/jira/browse/CASSANDRA-6864) | Proceed with truncate, if there are unreachable non storage nodes |  Major | . | Oleg Anastasyev | Oleg Anastasyev |
| [CASSANDRA-6800](https://issues.apache.org/jira/browse/CASSANDRA-6800) | ant codecoverage no longer works |  Minor | Testing | Edward Capriolo | Mikhail Stepura |
| [CASSANDRA-6613](https://issues.apache.org/jira/browse/CASSANDRA-6613) | Java 7u51 breaks internode encryption |  Major | . | Ray Sinnema | Brandon Williams |
| [CASSANDRA-6793](https://issues.apache.org/jira/browse/CASSANDRA-6793) | NPE in Hadoop Word count example |  Minor | . | Chander Pechetty | Chander Pechetty |
| [CASSANDRA-6646](https://issues.apache.org/jira/browse/CASSANDRA-6646) | Disk Failure Policy ignores CorruptBlockException |  Minor | . | sankalp kohli | Marcus Eriksson |
| [CASSANDRA-6774](https://issues.apache.org/jira/browse/CASSANDRA-6774) | Cleanup fails with assertion error after stopping previous run |  Major | . | Keith Wright | Marcus Eriksson |
| [CASSANDRA-6514](https://issues.apache.org/jira/browse/CASSANDRA-6514) | Nodetool Refresh / CFS.loadNewSSTables() can Lose New SSTables |  Minor | Tools | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-6884](https://issues.apache.org/jira/browse/CASSANDRA-6884) | AbstractPaxosCallback throws write timeout exception with SERIAL. |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-6104](https://issues.apache.org/jira/browse/CASSANDRA-6104) | Add additional limits in cassandra.conf provided by Debian package |  Trivial | Packaging | J.B. Langston | Brandon Williams |
| [CASSANDRA-6816](https://issues.apache.org/jira/browse/CASSANDRA-6816) | CQL3 docs don't mention BATCH UNLOGGED |  Minor | Documentation and Website | Duncan Sands | Tyler Hobbs |
| [CASSANDRA-6783](https://issues.apache.org/jira/browse/CASSANDRA-6783) | Collections should have a proper compare() method for UDT |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6883](https://issues.apache.org/jira/browse/CASSANDRA-6883) | stress read fails with IOException "Data returned was not validated" |  Major | Tools | Russ Hatch | Benedict |
| [CASSANDRA-6879](https://issues.apache.org/jira/browse/CASSANDRA-6879) | ConcurrentModificationException while doing range slice query. |  Major | . | Shao-Chuan Wang | Jonathan Ellis |
| [CASSANDRA-6168](https://issues.apache.org/jira/browse/CASSANDRA-6168) | nodetool status should issue a warning when no keyspace is specified |  Minor | Tools | Patricio Echague | Vijay |
| [CASSANDRA-6911](https://issues.apache.org/jira/browse/CASSANDRA-6911) | Netty dependency update broke stress |  Major | Tools | Ryan McGuire | Benedict |
| [CASSANDRA-6907](https://issues.apache.org/jira/browse/CASSANDRA-6907) | ignore snapshot repair flag on Windows |  Major | Tools | Jonathan Ellis | Joshua McKenzie |
| [CASSANDRA-6940](https://issues.apache.org/jira/browse/CASSANDRA-6940) | Consider removing user types rename for now |  Major | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6931](https://issues.apache.org/jira/browse/CASSANDRA-6931) | BatchLogManager shouldn't serialize mutations with version 1.2 in 2.1. |  Major | . | Sylvain Lebresne | Aleksey Yeschenko |
| [CASSANDRA-6869](https://issues.apache.org/jira/browse/CASSANDRA-6869) | Broken 1.2 sstables support in 2.1 |  Major | . | Aleksey Yeschenko | Sylvain Lebresne |
| [CASSANDRA-6822](https://issues.apache.org/jira/browse/CASSANDRA-6822) | Never ending batch replay after dropping column family |  Major | . | Duncan Sands | Aleksey Yeschenko |
| [CASSANDRA-6913](https://issues.apache.org/jira/browse/CASSANDRA-6913) | Compaction of system keyspaces during startup can cause early loading of non-system keyspaces |  Minor | . | Benedict | Benedict |
| [CASSANDRA-6825](https://issues.apache.org/jira/browse/CASSANDRA-6825) | Slice Queries Can Skip Intersecting SSTables |  Major | . | Bill Mitchell | Tyler Hobbs |
| [CASSANDRA-6943](https://issues.apache.org/jira/browse/CASSANDRA-6943) | UpdateFunction.abortEarly can cause BTree.update to leave its Builder in a bad state, which affects future operations |  Major | . | Russ Hatch | Benedict |
| [CASSANDRA-6978](https://issues.apache.org/jira/browse/CASSANDRA-6978) | Table creation does not add all the columns until schema is updated again |  Major | . | Ryan McGuire | Tyler Hobbs |
| [CASSANDRA-6818](https://issues.apache.org/jira/browse/CASSANDRA-6818) | SSTable references not released if stream session fails before it starts |  Major | . | Richard Low | Yuki Morishita |
| [CASSANDRA-5995](https://issues.apache.org/jira/browse/CASSANDRA-5995) | Cassandra-shuffle causes NumberFormatException |  Major | Tools | William Montaz | Lyuben Todorov |
| [CASSANDRA-6920](https://issues.apache.org/jira/browse/CASSANDRA-6920) | LatencyMetrics can return infinity |  Major | . | Nick Bailey | Benedict |
| [CASSANDRA-6912](https://issues.apache.org/jira/browse/CASSANDRA-6912) | SSTableReader.isReplaced does not allow for safe resource cleanup |  Major | . | Benedict | Benedict |
| [CASSANDRA-6957](https://issues.apache.org/jira/browse/CASSANDRA-6957) | testNewRepairedSSTable fails intermittently |  Major | . | Jonathan Ellis | Marcus Eriksson |
| [CASSANDRA-6948](https://issues.apache.org/jira/browse/CASSANDRA-6948) | After Bootstrap or Replace node startup, EXPIRING\_MAP\_REAPER is shutdown and cannot be restarted, causing callbacks to collect indefinitely |  Major | . | Keith Wright | Brandon Williams |
| [CASSANDRA-6971](https://issues.apache.org/jira/browse/CASSANDRA-6971) | Schedule schema pulls onChange |  Major | . | Russ Hatch | Brandon Williams |
| [CASSANDRA-6405](https://issues.apache.org/jira/browse/CASSANDRA-6405) | When making heavy use of counters, neighbor nodes occasionally enter spiral of constant memory consumpion |  Major | . | Jason Harvey |  |
| [CASSANDRA-7025](https://issues.apache.org/jira/browse/CASSANDRA-7025) | RejectedExecutionException when stopping a node after drain |  Trivial | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-7015](https://issues.apache.org/jira/browse/CASSANDRA-7015) | sstableloader NPE |  Major | . | Ryan McGuire | Benedict |
| [CASSANDRA-6924](https://issues.apache.org/jira/browse/CASSANDRA-6924) | Data Inserted Immediately After Secondary Index Creation is not Indexed |  Major | Secondary Indexes | Tyler Hobbs | Sam Tunnicliffe |
| [CASSANDRA-6999](https://issues.apache.org/jira/browse/CASSANDRA-6999) | Batchlog replay should account for CF truncation records |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-7018](https://issues.apache.org/jira/browse/CASSANDRA-7018) | cqlsh failing to handle utf8 decode errors in certain cases |  Minor | Tools | Colin B. | Mikhail Stepura |
| [CASSANDRA-7064](https://issues.apache.org/jira/browse/CASSANDRA-7064) | snapshot creation for non-system keyspaces snapshots to the flush and data dirs |  Blocker | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-7038](https://issues.apache.org/jira/browse/CASSANDRA-7038) | Nodetool rebuild\_index requires named indexes argument |  Trivial | Tools | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-6841](https://issues.apache.org/jira/browse/CASSANDRA-6841) | ConcurrentModificationException in commit-log-writer after local schema reset |  Minor | . | Pas | Benedict |
| [CASSANDRA-7050](https://issues.apache.org/jira/browse/CASSANDRA-7050) | AbstractColumnFamilyInputFormat & AbstractColumnFamilyOutputFormat throw NPE if username is provided but password is null |  Minor | . | Mike Adamson | Mike Adamson |
| [CASSANDRA-6949](https://issues.apache.org/jira/browse/CASSANDRA-6949) | Performance regression in tombstone heavy workloads |  Major | . | Jeremiah Jordan | Sam Tunnicliffe |
| [CASSANDRA-6939](https://issues.apache.org/jira/browse/CASSANDRA-6939) | LOCAL\_SERIAL use QUORAM consistency level to read rows in the read path |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-7043](https://issues.apache.org/jira/browse/CASSANDRA-7043) | CommitLogArchiver thread pool name inconsistent with others |  Trivial | . | Chris Lohfink | Chris Lohfink |
| [CASSANDRA-7058](https://issues.apache.org/jira/browse/CASSANDRA-7058) | HHOM and BM direct delivery should not cause hints to be written on timeout |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6994](https://issues.apache.org/jira/browse/CASSANDRA-6994) | SerializingCache doesn't always cleanup its references in case of exception |  Minor | . | Benedict | Benedict |
| [CASSANDRA-7089](https://issues.apache.org/jira/browse/CASSANDRA-7089) | Some problems with typed ByteBuffer comparisons? |  Minor | . | Benedict | Sylvain Lebresne |
| [CASSANDRA-6987](https://issues.apache.org/jira/browse/CASSANDRA-6987) | sstablesplit fails in 2.1 |  Major | . | Michael Shuler | Benedict |
| [CASSANDRA-7091](https://issues.apache.org/jira/browse/CASSANDRA-7091) | java.lang.NullPointerException  at org.apache.cassandra.db.ColumnFamilyStore.scrubDataDirectories(ColumnFamilyStore.java:437) |  Major | . | Vivek Mishra | Vivek Mishra |
| [CASSANDRA-2434](https://issues.apache.org/jira/browse/CASSANDRA-2434) | range movements can violate consistency |  Major | Streaming and Messaging | Peter Schuller | T Jake Luciani |
| [CASSANDRA-6831](https://issues.apache.org/jira/browse/CASSANDRA-6831) | Updates to COMPACT STORAGE tables via cli drop CQL information |  Minor | . | Russell Bradberry | Sylvain Lebresne |
| [CASSANDRA-6916](https://issues.apache.org/jira/browse/CASSANDRA-6916) | Preemptive opening of compaction result |  Minor | . | Benedict | Benedict |
| [CASSANDRA-6788](https://issues.apache.org/jira/browse/CASSANDRA-6788) | Race condition silently kills thrift server |  Major | . | Christian Rolf | Christian Rolf |
| [CASSANDRA-6541](https://issues.apache.org/jira/browse/CASSANDRA-6541) | New versions of Hotspot create new Class objects on every JMX connection causing the heap to fill up with them if CMSClassUnloadingEnabled isn't set. |  Minor | Configuration | jonathan lacefield | Brandon Williams |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6771](https://issues.apache.org/jira/browse/CASSANDRA-6771) | RangeTombstoneTest is failing on 2.1/trunk |  Minor | . | Yuki Morishita |  |
| [CASSANDRA-7009](https://issues.apache.org/jira/browse/CASSANDRA-7009) | topology\_test dtest fails in 2.1 |  Blocker | Testing | Michael Shuler | Brandon Williams |
| [CASSANDRA-7002](https://issues.apache.org/jira/browse/CASSANDRA-7002) | concurrent\_schema\_changes\_test snapshot\_test dtest needs to account for hashed data dirs in 2.1 |  Blocker | Testing | Michael Shuler | Brandon Williams |
| [CASSANDRA-6553](https://issues.apache.org/jira/browse/CASSANDRA-6553) | Benchmark counter improvements (counters++) |  Major | . | Ryan McGuire | Russ Hatch |
| [CASSANDRA-7109](https://issues.apache.org/jira/browse/CASSANDRA-7109) | Create replace\_address dtest |  Major | . | Ryan McGuire | Shawn Kumar |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6900](https://issues.apache.org/jira/browse/CASSANDRA-6900) | Remove unnecessary repair JMX interface |  Trivial | . | Yuki Morishita | Yuki Morishita |


