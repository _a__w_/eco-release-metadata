
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.11.3 - Unreleased (as of 2018-10-20)



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-14183](https://issues.apache.org/jira/browse/CASSANDRA-14183) | CVE-2017-5929 Security vulnerability and redefine default log rotation policy |  Major | Libraries | Thiago Veronezi | Thiago Veronezi |
| [CASSANDRA-14163](https://issues.apache.org/jira/browse/CASSANDRA-14163) | RateBasedBackPressure unnecessarily invokes a lock on the Guava RateLimiter |  Minor | Core | Nate McCall | Nate McCall |
| [CASSANDRA-14210](https://issues.apache.org/jira/browse/CASSANDRA-14210) | Optimize SSTables upgrade task scheduling |  Major | Compaction | Oleksandr Shulgin | Kurt Greaves |
| [CASSANDRA-13697](https://issues.apache.org/jira/browse/CASSANDRA-13697) | CDC and VIEW writeType missing from spec for write\_timeout / write\_failure |  Minor | Documentation and Website | Andy Tolbert | Vinay Chella |
| [CASSANDRA-14247](https://issues.apache.org/jira/browse/CASSANDRA-14247) | SASI tokenizer for simple delimiter based entries |  Major | sasi | mck | mck |
| [CASSANDRA-14370](https://issues.apache.org/jira/browse/CASSANDRA-14370) | Reduce level of log from debug to trace in CommitLogSegmentManager.java |  Trivial | . | Jay Zhuang | Nicolas Guyomar |
| [CASSANDRA-14202](https://issues.apache.org/jira/browse/CASSANDRA-14202) | Assertion error on sstable open during startup should invoke disk failure policy |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-14310](https://issues.apache.org/jira/browse/CASSANDRA-14310) | Don't allow nodetool refresh before cfs is opened |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-13910](https://issues.apache.org/jira/browse/CASSANDRA-13910) | Remove read\_repair\_chance/dclocal\_read\_repair\_chance |  Minor | . | Sylvain Lebresne | Aleksey Yeschenko |
| [CASSANDRA-14379](https://issues.apache.org/jira/browse/CASSANDRA-14379) | Better handling of missing partition columns in system\_schema.columns during startup |  Major | Distributed Metadata | Jay Zhuang | Jay Zhuang |
| [CASSANDRA-14428](https://issues.apache.org/jira/browse/CASSANDRA-14428) | Run ant eclipse-warnings in circleci |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-14416](https://issues.apache.org/jira/browse/CASSANDRA-14416) | Remove string formatting lines from hot path |  Minor | Core | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-14447](https://issues.apache.org/jira/browse/CASSANDRA-14447) | Cleanup StartupClusterConnectivityChecker and PING Verb |  Minor | . | Jason Brown | Jason Brown |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12763](https://issues.apache.org/jira/browse/CASSANDRA-12763) | Compaction performance issues when a table has a lot of sstables |  Major | Compaction | Tom van der Woerdt | Marcus Eriksson |
| [CASSANDRA-13686](https://issues.apache.org/jira/browse/CASSANDRA-13686) | Fix documentation typo |  Trivial | Documentation and Website | An Wu | Michael Shuler |
| [CASSANDRA-14194](https://issues.apache.org/jira/browse/CASSANDRA-14194) | Chain commit log marker potential performance regression in batch commit mode |  Major | Core, Testing | Ariel Weisberg | Jason Brown |
| [CASSANDRA-14251](https://issues.apache.org/jira/browse/CASSANDRA-14251) | View replica is not written to pending endpoint when base replica is also view replica |  Major | Materialized Views | Paulo Motta | Paulo Motta |
| [CASSANDRA-14292](https://issues.apache.org/jira/browse/CASSANDRA-14292) | Fix batch commitlog sync regression |  Major | . | ZhaoYang | ZhaoYang |
| [CASSANDRA-14166](https://issues.apache.org/jira/browse/CASSANDRA-14166) | sstabledump tries to delete a file |  Major | Tools | Python\_Max | Kurt Greaves |
| [CASSANDRA-14087](https://issues.apache.org/jira/browse/CASSANDRA-14087) | NPE when CAS encounters empty frozen collection |  Major | . | Jens Bannmann | Kurt Greaves |
| [CASSANDRA-11163](https://issues.apache.org/jira/browse/CASSANDRA-11163) | Summaries are needlessly rebuilt when the BF FP ratio is changed |  Major | Core | Brandon Williams | Kurt Greaves |
| [CASSANDRA-14170](https://issues.apache.org/jira/browse/CASSANDRA-14170) | Loss of digits when doing CAST from varint/bigint to decimal |  Minor | CQL | Daniel Fiala | Benjamin Lerer |
| [CASSANDRA-14215](https://issues.apache.org/jira/browse/CASSANDRA-14215) | Cassandra does not respect hint window for CAS |  Major | Hints, Streaming and Messaging | Arijit Banerjee | Kurt Greaves |
| [CASSANDRA-14330](https://issues.apache.org/jira/browse/CASSANDRA-14330) | Handle repeat open bound from SRP in read repair |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-13396](https://issues.apache.org/jira/browse/CASSANDRA-13396) | Cassandra 3.10: ClassCastException in ThreadAwareSecurityManager |  Minor | . | Edward Capriolo | Eric Hubert |
| [CASSANDRA-14245](https://issues.apache.org/jira/browse/CASSANDRA-14245) | SELECT JSON prints null on empty strings |  Major | CQL | Norbert Schultz | Francisco Fernandez |
| [CASSANDRA-14080](https://issues.apache.org/jira/browse/CASSANDRA-14080) | Handle incompletely written hint descriptors during startup |  Minor | Hints | Aleksandr Ivanov | Alex Lourie |
| [CASSANDRA-13891](https://issues.apache.org/jira/browse/CASSANDRA-13891) | fromJson(null) throws java.lang.NullPointerException on Cassandra |  Minor | CQL | Marcel Villet | Edward Ribeiro |
| [CASSANDRA-14284](https://issues.apache.org/jira/browse/CASSANDRA-14284) | Chunk checksum test needs to occur before uncompress to avoid JVM crash |  Major | Core | Gil Tene | Benjamin Lerer |
| [CASSANDRA-14387](https://issues.apache.org/jira/browse/CASSANDRA-14387) | SSTableReaderTest#testOpeningSSTable fails on macOS |  Minor | . | Dinesh Joshi | Dinesh Joshi |
| [CASSANDRA-14299](https://issues.apache.org/jira/browse/CASSANDRA-14299) | cqlsh: ssl setting not read from cqlshrc in 3.11 |  Major | . | Christian Becker | Stefan Podkowinski |
| [CASSANDRA-14286](https://issues.apache.org/jira/browse/CASSANDRA-14286) | IndexOutOfBoundsException with SELECT JSON using IN and ORDER BY |  Major | CQL | Szymon Acedański | Francisco Fernandez |
| [CASSANDRA-13851](https://issues.apache.org/jira/browse/CASSANDRA-13851) | Allow existing nodes to use all peers in shadow round |  Major | Lifecycle | Kurt Greaves | Kurt Greaves |
| [CASSANDRA-14332](https://issues.apache.org/jira/browse/CASSANDRA-14332) | Fix unbounded validation compactions on repair |  Major | . | Kurt Greaves | Kurt Greaves |
| [CASSANDRA-14359](https://issues.apache.org/jira/browse/CASSANDRA-14359) | CREATE TABLE fails if there is a column called "default" with Cassandra 3.11.2 |  Minor | CQL, Documentation and Website | Andy Klages | Andrés de la Peña |
| [CASSANDRA-14411](https://issues.apache.org/jira/browse/CASSANDRA-14411) | Use Bounds instead of Range to represent sstable first/last token when checking how to anticompact sstables |  Major | Repair | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-13740](https://issues.apache.org/jira/browse/CASSANDRA-13740) | Orphan hint file gets created while node is being removed from cluster |  Minor | Core, Hints | Jaydeepkumar Chovatia | Jaydeepkumar Chovatia |
| [CASSANDRA-12743](https://issues.apache.org/jira/browse/CASSANDRA-12743) | Assertion error while running compaction |  Major | Compaction | Jean-Baptiste Le Duigou | Jay Zhuang |
| [CASSANDRA-12244](https://issues.apache.org/jira/browse/CASSANDRA-12244) | progress in compactionstats is reported wrongly for view builds |  Minor | . | Tom van der Woerdt | ZhaoYang |
| [CASSANDRA-12271](https://issues.apache.org/jira/browse/CASSANDRA-12271) | NonSystemKeyspaces jmx attribute needs to return jre list |  Major | . | Chris Lohfink | Edward Ribeiro |
| [CASSANDRA-12793](https://issues.apache.org/jira/browse/CASSANDRA-12793) | invalid jvm type and architecture [cassandra-env.sh] |  Minor | Configuration | Ali Ebrahiminejad | Stefan Podkowinski |
| [CASSANDRA-12924](https://issues.apache.org/jira/browse/CASSANDRA-12924) | GraphiteReporter does not reconnect if graphite restarts |  Major | . | Stefano Ortolani | mck |
| [CASSANDRA-14418](https://issues.apache.org/jira/browse/CASSANDRA-14418) | Cassandra not starting when using enhanced startup scripts in windows |  Major | . | Shyam Phirke | Shyam Phirke |
| [CASSANDRA-14055](https://issues.apache.org/jira/browse/CASSANDRA-14055) | Index redistribution breaks SASI index |  Major | sasi | Ludovic Boutros | Jordan West |
| [CASSANDRA-11551](https://issues.apache.org/jira/browse/CASSANDRA-11551) | Incorrect counting of pending messages in OutboundTcpConnection |  Minor | . | Robert Stupp | Jaydeepkumar Chovatia |
| [CASSANDRA-14450](https://issues.apache.org/jira/browse/CASSANDRA-14450) | DelimiterAnalyzer: IllegalArgumentException: The key argument was zero-length |  Major | sasi | mck | mck |
| [CASSANDRA-14475](https://issues.apache.org/jira/browse/CASSANDRA-14475) | nodetool - Occasional high CPU on large, CPU capable machines |  Major | Tools | Thomas Steinmaurer | Thomas Steinmaurer |
| [CASSANDRA-14422](https://issues.apache.org/jira/browse/CASSANDRA-14422) | Missing dependencies airline and ohc-core-j8 for pom-all |  Minor | Build | Shichao An | Shichao An |
| [CASSANDRA-14451](https://issues.apache.org/jira/browse/CASSANDRA-14451) | Infinity ms Commit Log Sync |  Minor | . | Harry Hough | Jason Brown |
| [CASSANDRA-13929](https://issues.apache.org/jira/browse/CASSANDRA-13929) | BTree$Builder / io.netty.util.Recycler$Stack leaking memory |  Major | Core | Thomas Steinmaurer | Jay Zhuang |
| [CASSANDRA-14513](https://issues.apache.org/jira/browse/CASSANDRA-14513) | Reverse order queries in presence of range tombstones may cause permanent data loss |  Blocker | Core, CQL, Local Write-Read Paths | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-14515](https://issues.apache.org/jira/browse/CASSANDRA-14515) | Short read protection in presence of almost-purgeable range tombstones may cause permanent data loss |  Blocker | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-13669](https://issues.apache.org/jira/browse/CASSANDRA-13669) | Error when starting cassandra: Unable to make UUID from 'aa' (SASI index) |  Critical | sasi | Lukasz Biedrycki | ZhaoYang |
| [CASSANDRA-14423](https://issues.apache.org/jira/browse/CASSANDRA-14423) | SSTables stop being compacted |  Blocker | Compaction | Kurt Greaves | Kurt Greaves |
| [CASSANDRA-14167](https://issues.apache.org/jira/browse/CASSANDRA-14167) | IndexOutOfBoundsException when selecting column counter and consistency quorum |  Major | Coordination | Tristan Last | Francisco Fernandez |
| [CASSANDRA-14568](https://issues.apache.org/jira/browse/CASSANDRA-14568) | Static collection deletions are corrupted in 3.0 -\> 2.{1,2} messages |  Critical | . | Benedict | Benedict |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13698](https://issues.apache.org/jira/browse/CASSANDRA-13698) | Reinstate or get rid of unit tests with multiple compaction strategies |  Minor | Testing | Paulo Motta | Lerh Chuan Low |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-14240](https://issues.apache.org/jira/browse/CASSANDRA-14240) | Backport circleci yaml |  Trivial | . | Jason Brown | Jason Brown |


