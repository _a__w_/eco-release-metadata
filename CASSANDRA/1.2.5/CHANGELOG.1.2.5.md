
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.2.5 - 2013-05-19



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5459](https://issues.apache.org/jira/browse/CASSANDRA-5459) | Remove node from seeds list when it permanently leaves the cluster |  Minor | . | Jason Brown | Jason Brown |
| [CASSANDRA-5495](https://issues.apache.org/jira/browse/CASSANDRA-5495) | Do not hard code current version into BootstrapTest |  Trivial | Testing | Jason Brown | Jason Brown |
| [CASSANDRA-5490](https://issues.apache.org/jira/browse/CASSANDRA-5490) | Avoid using CFMetaData.compile(int oldCfId, ...) for post-1.1 system cfs. |  Trivial | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5506](https://issues.apache.org/jira/browse/CASSANDRA-5506) | Reduce memory consumption of IndexSummary |  Major | . | Nick Puz | Jonathan Ellis |
| [CASSANDRA-4316](https://issues.apache.org/jira/browse/CASSANDRA-4316) | Compaction Throttle too bursty with large rows |  Major | . | Wayne Lewis | Jonathan Ellis |
| [CASSANDRA-5528](https://issues.apache.org/jira/browse/CASSANDRA-5528) | CLUSTERING ORDER BY support for cqlsh's DESCRIBE |  Minor | Tools | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-5556](https://issues.apache.org/jira/browse/CASSANDRA-5556) | Enabling/Disabling incremental backup via nodetool |  Trivial | Tools | Pavel Margolin | Pavel Margolin |
| [CASSANDRA-5562](https://issues.apache.org/jira/browse/CASSANDRA-5562) | sstablescrub should respect MAX\_HEAP\_SIZE |  Minor | Tools | Mina Naguib | Mina Naguib |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5447](https://issues.apache.org/jira/browse/CASSANDRA-5447) | Include fatal errors in trace events |  Minor | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5445](https://issues.apache.org/jira/browse/CASSANDRA-5445) | PerRowSecondaryIndex isn't notified of row-level deletes |  Major | . | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-5452](https://issues.apache.org/jira/browse/CASSANDRA-5452) | Allow empty blob literals in CQL3 |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5418](https://issues.apache.org/jira/browse/CASSANDRA-5418) | streaming fails |  Critical | . | Igor Ivanov | Sylvain Lebresne |
| [CASSANDRA-5456](https://issues.apache.org/jira/browse/CASSANDRA-5456) | Large number of bootstrapping nodes cause gossip to stop working |  Major | . | Oleg Kibirev | Oleg Kibirev |
| [CASSANDRA-5468](https://issues.apache.org/jira/browse/CASSANDRA-5468) | Prepared statements from default keyspace are broken |  Major | . | Pierre Chalamet | Aleksey Yeschenko |
| [CASSANDRA-5475](https://issues.apache.org/jira/browse/CASSANDRA-5475) | remove dead classes (ArrayUtil.java, CreationTimeAwareFuture.java) |  Trivial | . | Dave Brosius |  |
| [CASSANDRA-5496](https://issues.apache.org/jira/browse/CASSANDRA-5496) | Fix SemanticVersion.isSupportedBy patch/minor handling |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-4463](https://issues.apache.org/jira/browse/CASSANDRA-4463) | Nodes Don't Restart: Assertion Error on Serializing Cache provider |  Minor | . | Arya Goudarzi | Jonathan Ellis |
| [CASSANDRA-5393](https://issues.apache.org/jira/browse/CASSANDRA-5393) | Add retry mechanism to OTC for non-droppable\_verbs |  Major | . | Jeremiah Jordan | Jason Brown |
| [CASSANDRA-5497](https://issues.apache.org/jira/browse/CASSANDRA-5497) | Use allocator information to improve memtable memory usage estimate |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5424](https://issues.apache.org/jira/browse/CASSANDRA-5424) | nodetool repair -pr on all nodes won't repair the full range when a Keyspace isn't in all DC's |  Critical | . | Jeremiah Jordan | Yuki Morishita |
| [CASSANDRA-5471](https://issues.apache.org/jira/browse/CASSANDRA-5471) | Spelling and grammar errors in cassandra.yaml |  Trivial | . | Jeremy Hanna | Jeremy Hanna |
| [CASSANDRA-5507](https://issues.apache.org/jira/browse/CASSANDRA-5507) | The native protocol server is not correctly stopped on shutdown |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5512](https://issues.apache.org/jira/browse/CASSANDRA-5512) | forceTablePrimaryRange fails with nullpointer exception |  Minor | Tools | Tobias Grahn | Yuki Morishita |
| [CASSANDRA-5504](https://issues.apache.org/jira/browse/CASSANDRA-5504) | Eternal iteration when using older hadoop version due to next() call and empty key value |  Minor | . | Alex Petrov | Alex Petrov |
| [CASSANDRA-5467](https://issues.apache.org/jira/browse/CASSANDRA-5467) | isRunning flag set prematurely in org.apache.cassandra.transport.Server |  Minor | . | John Sanda | Sylvain Lebresne |
| [CASSANDRA-5472](https://issues.apache.org/jira/browse/CASSANDRA-5472) | Timeuuid with CLUSTERING ORDER DESC cannot be used with the dateOf CQL3 function |  Major | . | Gareth Collins | Sylvain Lebresne |
| [CASSANDRA-5531](https://issues.apache.org/jira/browse/CASSANDRA-5531) | Disallow renaming columns one at a time when when the table don't have CQL3 metadata yet |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5484](https://issues.apache.org/jira/browse/CASSANDRA-5484) | Support custom secondary indexes in CQL |  Major | Secondary Indexes | Benjamin Coverston | Aleksey Yeschenko |
| [CASSANDRA-5535](https://issues.apache.org/jira/browse/CASSANDRA-5535) | Manifest file not fsynced |  Minor | . | Terry Cumaranatunge | Marcus Eriksson |
| [CASSANDRA-5229](https://issues.apache.org/jira/browse/CASSANDRA-5229) | after a IOException is thrown during streaming, streaming tasks hang in netstats |  Minor | . | Michael Kjellman | Yuki Morishita |
| [CASSANDRA-5432](https://issues.apache.org/jira/browse/CASSANDRA-5432) | Repair Freeze/Gossip Invisibility Issues 1.2.4 |  Critical | . | Arya Goudarzi | Vijay |
| [CASSANDRA-5540](https://issues.apache.org/jira/browse/CASSANDRA-5540) | Concurrent secondary index updates remove rows from the index |  Major | Secondary Indexes | Alexei Bakanov | Sam Tunnicliffe |
| [CASSANDRA-5551](https://issues.apache.org/jira/browse/CASSANDRA-5551) | intersects the bounds not right |  Minor | . | yangwei | yangwei |
| [CASSANDRA-5554](https://issues.apache.org/jira/browse/CASSANDRA-5554) | Growing pending compactions |  Major | . | Oleg Anastasyev | Oleg Anastasyev |
| [CASSANDRA-5566](https://issues.apache.org/jira/browse/CASSANDRA-5566) | java/org/apache/cassandra/dht/BytesToken.java returns invalid string token (with prefix   Token(bytes[" + Hex.bytesToHex(token) + "])) |  Minor | . | Juraj Sustek | Juraj Sustek |
| [CASSANDRA-5567](https://issues.apache.org/jira/browse/CASSANDRA-5567) | DESCRIBE TABLES is empty after case insensitive use |  Trivial | Tools | Mike Bulman | Aleksey Yeschenko |
| [CASSANDRA-5564](https://issues.apache.org/jira/browse/CASSANDRA-5564) | fix memorySize bugs |  Major | . | Jonathan Ellis | Carl Yeksigian |


