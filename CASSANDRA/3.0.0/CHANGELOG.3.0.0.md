
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.0.0 - 2015-11-09



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-8068](https://issues.apache.org/jira/browse/CASSANDRA-8068) | Allow to create authenticator which is aware of the client connection |  Minor | Distributed Metadata | Jacek Lewandowski | Sam Tunnicliffe |
| [CASSANDRA-10513](https://issues.apache.org/jira/browse/CASSANDRA-10513) | Update cqlsh for new driver execution API |  Minor | Tools | Adam Holmberg | Adam Holmberg |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10524](https://issues.apache.org/jira/browse/CASSANDRA-10524) | Add ability to skip TIME\_WAIT sockets on port check on Windows startup |  Trivial | Packaging | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-10544](https://issues.apache.org/jira/browse/CASSANDRA-10544) | Make cqlsh tests work when authentication is configured |  Trivial | Testing, Tools | Adam Holmberg | Stefania |
| [CASSANDRA-10595](https://issues.apache.org/jira/browse/CASSANDRA-10595) | Don't initialize un-registered indexes |  Minor | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-10559](https://issues.apache.org/jira/browse/CASSANDRA-10559) | Support encrypted and plain traffic on the same port |  Major | . | Norman Maurer | Norman Maurer |
| [CASSANDRA-9526](https://issues.apache.org/jira/browse/CASSANDRA-9526) | Provide a JMX hook to monitor phi values in the FailureDetector |  Major | . | Ron Kuris | Ron Kuris |
| [CASSANDRA-10365](https://issues.apache.org/jira/browse/CASSANDRA-10365) | Store types by their CQL names in schema tables instead of fully-qualified internal class names |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-7645](https://issues.apache.org/jira/browse/CASSANDRA-7645) | cqlsh: show partial trace if incomplete after max\_trace\_wait |  Trivial | Tools | Tyler Hobbs | Carl Yeksigian |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10377](https://issues.apache.org/jira/browse/CASSANDRA-10377) | AssertionError: attempted to delete non-existing file CommitLog |  Critical | . | Vovodroid | Vovodroid |
| [CASSANDRA-10545](https://issues.apache.org/jira/browse/CASSANDRA-10545) | JDK bug from CASSANDRA-8220 makes drain die early also |  Trivial | . | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-10264](https://issues.apache.org/jira/browse/CASSANDRA-10264) | Unable to use conditions on static columns for DELETE |  Major | CQL | DOAN DuyHai | Benjamin Lerer |
| [CASSANDRA-10381](https://issues.apache.org/jira/browse/CASSANDRA-10381) | NullPointerException in cqlsh paging through CF with static columns |  Major | CQL | Michael Keeney | Benjamin Lerer |
| [CASSANDRA-10367](https://issues.apache.org/jira/browse/CASSANDRA-10367) | Aggregate with Initial Condition fails with C\* 3.0 |  Major | . | Greg Bestland | Robert Stupp |
| [CASSANDRA-10251](https://issues.apache.org/jira/browse/CASSANDRA-10251) | JVM\_OPTS repetition when started from init script |  Major | Packaging | Eric Evans | Eric Evans |
| [CASSANDRA-10336](https://issues.apache.org/jira/browse/CASSANDRA-10336) | SinglePartitionSliceCommandTest.staticColumnsAreReturned flappy (3.0) |  Major | . | Robert Stupp | Blake Eggleston |
| [CASSANDRA-10360](https://issues.apache.org/jira/browse/CASSANDRA-10360) | UnsupportedOperationException when compacting system.size\_estimates after 2.1 -\> 2.2 -\> 3.0 upgrade |  Blocker | . | Andrew Hust | Sylvain Lebresne |
| [CASSANDRA-10561](https://issues.apache.org/jira/browse/CASSANDRA-10561) | Add a check to cqlsh to require Python-2.7 for version \>= 2.2 |  Minor | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-10421](https://issues.apache.org/jira/browse/CASSANDRA-10421) | Potential issue with LogTransaction as it only checks in a single directory for files |  Blocker | Local Write-Read Paths | Marcus Eriksson | Stefania |
| [CASSANDRA-10562](https://issues.apache.org/jira/browse/CASSANDRA-10562) | RolesCache should not be created for any authenticator that does not requireAuthentication |  Major | . | Mike Adamson | Mike Adamson |
| [CASSANDRA-5261](https://issues.apache.org/jira/browse/CASSANDRA-5261) | Remove token generator |  Minor | Tools | Jonathan Ellis | Robert Stupp |
| [CASSANDRA-10079](https://issues.apache.org/jira/browse/CASSANDRA-10079) | LEAK DETECTED, after nodetool drain |  Major | Lifecycle | Sebastian Estevez | Yuki Morishita |
| [CASSANDRA-10569](https://issues.apache.org/jira/browse/CASSANDRA-10569) | Keyspace validation errors are getting lost in system\_add\_keyspace |  Major | Distributed Metadata | Mike Adamson | Sam Tunnicliffe |
| [CASSANDRA-10554](https://issues.apache.org/jira/browse/CASSANDRA-10554) | Batch that updates two or more table can produce unreadable SSTable (was: Auto Bootstraping a new node fails) |  Blocker | . | Alan Boudreault | Sylvain Lebresne |
| [CASSANDRA-10601](https://issues.apache.org/jira/browse/CASSANDRA-10601) | SecondaryIndexManager incorrectly updates index build status system table |  Major | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-10606](https://issues.apache.org/jira/browse/CASSANDRA-10606) | PartitionUpdate.operationCount() return the wrong number of operation for compact tables |  Major | Local Write-Read Paths | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-10576](https://issues.apache.org/jira/browse/CASSANDRA-10576) | Thrift CAS on static columns doesn't work as expected |  Major | Coordination | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-10604](https://issues.apache.org/jira/browse/CASSANDRA-10604) | Secondary index metadata is not reloaded when table is altered |  Major | CQL, Distributed Metadata, Secondary Indexes | Tyler Hobbs | Sam Tunnicliffe |
| [CASSANDRA-10581](https://issues.apache.org/jira/browse/CASSANDRA-10581) | Update cassandra.yaml comments to reflect memory\_allocator deprecation, remove in 3.0 |  Major | . | Jim Witschey | Robert Stupp |
| [CASSANDRA-10620](https://issues.apache.org/jira/browse/CASSANDRA-10620) | Debian package build broken. |  Blocker | Packaging | Michael Shuler | Michael Shuler |
| [CASSANDRA-10572](https://issues.apache.org/jira/browse/CASSANDRA-10572) | SinglePartitionNamesCommand::canRemoveRow omits counter cells it shouldn't |  Major | . | Aleksey Yeschenko | Sylvain Lebresne |
| [CASSANDRA-10557](https://issues.apache.org/jira/browse/CASSANDRA-10557) | Streaming can throw exception when trying to retry |  Minor | Streaming and Messaging | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-10608](https://issues.apache.org/jira/browse/CASSANDRA-10608) | Adding a dynamic column to a compact storage table with the same name as the partition key causes a memtable flush deadlock |  Minor | . | Mike Adamson | Mike Adamson |
| [CASSANDRA-9937](https://issues.apache.org/jira/browse/CASSANDRA-9937) | logback-tools.xml is incorrectly configured for outputing to System.err |  Minor | Tools | Mike Adamson | Mike Adamson |
| [CASSANDRA-9223](https://issues.apache.org/jira/browse/CASSANDRA-9223) | ArithmeticException after decommission |  Minor | . | Brandon Williams | Branimir Lambov |
| [CASSANDRA-10298](https://issues.apache.org/jira/browse/CASSANDRA-10298) | Replaced dead node stayed in gossip forever |  Major | . | Dikang Gu | Dikang Gu |
| [CASSANDRA-10628](https://issues.apache.org/jira/browse/CASSANDRA-10628) | get JEMAlloc debug output out of nodetool output |  Major | . | Jim Witschey | Robert Stupp |
| [CASSANDRA-10590](https://issues.apache.org/jira/browse/CASSANDRA-10590) | LegacySSTableTest fails after CASSANDRA-10360 |  Major | . | Robert Stupp | Sylvain Lebresne |
| [CASSANDRA-10600](https://issues.apache.org/jira/browse/CASSANDRA-10600) | CqlInputFormat throws IOE if the size estimates are zero |  Minor | . | Mike Adamson | Mike Adamson |
| [CASSANDRA-10634](https://issues.apache.org/jira/browse/CASSANDRA-10634) | Materialized Views filter paired endpoints to local DC even when not using DC-aware replication |  Major | Coordination, Materialized Views | Joel Knighton | Joel Knighton |
| [CASSANDRA-10584](https://issues.apache.org/jira/browse/CASSANDRA-10584) | reads with EACH\_QUORUM  on keyspace with SimpleTopologyStrategy throw a ClassCastException |  Minor | CQL | Andy Tolbert | Carl Yeksigian |
| [CASSANDRA-10621](https://issues.apache.org/jira/browse/CASSANDRA-10621) | Error while bootstraping a new node with Materialized Views |  Critical | Coordination, Materialized Views | Alan Boudreault | Joel Knighton |
| [CASSANDRA-9912](https://issues.apache.org/jira/browse/CASSANDRA-9912) | SizeEstimatesRecorder has assertions after decommission sometimes |  Major | Coordination, Distributed Metadata | Jeremiah Jordan | Paulo Motta |
| [CASSANDRA-10655](https://issues.apache.org/jira/browse/CASSANDRA-10655) | Skipping values during reads may cause incorrect read repairs |  Critical | . | Sam Tunnicliffe | Sylvain Lebresne |
| [CASSANDRA-10571](https://issues.apache.org/jira/browse/CASSANDRA-10571) | ClusteringIndexNamesFilter::shouldInclude is not implemented, SinglePartitionNamesCommand not discarding the sstables it could |  Major | . | Aleksey Yeschenko | Sylvain Lebresne |
| [CASSANDRA-10652](https://issues.apache.org/jira/browse/CASSANDRA-10652) | Tracing prevents startup after upgrading |  Blocker | Observability | Carl Yeksigian | Sylvain Lebresne |
| [CASSANDRA-10491](https://issues.apache.org/jira/browse/CASSANDRA-10491) | Inconsistent "position" numbering for keys in "system\_schema.columns" |  Minor | . | Michael Penick | Sylvain Lebresne |
| [CASSANDRA-10633](https://issues.apache.org/jira/browse/CASSANDRA-10633) | cqlsh copy uses wrong variable name for time\_format |  Major | Tools | Jeremiah Jordan | Stefania |
| [CASSANDRA-10650](https://issues.apache.org/jira/browse/CASSANDRA-10650) | Store UDA initcond as CQL literal in the schema table, instead of a blob |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-10614](https://issues.apache.org/jira/browse/CASSANDRA-10614) | AssertionError while flushing memtables |  Critical | Local Write-Read Paths, Materialized Views | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-10488](https://issues.apache.org/jira/browse/CASSANDRA-10488) | Document supported upgrade paths to 3.0 in NEWS.txt |  Major | Documentation and Website | Tyler Hobbs | Sylvain Lebresne |
| [CASSANDRA-10269](https://issues.apache.org/jira/browse/CASSANDRA-10269) | Run new upgrade tests on supported upgrade paths |  Major | Testing | Jim Witschey | Jim Witschey |
| [CASSANDRA-9188](https://issues.apache.org/jira/browse/CASSANDRA-9188) | cqlsh does not display properly the modified UDTs |  Minor | . | Benjamin Lerer |  |
| [CASSANDRA-9915](https://issues.apache.org/jira/browse/CASSANDRA-9915) | IndexError('list index out of range') when trying to connect to Cassandra cluster with cqlsh |  Minor | . | Prabir Kr Sarkar | Adam Holmberg |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9930](https://issues.apache.org/jira/browse/CASSANDRA-9930) | Add test coverage for 2.1 -\> 3.0 upgrades |  Major | . | Ryan McGuire | Andrew Hust |
| [CASSANDRA-10166](https://issues.apache.org/jira/browse/CASSANDRA-10166) | Fix failing tests |  Major | . | Sylvain Lebresne | Sylvain Lebresne |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10558](https://issues.apache.org/jira/browse/CASSANDRA-10558) | Fix out-of-order scrub test |  Major | Testing | Sylvain Lebresne | Yuki Morishita |
| [CASSANDRA-10461](https://issues.apache.org/jira/browse/CASSANDRA-10461) | Fix sstableverify\_test dtest |  Major | Testing, Tools | Jim Witschey | Stefania |
| [CASSANDRA-10376](https://issues.apache.org/jira/browse/CASSANDRA-10376) | Fix upgrade\_tests/paging\_test.py:TestPagingData.static\_columns\_paging\_test |  Major | CQL, Testing | Aleksey Yeschenko | Sylvain Lebresne |
| [CASSANDRA-10462](https://issues.apache.org/jira/browse/CASSANDRA-10462) | Fix failing test\_failure\_threshold\_deletions upgrade test |  Major | . | Jim Witschey | Sylvain Lebresne |
| [CASSANDRA-9975](https://issues.apache.org/jira/browse/CASSANDRA-9975) | Flatten Iterator call hierarchy with a shared Transformer |  Major | . | Benedict | Benedict |
| [CASSANDRA-10577](https://issues.apache.org/jira/browse/CASSANDRA-10577) | Fix cqlsh COPY commands that use NULL |  Major | Tools | Jim Witschey | Stefania |
| [CASSANDRA-10452](https://issues.apache.org/jira/browse/CASSANDRA-10452) | Fix resummable\_bootstrap\_test and bootstrap\_with\_reset\_bootstrap\_state\_test |  Major | Testing | Jim Witschey | Yuki Morishita |
| [CASSANDRA-10276](https://issues.apache.org/jira/browse/CASSANDRA-10276) | Do STCS in DTCS-windows |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-10586](https://issues.apache.org/jira/browse/CASSANDRA-10586) | thrift get on compact storage table broken |  Critical | . | Jeremiah Jordan | Sylvain Lebresne |
| [CASSANDRA-10611](https://issues.apache.org/jira/browse/CASSANDRA-10611) | Upgrade test on 2.1-\>3.0 path fails with configuration problems |  Major | . | Jim Witschey | Andrew Hust |
| [CASSANDRA-10470](https://issues.apache.org/jira/browse/CASSANDRA-10470) | Fix upgrade\_tests.cql\_tests/TestCQL/counters\_test dtest |  Critical | Coordination, Testing | Jim Witschey | Paulo Motta |
| [CASSANDRA-10602](https://issues.apache.org/jira/browse/CASSANDRA-10602) | 2 upgrade test failures: static\_columns\_paging\_test and multi\_list\_set\_test |  Major | CQL, Testing | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-10656](https://issues.apache.org/jira/browse/CASSANDRA-10656) | Fix failing DeleteTest |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-10570](https://issues.apache.org/jira/browse/CASSANDRA-10570) | HSHA test\_closing\_connections test flapping on 3.0 |  Major | Testing | Jim Witschey | Carl Yeksigian |
| [CASSANDRA-10573](https://issues.apache.org/jira/browse/CASSANDRA-10573) | select\_distinct\_test flapping on 2.2 -\> 3.0 upgrade path |  Major | . | Jim Witschey | Benjamin Lerer |
| [CASSANDRA-10662](https://issues.apache.org/jira/browse/CASSANDRA-10662) | Make UFTest.testAmokUDF non-flappy |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-10667](https://issues.apache.org/jira/browse/CASSANDRA-10667) | compaction\_test.TestCompaction\_with\_LeveledCompactionStrategy.compaction\_throughput\_test is flapping |  Major | . | Sylvain Lebresne | Marcus Eriksson |
| [CASSANDRA-10638](https://issues.apache.org/jira/browse/CASSANDRA-10638) | logged\_batch\_compatibility tests fail on Windows |  Major | Testing | Jim Witschey | Paulo Motta |
| [CASSANDRA-10641](https://issues.apache.org/jira/browse/CASSANDRA-10641) | simultaneous\_bootstrap\_test fails on Windows |  Major | Packaging | Jim Witschey | Joshua McKenzie |
| [CASSANDRA-10476](https://issues.apache.org/jira/browse/CASSANDRA-10476) | Fix upgrade paging dtest failures on 2.2-\>3.0 path |  Major | . | Jim Witschey | Benjamin Lerer |


