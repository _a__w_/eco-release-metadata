
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.0.1 - 2013-09-23



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4536](https://issues.apache.org/jira/browse/CASSANDRA-4536) | Ability for CQL3 to list partition keys |  Minor | CQL | Jonathan Ellis | Aleksey Yeschenko |
| [CASSANDRA-5935](https://issues.apache.org/jira/browse/CASSANDRA-5935) | Allow disabling slab allocation |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5796](https://issues.apache.org/jira/browse/CASSANDRA-5796) | Cqlsh should return a result in the case of a CAS INSERT/UPDATE |  Major | Tools | Michaël Figuière | Aleksey Yeschenko |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5754](https://issues.apache.org/jira/browse/CASSANDRA-5754) | remove unused cli option schema-mwt |  Trivial | Tools | Dave Brosius | Dave Brosius |
| [CASSANDRA-2698](https://issues.apache.org/jira/browse/CASSANDRA-2698) | Instrument repair to be able to assess it's efficiency (precision) |  Minor | . | Sylvain Lebresne | Benedict |
| [CASSANDRA-5862](https://issues.apache.org/jira/browse/CASSANDRA-5862) | Switch to adler checksum for sstables |  Major | . | Jonathan Ellis | T Jake Luciani |
| [CASSANDRA-5884](https://issues.apache.org/jira/browse/CASSANDRA-5884) | Improve offheap memory performance |  Major | . | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-5875](https://issues.apache.org/jira/browse/CASSANDRA-5875) | Ambiguous error message: "You must specify one and only one PRIMARY KEY" |  Minor | . | Jacob Rhoden | Lyuben Todorov |
| [CASSANDRA-5722](https://issues.apache.org/jira/browse/CASSANDRA-5722) | Cleanup should skip sstables that don't contain data outside a nodes ranges |  Major | . | Nick Bailey | Tyler Hobbs |
| [CASSANDRA-5919](https://issues.apache.org/jira/browse/CASSANDRA-5919) | reduce Map allocation from one per HH column to just one |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-5664](https://issues.apache.org/jira/browse/CASSANDRA-5664) | Improve serialization in the native protocol |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3991](https://issues.apache.org/jira/browse/CASSANDRA-3991) | Investigate importance of jsvc in debian packages |  Minor | . | Brandon Williams | Eric Evans |
| [CASSANDRA-4952](https://issues.apache.org/jira/browse/CASSANDRA-4952) | Add blocking force compaction (and anything else) calls to NodeProbe |  Minor | . | Michael Harris | Jonathan Ellis |
| [CASSANDRA-2524](https://issues.apache.org/jira/browse/CASSANDRA-2524) | Use SSTableBoundedScanner for cleanup |  Minor | . | Stu Hood | Tyler Hobbs |
| [CASSANDRA-5921](https://issues.apache.org/jira/browse/CASSANDRA-5921) | Don't return empty list when the L0 compaction candidates could cause overlap in L1 |  Minor | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-5923](https://issues.apache.org/jira/browse/CASSANDRA-5923) | Upgrade Apache Thrift to 0.9.1 |  Minor | . | Jake Farrell | Jake Farrell |
| [CASSANDRA-5952](https://issues.apache.org/jira/browse/CASSANDRA-5952) | report compression ratio via nodetool cfstats |  Trivial | . | Robert Coli | Vijay |
| [CASSANDRA-5963](https://issues.apache.org/jira/browse/CASSANDRA-5963) | Require superuser status for adding triggers |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5816](https://issues.apache.org/jira/browse/CASSANDRA-5816) | [PATCH] Debian packaging: also recommend chrony and ptpd in addition to ntp |  Minor | Packaging | Blair Zajac | Blair Zajac |
| [CASSANDRA-6000](https://issues.apache.org/jira/browse/CASSANDRA-6000) | Enhance UntypedResultSet.Row by adding missing/additional helper methods |  Major | . | Benjamin Coverston | Benjamin Coverston |
| [CASSANDRA-6017](https://issues.apache.org/jira/browse/CASSANDRA-6017) | Move hints and exception metrics to o.a.c.metrics |  Minor | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-4210](https://issues.apache.org/jira/browse/CASSANDRA-4210) | Support for variadic parameters list for "in clause" in prepared cql query |  Minor | . | Pierre Chalamet | Sylvain Lebresne |
| [CASSANDRA-5980](https://issues.apache.org/jira/browse/CASSANDRA-5980) | Allow cache keys-to-save to be dynamically set |  Minor | . | Chris Burroughs | Chris Burroughs |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5755](https://issues.apache.org/jira/browse/CASSANDRA-5755) | shuffle clear fails |  Minor | Tools | Radim Kolar | Dave Brosius |
| [CASSANDRA-5614](https://issues.apache.org/jira/browse/CASSANDRA-5614) | W/O specified columns ASPCSI does not get notified of deletes |  Minor | . | Benjamin Coverston | Sam Tunnicliffe |
| [CASSANDRA-5288](https://issues.apache.org/jira/browse/CASSANDRA-5288) | stress percentile label does not match what is returned |  Minor | Tools | Chris Burroughs | Jonathan Ellis |
| [CASSANDRA-5948](https://issues.apache.org/jira/browse/CASSANDRA-5948) | StreamOut doesn't correctly handle wrapped ranges |  Major | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-5964](https://issues.apache.org/jira/browse/CASSANDRA-5964) | cqlsh raises a ValueError when connecting to Cassandra running in Eclipse |  Minor | . | Greg DeAngelis | Dave Brosius |
| [CASSANDRA-5967](https://issues.apache.org/jira/browse/CASSANDRA-5967) | Allow local batchlog writes for CL.ANY |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5958](https://issues.apache.org/jira/browse/CASSANDRA-5958) | "Unable to find property" errors from snakeyaml are confusing |  Minor | . | J.B. Langston | Mikhail Stepura |
| [CASSANDRA-5985](https://issues.apache.org/jira/browse/CASSANDRA-5985) | Paxos replay of in progress update is incorrect |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5975](https://issues.apache.org/jira/browse/CASSANDRA-5975) | Filtering on Secondary Index Takes a Long Time Even with Limit 1, Trace Log Filled with Looping Messages |  Major | Secondary Indexes | Russell Spitzer | Aleksey Yeschenko |
| [CASSANDRA-5867](https://issues.apache.org/jira/browse/CASSANDRA-5867) | The Pig CqlStorage/AbstractCassandraStorage classes don't handle collection types |  Major | . | Jeremy Hanna | Alex Liu |
| [CASSANDRA-6006](https://issues.apache.org/jira/browse/CASSANDRA-6006) | Value of JVM\_OPTS is partially lost when enabling JEMallocAllocator in cassandra-env.sh |  Minor | Configuration | Nikolai Grigoriev |  |
| [CASSANDRA-5847](https://issues.apache.org/jira/browse/CASSANDRA-5847) | Support thrift tables in Pig CqlStorage |  Major | . | Alex Liu | Alex Liu |
| [CASSANDRA-5990](https://issues.apache.org/jira/browse/CASSANDRA-5990) | Hinted Handoff: java.lang.ArithmeticException: / by zero |  Minor | . | Karl Mueller | Jonathan Ellis |
| [CASSANDRA-6005](https://issues.apache.org/jira/browse/CASSANDRA-6005) | StandaloneScrubber assumes old-style json leveled manifest |  Trivial | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-6012](https://issues.apache.org/jira/browse/CASSANDRA-6012) | CAS does not always correctly replay inProgress rounds |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6011](https://issues.apache.org/jira/browse/CASSANDRA-6011) | Race condition in snapshot repair |  Major | . | Nick Bailey | Yuki Morishita |
| [CASSANDRA-5968](https://issues.apache.org/jira/browse/CASSANDRA-5968) | Nodetool info throws NPE when connected to a booting instance |  Minor | . | Janne Jalkanen | Mikhail Stepura |
| [CASSANDRA-6013](https://issues.apache.org/jira/browse/CASSANDRA-6013) | CAS may return false but still commit the insert |  Major | . | Sylvain Lebresne | Jonathan Ellis |
| [CASSANDRA-6023](https://issues.apache.org/jira/browse/CASSANDRA-6023) | CAS should distinguish promised and accepted ballots |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6004](https://issues.apache.org/jira/browse/CASSANDRA-6004) | Performing a "Select count(\*)" when replication factor \< node count causes assertion error and timeout |  Major | CQL | James P | Sylvain Lebresne |
| [CASSANDRA-6027](https://issues.apache.org/jira/browse/CASSANDRA-6027) | 'null' error when running sstablesplit on valid sstable |  Major | . | Daniel Meyer | Sylvain Lebresne |
| [CASSANDRA-6041](https://issues.apache.org/jira/browse/CASSANDRA-6041) | AssertionError in Tracing |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6040](https://issues.apache.org/jira/browse/CASSANDRA-6040) | Paging filter empty rows a bit too agressively |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6035](https://issues.apache.org/jira/browse/CASSANDRA-6035) | Prepared statements are broken in native protocol v2 |  Trivial | . | Blair Zajac | Sylvain Lebresne |
| [CASSANDRA-5982](https://issues.apache.org/jira/browse/CASSANDRA-5982) | OutOfMemoryError when writing text blobs to a very large number of tables |  Minor | . | Ryan McGuire | Jonathan Ellis |
| [CASSANDRA-5661](https://issues.apache.org/jira/browse/CASSANDRA-5661) | Discard pooled readers for cold data |  Major | . | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-6009](https://issues.apache.org/jira/browse/CASSANDRA-6009) | Migrate pre-2.0 key/value/column aliases to system.schema\_columns |  Major | Tools | koray sariteke | Aleksey Yeschenko |
| [CASSANDRA-5605](https://issues.apache.org/jira/browse/CASSANDRA-5605) | Crash caused by insufficient disk space to flush |  Minor | . | Dan Hendry | Jonathan Ellis |
| [CASSANDRA-6047](https://issues.apache.org/jira/browse/CASSANDRA-6047) | Memory leak when using snapshot repairs |  Minor | . | J.B. Langston | Yuki Morishita |
| [CASSANDRA-5947](https://issues.apache.org/jira/browse/CASSANDRA-5947) | Sampling bug in metrics-core-2.0.3.jar used by Cassandra |  Minor | Tools | J.B. Langston | Yuki Morishita |
| [CASSANDRA-5852](https://issues.apache.org/jira/browse/CASSANDRA-5852) | json2sstable breaks on data exported from sstable2json. |  Minor | Tools | Ryan McGuire | Lyuben Todorov |
| [CASSANDRA-6050](https://issues.apache.org/jira/browse/CASSANDRA-6050) | 'Internal application error' on SELECT .. WHERE col1=val AND col2 IN (1,2) |  Major | . | Sergey Nagaytsev | Sylvain Lebresne |
| [CASSANDRA-6033](https://issues.apache.org/jira/browse/CASSANDRA-6033) | CQL: allow names for bind variables |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6067](https://issues.apache.org/jira/browse/CASSANDRA-6067) | Use non-pooling readers with openForBatch |  Minor | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-6025](https://issues.apache.org/jira/browse/CASSANDRA-6025) | Deleted row resurrects if was not compacted in GC Grace Timeout due to thombstone read optimization in CollactionController |  Major | . | Oleg Anastasyev | Jonathan Ellis |
| [CASSANDRA-6090](https://issues.apache.org/jira/browse/CASSANDRA-6090) | init.d script not working under Ubuntu |  Minor | Packaging | Laurent Raufaste | Eric Evans |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5828](https://issues.apache.org/jira/browse/CASSANDRA-5828) | add counters coverage to upgrade tests |  Major | Testing | Cathy Daw | Daniel Meyer |


