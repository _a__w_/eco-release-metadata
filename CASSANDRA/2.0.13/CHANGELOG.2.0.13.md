
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.0.13 - 2015-03-16



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-8414](https://issues.apache.org/jira/browse/CASSANDRA-8414) | Avoid loops over array backed iterators that call iter.remove() |  Major | . | Richard Low | Jimmy Mårdell |
| [CASSANDRA-8666](https://issues.apache.org/jira/browse/CASSANDRA-8666) | Simplify logic of ABSC#BatchRemoveIterator#commit() |  Trivial | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-8610](https://issues.apache.org/jira/browse/CASSANDRA-8610) | Allow IF EXISTS for UPDATE statements |  Minor | CQL | DOAN DuyHai | Prajakta Bhosale |
| [CASSANDRA-8742](https://issues.apache.org/jira/browse/CASSANDRA-8742) | add default\_time\_to\_live to CQL doc |  Minor | Documentation and Website | Jon Haddad | Jon Haddad |
| [CASSANDRA-8301](https://issues.apache.org/jira/browse/CASSANDRA-8301) | Create a tool that given a bunch of sstables creates a "decent" sstable leveling |  Major | . | Marcus Eriksson | Marcus Eriksson |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-8645](https://issues.apache.org/jira/browse/CASSANDRA-8645) | sstableloader reports nonsensical bandwidth |  Minor | . | Max Barnash | Max Barnash |
| [CASSANDRA-8619](https://issues.apache.org/jira/browse/CASSANDRA-8619) | using CQLSSTableWriter gives ConcurrentModificationException |  Major | Tools | Igor Berman | Benedict |
| [CASSANDRA-8687](https://issues.apache.org/jira/browse/CASSANDRA-8687) | Keyspace should also check Config.isClientMode |  Minor | Tools | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-8733](https://issues.apache.org/jira/browse/CASSANDRA-8733) | List prepend reverses item order |  Major | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-8448](https://issues.apache.org/jira/browse/CASSANDRA-8448) | "Comparison method violates its general contract" in AbstractEndpointSnitch |  Major | . | J.B. Langston | Brandon Williams |
| [CASSANDRA-8726](https://issues.apache.org/jira/browse/CASSANDRA-8726) | throw OOM in Memory if we fail to allocate |  Minor | . | Benedict | Benedict |
| [CASSANDRA-8748](https://issues.apache.org/jira/browse/CASSANDRA-8748) | Backport memory leak fix from CASSANDRA-8707 to 2.0 |  Major | . | Jeremy Hanna | Benedict |
| [CASSANDRA-8716](https://issues.apache.org/jira/browse/CASSANDRA-8716) | "java.util.concurrent.ExecutionException: java.lang.AssertionError: Memory was freed" when running cleanup |  Minor | . | Imri Zvik | Robert Stupp |
| [CASSANDRA-8767](https://issues.apache.org/jira/browse/CASSANDRA-8767) | "Added column does not sort as the last column" when using new python driver |  Major | . | Russ Garrett | Tyler Hobbs |
| [CASSANDRA-8815](https://issues.apache.org/jira/browse/CASSANDRA-8815) |  Race in sstable ref counting during streaming failures |  Major | . | sankalp kohli | Benedict |
| [CASSANDRA-8852](https://issues.apache.org/jira/browse/CASSANDRA-8852) | Stream throttling mixes up bits & bytes |  Major | . | Jimmy Mårdell | Björn Hegerfors |
| [CASSANDRA-8718](https://issues.apache.org/jira/browse/CASSANDRA-8718) | nodetool cleanup causes segfault |  Minor | Tools | Maxim Ivanov | Joshua McKenzie |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7607](https://issues.apache.org/jira/browse/CASSANDRA-7607) | Test coverage for authorization in DDL & DML statements |  Major | Testing | Robert Stupp | Rajanarayanan Thottuvaikkatumana |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-8678](https://issues.apache.org/jira/browse/CASSANDRA-8678) | CREATE TABLE accepts value for default\_time\_to\_live on counter table |  Minor | Tools | Aaron Ploetz | Jeff Jirsa |


