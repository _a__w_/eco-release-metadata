
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.0.0 - 2011-10-18



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2753](https://issues.apache.org/jira/browse/CASSANDRA-2753) | Capture the max client timestamp for an SSTable |  Minor | . | Alan Liang | Alan Liang |
| [CASSANDRA-2452](https://issues.apache.org/jira/browse/CASSANDRA-2452) | New EC2 Snitch to use public ip and hence natively support for EC2 mult-region's. |  Minor | . | Vijay | Vijay |
| [CASSANDRA-47](https://issues.apache.org/jira/browse/CASSANDRA-47) | SSTable compression |  Major | . | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-2962](https://issues.apache.org/jira/browse/CASSANDRA-2962) | Metrics for RoundRobinScheduler |  Major | CQL | Stu Hood | Stu Hood |
| [CASSANDRA-1717](https://issues.apache.org/jira/browse/CASSANDRA-1717) | Cassandra cannot detect corrupt-but-readable column data |  Major | . | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-2843](https://issues.apache.org/jira/browse/CASSANDRA-2843) | better performance on long row read |  Major | . | Yang Yang | Sylvain Lebresne |
| [CASSANDRA-292](https://issues.apache.org/jira/browse/CASSANDRA-292) | Cassandra should be runnable as a Windows service |  Minor | Tools | Michael Greene | Benjamin Coverston |
| [CASSANDRA-3165](https://issues.apache.org/jira/browse/CASSANDRA-3165) | ArrayBackedSortedColumns is not being used by ColumnFamily.cloneMeShallow() |  Minor | . | Yang Yang | Sylvain Lebresne |
| [CASSANDRA-3600](https://issues.apache.org/jira/browse/CASSANDRA-3600) | Allow overriding RING\_DELAY |  Major | . | Brandon Williams | Brandon Williams |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2280](https://issues.apache.org/jira/browse/CASSANDRA-2280) | Request specific column families using StreamIn |  Major | . | Stu Hood | Jonathan Ellis |
| [CASSANDRA-1610](https://issues.apache.org/jira/browse/CASSANDRA-1610) | Pluggable Compaction |  Minor | . | Chris Goffinet | Alan Liang |
| [CASSANDRA-2692](https://issues.apache.org/jira/browse/CASSANDRA-2692) | Nuke BMT from orbit |  Minor | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-2427](https://issues.apache.org/jira/browse/CASSANDRA-2427) | Heuristic or hard cap to prevent fragmented commit logs from bringing down the server |  Major | . | Benjamin Coverston | Patricio Echague |
| [CASSANDRA-2771](https://issues.apache.org/jira/browse/CASSANDRA-2771) |  Remove commitlog\_rotation\_threshold\_in\_mb |  Minor | . | Patricio Echague | Patricio Echague |
| [CASSANDRA-2062](https://issues.apache.org/jira/browse/CASSANDRA-2062) | Better control of iterator consumption |  Minor | . | Stu Hood | Stu Hood |
| [CASSANDRA-2679](https://issues.apache.org/jira/browse/CASSANDRA-2679) | Move some column creation logic into Column factory functions |  Minor | . | Stu Hood | Stu Hood |
| [CASSANDRA-2589](https://issues.apache.org/jira/browse/CASSANDRA-2589) | row deletes do not remove columns |  Minor | . | amorton | amorton |
| [CASSANDRA-2491](https://issues.apache.org/jira/browse/CASSANDRA-2491) | A new config parameter, broadcast\_address |  Trivial | . | Khee Chin | Vijay |
| [CASSANDRA-2879](https://issues.apache.org/jira/browse/CASSANDRA-2879) | Make SSTableWriter.append(...) methods seekless. |  Major | . | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-2677](https://issues.apache.org/jira/browse/CASSANDRA-2677) | Optimize streaming to be single-pass |  Minor | . | Jonathan Ellis | Yuki Morishita |
| [CASSANDRA-2521](https://issues.apache.org/jira/browse/CASSANDRA-2521) | Move away from Phantom References for Compaction/Memtable |  Major | . | Chris Goffinet | Sylvain Lebresne |
| [CASSANDRA-2914](https://issues.apache.org/jira/browse/CASSANDRA-2914) | Simplify HH to always store hints on the coordinator |  Major | . | Jonathan Ellis | Patricio Echague |
| [CASSANDRA-2921](https://issues.apache.org/jira/browse/CASSANDRA-2921) | Split BufferedRandomAccessFile (BRAF) into Input and Output classes |  Major | . | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-1966](https://issues.apache.org/jira/browse/CASSANDRA-1966) | Option to control how many items are read on cache load |  Minor | . | Chris Burroughs | Chris Burroughs |
| [CASSANDRA-2894](https://issues.apache.org/jira/browse/CASSANDRA-2894) | add paging to get\_count |  Minor | CQL | Jonathan Ellis | Byron Clark |
| [CASSANDRA-1788](https://issues.apache.org/jira/browse/CASSANDRA-1788) | reduce copies on read, write paths |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2045](https://issues.apache.org/jira/browse/CASSANDRA-2045) | Simplify HH to decrease read load when nodes come back |  Major | . | Chris Goffinet | Nicholas Telford |
| [CASSANDRA-3033](https://issues.apache.org/jira/browse/CASSANDRA-3033) | [patch] ensure full read of chuck |  Minor | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-2447](https://issues.apache.org/jira/browse/CASSANDRA-2447) | Remove auto-bootstrap option |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2820](https://issues.apache.org/jira/browse/CASSANDRA-2820) | Re-introduce FastByteArrayInputStream (and Output equivalent) |  Minor | . | Paul Loy | Paul Loy |
| [CASSANDRA-2252](https://issues.apache.org/jira/browse/CASSANDRA-2252) | arena allocation for memtables |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3046](https://issues.apache.org/jira/browse/CASSANDRA-3046) | [patch] No need to use .equals on enums, just opens up chance of NPE |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-3048](https://issues.apache.org/jira/browse/CASSANDRA-3048) | SimpleAuthority should reload property file automatically |  Minor | . | Thomas Peuss | Thomas Peuss |
| [CASSANDRA-3040](https://issues.apache.org/jira/browse/CASSANDRA-3040) | Refactor and optimize ColumnFamilyStore.files(...) and Descriptor.fromFilename and few other places responsible for work with SSTable files. |  Minor | . | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-3063](https://issues.apache.org/jira/browse/CASSANDRA-3063) | [patch] fix some obvious javadoc problems |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-2594](https://issues.apache.org/jira/browse/CASSANDRA-2594) | run cassandra under numactl --interleave=all |  Minor | . | Peter Schuller | Peter Schuller |
| [CASSANDRA-2498](https://issues.apache.org/jira/browse/CASSANDRA-2498) | Improve read performance in update-intensive workload |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3079](https://issues.apache.org/jira/browse/CASSANDRA-3079) | Allow the client request scheduler to throw Timeouts |  Minor | CQL | Stu Hood | Stu Hood |
| [CASSANDRA-3081](https://issues.apache.org/jira/browse/CASSANDRA-3081) | Cassandra cli strategy options should be a hash not an array of hashes |  Minor | Tools | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-1608](https://issues.apache.org/jira/browse/CASSANDRA-1608) | Redesigned Compaction |  Major | . | Chris Goffinet | Benjamin Coverston |
| [CASSANDRA-2630](https://issues.apache.org/jira/browse/CASSANDRA-2630) | CLI - 'describe column family' would be nice |  Minor | . | Jeremy Hanna | satish babu krishnamoorthy |
| [CASSANDRA-2806](https://issues.apache.org/jira/browse/CASSANDRA-2806) | Expose gossip/FD info to JMX |  Minor | . | Brandon Williams | Patricio Echague |
| [CASSANDRA-3078](https://issues.apache.org/jira/browse/CASSANDRA-3078) | Make Secondary Indexes Pluggable |  Major | Secondary Indexes | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-3107](https://issues.apache.org/jira/browse/CASSANDRA-3107) | CompactionInfo id is not stable |  Minor | . | Stu Hood | Stu Hood |
| [CASSANDRA-2034](https://issues.apache.org/jira/browse/CASSANDRA-2034) | Make Read Repair unnecessary when Hinted Handoff is enabled |  Major | . | Jonathan Ellis | Patricio Echague |
| [CASSANDRA-3001](https://issues.apache.org/jira/browse/CASSANDRA-3001) | Make the compression algorithm and chunk length configurable |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3080](https://issues.apache.org/jira/browse/CASSANDRA-3080) | Add throttling for internode streaming |  Major | . | Stu Hood | Stu Hood |
| [CASSANDRA-2610](https://issues.apache.org/jira/browse/CASSANDRA-2610) | Have the repair of a range repair \*all\* the replica for that range |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2606](https://issues.apache.org/jira/browse/CASSANDRA-2606) | Expose through JMX the ability to repair only the primary range |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3015](https://issues.apache.org/jira/browse/CASSANDRA-3015) | Streams Compression |  Major | . | Benjamin Coverston | Pavel Yaskevich |
| [CASSANDRA-3128](https://issues.apache.org/jira/browse/CASSANDRA-3128) | Replace compression and compression\_options config parameters by just a compression\_options map. |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2901](https://issues.apache.org/jira/browse/CASSANDRA-2901) | Allow taking advantage of multiple cores while compacting a single CF |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3148](https://issues.apache.org/jira/browse/CASSANDRA-3148) | Use TreeMap backed column families for the SSTable simple writers |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3146](https://issues.apache.org/jira/browse/CASSANDRA-3146) | Minor changes to IntervalTree |  Minor | . | paul cannon | paul cannon |
| [CASSANDRA-2247](https://issues.apache.org/jira/browse/CASSANDRA-2247) | Cleanup unused imports and generics |  Major | . | Norman Maurer | Norman Maurer |
| [CASSANDRA-2449](https://issues.apache.org/jira/browse/CASSANDRA-2449) | Deprecate or modify per-cf memtable sizes in favor of the global threshold |  Major | . | Stu Hood | Jonathan Ellis |
| [CASSANDRA-2936](https://issues.apache.org/jira/browse/CASSANDRA-2936) | improve dependency situation between JDBC driver and Cassandra |  Minor | CQL | Eric Evans | Eric Evans |
| [CASSANDRA-3162](https://issues.apache.org/jira/browse/CASSANDRA-3162) | use weak references to SlabAllocator regions |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3031](https://issues.apache.org/jira/browse/CASSANDRA-3031) | Add 4 byte integer type |  Minor | . | Radim Kolar | Radim Kolar |
| [CASSANDRA-3161](https://issues.apache.org/jira/browse/CASSANDRA-3161) | clean up read path |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3147](https://issues.apache.org/jira/browse/CASSANDRA-3147) | Add rowlevel support to secondary index API and methods need to throw exceptions |  Minor | Secondary Indexes | Jason Rutherglen | T Jake Luciani |
| [CASSANDRA-3190](https://issues.apache.org/jira/browse/CASSANDRA-3190) | Fix backwards compatibilty for cql memtable properties |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3201](https://issues.apache.org/jira/browse/CASSANDRA-3201) | When a mmap fails, Cassandra should exit. |  Minor | . | Eldon Stegall | Jonathan Ellis |
| [CASSANDRA-3229](https://issues.apache.org/jira/browse/CASSANDRA-3229) | Remove ability to disable dynamic snitch entirely |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2472](https://issues.apache.org/jira/browse/CASSANDRA-2472) | CQL 1.1 and 2.0 |  Major | CQL | Eric Evans |  |
| [CASSANDRA-3137](https://issues.apache.org/jira/browse/CASSANDRA-3137) | Implement wrapping intersections for ConfigHelper's InputKeyRange |  Minor | . | mck | mck |
| [CASSANDRA-3295](https://issues.apache.org/jira/browse/CASSANDRA-3295) | reduce default heap size |  Minor | . | Jonathan Ellis | satish babu krishnamoorthy |
| [CASSANDRA-3322](https://issues.apache.org/jira/browse/CASSANDRA-3322) | add estimated tasks to LeveledCompactionStrategy |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3308](https://issues.apache.org/jira/browse/CASSANDRA-3308) | Add compaction\_thread\_priority back |  Minor | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-3329](https://issues.apache.org/jira/browse/CASSANDRA-3329) | make HSHA the default Thrift server |  Minor | CQL | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3149](https://issues.apache.org/jira/browse/CASSANDRA-3149) | Update CQL type names to match expected (SQL) behavor |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2988](https://issues.apache.org/jira/browse/CASSANDRA-2988) | Improve SSTableReader.load() when loading index files |  Minor | . | Melvin Wang | Melvin Wang |
| [CASSANDRA-4107](https://issues.apache.org/jira/browse/CASSANDRA-4107) | fix broken link in cassandra-env.sh |  Major | . | Ilya Shipitsin | Ilya Shipitsin |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2263](https://issues.apache.org/jira/browse/CASSANDRA-2263) | cql driver jar |  Minor | CQL | Eric Evans | Eric Evans |
| [CASSANDRA-2764](https://issues.apache.org/jira/browse/CASSANDRA-2764) | generate-eclipse-files still referencing drivers/ source |  Minor | . | Kirk True | Kirk True |
| [CASSANDRA-2784](https://issues.apache.org/jira/browse/CASSANDRA-2784) | Fix build for removal of commons-collections |  Minor | . | Stu Hood | Stu Hood |
| [CASSANDRA-2769](https://issues.apache.org/jira/browse/CASSANDRA-2769) | Cannot Create Duplicate Compaction Marker |  Major | . | Benjamin Coverston | Sylvain Lebresne |
| [CASSANDRA-2468](https://issues.apache.org/jira/browse/CASSANDRA-2468) | Clean up after failed compaction |  Minor | . | Jonathan Ellis | amorton |
| [CASSANDRA-2317](https://issues.apache.org/jira/browse/CASSANDRA-2317) | Column family deletion time is not always reseted after gc\_grace |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2916](https://issues.apache.org/jira/browse/CASSANDRA-2916) | Streaming estimatedKey calculation should never be 0 |  Minor | . | Stu Hood | Stu Hood |
| [CASSANDRA-2906](https://issues.apache.org/jira/browse/CASSANDRA-2906) | Streaming SSTable build does not use cleanupIfNecessary |  Minor | . | Stu Hood | Yuki Morishita |
| [CASSANDRA-2082](https://issues.apache.org/jira/browse/CASSANDRA-2082) | Saved row cache continues to be read past max cache size |  Minor | . | Chris Burroughs | Chris Burroughs |
| [CASSANDRA-2643](https://issues.apache.org/jira/browse/CASSANDRA-2643) | read repair/reconciliation breaks slice based iteration at QUORUM |  Critical | . | Peter Schuller | Byron Clark |
| [CASSANDRA-2994](https://issues.apache.org/jira/browse/CASSANDRA-2994) | OutOfBounds in CompressedSequentialWriter.flushData |  Major | . | Stu Hood | Sylvain Lebresne |
| [CASSANDRA-2494](https://issues.apache.org/jira/browse/CASSANDRA-2494) | Quorum reads are not monotonically consistent |  Minor | . | Sean Bridges | Jonathan Ellis |
| [CASSANDRA-3034](https://issues.apache.org/jira/browse/CASSANDRA-3034) | [patch] BufferedInputStream.skip only skips bytes that are in the buffer, so keep skipping until done |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-2061](https://issues.apache.org/jira/browse/CASSANDRA-2061) | Missing logging for some exceptions |  Minor | . | Stu Hood | Jonathan Ellis |
| [CASSANDRA-3053](https://issues.apache.org/jira/browse/CASSANDRA-3053) | [patch] EstimatedHIstogram doesn't overide equals correctly |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-2942](https://issues.apache.org/jira/browse/CASSANDRA-2942) | Dropped columnfamilies can leave orphaned data files that do not get cleared on restart |  Minor | . | Cathy Daw | Jonathan Ellis |
| [CASSANDRA-3085](https://issues.apache.org/jira/browse/CASSANDRA-3085) | Race condition in sstable reference counting |  Critical | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3051](https://issues.apache.org/jira/browse/CASSANDRA-3051) | On Disk Compression breaks SSL Encryption |  Major | . | Benjamin Coverston | Pavel Yaskevich |
| [CASSANDRA-3110](https://issues.apache.org/jira/browse/CASSANDRA-3110) | SSTables iterators are closed and before being used |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3087](https://issues.apache.org/jira/browse/CASSANDRA-3087) | Leveled compaction allows multiple simultaneous compaction Tasks |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3106](https://issues.apache.org/jira/browse/CASSANDRA-3106) | getRangeToEndpointMap() method removed |  Minor | Tools | Nick Bailey | Nick Bailey |
| [CASSANDRA-3096](https://issues.apache.org/jira/browse/CASSANDRA-3096) | Test RoundRobinScheduler timeouts |  Major | CQL | Stu Hood | Stu Hood |
| [CASSANDRA-3003](https://issues.apache.org/jira/browse/CASSANDRA-3003) | Trunk single-pass streaming doesn't handle large row correctly |  Critical | . | Sylvain Lebresne | Yuki Morishita |
| [CASSANDRA-3144](https://issues.apache.org/jira/browse/CASSANDRA-3144) | trunk is unable to participate with an 0.8 ring, again |  Blocker | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3145](https://issues.apache.org/jira/browse/CASSANDRA-3145) | IntervalTree could miscalculate its max |  Minor | . | paul cannon | paul cannon |
| [CASSANDRA-3119](https://issues.apache.org/jira/browse/CASSANDRA-3119) | Cli syntax for creating keyspace is inconsistent in 1.0 |  Minor | . | Sylvain Lebresne | T Jake Luciani |
| [CASSANDRA-3157](https://issues.apache.org/jira/browse/CASSANDRA-3157) | After a "short read", the wrong read command may be used |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3160](https://issues.apache.org/jira/browse/CASSANDRA-3160) | PIG\_OPTS bash variable interpolation doesn't work correctly when PIG\_OPTS is set in the environment. |  Minor | . | Eldon Stegall |  |
| [CASSANDRA-3159](https://issues.apache.org/jira/browse/CASSANDRA-3159) | Multiple classpath entries in the cassandra-all.jar |  Minor | . | T Jake Luciani | Eric Evans |
| [CASSANDRA-3154](https://issues.apache.org/jira/browse/CASSANDRA-3154) | Bad equality check in ColumnFamilyStore.isCompleteSSTables() |  Minor | . | Tupshin Harper | Tupshin Harper |
| [CASSANDRA-3156](https://issues.apache.org/jira/browse/CASSANDRA-3156) | assertion error in RowRepairResolver |  Blocker | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-3163](https://issues.apache.org/jira/browse/CASSANDRA-3163) | SlabAllocator OOMs much faster than HeapAllocator |  Major | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-3171](https://issues.apache.org/jira/browse/CASSANDRA-3171) | AbstractCompactionIterable uses a 1MB buffer for every sstable |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3169](https://issues.apache.org/jira/browse/CASSANDRA-3169) | read repair: NEWS does not match actual behavior |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3168](https://issues.apache.org/jira/browse/CASSANDRA-3168) | Arena allocation causes excessive flushing on small heaps |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3182](https://issues.apache.org/jira/browse/CASSANDRA-3182) | Remove special-cased maximum on sstables-to-compact for leveled strategy |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3184](https://issues.apache.org/jira/browse/CASSANDRA-3184) | Update the versions that are referenced in the generated POMs so that they match the versions in svn's lib folder |  Minor | Packaging | Stephen Connolly | Stephen Connolly |
| [CASSANDRA-3192](https://issues.apache.org/jira/browse/CASSANDRA-3192) | NPE in RowRepairResolver |  Major | . | Cathy Daw | Jonathan Ellis |
| [CASSANDRA-3191](https://issues.apache.org/jira/browse/CASSANDRA-3191) | unable to start single node cluster when listen\_address is not localhost |  Major | . | T Jake Luciani | Jonathan Ellis |
| [CASSANDRA-3194](https://issues.apache.org/jira/browse/CASSANDRA-3194) | repair streaming forwarding loop |  Major | . | Anton Winter | Sylvain Lebresne |
| [CASSANDRA-3179](https://issues.apache.org/jira/browse/CASSANDRA-3179) | JVM segfaults |  Major | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-3193](https://issues.apache.org/jira/browse/CASSANDRA-3193) | Gossip teardown causes test failures |  Major | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-3181](https://issues.apache.org/jira/browse/CASSANDRA-3181) | Compaction fails to occur |  Major | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-3205](https://issues.apache.org/jira/browse/CASSANDRA-3205) | ColumnFamily.cloneMeShallow doesn't respect the insertionOrdered flag (for ArrayBackedSortedColumns) |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3204](https://issues.apache.org/jira/browse/CASSANDRA-3204) | stress cannot set the compaction strategy |  Major | Tools | Brandon Williams | Pavel Yaskevich |
| [CASSANDRA-3203](https://issues.apache.org/jira/browse/CASSANDRA-3203) | Odd flush behavior |  Critical | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-3210](https://issues.apache.org/jira/browse/CASSANDRA-3210) | memtables do not need to be flushed on the Table.apply() path anymore after 2449 |  Trivial | . | Yang Yang | Yang Yang |
| [CASSANDRA-3223](https://issues.apache.org/jira/browse/CASSANDRA-3223) | probably don't need to do full copy to row cache after un-mmap() change |  Minor | . | Yang Yang | Yang Yang |
| [CASSANDRA-3215](https://issues.apache.org/jira/browse/CASSANDRA-3215) | The word count example demonstrating hadoop integration fails in trunk |  Minor | . | Tharindu Mathew | Brandon Williams |
| [CASSANDRA-3216](https://issues.apache.org/jira/browse/CASSANDRA-3216) | A streamOutSession keeps sstables references forever if the remote end dies |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3219](https://issues.apache.org/jira/browse/CASSANDRA-3219) | Nodes started at the same time end up with the same token |  Major | . | T Jake Luciani | Sylvain Lebresne |
| [CASSANDRA-3230](https://issues.apache.org/jira/browse/CASSANDRA-3230) | clientutil jar missing from artifacts |  Major | Packaging | Eric Evans | Eric Evans |
| [CASSANDRA-3224](https://issues.apache.org/jira/browse/CASSANDRA-3224) | LeveledCompactionStrategy is too complacent |  Major | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-3235](https://issues.apache.org/jira/browse/CASSANDRA-3235) | OutboundTcpConnection throws RuntimeException |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3227](https://issues.apache.org/jira/browse/CASSANDRA-3227) | cassandra-cli use micro second timestamp, but CQL use milli second |  Major | CQL | Sabro Boucher | Jonathan Ellis |
| [CASSANDRA-3218](https://issues.apache.org/jira/browse/CASSANDRA-3218) | Failure reading a erroneous/spurious AutoSavingCache file can result in a failed application of a migration, which can prevent a node from reaching schema agreement. |  Minor | . | Eldon Stegall | Eldon Stegall |
| [CASSANDRA-3246](https://issues.apache.org/jira/browse/CASSANDRA-3246) | memtable\_total\_space\_in\_mb does not accept the value 0 in Cassandra 1.0 |  Minor | . | Tyler Hobbs | Jonathan Ellis |
| [CASSANDRA-3247](https://issues.apache.org/jira/browse/CASSANDRA-3247) | sstableloader ignores option doesn't work correctly |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3234](https://issues.apache.org/jira/browse/CASSANDRA-3234) | LeveledCompaction has several performance problems |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3253](https://issues.apache.org/jira/browse/CASSANDRA-3253) | inherent deadlock situation in commitLog flush? |  Critical | . | Yang Yang | Jonathan Ellis |
| [CASSANDRA-3257](https://issues.apache.org/jira/browse/CASSANDRA-3257) | Enabling SSL on a fairly light cluster leaks Open files. |  Minor | . | Vijay | Vijay |
| [CASSANDRA-3245](https://issues.apache.org/jira/browse/CASSANDRA-3245) | Don't fail when numactl is installed, but NUMA policies are not supported |  Minor | Packaging | paul cannon | Peter Schuller |
| [CASSANDRA-3260](https://issues.apache.org/jira/browse/CASSANDRA-3260) | MergeIterator assertion on sources != empty can be thrown |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3266](https://issues.apache.org/jira/browse/CASSANDRA-3266) | missed CQL term rename |  Major | CQL | Eric Evans | Eric Evans |
| [CASSANDRA-3262](https://issues.apache.org/jira/browse/CASSANDRA-3262) | SimpleSnitch.compareEndpoints doesn't respect the intent of the snitch |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3268](https://issues.apache.org/jira/browse/CASSANDRA-3268) | Cannot read counter value from jdbc cql |  Trivial | CQL | Corey Hulen | Corey Hulen |
| [CASSANDRA-3270](https://issues.apache.org/jira/browse/CASSANDRA-3270) | Error during multi-threaded compaction in 0.8 |  Major | . | Karl Mueller | Jonathan Ellis |
| [CASSANDRA-3285](https://issues.apache.org/jira/browse/CASSANDRA-3285) | Bootstrap is broken in 1.0.0-rc1 |  Critical | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3282](https://issues.apache.org/jira/browse/CASSANDRA-3282) | CLI does not support removing compression options from a ColumnFamily |  Minor | Tools | Nate McCall | Pavel Yaskevich |
| [CASSANDRA-3284](https://issues.apache.org/jira/browse/CASSANDRA-3284) | help create column family refers to outdated fields |  Minor | Tools | Radim Kolar | Radim Kolar |
| [CASSANDRA-3259](https://issues.apache.org/jira/browse/CASSANDRA-3259) | Replace token leaves the old node state in tact causing problems in cli |  Minor | . | Vijay | Vijay |
| [CASSANDRA-3288](https://issues.apache.org/jira/browse/CASSANDRA-3288) | CfDef can default to an invalid id and fail during system\_add\_column\_family |  Critical | CQL | Nate McCall | Jonathan Ellis |
| [CASSANDRA-3291](https://issues.apache.org/jira/browse/CASSANDRA-3291) | 1.0 needs to clean out old-style hints |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3289](https://issues.apache.org/jira/browse/CASSANDRA-3289) | assert err on ArrayBackedSortedColumns.addColumn(ArrayBackedSortedColumns.java:126) |  Critical | . | Jackson Chung | Jonathan Ellis |
| [CASSANDRA-3176](https://issues.apache.org/jira/browse/CASSANDRA-3176) | Disabling hinted handoff counterintuitively continues to log handoff messages |  Minor | . | Jeremy Hanna | Jonathan Ellis |
| [CASSANDRA-3293](https://issues.apache.org/jira/browse/CASSANDRA-3293) | Metered Flusher log message confusing |  Trivial | . | Radim Kolar |  |
| [CASSANDRA-3263](https://issues.apache.org/jira/browse/CASSANDRA-3263) | Whitespace in SimpleSeedProvider string makes seed ignored |  Minor | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-3273](https://issues.apache.org/jira/browse/CASSANDRA-3273) | FailureDetector can take a very long time to mark a host down |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3301](https://issues.apache.org/jira/browse/CASSANDRA-3301) | Java Stress Tool:  COUNTER\_GET reads from CounterSuper1 instead of SuperCounter1 |  Major | . | Cathy Daw | Sylvain Lebresne |
| [CASSANDRA-3304](https://issues.apache.org/jira/browse/CASSANDRA-3304) | Missing fields in show schema output |  Minor | Tools | Radim Kolar | Pavel Yaskevich |
| [CASSANDRA-3269](https://issues.apache.org/jira/browse/CASSANDRA-3269) | possible early deletion of commit logs |  Critical | . | Yang Yang | Sylvain Lebresne |
| [CASSANDRA-3298](https://issues.apache.org/jira/browse/CASSANDRA-3298) | CompressedRandomAccessReaderTest fails on Windows |  Major | . | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-3309](https://issues.apache.org/jira/browse/CASSANDRA-3309) | Nodetool Doesnt close the open JMX connection causing it to leak Threads |  Major | . | Vijay | Vijay |
| [CASSANDRA-3311](https://issues.apache.org/jira/browse/CASSANDRA-3311) | cqlsh: Error running "select \*" vs "select all columns" |  Major | CQL | Cathy Daw | Pavel Yaskevich |
| [CASSANDRA-3310](https://issues.apache.org/jira/browse/CASSANDRA-3310) | Update lib/jamm-0.2.5 to the version deployed to Maven central |  Minor | . | Stephen Connolly |  |
| [CASSANDRA-3297](https://issues.apache.org/jira/browse/CASSANDRA-3297) | truncate can still result in data being replayed after a restart |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3318](https://issues.apache.org/jira/browse/CASSANDRA-3318) | Unable to delete after running scrub |  Minor | . | Radim Kolar | Jonathan Ellis |
| [CASSANDRA-3325](https://issues.apache.org/jira/browse/CASSANDRA-3325) | Compaction degrades key cache stats |  Minor | . | Fabien Rousseau | Fabien Rousseau |
| [CASSANDRA-3338](https://issues.apache.org/jira/browse/CASSANDRA-3338) | Uncompressed sizes are used to estimate space for compaction of compressed sstables |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3343](https://issues.apache.org/jira/browse/CASSANDRA-3343) | nodetool printing classpath |  Trivial | . | Cathy Daw | Sylvain Lebresne |
| [CASSANDRA-3346](https://issues.apache.org/jira/browse/CASSANDRA-3346) | HsHa broken at startup |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3345](https://issues.apache.org/jira/browse/CASSANDRA-3345) | Repair still streams unnecessary sstables |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3913](https://issues.apache.org/jira/browse/CASSANDRA-3913) | Incorrect InetAddress equality test |  Trivial | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-4019](https://issues.apache.org/jira/browse/CASSANDRA-4019) | java.util.ConcurrentModificationException in Gossiper |  Minor | . | Thibaut | Brandon Williams |
| [CASSANDRA-4089](https://issues.apache.org/jira/browse/CASSANDRA-4089) | Typo fix: key\_valiation\_class -\> key\_validation\_class |  Minor | Documentation and Website | Andrew Ash |  |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2641](https://issues.apache.org/jira/browse/CASSANDRA-2641) | AbstractBounds.normalize should deal with overlapping ranges |  Minor | . | Stu Hood | Sylvain Lebresne |
| [CASSANDRA-2945](https://issues.apache.org/jira/browse/CASSANDRA-2945) | Add test for counter merge shard path |  Trivial | Testing | Sylvain Lebresne | Sylvain Lebresne |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2982](https://issues.apache.org/jira/browse/CASSANDRA-2982) | Refactor secondary index api |  Major | Secondary Indexes | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-2734](https://issues.apache.org/jira/browse/CASSANDRA-2734) | CQL protocol does not include schema information |  Minor | CQL | Cathy Daw | Jonathan Ellis |
| [CASSANDRA-2883](https://issues.apache.org/jira/browse/CASSANDRA-2883) | Add Support for BigDecimal Java data type as the "DecimalType" AbstractType |  Trivial | . | Rick Shaw | Rick Shaw |
| [CASSANDRA-3068](https://issues.apache.org/jira/browse/CASSANDRA-3068) | Fix count() |  Major | CQL | Jonathan Ellis | Jonathan Ellis |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-957](https://issues.apache.org/jira/browse/CASSANDRA-957) | convenience workflow for replacing dead node |  Major | Tools | Jonathan Ellis | Vijay |
| [CASSANDRA-2681](https://issues.apache.org/jira/browse/CASSANDRA-2681) | Upgrade ConcurrentLinkedHashMap (v1.2) |  Minor | . | Ben Manes | Jonathan Ellis |
| [CASSANDRA-2920](https://issues.apache.org/jira/browse/CASSANDRA-2920) | Remove SSTableWriter.Builder |  Minor | . | Jonathan Ellis | Stu Hood |
| [CASSANDRA-2998](https://issues.apache.org/jira/browse/CASSANDRA-2998) | Remove dependency on old version of cdh3 in any builds |  Major | . | Jeremy Hanna | Nate McCall |
| [CASSANDRA-3064](https://issues.apache.org/jira/browse/CASSANDRA-3064) | Add query-by-column mode to stress.java |  Minor | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-3032](https://issues.apache.org/jira/browse/CASSANDRA-3032) | clean up KSMetadata, CFMetadata |  Trivial | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3104](https://issues.apache.org/jira/browse/CASSANDRA-3104) | remove compaction\_thread\_priority setting |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3167](https://issues.apache.org/jira/browse/CASSANDRA-3167) | remove drivers/ from new branches |  Minor | CQL | Eric Evans | Sylvain Lebresne |
| [CASSANDRA-3183](https://issues.apache.org/jira/browse/CASSANDRA-3183) | Make SerializingCacheProvider the default if JNA is available |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3180](https://issues.apache.org/jira/browse/CASSANDRA-3180) | relocate Python CQL driver |  Minor | . | Eric Evans | Eric Evans |
| [CASSANDRA-2922](https://issues.apache.org/jira/browse/CASSANDRA-2922) | Move SimpleAuthenticator and SimpleAuthority to examples/ |  Minor | . | Jonathan Ellis | Sylvain Lebresne |


