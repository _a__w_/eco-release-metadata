
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.0.16 - 2018-02-19



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13954](https://issues.apache.org/jira/browse/CASSANDRA-13954) | Provide a JMX call to sync schema with local storage |  Minor | Distributed Metadata | Aleksey Yeschenko | Aleksey Yeschenko |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13922](https://issues.apache.org/jira/browse/CASSANDRA-13922) | nodetool verify should also verify sstable metadata |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-10857](https://issues.apache.org/jira/browse/CASSANDRA-10857) | Allow dropping COMPACT STORAGE flag from tables in 3.X |  Blocker | CQL, Distributed Metadata | Aleksey Yeschenko | Alex Petrov |
| [CASSANDRA-13987](https://issues.apache.org/jira/browse/CASSANDRA-13987) | Multithreaded commitlog subtly changed durability |  Major | . | Jason Brown | Jason Brown |
| [CASSANDRA-14094](https://issues.apache.org/jira/browse/CASSANDRA-14094) | Avoid pointless calls to ThreadLocalRandom |  Minor | Core | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-14133](https://issues.apache.org/jira/browse/CASSANDRA-14133) | Log file names of files streamed in to a node |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-14217](https://issues.apache.org/jira/browse/CASSANDRA-14217) | nodetool verify needs to use the correct digest file and reload sstable metadata |  Major | . | Marcus Eriksson | Marcus Eriksson |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13939](https://issues.apache.org/jira/browse/CASSANDRA-13939) | Mishandling of cells for removed/dropped columns when reading legacy files |  Major | Local Write-Read Paths | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-13813](https://issues.apache.org/jira/browse/CASSANDRA-13813) | Don't let user drop (or generally break) tables in system\_distributed |  Major | Distributed Metadata | Sylvain Lebresne | Aleksey Yeschenko |
| [CASSANDRA-13123](https://issues.apache.org/jira/browse/CASSANDRA-13123) | Draining a node might fail to delete all inactive commitlogs |  Major | Local Write-Read Paths | Jan Urbański | Jan Urbański |
| [CASSANDRA-13959](https://issues.apache.org/jira/browse/CASSANDRA-13959) | Add yaml flag for disabling MVs, log warnings on creation |  Minor | Materialized Views | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-13964](https://issues.apache.org/jira/browse/CASSANDRA-13964) | Tracing interferes with digest requests when using RandomPartitioner |  Major | Local Write-Read Paths, Observability | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-13975](https://issues.apache.org/jira/browse/CASSANDRA-13975) | Add a workaround for overly large read repair mutations |  Major | Coordination | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-14057](https://issues.apache.org/jira/browse/CASSANDRA-14057) | The size of a byte is not 2 |  Trivial | Streaming and Messaging | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-14091](https://issues.apache.org/jira/browse/CASSANDRA-14091) | DynamicSnitch creates a lot of garbage |  Major | . | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-14071](https://issues.apache.org/jira/browse/CASSANDRA-14071) | Materialized view on table with TTL issue |  Major | Coordination, Materialized Views | Silviu Butnariu | ZhaoYang |
| [CASSANDRA-13526](https://issues.apache.org/jira/browse/CASSANDRA-13526) | nodetool cleanup on KS with no replicas should remove old data, not silently complete |  Major | Compaction | Jeff Jirsa | ZhaoYang |
| [CASSANDRA-14088](https://issues.apache.org/jira/browse/CASSANDRA-14088) | Forward slash in role name breaks CassandraAuthorizer |  Minor | Auth | Jesse Haber-Kucharsky | Kurt Greaves |
| [CASSANDRA-14010](https://issues.apache.org/jira/browse/CASSANDRA-14010) | Fix SStable ordering by max timestamp in SinglePartitionReadCommand |  Major | Local Write-Read Paths | Jonathan Pellby | ZhaoYang |
| [CASSANDRA-13873](https://issues.apache.org/jira/browse/CASSANDRA-13873) | Ref bug in Scrub |  Major | Tools | T Jake Luciani | Marcus Eriksson |
| [CASSANDRA-13006](https://issues.apache.org/jira/browse/CASSANDRA-13006) | Disable automatic heap dumps on OOM error |  Minor | Configuration | anmols | Benjamin Lerer |
| [CASSANDRA-14008](https://issues.apache.org/jira/browse/CASSANDRA-14008) | RTs at index boundaries in 2.x sstables can create unexpected CQL row in 3.x |  Major | Local Write-Read Paths | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-13801](https://issues.apache.org/jira/browse/CASSANDRA-13801) | CompactionManager sometimes wrongly determines that a background compaction is running for a particular table |  Minor | Compaction | Dimitar Dimitrov | Dimitar Dimitrov |
| [CASSANDRA-14108](https://issues.apache.org/jira/browse/CASSANDRA-14108) | Improve commit log chain marker updating |  Major | . | Jason Brown | Jason Brown |
| [CASSANDRA-14112](https://issues.apache.org/jira/browse/CASSANDRA-14112) | The inspectJvmOptions startup check can trigger some Exception on some JRE versions |  Major | Core | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-14104](https://issues.apache.org/jira/browse/CASSANDRA-14104) | Index target doesn't correctly recognise non-UTF column names after COMPACT STORAGE drop |  Major | . | Alex Petrov | Alex Petrov |
| [CASSANDRA-14137](https://issues.apache.org/jira/browse/CASSANDRA-14137) | Cassandra crashes on startup.  Crash Problematic frame: # C  [sigar-amd64-winnt.dll+0x14ed4] using JRE version: Java(TM) SE Runtime Environment (9.0+11) |  Minor | Core, Metrics | mark pettovello |  |
| [CASSANDRA-14154](https://issues.apache.org/jira/browse/CASSANDRA-14154) | \`ant javadoc\` task broken due to UTF-8 characters in multiple source files |  Minor | Build | Johannes Grassler | Johannes Grassler |
| [CASSANDRA-9630](https://issues.apache.org/jira/browse/CASSANDRA-9630) | Killing cassandra process results in unclosed connections |  Minor | Distributed Metadata, Streaming and Messaging | Paulo Motta | Paulo Motta |
| [CASSANDRA-14021](https://issues.apache.org/jira/browse/CASSANDRA-14021) | test\_pycodestyle\_compliance - cqlsh\_tests.cqlsh\_tests.TestCqlsh code style errors |  Major | . | Michael Kjellman | Michael Kjellman |
| [CASSANDRA-14175](https://issues.apache.org/jira/browse/CASSANDRA-14175) | Incorrect documentation about CASSANDRA\_INCLUDE priority |  Trivial | Documentation and Website | Marcel Dopita | Jason Brown |
| [CASSANDRA-14180](https://issues.apache.org/jira/browse/CASSANDRA-14180) | cassandra.spec needs to require ant-junit |  Minor | Packaging | Troels Arvin | Troels Arvin |
| [CASSANDRA-14181](https://issues.apache.org/jira/browse/CASSANDRA-14181) | RPM package has too many executable files |  Minor | Packaging | Troels Arvin | Troels Arvin |
| [CASSANDRA-13933](https://issues.apache.org/jira/browse/CASSANDRA-13933) | Handle mutateRepaired failure in nodetool verify |  Major | . | Marcus Eriksson | Sumanth Pasupuleti |
| [CASSANDRA-14092](https://issues.apache.org/jira/browse/CASSANDRA-14092) | Max ttl of 20 years will overflow localDeletionTime |  Blocker | Core | Paulo Motta | Paulo Motta |
| [CASSANDRA-12840](https://issues.apache.org/jira/browse/CASSANDRA-12840) | Cassandra doesn't start on MINGW32 (Windows) |  Minor | . | Amichai Rothman | Amichai Rothman |
| [CASSANDRA-14219](https://issues.apache.org/jira/browse/CASSANDRA-14219) | Change to AlterTableStatement logging breaks MView tests |  Major | . | Jason Brown | Dinesh Joshi |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-14003](https://issues.apache.org/jira/browse/CASSANDRA-14003) | Correct logger message formatting in SSTableLoader |  Trivial | Tools | Jaydeepkumar Chovatia | Jaydeepkumar Chovatia |


