
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.0 beta 2 - 2015-09-08



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10199](https://issues.apache.org/jira/browse/CASSANDRA-10199) | Warn on tiny disks instead of failing startup |  Minor | . | Jonathan Ellis | Jonathan Ellis |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10061](https://issues.apache.org/jira/browse/CASSANDRA-10061) | Only use batchlog when paired view replica is remote |  Major | Coordination, Materialized Views | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-10060](https://issues.apache.org/jira/browse/CASSANDRA-10060) | Reuse TemporalRow when updating multiple MaterializedViews |  Major | Coordination, Materialized Views | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-10086](https://issues.apache.org/jira/browse/CASSANDRA-10086) | Add a "CLEAR" cqlsh command to clear the console |  Trivial | . | Paul O'Fallon | Paul O'Fallon |
| [CASSANDRA-8611](https://issues.apache.org/jira/browse/CASSANDRA-8611) | give streaming\_socket\_timeout\_in\_ms a non-zero default |  Major | . | Jeremy Hanna | Robert Coli |
| [CASSANDRA-10234](https://issues.apache.org/jira/browse/CASSANDRA-10234) | Add nodetool gettraceprobability command |  Trivial | . | sequoyha pelletier | sequoyha pelletier |
| [CASSANDRA-9901](https://issues.apache.org/jira/browse/CASSANDRA-9901) | Make AbstractType.isByteOrderComparable abstract |  Major | . | Benedict | Benedict |
| [CASSANDRA-9673](https://issues.apache.org/jira/browse/CASSANDRA-9673) | Improve batchlog write path |  Major | Coordination, Materialized Views | Aleksey Yeschenko | Stefania |
| [CASSANDRA-10147](https://issues.apache.org/jira/browse/CASSANDRA-10147) | Base table PRIMARY KEY can be assumed to be NOT NULL in MV creation |  Minor | CQL, Materialized Views | Jonathan Ellis | Carl Yeksigian |
| [CASSANDRA-10193](https://issues.apache.org/jira/browse/CASSANDRA-10193) | Improve Rows.diff and Rows.merge efficiency; downgrade Row.columns() to Collection\<ColumnDefinition\> |  Major | . | Benedict | Benedict |
| [CASSANDRA-6237](https://issues.apache.org/jira/browse/CASSANDRA-6237) | Allow range deletions in CQL |  Minor | CQL | Sylvain Lebresne | Benjamin Lerer |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10072](https://issues.apache.org/jira/browse/CASSANDRA-10072) | "Replica(s) failed to execute read" on simple select on stress-created table with \>1 nodes |  Major | . | Jim Witschey | Marcus Eriksson |
| [CASSANDRA-10043](https://issues.apache.org/jira/browse/CASSANDRA-10043) | A NullPointerException is thrown if the column name is unknown for an IN relation |  Major | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-10048](https://issues.apache.org/jira/browse/CASSANDRA-10048) | cassandra-stress - Decimal is a BigInt not a Double |  Major | . | Sebastian Estevez |  |
| [CASSANDRA-9906](https://issues.apache.org/jira/browse/CASSANDRA-9906) | get\_slice and multiget\_slice failing on trunk |  Blocker | . | Mike Adamson | Benjamin Lerer |
| [CASSANDRA-10163](https://issues.apache.org/jira/browse/CASSANDRA-10163) | Test regression for secondary\_indexes\_test.TestSecondaryIndexesOnCollections.test\_map\_indexes |  Major | CQL, Secondary Indexes, Testing | Sylvain Lebresne | Sam Tunnicliffe |
| [CASSANDRA-9917](https://issues.apache.org/jira/browse/CASSANDRA-9917) | MVs should validate gc grace seconds on the tables involved |  Major | Coordination, Materialized Views | Aleksey Yeschenko | Paulo Motta |
| [CASSANDRA-10132](https://issues.apache.org/jira/browse/CASSANDRA-10132) | sstablerepairedset throws exception while loading metadata |  Major | Tools | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-10157](https://issues.apache.org/jira/browse/CASSANDRA-10157) | SchemaKeyspace.addAggregateToSchemaMutation keeps previous on CREATE OR REPLACE AGGREGATE |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-10160](https://issues.apache.org/jira/browse/CASSANDRA-10160) | Test regression for write\_failures\_test.TestWriteFailures.test\_mutation\_v2 |  Major | . | Sylvain Lebresne | Aleksey Yeschenko |
| [CASSANDRA-10135](https://issues.apache.org/jira/browse/CASSANDRA-10135) | Quoting changed for username in GRANT statement |  Minor | CQL | Bernhard K. Weisshuhn | Sam Tunnicliffe |
| [CASSANDRA-10168](https://issues.apache.org/jira/browse/CASSANDRA-10168) | CassandraAuthorizer.authorize must throw exception when lookup of any auth table fails |  Major | . | Vishy Kasar | Vishy Kasar |
| [CASSANDRA-9898](https://issues.apache.org/jira/browse/CASSANDRA-9898) | cqlsh crashes if it load a utf-8 file. |  Minor | Tools | Yasuharu Goto | Yasuharu Goto |
| [CASSANDRA-9232](https://issues.apache.org/jira/browse/CASSANDRA-9232) | "timestamp" is considered as a reserved keyword in cqlsh completion |  Trivial | Tools | Michaël Figuière | Stefania |
| [CASSANDRA-9142](https://issues.apache.org/jira/browse/CASSANDRA-9142) | DC Local repair or -hosts should only be allowed with -full repair |  Minor | Streaming and Messaging | sankalp kohli | Marcus Eriksson |
| [CASSANDRA-10180](https://issues.apache.org/jira/browse/CASSANDRA-10180) | Secondary index tables are flushed twice |  Minor | Secondary Indexes | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-10181](https://issues.apache.org/jira/browse/CASSANDRA-10181) | Deadlock flushing tables with CUSTOM indexes |  Major | CQL, Local Write-Read Paths | Tyler Hobbs | Sam Tunnicliffe |
| [CASSANDRA-10203](https://issues.apache.org/jira/browse/CASSANDRA-10203) | UnbufferedDataOutputstreamPlus.writeUTF fails for 0 length and \> 8190 length strings |  Major | Compaction, Local Write-Read Paths, Streaming and Messaging | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-10178](https://issues.apache.org/jira/browse/CASSANDRA-10178) | AssertionError inserting more than 8 values in 100-column table |  Major | CQL | Jim Witschey | Benjamin Lerer |
| [CASSANDRA-10159](https://issues.apache.org/jira/browse/CASSANDRA-10159) | Incorrect last update time causes dtest to fail due to unexpected errors |  Major | Local Write-Read Paths | Stefania | Stefania |
| [CASSANDRA-10197](https://issues.apache.org/jira/browse/CASSANDRA-10197) | LWW bug in Materialized Views |  Major | Coordination, Materialized Views | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-10182](https://issues.apache.org/jira/browse/CASSANDRA-10182) | Cassandra stress driver settings broken |  Minor | Tools | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-10156](https://issues.apache.org/jira/browse/CASSANDRA-10156) | Creating Materialized views concurrently leads to missing data |  Major | Coordination, Materialized Views | Alan Boudreault | T Jake Luciani |
| [CASSANDRA-9872](https://issues.apache.org/jira/browse/CASSANDRA-9872) | only check KeyCache when it is enabled |  Minor | . | Chris Burroughs | Chris Burroughs |
| [CASSANDRA-9554](https://issues.apache.org/jira/browse/CASSANDRA-9554) | Avoid digest mismatch storm on upgrade to 3.0 |  Major | . | Aleksey Yeschenko | Sylvain Lebresne |
| [CASSANDRA-10057](https://issues.apache.org/jira/browse/CASSANDRA-10057) | RepairMessageVerbHandler.java:95 - Cannot start multiple repair sessions over the same sstables |  Major | Streaming and Messaging | Victor Trac | Yuki Morishita |
| [CASSANDRA-10164](https://issues.apache.org/jira/browse/CASSANDRA-10164) | Re-apply MV updates on commitlog replay |  Major | Coordination, Materialized Views | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-10206](https://issues.apache.org/jira/browse/CASSANDRA-10206) | Incorrect handling of end-of stream leading to infinite loop in streaming session |  Major | . | Alexey Burylov |  |
| [CASSANDRA-9857](https://issues.apache.org/jira/browse/CASSANDRA-9857) | Deal with backward compatibilty issue of broken AbstractBounds serialization |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-10262](https://issues.apache.org/jira/browse/CASSANDRA-10262) | Bootstrapping nodes failing to apply view updates correctly. |  Major | Coordination, Materialized Views | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-8741](https://issues.apache.org/jira/browse/CASSANDRA-8741) | Running a drain before a decommission apparently the wrong thing to do |  Trivial | . | Casey Marshall | Jan Karlsson |
| [CASSANDRA-9893](https://issues.apache.org/jira/browse/CASSANDRA-9893) | Fix upgrade tests from #9704 that are still failing |  Major | . | Sylvain Lebresne | Blake Eggleston |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10151](https://issues.apache.org/jira/browse/CASSANDRA-10151) | Fix TrackerTest.testTryModify |  Major | . | Stefania | Benedict |
| [CASSANDRA-9575](https://issues.apache.org/jira/browse/CASSANDRA-9575) | LeveledCompactionStrategyTest.testCompactionRace and testMutateLevel sometimes fail |  Major | Testing | Ariel Weisberg | Yuki Morishita |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10036](https://issues.apache.org/jira/browse/CASSANDRA-10036) | Windows utest 3.0: ColumnFamilyStoreTest.testScrubDataDirectories failing |  Major | Testing, Tools | Joshua McKenzie | Stefania |
| [CASSANDRA-10097](https://issues.apache.org/jira/browse/CASSANDRA-10097) | Windows dtest 3.0: bootstrap\_test.py:TestBootstrap.bootstrap\_with\_reset\_bootstrap\_state\_test fails |  Major | . | Joshua McKenzie | Paulo Motta |
| [CASSANDRA-10046](https://issues.apache.org/jira/browse/CASSANDRA-10046) | RangeTombstones handled incorrectly in Thrift |  Minor | . | T Jake Luciani | Sylvain Lebresne |
| [CASSANDRA-10101](https://issues.apache.org/jira/browse/CASSANDRA-10101) | Windows dtest 3.0: HintedHandoff tests failing |  Major | Testing | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-10118](https://issues.apache.org/jira/browse/CASSANDRA-10118) | Windows utest 3.0: SSTableLoaderTest.testLoadingSSTable flaky |  Major | . | Joshua McKenzie | Branimir Lambov |
| [CASSANDRA-10162](https://issues.apache.org/jira/browse/CASSANDRA-10162) | Test regression for  cqlsh\_tests.TestCqlsh.test\_describe |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-10184](https://issues.apache.org/jira/browse/CASSANDRA-10184) | Windows dtest 3.0: offline\_tools\_test.py:TestOfflineTools.sstablelevelreset\_test fails |  Major | Testing | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-10104](https://issues.apache.org/jira/browse/CASSANDRA-10104) | Windows dtest 3.0: jmx\_test.py:TestJMX.netstats\_test fails |  Major | . | Joshua McKenzie | Paulo Motta |
| [CASSANDRA-10186](https://issues.apache.org/jira/browse/CASSANDRA-10186) | Windows dtest 3.0: schema\_metadata\_test.py:TestSchemaMetadata.indexes\_test fails |  Major | Testing | Joshua McKenzie | Paulo Motta |
| [CASSANDRA-10045](https://issues.apache.org/jira/browse/CASSANDRA-10045) | Sparse/Dense decision should be made per-row, not per-file |  Minor | . | Benedict | Benedict |
| [CASSANDRA-10136](https://issues.apache.org/jira/browse/CASSANDRA-10136) | startup error after upgrade to 3.0 |  Major | . | Russ Hatch | Sylvain Lebresne |


