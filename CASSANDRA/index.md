
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
* Apache Cassandra v3.11.x
    * [Changelog](3.11.x/CHANGELOG.3.11.x.html)
    * [Release Notes](3.11.x/RELEASENOTES.3.11.x.html)
* Apache Cassandra v3.11.4
    * [Changelog](3.11.4/CHANGELOG.3.11.4.html)
    * [Release Notes](3.11.4/RELEASENOTES.3.11.4.html)
* Apache Cassandra v3.11.3
    * [Changelog](3.11.3/CHANGELOG.3.11.3.html)
    * [Release Notes](3.11.3/RELEASENOTES.3.11.3.html)
* Apache Cassandra v3.11.2
    * [Changelog](3.11.2/CHANGELOG.3.11.2.html)
    * [Release Notes](3.11.2/RELEASENOTES.3.11.2.html)
* Apache Cassandra v3.11.1
    * [Changelog](3.11.1/CHANGELOG.3.11.1.html)
    * [Release Notes](3.11.1/RELEASENOTES.3.11.1.html)
* Apache Cassandra v3.11.0
    * [Changelog](3.11.0/CHANGELOG.3.11.0.html)
    * [Release Notes](3.11.0/RELEASENOTES.3.11.0.html)
* Apache Cassandra v3.10
    * [Changelog](3.10/CHANGELOG.3.10.html)
    * [Release Notes](3.10/RELEASENOTES.3.10.html)
* Apache Cassandra v3.9
    * [Changelog](3.9/CHANGELOG.3.9.html)
    * [Release Notes](3.9/RELEASENOTES.3.9.html)
* Apache Cassandra v3.8
    * [Changelog](3.8/CHANGELOG.3.8.html)
    * [Release Notes](3.8/RELEASENOTES.3.8.html)
* Apache Cassandra v3.7
    * [Changelog](3.7/CHANGELOG.3.7.html)
    * [Release Notes](3.7/RELEASENOTES.3.7.html)
* Apache Cassandra v3.6
    * [Changelog](3.6/CHANGELOG.3.6.html)
    * [Release Notes](3.6/RELEASENOTES.3.6.html)
* Apache Cassandra v3.5
    * [Changelog](3.5/CHANGELOG.3.5.html)
    * [Release Notes](3.5/RELEASENOTES.3.5.html)
* Apache Cassandra v3.4
    * [Changelog](3.4/CHANGELOG.3.4.html)
    * [Release Notes](3.4/RELEASENOTES.3.4.html)
* Apache Cassandra v3.3
    * [Changelog](3.3/CHANGELOG.3.3.html)
    * [Release Notes](3.3/RELEASENOTES.3.3.html)
* Apache Cassandra v3.2.1
    * [Changelog](3.2.1/CHANGELOG.3.2.1.html)
    * [Release Notes](3.2.1/RELEASENOTES.3.2.1.html)
* Apache Cassandra v3.2
    * [Changelog](3.2/CHANGELOG.3.2.html)
    * [Release Notes](3.2/RELEASENOTES.3.2.html)
* Apache Cassandra v3.1.1
    * [Changelog](3.1.1/CHANGELOG.3.1.1.html)
    * [Release Notes](3.1.1/RELEASENOTES.3.1.1.html)
* Apache Cassandra v3.1
    * [Changelog](3.1/CHANGELOG.3.1.html)
    * [Release Notes](3.1/RELEASENOTES.3.1.html)
* Apache Cassandra v3.0.x
    * [Changelog](3.0.x/CHANGELOG.3.0.x.html)
    * [Release Notes](3.0.x/RELEASENOTES.3.0.x.html)
* Apache Cassandra v3.0 beta 2
    * [Changelog](3.0 beta 2/CHANGELOG.3.0 beta 2.html)
    * [Release Notes](3.0 beta 2/RELEASENOTES.3.0 beta 2.html)
* Apache Cassandra v3.0 beta 1
    * [Changelog](3.0 beta 1/CHANGELOG.3.0 beta 1.html)
    * [Release Notes](3.0 beta 1/RELEASENOTES.3.0 beta 1.html)
* Apache Cassandra v3.0 alpha 1
    * [Changelog](3.0 alpha 1/CHANGELOG.3.0 alpha 1.html)
    * [Release Notes](3.0 alpha 1/RELEASENOTES.3.0 alpha 1.html)
* Apache Cassandra v3.0.18
    * [Changelog](3.0.18/CHANGELOG.3.0.18.html)
    * [Release Notes](3.0.18/RELEASENOTES.3.0.18.html)
* Apache Cassandra v3.0.17
    * [Changelog](3.0.17/CHANGELOG.3.0.17.html)
    * [Release Notes](3.0.17/RELEASENOTES.3.0.17.html)
* Apache Cassandra v3.0.16
    * [Changelog](3.0.16/CHANGELOG.3.0.16.html)
    * [Release Notes](3.0.16/RELEASENOTES.3.0.16.html)
* Apache Cassandra v3.0.15
    * [Changelog](3.0.15/CHANGELOG.3.0.15.html)
    * [Release Notes](3.0.15/RELEASENOTES.3.0.15.html)
* Apache Cassandra v3.0.14
    * [Changelog](3.0.14/CHANGELOG.3.0.14.html)
    * [Release Notes](3.0.14/RELEASENOTES.3.0.14.html)
* Apache Cassandra v3.0.13
    * [Changelog](3.0.13/CHANGELOG.3.0.13.html)
    * [Release Notes](3.0.13/RELEASENOTES.3.0.13.html)
* Apache Cassandra v3.0.12
    * [Changelog](3.0.12/CHANGELOG.3.0.12.html)
    * [Release Notes](3.0.12/RELEASENOTES.3.0.12.html)
* Apache Cassandra v3.0.11
    * [Changelog](3.0.11/CHANGELOG.3.0.11.html)
    * [Release Notes](3.0.11/RELEASENOTES.3.0.11.html)
* Apache Cassandra v3.0.10
    * [Changelog](3.0.10/CHANGELOG.3.0.10.html)
    * [Release Notes](3.0.10/RELEASENOTES.3.0.10.html)
* Apache Cassandra v3.0.9
    * [Changelog](3.0.9/CHANGELOG.3.0.9.html)
    * [Release Notes](3.0.9/RELEASENOTES.3.0.9.html)
* Apache Cassandra v3.0.8
    * [Changelog](3.0.8/CHANGELOG.3.0.8.html)
    * [Release Notes](3.0.8/RELEASENOTES.3.0.8.html)
* Apache Cassandra v3.0.7
    * [Changelog](3.0.7/CHANGELOG.3.0.7.html)
    * [Release Notes](3.0.7/RELEASENOTES.3.0.7.html)
* Apache Cassandra v3.0.6
    * [Changelog](3.0.6/CHANGELOG.3.0.6.html)
    * [Release Notes](3.0.6/RELEASENOTES.3.0.6.html)
* Apache Cassandra v3.0.5
    * [Changelog](3.0.5/CHANGELOG.3.0.5.html)
    * [Release Notes](3.0.5/RELEASENOTES.3.0.5.html)
* Apache Cassandra v3.0.4
    * [Changelog](3.0.4/CHANGELOG.3.0.4.html)
    * [Release Notes](3.0.4/RELEASENOTES.3.0.4.html)
* Apache Cassandra v3.0.3
    * [Changelog](3.0.3/CHANGELOG.3.0.3.html)
    * [Release Notes](3.0.3/RELEASENOTES.3.0.3.html)
* Apache Cassandra v3.0.2
    * [Changelog](3.0.2/CHANGELOG.3.0.2.html)
    * [Release Notes](3.0.2/RELEASENOTES.3.0.2.html)
* Apache Cassandra v3.0.1
    * [Changelog](3.0.1/CHANGELOG.3.0.1.html)
    * [Release Notes](3.0.1/RELEASENOTES.3.0.1.html)
* Apache Cassandra v3.0.0 rc2
    * [Changelog](3.0.0 rc2/CHANGELOG.3.0.0 rc2.html)
    * [Release Notes](3.0.0 rc2/RELEASENOTES.3.0.0 rc2.html)
* Apache Cassandra v3.0.0 rc1
    * [Changelog](3.0.0 rc1/CHANGELOG.3.0.0 rc1.html)
    * [Release Notes](3.0.0 rc1/RELEASENOTES.3.0.0 rc1.html)
* Apache Cassandra v3.0.0
    * [Changelog](3.0.0/CHANGELOG.3.0.0.html)
    * [Release Notes](3.0.0/RELEASENOTES.3.0.0.html)
* Apache Cassandra v2.2.x
    * [Changelog](2.2.x/CHANGELOG.2.2.x.html)
    * [Release Notes](2.2.x/RELEASENOTES.2.2.x.html)
* Apache Cassandra v2.2.14
    * [Changelog](2.2.14/CHANGELOG.2.2.14.html)
    * [Release Notes](2.2.14/RELEASENOTES.2.2.14.html)
* Apache Cassandra v2.2.13
    * [Changelog](2.2.13/CHANGELOG.2.2.13.html)
    * [Release Notes](2.2.13/RELEASENOTES.2.2.13.html)
* Apache Cassandra v2.2.12
    * [Changelog](2.2.12/CHANGELOG.2.2.12.html)
    * [Release Notes](2.2.12/RELEASENOTES.2.2.12.html)
* Apache Cassandra v2.2.11
    * [Changelog](2.2.11/CHANGELOG.2.2.11.html)
    * [Release Notes](2.2.11/RELEASENOTES.2.2.11.html)
* Apache Cassandra v2.2.10
    * [Changelog](2.2.10/CHANGELOG.2.2.10.html)
    * [Release Notes](2.2.10/RELEASENOTES.2.2.10.html)
* Apache Cassandra v2.2.9
    * [Changelog](2.2.9/CHANGELOG.2.2.9.html)
    * [Release Notes](2.2.9/RELEASENOTES.2.2.9.html)
* Apache Cassandra v2.2.8
    * [Changelog](2.2.8/CHANGELOG.2.2.8.html)
    * [Release Notes](2.2.8/RELEASENOTES.2.2.8.html)
* Apache Cassandra v2.2.7
    * [Changelog](2.2.7/CHANGELOG.2.2.7.html)
    * [Release Notes](2.2.7/RELEASENOTES.2.2.7.html)
* Apache Cassandra v2.2.6
    * [Changelog](2.2.6/CHANGELOG.2.2.6.html)
    * [Release Notes](2.2.6/RELEASENOTES.2.2.6.html)
* Apache Cassandra v2.2.5
    * [Changelog](2.2.5/CHANGELOG.2.2.5.html)
    * [Release Notes](2.2.5/RELEASENOTES.2.2.5.html)
* Apache Cassandra v2.2.4
    * [Changelog](2.2.4/CHANGELOG.2.2.4.html)
    * [Release Notes](2.2.4/RELEASENOTES.2.2.4.html)
* Apache Cassandra v2.2.3
    * [Changelog](2.2.3/CHANGELOG.2.2.3.html)
    * [Release Notes](2.2.3/RELEASENOTES.2.2.3.html)
* Apache Cassandra v2.2.2
    * [Changelog](2.2.2/CHANGELOG.2.2.2.html)
    * [Release Notes](2.2.2/RELEASENOTES.2.2.2.html)
* Apache Cassandra v2.2.1
    * [Changelog](2.2.1/CHANGELOG.2.2.1.html)
    * [Release Notes](2.2.1/RELEASENOTES.2.2.1.html)
* Apache Cassandra v2.2.0 rc2
    * [Changelog](2.2.0 rc2/CHANGELOG.2.2.0 rc2.html)
    * [Release Notes](2.2.0 rc2/RELEASENOTES.2.2.0 rc2.html)
* Apache Cassandra v2.2.0 rc1
    * [Changelog](2.2.0 rc1/CHANGELOG.2.2.0 rc1.html)
    * [Release Notes](2.2.0 rc1/RELEASENOTES.2.2.0 rc1.html)
* Apache Cassandra v2.2.0 beta 1
    * [Changelog](2.2.0 beta 1/CHANGELOG.2.2.0 beta 1.html)
    * [Release Notes](2.2.0 beta 1/RELEASENOTES.2.2.0 beta 1.html)
* Apache Cassandra v2.2.0
    * [Changelog](2.2.0/CHANGELOG.2.2.0.html)
    * [Release Notes](2.2.0/RELEASENOTES.2.2.0.html)
* Apache Cassandra v2.1.x
    * [Changelog](2.1.x/CHANGELOG.2.1.x.html)
    * [Release Notes](2.1.x/RELEASENOTES.2.1.x.html)
* Apache Cassandra v2.1 rc6
    * [Changelog](2.1 rc6/CHANGELOG.2.1 rc6.html)
    * [Release Notes](2.1 rc6/RELEASENOTES.2.1 rc6.html)
* Apache Cassandra v2.1 rc5
    * [Changelog](2.1 rc5/CHANGELOG.2.1 rc5.html)
    * [Release Notes](2.1 rc5/RELEASENOTES.2.1 rc5.html)
* Apache Cassandra v2.1 rc4
    * [Changelog](2.1 rc4/CHANGELOG.2.1 rc4.html)
    * [Release Notes](2.1 rc4/RELEASENOTES.2.1 rc4.html)
* Apache Cassandra v2.1 rc3
    * [Changelog](2.1 rc3/CHANGELOG.2.1 rc3.html)
    * [Release Notes](2.1 rc3/RELEASENOTES.2.1 rc3.html)
* Apache Cassandra v2.1 rc2
    * [Changelog](2.1 rc2/CHANGELOG.2.1 rc2.html)
    * [Release Notes](2.1 rc2/RELEASENOTES.2.1 rc2.html)
* Apache Cassandra v2.1 rc1
    * [Changelog](2.1 rc1/CHANGELOG.2.1 rc1.html)
    * [Release Notes](2.1 rc1/RELEASENOTES.2.1 rc1.html)
* Apache Cassandra v2.1 beta2
    * [Changelog](2.1 beta2/CHANGELOG.2.1 beta2.html)
    * [Release Notes](2.1 beta2/RELEASENOTES.2.1 beta2.html)
* Apache Cassandra v2.1 beta1
    * [Changelog](2.1 beta1/CHANGELOG.2.1 beta1.html)
    * [Release Notes](2.1 beta1/RELEASENOTES.2.1 beta1.html)
* Apache Cassandra v2.1.21
    * [Changelog](2.1.21/CHANGELOG.2.1.21.html)
    * [Release Notes](2.1.21/RELEASENOTES.2.1.21.html)
* Apache Cassandra v2.1.20
    * [Changelog](2.1.20/CHANGELOG.2.1.20.html)
    * [Release Notes](2.1.20/RELEASENOTES.2.1.20.html)
* Apache Cassandra v2.1.19
    * [Changelog](2.1.19/CHANGELOG.2.1.19.html)
    * [Release Notes](2.1.19/RELEASENOTES.2.1.19.html)
* Apache Cassandra v2.1.18
    * [Changelog](2.1.18/CHANGELOG.2.1.18.html)
    * [Release Notes](2.1.18/RELEASENOTES.2.1.18.html)
* Apache Cassandra v2.1.17
    * [Changelog](2.1.17/CHANGELOG.2.1.17.html)
    * [Release Notes](2.1.17/RELEASENOTES.2.1.17.html)
* Apache Cassandra v2.1.16
    * [Changelog](2.1.16/CHANGELOG.2.1.16.html)
    * [Release Notes](2.1.16/RELEASENOTES.2.1.16.html)
* Apache Cassandra v2.1.15
    * [Changelog](2.1.15/CHANGELOG.2.1.15.html)
    * [Release Notes](2.1.15/RELEASENOTES.2.1.15.html)
* Apache Cassandra v2.1.14
    * [Changelog](2.1.14/CHANGELOG.2.1.14.html)
    * [Release Notes](2.1.14/RELEASENOTES.2.1.14.html)
* Apache Cassandra v2.1.13
    * [Changelog](2.1.13/CHANGELOG.2.1.13.html)
    * [Release Notes](2.1.13/RELEASENOTES.2.1.13.html)
* Apache Cassandra v2.1.12
    * [Changelog](2.1.12/CHANGELOG.2.1.12.html)
    * [Release Notes](2.1.12/RELEASENOTES.2.1.12.html)
* Apache Cassandra v2.1.11
    * [Changelog](2.1.11/CHANGELOG.2.1.11.html)
    * [Release Notes](2.1.11/RELEASENOTES.2.1.11.html)
* Apache Cassandra v2.1.10
    * [Changelog](2.1.10/CHANGELOG.2.1.10.html)
    * [Release Notes](2.1.10/RELEASENOTES.2.1.10.html)
* Apache Cassandra v2.1.9
    * [Changelog](2.1.9/CHANGELOG.2.1.9.html)
    * [Release Notes](2.1.9/RELEASENOTES.2.1.9.html)
* Apache Cassandra v2.1.8
    * [Changelog](2.1.8/CHANGELOG.2.1.8.html)
    * [Release Notes](2.1.8/RELEASENOTES.2.1.8.html)
* Apache Cassandra v2.1.7
    * [Changelog](2.1.7/CHANGELOG.2.1.7.html)
    * [Release Notes](2.1.7/RELEASENOTES.2.1.7.html)
* Apache Cassandra v2.1.6
    * [Changelog](2.1.6/CHANGELOG.2.1.6.html)
    * [Release Notes](2.1.6/RELEASENOTES.2.1.6.html)
* Apache Cassandra v2.1.5
    * [Changelog](2.1.5/CHANGELOG.2.1.5.html)
    * [Release Notes](2.1.5/RELEASENOTES.2.1.5.html)
* Apache Cassandra v2.1.4
    * [Changelog](2.1.4/CHANGELOG.2.1.4.html)
    * [Release Notes](2.1.4/RELEASENOTES.2.1.4.html)
* Apache Cassandra v2.1.3
    * [Changelog](2.1.3/CHANGELOG.2.1.3.html)
    * [Release Notes](2.1.3/RELEASENOTES.2.1.3.html)
* Apache Cassandra v2.1.2
    * [Changelog](2.1.2/CHANGELOG.2.1.2.html)
    * [Release Notes](2.1.2/RELEASENOTES.2.1.2.html)
* Apache Cassandra v2.1.1
    * [Changelog](2.1.1/CHANGELOG.2.1.1.html)
    * [Release Notes](2.1.1/RELEASENOTES.2.1.1.html)
* Apache Cassandra v2.1.0
    * [Changelog](2.1.0/CHANGELOG.2.1.0.html)
    * [Release Notes](2.1.0/RELEASENOTES.2.1.0.html)
* Apache Cassandra v2.0 rc1
    * [Changelog](2.0 rc1/CHANGELOG.2.0 rc1.html)
    * [Release Notes](2.0 rc1/RELEASENOTES.2.0 rc1.html)
* Apache Cassandra v2.0 beta 2
    * [Changelog](2.0 beta 2/CHANGELOG.2.0 beta 2.html)
    * [Release Notes](2.0 beta 2/RELEASENOTES.2.0 beta 2.html)
* Apache Cassandra v2.0 beta 1
    * [Changelog](2.0 beta 1/CHANGELOG.2.0 beta 1.html)
    * [Release Notes](2.0 beta 1/RELEASENOTES.2.0 beta 1.html)
* Apache Cassandra v2.0.17
    * [Changelog](2.0.17/CHANGELOG.2.0.17.html)
    * [Release Notes](2.0.17/RELEASENOTES.2.0.17.html)
* Apache Cassandra v2.0.16
    * [Changelog](2.0.16/CHANGELOG.2.0.16.html)
    * [Release Notes](2.0.16/RELEASENOTES.2.0.16.html)
* Apache Cassandra v2.0.15
    * [Changelog](2.0.15/CHANGELOG.2.0.15.html)
    * [Release Notes](2.0.15/RELEASENOTES.2.0.15.html)
* Apache Cassandra v2.0.14
    * [Changelog](2.0.14/CHANGELOG.2.0.14.html)
    * [Release Notes](2.0.14/RELEASENOTES.2.0.14.html)
* Apache Cassandra v2.0.13
    * [Changelog](2.0.13/CHANGELOG.2.0.13.html)
    * [Release Notes](2.0.13/RELEASENOTES.2.0.13.html)
* Apache Cassandra v2.0.12
    * [Changelog](2.0.12/CHANGELOG.2.0.12.html)
    * [Release Notes](2.0.12/RELEASENOTES.2.0.12.html)
* Apache Cassandra v2.0.11
    * [Changelog](2.0.11/CHANGELOG.2.0.11.html)
    * [Release Notes](2.0.11/RELEASENOTES.2.0.11.html)
* Apache Cassandra v2.0.10
    * [Changelog](2.0.10/CHANGELOG.2.0.10.html)
    * [Release Notes](2.0.10/RELEASENOTES.2.0.10.html)
* Apache Cassandra v2.0.9
    * [Changelog](2.0.9/CHANGELOG.2.0.9.html)
    * [Release Notes](2.0.9/RELEASENOTES.2.0.9.html)
* Apache Cassandra v2.0.8
    * [Changelog](2.0.8/CHANGELOG.2.0.8.html)
    * [Release Notes](2.0.8/RELEASENOTES.2.0.8.html)
* Apache Cassandra v2.0.7
    * [Changelog](2.0.7/CHANGELOG.2.0.7.html)
    * [Release Notes](2.0.7/RELEASENOTES.2.0.7.html)
* Apache Cassandra v2.0.6
    * [Changelog](2.0.6/CHANGELOG.2.0.6.html)
    * [Release Notes](2.0.6/RELEASENOTES.2.0.6.html)
* Apache Cassandra v2.0.5
    * [Changelog](2.0.5/CHANGELOG.2.0.5.html)
    * [Release Notes](2.0.5/RELEASENOTES.2.0.5.html)
* Apache Cassandra v2.0.4
    * [Changelog](2.0.4/CHANGELOG.2.0.4.html)
    * [Release Notes](2.0.4/RELEASENOTES.2.0.4.html)
* Apache Cassandra v2.0.3
    * [Changelog](2.0.3/CHANGELOG.2.0.3.html)
    * [Release Notes](2.0.3/RELEASENOTES.2.0.3.html)
* Apache Cassandra v2.0.2
    * [Changelog](2.0.2/CHANGELOG.2.0.2.html)
    * [Release Notes](2.0.2/RELEASENOTES.2.0.2.html)
* Apache Cassandra v2.0.1
    * [Changelog](2.0.1/CHANGELOG.2.0.1.html)
    * [Release Notes](2.0.1/RELEASENOTES.2.0.1.html)
* Apache Cassandra v2.0.0
    * [Changelog](2.0.0/CHANGELOG.2.0.0.html)
    * [Release Notes](2.0.0/RELEASENOTES.2.0.0.html)
* Apache Cassandra v1.2.19
    * [Changelog](1.2.19/CHANGELOG.1.2.19.html)
    * [Release Notes](1.2.19/RELEASENOTES.1.2.19.html)
* Apache Cassandra v1.2.18
    * [Changelog](1.2.18/CHANGELOG.1.2.18.html)
    * [Release Notes](1.2.18/RELEASENOTES.1.2.18.html)
* Apache Cassandra v1.2.17
    * [Changelog](1.2.17/CHANGELOG.1.2.17.html)
    * [Release Notes](1.2.17/RELEASENOTES.1.2.17.html)
* Apache Cassandra v1.2.16
    * [Changelog](1.2.16/CHANGELOG.1.2.16.html)
    * [Release Notes](1.2.16/RELEASENOTES.1.2.16.html)
* Apache Cassandra v1.2.15
    * [Changelog](1.2.15/CHANGELOG.1.2.15.html)
    * [Release Notes](1.2.15/RELEASENOTES.1.2.15.html)
* Apache Cassandra v1.2.14
    * [Changelog](1.2.14/CHANGELOG.1.2.14.html)
    * [Release Notes](1.2.14/RELEASENOTES.1.2.14.html)
* Apache Cassandra v1.2.13
    * [Changelog](1.2.13/CHANGELOG.1.2.13.html)
    * [Release Notes](1.2.13/RELEASENOTES.1.2.13.html)
* Apache Cassandra v1.2.12
    * [Changelog](1.2.12/CHANGELOG.1.2.12.html)
    * [Release Notes](1.2.12/RELEASENOTES.1.2.12.html)
* Apache Cassandra v1.2.11
    * [Changelog](1.2.11/CHANGELOG.1.2.11.html)
    * [Release Notes](1.2.11/RELEASENOTES.1.2.11.html)
* Apache Cassandra v1.2.10
    * [Changelog](1.2.10/CHANGELOG.1.2.10.html)
    * [Release Notes](1.2.10/RELEASENOTES.1.2.10.html)
* Apache Cassandra v1.2.9
    * [Changelog](1.2.9/CHANGELOG.1.2.9.html)
    * [Release Notes](1.2.9/RELEASENOTES.1.2.9.html)
* Apache Cassandra v1.2.8
    * [Changelog](1.2.8/CHANGELOG.1.2.8.html)
    * [Release Notes](1.2.8/RELEASENOTES.1.2.8.html)
* Apache Cassandra v1.2.7
    * [Changelog](1.2.7/CHANGELOG.1.2.7.html)
    * [Release Notes](1.2.7/RELEASENOTES.1.2.7.html)
* Apache Cassandra v1.2.6
    * [Changelog](1.2.6/CHANGELOG.1.2.6.html)
    * [Release Notes](1.2.6/RELEASENOTES.1.2.6.html)
* Apache Cassandra v1.2.5
    * [Changelog](1.2.5/CHANGELOG.1.2.5.html)
    * [Release Notes](1.2.5/RELEASENOTES.1.2.5.html)
* Apache Cassandra v1.2.4
    * [Changelog](1.2.4/CHANGELOG.1.2.4.html)
    * [Release Notes](1.2.4/RELEASENOTES.1.2.4.html)
* Apache Cassandra v1.2.3
    * [Changelog](1.2.3/CHANGELOG.1.2.3.html)
    * [Release Notes](1.2.3/RELEASENOTES.1.2.3.html)
* Apache Cassandra v1.2.2
    * [Changelog](1.2.2/CHANGELOG.1.2.2.html)
    * [Release Notes](1.2.2/RELEASENOTES.1.2.2.html)
* Apache Cassandra v1.2.1
    * [Changelog](1.2.1/CHANGELOG.1.2.1.html)
    * [Release Notes](1.2.1/RELEASENOTES.1.2.1.html)
* Apache Cassandra v1.2.0 rc2
    * [Changelog](1.2.0 rc2/CHANGELOG.1.2.0 rc2.html)
    * [Release Notes](1.2.0 rc2/RELEASENOTES.1.2.0 rc2.html)
* Apache Cassandra v1.2.0 rc1
    * [Changelog](1.2.0 rc1/CHANGELOG.1.2.0 rc1.html)
    * [Release Notes](1.2.0 rc1/RELEASENOTES.1.2.0 rc1.html)
* Apache Cassandra v1.2.0 beta 3
    * [Changelog](1.2.0 beta 3/CHANGELOG.1.2.0 beta 3.html)
    * [Release Notes](1.2.0 beta 3/RELEASENOTES.1.2.0 beta 3.html)
* Apache Cassandra v1.2.0 beta 2
    * [Changelog](1.2.0 beta 2/CHANGELOG.1.2.0 beta 2.html)
    * [Release Notes](1.2.0 beta 2/RELEASENOTES.1.2.0 beta 2.html)
* Apache Cassandra v1.2.0 beta 1
    * [Changelog](1.2.0 beta 1/CHANGELOG.1.2.0 beta 1.html)
    * [Release Notes](1.2.0 beta 1/RELEASENOTES.1.2.0 beta 1.html)
* Apache Cassandra v1.2.0
    * [Changelog](1.2.0/CHANGELOG.1.2.0.html)
    * [Release Notes](1.2.0/RELEASENOTES.1.2.0.html)
* Apache Cassandra v1.1.12
    * [Changelog](1.1.12/CHANGELOG.1.1.12.html)
    * [Release Notes](1.1.12/RELEASENOTES.1.1.12.html)
* Apache Cassandra v1.1.11
    * [Changelog](1.1.11/CHANGELOG.1.1.11.html)
    * [Release Notes](1.1.11/RELEASENOTES.1.1.11.html)
* Apache Cassandra v1.1.10
    * [Changelog](1.1.10/CHANGELOG.1.1.10.html)
    * [Release Notes](1.1.10/RELEASENOTES.1.1.10.html)
* Apache Cassandra v1.1.9
    * [Changelog](1.1.9/CHANGELOG.1.1.9.html)
    * [Release Notes](1.1.9/RELEASENOTES.1.1.9.html)
* Apache Cassandra v1.1.8
    * [Changelog](1.1.8/CHANGELOG.1.1.8.html)
    * [Release Notes](1.1.8/RELEASENOTES.1.1.8.html)
* Apache Cassandra v1.1.7
    * [Changelog](1.1.7/CHANGELOG.1.1.7.html)
    * [Release Notes](1.1.7/RELEASENOTES.1.1.7.html)
* Apache Cassandra v1.1.6
    * [Changelog](1.1.6/CHANGELOG.1.1.6.html)
    * [Release Notes](1.1.6/RELEASENOTES.1.1.6.html)
* Apache Cassandra v1.1.5
    * [Changelog](1.1.5/CHANGELOG.1.1.5.html)
    * [Release Notes](1.1.5/RELEASENOTES.1.1.5.html)
* Apache Cassandra v1.1.4
    * [Changelog](1.1.4/CHANGELOG.1.1.4.html)
    * [Release Notes](1.1.4/RELEASENOTES.1.1.4.html)
* Apache Cassandra v1.1.3
    * [Changelog](1.1.3/CHANGELOG.1.1.3.html)
    * [Release Notes](1.1.3/RELEASENOTES.1.1.3.html)
* Apache Cassandra v1.1.2
    * [Changelog](1.1.2/CHANGELOG.1.1.2.html)
    * [Release Notes](1.1.2/RELEASENOTES.1.1.2.html)
* Apache Cassandra v1.1.1
    * [Changelog](1.1.1/CHANGELOG.1.1.1.html)
    * [Release Notes](1.1.1/RELEASENOTES.1.1.1.html)
* Apache Cassandra v1.1.0
    * [Changelog](1.1.0/CHANGELOG.1.1.0.html)
    * [Release Notes](1.1.0/RELEASENOTES.1.1.0.html)
* Apache Cassandra v1.0.12
    * [Changelog](1.0.12/CHANGELOG.1.0.12.html)
    * [Release Notes](1.0.12/RELEASENOTES.1.0.12.html)
* Apache Cassandra v1.0.11
    * [Changelog](1.0.11/CHANGELOG.1.0.11.html)
    * [Release Notes](1.0.11/RELEASENOTES.1.0.11.html)
* Apache Cassandra v1.0.10
    * [Changelog](1.0.10/CHANGELOG.1.0.10.html)
    * [Release Notes](1.0.10/RELEASENOTES.1.0.10.html)
* Apache Cassandra v1.0.9
    * [Changelog](1.0.9/CHANGELOG.1.0.9.html)
    * [Release Notes](1.0.9/RELEASENOTES.1.0.9.html)
* Apache Cassandra v1.0.8
    * [Changelog](1.0.8/CHANGELOG.1.0.8.html)
    * [Release Notes](1.0.8/RELEASENOTES.1.0.8.html)
* Apache Cassandra v1.0.7
    * [Changelog](1.0.7/CHANGELOG.1.0.7.html)
    * [Release Notes](1.0.7/RELEASENOTES.1.0.7.html)
* Apache Cassandra v1.0.6
    * [Changelog](1.0.6/CHANGELOG.1.0.6.html)
    * [Release Notes](1.0.6/RELEASENOTES.1.0.6.html)
* Apache Cassandra v1.0.5
    * [Changelog](1.0.5/CHANGELOG.1.0.5.html)
    * [Release Notes](1.0.5/RELEASENOTES.1.0.5.html)
* Apache Cassandra v1.0.4
    * [Changelog](1.0.4/CHANGELOG.1.0.4.html)
    * [Release Notes](1.0.4/RELEASENOTES.1.0.4.html)
* Apache Cassandra v1.0.3
    * [Changelog](1.0.3/CHANGELOG.1.0.3.html)
    * [Release Notes](1.0.3/RELEASENOTES.1.0.3.html)
* Apache Cassandra v1.0.2
    * [Changelog](1.0.2/CHANGELOG.1.0.2.html)
    * [Release Notes](1.0.2/RELEASENOTES.1.0.2.html)
* Apache Cassandra v1.0.1
    * [Changelog](1.0.1/CHANGELOG.1.0.1.html)
    * [Release Notes](1.0.1/RELEASENOTES.1.0.1.html)
* Apache Cassandra v1.0.0
    * [Changelog](1.0.0/CHANGELOG.1.0.0.html)
    * [Release Notes](1.0.0/RELEASENOTES.1.0.0.html)
* Apache Cassandra v0.8 beta 1
    * [Changelog](0.8 beta 1/CHANGELOG.0.8 beta 1.html)
    * [Release Notes](0.8 beta 1/RELEASENOTES.0.8 beta 1.html)
* Apache Cassandra v0.8.10
    * [Changelog](0.8.10/CHANGELOG.0.8.10.html)
    * [Release Notes](0.8.10/RELEASENOTES.0.8.10.html)
* Apache Cassandra v0.8.9
    * [Changelog](0.8.9/CHANGELOG.0.8.9.html)
    * [Release Notes](0.8.9/RELEASENOTES.0.8.9.html)
* Apache Cassandra v0.8.8
    * [Changelog](0.8.8/CHANGELOG.0.8.8.html)
    * [Release Notes](0.8.8/RELEASENOTES.0.8.8.html)
* Apache Cassandra v0.8.7
    * [Changelog](0.8.7/CHANGELOG.0.8.7.html)
    * [Release Notes](0.8.7/RELEASENOTES.0.8.7.html)
* Apache Cassandra v0.8.6
    * [Changelog](0.8.6/CHANGELOG.0.8.6.html)
    * [Release Notes](0.8.6/RELEASENOTES.0.8.6.html)
* Apache Cassandra v0.8.5
    * [Changelog](0.8.5/CHANGELOG.0.8.5.html)
    * [Release Notes](0.8.5/RELEASENOTES.0.8.5.html)
* Apache Cassandra v0.8.4
    * [Changelog](0.8.4/CHANGELOG.0.8.4.html)
    * [Release Notes](0.8.4/RELEASENOTES.0.8.4.html)
* Apache Cassandra v0.8.3
    * [Changelog](0.8.3/CHANGELOG.0.8.3.html)
    * [Release Notes](0.8.3/RELEASENOTES.0.8.3.html)
* Apache Cassandra v0.8.2
    * [Changelog](0.8.2/CHANGELOG.0.8.2.html)
    * [Release Notes](0.8.2/RELEASENOTES.0.8.2.html)
* Apache Cassandra v0.8.1
    * [Changelog](0.8.1/CHANGELOG.0.8.1.html)
    * [Release Notes](0.8.1/RELEASENOTES.0.8.1.html)
* Apache Cassandra v0.8.0 beta 2
    * [Changelog](0.8.0 beta 2/CHANGELOG.0.8.0 beta 2.html)
    * [Release Notes](0.8.0 beta 2/RELEASENOTES.0.8.0 beta 2.html)
* Apache Cassandra v0.8.0
    * [Changelog](0.8.0/CHANGELOG.0.8.0.html)
    * [Release Notes](0.8.0/RELEASENOTES.0.8.0.html)
* Apache Cassandra v0.7 beta 3
    * [Changelog](0.7 beta 3/CHANGELOG.0.7 beta 3.html)
    * [Release Notes](0.7 beta 3/RELEASENOTES.0.7 beta 3.html)
* Apache Cassandra v0.7 beta 2
    * [Changelog](0.7 beta 2/CHANGELOG.0.7 beta 2.html)
    * [Release Notes](0.7 beta 2/RELEASENOTES.0.7 beta 2.html)
* Apache Cassandra v0.7 beta 1
    * [Changelog](0.7 beta 1/CHANGELOG.0.7 beta 1.html)
    * [Release Notes](0.7 beta 1/RELEASENOTES.0.7 beta 1.html)
* Apache Cassandra v0.7.10
    * [Changelog](0.7.10/CHANGELOG.0.7.10.html)
    * [Release Notes](0.7.10/RELEASENOTES.0.7.10.html)
* Apache Cassandra v0.7.9
    * [Changelog](0.7.9/CHANGELOG.0.7.9.html)
    * [Release Notes](0.7.9/RELEASENOTES.0.7.9.html)
* Apache Cassandra v0.7.8
    * [Changelog](0.7.8/CHANGELOG.0.7.8.html)
    * [Release Notes](0.7.8/RELEASENOTES.0.7.8.html)
* Apache Cassandra v0.7.7
    * [Changelog](0.7.7/CHANGELOG.0.7.7.html)
    * [Release Notes](0.7.7/RELEASENOTES.0.7.7.html)
* Apache Cassandra v0.7.6
    * [Changelog](0.7.6/CHANGELOG.0.7.6.html)
    * [Release Notes](0.7.6/RELEASENOTES.0.7.6.html)
* Apache Cassandra v0.7.5
    * [Changelog](0.7.5/CHANGELOG.0.7.5.html)
    * [Release Notes](0.7.5/RELEASENOTES.0.7.5.html)
* Apache Cassandra v0.7.4
    * [Changelog](0.7.4/CHANGELOG.0.7.4.html)
    * [Release Notes](0.7.4/RELEASENOTES.0.7.4.html)
* Apache Cassandra v0.7.3
    * [Changelog](0.7.3/CHANGELOG.0.7.3.html)
    * [Release Notes](0.7.3/RELEASENOTES.0.7.3.html)
* Apache Cassandra v0.7.2
    * [Changelog](0.7.2/CHANGELOG.0.7.2.html)
    * [Release Notes](0.7.2/RELEASENOTES.0.7.2.html)
* Apache Cassandra v0.7.1
    * [Changelog](0.7.1/CHANGELOG.0.7.1.html)
    * [Release Notes](0.7.1/RELEASENOTES.0.7.1.html)
* Apache Cassandra v0.7.0 rc 3
    * [Changelog](0.7.0 rc 3/CHANGELOG.0.7.0 rc 3.html)
    * [Release Notes](0.7.0 rc 3/RELEASENOTES.0.7.0 rc 3.html)
* Apache Cassandra v0.7.0 rc 2
    * [Changelog](0.7.0 rc 2/CHANGELOG.0.7.0 rc 2.html)
    * [Release Notes](0.7.0 rc 2/RELEASENOTES.0.7.0 rc 2.html)
* Apache Cassandra v0.7.0 rc 1
    * [Changelog](0.7.0 rc 1/CHANGELOG.0.7.0 rc 1.html)
    * [Release Notes](0.7.0 rc 1/RELEASENOTES.0.7.0 rc 1.html)
* Apache Cassandra v0.7.0
    * [Changelog](0.7.0/CHANGELOG.0.7.0.html)
    * [Release Notes](0.7.0/RELEASENOTES.0.7.0.html)
* Apache Cassandra v0.6.13
    * [Changelog](0.6.13/CHANGELOG.0.6.13.html)
    * [Release Notes](0.6.13/RELEASENOTES.0.6.13.html)
* Apache Cassandra v0.6.12
    * [Changelog](0.6.12/CHANGELOG.0.6.12.html)
    * [Release Notes](0.6.12/RELEASENOTES.0.6.12.html)
* Apache Cassandra v0.6.11
    * [Changelog](0.6.11/CHANGELOG.0.6.11.html)
    * [Release Notes](0.6.11/RELEASENOTES.0.6.11.html)
* Apache Cassandra v0.6.10
    * [Changelog](0.6.10/CHANGELOG.0.6.10.html)
    * [Release Notes](0.6.10/RELEASENOTES.0.6.10.html)
* Apache Cassandra v0.6.9
    * [Changelog](0.6.9/CHANGELOG.0.6.9.html)
    * [Release Notes](0.6.9/RELEASENOTES.0.6.9.html)
* Apache Cassandra v0.6.8
    * [Changelog](0.6.8/CHANGELOG.0.6.8.html)
    * [Release Notes](0.6.8/RELEASENOTES.0.6.8.html)
* Apache Cassandra v0.6.7
    * [Changelog](0.6.7/CHANGELOG.0.6.7.html)
    * [Release Notes](0.6.7/RELEASENOTES.0.6.7.html)
* Apache Cassandra v0.6.6
    * [Changelog](0.6.6/CHANGELOG.0.6.6.html)
    * [Release Notes](0.6.6/RELEASENOTES.0.6.6.html)
* Apache Cassandra v0.6.5
    * [Changelog](0.6.5/CHANGELOG.0.6.5.html)
    * [Release Notes](0.6.5/RELEASENOTES.0.6.5.html)
* Apache Cassandra v0.6.4
    * [Changelog](0.6.4/CHANGELOG.0.6.4.html)
    * [Release Notes](0.6.4/RELEASENOTES.0.6.4.html)
* Apache Cassandra v0.6.3
    * [Changelog](0.6.3/CHANGELOG.0.6.3.html)
    * [Release Notes](0.6.3/RELEASENOTES.0.6.3.html)
* Apache Cassandra v0.6.2
    * [Changelog](0.6.2/CHANGELOG.0.6.2.html)
    * [Release Notes](0.6.2/RELEASENOTES.0.6.2.html)
* Apache Cassandra v0.6.1
    * [Changelog](0.6.1/CHANGELOG.0.6.1.html)
    * [Release Notes](0.6.1/RELEASENOTES.0.6.1.html)
* Apache Cassandra v0.6
    * [Changelog](0.6/CHANGELOG.0.6.html)
    * [Release Notes](0.6/RELEASENOTES.0.6.html)
* Apache Cassandra v0.5
    * [Changelog](0.5/CHANGELOG.0.5.html)
    * [Release Notes](0.5/RELEASENOTES.0.5.html)
* Apache Cassandra v0.4
    * [Changelog](0.4/CHANGELOG.0.4.html)
    * [Release Notes](0.4/RELEASENOTES.0.4.html)
* Apache Cassandra v0.3
    * [Changelog](0.3/CHANGELOG.0.3.html)
    * [Release Notes](0.3/RELEASENOTES.0.3.html)
