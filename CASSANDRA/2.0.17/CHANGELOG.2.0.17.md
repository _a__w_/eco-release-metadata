
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.0.17 - 2015-09-21



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9603](https://issues.apache.org/jira/browse/CASSANDRA-9603) | Expose private listen\_address in system.local |  Major | Distributed Metadata | Piotr Kołaczkowski | Carl Yeksigian |
| [CASSANDRA-9591](https://issues.apache.org/jira/browse/CASSANDRA-9591) | Scrub (recover) sstables even when -Index.db is missing |  Major | . | mck | mck |
| [CASSANDRA-9682](https://issues.apache.org/jira/browse/CASSANDRA-9682) | setting log4j.logger.org.apache.cassandra=DEBUG causes keyspace username/password to show up in system.log |  Major | Observability | Victor Chen | Sam Tunnicliffe |
| [CASSANDRA-9793](https://issues.apache.org/jira/browse/CASSANDRA-9793) | Log when messages are dropped due to cross\_node\_timeout |  Major | Streaming and Messaging | Brandon Williams | Stefania |
| [CASSANDRA-10015](https://issues.apache.org/jira/browse/CASSANDRA-10015) | Create tool to debug why expired sstables are not getting dropped |  Major | Tools | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-10330](https://issues.apache.org/jira/browse/CASSANDRA-10330) | Gossipinfo could return more useful information |  Minor | . | Brandon Williams | Brandon Williams |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9565](https://issues.apache.org/jira/browse/CASSANDRA-9565) | 'WITH WITH' in alter keyspace statements causes NPE |  Major | . | Jim Witschey | Benjamin Lerer |
| [CASSANDRA-9527](https://issues.apache.org/jira/browse/CASSANDRA-9527) | Cannot create secondary index on a table WITH COMPACT STORAGE |  Minor | CQL, Secondary Indexes | fuggy\_yama | Benjamin Lerer |
| [CASSANDRA-9385](https://issues.apache.org/jira/browse/CASSANDRA-9385) | cqlsh autocomplete does not work for DTCS min\_threshold |  Trivial | . | Sebastian Estevez | Alex Buck |
| [CASSANDRA-9631](https://issues.apache.org/jira/browse/CASSANDRA-9631) | Unnecessary required filtering for query on indexed clustering key |  Major | CQL | Kevin Deldycke | Benjamin Lerer |
| [CASSANDRA-9649](https://issues.apache.org/jira/browse/CASSANDRA-9649) | Paxos ballot in StorageProxy could clash |  Minor | Coordination | Stefania | Stefania |
| [CASSANDRA-9560](https://issues.apache.org/jira/browse/CASSANDRA-9560) | Changing durable\_writes on a keyspace is only applied after restart of node |  Major | Distributed Metadata | Fred A | Carl Yeksigian |
| [CASSANDRA-9636](https://issues.apache.org/jira/browse/CASSANDRA-9636) | Duplicate columns in selection causes AssertionError |  Major | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-9727](https://issues.apache.org/jira/browse/CASSANDRA-9727) | AuthSuccess NPE |  Major | . | Pierre N. |  |
| [CASSANDRA-9662](https://issues.apache.org/jira/browse/CASSANDRA-9662) | compactionManager reporting wrong pendingtasks |  Minor | CQL | Tony Xu | Yuki Morishita |
| [CASSANDRA-9740](https://issues.apache.org/jira/browse/CASSANDRA-9740) | Can't transition from write survey to normal mode |  Trivial | . | Jason Brown | Jason Brown |
| [CASSANDRA-9519](https://issues.apache.org/jira/browse/CASSANDRA-9519) | CASSANDRA-8448 Doesn't seem to be fixed |  Major | . | Jeremiah Jordan | Sylvain Lebresne |
| [CASSANDRA-9765](https://issues.apache.org/jira/browse/CASSANDRA-9765) | checkForEndpointCollision fails for legitimate collisions |  Major | Distributed Metadata | Richard Low | Stefania |
| [CASSANDRA-9382](https://issues.apache.org/jira/browse/CASSANDRA-9382) | Snapshot file descriptors not getting purged (possible fd leak) |  Major | Local Write-Read Paths | Mark Curtis | Yuki Morishita |
| [CASSANDRA-9959](https://issues.apache.org/jira/browse/CASSANDRA-9959) | Expected bloom filter size should not be an int |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9129](https://issues.apache.org/jira/browse/CASSANDRA-9129) | HintedHandoff in pending state forever after upgrading to 2.0.14 from 2.0.11 and 2.0.12 |  Major | Observability | Russ Lavoie | Sam Tunnicliffe |
| [CASSANDRA-9487](https://issues.apache.org/jira/browse/CASSANDRA-9487) | CommitLogTest hangs intermittently in 2.0 |  Major | Testing | Michael Shuler | Branimir Lambov |
| [CASSANDRA-9882](https://issues.apache.org/jira/browse/CASSANDRA-9882) | DTCS (maybe other strategies) can block flushing when there are lots of sstables |  Major | Compaction | Jeremiah Jordan | Marcus Eriksson |
| [CASSANDRA-10144](https://issues.apache.org/jira/browse/CASSANDRA-10144) | CASSANDRA-8989 backported CASSANDRA-6863 but didn't include the fix from CASSANDRA-8013 in that backport |  Major | . | Jeremiah Jordan | Carl Yeksigian |
| [CASSANDRA-8741](https://issues.apache.org/jira/browse/CASSANDRA-8741) | Running a drain before a decommission apparently the wrong thing to do |  Trivial | . | Casey Marshall | Jan Karlsson |
| [CASSANDRA-10240](https://issues.apache.org/jira/browse/CASSANDRA-10240) | sstableexpiredblockers can throw FileNotFound exceptions |  Major | Tools | Brandon Williams | Marcus Eriksson |
| [CASSANDRA-10366](https://issues.apache.org/jira/browse/CASSANDRA-10366) | Added gossip states can shadow older unseen states |  Critical | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-10238](https://issues.apache.org/jira/browse/CASSANDRA-10238) | Consolidating racks violates the RF contract |  Critical | Coordination | Brandon Williams | Stefania |


