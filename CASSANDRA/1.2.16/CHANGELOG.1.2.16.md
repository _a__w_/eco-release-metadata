
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.2.16 - 2014-03-31



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6655](https://issues.apache.org/jira/browse/CASSANDRA-6655) | Writing mostly deletes to a Memtable results in undercounting the table's occupancy so it may not flush |  Minor | . | Benedict | Benedict |
| [CASSANDRA-6718](https://issues.apache.org/jira/browse/CASSANDRA-6718) | DateType should accept pre-epoch (negative) timestamp in fromString |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6645](https://issues.apache.org/jira/browse/CASSANDRA-6645) | upgradesstables causes NPE for secondary indexes without an underlying column family |  Major | Secondary Indexes | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-6678](https://issues.apache.org/jira/browse/CASSANDRA-6678) | Unwanted schema pull while upgrading nodes from 1.2 to 2.0 |  Major | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-6667](https://issues.apache.org/jira/browse/CASSANDRA-6667) | Mean cells per sstable is calculated incorrectly |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-6685](https://issues.apache.org/jira/browse/CASSANDRA-6685) | Nodes never bootstrap if schema is empty |  Major | . | Richard Low | Brandon Williams |
| [CASSANDRA-6649](https://issues.apache.org/jira/browse/CASSANDRA-6649) | CQL: disallow counter update with "USING TIMESTAMP" and "USING TTL" |  Trivial | . | Sylvain Lebresne | Aleksey Yeschenko |
| [CASSANDRA-6622](https://issues.apache.org/jira/browse/CASSANDRA-6622) | Streaming session failures during node replace of same address |  Major | . | Ravi Prasad | Brandon Williams |
| [CASSANDRA-6695](https://issues.apache.org/jira/browse/CASSANDRA-6695) | Don't exchange schema between nodes with different versions (no pull, no push) |  Major | . | Piotr Kołaczkowski | Aleksey Yeschenko |
| [CASSANDRA-6700](https://issues.apache.org/jira/browse/CASSANDRA-6700) | Use real node messaging versions for schema exchange decisions |  Major | . | Piotr Kołaczkowski | Aleksey Yeschenko |
| [CASSANDRA-6701](https://issues.apache.org/jira/browse/CASSANDRA-6701) | IN on the last clustering columns + ORDER BY DESC yield no results |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6711](https://issues.apache.org/jira/browse/CASSANDRA-6711) | SecondaryIndexManager#deleteFromIndexes() doesn't correctly retrieve column indexes |  Major | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-6713](https://issues.apache.org/jira/browse/CASSANDRA-6713) | Snapshot based repair does not send snapshot command to itself |  Major | . | sankalp kohli | Yuki Morishita |
| [CASSANDRA-6732](https://issues.apache.org/jira/browse/CASSANDRA-6732) | Cross DC writes not compatible 1.2-\>1.1 during rolling upgrade |  Major | . | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-5631](https://issues.apache.org/jira/browse/CASSANDRA-5631) | NPE when creating column family shortly after multinode startup |  Major | . | Martin Serrano | Aleksey Yeschenko |
| [CASSANDRA-6735](https://issues.apache.org/jira/browse/CASSANDRA-6735) | Exceptions during memtable flushes on shutdown hook prevent process shutdown |  Minor | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-6791](https://issues.apache.org/jira/browse/CASSANDRA-6791) | CompressedSequentialWriter can write zero-length segments during scrub |  Minor | . | Jonathan Ellis | Viktor Kuzmin |
| [CASSANDRA-6811](https://issues.apache.org/jira/browse/CASSANDRA-6811) | nodetool no longer shows node joining |  Minor | . | Brandon Williams | Vijay |
| [CASSANDRA-6803](https://issues.apache.org/jira/browse/CASSANDRA-6803) | nodetool getsstables converts key from String incorrectly |  Major | Tools | Nate McCall | Nate McCall |
| [CASSANDRA-6847](https://issues.apache.org/jira/browse/CASSANDRA-6847) | The binary transport doesn't load truststore file |  Minor | . | Mikhail Stepura | Mikhail Stepura |
| [CASSANDRA-6773](https://issues.apache.org/jira/browse/CASSANDRA-6773) | Delimiter not working for special characters in COPY command from CQLSH |  Trivial | . | Shiti Saxena | Shiti Saxena |
| [CASSANDRA-6376](https://issues.apache.org/jira/browse/CASSANDRA-6376) | Pig tests are failing |  Minor | . | Sylvain Lebresne | Alex Liu |
| [CASSANDRA-6613](https://issues.apache.org/jira/browse/CASSANDRA-6613) | Java 7u51 breaks internode encryption |  Major | . | Ray Sinnema | Brandon Williams |
| [CASSANDRA-6104](https://issues.apache.org/jira/browse/CASSANDRA-6104) | Add additional limits in cassandra.conf provided by Debian package |  Trivial | Packaging | J.B. Langston | Brandon Williams |
| [CASSANDRA-6816](https://issues.apache.org/jira/browse/CASSANDRA-6816) | CQL3 docs don't mention BATCH UNLOGGED |  Minor | Documentation and Website | Duncan Sands | Tyler Hobbs |
| [CASSANDRA-6891](https://issues.apache.org/jira/browse/CASSANDRA-6891) | nodetool status always does dns lookups, even if you didn't pass --resolve-ip |  Major | . | Jeremiah Jordan |  |
| [CASSANDRA-6168](https://issues.apache.org/jira/browse/CASSANDRA-6168) | nodetool status should issue a warning when no keyspace is specified |  Minor | Tools | Patricio Echague | Vijay |
| [CASSANDRA-6896](https://issues.apache.org/jira/browse/CASSANDRA-6896) | status output is confused when hostname resolution is enabled |  Minor | Tools | Brandon Williams | Vijay |
| [CASSANDRA-6818](https://issues.apache.org/jira/browse/CASSANDRA-6818) | SSTable references not released if stream session fails before it starts |  Major | . | Richard Low | Yuki Morishita |
| [CASSANDRA-6541](https://issues.apache.org/jira/browse/CASSANDRA-6541) | New versions of Hotspot create new Class objects on every JMX connection causing the heap to fill up with them if CMSClassUnloadingEnabled isn't set. |  Minor | Configuration | jonathan lacefield | Brandon Williams |


