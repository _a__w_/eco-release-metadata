
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.7.5 - 2011-04-27



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2343](https://issues.apache.org/jira/browse/CASSANDRA-2343) | Allow configured ports to be overridden with system properties |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-2354](https://issues.apache.org/jira/browse/CASSANDRA-2354) | CLI should allow users to chose consistency level |  Minor | Tools | Edward Capriolo | Edward Capriolo |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2318](https://issues.apache.org/jira/browse/CASSANDRA-2318) | Avoid seeking when sstable2json exports the entire file |  Minor | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2329](https://issues.apache.org/jira/browse/CASSANDRA-2329) | comment topology file about ipv6 configuration |  Trivial | . | Eric Tamme |  |
| [CASSANDRA-2331](https://issues.apache.org/jira/browse/CASSANDRA-2331) | Allow a Job Configuration to control the consistency level with which reads and writes occur for mapreduce jobs. |  Minor | . | Eldon Stegall |  |
| [CASSANDRA-2330](https://issues.apache.org/jira/browse/CASSANDRA-2330) | Queue indexes for flush before the parent |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-2080](https://issues.apache.org/jira/browse/CASSANDRA-2080) | Upgrade to release of Whirr 0.3.0 |  Trivial | . | Stu Hood | Stu Hood |
| [CASSANDRA-1618](https://issues.apache.org/jira/browse/CASSANDRA-1618) | Allow specifying a slice predicate for pig queries to filter out columns |  Major | . | Jeremy Hanna | Brandon Williams |
| [CASSANDRA-2345](https://issues.apache.org/jira/browse/CASSANDRA-2345) | CLI: Units on show keyspaces output |  Minor | . | Jon Hermes | Pavel Yaskevich |
| [CASSANDRA-2361](https://issues.apache.org/jira/browse/CASSANDRA-2361) | AES depends on java serialization |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-2414](https://issues.apache.org/jira/browse/CASSANDRA-2414) | IntegerType isn't noted in cli |  Trivial | . | Joaquin Casares | Joaquin Casares |
| [CASSANDRA-2387](https://issues.apache.org/jira/browse/CASSANDRA-2387) | Make it possible for pig to understand packed data |  Major | . | Jeremy Hanna | Jeremy Hanna |
| [CASSANDRA-2403](https://issues.apache.org/jira/browse/CASSANDRA-2403) | Backport AbstractType.compose from trunk |  Minor | . | Brandon Williams |  |
| [CASSANDRA-2425](https://issues.apache.org/jira/browse/CASSANDRA-2425) | Avoid unnecessary copies in range/index scans |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2442](https://issues.apache.org/jira/browse/CASSANDRA-2442) | For 'describe keyspace ks' - it should output the default\_validation\_class for each column family |  Trivial | Tools | Jeremy Hanna | Pavel Yaskevich |
| [CASSANDRA-2451](https://issues.apache.org/jira/browse/CASSANDRA-2451) | Make clean compactions cleanup the row cache |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2460](https://issues.apache.org/jira/browse/CASSANDRA-2460) | Make scrub validate deserialized columns |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2429](https://issues.apache.org/jira/browse/CASSANDRA-2429) | Silence non-errors while reconnecting |  Trivial | . | Joaquin Casares | Jonathan Ellis |
| [CASSANDRA-2502](https://issues.apache.org/jira/browse/CASSANDRA-2502) | disable cache saving on system CFs |  Minor | . | Ryan King | Ryan King |
| [CASSANDRA-2513](https://issues.apache.org/jira/browse/CASSANDRA-2513) | r/m unnecessary AbstractType getInstance methods |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1851](https://issues.apache.org/jira/browse/CASSANDRA-1851) | Publish cassandra artifacts to the maven central repository |  Minor | Packaging | Stephen Connolly | Stephen Connolly |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2320](https://issues.apache.org/jira/browse/CASSANDRA-2320) | Dropping an index leaves index in Built state in system table |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2258](https://issues.apache.org/jira/browse/CASSANDRA-2258) | service.SerializationsTest failes under cobertura |  Minor | Testing | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-1761](https://issues.apache.org/jira/browse/CASSANDRA-1761) | Indexes: Auto-generating the CFname may collide with user-generated names |  Minor | . | Jon Hermes | Jon Hermes |
| [CASSANDRA-2269](https://issues.apache.org/jira/browse/CASSANDRA-2269) | OOM in the Thrift thread doesn't shut down server |  Minor | CQL | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2337](https://issues.apache.org/jira/browse/CASSANDRA-2337) | Windows: CliTest broken because of /r/n |  Major | . | Benjamin Coverston | Benjamin Coverston |
| [CASSANDRA-2328](https://issues.apache.org/jira/browse/CASSANDRA-2328) | Index predicate values used in get\_indexed\_slice() are not validated |  Minor | . | amorton | amorton |
| [CASSANDRA-2347](https://issues.apache.org/jira/browse/CASSANDRA-2347) | index scan uses incorrect comparator on non-indexed expressions |  Major | . | Jonathan Ellis | Roland Gude |
| [CASSANDRA-2349](https://issues.apache.org/jira/browse/CASSANDRA-2349) | Expring columns can expire between the two phase of LazilyCompactedRow. |  Critical | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2350](https://issues.apache.org/jira/browse/CASSANDRA-2350) | Races between schema changes and StorageService operations |  Minor | . | Jeffrey Wang | Jonathan Ellis |
| [CASSANDRA-2360](https://issues.apache.org/jira/browse/CASSANDRA-2360) | help in schema-sample uses wrong file name |  Trivial | . | amorton | amorton |
| [CASSANDRA-2365](https://issues.apache.org/jira/browse/CASSANDRA-2365) | ByteBufferUtil.read(byte[]) returns 0 when the end of the stream has been reached. |  Minor | . | Yewei Zhang | Jonathan Ellis |
| [CASSANDRA-2333](https://issues.apache.org/jira/browse/CASSANDRA-2333) | Clean up thread pool and queue sizes |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2377](https://issues.apache.org/jira/browse/CASSANDRA-2377) | NPE During Repair In StreamReplyVerbHandler |  Major | . | Benjamin Coverston | Brandon Williams |
| [CASSANDRA-2390](https://issues.apache.org/jira/browse/CASSANDRA-2390) | MarshalException is thrown when cassandra-cli creates the example Keyspace specified by conf/schema-sample.txt |  Minor | . | Jingguo Yao | Jonathan Ellis |
| [CASSANDRA-2376](https://issues.apache.org/jira/browse/CASSANDRA-2376) | Both name an index iterators cast block offset to int |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2381](https://issues.apache.org/jira/browse/CASSANDRA-2381) | orphaned data files may be created during migration race |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2367](https://issues.apache.org/jira/browse/CASSANDRA-2367) | Cleanup conversions between bytes and strings |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2323](https://issues.apache.org/jira/browse/CASSANDRA-2323) | stress.java should not allow arbitrary arguments |  Trivial | . | Brandon Williams | Pavel Yaskevich |
| [CASSANDRA-2358](https://issues.apache.org/jira/browse/CASSANDRA-2358) | CLI doesn't handle inserting negative integers |  Trivial | Tools | Tyler Hobbs | Pavel Yaskevich |
| [CASSANDRA-2413](https://issues.apache.org/jira/browse/CASSANDRA-2413) | Reduce default memtable size |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2417](https://issues.apache.org/jira/browse/CASSANDRA-2417) | convert mmap assertion to if/throw |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2382](https://issues.apache.org/jira/browse/CASSANDRA-2382) | statistics component not fsynced |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2418](https://issues.apache.org/jira/browse/CASSANDRA-2418) | default gc log settings overwrite previous log |  Minor | . | Chris Burroughs | Chris Burroughs |
| [CASSANDRA-2431](https://issues.apache.org/jira/browse/CASSANDRA-2431) | Try harder to close scanners after a failed compaction |  Major | . | Stu Hood | Stu Hood |
| [CASSANDRA-2428](https://issues.apache.org/jira/browse/CASSANDRA-2428) | Running cleanup on a node with join\_ring=false removes all data |  Critical | . | Chris Goffinet | Sylvain Lebresne |
| [CASSANDRA-2279](https://issues.apache.org/jira/browse/CASSANDRA-2279) | Tombstones not collected post-repair |  Minor | Tools | Joaquin Casares | Sylvain Lebresne |
| [CASSANDRA-2305](https://issues.apache.org/jira/browse/CASSANDRA-2305) | Tombstoned rows not purged from cache after gcgraceseconds |  Minor | . | Jeffrey Wang | Sylvain Lebresne |
| [CASSANDRA-2435](https://issues.apache.org/jira/browse/CASSANDRA-2435) | auto bootstrap happened on already bootstrapped nodes |  Critical | . | Peter Schuller | Jonathan Ellis |
| [CASSANDRA-2463](https://issues.apache.org/jira/browse/CASSANDRA-2463) | Flush and Compaction Unnecessarily Allocate 256MB Contiguous Buffers |  Major | . | C. Scott Andreas | C. Scott Andreas |
| [CASSANDRA-2465](https://issues.apache.org/jira/browse/CASSANDRA-2465) | Pig load/storefunc loads only one schema and BytesType validation class needs fix |  Major | . | Jeremy Hanna | Jeremy Hanna |
| [CASSANDRA-2326](https://issues.apache.org/jira/browse/CASSANDRA-2326) | stress.java indexed range slicing is broken |  Trivial | . | Brandon Williams | Pavel Yaskevich |
| [CASSANDRA-2088](https://issues.apache.org/jira/browse/CASSANDRA-2088) | Clean up after failed (repair) streaming operation |  Minor | . | Stu Hood | Sylvain Lebresne |
| [CASSANDRA-2490](https://issues.apache.org/jira/browse/CASSANDRA-2490) | DatabaseDescriptor.defsVersion should be volatile |  Minor | . | Jeffrey Wang | Jeffrey Wang |
| [CASSANDRA-2484](https://issues.apache.org/jira/browse/CASSANDRA-2484) | CassandraStorage LoadPushDown implementation causes heisenbugs |  Major | . | Jeremy Hanna | Jeremy Hanna |
| [CASSANDRA-2406](https://issues.apache.org/jira/browse/CASSANDRA-2406) | Secondary index and index expression problems |  Major | Secondary Indexes | Muga Nishizawa | Pavel Yaskevich |
| [CASSANDRA-2420](https://issues.apache.org/jira/browse/CASSANDRA-2420) | row cache / streaming aren't aware of each other |  Minor | . | Matthew F. Dennis | Sylvain Lebresne |
| [CASSANDRA-2416](https://issues.apache.org/jira/browse/CASSANDRA-2416) | NullPointerException in CacheWriter.saveCache() |  Minor | . | Shotaro Kamio | Jonathan Ellis |
| [CASSANDRA-2371](https://issues.apache.org/jira/browse/CASSANDRA-2371) | Removed/Dead Node keeps reappearing |  Minor | Tools | techlabs | Brandon Williams |
| [CASSANDRA-2283](https://issues.apache.org/jira/browse/CASSANDRA-2283) | Streaming Old Format Data Fails in 0.7.3 after upgrade from 0.6.8 |  Major | . | Erik Onnen | Jonathan Ellis |
| [CASSANDRA-2316](https://issues.apache.org/jira/browse/CASSANDRA-2316) | NoSuchElement exception on node which is streaming a repair |  Major | . | Jason Harvey | Jonathan Ellis |
| [CASSANDRA-2290](https://issues.apache.org/jira/browse/CASSANDRA-2290) | Repair hangs if one of the neighbor is dead |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2492](https://issues.apache.org/jira/browse/CASSANDRA-2492) | add an escapeSQLString function and fix unescapeSQLString |  Trivial | . | amorton | amorton |
| [CASSANDRA-2519](https://issues.apache.org/jira/browse/CASSANDRA-2519) | row deletions do not add to memtable op count |  Minor | . | amorton | amorton |
| [CASSANDRA-2514](https://issues.apache.org/jira/browse/CASSANDRA-2514) | batch\_mutate operations with CL=LOCAL\_QUORUM throw TimeOutException when there aren't sufficient live nodes |  Minor | . | Narendra Sharma | Narendra Sharma |
| [CASSANDRA-2512](https://issues.apache.org/jira/browse/CASSANDRA-2512) | Updating a column's validation class from AsciiType to UTF8Type does not actually work |  Major | . | Cathy Daw | Jonathan Ellis |
| [CASSANDRA-2516](https://issues.apache.org/jira/browse/CASSANDRA-2516) | Allow LOCAL\_QUORUM, EACH\_QUORUM CLs to work w/ any Strategy class |  Minor | . | Jonathan Ellis | Jonathan Ellis |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2227](https://issues.apache.org/jira/browse/CASSANDRA-2227) | add cache loading to row/key cache tests |  Minor | . | Jonathan Ellis | Pavel Yaskevich |


