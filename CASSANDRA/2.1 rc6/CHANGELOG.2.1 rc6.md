
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1 rc6 - 2014-08-19



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7690](https://issues.apache.org/jira/browse/CASSANDRA-7690) | build.xml - exclude \*.ps1 from fixcrlf |  Trivial | . | Robert Stupp | Robert Stupp |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7733](https://issues.apache.org/jira/browse/CASSANDRA-7733) | Fix supercolumn range deletion from Thrift (+ a few tests) |  Trivial | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-7742](https://issues.apache.org/jira/browse/CASSANDRA-7742) | Mutated ColumnFamily is not available to per-row indexes during update |  Major | . | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-7735](https://issues.apache.org/jira/browse/CASSANDRA-7735) | Remove ref-counting of netty buffers |  Critical | . | Benedict | T Jake Luciani |
| [CASSANDRA-7750](https://issues.apache.org/jira/browse/CASSANDRA-7750) | Do not flush on truncate if "durable\_writes" is false. |  Minor | . | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-7729](https://issues.apache.org/jira/browse/CASSANDRA-7729) | Describe keyspace error |  Major | . | Erin Moy | Tyler Hobbs |
| [CASSANDRA-7741](https://issues.apache.org/jira/browse/CASSANDRA-7741) | Handle zero index searchers in StorageProxy#estimateResultRowsPerRange() |  Major | . | Aleksey Yeschenko | Sam Tunnicliffe |
| [CASSANDRA-7770](https://issues.apache.org/jira/browse/CASSANDRA-7770) | NPE when clean up compaction leftover if table is already dropped |  Minor | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-7756](https://issues.apache.org/jira/browse/CASSANDRA-7756) | NullPointerException in getTotalBufferSize |  Major | Local Write-Read Paths | Leonid Shalupov | T Jake Luciani |
| [CASSANDRA-7743](https://issues.apache.org/jira/browse/CASSANDRA-7743) | Possible C\* OOM issue during long running test |  Major | . | Pierre Laporte | Benedict |


