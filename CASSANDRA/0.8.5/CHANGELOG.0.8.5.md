
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.8.5 - 2011-09-08



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2991](https://issues.apache.org/jira/browse/CASSANDRA-2991) | Add a 'load new sstables' JMX/nodetool command |  Major | Tools | Brandon Williams | Pavel Yaskevich |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3019](https://issues.apache.org/jira/browse/CASSANDRA-3019) | log the keyspace and CF of large rows being compacted |  Minor | . | Ryan King | Ryan King |
| [CASSANDRA-3027](https://issues.apache.org/jira/browse/CASSANDRA-3027) | [patch] use long math for long values |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-3030](https://issues.apache.org/jira/browse/CASSANDRA-3030) | [patch] remove dead allocation |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-3035](https://issues.apache.org/jira/browse/CASSANDRA-3035) | [patch] CommitLog.recover creates potentially many CRC32 objects. Just create one and reset it. |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-2941](https://issues.apache.org/jira/browse/CASSANDRA-2941) | Expose number of rpc timeouts for individual hosts metric via jmx |  Minor | . | Melvin Wang | Melvin Wang |
| [CASSANDRA-2742](https://issues.apache.org/jira/browse/CASSANDRA-2742) | Add CQL to the python package index |  Minor | CQL | Jonathan Ellis | paul cannon |
| [CASSANDRA-3061](https://issues.apache.org/jira/browse/CASSANDRA-3061) | Optionally skip log4j configuration |  Minor | . | amorton | T Jake Luciani |
| [CASSANDRA-3113](https://issues.apache.org/jira/browse/CASSANDRA-3113) | Bundle sstableloader with the Debian package |  Minor | . | Joaquin Casares | Sylvain Lebresne |
| [CASSANDRA-3122](https://issues.apache.org/jira/browse/CASSANDRA-3122) | SSTableSimpleUnsortedWriter take long time when inserting big rows |  Minor | . | Benoit Perroud | Sylvain Lebresne |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2662](https://issues.apache.org/jira/browse/CASSANDRA-2662) | Nodes get ignored by dynamic snitch when read repair chance is zero |  Trivial | . | Daniel Doubleday | Brandon Williams |
| [CASSANDRA-3007](https://issues.apache.org/jira/browse/CASSANDRA-3007) | NullPointerException in MessagingService.java:420 |  Minor | . | Viliam Holub | Jonathan Ellis |
| [CASSANDRA-2849](https://issues.apache.org/jira/browse/CASSANDRA-2849) | InvalidRequestException when validating column data includes entire column value |  Minor | . | David Allsopp | David Allsopp |
| [CASSANDRA-2950](https://issues.apache.org/jira/browse/CASSANDRA-2950) | Data from truncated CF reappears after server restart |  Major | . | Cathy Daw | Jonathan Ellis |
| [CASSANDRA-3021](https://issues.apache.org/jira/browse/CASSANDRA-3021) | Null pointer dereference of m in org.apache.cassandra.db.commitlog.CommitLogSegment.dirtyString() |  Minor | . | fantayeneh asres gizaw | fantayeneh asres gizaw |
| [CASSANDRA-2826](https://issues.apache.org/jira/browse/CASSANDRA-2826) | Debian Package for 0.8 is missing |  Minor | Packaging | Scott Mitchell | Sylvain Lebresne |
| [CASSANDRA-2952](https://issues.apache.org/jira/browse/CASSANDRA-2952) | cassandra.bat fails when CASSANDRA\_HOME contains a whitespace, again |  Minor | Packaging | Koji Ando | Koji Ando |
| [CASSANDRA-3022](https://issues.apache.org/jira/browse/CASSANDRA-3022) | Failures in cassandra long test: LongCompactionSpeedTest |  Major | . | Cathy Daw | Jonathan Ellis |
| [CASSANDRA-3039](https://issues.apache.org/jira/browse/CASSANDRA-3039) | AssertionError on nodetool cleanup |  Major | . | Ray Slakinski | Jonathan Ellis |
| [CASSANDRA-3043](https://issues.apache.org/jira/browse/CASSANDRA-3043) | SSTableImportTest fails on Windows because of malformed file path |  Trivial | Testing | Vladimir Loncar | Vladimir Loncar |
| [CASSANDRA-3036](https://issues.apache.org/jira/browse/CASSANDRA-3036) | Vague primary key references in CQL |  Minor | CQL | Kelley Reynolds | Pavel Yaskevich |
| [CASSANDRA-3044](https://issues.apache.org/jira/browse/CASSANDRA-3044) | Hector NodeAutoDiscoverService fails to resolve hosts due to / being part of the IP address |  Major | . | Aaron Turner | Brandon Williams |
| [CASSANDRA-3057](https://issues.apache.org/jira/browse/CASSANDRA-3057) | secondary index on a column that has a value of size \> 64k will fail on flush |  Minor | Secondary Indexes | Jackson Chung | Pavel Yaskevich |
| [CASSANDRA-3054](https://issues.apache.org/jira/browse/CASSANDRA-3054) | CLI "drop index" doesn't handle numeric-only hex column identifiers properly |  Trivial | Tools | Dan Kuebrich | Pavel Yaskevich |
| [CASSANDRA-3038](https://issues.apache.org/jira/browse/CASSANDRA-3038) | nodetool snapshot does not handle keyspace arguments correctly |  Minor | Tools | Nate McCall | Nate McCall |
| [CASSANDRA-3052](https://issues.apache.org/jira/browse/CASSANDRA-3052) | CQL: ResultSet.next() gives NPE when run after an INSERT or CREATE statement |  Major | . | Cathy Daw | Rick Shaw |
| [CASSANDRA-2708](https://issues.apache.org/jira/browse/CASSANDRA-2708) | memory leak in CompactionManager's estimatedCompactions |  Minor | . | Dan LaRocque | Dan LaRocque |
| [CASSANDRA-2881](https://issues.apache.org/jira/browse/CASSANDRA-2881) | RPM classpath evaluation include current directory (-cp:) |  Minor | Packaging | Wojciech Meler | Wojciech Meler |
| [CASSANDRA-3071](https://issues.apache.org/jira/browse/CASSANDRA-3071) | Gossip state is not removed after a new IP takes over a token |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-2868](https://issues.apache.org/jira/browse/CASSANDRA-2868) | Native Memory Leak |  Minor | . | Daniel Doubleday | Brandon Williams |
| [CASSANDRA-3059](https://issues.apache.org/jira/browse/CASSANDRA-3059) | sstable2json on an index sstable failed with NPE |  Minor | Tools | Jackson Chung | Jonathan Ellis |
| [CASSANDRA-3023](https://issues.apache.org/jira/browse/CASSANDRA-3023) | NPE in describe\_ring |  Major | . | Eric Falcao | Brandon Williams |
| [CASSANDRA-3074](https://issues.apache.org/jira/browse/CASSANDRA-3074) | comments and documentation for index\_interval are misleading |  Minor | . | Matthew F. Dennis | Matthew F. Dennis |
| [CASSANDRA-3082](https://issues.apache.org/jira/browse/CASSANDRA-3082) | Operation with CL=EACH\_QUORUM doesn't succeed when a replica is down (RF=3) |  Minor | . | Patricio Echague | Patricio Echague |
| [CASSANDRA-3075](https://issues.apache.org/jira/browse/CASSANDRA-3075) | Cassandra CLI unable to use list command with INTEGER column names, resulting in syntax error |  Minor | Tools | Renato Bacelar da Silveira | Pavel Yaskevich |
| [CASSANDRA-3099](https://issues.apache.org/jira/browse/CASSANDRA-3099) | Counters are not always hinted |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2857](https://issues.apache.org/jira/browse/CASSANDRA-2857) | initialize log4j correctly in EmbeddedCassandraService |  Trivial | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3084](https://issues.apache.org/jira/browse/CASSANDRA-3084) | o.a.c.dht.Range.differenceToFetch() doesn't handle all cases correctly |  Major | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-3066](https://issues.apache.org/jira/browse/CASSANDRA-3066) | Creating a keyspace SYSTEM cause issue |  Minor | . | Sébastien Giroux | Jonathan Ellis |
| [CASSANDRA-3102](https://issues.apache.org/jira/browse/CASSANDRA-3102) | catch invalid key\_validation\_class before instantiating UpdateColumnFamily |  Trivial | CQL | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3108](https://issues.apache.org/jira/browse/CASSANDRA-3108) | Make Range and Bounds objects client-safe |  Major | . | Jonathan Ellis | mck |
| [CASSANDRA-2433](https://issues.apache.org/jira/browse/CASSANDRA-2433) | Failed Streams Break Repair |  Major | . | Benjamin Coverston | Sylvain Lebresne |
| [CASSANDRA-3111](https://issues.apache.org/jira/browse/CASSANDRA-3111) | Bug in ReversedType comparator |  Minor | . | Fabien Rousseau |  |
| [CASSANDRA-3076](https://issues.apache.org/jira/browse/CASSANDRA-3076) | AssertionError in new GCInspector log |  Minor | . | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-3117](https://issues.apache.org/jira/browse/CASSANDRA-3117) | StorageServiceMBean is missing a getCompactionThroughputMbPerSec() method |  Minor | Tools | Eric Evans | Eric Evans |
| [CASSANDRA-3123](https://issues.apache.org/jira/browse/CASSANDRA-3123) | Don't try to build secondary indexes when there is none |  Trivial | Secondary Indexes | Sylvain Lebresne | Sylvain Lebresne |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2926](https://issues.apache.org/jira/browse/CASSANDRA-2926) | The JDBC Suite should provide a simple DatabaseMetaData implementation |  Minor | . | Rick Shaw | Rick Shaw |


