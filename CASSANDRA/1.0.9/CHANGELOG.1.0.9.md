
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.0.9 - 2012-04-06



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3980](https://issues.apache.org/jira/browse/CASSANDRA-3980) | Cli should be able to define CompositeType comparators |  Major | . | Brandon Williams | Pavel Yaskevich |
| [CASSANDRA-4063](https://issues.apache.org/jira/browse/CASSANDRA-4063) | Expose nodetool cfhistograms for secondary index CFs |  Minor | Secondary Indexes, Tools | Tyler Hobbs | Brandon Williams |
| [CASSANDRA-4023](https://issues.apache.org/jira/browse/CASSANDRA-4023) | Improve BloomFilter deserialization performance |  Minor | . | Joaquin Casares | Yuki Morishita |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3955](https://issues.apache.org/jira/browse/CASSANDRA-3955) | HintedHandoff won't compact a single sstable, resulting in repeated log messages |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3962](https://issues.apache.org/jira/browse/CASSANDRA-3962) | CassandraStorage can't cast fields from a CF correctly |  Major | . | Janne Jalkanen | Brandon Williams |
| [CASSANDRA-3973](https://issues.apache.org/jira/browse/CASSANDRA-3973) | Pig CounterColumnFamily support |  Major | . | Janne Jalkanen | Janne Jalkanen |
| [CASSANDRA-3975](https://issues.apache.org/jira/browse/CASSANDRA-3975) | Hints Should Be Dropped When Missing CFid Implies Deleted ColumnFamily |  Major | . | Chris Herron | Jonathan Ellis |
| [CASSANDRA-3684](https://issues.apache.org/jira/browse/CASSANDRA-3684) | Composite Column Support for PIG |  Major | . | Benjamin Coverston | Janne Jalkanen |
| [CASSANDRA-3986](https://issues.apache.org/jira/browse/CASSANDRA-3986) | Cli shouldn't call FBU.getBroadcastAddress needlessly |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3988](https://issues.apache.org/jira/browse/CASSANDRA-3988) | NullPointerException in org.apache.cassandra.service.AntiEntropyService when repair finds a keyspace with no CFs |  Minor | . | Bill Hathaway | Sylvain Lebresne |
| [CASSANDRA-4000](https://issues.apache.org/jira/browse/CASSANDRA-4000) | Division by zero on get\_slice |  Major | . | Sergey B | Sylvain Lebresne |
| [CASSANDRA-3957](https://issues.apache.org/jira/browse/CASSANDRA-3957) | Supercolumn serialization assertion failure |  Major | . | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-3989](https://issues.apache.org/jira/browse/CASSANDRA-3989) | nodetool cleanup/scrub/upgradesstables promotes all sstables to next level (LeveledCompaction) |  Minor | . | Maki Watanabe | Sylvain Lebresne |
| [CASSANDRA-4019](https://issues.apache.org/jira/browse/CASSANDRA-4019) | java.util.ConcurrentModificationException in Gossiper |  Minor | . | Thibaut | Brandon Williams |
| [CASSANDRA-4026](https://issues.apache.org/jira/browse/CASSANDRA-4026) | EC2 snitch incorrectly reports regions |  Major | . | Todd Nine | Vijay |
| [CASSANDRA-4044](https://issues.apache.org/jira/browse/CASSANDRA-4044) | examples/simple\_authenticator README doesn't mention JVM args |  Trivial | Documentation and Website | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-4070](https://issues.apache.org/jira/browse/CASSANDRA-4070) | CFS.setMaxCompactionThreshold doesn't allow 0 unless min is also 0 |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4086](https://issues.apache.org/jira/browse/CASSANDRA-4086) | decom should shut thrift down |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-4033](https://issues.apache.org/jira/browse/CASSANDRA-4033) | cqlsh: double wide unicode chars cause incorrect padding in select output |  Trivial | Tools | Tyler Patterson | paul cannon |
| [CASSANDRA-4003](https://issues.apache.org/jira/browse/CASSANDRA-4003) | cqlsh still failing to handle decode errors in some column names |  Minor | Tools | paul cannon | paul cannon |
| [CASSANDRA-3612](https://issues.apache.org/jira/browse/CASSANDRA-3612) | CQL inserting blank key. |  Minor | CQL | samal | paul cannon |
| [CASSANDRA-3755](https://issues.apache.org/jira/browse/CASSANDRA-3755) | NPE on invalid CQL DELETE command |  Minor | . | paul cannon | Dave Brosius |
| [CASSANDRA-4081](https://issues.apache.org/jira/browse/CASSANDRA-4081) | Issue with cassandra-cli "assume" command and custom types |  Major | . | Drew Kutcharian | Pavel Yaskevich |
| [CASSANDRA-4095](https://issues.apache.org/jira/browse/CASSANDRA-4095) | Internal error processing get\_slice (NullPointerException) |  Minor | . | John Laban | Jonathan Ellis |
| [CASSANDRA-4096](https://issues.apache.org/jira/browse/CASSANDRA-4096) | mlockall() returned code is ignored w/o assertions |  Minor | . | Peter Schuller | Jonathan Ellis |
| [CASSANDRA-4112](https://issues.apache.org/jira/browse/CASSANDRA-4112) | nodetool cleanup giving exception |  Major | . | Shoaib | Jonathan Ellis |
| [CASSANDRA-4099](https://issues.apache.org/jira/browse/CASSANDRA-4099) | IncomingTCPConnection recognizes from by doing socket.getInetAddress() instead of BroadCastAddress |  Minor | . | Vijay | Vijay |


