
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.7.4 - 2011-03-15



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1828](https://issues.apache.org/jira/browse/CASSANDRA-1828) | Create a pig storefunc |  Minor | . | Brandon Williams | Brandon Williams |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2219](https://issues.apache.org/jira/browse/CASSANDRA-2219) | modernize nodecmd option handling |  Minor | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-2160](https://issues.apache.org/jira/browse/CASSANDRA-2160) | Add "join" command to the nodetool |  Minor | . | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-2075](https://issues.apache.org/jira/browse/CASSANDRA-2075) | Eliminate excess comparator creation |  Minor | . | Stu Hood | Jonathan Ellis |
| [CASSANDRA-1559](https://issues.apache.org/jira/browse/CASSANDRA-1559) | make {SuperColumn,ColumnFamily}.addColumn() correct in the face of concurrent removals |  Trivial | . | Peter Schuller | Peter Schuller |
| [CASSANDRA-2271](https://issues.apache.org/jira/browse/CASSANDRA-2271) | Audit uses of BBU.clone |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2176](https://issues.apache.org/jira/browse/CASSANDRA-2176) | Add configuration setting to cap the number of Thrift connections |  Minor | CQL | Jonathan Ellis | T Jake Luciani |
| [CASSANDRA-2013](https://issues.apache.org/jira/browse/CASSANDRA-2013) | Add CL.TWO, CL.THREE; tweak CL documentation |  Minor | . | Peter Schuller | Peter Schuller |
| [CASSANDRA-2266](https://issues.apache.org/jira/browse/CASSANDRA-2266) | Add standard deviation to column names/values in stress.java |  Minor | . | Brandon Williams | Pavel Yaskevich |
| [CASSANDRA-2276](https://issues.apache.org/jira/browse/CASSANDRA-2276) | Pig memory issues with default LIMIT and large rows. |  Trivial | . | Matt Kennedy | Matt Kennedy |
| [CASSANDRA-2007](https://issues.apache.org/jira/browse/CASSANDRA-2007) | Move demo Keyspace1 definition from casandra.yaml to an input file for cassandra-cli |  Trivial | . | amorton | amorton |
| [CASSANDRA-2298](https://issues.apache.org/jira/browse/CASSANDRA-2298) | Allow running the build with test failures ignored |  Major | . | Stephen Connolly | Stephen Connolly |
| [CASSANDRA-2303](https://issues.apache.org/jira/browse/CASSANDRA-2303) | rename pig jar now that it is both input and output |  Trivial | . | Jeremy Hanna | Jeremy Hanna |
| [CASSANDRA-2295](https://issues.apache.org/jira/browse/CASSANDRA-2295) | memory pressure flusher should include secondary index CFs |  Minor | Secondary Indexes | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1872](https://issues.apache.org/jira/browse/CASSANDRA-1872) | Provide ability send notifications via JMX when an SSTable is written |  Minor | Tools | Nate McCall | Yang Yang |
| [CASSANDRA-2294](https://issues.apache.org/jira/browse/CASSANDRA-2294) | secondary index CFs should use parent CF flush thresholds |  Minor | Secondary Indexes | Jonathan Ellis | Jonathan Ellis |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2158](https://issues.apache.org/jira/browse/CASSANDRA-2158) | memtable\_throughput\_in\_mb can not support sizes over 2.2 gigs because of an integer overflow. |  Minor | . | Eddie | Jonathan Ellis |
| [CASSANDRA-2254](https://issues.apache.org/jira/browse/CASSANDRA-2254) | assert when using CL.EACH\_QUORUM |  Major | . | Mark Guzman | Mark Guzman |
| [CASSANDRA-2256](https://issues.apache.org/jira/browse/CASSANDRA-2256) | BRAF assertion error |  Minor | . | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-2244](https://issues.apache.org/jira/browse/CASSANDRA-2244) | secondary indexes aren't created on pre-existing or streamed data |  Major | Secondary Indexes | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-2228](https://issues.apache.org/jira/browse/CASSANDRA-2228) | Race conditions when reinitialisating nodes (OOM + Nullpointer) |  Major | . | Thibaut | Gary Dusbabek |
| [CASSANDRA-2270](https://issues.apache.org/jira/browse/CASSANDRA-2270) | nodetool info NPE when node isn't fully booted |  Minor | . | Sébastien Giroux | Brandon Williams |
| [CASSANDRA-2168](https://issues.apache.org/jira/browse/CASSANDRA-2168) | SSTable2Json tool returns a different key when a querying for a specific key in an SSTable that does not exist |  Minor | . | Benjamin Coverston | Benjamin Coverston |
| [CASSANDRA-2259](https://issues.apache.org/jira/browse/CASSANDRA-2259) | column values are only being validated in insert() |  Major | CQL | Eric Evans | Vivek Mishra |
| [CASSANDRA-2292](https://issues.apache.org/jira/browse/CASSANDRA-2292) | Connections are not reset if a node is restarted but we had not marked it down |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2296](https://issues.apache.org/jira/browse/CASSANDRA-2296) | Scrub resulting in "bloom filter claims to be longer than entire row size" error |  Major | Tools | Jason Harvey | Jonathan Ellis |
| [CASSANDRA-2282](https://issues.apache.org/jira/browse/CASSANDRA-2282) | ReadCallback AssertionError: resolver.getMessageCount() \<= endpoints.size() |  Major | . | Tyler Hobbs | Jonathan Ellis |
| [CASSANDRA-2285](https://issues.apache.org/jira/browse/CASSANDRA-2285) | Reading an empty commit log throw an exception |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2304](https://issues.apache.org/jira/browse/CASSANDRA-2304) | sstable2json dies with "Too many open files", regardless of ulimit |  Minor | Tools | Jason Harvey | Jonathan Ellis |
| [CASSANDRA-2310](https://issues.apache.org/jira/browse/CASSANDRA-2310) | CassandraStorage for pig checks for environment variable on mappers/reducers, but it should only need to be set on the machine launching pig. |  Minor | . | Eldon Stegall |  |
| [CASSANDRA-2301](https://issues.apache.org/jira/browse/CASSANDRA-2301) | OOM on repair with many inconsistent ranges |  Major | . | Joaquin Casares | Joaquin Casares |
| [CASSANDRA-1924](https://issues.apache.org/jira/browse/CASSANDRA-1924) | Broken keyspace strategy\_option with zero replicas |  Minor | . | Thor Carpenter | Sylvain Lebresne |
| [CASSANDRA-2312](https://issues.apache.org/jira/browse/CASSANDRA-2312) | Stress.java columns are bigger than advertised |  Minor | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2286](https://issues.apache.org/jira/browse/CASSANDRA-2286) | range queries don't respect snitch for local replicas |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2297](https://issues.apache.org/jira/browse/CASSANDRA-2297) | UnsupportedOperationException: Overflow in bytesPastMark(..) |  Major | . | Muga Nishizawa | Jonathan Ellis |
| [CASSANDRA-3598](https://issues.apache.org/jira/browse/CASSANDRA-3598) | Index Scan's will span across multiple DC's |  Minor | . | Vijay | Vijay |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2242](https://issues.apache.org/jira/browse/CASSANDRA-2242) | Improve BRAFTest |  Major | . | Jonathan Ellis | Pavel Yaskevich |


