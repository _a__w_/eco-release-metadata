
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.10 - 2017-02-03



### IMPORTANT ISSUES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12916](https://issues.apache.org/jira/browse/CASSANDRA-12916) | Broken UDT muitations loading from CommitLog |  Critical | Local Write-Read Paths | Sergey Dobrodey | Sam Tunnicliffe |


### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12016](https://issues.apache.org/jira/browse/CASSANDRA-12016) | Create MessagingService mocking classes |  Major | Testing | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-11031](https://issues.apache.org/jira/browse/CASSANDRA-11031) | Allow filtering on partition key columns for queries without secondary indexes |  Minor | CQL, Secondary Indexes | ZhaoYang | ZhaoYang |
| [CASSANDRA-12039](https://issues.apache.org/jira/browse/CASSANDRA-12039) | Add an index callback to be notified post bootstrap and before joining the ring |  Major | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-11550](https://issues.apache.org/jira/browse/CASSANDRA-11550) | Make the fanout size for LeveledCompactionStrategy to be configurable |  Major | Compaction | Dikang Gu | Dikang Gu |
| [CASSANDRA-12788](https://issues.apache.org/jira/browse/CASSANDRA-12788) | AbstractReplicationStrategy is not extendable from outside the package |  Major | . | Kurt Greaves | Kurt Greaves |
| [CASSANDRA-12967](https://issues.apache.org/jira/browse/CASSANDRA-12967) | Spec for Cassandra RPM Build |  Major | Packaging | Bhuvan Rawal | Michael Shuler |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12074](https://issues.apache.org/jira/browse/CASSANDRA-12074) | Faster check for open JMX port on startup |  Minor | Lifecycle | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-11880](https://issues.apache.org/jira/browse/CASSANDRA-11880) | Display number of tables in cfstats |  Minor | . | Geoffrey Yu | Geoffrey Yu |
| [CASSANDRA-12062](https://issues.apache.org/jira/browse/CASSANDRA-12062) | Prevent key invalidation if there's no key to invalidate |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-12076](https://issues.apache.org/jira/browse/CASSANDRA-12076) | Add username to AuthenticationException messages |  Trivial | . | Geoffrey Yu | Geoffrey Yu |
| [CASSANDRA-11970](https://issues.apache.org/jira/browse/CASSANDRA-11970) | Reuse DataOutputBuffer from ColumnIndex |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-8831](https://issues.apache.org/jira/browse/CASSANDRA-8831) | Create a system table to expose prepared statements |  Major | . | Sylvain Lebresne | Robert Stupp |
| [CASSANDRA-9613](https://issues.apache.org/jira/browse/CASSANDRA-9613) | Omit (de)serialization of state variable in UDAs |  Minor | CQL | Robert Stupp | Robert Stupp |
| [CASSANDRA-12150](https://issues.apache.org/jira/browse/CASSANDRA-12150) | cqlsh does not automatically downgrade CQL version |  Minor | Tools | Yusuke Takata | Yusuke Takata |
| [CASSANDRA-12153](https://issues.apache.org/jira/browse/CASSANDRA-12153) | RestrictionSet.hasIN() is slow |  Minor | CQL | Tyler Hobbs | Benjamin Lerer |
| [CASSANDRA-12178](https://issues.apache.org/jira/browse/CASSANDRA-12178) | Add prefixes to the name of snapshots created before a truncate or drop |  Minor | Observability | Geoffrey Yu | Geoffrey Yu |
| [CASSANDRA-10635](https://issues.apache.org/jira/browse/CASSANDRA-10635) | Add metrics for authentication failures |  Minor | . | Soumava Ghosh | Soumava Ghosh |
| [CASSANDRA-12035](https://issues.apache.org/jira/browse/CASSANDRA-12035) | Structure for tpstats output (JSON, YAML) |  Minor | Tools | Hiroyuki Nishi | Hiroyuki Nishi |
| [CASSANDRA-11424](https://issues.apache.org/jira/browse/CASSANDRA-11424) | Option to leave omitted columns in INSERT JSON unset |  Major | . | Ralf Steppacher | Oded Peer |
| [CASSANDRA-10202](https://issues.apache.org/jira/browse/CASSANDRA-10202) | simplify CommitLogSegmentManager |  Minor | Local Write-Read Paths | Jonathan Ellis | Branimir Lambov |
| [CASSANDRA-11738](https://issues.apache.org/jira/browse/CASSANDRA-11738) | Re-think the use of Severity in the DynamicEndpointSnitch calculation |  Minor | Core | Jeremiah Jordan | Jonathan Ellis |
| [CASSANDRA-12269](https://issues.apache.org/jira/browse/CASSANDRA-12269) | Faster write path |  Major | Local Write-Read Paths, Streaming and Messaging | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-12174](https://issues.apache.org/jira/browse/CASSANDRA-12174) | COPY FROM should raise error for non-existing input files |  Minor | Tools | Stefan Podkowinski | Hiroyuki Nishi |
| [CASSANDRA-10368](https://issues.apache.org/jira/browse/CASSANDRA-10368) | Support Restricting non-PK Cols in Materialized View Select Statements |  Minor | CQL, Materialized Views | Tyler Hobbs | Jochen Niebuhr |
| [CASSANDRA-7019](https://issues.apache.org/jira/browse/CASSANDRA-7019) | Improve tombstone compactions |  Major | Compaction | Marcus Eriksson | Branimir Lambov |
| [CASSANDRA-12258](https://issues.apache.org/jira/browse/CASSANDRA-12258) | Casandra stress version option |  Minor | Tools | Christopher Batey | Christopher Batey |
| [CASSANDRA-12343](https://issues.apache.org/jira/browse/CASSANDRA-12343) | Make 'static final boolean' easier to optimize for Hotspot |  Trivial | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-10707](https://issues.apache.org/jira/browse/CASSANDRA-10707) | Add support for Group By to Select statement |  Major | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-12342](https://issues.apache.org/jira/browse/CASSANDRA-12342) | CLibrary improvements |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-7384](https://issues.apache.org/jira/browse/CASSANDRA-7384) | Collect metrics on queries by consistency level |  Minor | . | Vishy Kasar | sankalp kohli |
| [CASSANDRA-12179](https://issues.apache.org/jira/browse/CASSANDRA-12179) | Allow updating DynamicEndpointSnitch properties via JMX |  Trivial | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-10643](https://issues.apache.org/jira/browse/CASSANDRA-10643) | Implement compaction for a specific token range |  Major | Compaction | Vishy Kasar | Vishy Kasar |
| [CASSANDRA-9876](https://issues.apache.org/jira/browse/CASSANDRA-9876) | One way targeted repair |  Minor | . | sankalp kohli | Geoffrey Yu |
| [CASSANDRA-12008](https://issues.apache.org/jira/browse/CASSANDRA-12008) | Make decommission operations resumable |  Minor | Streaming and Messaging | Tom van der Woerdt | Kaide Mu |
| [CASSANDRA-7190](https://issues.apache.org/jira/browse/CASSANDRA-7190) | Add schema to snapshot manifest |  Minor | Materialized Views, Tools | Jonathan Ellis | Alex Petrov |
| [CASSANDRA-12216](https://issues.apache.org/jira/browse/CASSANDRA-12216) | TTL Reading And Writing is Asymmetric |  Minor | CQL | Russell Spitzer | Russell Spitzer |
| [CASSANDRA-12256](https://issues.apache.org/jira/browse/CASSANDRA-12256) | Count entire coordinated request against timeout |  Major | Coordination | Sylvain Lebresne | Geoffrey Yu |
| [CASSANDRA-12403](https://issues.apache.org/jira/browse/CASSANDRA-12403) | Slow query detecting |  Major | Observability | Shogo Hoshii | Shogo Hoshii |
| [CASSANDRA-12425](https://issues.apache.org/jira/browse/CASSANDRA-12425) | Log at DEBUG when running unit tests |  Minor | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9054](https://issues.apache.org/jira/browse/CASSANDRA-9054) | Let DatabaseDescriptor not implicitly startup services |  Major | . | Jeremiah Jordan | Robert Stupp |
| [CASSANDRA-12311](https://issues.apache.org/jira/browse/CASSANDRA-12311) | Propagate TombstoneOverwhelmingException to the client |  Minor | Observability | Geoffrey Yu | Geoffrey Yu |
| [CASSANDRA-12474](https://issues.apache.org/jira/browse/CASSANDRA-12474) | Define executeLocally() at the ReadQuery Level |  Major | Local Write-Read Paths | Stefania | Stefania |
| [CASSANDRA-12385](https://issues.apache.org/jira/browse/CASSANDRA-12385) | Disk failure policy should not be invoked on out of space |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-9875](https://issues.apache.org/jira/browse/CASSANDRA-9875) | Rebuild from targeted replica |  Minor | . | sankalp kohli | Geoffrey Yu |
| [CASSANDRA-11914](https://issues.apache.org/jira/browse/CASSANDRA-11914) | Provide option for cassandra-stress to dump all settings |  Minor | Tools | Ben Slater | Ben Slater |
| [CASSANDRA-12486](https://issues.apache.org/jira/browse/CASSANDRA-12486) | Structure for compactionhistory output (JSON, YAML) |  Minor | Tools | Masataka Yamaguchi | Masataka Yamaguchi |
| [CASSANDRA-12550](https://issues.apache.org/jira/browse/CASSANDRA-12550) | Replace Config.setClientMode with DatabaseDescriptor.clientInit() |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-12583](https://issues.apache.org/jira/browse/CASSANDRA-12583) | Code Analysis got a problem |  Minor | Local Write-Read Paths | Edward Capriolo | Edward Capriolo |
| [CASSANDRA-12536](https://issues.apache.org/jira/browse/CASSANDRA-12536) | Update jmh-core/jmh-generator-annprocess to quell RELEASE\_6 warnings |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-12586](https://issues.apache.org/jira/browse/CASSANDRA-12586) | JMH benchmark improvements |  Minor | Tools | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-12585](https://issues.apache.org/jira/browse/CASSANDRA-12585) | Fixup Cassandra Stress reporting thread model and precision |  Minor | Tools | Nitsan Wakart | Nitsan Wakart |
| [CASSANDRA-12647](https://issues.apache.org/jira/browse/CASSANDRA-12647) | Improve vnode allocation on RandomPartitioner |  Minor | . | Dikang Gu | Dikang Gu |
| [CASSANDRA-9318](https://issues.apache.org/jira/browse/CASSANDRA-9318) | Bound the number of in-flight requests at the coordinator |  Major | Local Write-Read Paths, Streaming and Messaging | Ariel Weisberg | Sergio Bossa |
| [CASSANDRA-12532](https://issues.apache.org/jira/browse/CASSANDRA-12532) | Include repair id in repair start message |  Major | Streaming and Messaging | Chris Lohfink | Chris Lohfink |
| [CASSANDRA-12232](https://issues.apache.org/jira/browse/CASSANDRA-12232) | Add +=/-= shortcut syntax |  Minor | CQL | Sylvain Lebresne | Alex Petrov |
| [CASSANDRA-12248](https://issues.apache.org/jira/browse/CASSANDRA-12248) | Allow tuning compaction thread count at runtime |  Minor | . | Tom van der Woerdt | Dikang Gu |
| [CASSANDRA-12089](https://issues.apache.org/jira/browse/CASSANDRA-12089) | Update metrics-reporter dependencies |  Minor | . | Robert Stupp | Michał Matłoka |
| [CASSANDRA-12731](https://issues.apache.org/jira/browse/CASSANDRA-12731) | Remove IndexInfo cache from FileIndexInfoRetriever. |  Major | Local Write-Read Paths | Yasuharu Goto | Yasuharu Goto |
| [CASSANDRA-12693](https://issues.apache.org/jira/browse/CASSANDRA-12693) | Add the JMX metrics about the total number of hints we have delivered |  Minor | . | Dikang Gu | Dikang Gu |
| [CASSANDRA-12461](https://issues.apache.org/jira/browse/CASSANDRA-12461) | Add hooks to StorageService shutdown |  Major | Lifecycle | Anthony Cozzie | Anthony Cozzie |
| [CASSANDRA-12199](https://issues.apache.org/jira/browse/CASSANDRA-12199) | Config class uses boxed types but DD exposes primitive types |  Minor | Configuration | Robert Stupp | Robert Stupp |
| [CASSANDRA-12733](https://issues.apache.org/jira/browse/CASSANDRA-12733) | Throw an exception if there is a prepared statement id hash conflict. |  Minor | CQL | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-12268](https://issues.apache.org/jira/browse/CASSANDRA-12268) | Make MV Index creation robust for wide referent rows |  Major | Core, Materialized Views | Jonathan Shook | Carl Yeksigian |
| [CASSANDRA-12646](https://issues.apache.org/jira/browse/CASSANDRA-12646) | nodetool stopdaemon errors out on stopdaemon |  Minor | Tools | Robert Stupp | Robert Stupp |
| [CASSANDRA-11873](https://issues.apache.org/jira/browse/CASSANDRA-11873) | Add duration type |  Major | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-12790](https://issues.apache.org/jira/browse/CASSANDRA-12790) | Support InfluxDb metrics reporter config |  Minor | Configuration, Packaging | Achmad Nasirudin Sandi | Benjamin Lerer |
| [CASSANDRA-12384](https://issues.apache.org/jira/browse/CASSANDRA-12384) | Include info about sstable on "Compacting large row” message |  Trivial | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-12490](https://issues.apache.org/jira/browse/CASSANDRA-12490) | Add sequence distribution type to cassandra stress |  Minor | Tools | Ben Slater | Ben Slater |
| [CASSANDRA-12777](https://issues.apache.org/jira/browse/CASSANDRA-12777) | Optimize the vnode allocation for single replica per DC |  Major | . | Dikang Gu | Dikang Gu |
| [CASSANDRA-12836](https://issues.apache.org/jira/browse/CASSANDRA-12836) | Set JOINING mode before executing the pre-join index callback |  Major | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-12889](https://issues.apache.org/jira/browse/CASSANDRA-12889) | Pass root cause to CorruptBlockException when uncompression failed |  Trivial | Local Write-Read Paths | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-12897](https://issues.apache.org/jira/browse/CASSANDRA-12897) | Move cqlsh syntax rules into separate module and allow easier customization |  Minor | Tools | Eduard Tudenhoefner | Eduard Tudenhoefner |
| [CASSANDRA-12989](https://issues.apache.org/jira/browse/CASSANDRA-12989) | Correct function doc to clear up statement isolation |  Major | Documentation and Website | Edward Capriolo | Edward Capriolo |
| [CASSANDRA-12186](https://issues.apache.org/jira/browse/CASSANDRA-12186) | anticompaction log message doesn't include the parent repair session id |  Minor | Observability | Wei Deng | Tommy Stendahl |
| [CASSANDRA-12883](https://issues.apache.org/jira/browse/CASSANDRA-12883) | Remove support for non-JavaScript UDFs |  Minor | CQL | Robert Stupp | Robert Stupp |
| [CASSANDRA-12981](https://issues.apache.org/jira/browse/CASSANDRA-12981) | Refactor ColumnCondition |  Major | CQL | Benjamin Lerer | Benjamin Lerer |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12292](https://issues.apache.org/jira/browse/CASSANDRA-12292) | Wrong buffer size after CASSANDRA-11580 |  Trivial | Local Write-Read Paths | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-12004](https://issues.apache.org/jira/browse/CASSANDRA-12004) | Inconsistent timezone in logs |  Trivial | . | Jérôme Mainaud |  |
| [CASSANDRA-12133](https://issues.apache.org/jira/browse/CASSANDRA-12133) | Failed to load Java8 implementation ohc-core-j8 |  Trivial | Core | Mike | Robert Stupp |
| [CASSANDRA-12228](https://issues.apache.org/jira/browse/CASSANDRA-12228) | Write performance regression in 3.x vs 3.0 |  Minor | . | T Jake Luciani | Ariel Weisberg |
| [CASSANDRA-11960](https://issues.apache.org/jira/browse/CASSANDRA-11960) | Hints are not seekable |  Major | . | Robert Stupp | Stefan Podkowinski |
| [CASSANDRA-12358](https://issues.apache.org/jira/browse/CASSANDRA-12358) | Slow PostFlush execution due to 2i flushing can cause near OOM to OOM |  Major | Core | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-12366](https://issues.apache.org/jira/browse/CASSANDRA-12366) | Fix compaction throttle |  Major | Compaction | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-12223](https://issues.apache.org/jira/browse/CASSANDRA-12223) | SASI Indexes querying incorrectly return 0 rows |  Major | sasi | Qiu Zhida | Alex Petrov |
| [CASSANDRA-12378](https://issues.apache.org/jira/browse/CASSANDRA-12378) | Creating SASI index on clustering column in presence of static column breaks writes |  Critical | sasi | Alex Petrov | Alex Petrov |
| [CASSANDRA-12374](https://issues.apache.org/jira/browse/CASSANDRA-12374) | Can't rebuild SASI index |  Major | . | Alex Petrov | Alex Petrov |
| [CASSANDRA-12154](https://issues.apache.org/jira/browse/CASSANDRA-12154) | "SELECT \* FROM foo LIMIT ;" does not error out |  Minor | CQL | Robert Stupp | Thomas Boucher |
| [CASSANDRA-12192](https://issues.apache.org/jira/browse/CASSANDRA-12192) | Retry all internode messages once after reopening connections |  Major | Core | Sean McCarthy | Tyler Hobbs |
| [CASSANDRA-12476](https://issues.apache.org/jira/browse/CASSANDRA-12476) | SyntaxException when COPY FROM Counter Table with Null value |  Minor | Tools | Ashraful Islam | Stefania |
| [CASSANDRA-12504](https://issues.apache.org/jira/browse/CASSANDRA-12504) | BatchlogManager is shut down twice during drain |  Minor | Lifecycle | Alex Petrov | Stefania |
| [CASSANDRA-12528](https://issues.apache.org/jira/browse/CASSANDRA-12528) | Fix eclipse-warning problems |  Major | Core | Joel Knighton | Sam Tunnicliffe |
| [CASSANDRA-12508](https://issues.apache.org/jira/browse/CASSANDRA-12508) | nodetool repair returns status code 0 for some errors |  Major | Tools | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-11594](https://issues.apache.org/jira/browse/CASSANDRA-11594) | Too many open files on directories |  Critical | Core | n0rad | Stefania |
| [CASSANDRA-11706](https://issues.apache.org/jira/browse/CASSANDRA-11706) | Tracing payload not passed through newSession(..) |  Minor | . | mck | mck |
| [CASSANDRA-11889](https://issues.apache.org/jira/browse/CASSANDRA-11889) | LogRecord: file system race condition may cause verify() to fail |  Major | Local Write-Read Paths | Stefania | Stefania |
| [CASSANDRA-12261](https://issues.apache.org/jira/browse/CASSANDRA-12261) | dtest failure in write\_failures\_test.TestWriteFailures.test\_thrift |  Major | Local Write-Read Paths | Philip Thompson | Stefania |
| [CASSANDRA-12279](https://issues.apache.org/jira/browse/CASSANDRA-12279) | nodetool repair hangs on non-existant table |  Minor | . | Benjamin Roth | Masataka Yamaguchi |
| [CASSANDRA-12522](https://issues.apache.org/jira/browse/CASSANDRA-12522) | nodetool repair -pr and -local option rejected |  Major | Tools | Jérôme Mainaud | Jérôme Mainaud |
| [CASSANDRA-12208](https://issues.apache.org/jira/browse/CASSANDRA-12208) | Estimated droppable tombstones given by sstablemetadata counts tombstones that aren't actually "droppable" |  Minor | Tools | Thanh | Marcus Eriksson |
| [CASSANDRA-8523](https://issues.apache.org/jira/browse/CASSANDRA-8523) | Writes should be sent to a replacement node which has a new IP while it is streaming in data |  Major | . | Richard Wagner | Paulo Motta |
| [CASSANDRA-12564](https://issues.apache.org/jira/browse/CASSANDRA-12564) | Stress daemon mode no longer works |  Minor | Tools | Christopher Batey | Christopher Batey |
| [CASSANDRA-12565](https://issues.apache.org/jira/browse/CASSANDRA-12565) | Node may not start when upgrading from 2.2 to 3.0 if drain is not run |  Major | Local Write-Read Paths | Stefania | Stefania |
| [CASSANDRA-12481](https://issues.apache.org/jira/browse/CASSANDRA-12481) | dtest failure in cqlshlib.test.test\_cqlsh\_output.TestCqlshOutput.test\_describe\_keyspace\_output |  Major | Testing | Craig Kodman | Stefania |
| [CASSANDRA-12527](https://issues.apache.org/jira/browse/CASSANDRA-12527) | Stack Overflow returned to queries while upgrading |  Major | Local Write-Read Paths | Steve Severance | Sylvain Lebresne |
| [CASSANDRA-11126](https://issues.apache.org/jira/browse/CASSANDRA-11126) | select\_distinct\_with\_deletions\_test failing on non-vnode environments |  Major | . | Ryan McGuire | Sylvain Lebresne |
| [CASSANDRA-11332](https://issues.apache.org/jira/browse/CASSANDRA-11332) | nodes connect to themselves when NTS is used |  Major | Core | Brandon Williams | Branimir Lambov |
| [CASSANDRA-12413](https://issues.apache.org/jira/browse/CASSANDRA-12413) | CompactionsCQLTest.testTriggerMinorCompactionDTCS fails |  Major | Testing | Joshua McKenzie | Marcus Eriksson |
| [CASSANDRA-12473](https://issues.apache.org/jira/browse/CASSANDRA-12473) | Errors in cassandra-stress print settings output |  Minor | Tools | Ben Slater | Ben Slater |
| [CASSANDRA-12609](https://issues.apache.org/jira/browse/CASSANDRA-12609) | sstableloader NPE when partial directory path is given |  Minor | Tools | Hannu Kröger | Hannu Kröger |
| [CASSANDRA-12423](https://issues.apache.org/jira/browse/CASSANDRA-12423) | Cells missing from compact storage table after upgrading from 2.1.9 to 3.7 |  Major | Local Write-Read Paths | Tomasz Grabiec | Stefania |
| [CASSANDRA-12551](https://issues.apache.org/jira/browse/CASSANDRA-12551) | Fix CQLSSTableWriter compatibility changes from CASSANDRA-11844 |  Blocker | . | T Jake Luciani | Jeremiah Jordan |
| [CASSANDRA-12237](https://issues.apache.org/jira/browse/CASSANDRA-12237) | Cassandra stress graphing is broken |  Major | Tools | Christopher Batey | Christopher Batey |
| [CASSANDRA-12516](https://issues.apache.org/jira/browse/CASSANDRA-12516) | Interned column identifiers can be overridden incorrectly |  Major | Local Write-Read Paths | Aleksey Yeschenko | Stefania |
| [CASSANDRA-11363](https://issues.apache.org/jira/browse/CASSANDRA-11363) | High Blocked NTR When Connecting |  Major | Coordination | Russell Bradberry | T Jake Luciani |
| [CASSANDRA-12667](https://issues.apache.org/jira/browse/CASSANDRA-12667) | CQLSSTableWriter can fail because DiskOptimizationStrategy is null |  Major | Tools | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-12499](https://issues.apache.org/jira/browse/CASSANDRA-12499) | Row cache does not cache partitions on tables without clustering keys |  Major | . | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-12060](https://issues.apache.org/jira/browse/CASSANDRA-12060) | Establish consistent distinction between non-existing partition and NULL value for LWTs on static columns |  Major | . | Alex Petrov | Alex Petrov |
| [CASSANDRA-12554](https://issues.apache.org/jira/browse/CASSANDRA-12554) | updateJobs in PendingRangeCalculatorService should be decremented in finally block |  Minor | Distributed Metadata | sankalp kohli | sankalp kohli |
| [CASSANDRA-11670](https://issues.apache.org/jira/browse/CASSANDRA-11670) | Rebuilding or streaming MV generates mutations larger than max\_mutation\_size\_in\_kb |  Major | Configuration, Materialized Views, Streaming and Messaging | Anastasia Osintseva | Paulo Motta |
| [CASSANDRA-12642](https://issues.apache.org/jira/browse/CASSANDRA-12642) | cqlsh NoHostsAvailable/AuthenticationFailure when sourcing a file with COPY commands |  Minor | Tools | Adam Holmberg | Stefania |
| [CASSANDRA-12253](https://issues.apache.org/jira/browse/CASSANDRA-12253) | Fix exceptions when enabling gossip on proxy nodes. |  Minor | Distributed Metadata | Dikang Gu | Dikang Gu |
| [CASSANDRA-12688](https://issues.apache.org/jira/browse/CASSANDRA-12688) | Change -ea comment in jvm.options |  Minor | . | Edward Capriolo | Edward Capriolo |
| [CASSANDRA-12703](https://issues.apache.org/jira/browse/CASSANDRA-12703) | Compaction does not purge tombstones when only\_purge\_repaired\_tombstones is set |  Major | Compaction | Sharvanath Pathak | Sharvanath Pathak |
| [CASSANDRA-12618](https://issues.apache.org/jira/browse/CASSANDRA-12618) | Out of memory bug with one insert |  Critical | . | Eduardo Alonso de Blas | Benjamin Lerer |
| [CASSANDRA-12605](https://issues.apache.org/jira/browse/CASSANDRA-12605) | Timestamp-order searching of sstables does not handle non-frozen UDTs, frozen collections correctly |  Major | Local Write-Read Paths | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-12632](https://issues.apache.org/jira/browse/CASSANDRA-12632) | Failure in LogTransactionTest.testUnparsableFirstRecord-compression |  Major | Testing | Joel Knighton | Stefania |
| [CASSANDRA-12697](https://issues.apache.org/jira/browse/CASSANDRA-12697) | cdc column addition still breaks schema migration tasks |  Major | Distributed Metadata | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-12580](https://issues.apache.org/jira/browse/CASSANDRA-12580) | Fix merkle tree size calculation |  Major | Streaming and Messaging | Paulo Motta | Paulo Motta |
| [CASSANDRA-12677](https://issues.apache.org/jira/browse/CASSANDRA-12677) | Failure in DatabaseDescriptorRefTest.testDatabaseDescriptorRef |  Major | Testing | Philip Thompson | Yuki Morishita |
| [CASSANDRA-12706](https://issues.apache.org/jira/browse/CASSANDRA-12706) | Exception supposedly after ttl expires and compaction occurs |  Critical | Local Write-Read Paths | Nikhil Sharma | Stefania |
| [CASSANDRA-12509](https://issues.apache.org/jira/browse/CASSANDRA-12509) | Shutdown process triggered twice during if the node is drained |  Major | Lifecycle | Alex Petrov | Alex Petrov |
| [CASSANDRA-12502](https://issues.apache.org/jira/browse/CASSANDRA-12502) | ColumnIndex does not reuse buffer |  Minor | Local Write-Read Paths | Branimir Lambov | Robert Stupp |
| [CASSANDRA-12717](https://issues.apache.org/jira/browse/CASSANDRA-12717) | IllegalArgumentException in CompactionTask |  Major | Local Write-Read Paths | Yasuharu Goto | Yasuharu Goto |
| [CASSANDRA-12715](https://issues.apache.org/jira/browse/CASSANDRA-12715) | Fix exceptions with the new vnode allocation. |  Major | . | Dikang Gu | Dikang Gu |
| [CASSANDRA-12478](https://issues.apache.org/jira/browse/CASSANDRA-12478) | cassandra stress still uses CFMetaData.compile() |  Major | Tools | Denis Ranger | Denis Ranger |
| [CASSANDRA-12450](https://issues.apache.org/jira/browse/CASSANDRA-12450) | CQLSSTableWriter does not allow Update statement |  Major | Tools | Kuku1 | Alex Petrov |
| [CASSANDRA-12457](https://issues.apache.org/jira/browse/CASSANDRA-12457) | dtest failure in upgrade\_tests.cql\_tests.TestCQLNodes2RF1\_Upgrade\_current\_2\_1\_x\_To\_indev\_2\_2\_x.bug\_5732\_test |  Major | Lifecycle | Craig Kodman | Stefania |
| [CASSANDRA-12552](https://issues.apache.org/jira/browse/CASSANDRA-12552) | CompressedRandomAccessReaderTest.testDataCorruptionDetection fails sporadically |  Minor | . | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-12582](https://issues.apache.org/jira/browse/CASSANDRA-12582) | Removing static column results in ReadFailure due to CorruptSSTableException |  Critical | Local Write-Read Paths | Evan Prothro | Stefania |
| [CASSANDRA-12729](https://issues.apache.org/jira/browse/CASSANDRA-12729) | Cassandra-Stress: Use single seed in UUID generation |  Minor | Tools | Chris Splinter | Chris Splinter |
| [CASSANDRA-12759](https://issues.apache.org/jira/browse/CASSANDRA-12759) | cassandra-stress shows the incorrect JMX port in settings output |  Trivial | Tools | Guy Bolton King | Guy Bolton King |
| [CASSANDRA-12740](https://issues.apache.org/jira/browse/CASSANDRA-12740) | cqlsh copy tests hang in case of no answer from the server or driver |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-12598](https://issues.apache.org/jira/browse/CASSANDRA-12598) | BailErrorStragery alike for ANTLR grammar parsing |  Major | CQL | Berenguer Blasi | Berenguer Blasi |
| [CASSANDRA-12700](https://issues.apache.org/jira/browse/CASSANDRA-12700) | During writing data into Cassandra 3.7.0 using Python driver 3.7 sometimes Connection get lost, because of Server NullPointerException |  Major | Core | Rajesh Radhakrishnan | Jeff Jirsa |
| [CASSANDRA-12274](https://issues.apache.org/jira/browse/CASSANDRA-12274) | mx4j does not work in 3.0.8 |  Major | Core | Ilya | Robert Stupp |
| [CASSANDRA-11534](https://issues.apache.org/jira/browse/CASSANDRA-11534) | cqlsh fails to format collections when using aliases |  Minor | Tools | Robert Stupp | Stefania |
| [CASSANDRA-11117](https://issues.apache.org/jira/browse/CASSANDRA-11117) | ColUpdateTimeDeltaHistogram histogram overflow |  Minor | Observability | Chris Lohfink | Joel Knighton |
| [CASSANDRA-12720](https://issues.apache.org/jira/browse/CASSANDRA-12720) | Permissions on aggregate functions are not removed on drop |  Major | Distributed Metadata | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-12761](https://issues.apache.org/jira/browse/CASSANDRA-12761) | Make cassandra.yaml docs for batch\_size\_\*\_threshold\_in\_kb reflect changes in CASSANDRA-10876 |  Trivial | Configuration | Guy Bolton King | Guy Bolton King |
| [CASSANDRA-12417](https://issues.apache.org/jira/browse/CASSANDRA-12417) | Built-in AVG aggregate is much less useful than it should be |  Major | CQL | Branimir Lambov | Alex Petrov |
| [CASSANDRA-12784](https://issues.apache.org/jira/browse/CASSANDRA-12784) | ReplicationAwareTokenAllocatorTest times out almost every time for 3.X and trunk |  Major | Testing | Stefania | Stefania |
| [CASSANDRA-12626](https://issues.apache.org/jira/browse/CASSANDRA-12626) | LWT contention appears to be under reported |  Minor | Observability | Christopher Batey | Christopher Batey |
| [CASSANDRA-12776](https://issues.apache.org/jira/browse/CASSANDRA-12776) | when memtable flush Statistics thisOffHeap error |  Trivial | . | scott.zhai | Kurt Greaves |
| [CASSANDRA-12789](https://issues.apache.org/jira/browse/CASSANDRA-12789) | ViewTest.testCompaction releases reference twice |  Major | Testing | Stefania | Stefania |
| [CASSANDRA-11803](https://issues.apache.org/jira/browse/CASSANDRA-11803) | Creating a materialized view on a table with "token" column breaks the cluster |  Major | CQL, Materialized Views | Victor Trac | Carl Yeksigian |
| [CASSANDRA-12812](https://issues.apache.org/jira/browse/CASSANDRA-12812) | testall failure in org.apache.cassandra.dht.tokenallocator.RandomReplicationAwareTokenAllocatorTest.testExistingCluster |  Major | Testing | Sean McCarthy | Stefania |
| [CASSANDRA-12765](https://issues.apache.org/jira/browse/CASSANDRA-12765) | SSTable ignored incorrectly with partition level tombstone |  Major | Local Write-Read Paths | Cameron Zemek | Cameron Zemek |
| [CASSANDRA-12786](https://issues.apache.org/jira/browse/CASSANDRA-12786) | Fix a bug in CASSANDRA-11005(Split consisten range movement flag) |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-12754](https://issues.apache.org/jira/browse/CASSANDRA-12754) | Change cassandra.wait\_for\_tracing\_events\_timeout\_secs default to -1 so C\* doesn't wait on trace events to be written before responding to request by default |  Major | Observability | Andy Tolbert | Stefania |
| [CASSANDRA-12801](https://issues.apache.org/jira/browse/CASSANDRA-12801) | KeyCacheCqlTest fails on 3.0, 3.X and trunk |  Major | Testing | Stefania | Stefania |
| [CASSANDRA-12815](https://issues.apache.org/jira/browse/CASSANDRA-12815) | cqlsh may throw AttributeError due to no table metadata |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-12466](https://issues.apache.org/jira/browse/CASSANDRA-12466) | Use different build directories for Eclipse and Ant |  Trivial | . | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-12803](https://issues.apache.org/jira/browse/CASSANDRA-12803) | PER PARTITION LIMIT not recognized by cqlsh auto completion |  Trivial | . | Robert Stupp | Benjamin Lerer |
| [CASSANDRA-12149](https://issues.apache.org/jira/browse/CASSANDRA-12149) | NullPointerException on SELECT using index with token restrictions fully overriden by other PK restrictions |  Major | . | Andrey Konstantinov | Alex Petrov |
| [CASSANDRA-12454](https://issues.apache.org/jira/browse/CASSANDRA-12454) | Unable to start on IPv6-only node with local JMX |  Major | . | Vadim Tsesko | Sam Tunnicliffe |
| [CASSANDRA-12845](https://issues.apache.org/jira/browse/CASSANDRA-12845) | net.mintern.primitive library has GPL license |  Major | . | David Pennell | Robert Stupp |
| [CASSANDRA-12689](https://issues.apache.org/jira/browse/CASSANDRA-12689) | All MutationStage threads blocked, kills server |  Critical | Local Write-Read Paths, Materialized Views | Benjamin Roth | Benjamin Roth |
| [CASSANDRA-12869](https://issues.apache.org/jira/browse/CASSANDRA-12869) | Don't load mx4j beans twice |  Major | Core | Eduard Tudenhoefner | Eduard Tudenhoefner |
| [CASSANDRA-12791](https://issues.apache.org/jira/browse/CASSANDRA-12791) | MessageIn logic to determine if the message is cross-node is wrong |  Minor | Observability, Streaming and Messaging | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-12854](https://issues.apache.org/jira/browse/CASSANDRA-12854) | CommitLogTest.testDeleteIfNotDirty failed in 3.X |  Major | Testing | Stefania | Stefania |
| [CASSANDRA-12296](https://issues.apache.org/jira/browse/CASSANDRA-12296) | Better error message when streaming with insufficient sources in DC |  Minor | . | Jim Witschey | Kurt Greaves |
| [CASSANDRA-12867](https://issues.apache.org/jira/browse/CASSANDRA-12867) | Batch with multiple conditional updates for the same partition causes AssertionError |  Critical | CQL | Kurt Greaves | Sylvain Lebresne |
| [CASSANDRA-12695](https://issues.apache.org/jira/browse/CASSANDRA-12695) | Truncate ALWAYS not applied |  Trivial | . | Alwyn Davis | Alwyn Davis |
| [CASSANDRA-12462](https://issues.apache.org/jira/browse/CASSANDRA-12462) | NullPointerException in CompactionInfo.getId(CompactionInfo.java:65) |  Major | Compaction | Jonathan DePrizio | Simon Zhou |
| [CASSANDRA-12808](https://issues.apache.org/jira/browse/CASSANDRA-12808) | testall failure inorg.apache.cassandra.io.sstable.IndexSummaryManagerTest.testCancelIndex |  Major | . | Sean McCarthy | Sam Tunnicliffe |
| [CASSANDRA-12834](https://issues.apache.org/jira/browse/CASSANDRA-12834) | testall failure in org.apache.cassandra.index.internal.CassandraIndexTest.indexOnFirstClusteringColumn |  Major | . | Sean McCarthy | Sylvain Lebresne |
| [CASSANDRA-12863](https://issues.apache.org/jira/browse/CASSANDRA-12863) | cqlsh COPY FROM cannot parse timestamp in partition key if table contains a counter value |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-12813](https://issues.apache.org/jira/browse/CASSANDRA-12813) | NPE in auth for bootstrapping node |  Major | . | Charles Mims | Alex Petrov |
| [CASSANDRA-12535](https://issues.apache.org/jira/browse/CASSANDRA-12535) | Prevent reloading of logback.xml from UDF sandbox |  Minor | . | Pat Patterson | Robert Stupp |
| [CASSANDRA-12861](https://issues.apache.org/jira/browse/CASSANDRA-12861) | example/triggers build fail. |  Trivial | . | Yasuharu Goto | Sylvain Lebresne |
| [CASSANDRA-12911](https://issues.apache.org/jira/browse/CASSANDRA-12911) | cassandra.yaml mentions wrong table names for PasswordAuthenticator/CassandraAuthorizer |  Trivial | Core | Eduard Tudenhoefner | Eduard Tudenhoefner |
| [CASSANDRA-12858](https://issues.apache.org/jira/browse/CASSANDRA-12858) | testall failure in org.apache.cassandra.dht.Murmur3PartitionerTest.testSplitWrapping-compression |  Major | . | Sean McCarthy | Dikang Gu |
| [CASSANDRA-12903](https://issues.apache.org/jira/browse/CASSANDRA-12903) | internode\_encryption + bootstrapping a node fails due to calling an unsupported method on an SSL Socket |  Blocker | Streaming and Messaging | Eduard Tudenhoefner | Paulo Motta |
| [CASSANDRA-12901](https://issues.apache.org/jira/browse/CASSANDRA-12901) | Repair can hang if node dies during sync or anticompaction |  Major | Streaming and Messaging | Paulo Motta | Paulo Motta |
| [CASSANDRA-12792](https://issues.apache.org/jira/browse/CASSANDRA-12792) | delete with timestamp long.MAX\_VALUE for the whole key creates tombstone that cannot be removed. |  Major | Compaction | Ian Ilsley | Joel Knighton |
| [CASSANDRA-12281](https://issues.apache.org/jira/browse/CASSANDRA-12281) | Gossip blocks on startup when there are pending range movements |  Major | Core | Eric Evans | Stefan Podkowinski |
| [CASSANDRA-12934](https://issues.apache.org/jira/browse/CASSANDRA-12934) | AnticompactionRequestSerializer serializedSize is incorrect |  Major | Core | Jason Brown | Jason Brown |
| [CASSANDRA-12919](https://issues.apache.org/jira/browse/CASSANDRA-12919) | Fix inconsistencies in cassandra-stress load balancing policy |  Minor | Tools | Stefania | Stefania |
| [CASSANDRA-12945](https://issues.apache.org/jira/browse/CASSANDRA-12945) | Resolve unit testing without JCE security libraries installed |  Major | Core | Jason Brown | Stefan Podkowinski |
| [CASSANDRA-12875](https://issues.apache.org/jira/browse/CASSANDRA-12875) | testall failure in org.apache.cassandra.net.MessagingServiceTest.testDCLatency-compression |  Major | . | Sean McCarthy | Chris Lohfink |
| [CASSANDRA-12899](https://issues.apache.org/jira/browse/CASSANDRA-12899) | stand alone sstableupgrade prints LEAK DETECTED warnings some times |  Major | . | Jeremiah Jordan | Yuki Morishita |
| [CASSANDRA-12651](https://issues.apache.org/jira/browse/CASSANDRA-12651) | Failure in SecondaryIndexTest.testAllowFilteringOnPartitionKeyWithSecondaryIndex |  Critical | Testing | Joel Knighton | Sam Tunnicliffe |
| [CASSANDRA-12590](https://issues.apache.org/jira/browse/CASSANDRA-12590) | Segfault reading secondary index |  Critical | Local Write-Read Paths, Secondary Indexes | Cameron Zemek | Sam Tunnicliffe |
| [CASSANDRA-12914](https://issues.apache.org/jira/browse/CASSANDRA-12914) | cqlsh describe fails with "list[i] not a string for i in..." |  Minor | Tools | Adam Holmberg | Adam Holmberg |
| [CASSANDRA-12666](https://issues.apache.org/jira/browse/CASSANDRA-12666) | dtest failure in paging\_test.TestPagingData.test\_paging\_with\_filtering\_on\_partition\_key |  Critical | . | Sean McCarthy | Alex Petrov |
| [CASSANDRA-12900](https://issues.apache.org/jira/browse/CASSANDRA-12900) | Resurrect or remove HeapPool (unslabbed\_heap\_buffers) |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-12739](https://issues.apache.org/jira/browse/CASSANDRA-12739) | Nodetool uses cassandra-env.sh MAX\_HEAP\_SIZE if set |  Major | Tools | Brad Vernon |  |
| [CASSANDRA-12868](https://issues.apache.org/jira/browse/CASSANDRA-12868) | Reject default\_time\_to\_live option when creating or altering MVs |  Minor | . | Srinivasarao Daruna | Sundar Srinivasan |
| [CASSANDRA-12935](https://issues.apache.org/jira/browse/CASSANDRA-12935) | Use saved tokens when setting local tokens on StorageService.joinRing() |  Minor | Coordination | Paulo Motta | Paulo Motta |
| [CASSANDRA-12817](https://issues.apache.org/jira/browse/CASSANDRA-12817) | testall failure in org.apache.cassandra.cql3.validation.entities.UFTest.testAllNativeTypes |  Major | . | Sean McCarthy | Robert Stupp |
| [CASSANDRA-12768](https://issues.apache.org/jira/browse/CASSANDRA-12768) | CQL often queries static columns unnecessarily |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-12694](https://issues.apache.org/jira/browse/CASSANDRA-12694) | PAXOS Update Corrupted empty row exception |  Major | Local Write-Read Paths | Cameron Zemek | Alex Petrov |
| [CASSANDRA-12673](https://issues.apache.org/jira/browse/CASSANDRA-12673) | Nodes cannot see each other in multi-DC, non-EC2 environment with two-interface nodes due to outbound node-to-node connection binding to private interface |  Minor | Core | Milan Majercik | Milan Majercik |
| [CASSANDRA-12781](https://issues.apache.org/jira/browse/CASSANDRA-12781) | Disable RPC\_READY gossip flag when shutting down client servers |  Major | Distributed Metadata | Sean McCarthy | Stefania |
| [CASSANDRA-13033](https://issues.apache.org/jira/browse/CASSANDRA-13033) | Thread local pools never cleaned up |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-12796](https://issues.apache.org/jira/browse/CASSANDRA-12796) | Heap exhaustion when rebuilding secondary index over a table with wide partitions |  Critical | Core, Secondary Indexes | Milan Majercik | Sam Tunnicliffe |
| [CASSANDRA-13040](https://issues.apache.org/jira/browse/CASSANDRA-13040) | Estimated TS drop-time histogram updated with Cell.NO\_DELETION\_TIME |  Major | . | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-12984](https://issues.apache.org/jira/browse/CASSANDRA-12984) | MVs are built unnecessarily again after bootstrap |  Major | Core | Benjamin Roth | Benjamin Roth |
| [CASSANDRA-8616](https://issues.apache.org/jira/browse/CASSANDRA-8616) | sstable tools may result in commit log segments be written |  Major | Tools | Tyler Hobbs | Yuki Morishita |
| [CASSANDRA-13035](https://issues.apache.org/jira/browse/CASSANDRA-13035) | Failing CDC unit tests |  Major | . | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-12905](https://issues.apache.org/jira/browse/CASSANDRA-12905) | Retry acquire MV lock on failure instead of throwing WTE on streaming |  Critical | Materialized Views, Streaming and Messaging | Nir Zilka | Benjamin Roth |
| [CASSANDRA-12620](https://issues.apache.org/jira/browse/CASSANDRA-12620) | Resurrected rows with expired TTL on update to 3.x |  Major | Local Write-Read Paths | Collin Sauve | Benjamin Lerer |
| [CASSANDRA-13056](https://issues.apache.org/jira/browse/CASSANDRA-13056) | dtest failure in materialized\_views\_test.TestMaterializedViews.base\_replica\_repair\_test |  Blocker | Materialized Views, Testing | Sean McCarthy | Paulo Motta |
| [CASSANDRA-12959](https://issues.apache.org/jira/browse/CASSANDRA-12959) | copy from csv import wrong values with udt having set when fields are not specified in correct order in csv |  Major | Tools | Quentin Ambard | Stefania |
| [CASSANDRA-12909](https://issues.apache.org/jira/browse/CASSANDRA-12909) | cqlsh copy cannot parse strings when counters are present |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-13050](https://issues.apache.org/jira/browse/CASSANDRA-13050) | ReadCommand.CheckForAbort not monitoring CQL rows for range queries |  Major | Local Write-Read Paths | Stefania | Stefania |
| [CASSANDRA-13074](https://issues.apache.org/jira/browse/CASSANDRA-13074) | DynamicEndpointSnitch frequently no-ops through early exit in multi-datacenter situations |  Major | Coordination | Joel Knighton | Joel Knighton |
| [CASSANDRA-12453](https://issues.apache.org/jira/browse/CASSANDRA-12453) | AutoSavingCache does not store required keys making RowCacheTests Flaky |  Minor | Core | sankalp kohli | Jay Zhuang |
| [CASSANDRA-13046](https://issues.apache.org/jira/browse/CASSANDRA-13046) | Missing PID file directory when installing from RPM |  Minor | Packaging | Marcel Dopita | Michael Shuler |
| [CASSANDRA-12794](https://issues.apache.org/jira/browse/CASSANDRA-12794) | COPY FROM with NULL='' fails when inserting empty row in primary key |  Major | Tools | Sucwinder Bassi | Stefania |
| [CASSANDRA-12979](https://issues.apache.org/jira/browse/CASSANDRA-12979) | checkAvailableDiskSpace doesn't update expectedWriteSize when reducing thread scope |  Major | . | Jon Haddad | Jon Haddad |
| [CASSANDRA-12856](https://issues.apache.org/jira/browse/CASSANDRA-12856) | dtest failure in replication\_test.SnitchConfigurationUpdateTest.test\_cannot\_restart\_with\_different\_rack |  Major | . | Sean McCarthy | Stefania |
| [CASSANDRA-12203](https://issues.apache.org/jira/browse/CASSANDRA-12203) | AssertionError on compaction after upgrade (2.1.9 -\> 3.7) |  Critical | Core | Roman S. Borschel | Yuki Morishita |
| [CASSANDRA-12563](https://issues.apache.org/jira/browse/CASSANDRA-12563) | Stress daemon help is incorrect |  Trivial | Tools | Christopher Batey | Christopher Batey |
| [CASSANDRA-13115](https://issues.apache.org/jira/browse/CASSANDRA-13115) | Read repair is not blocking repair to finish in foreground repair |  Major | . | Xiaolong Jiang | Sylvain Lebresne |
| [CASSANDRA-13133](https://issues.apache.org/jira/browse/CASSANDRA-13133) | Unclosed file descriptors when querying SnapshotsSize metric |  Major | Local Write-Read Paths | Nicholas Rushton |  |
| [CASSANDRA-13075](https://issues.apache.org/jira/browse/CASSANDRA-13075) | Indexer is not correctly invoked when building indexes over sstables |  Critical | . | Sergio Bossa | Alex Petrov |
| [CASSANDRA-12443](https://issues.apache.org/jira/browse/CASSANDRA-12443) | Remove alter type support |  Major | . | Carl Yeksigian | Benjamin Lerer |
| [CASSANDRA-12925](https://issues.apache.org/jira/browse/CASSANDRA-12925) | AssertionError executing authz stmt on UDF without keyspace |  Minor | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-12664](https://issues.apache.org/jira/browse/CASSANDRA-12664) | GCCompactionTest is flaky |  Minor | Local Write-Read Paths | Stefania | Branimir Lambov |
| [CASSANDRA-13025](https://issues.apache.org/jira/browse/CASSANDRA-13025) | dtest failure in upgrade\_tests.cql\_tests.TestCQLNodes3RF3\_Upgrade\_current\_3\_x\_To\_indev\_3\_x.static\_columns\_with\_distinct\_test |  Critical | Testing | Sean McCarthy | Sylvain Lebresne |
| [CASSANDRA-13143](https://issues.apache.org/jira/browse/CASSANDRA-13143) | Cassandra can accept invalid durations |  Major | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-13013](https://issues.apache.org/jira/browse/CASSANDRA-13013) | Potential regression from CASSANDRA-6377 |  Major | CQL | Sylvain Lebresne | Benjamin Lerer |
| [CASSANDRA-12829](https://issues.apache.org/jira/browse/CASSANDRA-12829) | DELETE query with an empty IN clause can delete more than expected |  Major | CQL | Jason T. Bradshaw | Alex Petrov |
| [CASSANDRA-12368](https://issues.apache.org/jira/browse/CASSANDRA-12368) | Background read repair not fixing replicas |  Major | Coordination, Local Write-Read Paths | Paulo Motta | Alex Petrov |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11638](https://issues.apache.org/jira/browse/CASSANDRA-11638) | Add cassandra-stress test source |  Minor | Tools | Christopher Batey | Christopher Batey |
| [CASSANDRA-12225](https://issues.apache.org/jira/browse/CASSANDRA-12225) | dtest failure in materialized\_views\_test.TestMaterializedViews.clustering\_column\_test |  Major | Materialized Views, Testing | Sean McCarthy | Carl Yeksigian |
| [CASSANDRA-12894](https://issues.apache.org/jira/browse/CASSANDRA-12894) | testall failure in org.apache.cassandra.hints.HintsBufferPoolTest.testBackpressure-compression |  Major | . | Michael Shuler | Ariel Weisberg |
| [CASSANDRA-13058](https://issues.apache.org/jira/browse/CASSANDRA-13058) | dtest failure in hintedhandoff\_test.TestHintedHandoff.hintedhandoff\_decom\_test |  Major | Testing | Sean McCarthy | Stefan Podkowinski |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11580](https://issues.apache.org/jira/browse/CASSANDRA-11580) | remove DatabaseDescriptor dependency from SegmentedFile |  Major | Local Write-Read Paths | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-12142](https://issues.apache.org/jira/browse/CASSANDRA-12142) | Add "beta" version native protocol flag |  Major | CQL | Tyler Hobbs | Alex Petrov |
| [CASSANDRA-11844](https://issues.apache.org/jira/browse/CASSANDRA-11844) | Create compaction-stress |  Major | Compaction, Tools | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-6216](https://issues.apache.org/jira/browse/CASSANDRA-6216) | Level Compaction should persist last compacted key per level |  Minor | . | sankalp kohli | Dikang Gu |
| [CASSANDRA-12544](https://issues.apache.org/jira/browse/CASSANDRA-12544) | Portability Flaw: Locale Dependent Comparison |  Trivial | . | Eduardo Aguinaga | Dave Brosius |
| [CASSANDRA-12566](https://issues.apache.org/jira/browse/CASSANDRA-12566) | Null Dereference |  Minor | . | Eduardo Aguinaga | Dave Brosius |
| [CASSANDRA-12569](https://issues.apache.org/jira/browse/CASSANDRA-12569) | Null Dereference |  Trivial | . | Eduardo Aguinaga |  |
| [CASSANDRA-11841](https://issues.apache.org/jira/browse/CASSANDRA-11841) | Add keep-alive to stream protocol |  Major | Streaming and Messaging | Paulo Motta | Paulo Motta |
| [CASSANDRA-12567](https://issues.apache.org/jira/browse/CASSANDRA-12567) | Cleanup uses of AlterTableStatementColumn |  Trivial | CQL | Eduardo Aguinaga | Sylvain Lebresne |
| [CASSANDRA-12541](https://issues.apache.org/jira/browse/CASSANDRA-12541) | Portability Flaw: Locale Dependent Comparison |  Trivial | . | Eduardo Aguinaga |  |
| [CASSANDRA-12542](https://issues.apache.org/jira/browse/CASSANDRA-12542) | Portability Flaw: Locale Dependent Comparison |  Trivial | . | Eduardo Aguinaga |  |
| [CASSANDRA-12543](https://issues.apache.org/jira/browse/CASSANDRA-12543) | Portability Flaw: Locale Dependent Comparison |  Trivial | . | Eduardo Aguinaga |  |
| [CASSANDRA-12545](https://issues.apache.org/jira/browse/CASSANDRA-12545) | Portability Flaw: Locale Dependent Comparison |  Trivial | . | Eduardo Aguinaga |  |
| [CASSANDRA-12329](https://issues.apache.org/jira/browse/CASSANDRA-12329) | Unreleased Resource: Sockets |  Major | Streaming and Messaging | Eduardo Aguinaga | Arunkumar M |
| [CASSANDRA-12330](https://issues.apache.org/jira/browse/CASSANDRA-12330) | Unreleased Resource: Sockets |  Major | Streaming and Messaging | Eduardo Aguinaga | Arunkumar M |
| [CASSANDRA-12838](https://issues.apache.org/jira/browse/CASSANDRA-12838) | Extend native protocol flags and add supported versions to the SUPPORTED response |  Major | CQL | Stefania | Stefania |
| [CASSANDRA-12850](https://issues.apache.org/jira/browse/CASSANDRA-12850) | Add the duration type to the protocol V5 |  Major | CQL | Benjamin Lerer | Benjamin Lerer |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12712](https://issues.apache.org/jira/browse/CASSANDRA-12712) | Update Debian Install Doc |  Major | Documentation and Website | Michael Shuler | Michael Shuler |


