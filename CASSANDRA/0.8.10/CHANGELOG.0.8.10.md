
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.8.10 - 2012-02-13



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3661](https://issues.apache.org/jira/browse/CASSANDRA-3661) | Re-introduce timeout debug messages in CassandraServer |  Trivial | CQL | Jonathan Ellis | Brandon Williams |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3626](https://issues.apache.org/jira/browse/CASSANDRA-3626) | Nodes can get stuck in UP state forever, despite being DOWN |  Major | . | Peter Schuller | Brandon Williams |
| [CASSANDRA-3327](https://issues.apache.org/jira/browse/CASSANDRA-3327) | Support TimeUUID in CassandraStorage |  Major | . | Manuel Kreutz | Brandon Williams |
| [CASSANDRA-3415](https://issues.apache.org/jira/browse/CASSANDRA-3415) | show schema fails |  Minor | Tools | Radim Kolar | Jonathan Ellis |
| [CASSANDRA-3656](https://issues.apache.org/jira/browse/CASSANDRA-3656) | GC can take 0 ms |  Trivial | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3696](https://issues.apache.org/jira/browse/CASSANDRA-3696) | Adding another datacenter's node results in 0 rows returned on first datacenter |  Major | . | Joaquin Casares | Jonathan Ellis |
| [CASSANDRA-3733](https://issues.apache.org/jira/browse/CASSANDRA-3733) | Once a host has been hinted to, log messages for it repeat every 10 mins even if no hints are delivered |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3251](https://issues.apache.org/jira/browse/CASSANDRA-3251) | CassandraStorage uses comparator for both super column names and sub column names. |  Major | . | Dana H. P'Simer, Jr. | Pavel Yaskevich |
| [CASSANDRA-3751](https://issues.apache.org/jira/browse/CASSANDRA-3751) | Possible livelock during commit log playback |  Major | . | John Chakerian | Jonathan Ellis |
| [CASSANDRA-3875](https://issues.apache.org/jira/browse/CASSANDRA-3875) | SuperColumn may ignore relevant tombstones |  Major | . | Sylvain Lebresne | Sylvain Lebresne |


