
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.0.2 - 2013-10-28



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5515](https://issues.apache.org/jira/browse/CASSANDRA-5515) | Track sstable coldness |  Major | . | Jonathan Ellis | Tyler Hobbs |
| [CASSANDRA-6117](https://issues.apache.org/jira/browse/CASSANDRA-6117) | Avoid death-by-tombstone by default |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-6016](https://issues.apache.org/jira/browse/CASSANDRA-6016) | Ability to change replication factor for the trace keyspace |  Minor | . | Jeremiah Jordan | Yuki Morishita |
| [CASSANDRA-6170](https://issues.apache.org/jira/browse/CASSANDRA-6170) | Modify AbstractCassandraDaemon.initLog4j() to allow for hotfixing log level on on a cassandra class |  Major | Configuration | Darla Baker | Brandon Williams |
| [CASSANDRA-4191](https://issues.apache.org/jira/browse/CASSANDRA-4191) | Add \`nodetool cfstats \<ks\> \<cf\>\` abilities |  Minor | . | Joaquin Casares | Lyuben Todorov |
| [CASSANDRA-6202](https://issues.apache.org/jira/browse/CASSANDRA-6202) | Add a LOCAL\_ONE consistency level |  Minor | . | Brandon Williams | Jason Brown |
| [CASSANDRA-4949](https://issues.apache.org/jira/browse/CASSANDRA-4949) | Expose reload trigger functionality to nodetool to orchestrate trigger (re)loads across the cluster |  Major | . | Nate McCall | Suresh |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6058](https://issues.apache.org/jira/browse/CASSANDRA-6058) | Make ThriftServer more easlly extensible |  Minor | . | Aleksey Yeschenko | Sam Tunnicliffe |
| [CASSANDRA-6062](https://issues.apache.org/jira/browse/CASSANDRA-6062) | Remove Hadoop dependency from ITransportFactory |  Trivial | . | Aleksey Yeschenko | Sam Tunnicliffe |
| [CASSANDRA-6059](https://issues.apache.org/jira/browse/CASSANDRA-6059) | Improve memory-use defaults |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3916](https://issues.apache.org/jira/browse/CASSANDRA-3916) | Do not bind the storage\_port if internode\_encryption = all |  Minor | . | Wade Poziombka | Dave Brosius |
| [CASSANDRA-6042](https://issues.apache.org/jira/browse/CASSANDRA-6042) | Add WARN when there are a lot of tombstones in a query |  Minor | . | Jeremiah Jordan | Russell Spitzer |
| [CASSANDRA-6057](https://issues.apache.org/jira/browse/CASSANDRA-6057) | JMX Metric for number of tombstones per read |  Minor | Tools | sankalp kohli | Lyuben Todorov |
| [CASSANDRA-6070](https://issues.apache.org/jira/browse/CASSANDRA-6070) | Keep clients' remote addresses in ClientState |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6111](https://issues.apache.org/jira/browse/CASSANDRA-6111) | Add more hooks for compaction strategy implementations |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-4430](https://issues.apache.org/jira/browse/CASSANDRA-4430) | optional pluggable o.a.c.metrics reporters |  Minor | . | Chris Burroughs | Chris Burroughs |
| [CASSANDRA-5078](https://issues.apache.org/jira/browse/CASSANDRA-5078) | save compaction merge counts in a system table |  Minor | . | Matthew F. Dennis | lantao yan |
| [CASSANDRA-6044](https://issues.apache.org/jira/browse/CASSANDRA-6044) | FailureDetectorMBean should expose an integer of machines which are up and down. |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-6122](https://issues.apache.org/jira/browse/CASSANDRA-6122) | Optimize the auth default super user/default user check |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6176](https://issues.apache.org/jira/browse/CASSANDRA-6176) | remove decompression code in CompressedSequentialWriter |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-6068](https://issues.apache.org/jira/browse/CASSANDRA-6068) | Support login/password auth in cassandra-stress |  Trivial | . | Aleksey Yeschenko | Mikhail Stepura |
| [CASSANDRA-6191](https://issues.apache.org/jira/browse/CASSANDRA-6191) | Add a warning for small sstable size setting in LCS |  Minor | . | Tomas Salfischberger | Jonathan Ellis |
| [CASSANDRA-5571](https://issues.apache.org/jira/browse/CASSANDRA-5571) | Reject bootstrapping endpoints that are already in the ring with different gossip data |  Major | . | Rick Branson | Tyler Hobbs |
| [CASSANDRA-6030](https://issues.apache.org/jira/browse/CASSANDRA-6030) | java.lang.IncompatibleClassChangeError when starting EmbeddedCassandraService |  Minor | Documentation and Website | Jinder Aujla | Sylvain Lebresne |
| [CASSANDRA-6228](https://issues.apache.org/jira/browse/CASSANDRA-6228) | Add "view trace session" to cqlsh |  Trivial | Tools | Jeremiah Jordan | Mikhail Stepura |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6037](https://issues.apache.org/jira/browse/CASSANDRA-6037) | Parens around WHERE condition break query |  Minor | . | Sergey Nagaytsev | Dave Brosius |
| [CASSANDRA-6077](https://issues.apache.org/jira/browse/CASSANDRA-6077) | SSTableMetadata.min(max)ColumnNames keep whole SlabAllocatior region referenced; wasting memory |  Major | . | Oleg Anastasyev | Oleg Anastasyev |
| [CASSANDRA-6079](https://issues.apache.org/jira/browse/CASSANDRA-6079) | Memtables flush is delayed when having a lot of batchlog activity, making node OOM |  Minor | . | Oleg Anastasyev | Oleg Anastasyev |
| [CASSANDRA-6069](https://issues.apache.org/jira/browse/CASSANDRA-6069) | Sets not stored by INSERT with IF NOT EXISTS |  Major | . | Eric Evans | Sylvain Lebresne |
| [CASSANDRA-6081](https://issues.apache.org/jira/browse/CASSANDRA-6081) | Thrift validation refuses row markers on CQL3 tables |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4053](https://issues.apache.org/jira/browse/CASSANDRA-4053) | IncomingTcpConnection can not be closed when the peer is brutaly terminated or switch is failed |  Major | . | Zhu Han | Marcus Eriksson |
| [CASSANDRA-6088](https://issues.apache.org/jira/browse/CASSANDRA-6088) | StorageProxy.truncateBlocking sends truncation messages to fat client hosts; making truncation timeout |  Minor | . | Oleg Anastasyev | Oleg Anastasyev |
| [CASSANDRA-5954](https://issues.apache.org/jira/browse/CASSANDRA-5954) | Make nodetool ring print an error message and suggest nodetool status when vnodes are enabled |  Minor | Tools | Jonathan Ellis | Lyuben Todorov |
| [CASSANDRA-6087](https://issues.apache.org/jira/browse/CASSANDRA-6087) | Node OOMs on commit log replay when starting up |  Major | . | Oleg Anastasyev | Oleg Anastasyev |
| [CASSANDRA-6078](https://issues.apache.org/jira/browse/CASSANDRA-6078) | Wrong memtable size estimation: liveRatio is not honored in edge cases |  Minor | . | Oleg Anastasyev | Jonathan Ellis |
| [CASSANDRA-6080](https://issues.apache.org/jira/browse/CASSANDRA-6080) | Bad metadata returned for SELECT COUNT |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6093](https://issues.apache.org/jira/browse/CASSANDRA-6093) | Cant migrate json manifest with multiple data directories |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-6089](https://issues.apache.org/jira/browse/CASSANDRA-6089) | client\_only example does not work |  Minor | . | Mikhail Panchenko | Mikhail Panchenko |
| [CASSANDRA-6072](https://issues.apache.org/jira/browse/CASSANDRA-6072) | NPE in Pig CassandraStorage |  Major | . | Jeremiah Jordan | Alex Liu |
| [CASSANDRA-6071](https://issues.apache.org/jira/browse/CASSANDRA-6071) | CqlStorage loading compact table adds an extraneous field to the pig schema |  Minor | . | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-5932](https://issues.apache.org/jira/browse/CASSANDRA-5932) | Speculative read performance data show unexpected results |  Major | . | Ryan McGuire | Aleksey Yeschenko |
| [CASSANDRA-5950](https://issues.apache.org/jira/browse/CASSANDRA-5950) | Make snapshot/sequential repair the default |  Minor | Tools | Jonathan Ellis | Lyuben Todorov |
| [CASSANDRA-6112](https://issues.apache.org/jira/browse/CASSANDRA-6112) | Memtable flushing should write any columns shadowed by partition/range tombstones if any 2i are present |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6103](https://issues.apache.org/jira/browse/CASSANDRA-6103) | ConcurrentModificationException in TokenMetadata.cloneOnlyTokenMap |  Minor | . | Mike Schrag | Mikhail Stepura |
| [CASSANDRA-6098](https://issues.apache.org/jira/browse/CASSANDRA-6098) | NullPointerException causing query timeout |  Major | . | Lex Lythius | Sylvain Lebresne |
| [CASSANDRA-6115](https://issues.apache.org/jira/browse/CASSANDRA-6115) | UnsortedColumns.addAll(ColumnFamily) doesn't add the deletion info of the CF in argument |  Major | . | Sargun Dhillon | Sylvain Lebresne |
| [CASSANDRA-6133](https://issues.apache.org/jira/browse/CASSANDRA-6133) | Tracing should deal with write failure |  Trivial | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-6132](https://issues.apache.org/jira/browse/CASSANDRA-6132) | CL.ANY writes can still time out |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-6136](https://issues.apache.org/jira/browse/CASSANDRA-6136) | CQL should not allow an empty string as column identifier |  Minor | . | Michaël Figuière | Dave Brosius |
| [CASSANDRA-5383](https://issues.apache.org/jira/browse/CASSANDRA-5383) | Windows 7 deleting/renaming files problem |  Major | Testing | Ryan McGuire | Marcus Eriksson |
| [CASSANDRA-6101](https://issues.apache.org/jira/browse/CASSANDRA-6101) | Debian init script broken |  Minor | . | Anton Winter | Eric Evans |
| [CASSANDRA-6129](https://issues.apache.org/jira/browse/CASSANDRA-6129) | get java.util.ConcurrentModificationException while bulkloading from sstable for widerow table |  Major | Tools | koray sariteke | Jonathan Ellis |
| [CASSANDRA-6128](https://issues.apache.org/jira/browse/CASSANDRA-6128) | Add more data mappings for Pig |  Major | . | Alex Liu | Alex Liu |
| [CASSANDRA-6149](https://issues.apache.org/jira/browse/CASSANDRA-6149) | OOM in Cassandra 2.0.1 |  Major | . | Kai Wang | Jonathan Ellis |
| [CASSANDRA-5889](https://issues.apache.org/jira/browse/CASSANDRA-5889) | add tombstone metrics to cfstats or cfhistograms |  Minor | Tools | Jonathan Ellis | Mikhail Stepura |
| [CASSANDRA-6165](https://issues.apache.org/jira/browse/CASSANDRA-6165) | AE in hinted handoff delivery |  Major | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-6166](https://issues.apache.org/jira/browse/CASSANDRA-6166) | getOperationMode can thow NullPointerException |  Minor | . | Connor Warrington | Connor Warrington |
| [CASSANDRA-5725](https://issues.apache.org/jira/browse/CASSANDRA-5725) | Silently failing messages in case of schema not fully propagated |  Major | . | Sergio Bossa | Suresh |
| [CASSANDRA-6164](https://issues.apache.org/jira/browse/CASSANDRA-6164) | Switch CFS histograms to biased sampling |  Minor | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-6159](https://issues.apache.org/jira/browse/CASSANDRA-6159) | Documentation cites three but only two rpc\_server\_types provided |  Trivial | Documentation and Website | rektide de la fey | rektide de la fey |
| [CASSANDRA-6169](https://issues.apache.org/jira/browse/CASSANDRA-6169) | Too many splits causes a "OutOfMemoryError: unable to create new native thread" in AbstractColumnFamilyInputFormat |  Minor | . | Patricio Echague | Patricio Echague |
| [CASSANDRA-6175](https://issues.apache.org/jira/browse/CASSANDRA-6175) | counter increment hangs |  Major | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-5732](https://issues.apache.org/jira/browse/CASSANDRA-5732) | Can not query secondary index |  Major | Secondary Indexes | Tony Anecito | Sam Tunnicliffe |
| [CASSANDRA-6195](https://issues.apache.org/jira/browse/CASSANDRA-6195) | Typo in error msg in cqlsh: Bad Request: Only superusers are allowed to perfrom CREATE USER queries |  Trivial | . | Hari Sekhon | Brandon Williams |
| [CASSANDRA-5957](https://issues.apache.org/jira/browse/CASSANDRA-5957) | Cannot drop keyspace Keyspace1 after running cassandra-stress |  Minor | . | Piotr Kołaczkowski | Tyler Hobbs |
| [CASSANDRA-6183](https://issues.apache.org/jira/browse/CASSANDRA-6183) | Skip mutations that pass CRC but fail to deserialize |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5946](https://issues.apache.org/jira/browse/CASSANDRA-5946) | Commit Logs referencing deleted CFIDs not handled properly |  Major | . | Rick Branson | Jonathan Ellis |
| [CASSANDRA-5916](https://issues.apache.org/jira/browse/CASSANDRA-5916) | gossip and tokenMetadata get hostId out of sync on failed replace\_node with the same IP address |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-6217](https://issues.apache.org/jira/browse/CASSANDRA-6217) | replace doesn't clean up system.peers if you have a new IP |  Major | . | Jeremiah Jordan | Brandon Williams |
| [CASSANDRA-4925](https://issues.apache.org/jira/browse/CASSANDRA-4925) | Failure Detector should log or ignore sudden time change to the past |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-6114](https://issues.apache.org/jira/browse/CASSANDRA-6114) | Pig with widerows=true and batch size = 1 works incorrectly |  Minor | . | Alex Liu | Alex Liu |
| [CASSANDRA-6212](https://issues.apache.org/jira/browse/CASSANDRA-6212) | TimestampType doesn't support pre-epoch long |  Minor | CQL | Simon Hopkin | Mikhail Stepura |
| [CASSANDRA-6185](https://issues.apache.org/jira/browse/CASSANDRA-6185) | Can't update int column to blob type. |  Minor | . | Nick Bailey | Sylvain Lebresne |
| [CASSANDRA-6139](https://issues.apache.org/jira/browse/CASSANDRA-6139) | Cqlsh shouldn't display empty "value alias" |  Minor | . | Sylvain Lebresne | Aleksey Yeschenko |
| [CASSANDRA-6196](https://issues.apache.org/jira/browse/CASSANDRA-6196) | Add compaction, compression to cqlsh tab completion for CREATE TABLE |  Minor | Tools | Jonathan Ellis | Mikhail Stepura |
| [CASSANDRA-6205](https://issues.apache.org/jira/browse/CASSANDRA-6205) | sstableloader broken in 2.0 HEAD |  Major | . | Nick Bailey | Tyler Hobbs |
| [CASSANDRA-5815](https://issues.apache.org/jira/browse/CASSANDRA-5815) | NPE from migration manager |  Minor | . | Vishy Kasar | Brandon Williams |
| [CASSANDRA-6211](https://issues.apache.org/jira/browse/CASSANDRA-6211) | NPE in system.log |  Major | . | Mikhail Mazurskiy | Sylvain Lebresne |
| [CASSANDRA-6181](https://issues.apache.org/jira/browse/CASSANDRA-6181) | Replaying a commit led to java.lang.StackOverflowError and node crash |  Critical | . | Jeffrey Damick | Sylvain Lebresne |
| [CASSANDRA-6194](https://issues.apache.org/jira/browse/CASSANDRA-6194) | speculative retry can sometimes violate consistency |  Major | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-6233](https://issues.apache.org/jira/browse/CASSANDRA-6233) | Authentication is broken for the protocol v1 on C\* 2.0 |  Major | . | Sylvain Lebresne | Sylvain Lebresne |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5730](https://issues.apache.org/jira/browse/CASSANDRA-5730) | Re-add ScrubTest post single-pass compaction |  Minor | . | Sylvain Lebresne | Tyler Hobbs |
| [CASSANDRA-5695](https://issues.apache.org/jira/browse/CASSANDRA-5695) | Convert pig smoke tests into real PigUnit tests |  Minor | . | Brandon Williams | Alex Liu |
| [CASSANDRA-6197](https://issues.apache.org/jira/browse/CASSANDRA-6197) | Create more Pig unit tests for type mappings |  Minor | . | Alex Liu | Alex Liu |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4549](https://issues.apache.org/jira/browse/CASSANDRA-4549) | Update the pig examples to include more recent pig/cassandra features |  Minor | . | Jeremy Hanna | Alex Liu |
| [CASSANDRA-6214](https://issues.apache.org/jira/browse/CASSANDRA-6214) | Make LOCAL\_ONE the default consistency for cassandra.consistencylevel.[read\|write] |  Major | . | Jeremy Hanna | Alex Liu |


