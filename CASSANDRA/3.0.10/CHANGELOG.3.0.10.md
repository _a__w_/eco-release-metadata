
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.0.10 - 2016-11-16



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12788](https://issues.apache.org/jira/browse/CASSANDRA-12788) | AbstractReplicationStrategy is not extendable from outside the package |  Major | . | Kurt Greaves | Kurt Greaves |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12268](https://issues.apache.org/jira/browse/CASSANDRA-12268) | Make MV Index creation robust for wide referent rows |  Major | Core, Materialized Views | Jonathan Shook | Carl Yeksigian |
| [CASSANDRA-12646](https://issues.apache.org/jira/browse/CASSANDRA-12646) | nodetool stopdaemon errors out on stopdaemon |  Minor | Tools | Robert Stupp | Robert Stupp |
| [CASSANDRA-12384](https://issues.apache.org/jira/browse/CASSANDRA-12384) | Include info about sstable on "Compacting large row” message |  Trivial | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-12889](https://issues.apache.org/jira/browse/CASSANDRA-12889) | Pass root cause to CorruptBlockException when uncompression failed |  Trivial | Local Write-Read Paths | Yuki Morishita | Yuki Morishita |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12472](https://issues.apache.org/jira/browse/CASSANDRA-12472) | Backport CASSANDRA-10756 to 3.0 |  Major | Testing | Joel Knighton | Alex Petrov |
| [CASSANDRA-12516](https://issues.apache.org/jira/browse/CASSANDRA-12516) | Interned column identifiers can be overridden incorrectly |  Major | Local Write-Read Paths | Aleksey Yeschenko | Stefania |
| [CASSANDRA-11363](https://issues.apache.org/jira/browse/CASSANDRA-11363) | High Blocked NTR When Connecting |  Major | Coordination | Russell Bradberry | T Jake Luciani |
| [CASSANDRA-12499](https://issues.apache.org/jira/browse/CASSANDRA-12499) | Row cache does not cache partitions on tables without clustering keys |  Major | . | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-12060](https://issues.apache.org/jira/browse/CASSANDRA-12060) | Establish consistent distinction between non-existing partition and NULL value for LWTs on static columns |  Major | . | Alex Petrov | Alex Petrov |
| [CASSANDRA-12554](https://issues.apache.org/jira/browse/CASSANDRA-12554) | updateJobs in PendingRangeCalculatorService should be decremented in finally block |  Minor | Distributed Metadata | sankalp kohli | sankalp kohli |
| [CASSANDRA-11670](https://issues.apache.org/jira/browse/CASSANDRA-11670) | Rebuilding or streaming MV generates mutations larger than max\_mutation\_size\_in\_kb |  Major | Configuration, Materialized Views, Streaming and Messaging | Anastasia Osintseva | Paulo Motta |
| [CASSANDRA-12642](https://issues.apache.org/jira/browse/CASSANDRA-12642) | cqlsh NoHostsAvailable/AuthenticationFailure when sourcing a file with COPY commands |  Minor | Tools | Adam Holmberg | Stefania |
| [CASSANDRA-12253](https://issues.apache.org/jira/browse/CASSANDRA-12253) | Fix exceptions when enabling gossip on proxy nodes. |  Minor | Distributed Metadata | Dikang Gu | Dikang Gu |
| [CASSANDRA-12703](https://issues.apache.org/jira/browse/CASSANDRA-12703) | Compaction does not purge tombstones when only\_purge\_repaired\_tombstones is set |  Major | Compaction | Sharvanath Pathak | Sharvanath Pathak |
| [CASSANDRA-12618](https://issues.apache.org/jira/browse/CASSANDRA-12618) | Out of memory bug with one insert |  Critical | . | Eduardo Alonso de Blas | Benjamin Lerer |
| [CASSANDRA-12605](https://issues.apache.org/jira/browse/CASSANDRA-12605) | Timestamp-order searching of sstables does not handle non-frozen UDTs, frozen collections correctly |  Major | Local Write-Read Paths | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-12632](https://issues.apache.org/jira/browse/CASSANDRA-12632) | Failure in LogTransactionTest.testUnparsableFirstRecord-compression |  Major | Testing | Joel Knighton | Stefania |
| [CASSANDRA-12580](https://issues.apache.org/jira/browse/CASSANDRA-12580) | Fix merkle tree size calculation |  Major | Streaming and Messaging | Paulo Motta | Paulo Motta |
| [CASSANDRA-12706](https://issues.apache.org/jira/browse/CASSANDRA-12706) | Exception supposedly after ttl expires and compaction occurs |  Critical | Local Write-Read Paths | Nikhil Sharma | Stefania |
| [CASSANDRA-12509](https://issues.apache.org/jira/browse/CASSANDRA-12509) | Shutdown process triggered twice during if the node is drained |  Major | Lifecycle | Alex Petrov | Alex Petrov |
| [CASSANDRA-12715](https://issues.apache.org/jira/browse/CASSANDRA-12715) | Fix exceptions with the new vnode allocation. |  Major | . | Dikang Gu | Dikang Gu |
| [CASSANDRA-12478](https://issues.apache.org/jira/browse/CASSANDRA-12478) | cassandra stress still uses CFMetaData.compile() |  Major | Tools | Denis Ranger | Denis Ranger |
| [CASSANDRA-12457](https://issues.apache.org/jira/browse/CASSANDRA-12457) | dtest failure in upgrade\_tests.cql\_tests.TestCQLNodes2RF1\_Upgrade\_current\_2\_1\_x\_To\_indev\_2\_2\_x.bug\_5732\_test |  Major | Lifecycle | Craig Kodman | Stefania |
| [CASSANDRA-12552](https://issues.apache.org/jira/browse/CASSANDRA-12552) | CompressedRandomAccessReaderTest.testDataCorruptionDetection fails sporadically |  Minor | . | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-12582](https://issues.apache.org/jira/browse/CASSANDRA-12582) | Removing static column results in ReadFailure due to CorruptSSTableException |  Critical | Local Write-Read Paths | Evan Prothro | Stefania |
| [CASSANDRA-12740](https://issues.apache.org/jira/browse/CASSANDRA-12740) | cqlsh copy tests hang in case of no answer from the server or driver |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-12700](https://issues.apache.org/jira/browse/CASSANDRA-12700) | During writing data into Cassandra 3.7.0 using Python driver 3.7 sometimes Connection get lost, because of Server NullPointerException |  Major | Core | Rajesh Radhakrishnan | Jeff Jirsa |
| [CASSANDRA-12274](https://issues.apache.org/jira/browse/CASSANDRA-12274) | mx4j does not work in 3.0.8 |  Major | Core | Ilya | Robert Stupp |
| [CASSANDRA-11117](https://issues.apache.org/jira/browse/CASSANDRA-11117) | ColUpdateTimeDeltaHistogram histogram overflow |  Minor | Observability | Chris Lohfink | Joel Knighton |
| [CASSANDRA-12720](https://issues.apache.org/jira/browse/CASSANDRA-12720) | Permissions on aggregate functions are not removed on drop |  Major | Distributed Metadata | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-12417](https://issues.apache.org/jira/browse/CASSANDRA-12417) | Built-in AVG aggregate is much less useful than it should be |  Major | CQL | Branimir Lambov | Alex Petrov |
| [CASSANDRA-12776](https://issues.apache.org/jira/browse/CASSANDRA-12776) | when memtable flush Statistics thisOffHeap error |  Trivial | . | scott.zhai | Kurt Greaves |
| [CASSANDRA-12789](https://issues.apache.org/jira/browse/CASSANDRA-12789) | ViewTest.testCompaction releases reference twice |  Major | Testing | Stefania | Stefania |
| [CASSANDRA-11803](https://issues.apache.org/jira/browse/CASSANDRA-11803) | Creating a materialized view on a table with "token" column breaks the cluster |  Major | CQL, Materialized Views | Victor Trac | Carl Yeksigian |
| [CASSANDRA-12810](https://issues.apache.org/jira/browse/CASSANDRA-12810) | testall failure in org.apache.cassandra.db.ColumnFamilyStoreCQLHelperTest.testDynamicComposite |  Major | Testing | Sean McCarthy | Stefania |
| [CASSANDRA-12765](https://issues.apache.org/jira/browse/CASSANDRA-12765) | SSTable ignored incorrectly with partition level tombstone |  Major | Local Write-Read Paths | Cameron Zemek | Cameron Zemek |
| [CASSANDRA-12786](https://issues.apache.org/jira/browse/CASSANDRA-12786) | Fix a bug in CASSANDRA-11005(Split consisten range movement flag) |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-12754](https://issues.apache.org/jira/browse/CASSANDRA-12754) | Change cassandra.wait\_for\_tracing\_events\_timeout\_secs default to -1 so C\* doesn't wait on trace events to be written before responding to request by default |  Major | Observability | Andy Tolbert | Stefania |
| [CASSANDRA-12801](https://issues.apache.org/jira/browse/CASSANDRA-12801) | KeyCacheCqlTest fails on 3.0, 3.X and trunk |  Major | Testing | Stefania | Stefania |
| [CASSANDRA-12689](https://issues.apache.org/jira/browse/CASSANDRA-12689) | All MutationStage threads blocked, kills server |  Critical | Local Write-Read Paths, Materialized Views | Benjamin Roth | Benjamin Roth |
| [CASSANDRA-12854](https://issues.apache.org/jira/browse/CASSANDRA-12854) | CommitLogTest.testDeleteIfNotDirty failed in 3.X |  Major | Testing | Stefania | Stefania |
| [CASSANDRA-12296](https://issues.apache.org/jira/browse/CASSANDRA-12296) | Better error message when streaming with insufficient sources in DC |  Minor | . | Jim Witschey | Kurt Greaves |
| [CASSANDRA-12867](https://issues.apache.org/jira/browse/CASSANDRA-12867) | Batch with multiple conditional updates for the same partition causes AssertionError |  Critical | CQL | Kurt Greaves | Sylvain Lebresne |
| [CASSANDRA-12462](https://issues.apache.org/jira/browse/CASSANDRA-12462) | NullPointerException in CompactionInfo.getId(CompactionInfo.java:65) |  Major | Compaction | Jonathan DePrizio | Simon Zhou |
| [CASSANDRA-12808](https://issues.apache.org/jira/browse/CASSANDRA-12808) | testall failure inorg.apache.cassandra.io.sstable.IndexSummaryManagerTest.testCancelIndex |  Major | . | Sean McCarthy | Sam Tunnicliffe |
| [CASSANDRA-12184](https://issues.apache.org/jira/browse/CASSANDRA-12184) | incorrect compaction log information on totalSourceRows in C\* pre-3.8 versions |  Minor | Compaction, Observability | Wei Deng | Carl Yeksigian |
| [CASSANDRA-12834](https://issues.apache.org/jira/browse/CASSANDRA-12834) | testall failure in org.apache.cassandra.index.internal.CassandraIndexTest.indexOnFirstClusteringColumn |  Major | . | Sean McCarthy | Sylvain Lebresne |
| [CASSANDRA-12863](https://issues.apache.org/jira/browse/CASSANDRA-12863) | cqlsh COPY FROM cannot parse timestamp in partition key if table contains a counter value |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-11039](https://issues.apache.org/jira/browse/CASSANDRA-11039) | SegFault in Cassandra |  Blocker | Configuration | Nimi Wariboko Jr. | T Jake Luciani |
| [CASSANDRA-12813](https://issues.apache.org/jira/browse/CASSANDRA-12813) | NPE in auth for bootstrapping node |  Major | . | Charles Mims | Alex Petrov |
| [CASSANDRA-12792](https://issues.apache.org/jira/browse/CASSANDRA-12792) | delete with timestamp long.MAX\_VALUE for the whole key creates tombstone that cannot be removed. |  Major | Compaction | Ian Ilsley | Joel Knighton |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12225](https://issues.apache.org/jira/browse/CASSANDRA-12225) | dtest failure in materialized\_views\_test.TestMaterializedViews.clustering\_column\_test |  Major | Materialized Views, Testing | Sean McCarthy | Carl Yeksigian |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12541](https://issues.apache.org/jira/browse/CASSANDRA-12541) | Portability Flaw: Locale Dependent Comparison |  Trivial | . | Eduardo Aguinaga |  |
| [CASSANDRA-12542](https://issues.apache.org/jira/browse/CASSANDRA-12542) | Portability Flaw: Locale Dependent Comparison |  Trivial | . | Eduardo Aguinaga |  |
| [CASSANDRA-12543](https://issues.apache.org/jira/browse/CASSANDRA-12543) | Portability Flaw: Locale Dependent Comparison |  Trivial | . | Eduardo Aguinaga |  |
| [CASSANDRA-12545](https://issues.apache.org/jira/browse/CASSANDRA-12545) | Portability Flaw: Locale Dependent Comparison |  Trivial | . | Eduardo Aguinaga |  |
| [CASSANDRA-12329](https://issues.apache.org/jira/browse/CASSANDRA-12329) | Unreleased Resource: Sockets |  Major | Streaming and Messaging | Eduardo Aguinaga | Arunkumar M |
| [CASSANDRA-12330](https://issues.apache.org/jira/browse/CASSANDRA-12330) | Unreleased Resource: Sockets |  Major | Streaming and Messaging | Eduardo Aguinaga | Arunkumar M |


