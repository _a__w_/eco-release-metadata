
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.2.6 - 2016-04-26



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10622](https://issues.apache.org/jira/browse/CASSANDRA-10622) | Add all options in cqlshrc sample as commented out choices |  Minor | Documentation and Website | Lorina Poland | Tyler Hobbs |
| [CASSANDRA-11041](https://issues.apache.org/jira/browse/CASSANDRA-11041) | Make it clear what timestamp\_resolution is used for with DTCS |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-10397](https://issues.apache.org/jira/browse/CASSANDRA-10397) | Add local timezone support to cqlsh |  Minor | . | Suleman Rai | Stefan Podkowinski |
| [CASSANDRA-10888](https://issues.apache.org/jira/browse/CASSANDRA-10888) | Tombstone error warning does not log partition key |  Major | Local Write-Read Paths | Ole Pedersen | Brett Snyder |
| [CASSANDRA-11124](https://issues.apache.org/jira/browse/CASSANDRA-11124) | Change default cqlsh encoding to utf-8 |  Trivial | Tools | Paulo Motta | Paulo Motta |
| [CASSANDRA-6377](https://issues.apache.org/jira/browse/CASSANDRA-6377) | ALLOW FILTERING should allow seq scan filtering |  Major | CQL | Jonathan Ellis | Benjamin Lerer |
| [CASSANDRA-10900](https://issues.apache.org/jira/browse/CASSANDRA-10900) | CQL3 spec should clarify that now() function is calculated on the coordinator node |  Trivial | Documentation and Website | Wei Deng | Benjamin Lerer |
| [CASSANDRA-10809](https://issues.apache.org/jira/browse/CASSANDRA-10809) | Create a -D option to prevent gossip startup |  Major | Distributed Metadata | Brandon Williams | Sylvain Lebresne |
| [CASSANDRA-11411](https://issues.apache.org/jira/browse/CASSANDRA-11411) | Reduce the amount of logging during repair |  Minor | Streaming and Messaging | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-11179](https://issues.apache.org/jira/browse/CASSANDRA-11179) | Parallel cleanup can lead to disk space exhaustion |  Major | Compaction, Tools | Tyler Hobbs | Marcus Eriksson |
| [CASSANDRA-9325](https://issues.apache.org/jira/browse/CASSANDRA-9325) | cassandra-stress requires keystore for SSL but provides no way to configure it |  Major | Tools | J.B. Langston | Stefan Podkowinski |
| [CASSANDRA-11312](https://issues.apache.org/jira/browse/CASSANDRA-11312) | DatabaseDescriptor#applyConfig should log stacktrace in case of Eception during seed provider creation |  Minor | Configuration | Andrzej Ludwikowski | Andrzej Ludwikowski |
| [CASSANDRA-11486](https://issues.apache.org/jira/browse/CASSANDRA-11486) | Duplicate logging of merkle tree request |  Trivial | . | Marcus Olsson | Marcus Olsson |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10010](https://issues.apache.org/jira/browse/CASSANDRA-10010) | Paging on DISTINCT queries repeats result when first row in partition changes |  Minor | . | Tyler Hobbs | Benjamin Lerer |
| [CASSANDRA-11116](https://issues.apache.org/jira/browse/CASSANDRA-11116) | Gossiper#isEnabled is not thread safe |  Critical | Core | Sergio Bossa | Ariel Weisberg |
| [CASSANDRA-11113](https://issues.apache.org/jira/browse/CASSANDRA-11113) | DateTieredCompactionStrategy.getMaximalTask compacts repaired and unrepaired sstables together |  Major | . | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-11030](https://issues.apache.org/jira/browse/CASSANDRA-11030) | utf-8 characters incorrectly displayed/inserted on cqlsh on Windows |  Minor | . | Paulo Motta | Paulo Motta |
| [CASSANDRA-7238](https://issues.apache.org/jira/browse/CASSANDRA-7238) | Nodetool Status performance is much slower with VNodes On |  Minor | Tools | Russell Spitzer | Yuki Morishita |
| [CASSANDRA-10697](https://issues.apache.org/jira/browse/CASSANDRA-10697) | Leak detected while running offline scrub |  Major | Tools | mlowicki | Marcus Eriksson |
| [CASSANDRA-11079](https://issues.apache.org/jira/browse/CASSANDRA-11079) | LeveledCompactionStrategyTest.testCompactionProgress unit test fails sometimes |  Major | Testing | Sylvain Lebresne | Marcus Eriksson |
| [CASSANDRA-11080](https://issues.apache.org/jira/browse/CASSANDRA-11080) | CompactionsCQLTest.testTriggerMinorCompactionSTCS is flaky on 2.2 |  Major | . | Sylvain Lebresne | Marcus Eriksson |
| [CASSANDRA-7281](https://issues.apache.org/jira/browse/CASSANDRA-7281) | SELECT on tuple relations are broken for mixed ASC/DESC clustering order |  Major | . | Sylvain Lebresne | Marcin Szymaniuk |
| [CASSANDRA-11048](https://issues.apache.org/jira/browse/CASSANDRA-11048) | JSON queries are not thread safe |  Critical | Coordination | Sergio Bossa | Tyler Hobbs |
| [CASSANDRA-10733](https://issues.apache.org/jira/browse/CASSANDRA-10733) | Inconsistencies in CQLSH auto-complete |  Trivial | CQL, Tools | Michael Edge | Michael Edge |
| [CASSANDRA-10512](https://issues.apache.org/jira/browse/CASSANDRA-10512) | We do not save an upsampled index summaries |  Major | Local Write-Read Paths | Benedict | Ariel Weisberg |
| [CASSANDRA-10736](https://issues.apache.org/jira/browse/CASSANDRA-10736) | TestTopology.simple\_decommission\_test failing due to assertion triggered by SizeEstimatesRecorder |  Minor | Distributed Metadata | Joel Knighton | Joel Knighton |
| [CASSANDRA-11083](https://issues.apache.org/jira/browse/CASSANDRA-11083) | cassandra-2.2 eclipse-warnings |  Minor | Testing | Michael Shuler | Benjamin Lerer |
| [CASSANDRA-10176](https://issues.apache.org/jira/browse/CASSANDRA-10176) | nodetool status says ' Non-system keyspaces don't have the same replication settings' when they do |  Minor | Distributed Metadata | Chris Burroughs | Sylvain Lebresne |
| [CASSANDRA-11123](https://issues.apache.org/jira/browse/CASSANDRA-11123) | cqlsh pg-style-strings broken if line ends with ';' |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-9714](https://issues.apache.org/jira/browse/CASSANDRA-9714) | sstableloader appears to use the cassandra.yaml outgoing stream throttle |  Major | Tools | Jeremy Hanna | Yuki Morishita |
| [CASSANDRA-10767](https://issues.apache.org/jira/browse/CASSANDRA-10767) | Checking version of Cassandra command creates \`cassandra.logdir\_IS\_UNDEFINED/\` |  Trivial | Tools | Jens Rantil | Yuki Morishita |
| [CASSANDRA-10991](https://issues.apache.org/jira/browse/CASSANDRA-10991) | Cleanup OpsCenter keyspace fails - node thinks that didn't joined the ring yet |  Major | Observability | mlowicki | Marcus Eriksson |
| [CASSANDRA-11146](https://issues.apache.org/jira/browse/CASSANDRA-11146) | Adding field to UDT definition breaks SELECT JSON |  Major | CQL | Alexander | Tyler Hobbs |
| [CASSANDRA-11065](https://issues.apache.org/jira/browse/CASSANDRA-11065) | null pointer exception in CassandraDaemon.java:195 |  Minor | Streaming and Messaging | Vassil Lunchev | Paulo Motta |
| [CASSANDRA-10793](https://issues.apache.org/jira/browse/CASSANDRA-10793) | fix ohc and java-driver pom dependencies in build.xml |  Major | Packaging | Jeremiah Jordan | Robert Stupp |
| [CASSANDRA-11037](https://issues.apache.org/jira/browse/CASSANDRA-11037) | cqlsh bash script cannot be called through symlink |  Trivial | Tools | Benjamin Zarzycki | Benjamin Zarzycki |
| [CASSANDRA-10840](https://issues.apache.org/jira/browse/CASSANDRA-10840) | Replacing an aggregate with a new version doesn't reset INITCOND |  Major | CQL | Sandeep Tamhankar | Robert Stupp |
| [CASSANDRA-11172](https://issues.apache.org/jira/browse/CASSANDRA-11172) | Infinite loop bug adding high-level SSTableReader in compaction |  Major | Compaction | Jeff Ferland | Marcus Eriksson |
| [CASSANDRA-11167](https://issues.apache.org/jira/browse/CASSANDRA-11167) | NPE when creating serializing ErrorMessage for Exception with null message |  Minor | Coordination | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-10371](https://issues.apache.org/jira/browse/CASSANDRA-10371) | Decommissioned nodes can remain in gossip |  Minor | Distributed Metadata | Brandon Williams | Joel Knighton |
| [CASSANDRA-11212](https://issues.apache.org/jira/browse/CASSANDRA-11212) | cqlsh python version checking is out of date |  Major | Tools | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-11216](https://issues.apache.org/jira/browse/CASSANDRA-11216) | Range.compareTo() violates the contract of Comparable |  Minor | Core | Jason Brown | Jason Brown |
| [CASSANDRA-11164](https://issues.apache.org/jira/browse/CASSANDRA-11164) | Order and filter cipher suites correctly |  Minor | . | Tom Petracca | Stefan Podkowinski |
| [CASSANDRA-11215](https://issues.apache.org/jira/browse/CASSANDRA-11215) | Reference leak with parallel repairs on the same table |  Major | . | Marcus Olsson | Marcus Olsson |
| [CASSANDRA-11176](https://issues.apache.org/jira/browse/CASSANDRA-11176) | SSTableRewriter.InvalidateKeys should have a weak reference to cache |  Major | Core | Jeremiah Jordan | Marcus Eriksson |
| [CASSANDRA-11217](https://issues.apache.org/jira/browse/CASSANDRA-11217) | Only log yaml config once, at startup |  Minor | Configuration, Core | Jason Brown | Jason Brown |
| [CASSANDRA-11210](https://issues.apache.org/jira/browse/CASSANDRA-11210) | Unresolved hostname in replace address |  Minor | . | sankalp kohli | Jan Karlsson |
| [CASSANDRA-11297](https://issues.apache.org/jira/browse/CASSANDRA-11297) | AssertionError in nodetool cfstats in 3.0 (Back port CASSANDRA-10859) |  Major | Tools | Jeremiah Jordan | Yuki Morishita |
| [CASSANDRA-11302](https://issues.apache.org/jira/browse/CASSANDRA-11302) | Invalid time unit conversion causing write timeouts |  Major | Core | Mike Heffner | Sylvain Lebresne |
| [CASSANDRA-11325](https://issues.apache.org/jira/browse/CASSANDRA-11325) | Missing newline at end of bin/cqlsh causes it to be changed by the fixcrlf Ant plugin |  Trivial | Tools | Joel Knighton | Joel Knighton |
| [CASSANDRA-11301](https://issues.apache.org/jira/browse/CASSANDRA-11301) | Non-obsoleting compaction operations over compressed files can impose rate limit on normal reads |  Major | Core | Benedict | Stefania |
| [CASSANDRA-11286](https://issues.apache.org/jira/browse/CASSANDRA-11286) | streaming socket never times out |  Major | Streaming and Messaging | Paulo Motta | Paulo Motta |
| [CASSANDRA-11196](https://issues.apache.org/jira/browse/CASSANDRA-11196) | tuple\_notation\_test upgrade tests flaps |  Major | CQL | Jim Witschey | Benjamin Lerer |
| [CASSANDRA-11092](https://issues.apache.org/jira/browse/CASSANDRA-11092) | EXPAND breaks when the result has 0 rows in cqlsh |  Trivial | Tools | Kishan Karunaratne | Yuki Morishita |
| [CASSANDRA-11168](https://issues.apache.org/jira/browse/CASSANDRA-11168) | Hint Metrics are updated even if hinted\_hand-offs=false |  Minor | Coordination, Observability | Anubhav Kale | Anubhav Kale |
| [CASSANDRA-11344](https://issues.apache.org/jira/browse/CASSANDRA-11344) | Fix bloom filter sizing with LCS |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-10342](https://issues.apache.org/jira/browse/CASSANDRA-10342) | Read defragmentation can cause unnecessary repairs |  Minor | . | Marcus Olsson | Marcus Eriksson |
| [CASSANDRA-11184](https://issues.apache.org/jira/browse/CASSANDRA-11184) | cqlsh\_tests.py:TestCqlsh.test\_sub\_second\_precision is failing |  Major | Testing | Michael Shuler | Jim Witschey |
| [CASSANDRA-11185](https://issues.apache.org/jira/browse/CASSANDRA-11185) | cqlsh\_copy\_tests.py:CqlshCopyTest.test\_round\_trip\_with\_sub\_second\_precision is failing |  Major | Testing | Michael Shuler | Jim Witschey |
| [CASSANDRA-10752](https://issues.apache.org/jira/browse/CASSANDRA-10752) | CQL.textile wasn't updated for CASSANDRA-6839 |  Major | Documentation and Website | Jeremiah Jordan | Sylvain Lebresne |
| [CASSANDRA-10748](https://issues.apache.org/jira/browse/CASSANDRA-10748) | UTF8Validator.validate() wrong ?? |  Minor | Core | Robert Stupp | Benjamin Lerer |
| [CASSANDRA-11333](https://issues.apache.org/jira/browse/CASSANDRA-11333) | cqlsh: COPY FROM should check that explicit column names are valid |  Minor | Tools | Stefania | Stefania |
| [CASSANDRA-9598](https://issues.apache.org/jira/browse/CASSANDRA-9598) | bad classapth for 'sstablerepairedset' in 'cassandra-tools' package |  Minor | Tools | Clément Lardeur | Yuki Morishita |
| [CASSANDRA-11317](https://issues.apache.org/jira/browse/CASSANDRA-11317) | dtest failure in repair\_tests.incremental\_repair\_test.TestIncRepair.sstable\_repairedset\_test |  Major | Tools | Jim Witschey | Joel Knighton |
| [CASSANDRA-11435](https://issues.apache.org/jira/browse/CASSANDRA-11435) | PrepareCallback#response should not have DEBUG output |  Major | Core | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-11053](https://issues.apache.org/jira/browse/CASSANDRA-11053) | COPY FROM on large datasets: fix progress report and optimize performance part 4 |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-11373](https://issues.apache.org/jira/browse/CASSANDRA-11373) | Cancelled compaction leading to infinite loop in compaction strategy getNextBackgroundTask |  Major | Compaction | Eduard Tudenhoefner | Marcus Eriksson |
| [CASSANDRA-11450](https://issues.apache.org/jira/browse/CASSANDRA-11450) | Should not search for the index of a column if the table is not using secondaryIndex. |  Major | Compaction | Dikang Gu | Dikang Gu |
| [CASSANDRA-11451](https://issues.apache.org/jira/browse/CASSANDRA-11451) | Don't mark sstables as repairing when doing sub range repair |  Major | Streaming and Messaging | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-11448](https://issues.apache.org/jira/browse/CASSANDRA-11448) | Running OOS should trigger the disk failure policy |  Major | . | Brandon Williams | Branimir Lambov |
| [CASSANDRA-10587](https://issues.apache.org/jira/browse/CASSANDRA-10587) | sstablemetadata NPE on cassandra 2.2 |  Minor | Tools | Tiago Batista | Yuki Morishita |
| [CASSANDRA-11467](https://issues.apache.org/jira/browse/CASSANDRA-11467) | Paging loses rows in certain conditions |  Major | CQL | Ian McMahon | Benjamin Lerer |
| [CASSANDRA-11462](https://issues.apache.org/jira/browse/CASSANDRA-11462) | IncomingStreamingConnection version check message wrong |  Trivial | . | Robert Stupp |  |
| [CASSANDRA-11515](https://issues.apache.org/jira/browse/CASSANDRA-11515) | C\* won't launch with whitespace in path on Windows |  Trivial | . | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-11430](https://issues.apache.org/jira/browse/CASSANDRA-11430) | Add legacy notifications backward-support on deprecated repair methods |  Major | Observability, Streaming and Messaging | Nick Bailey | Paulo Motta |
| [CASSANDRA-11529](https://issues.apache.org/jira/browse/CASSANDRA-11529) | Checking if an unlogged batch is local is inefficient |  Critical | Coordination | Paulo Motta | Stefania |
| [CASSANDRA-11532](https://issues.apache.org/jira/browse/CASSANDRA-11532) | CqlConfigHelper requires both truststore and keystore to work with SSL encryption |  Major | . | Jacek Lewandowski | Jacek Lewandowski |
| [CASSANDRA-11427](https://issues.apache.org/jira/browse/CASSANDRA-11427) | Range slice queries CL \> ONE trigger read-repair of purgeable tombstones |  Minor | . | Stefan Podkowinski | Stefan Podkowinski |


