
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.2.10 - 2013-09-23



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5935](https://issues.apache.org/jira/browse/CASSANDRA-5935) | Allow disabling slab allocation |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5881](https://issues.apache.org/jira/browse/CASSANDRA-5881) | Need "describe cluster;" functionality in nodetool or cqlsh |  Major | Tools | Jeremiah Jordan | Lyuben Todorov |
| [CASSANDRA-6010](https://issues.apache.org/jira/browse/CASSANDRA-6010) | Notify before deleting SSTable |  Minor | . | Piotr Kołaczkowski | Piotr Kołaczkowski |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4952](https://issues.apache.org/jira/browse/CASSANDRA-4952) | Add blocking force compaction (and anything else) calls to NodeProbe |  Minor | . | Michael Harris | Jonathan Ellis |
| [CASSANDRA-5952](https://issues.apache.org/jira/browse/CASSANDRA-5952) | report compression ratio via nodetool cfstats |  Trivial | . | Robert Coli | Vijay |
| [CASSANDRA-5999](https://issues.apache.org/jira/browse/CASSANDRA-5999) | Modify PRSI interface and related callers to use the index method containing the updated key and cf. |  Major | . | Benjamin Coverston | Benjamin Coverston |
| [CASSANDRA-5994](https://issues.apache.org/jira/browse/CASSANDRA-5994) | Allow empty CQL3 batches (as no-op) |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6000](https://issues.apache.org/jira/browse/CASSANDRA-6000) | Enhance UntypedResultSet.Row by adding missing/additional helper methods |  Major | . | Benjamin Coverston | Benjamin Coverston |
| [CASSANDRA-6017](https://issues.apache.org/jira/browse/CASSANDRA-6017) | Move hints and exception metrics to o.a.c.metrics |  Minor | . | Yuki Morishita | Yuki Morishita |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5288](https://issues.apache.org/jira/browse/CASSANDRA-5288) | stress percentile label does not match what is returned |  Minor | Tools | Chris Burroughs | Jonathan Ellis |
| [CASSANDRA-5949](https://issues.apache.org/jira/browse/CASSANDRA-5949) | CqlRecordWriter sends empty composite partition-key components to thrift |  Minor | . | craig mcmillan | craig mcmillan |
| [CASSANDRA-5948](https://issues.apache.org/jira/browse/CASSANDRA-5948) | StreamOut doesn't correctly handle wrapped ranges |  Major | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-5967](https://issues.apache.org/jira/browse/CASSANDRA-5967) | Allow local batchlog writes for CL.ANY |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5955](https://issues.apache.org/jira/browse/CASSANDRA-5955) | The native protocol server can trigger a Netty bug |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5965](https://issues.apache.org/jira/browse/CASSANDRA-5965) | IndexSummary load fails when empty key exists in summary |  Major | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-5975](https://issues.apache.org/jira/browse/CASSANDRA-5975) | Filtering on Secondary Index Takes a Long Time Even with Limit 1, Trace Log Filled with Looping Messages |  Major | Secondary Indexes | Russell Spitzer | Aleksey Yeschenko |
| [CASSANDRA-5867](https://issues.apache.org/jira/browse/CASSANDRA-5867) | The Pig CqlStorage/AbstractCassandraStorage classes don't handle collection types |  Major | . | Jeremy Hanna | Alex Liu |
| [CASSANDRA-5910](https://issues.apache.org/jira/browse/CASSANDRA-5910) | Most CQL3 functions should handle null gracefully |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5847](https://issues.apache.org/jira/browse/CASSANDRA-5847) | Support thrift tables in Pig CqlStorage |  Major | . | Alex Liu | Alex Liu |
| [CASSANDRA-5990](https://issues.apache.org/jira/browse/CASSANDRA-5990) | Hinted Handoff: java.lang.ArithmeticException: / by zero |  Minor | . | Karl Mueller | Jonathan Ellis |
| [CASSANDRA-6007](https://issues.apache.org/jira/browse/CASSANDRA-6007) | Replace the deprecated MapMaker with CacheLoader for compatibility with Guava 15.0+ |  Minor | . | Max Penet | Aleksey Yeschenko |
| [CASSANDRA-6002](https://issues.apache.org/jira/browse/CASSANDRA-6002) | CqlRecordWriter misses clusterClumns and partitionKeyColumns size issue for thrift tables |  Minor | . | Alex Liu | Alex Liu |
| [CASSANDRA-6011](https://issues.apache.org/jira/browse/CASSANDRA-6011) | Race condition in snapshot repair |  Major | . | Nick Bailey | Yuki Morishita |
| [CASSANDRA-5968](https://issues.apache.org/jira/browse/CASSANDRA-5968) | Nodetool info throws NPE when connected to a booting instance |  Minor | . | Janne Jalkanen | Mikhail Stepura |
| [CASSANDRA-5909](https://issues.apache.org/jira/browse/CASSANDRA-5909) | CommitLogReplayer date time issue |  Minor | . | Artur Kronenberg | Vijay |
| [CASSANDRA-6026](https://issues.apache.org/jira/browse/CASSANDRA-6026) | NPE when running sstablesplit on valid sstable |  Major | . | Daniel Meyer | Sylvain Lebresne |
| [CASSANDRA-5982](https://issues.apache.org/jira/browse/CASSANDRA-5982) | OutOfMemoryError when writing text blobs to a very large number of tables |  Minor | . | Ryan McGuire | Jonathan Ellis |
| [CASSANDRA-6051](https://issues.apache.org/jira/browse/CASSANDRA-6051) | Wrong toString() of CQL3Type.Collection |  Minor | . | Alexander Radzin | Sylvain Lebresne |
| [CASSANDRA-5605](https://issues.apache.org/jira/browse/CASSANDRA-5605) | Crash caused by insufficient disk space to flush |  Minor | . | Dan Hendry | Jonathan Ellis |
| [CASSANDRA-6047](https://issues.apache.org/jira/browse/CASSANDRA-6047) | Memory leak when using snapshot repairs |  Minor | . | J.B. Langston | Yuki Morishita |
| [CASSANDRA-5947](https://issues.apache.org/jira/browse/CASSANDRA-5947) | Sampling bug in metrics-core-2.0.3.jar used by Cassandra |  Minor | Tools | J.B. Langston | Yuki Morishita |
| [CASSANDRA-5852](https://issues.apache.org/jira/browse/CASSANDRA-5852) | json2sstable breaks on data exported from sstable2json. |  Minor | Tools | Ryan McGuire | Lyuben Todorov |


