
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.1 - 2015-12-08



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9579](https://issues.apache.org/jira/browse/CASSANDRA-9579) | Add JMX / nodetool command to refresh system.size\_estimates |  Minor | CQL | Piotr Kołaczkowski | Carl Yeksigian |
| [CASSANDRA-10705](https://issues.apache.org/jira/browse/CASSANDRA-10705) | Fix typo in comment in cassandra.yaml |  Trivial | Configuration | Hobin Yoon |  |
| [CASSANDRA-10704](https://issues.apache.org/jira/browse/CASSANDRA-10704) | remove cobertura from build file |  Minor | . | Russ Hatch | Russ Hatch |
| [CASSANDRA-10703](https://issues.apache.org/jira/browse/CASSANDRA-10703) | backport test parallelization build tasks |  Major | . | Russ Hatch | Russ Hatch |
| [CASSANDRA-9043](https://issues.apache.org/jira/browse/CASSANDRA-9043) | Improve COPY command to work with Counter columns |  Minor | . | Sebastian Estevez | ZhaoYang |
| [CASSANDRA-9844](https://issues.apache.org/jira/browse/CASSANDRA-9844) | Reevaluate inspections in generate-idea-files target |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-10719](https://issues.apache.org/jira/browse/CASSANDRA-10719) | Inconsistencies within CQL 'describe', and CQL docs/'help describe' |  Minor | CQL, Documentation and Website, Tools | Michael Edge | Michael Edge |
| [CASSANDRA-8935](https://issues.apache.org/jira/browse/CASSANDRA-8935) | cqlsh: Make the column order in COPY FROM more apparent |  Trivial | Tools | Tyler Hobbs | Michael Edge |
| [CASSANDRA-9304](https://issues.apache.org/jira/browse/CASSANDRA-9304) | COPY TO improvements |  Minor | Tools | Jonathan Ellis | Stefania |
| [CASSANDRA-7225](https://issues.apache.org/jira/browse/CASSANDRA-7225) | show CQL help in cqlsh in web browser |  Trivial | Documentation and Website, Tools | Robert Stupp | Robert Stupp |
| [CASSANDRA-9984](https://issues.apache.org/jira/browse/CASSANDRA-9984) | Improve error reporting for malformed schemas in stress profile |  Trivial | Tools | Jim Witschey | T Jake Luciani |
| [CASSANDRA-10431](https://issues.apache.org/jira/browse/CASSANDRA-10431) | Normalize cqlsh DESC output |  Minor | CQL | Adam Holmberg | Adam Holmberg |
| [CASSANDRA-10243](https://issues.apache.org/jira/browse/CASSANDRA-10243) | Warn or fail when changing cluster topology live |  Critical | Tools | Jonathan Ellis | Stefania |
| [CASSANDRA-9474](https://issues.apache.org/jira/browse/CASSANDRA-9474) | Validate dc information on startup |  Major | . | Marcus Olsson | Marcus Olsson |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10672](https://issues.apache.org/jira/browse/CASSANDRA-10672) | jacoco instrumentation breaks UDF validation |  Major | . | Russ Hatch | Russ Hatch |
| [CASSANDRA-10605](https://issues.apache.org/jira/browse/CASSANDRA-10605) | Remove superfluous COUNTER\_MUTATION stage mapping |  Major | . | Anubhav Kale | Anubhav Kale |
| [CASSANDRA-10258](https://issues.apache.org/jira/browse/CASSANDRA-10258) | Reject counter writes in CQLSSTableWriter |  Major | Tools | Guillaume VIEL | Paulo Motta |
| [CASSANDRA-10058](https://issues.apache.org/jira/browse/CASSANDRA-10058) | Close Java driver Client object in Hadoop and Pig classes |  Major | . | Alex Liu | Alex Liu |
| [CASSANDRA-10257](https://issues.apache.org/jira/browse/CASSANDRA-10257) | InvertedIndex trigger example has not been updated post 8099 |  Minor | . | Mike Adamson | Mike Adamson |
| [CASSANDRA-10640](https://issues.apache.org/jira/browse/CASSANDRA-10640) | hadoop splits are calculated incorrectly |  Major | . | Alex Liu | Aleksey Yeschenko |
| [CASSANDRA-10089](https://issues.apache.org/jira/browse/CASSANDRA-10089) | NullPointerException in Gossip handleStateNormal |  Major | Distributed Metadata | Stefania | Stefania |
| [CASSANDRA-10341](https://issues.apache.org/jira/browse/CASSANDRA-10341) | Streaming does not guarantee cache invalidation |  Major | Streaming and Messaging | Benedict | Paulo Motta |
| [CASSANDRA-10027](https://issues.apache.org/jira/browse/CASSANDRA-10027) | ALTER TABLE TYPE check broken |  Minor | CQL | Aaron Ploetz | Benjamin Lerer |
| [CASSANDRA-10485](https://issues.apache.org/jira/browse/CASSANDRA-10485) | Missing host ID on hinted handoff write |  Major | Coordination | Paulo Motta | Paulo Motta |
| [CASSANDRA-10534](https://issues.apache.org/jira/browse/CASSANDRA-10534) | CompressionInfo not being fsynced on close |  Major | Local Write-Read Paths | Sharvanath Pathak | Stefania |
| [CASSANDRA-10538](https://issues.apache.org/jira/browse/CASSANDRA-10538) | Assertion failed in LogFile when disk is full |  Major | Local Write-Read Paths | Stefania | Stefania |
| [CASSANDRA-10685](https://issues.apache.org/jira/browse/CASSANDRA-10685) | Index transaction dealing with cleanup doesn't notify indexes of partition deletion |  Minor | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-10694](https://issues.apache.org/jira/browse/CASSANDRA-10694) | Deletion info is dropped on updated rows when notifying secondary index |  Major | Local Write-Read Paths, Secondary Indexes | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-10422](https://issues.apache.org/jira/browse/CASSANDRA-10422) | Avoid anticompaction when doing subrange repair |  Major | Compaction | Marcus Eriksson | Ariel Weisberg |
| [CASSANDRA-8879](https://issues.apache.org/jira/browse/CASSANDRA-8879) | Alter table on compact storage broken |  Minor | . | Nick Bailey | Aleksey Yeschenko |
| [CASSANDRA-10680](https://issues.apache.org/jira/browse/CASSANDRA-10680) | Deal with small compression chunk size better during streaming plan setup |  Major | Streaming and Messaging | Jeff Jirsa | Yuki Morishita |
| [CASSANDRA-10582](https://issues.apache.org/jira/browse/CASSANDRA-10582) | CorruptSSTableException should print the SS Table Name |  Minor | . | Anubhav Kale | Jeremiah Jordan |
| [CASSANDRA-10188](https://issues.apache.org/jira/browse/CASSANDRA-10188) | sstableloader does not use MAX\_HEAP\_SIZE env parameter |  Minor | . | Dan Hable | Michael Shuler |
| [CASSANDRA-10702](https://issues.apache.org/jira/browse/CASSANDRA-10702) | Statement concerning default ParallelGCThreads in jvm.options is not correct |  Trivial | Configuration | Nate McCall | Nate McCall |
| [CASSANDRA-10717](https://issues.apache.org/jira/browse/CASSANDRA-10717) | Changes from CASSANDRA-9353 makes Hadoop integration backward incompatible |  Major | . | Jacek Lewandowski | Jacek Lewandowski |
| [CASSANDRA-10741](https://issues.apache.org/jira/browse/CASSANDRA-10741) | Unable to create a function with argument of type Inet |  Major | CQL | dan jatnieks | Robert Stupp |
| [CASSANDRA-10012](https://issues.apache.org/jira/browse/CASSANDRA-10012) | Deadlock when session streaming is retried after exception |  Major | Streaming and Messaging | Chris Moos | Yuki Morishita |
| [CASSANDRA-10754](https://issues.apache.org/jira/browse/CASSANDRA-10754) | Failure in QueryPagerTest.multiQueryTest due to assertion in SinglePartitionReadCommand$Group |  Major | Testing | Joel Knighton | Joel Knighton |
| [CASSANDRA-10731](https://issues.apache.org/jira/browse/CASSANDRA-10731) | Bootstrap starts before migration responses have completed |  Major | Streaming and Messaging | Mike Adamson | Mike Adamson |
| [CASSANDRA-7217](https://issues.apache.org/jira/browse/CASSANDRA-7217) | Native transport performance (with cassandra-stress) drops precipitously past around 1000 threads |  Major | Tools | Benedict | Ariel Weisberg |
| [CASSANDRA-10740](https://issues.apache.org/jira/browse/CASSANDRA-10740) | Incorrect condition causes cleanup of SSTables which might not need it |  Major | . | Jakub Janecek | Jakub Janecek |
| [CASSANDRA-10729](https://issues.apache.org/jira/browse/CASSANDRA-10729) | SELECT statement with IN restrictions on partition key + ORDER BY + LIMIT return wrong results |  Major | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-10747](https://issues.apache.org/jira/browse/CASSANDRA-10747) | CQL.textile syntax incorrectly includes optional keyspace for aggregate SFUNC and FINALFUNC |  Trivial | Documentation and Website | dan jatnieks | Robert Stupp |
| [CASSANDRA-10749](https://issues.apache.org/jira/browse/CASSANDRA-10749) | DeletionTime.compareTo wrong in rare cases |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-10658](https://issues.apache.org/jira/browse/CASSANDRA-10658) | Some DROP ... IF EXISTS incorrectly result in exceptions on non-existing KS |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-8505](https://issues.apache.org/jira/browse/CASSANDRA-8505) | Invalid results are returned while secondary index are being build |  Major | Coordination, CQL, Secondary Indexes | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-9800](https://issues.apache.org/jira/browse/CASSANDRA-9800) | 2.2 eclipse-warnings |  Major | Testing | Michael Shuler | Ariel Weisberg |
| [CASSANDRA-10213](https://issues.apache.org/jira/browse/CASSANDRA-10213) | Status command in debian/ubuntu init script doesn't work |  Minor | . | Ruggero Marchei | Ruggero Marchei |
| [CASSANDRA-10775](https://issues.apache.org/jira/browse/CASSANDRA-10775) | SSTable compression ratio is not serialized properly |  Major | Local Write-Read Paths | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-10758](https://issues.apache.org/jira/browse/CASSANDRA-10758) | Dropping an index does not invalidate prepared statements anymore |  Major | CQL | Sylvain Lebresne | Sam Tunnicliffe |
| [CASSANDRA-10778](https://issues.apache.org/jira/browse/CASSANDRA-10778) | RowIndexEntry$Serializer invoked to serialize old format RIE triggering assertion |  Major | Local Write-Read Paths | Will Zhang | Ariel Weisberg |
| [CASSANDRA-10739](https://issues.apache.org/jira/browse/CASSANDRA-10739) | Timeout for CQL Deletes on an Entire Partition Against Specified Columns |  Major | CQL, Local Write-Read Paths | Caleb Rackliffe | Benjamin Lerer |
| [CASSANDRA-10753](https://issues.apache.org/jira/browse/CASSANDRA-10753) | Fix completion problems breaking clqshlib tests |  Major | Testing, Tools | Stefania | Stefania |
| [CASSANDRA-10774](https://issues.apache.org/jira/browse/CASSANDRA-10774) | Fail stream session if receiver cannot process data |  Critical | Streaming and Messaging | Paulo Motta | Paulo Motta |
| [CASSANDRA-10592](https://issues.apache.org/jira/browse/CASSANDRA-10592) | IllegalArgumentException in DataOutputBuffer.reallocate |  Major | Compaction, Local Write-Read Paths, Streaming and Messaging | Sebastian Estevez | Ariel Weisberg |
| [CASSANDRA-10768](https://issues.apache.org/jira/browse/CASSANDRA-10768) | Optimize the way we check if a token is repaired in anticompaction |  Major | Compaction, Streaming and Messaging | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-10288](https://issues.apache.org/jira/browse/CASSANDRA-10288) | Incremental repair can hang if replica aren't all up (was: Inconsistent behaviours on repair when a node in RF is missing) |  Major | Streaming and Messaging | Alan Boudreault | Yuki Morishita |
| [CASSANDRA-9510](https://issues.apache.org/jira/browse/CASSANDRA-9510) | assassinating an unknown endpoint could npe |  Trivial | Distributed Metadata | Dave Brosius | Dave Brosius |
| [CASSANDRA-10690](https://issues.apache.org/jira/browse/CASSANDRA-10690) | Remove unclear indexes() method from 2ndary index API |  Major | Local Write-Read Paths | Tyler Hobbs | Sylvain Lebresne |
| [CASSANDRA-10808](https://issues.apache.org/jira/browse/CASSANDRA-10808) | Cannot start Stress on Windows |  Major | Tools | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-10374](https://issues.apache.org/jira/browse/CASSANDRA-10374) | List and Map values incorrectly limited to 64k size |  Minor | . | Tyler Hobbs | Benjamin Lerer |
| [CASSANDRA-10788](https://issues.apache.org/jira/browse/CASSANDRA-10788) | Upgrade from 2.2.1 to 3.0.0 fails with NullPointerException |  Major | Lifecycle | Tomas Ramanauskas | Stefania |
| [CASSANDRA-10796](https://issues.apache.org/jira/browse/CASSANDRA-10796) | Views do not handle single-column deletions of view PK columns correctly |  Major | Coordination, Materialized Views | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-10018](https://issues.apache.org/jira/browse/CASSANDRA-10018) | Stats for several pools removed from nodetool tpstats output |  Major | Observability | Sam Tunnicliffe | Joel Knighton |
| [CASSANDRA-10585](https://issues.apache.org/jira/browse/CASSANDRA-10585) | SSTablesPerReadHistogram seems wrong when row cache hit happend |  Minor | Observability | Ivan Burmistrov | Ivan Burmistrov |
| [CASSANDRA-10122](https://issues.apache.org/jira/browse/CASSANDRA-10122) | AssertionError after upgrade to 3.0 |  Major | . | Russ Hatch | Sylvain Lebresne |
| [CASSANDRA-10674](https://issues.apache.org/jira/browse/CASSANDRA-10674) | Materialized View SSTable streaming/leaving status race on decommission |  Major | Coordination, Distributed Metadata, Materialized Views | Joel Knighton | Paulo Motta |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10059](https://issues.apache.org/jira/browse/CASSANDRA-10059) | Test Coverage and related bug-fixes for AbstractBTreePartition and hierarchy |  Major | Local Write-Read Paths | Benedict | Branimir Lambov |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10644](https://issues.apache.org/jira/browse/CASSANDRA-10644) | multiple repair dtest fails under Windows |  Major | Testing | Jim Witschey | Paulo Motta |
| [CASSANDRA-10669](https://issues.apache.org/jira/browse/CASSANDRA-10669) | Scrub dtests are failing |  Major | Local Write-Read Paths | Sylvain Lebresne | Marcus Eriksson |
| [CASSANDRA-10646](https://issues.apache.org/jira/browse/CASSANDRA-10646) | crash\_during\_decommission\_test dtest fails on windows |  Major | Testing | Jim Witschey | Paulo Motta |
| [CASSANDRA-10682](https://issues.apache.org/jira/browse/CASSANDRA-10682) | Fix timeouts in BeforeFirstTest |  Major | Testing | Stefania | Stefania |
| [CASSANDRA-10280](https://issues.apache.org/jira/browse/CASSANDRA-10280) | Make DTCS work well with old data |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-10771](https://issues.apache.org/jira/browse/CASSANDRA-10771) | bootstrap\_test.py:TestBootstrap.resumable\_bootstrap\_test is failing |  Major | Streaming and Messaging | Philip Thompson | Yuki Morishita |
| [CASSANDRA-10761](https://issues.apache.org/jira/browse/CASSANDRA-10761) | Possible regression of CASSANDRA-9201 |  Major | Distributed Metadata | Philip Thompson | Sam Tunnicliffe |
| [CASSANDRA-10803](https://issues.apache.org/jira/browse/CASSANDRA-10803) | MV repair tests are hanging |  Blocker | . | Philip Thompson | Paulo Motta |


