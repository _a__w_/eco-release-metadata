
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.2.11 - 2013-10-22



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6170](https://issues.apache.org/jira/browse/CASSANDRA-6170) | Modify AbstractCassandraDaemon.initLog4j() to allow for hotfixing log level on on a cassandra class |  Major | Configuration | Darla Baker | Brandon Williams |
| [CASSANDRA-6202](https://issues.apache.org/jira/browse/CASSANDRA-6202) | Add a LOCAL\_ONE consistency level |  Minor | . | Brandon Williams | Jason Brown |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5980](https://issues.apache.org/jira/browse/CASSANDRA-5980) | Allow cache keys-to-save to be dynamically set |  Minor | . | Chris Burroughs | Chris Burroughs |
| [CASSANDRA-3916](https://issues.apache.org/jira/browse/CASSANDRA-3916) | Do not bind the storage\_port if internode\_encryption = all |  Minor | . | Wade Poziombka | Dave Brosius |
| [CASSANDRA-6076](https://issues.apache.org/jira/browse/CASSANDRA-6076) | implement HELP  COMPOSITE\_PRIMARY\_KEYS |  Trivial | Tools | Dave Brosius | Dave Brosius |
| [CASSANDRA-6042](https://issues.apache.org/jira/browse/CASSANDRA-6042) | Add WARN when there are a lot of tombstones in a query |  Minor | . | Jeremiah Jordan | Russell Spitzer |
| [CASSANDRA-6057](https://issues.apache.org/jira/browse/CASSANDRA-6057) | JMX Metric for number of tombstones per read |  Minor | Tools | sankalp kohli | Lyuben Todorov |
| [CASSANDRA-6046](https://issues.apache.org/jira/browse/CASSANDRA-6046) | Update repair help to reference -snapshot |  Trivial | Tools | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-5084](https://issues.apache.org/jira/browse/CASSANDRA-5084) | Cassandra should expose connected client state via JMX |  Minor | . | Robert Coli | Mikhail Stepura |
| [CASSANDRA-6044](https://issues.apache.org/jira/browse/CASSANDRA-6044) | FailureDetectorMBean should expose an integer of machines which are up and down. |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-6122](https://issues.apache.org/jira/browse/CASSANDRA-6122) | Optimize the auth default super user/default user check |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6001](https://issues.apache.org/jira/browse/CASSANDRA-6001) | Add a Tracing line for index/seq scan queries |  Major | . | Jonathan Ellis | Lyuben Todorov |
| [CASSANDRA-6068](https://issues.apache.org/jira/browse/CASSANDRA-6068) | Support login/password auth in cassandra-stress |  Trivial | . | Aleksey Yeschenko | Mikhail Stepura |
| [CASSANDRA-6191](https://issues.apache.org/jira/browse/CASSANDRA-6191) | Add a warning for small sstable size setting in LCS |  Minor | . | Tomas Salfischberger | Jonathan Ellis |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6037](https://issues.apache.org/jira/browse/CASSANDRA-6037) | Parens around WHERE condition break query |  Minor | . | Sergey Nagaytsev | Dave Brosius |
| [CASSANDRA-6079](https://issues.apache.org/jira/browse/CASSANDRA-6079) | Memtables flush is delayed when having a lot of batchlog activity, making node OOM |  Minor | . | Oleg Anastasyev | Oleg Anastasyev |
| [CASSANDRA-6088](https://issues.apache.org/jira/browse/CASSANDRA-6088) | StorageProxy.truncateBlocking sends truncation messages to fat client hosts; making truncation timeout |  Minor | . | Oleg Anastasyev | Oleg Anastasyev |
| [CASSANDRA-5954](https://issues.apache.org/jira/browse/CASSANDRA-5954) | Make nodetool ring print an error message and suggest nodetool status when vnodes are enabled |  Minor | Tools | Jonathan Ellis | Lyuben Todorov |
| [CASSANDRA-6087](https://issues.apache.org/jira/browse/CASSANDRA-6087) | Node OOMs on commit log replay when starting up |  Major | . | Oleg Anastasyev | Oleg Anastasyev |
| [CASSANDRA-6078](https://issues.apache.org/jira/browse/CASSANDRA-6078) | Wrong memtable size estimation: liveRatio is not honored in edge cases |  Minor | . | Oleg Anastasyev | Jonathan Ellis |
| [CASSANDRA-5865](https://issues.apache.org/jira/browse/CASSANDRA-5865) | NPE when you mistakenly set listen\_address to 0.0.0.0 |  Trivial | . | Michaël Figuière | Brandon Williams |
| [CASSANDRA-6089](https://issues.apache.org/jira/browse/CASSANDRA-6089) | client\_only example does not work |  Minor | . | Mikhail Panchenko | Mikhail Panchenko |
| [CASSANDRA-6072](https://issues.apache.org/jira/browse/CASSANDRA-6072) | NPE in Pig CassandraStorage |  Major | . | Jeremiah Jordan | Alex Liu |
| [CASSANDRA-6099](https://issues.apache.org/jira/browse/CASSANDRA-6099) | Pig Storage classes should use IOException instead of RuntimeException |  Trivial | . | Alex Liu | Alex Liu |
| [CASSANDRA-6071](https://issues.apache.org/jira/browse/CASSANDRA-6071) | CqlStorage loading compact table adds an extraneous field to the pig schema |  Minor | . | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-6112](https://issues.apache.org/jira/browse/CASSANDRA-6112) | Memtable flushing should write any columns shadowed by partition/range tombstones if any 2i are present |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6103](https://issues.apache.org/jira/browse/CASSANDRA-6103) | ConcurrentModificationException in TokenMetadata.cloneOnlyTokenMap |  Minor | . | Mike Schrag | Mikhail Stepura |
| [CASSANDRA-6133](https://issues.apache.org/jira/browse/CASSANDRA-6133) | Tracing should deal with write failure |  Trivial | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-6119](https://issues.apache.org/jira/browse/CASSANDRA-6119) | IndexedSliceReader can skip columns when fetching multiple contiguous slices |  Major | . | Fabien Rousseau | Fabien Rousseau |
| [CASSANDRA-6107](https://issues.apache.org/jira/browse/CASSANDRA-6107) | CQL3 Batch statement memory leak |  Minor | CQL | Constance Eustace | Lyuben Todorov |
| [CASSANDRA-6128](https://issues.apache.org/jira/browse/CASSANDRA-6128) | Add more data mappings for Pig |  Major | . | Alex Liu | Alex Liu |
| [CASSANDRA-5889](https://issues.apache.org/jira/browse/CASSANDRA-5889) | add tombstone metrics to cfstats or cfhistograms |  Minor | Tools | Jonathan Ellis | Mikhail Stepura |
| [CASSANDRA-5725](https://issues.apache.org/jira/browse/CASSANDRA-5725) | Silently failing messages in case of schema not fully propagated |  Major | . | Sergio Bossa | Suresh |
| [CASSANDRA-6164](https://issues.apache.org/jira/browse/CASSANDRA-6164) | Switch CFS histograms to biased sampling |  Minor | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5752](https://issues.apache.org/jira/browse/CASSANDRA-5752) | Thrift tables are not supported from CqlPagingInputFormat |  Major | . | Jonathan Ellis | Alex Liu |
| [CASSANDRA-6160](https://issues.apache.org/jira/browse/CASSANDRA-6160) | Throw errror when attempting to create a secondary index against counter |  Minor | CQL, Secondary Indexes | Adam Hattrell | Sylvain Lebresne |
| [CASSANDRA-6097](https://issues.apache.org/jira/browse/CASSANDRA-6097) | nodetool repair randomly hangs. |  Minor | . | J.B. Langston | Yuki Morishita |
| [CASSANDRA-6169](https://issues.apache.org/jira/browse/CASSANDRA-6169) | Too many splits causes a "OutOfMemoryError: unable to create new native thread" in AbstractColumnFamilyInputFormat |  Minor | . | Patricio Echague | Patricio Echague |
| [CASSANDRA-6102](https://issues.apache.org/jira/browse/CASSANDRA-6102) | CassandraStorage broken for bigints and ints |  Major | . | Janne Jalkanen | Alex Liu |
| [CASSANDRA-5732](https://issues.apache.org/jira/browse/CASSANDRA-5732) | Can not query secondary index |  Major | Secondary Indexes | Tony Anecito | Sam Tunnicliffe |
| [CASSANDRA-6195](https://issues.apache.org/jira/browse/CASSANDRA-6195) | Typo in error msg in cqlsh: Bad Request: Only superusers are allowed to perfrom CREATE USER queries |  Trivial | . | Hari Sekhon | Brandon Williams |
| [CASSANDRA-5957](https://issues.apache.org/jira/browse/CASSANDRA-5957) | Cannot drop keyspace Keyspace1 after running cassandra-stress |  Minor | . | Piotr Kołaczkowski | Tyler Hobbs |
| [CASSANDRA-6152](https://issues.apache.org/jira/browse/CASSANDRA-6152) | Assertion error in 2.0.1 at db.ColumnSerializer.serialize(ColumnSerializer.java:56) |  Major | . | Donald Smith | Sylvain Lebresne |
| [CASSANDRA-6183](https://issues.apache.org/jira/browse/CASSANDRA-6183) | Skip mutations that pass CRC but fail to deserialize |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5946](https://issues.apache.org/jira/browse/CASSANDRA-5946) | Commit Logs referencing deleted CFIDs not handled properly |  Major | . | Rick Branson | Jonathan Ellis |
| [CASSANDRA-6208](https://issues.apache.org/jira/browse/CASSANDRA-6208) | AE while saving cache |  Major | . | Brandon Williams | Sam Tunnicliffe |
| [CASSANDRA-5916](https://issues.apache.org/jira/browse/CASSANDRA-5916) | gossip and tokenMetadata get hostId out of sync on failed replace\_node with the same IP address |  Major | . | Brandon Williams | Brandon Williams |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5695](https://issues.apache.org/jira/browse/CASSANDRA-5695) | Convert pig smoke tests into real PigUnit tests |  Minor | . | Brandon Williams | Alex Liu |
| [CASSANDRA-6197](https://issues.apache.org/jira/browse/CASSANDRA-6197) | Create more Pig unit tests for type mappings |  Minor | . | Alex Liu | Alex Liu |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4549](https://issues.apache.org/jira/browse/CASSANDRA-4549) | Update the pig examples to include more recent pig/cassandra features |  Minor | . | Jeremy Hanna | Alex Liu |


