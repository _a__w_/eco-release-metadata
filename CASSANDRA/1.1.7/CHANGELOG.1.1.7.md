
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.1.7 - 2012-11-30



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3756](https://issues.apache.org/jira/browse/CASSANDRA-3756) | cqlsh: allow configuration of value display formats |  Trivial | Tools | paul cannon | Aleksey Yeschenko |
| [CASSANDRA-4664](https://issues.apache.org/jira/browse/CASSANDRA-4664) | Rename permission USE to DESCRIBE. |  Major | . | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-4859](https://issues.apache.org/jira/browse/CASSANDRA-4859) | Include number of entries in CachedService |  Minor | . | Chris Burroughs | Chris Burroughs |
| [CASSANDRA-4875](https://issues.apache.org/jira/browse/CASSANDRA-4875) | Revert IAuthority2 interface |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-4921](https://issues.apache.org/jira/browse/CASSANDRA-4921) | improve cqlsh COPY FROM performance |  Minor | Tools | Jonathan Ellis | Aleksey Yeschenko |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4799](https://issues.apache.org/jira/browse/CASSANDRA-4799) | assertion failure in leveled compaction test |  Major | . | Brandon Williams | Yuki Morishita |
| [CASSANDRA-4807](https://issues.apache.org/jira/browse/CASSANDRA-4807) | Compaction progress counts more than 100% |  Minor | . | Omid Aladini | Yuki Morishita |
| [CASSANDRA-4804](https://issues.apache.org/jira/browse/CASSANDRA-4804) | Wrong assumption for KeyRange about range.end\_token in get\_range\_slices(). |  Minor | CQL | Nikolay | Nikolay |
| [CASSANDRA-4820](https://issues.apache.org/jira/browse/CASSANDRA-4820) | Add a close() method to CRAR to prevent leaking file descriptors. |  Major | . | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-4828](https://issues.apache.org/jira/browse/CASSANDRA-4828) | default cache provider does not match default yaml |  Trivial | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-4765](https://issues.apache.org/jira/browse/CASSANDRA-4765) | StackOverflowError in CompactionExecutor thread |  Major | . | Ahmed Bashir | Jonathan Ellis |
| [CASSANDRA-4832](https://issues.apache.org/jira/browse/CASSANDRA-4832) | AssertionError: keys must not be empty |  Minor | . | Tristan Seligmann | Tristan Seligmann |
| [CASSANDRA-4833](https://issues.apache.org/jira/browse/CASSANDRA-4833) | get\_count with 'count' param between 1024 and ~actual column count fails |  Major | . | Tyler Hobbs | Yuki Morishita |
| [CASSANDRA-4842](https://issues.apache.org/jira/browse/CASSANDRA-4842) | DateType in Column MetaData causes server crash |  Minor | . | Russell Bradberry | Sylvain Lebresne |
| [CASSANDRA-4816](https://issues.apache.org/jira/browse/CASSANDRA-4816) | Broken get\_paged\_slice |  Major | . | Piotr Kołaczkowski | Piotr Kołaczkowski |
| [CASSANDRA-4811](https://issues.apache.org/jira/browse/CASSANDRA-4811) | Some cqlsh help topics don't work (select, create, insert and anything else that is a cql statement) |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-4855](https://issues.apache.org/jira/browse/CASSANDRA-4855) | Debian packaging doesn't do auto-reloading of log4j properties file |  Minor | Packaging | Rick Branson | Brandon Williams |
| [CASSANDRA-4864](https://issues.apache.org/jira/browse/CASSANDRA-4864) | CQL2 CREATE COLUMNFAMILY checks wrong permission |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-4711](https://issues.apache.org/jira/browse/CASSANDRA-4711) | Duplicate column names with DynamicCompositeType |  Major | . | Shane | Sylvain Lebresne |
| [CASSANDRA-4746](https://issues.apache.org/jira/browse/CASSANDRA-4746) | cqlsh timestamp formatting is broken - displays wrong timezone info (at least on Ubuntu) |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-3306](https://issues.apache.org/jira/browse/CASSANDRA-3306) | Failed streaming may cause duplicate SSTable reference |  Major | . | Radim Kolar | Yuki Morishita |
| [CASSANDRA-4840](https://issues.apache.org/jira/browse/CASSANDRA-4840) | remnants of removed nodes remain after removal |  Minor | . | Jeremy Hanna | Brandon Williams |
| [CASSANDRA-4834](https://issues.apache.org/jira/browse/CASSANDRA-4834) | Old-style mapred interface only populates row key for first column when using wide rows |  Minor | . | Ben Kempe | Ben Kempe |
| [CASSANDRA-4910](https://issues.apache.org/jira/browse/CASSANDRA-4910) | CQL3 doesn't allow static CF definition with compact storage in C\* 1.1 |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4976](https://issues.apache.org/jira/browse/CASSANDRA-4976) | "AssertionError: Wrong class type" at CounterColumn.diff() |  Major | . | J.B. Langston | Sylvain Lebresne |
| [CASSANDRA-4830](https://issues.apache.org/jira/browse/CASSANDRA-4830) | JdbcDate.compose is not null safe |  Trivial | . | Sridharan Kuppa | Sridharan Kuppa |
| [CASSANDRA-4880](https://issues.apache.org/jira/browse/CASSANDRA-4880) | Endless loop flushing+compacting system/schema\_keyspaces and system/schema\_columnfamilies |  Major | . | Mina Naguib | Pavel Yaskevich |


