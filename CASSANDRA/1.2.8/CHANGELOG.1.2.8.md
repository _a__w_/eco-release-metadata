
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.2.8 - 2013-07-28



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5790](https://issues.apache.org/jira/browse/CASSANDRA-5790) | CqlStorage Pig partition push down support |  Major | . | Alex Liu | Alex Liu |
| [CASSANDRA-5626](https://issues.apache.org/jira/browse/CASSANDRA-5626) | Support empty IN queries |  Minor | . | Alexander Solovyev | Aleksey Yeschenko |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5698](https://issues.apache.org/jira/browse/CASSANDRA-5698) | cqlsh should support collections in COPY FROM |  Minor | Tools | Hiroshi Kise | Aleksey Yeschenko |
| [CASSANDRA-5804](https://issues.apache.org/jira/browse/CASSANDRA-5804) | AntiEntropySession fails when OutboundTcpConnection receives IOException |  Major | . | Ashley Winters | Ashley Winters |
| [CASSANDRA-5812](https://issues.apache.org/jira/browse/CASSANDRA-5812) | CQLSH Windows: TypeError: argument of type 'NoneType' is not iterable |  Minor | Tools | mick delaney | Aleksey Yeschenko |
| [CASSANDRA-5814](https://issues.apache.org/jira/browse/CASSANDRA-5814) | RowIndexEntry.deletionTime raises UnsupportedOperationException when upgrading to 1.2.7 |  Major | . | Brett Hoerner | Jonathan Ellis |


