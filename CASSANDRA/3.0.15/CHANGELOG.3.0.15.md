
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.0.15 - 2017-10-10



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13557](https://issues.apache.org/jira/browse/CASSANDRA-13557) | allow different NUMACTL\_ARGS to be passed in |  Minor | Configuration | Matt Byrd | Matt Byrd |
| [CASSANDRA-13078](https://issues.apache.org/jira/browse/CASSANDRA-13078) | Increase unittest test.runners to speed up the test |  Minor | Testing | Jay Zhuang | Jay Zhuang |
| [CASSANDRA-13722](https://issues.apache.org/jira/browse/CASSANDRA-13722) | "Number of keys (estimate)" in the output of the \`nodetool tablestats\` should be "Number of parititions (estimate)" |  Trivial | Tools | Milan Milosevic | Milan Milosevic |
| [CASSANDRA-12966](https://issues.apache.org/jira/browse/CASSANDRA-12966) | Gossip thread slows down when using batch commit log |  Minor | . | Jason Brown | Jason Brown |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13568](https://issues.apache.org/jira/browse/CASSANDRA-13568) | nodetool listsnapshots output is missing a newline, if there are no snapshots |  Trivial | Tools | Arnd Hannemann | Arnd Hannemann |
| [CASSANDRA-13627](https://issues.apache.org/jira/browse/CASSANDRA-13627) | Index queries are rejected on COMPACT tables |  Major | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-12606](https://issues.apache.org/jira/browse/CASSANDRA-12606) | CQLSSTableWriter unable to use blob conversion functions |  Minor | CQL, Tools | Mark Reddy | Alex Petrov |
| [CASSANDRA-13592](https://issues.apache.org/jira/browse/CASSANDRA-13592) | Null Pointer exception at SELECT JSON statement |  Major | CQL | Wyss Philipp | ZhaoYang |
| [CASSANDRA-13646](https://issues.apache.org/jira/browse/CASSANDRA-13646) | Bind parameters of collection types are not properly validated |  Major | . | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-13482](https://issues.apache.org/jira/browse/CASSANDRA-13482) | NPE on non-existing row read when row cache is enabled |  Major | . | Alex Petrov | Alex Petrov |
| [CASSANDRA-13272](https://issues.apache.org/jira/browse/CASSANDRA-13272) | "nodetool bootstrap resume" does not exit |  Major | Lifecycle, Streaming and Messaging | Tom van der Woerdt | Tim Lamballais |
| [CASSANDRA-13643](https://issues.apache.org/jira/browse/CASSANDRA-13643) | converting expired ttl cells to tombstones causing unnecessary digest mismatches |  Minor | . | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-13700](https://issues.apache.org/jira/browse/CASSANDRA-13700) | Heartbeats can cause gossip information to go permanently missing on certain nodes |  Critical | Distributed Metadata | Joel Knighton | Joel Knighton |
| [CASSANDRA-13696](https://issues.apache.org/jira/browse/CASSANDRA-13696) | Digest mismatch Exception if hints file has UnknownColumnFamily |  Blocker | Core | Jay Zhuang | Jay Zhuang |
| [CASSANDRA-13711](https://issues.apache.org/jira/browse/CASSANDRA-13711) | Invalid writetime for null columns in cqlsh |  Major | CQL | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-13691](https://issues.apache.org/jira/browse/CASSANDRA-13691) | Fix incorrect [2.1 \<— 3.0] serialization of counter cells with pre-2.1 local shards |  Major | Coordination | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-11223](https://issues.apache.org/jira/browse/CASSANDRA-11223) | Queries with LIMIT filtering on clustering columns can return less rows than expected |  Major | Local Write-Read Paths | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-13371](https://issues.apache.org/jira/browse/CASSANDRA-13371) | Remove legacy auth tables support |  Major | Auth | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-13730](https://issues.apache.org/jira/browse/CASSANDRA-13730) | Dropping a table doesn't drop its dropped columns |  Minor | Distributed Metadata | Duarte Nunes | ZhaoYang |
| [CASSANDRA-13737](https://issues.apache.org/jira/browse/CASSANDRA-13737) | Node start can fail if the base table of a materialized view is not found |  Major | Distributed Metadata, Materialized Views | Andrés de la Peña | Andrés de la Peña |
| [CASSANDRA-13067](https://issues.apache.org/jira/browse/CASSANDRA-13067) | Integer overflows with file system size reported by Amazon Elastic File System (EFS) |  Major | Core | Michael Hanselmann | Benjamin Lerer |
| [CASSANDRA-13532](https://issues.apache.org/jira/browse/CASSANDRA-13532) | sstabledump reports incorrect usage for argument order |  Minor | Tools | Ian Ilsley | Varun Barala |
| [CASSANDRA-12884](https://issues.apache.org/jira/browse/CASSANDRA-12884) | Batch logic can lead to unbalanced use of system.batches |  Major | Core | Adam Hattrell | Daniel Cranford |
| [CASSANDRA-13775](https://issues.apache.org/jira/browse/CASSANDRA-13775) | CircleCI tests fail because \*stress-test\* isn't a valid target |  Major | Build | Eduard Tudenhoefner | Eduard Tudenhoefner |
| [CASSANDRA-13649](https://issues.apache.org/jira/browse/CASSANDRA-13649) | Uncaught exceptions in Netty pipeline |  Major | Streaming and Messaging, Testing | Stefan Podkowinski | Norman Maurer |
| [CASSANDRA-13773](https://issues.apache.org/jira/browse/CASSANDRA-13773) | cassandra-stress writes even data when n=0 |  Minor | Stress | Eduard Tudenhoefner | Eduard Tudenhoefner |
| [CASSANDRA-13719](https://issues.apache.org/jira/browse/CASSANDRA-13719) | Potential AssertionError during ReadRepair of range tombstone and partition deletions |  Major | Coordination | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-13776](https://issues.apache.org/jira/browse/CASSANDRA-13776) | Adding a field to an UDT can corrupte the tables using it |  Critical | . | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-13620](https://issues.apache.org/jira/browse/CASSANDRA-13620) | Don't skip corrupt sstables on startup |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-13747](https://issues.apache.org/jira/browse/CASSANDRA-13747) | Fix AssertionError in short read protection |  Major | Coordination | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-13764](https://issues.apache.org/jira/browse/CASSANDRA-13764) | SelectTest.testMixedTTLOnColumnsWide is flaky |  Trivial | Testing | Joel Knighton | Jeff Jirsa |
| [CASSANDRA-13807](https://issues.apache.org/jira/browse/CASSANDRA-13807) | CircleCI fix - only collect the xml file from containers where it exists |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-13587](https://issues.apache.org/jira/browse/CASSANDRA-13587) | Deadlock during CommitLog replay when Cassandra restarts |  Major | Core | Jaydeepkumar Chovatia | Jaydeepkumar Chovatia |
| [CASSANDRA-13363](https://issues.apache.org/jira/browse/CASSANDRA-13363) | Fix racy read command serialization |  Major | . | Artem Rokhin | Aleksey Yeschenko |
| [CASSANDRA-13626](https://issues.apache.org/jira/browse/CASSANDRA-13626) | Check hashed password matches expected bcrypt hash format before checking |  Minor | Auth | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-13833](https://issues.apache.org/jira/browse/CASSANDRA-13833) | Failed compaction is not captured |  Major | Compaction | Jay Zhuang | Jay Zhuang |
| [CASSANDRA-11500](https://issues.apache.org/jira/browse/CASSANDRA-11500) | Obsolete MV entry may not be properly deleted |  Major | Materialized Views | Sylvain Lebresne | ZhaoYang |
| [CASSANDRA-13738](https://issues.apache.org/jira/browse/CASSANDRA-13738) | Load is over calculated after each IndexSummaryRedistribution |  Major | Core | Jay Zhuang | Jay Zhuang |
| [CASSANDRA-13263](https://issues.apache.org/jira/browse/CASSANDRA-13263) | Incorrect ComplexColumnData hashCode implementation |  Major | . | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-13756](https://issues.apache.org/jira/browse/CASSANDRA-13756) | StreamingHistogram is not thread safe |  Major | . | xiangzhou xia | Jeff Jirsa |
| [CASSANDRA-11995](https://issues.apache.org/jira/browse/CASSANDRA-11995) | Commitlog replaced with all NULs |  Major | Core | James Howe | Jeff Jirsa |
| [CASSANDRA-13603](https://issues.apache.org/jira/browse/CASSANDRA-13603) | Change repair midpoint logging from  CASSANDRA-13052 |  Trivial | Streaming and Messaging | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-13655](https://issues.apache.org/jira/browse/CASSANDRA-13655) | Range deletes in a CAS batch are ignored |  Blocker | CQL | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-13069](https://issues.apache.org/jira/browse/CASSANDRA-13069) | Local batchlog for MV may not be correctly written on node movements |  Major | Materialized Views | Sylvain Lebresne | Paulo Motta |
| [CASSANDRA-13717](https://issues.apache.org/jira/browse/CASSANDRA-13717) | INSERT statement fails when Tuple type is used as clustering column with default DESC order |  Critical | Core, CQL | Anastasios Kichidis | Stavros Kontopoulos |
| [CASSANDRA-12014](https://issues.apache.org/jira/browse/CASSANDRA-12014) | IndexSummary \> 2G causes an assertion error |  Minor | Core | Brandon Williams | Stefania |
| [CASSANDRA-13868](https://issues.apache.org/jira/browse/CASSANDRA-13868) | Safely handle empty buffers when outputting to JSON |  Minor | . | Jason Brown | Jason Brown |
| [CASSANDRA-12872](https://issues.apache.org/jira/browse/CASSANDRA-12872) | Fix short read protection when more than one row is missing |  Critical | Coordination | Bhaskar Muppana | Aleksey Yeschenko |
| [CASSANDRA-13866](https://issues.apache.org/jira/browse/CASSANDRA-13866) | Clock-dependent integer overflow in tests CellTest and RowsTest |  Trivial | Testing | Joel Knighton | Joel Knighton |
| [CASSANDRA-13619](https://issues.apache.org/jira/browse/CASSANDRA-13619) | java.nio.BufferOverflowException: null while flushing hints |  Major | Coordination, Core | Milan Milosevic | Marcus Eriksson |
| [CASSANDRA-13880](https://issues.apache.org/jira/browse/CASSANDRA-13880) | Fix short read protection for tables with no clustering columns |  Major | Coordination | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-13787](https://issues.apache.org/jira/browse/CASSANDRA-13787) | RangeTombstoneMarker and PartitionDeletion is not properly included in MV |  Major | Local Write-Read Paths, Materialized Views | ZhaoYang | ZhaoYang |
| [CASSANDRA-13794](https://issues.apache.org/jira/browse/CASSANDRA-13794) | Fix short read protection logic for querying more rows |  Major | Coordination | Benedict | Aleksey Yeschenko |
| [CASSANDRA-13043](https://issues.apache.org/jira/browse/CASSANDRA-13043) | UnavailabeException caused by counter writes forwarded to leaders without complete cluster view |  Minor | Coordination | Catalin Alexandru Zamfir | Stefano Ortolani |
| [CASSANDRA-13894](https://issues.apache.org/jira/browse/CASSANDRA-13894) | TriggerExecutor ignored original PartitionUpdate |  Major | Local Write-Read Paths | ZhaoYang | ZhaoYang |
| [CASSANDRA-13883](https://issues.apache.org/jira/browse/CASSANDRA-13883) | StrictLiveness for view row is not handled in AbstractRow |  Major | Materialized Views | ZhaoYang | ZhaoYang |
| [CASSANDRA-12373](https://issues.apache.org/jira/browse/CASSANDRA-12373) | 3.0 breaks CQL compatibility with super columns families |  Major | CQL | Sylvain Lebresne | Alex Petrov |
| [CASSANDRA-13149](https://issues.apache.org/jira/browse/CASSANDRA-13149) | AssertionError prepending to a list |  Major | CQL | Steven Warren | Jason Brown |
| [CASSANDRA-13918](https://issues.apache.org/jira/browse/CASSANDRA-13918) | Header only commit logs should be filtered before recovery |  Major | . | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-13911](https://issues.apache.org/jira/browse/CASSANDRA-13911) | IllegalStateException thrown by UPI.Serializer.hasNext() for some SELECT queries |  Major | Coordination | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-13595](https://issues.apache.org/jira/browse/CASSANDRA-13595) | Implement short read protection on partition boundaries |  Major | Coordination | Andrés de la Peña | Aleksey Yeschenko |
| [CASSANDRA-13909](https://issues.apache.org/jira/browse/CASSANDRA-13909) | Improve TRUNCATE performance with many sstables |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-14137](https://issues.apache.org/jira/browse/CASSANDRA-14137) | Cassandra crashes on startup.  Crash Problematic frame: # C  [sigar-amd64-winnt.dll+0x14ed4] using JRE version: Java(TM) SE Runtime Environment (9.0+11) |  Minor | Core, Metrics | mark pettovello |  |
| [CASSANDRA-13797](https://issues.apache.org/jira/browse/CASSANDRA-13797) | RepairJob blocks on syncTasks |  Major | Repair | Blake Eggleston | Blake Eggleston |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13782](https://issues.apache.org/jira/browse/CASSANDRA-13782) | Cassandra RPM has wrong owner for /usr/share directories |  Major | Packaging | Hannu Kröger | Sasatani Takenobu |


