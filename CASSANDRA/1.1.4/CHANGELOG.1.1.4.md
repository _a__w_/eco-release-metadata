
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.1.4 - 2012-08-19



### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4411](https://issues.apache.org/jira/browse/CASSANDRA-4411) | Assertion with LCS compaction |  Major | . | Anton Winter | Sylvain Lebresne |
| [CASSANDRA-4455](https://issues.apache.org/jira/browse/CASSANDRA-4455) | Nodetool fail to setcompactionthreshold |  Minor | Tools | Jason Tang | Aleksey Yeschenko |
| [CASSANDRA-4337](https://issues.apache.org/jira/browse/CASSANDRA-4337) | Data insertion fails because of commitlog rename failure |  Major | . | Patrycjusz | Jonathan Ellis |
| [CASSANDRA-4466](https://issues.apache.org/jira/browse/CASSANDRA-4466) | ColumnFamilyRecordReader hadoop integration fails with ghost keys |  Minor | . | Niel Drummond | Jonathan Ellis |
| [CASSANDRA-4484](https://issues.apache.org/jira/browse/CASSANDRA-4484) | Drain causes incorrect error messages: "Stream took more than 24H to complete; skipping" |  Minor | . | Christopher Porter | Christopher Porter |
| [CASSANDRA-4494](https://issues.apache.org/jira/browse/CASSANDRA-4494) | nodetool can't work at all ! |  Critical | Tools | sunjian | paul cannon |
| [CASSANDRA-4534](https://issues.apache.org/jira/browse/CASSANDRA-4534) | old-style mapred interface doesn't set key limit correctly |  Minor | . | Jonathan Ellis | Jonathan Ellis |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3072](https://issues.apache.org/jira/browse/CASSANDRA-3072) | Add a wide-row slice system test |  Minor | Testing | Stu Hood | Brandon Williams |


