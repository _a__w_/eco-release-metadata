
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.0 alpha 1 - 2015-08-03



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7207](https://issues.apache.org/jira/browse/CASSANDRA-7207) | nodetool: Allow to stop a specific compaction |  Minor | Tools | PJ | Lyuben Todorov |
| [CASSANDRA-6477](https://issues.apache.org/jira/browse/CASSANDRA-6477) | Materialized Views (was: Global Indexes) |  Major | CQL, Materialized Views | Jonathan Ellis | Carl Yeksigian |
| [CASSANDRA-7486](https://issues.apache.org/jira/browse/CASSANDRA-7486) | Migrate to G1GC by default |  Major | Configuration | Jonathan Ellis |  |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9317](https://issues.apache.org/jira/browse/CASSANDRA-9317) | Populate TokenMetadata earlier during startup |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9397](https://issues.apache.org/jira/browse/CASSANDRA-9397) | Wrong gc\_grace\_seconds used in anticompaction |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9401](https://issues.apache.org/jira/browse/CASSANDRA-9401) | Better check for gossip stabilization on startup |  Major | . | Tyler Hobbs | Brandon Williams |
| [CASSANDRA-9447](https://issues.apache.org/jira/browse/CASSANDRA-9447) | SSTableDeletingTask uses COWArraySet unnnecessarily |  Trivial | . | Benedict | Benedict |
| [CASSANDRA-9342](https://issues.apache.org/jira/browse/CASSANDRA-9342) | Remove WrappingCompactionStrategy |  Minor | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9553](https://issues.apache.org/jira/browse/CASSANDRA-9553) | AbstractCompactionStrategy#filterSSTablesForReads is unused externally, remove |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9353](https://issues.apache.org/jira/browse/CASSANDRA-9353) | Remove deprecated legacy Hadoop code in 3.0 |  Major | . | Aleksey Yeschenko | Philip Thompson |
| [CASSANDRA-8897](https://issues.apache.org/jira/browse/CASSANDRA-8897) | Remove FileCacheService, instead pooling the buffers |  Major | Local Write-Read Paths | Benedict | Stefania |
| [CASSANDRA-6591](https://issues.apache.org/jira/browse/CASSANDRA-6591) | un-deprecate cache recentHitRate and expose in o.a.c.metrics |  Minor | . | Chris Burroughs | Chris Burroughs |
| [CASSANDRA-9571](https://issues.apache.org/jira/browse/CASSANDRA-9571) | Set HAS\_MORE\_PAGES flag when PagingState is null |  Minor | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-9547](https://issues.apache.org/jira/browse/CASSANDRA-9547) | Add nodetool command to force blocking batchlog replay |  Minor | Tools | Carl Yeksigian | Carl Yeksigian |
| [CASSANDRA-9532](https://issues.apache.org/jira/browse/CASSANDRA-9532) | Provide access to select statement's real column definitions |  Major | CQL | mck | Sam Tunnicliffe |
| [CASSANDRA-7032](https://issues.apache.org/jira/browse/CASSANDRA-7032) | Improve vnode allocation |  Major | . | Benedict | Branimir Lambov |
| [CASSANDRA-8584](https://issues.apache.org/jira/browse/CASSANDRA-8584) | Add rate limited logging of failed trySkipCache calls and commit log lag |  Trivial | Local Write-Read Paths, Observability | Joshua McKenzie | Ariel Weisberg |
| [CASSANDRA-9090](https://issues.apache.org/jira/browse/CASSANDRA-9090) | Allow JMX over SSL directly from nodetool |  Major | Tools | Philip Thompson |  |
| [CASSANDRA-7814](https://issues.apache.org/jira/browse/CASSANDRA-7814) | enable describe on indices |  Minor | Tools | radha | Stefania |
| [CASSANDRA-9677](https://issues.apache.org/jira/browse/CASSANDRA-9677) | Refactor KSMetaData |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-9701](https://issues.apache.org/jira/browse/CASSANDRA-9701) | Enforce simple \<\< complex sort order more strictly and efficiently |  Minor | . | Benedict | Benedict |
| [CASSANDRA-9499](https://issues.apache.org/jira/browse/CASSANDRA-9499) | Introduce writeVInt method to DataOutputStreamPlus |  Minor | Compaction, Local Write-Read Paths, Streaming and Messaging | Benedict | Ariel Weisberg |
| [CASSANDRA-9706](https://issues.apache.org/jira/browse/CASSANDRA-9706) | Precompute ColumnIdentifier comparison |  Minor | . | Benedict | Benedict |
| [CASSANDRA-9522](https://issues.apache.org/jira/browse/CASSANDRA-9522) | Specify unset column ratios in cassandra-stress write |  Major | Tools | Jim Witschey | T Jake Luciani |
| [CASSANDRA-9697](https://issues.apache.org/jira/browse/CASSANDRA-9697) | remove StorageProxy.OPTIMIZE\_LOCAL\_REQUESTS |  Major | Coordination | Robert Stupp | Stefania |
| [CASSANDRA-9653](https://issues.apache.org/jira/browse/CASSANDRA-9653) | Update Guava to version 18 |  Major | . | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-9769](https://issues.apache.org/jira/browse/CASSANDRA-9769) | Improve BTree |  Major | . | Benedict | Benedict |
| [CASSANDRA-9682](https://issues.apache.org/jira/browse/CASSANDRA-9682) | setting log4j.logger.org.apache.cassandra=DEBUG causes keyspace username/password to show up in system.log |  Major | Observability | Victor Chen | Sam Tunnicliffe |
| [CASSANDRA-8915](https://issues.apache.org/jira/browse/CASSANDRA-8915) | Improve MergeIterator performance |  Minor | . | Branimir Lambov | Branimir Lambov |
| [CASSANDRA-9797](https://issues.apache.org/jira/browse/CASSANDRA-9797) | Don't wrap byte arrays in SequentialWriter |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-9723](https://issues.apache.org/jira/browse/CASSANDRA-9723) | UDF / UDA execution time in trace |  Minor | . | Christopher Batey | Christopher Batey |
| [CASSANDRA-9845](https://issues.apache.org/jira/browse/CASSANDRA-9845) | Fix build.xml to not download non-existent source jars, cleanup duplicate artifacts |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-9850](https://issues.apache.org/jira/browse/CASSANDRA-9850) | Allow optional schema statements in cassandra-stress yaml |  Minor | Materialized Views, Tools | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-9757](https://issues.apache.org/jira/browse/CASSANDRA-9757) | Cache maven-ant-tasks.jar locally |  Trivial | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-9448](https://issues.apache.org/jira/browse/CASSANDRA-9448) | Metrics should use up to date nomenclature |  Major | Tools | Sam Tunnicliffe | Stefania |
| [CASSANDRA-9699](https://issues.apache.org/jira/browse/CASSANDRA-9699) | Make choice of sstable reader types explicit |  Major | . | Benedict | Benedict |
| [CASSANDRA-9423](https://issues.apache.org/jira/browse/CASSANDRA-9423) | Improve Leak Detection to cover strong reference leaks |  Major | . | Benedict | Benedict |
| [CASSANDRA-9888](https://issues.apache.org/jira/browse/CASSANDRA-9888) | BTreeBackedRow and ComplexColumnData |  Major | . | Benedict | Benedict |
| [CASSANDRA-7066](https://issues.apache.org/jira/browse/CASSANDRA-7066) | Simplify (and unify) cleanup of compaction leftovers |  Minor | Local Write-Read Paths | Benedict | Stefania |
| [CASSANDRA-8099](https://issues.apache.org/jira/browse/CASSANDRA-8099) | Refactor and modernize the storage engine |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-8894](https://issues.apache.org/jira/browse/CASSANDRA-8894) | Our default buffer size for (uncompressed) buffered reads should be smaller, and based on the expected record size |  Major | Local Write-Read Paths | Benedict | Stefania |
| [CASSANDRA-6388](https://issues.apache.org/jira/browse/CASSANDRA-6388) | delay vnode selection if bootstrapping |  Major | . | Brandon Williams | Branimir Lambov |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9240](https://issues.apache.org/jira/browse/CASSANDRA-9240) | Performance issue after a restart |  Minor | . | Alan Boudreault | Benedict |
| [CASSANDRA-9405](https://issues.apache.org/jira/browse/CASSANDRA-9405) | AssertionError: Data component is missing |  Major | . | Tyler Hobbs | Marcus Eriksson |
| [CASSANDRA-8801](https://issues.apache.org/jira/browse/CASSANDRA-8801) | Decommissioned nodes are willing to rejoin the cluster if restarted |  Major | . | Eric Stevens | Brandon Williams |
| [CASSANDRA-9508](https://issues.apache.org/jira/browse/CASSANDRA-9508) | Avoid early opening of compaction results when doing anticompaction |  Critical | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9521](https://issues.apache.org/jira/browse/CASSANDRA-9521) | Cannot start dclocal repairs |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9534](https://issues.apache.org/jira/browse/CASSANDRA-9534) | Repair data not always saved to system\_distributed |  Major | . | Jim Witschey | Marcus Eriksson |
| [CASSANDRA-9566](https://issues.apache.org/jira/browse/CASSANDRA-9566) | Running upgradesstables on 2.2 fails |  Major | . | Carl Yeksigian | Marcus Eriksson |
| [CASSANDRA-9569](https://issues.apache.org/jira/browse/CASSANDRA-9569) | nodetool should exit with status code != 0 if NodeProbe fails |  Major | Tools | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9586](https://issues.apache.org/jira/browse/CASSANDRA-9586) | ant eclipse-warnings fails in trunk |  Major | Local Write-Read Paths | Michael Shuler | Stefania |
| [CASSANDRA-9572](https://issues.apache.org/jira/browse/CASSANDRA-9572) | DateTieredCompactionStrategy fails to combine SSTables correctly when TTL is used. |  Major | Compaction | Antti Nissinen | Marcus Eriksson |
| [CASSANDRA-9567](https://issues.apache.org/jira/browse/CASSANDRA-9567) | Windows does not handle ipv6 addresses |  Major | . | Philip Thompson | Philip Thompson |
| [CASSANDRA-9621](https://issues.apache.org/jira/browse/CASSANDRA-9621) | Repair of the SystemDistributed keyspace creates a non-trivial amount of memory pressure |  Minor | . | Sylvain Lebresne | Marcus Eriksson |
| [CASSANDRA-9698](https://issues.apache.org/jira/browse/CASSANDRA-9698) | UFTest fails with BOP |  Trivial | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-9681](https://issues.apache.org/jira/browse/CASSANDRA-9681) | Memtable heap size grows and many long GC pauses are triggered |  Critical | . | mlowicki | Benedict |
| [CASSANDRA-9636](https://issues.apache.org/jira/browse/CASSANDRA-9636) | Duplicate columns in selection causes AssertionError |  Major | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-8413](https://issues.apache.org/jira/browse/CASSANDRA-8413) | Bloom filter false positive ratio is not honoured |  Major | . | Benedict | Robert Stupp |
| [CASSANDRA-9727](https://issues.apache.org/jira/browse/CASSANDRA-9727) | AuthSuccess NPE |  Major | . | Pierre N. |  |
| [CASSANDRA-9743](https://issues.apache.org/jira/browse/CASSANDRA-9743) | Fix merging of rows in PartitionUpdate |  Major | . | Aleksey Yeschenko | Sylvain Lebresne |
| [CASSANDRA-9733](https://issues.apache.org/jira/browse/CASSANDRA-9733) |  InsertUpdateIfCondition.testConditionalDelete is failing on trunk |  Major | Local Write-Read Paths | Benjamin Lerer | Stefania |
| [CASSANDRA-9750](https://issues.apache.org/jira/browse/CASSANDRA-9750) | hashCode in UDFunction broken |  Trivial | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-9671](https://issues.apache.org/jira/browse/CASSANDRA-9671) | sum() and avg() functions missing for smallint and tinyint types |  Major | . | Aleksey Yeschenko | Robert Stupp |
| [CASSANDRA-9746](https://issues.apache.org/jira/browse/CASSANDRA-9746) | Fix PartitionUpdate.rowCount() |  Major | . | Aleksey Yeschenko | Sylvain Lebresne |
| [CASSANDRA-9780](https://issues.apache.org/jira/browse/CASSANDRA-9780) | Get rid of CFMetaData isPurged(), markPurged() |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-9759](https://issues.apache.org/jira/browse/CASSANDRA-9759) | RowAndTombstoneMergeIterator combine RangeTombstones even if they should not be |  Major | Local Write-Read Paths | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-9686](https://issues.apache.org/jira/browse/CASSANDRA-9686) | FSReadError and LEAK DETECTED after upgrading |  Major | Local Write-Read Paths | Andreas Schnitzerling | Stefania |
| [CASSANDRA-9781](https://issues.apache.org/jira/browse/CASSANDRA-9781) | Dropped columns handled incorrectly in trunk |  Major | . | Aleksey Yeschenko | Sylvain Lebresne |
| [CASSANDRA-9808](https://issues.apache.org/jira/browse/CASSANDRA-9808) | 3.0 cannot read 2.2 sstables |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-9837](https://issues.apache.org/jira/browse/CASSANDRA-9837) | Fix broken logging for "empty" flushes in Memtable |  Trivial | . | Michael McKibben |  |
| [CASSANDRA-9825](https://issues.apache.org/jira/browse/CASSANDRA-9825) | SELECT queries broken for Thrift-created tables with non-string comparators |  Major | CQL | Aleksey Yeschenko | Sylvain Lebresne |
| [CASSANDRA-9816](https://issues.apache.org/jira/browse/CASSANDRA-9816) | ALTER TABLE ADD validation is overly strict for re-added collection columns |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-8220](https://issues.apache.org/jira/browse/CASSANDRA-8220) | JDK bug prevents clean shutdown on OSX with Java 1.8.0\_20 |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-9803](https://issues.apache.org/jira/browse/CASSANDRA-9803) | trunk eclipse-warnings |  Major | Testing | Michael Shuler | Ariel Weisberg |
| [CASSANDRA-9848](https://issues.apache.org/jira/browse/CASSANDRA-9848) | Broken serialization of CFMetaData ids |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-9787](https://issues.apache.org/jira/browse/CASSANDRA-9787) | Unit tests are failing on trunk after CASSANDRA-9650 |  Major | Local Write-Read Paths, Testing | Yuki Morishita | T Jake Luciani |
| [CASSANDRA-7357](https://issues.apache.org/jira/browse/CASSANDRA-7357) | Cleaning up snapshots on startup leftover from repairs that got interrupted |  Minor | Streaming and Messaging | Stephen Johnson | Paulo Motta |
| [CASSANDRA-9765](https://issues.apache.org/jira/browse/CASSANDRA-9765) | checkForEndpointCollision fails for legitimate collisions |  Major | Distributed Metadata | Richard Low | Stefania |
| [CASSANDRA-9859](https://issues.apache.org/jira/browse/CASSANDRA-9859) | IndexedReader updateBlock exception |  Major | Local Write-Read Paths, Materialized Views | Carl Yeksigian | Carl Yeksigian |
| [CASSANDRA-9864](https://issues.apache.org/jira/browse/CASSANDRA-9864) | Stress leaves threads running after a fatal error |  Major | Tools | Ryan McGuire | Benedict |
| [CASSANDRA-9731](https://issues.apache.org/jira/browse/CASSANDRA-9731) | PartitionTest.testColumnStatsRecordsRowDeletesCorrectly is flappy |  Major | . | Robert Stupp | Sylvain Lebresne |
| [CASSANDRA-9867](https://issues.apache.org/jira/browse/CASSANDRA-9867) | Removing a column via Thrift is broken for compact static tables |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-8735](https://issues.apache.org/jira/browse/CASSANDRA-8735) | Batch log replication is not randomized when there are only 2 racks |  Minor | . | Yuki Morishita | Mihai Suteu |
| [CASSANDRA-9874](https://issues.apache.org/jira/browse/CASSANDRA-9874) | Compact value columns aren't being migrated properly in 3.0 |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-9841](https://issues.apache.org/jira/browse/CASSANDRA-9841) | trunk pig-test fails |  Major | Testing | Michael Shuler | Aleksey Yeschenko |
| [CASSANDRA-9887](https://issues.apache.org/jira/browse/CASSANDRA-9887) | dtest: user\_functions\_test broken |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-9863](https://issues.apache.org/jira/browse/CASSANDRA-9863) | NIODataInputStream has problems on trunk |  Blocker | Compaction, Local Write-Read Paths, Streaming and Messaging | Sylvain Lebresne | Ariel Weisberg |
| [CASSANDRA-9382](https://issues.apache.org/jira/browse/CASSANDRA-9382) | Snapshot file descriptors not getting purged (possible fd leak) |  Major | Local Write-Read Paths | Mark Curtis | Yuki Morishita |
| [CASSANDRA-9899](https://issues.apache.org/jira/browse/CASSANDRA-9899) | If compaction is disabled in schema, you can't enable a single node through nodetool |  Major | Compaction | Jeremiah Jordan | Marcus Eriksson |
| [CASSANDRA-9865](https://issues.apache.org/jira/browse/CASSANDRA-9865) | Broken vint encoding, at least when interacting with OHCProvider |  Major | Compaction, Local Write-Read Paths, Streaming and Messaging | Sylvain Lebresne | Ariel Weisberg |
| [CASSANDRA-7757](https://issues.apache.org/jira/browse/CASSANDRA-7757) | Possible Atomicity Violations in StreamSession and ThriftSessionManager |  Minor | Streaming and Messaging | Diogo Sousa | Paulo Motta |
| [CASSANDRA-9900](https://issues.apache.org/jira/browse/CASSANDRA-9900) | Anticompaction can mix old and new data with DTCS in 2.2+ |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9307](https://issues.apache.org/jira/browse/CASSANDRA-9307) | test-long contains tests that are not appropriate for running on every commit |  Major | Testing | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-9288](https://issues.apache.org/jira/browse/CASSANDRA-9288) | LongLeveledCompactionStrategyTest is failing |  Major | Testing | Ariel Weisberg | Stefania |
| [CASSANDRA-9561](https://issues.apache.org/jira/browse/CASSANDRA-9561) | Lifecycle unit tests fail when run in parallel |  Major | Local Write-Read Paths, Testing | Stefania | Stefania |
| [CASSANDRA-9463](https://issues.apache.org/jira/browse/CASSANDRA-9463) | ant test-all results incomplete when parsed |  Major | Testing | Michael Shuler | Ariel Weisberg |
| [CASSANDRA-8724](https://issues.apache.org/jira/browse/CASSANDRA-8724) | Move PigTestBase to Java Driver |  Major | Testing | Philip Thompson | Philip Thompson |
| [CASSANDRA-9583](https://issues.apache.org/jira/browse/CASSANDRA-9583) | test-compression could run multiple unit tests in parallel like test |  Major | Testing | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-9346](https://issues.apache.org/jira/browse/CASSANDRA-9346) | Expand upgrade testing for commitlog changes |  Major | Testing | Philip Thompson | Branimir Lambov |
| [CASSANDRA-9528](https://issues.apache.org/jira/browse/CASSANDRA-9528) | Improve log output from unit tests |  Major | Testing | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-9799](https://issues.apache.org/jira/browse/CASSANDRA-9799) | RangeTombstonListTest sometimes fails on trunk |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-9581](https://issues.apache.org/jira/browse/CASSANDRA-9581) | pig-tests spend time waiting on /dev/random for SecureRandom |  Major | Testing | Ariel Weisberg | Ariel Weisberg |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9665](https://issues.apache.org/jira/browse/CASSANDRA-9665) | Improve handling of UDF and UDA metadata |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-9035](https://issues.apache.org/jira/browse/CASSANDRA-9035) | Fix hinted\_handoff\_enabled yaml setting |  Minor | Configuration | Stefania | Stefania |
| [CASSANDRA-8384](https://issues.apache.org/jira/browse/CASSANDRA-8384) | Change CREATE TABLE syntax for compression options |  Major | CQL, Distributed Metadata | Aleksey Yeschenko | Benjamin Lerer |
| [CASSANDRA-9709](https://issues.apache.org/jira/browse/CASSANDRA-9709) | CFMetaData serialization is unnecessarily inefficient |  Trivial | . | Benedict | Benedict |
| [CASSANDRA-9705](https://issues.apache.org/jira/browse/CASSANDRA-9705) | Simplify some of 8099's concrete implementations |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-9847](https://issues.apache.org/jira/browse/CASSANDRA-9847) | Don't serialize CFMetaData in read responses |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-9828](https://issues.apache.org/jira/browse/CASSANDRA-9828) | Minor improvements to RowStats |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-9801](https://issues.apache.org/jira/browse/CASSANDRA-9801) | Use vints where it makes sense |  Major | Compaction, Local Write-Read Paths, Streaming and Messaging | Sylvain Lebresne | Ariel Weisberg |
| [CASSANDRA-9790](https://issues.apache.org/jira/browse/CASSANDRA-9790) | CommitLogUpgradeTest.test{20,21} failure |  Blocker | . | Michael Shuler | Branimir Lambov |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9767](https://issues.apache.org/jira/browse/CASSANDRA-9767) | Allow the selection of columns together with aggregates |  Minor | CQL | Ajay | Benjamin Lerer |
| [CASSANDRA-8168](https://issues.apache.org/jira/browse/CASSANDRA-8168) | Require Java 8 |  Major | Configuration | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-9408](https://issues.apache.org/jira/browse/CASSANDRA-9408) | Update 3.0 debian/control for JDK8 |  Trivial | Packaging | Michael Shuler | Michael Shuler |
| [CASSANDRA-9402](https://issues.apache.org/jira/browse/CASSANDRA-9402) | Implement proper sandboxing for UDFs |  Critical | . | T Jake Luciani | Robert Stupp |


