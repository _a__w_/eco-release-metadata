
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.0.12 - 2012-10-03



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5533](https://issues.apache.org/jira/browse/CASSANDRA-5533) | explicitly set javac source/target attributes to 1.6 (for 1.0/1.1/1.2) |  Trivial | Tools | Dave Brosius | Dave Brosius |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4466](https://issues.apache.org/jira/browse/CASSANDRA-4466) | ColumnFamilyRecordReader hadoop integration fails with ghost keys |  Minor | . | Niel Drummond | Jonathan Ellis |
| [CASSANDRA-4534](https://issues.apache.org/jira/browse/CASSANDRA-4534) | old-style mapred interface doesn't set key limit correctly |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4574](https://issues.apache.org/jira/browse/CASSANDRA-4574) | Missing String.format() in AntiEntropyService.java logs |  Trivial | . | Julien Lambert | Dave Brosius |
| [CASSANDRA-4568](https://issues.apache.org/jira/browse/CASSANDRA-4568) | countPendingHints JMX operation is returning garbage for the key |  Minor | . | J.B. Langston | Brandon Williams |
| [CASSANDRA-4631](https://issues.apache.org/jira/browse/CASSANDRA-4631) | minimum stack size for u34 and later is 180k |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-4626](https://issues.apache.org/jira/browse/CASSANDRA-4626) | Multiple values for CurrentLocal Node ID |  Major | . | amorton | Sylvain Lebresne |
| [CASSANDRA-4708](https://issues.apache.org/jira/browse/CASSANDRA-4708) | StorageProxy slow-down and memory leak |  Major | . | Daniel Norberg | Daniel Norberg |
| [CASSANDRA-4602](https://issues.apache.org/jira/browse/CASSANDRA-4602) | Stack Size on Sun JVM 1.6.0\_33 must be at least 160k |  Major | . | amorton | Jonathan Ellis |


