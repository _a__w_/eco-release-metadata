
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.2.0 beta 1 - 2015-05-19



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7395](https://issues.apache.org/jira/browse/CASSANDRA-7395) | Support for pure user-defined functions (UDF) |  Major | CQL | Jonathan Ellis | Robert Stupp |
| [CASSANDRA-7562](https://issues.apache.org/jira/browse/CASSANDRA-7562) | Java source code for UDFs |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-7069](https://issues.apache.org/jira/browse/CASSANDRA-7069) | Prevent operator mistakes due to simultaneous bootstrap |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-4914](https://issues.apache.org/jira/browse/CASSANDRA-4914) | Aggregation functions in CQL |  Major | CQL | Vijay | Benjamin Lerer |
| [CASSANDRA-7526](https://issues.apache.org/jira/browse/CASSANDRA-7526) | Defining UDFs using scripting language directly from CQL |  Major | . | Sylvain Lebresne | Robert Stupp |
| [CASSANDRA-8053](https://issues.apache.org/jira/browse/CASSANDRA-8053) | Support for user defined aggregate functions |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-7536](https://issues.apache.org/jira/browse/CASSANDRA-7536) | High resolution types for timestamp and time |  Minor | . | Robert Stupp |  |
| [CASSANDRA-8592](https://issues.apache.org/jira/browse/CASSANDRA-8592) | Add WriteFailureException |  Major | CQL | Tyler Hobbs | Stefania |
| [CASSANDRA-5791](https://issues.apache.org/jira/browse/CASSANDRA-5791) | A nodetool command to validate all sstables in a node |  Minor | . | sankalp kohli | Jeff Jirsa |
| [CASSANDRA-7970](https://issues.apache.org/jira/browse/CASSANDRA-7970) | JSON support for CQL |  Major | CQL | Jonathan Ellis | Tyler Hobbs |
| [CASSANDRA-9212](https://issues.apache.org/jira/browse/CASSANDRA-9212) | QueryHandler to return custom payloads for testing |  Minor | . | Adam Holmberg | Adam Holmberg |
| [CASSANDRA-5839](https://issues.apache.org/jira/browse/CASSANDRA-5839) | Save repair data to system table |  Minor | Tools | Jonathan Ellis | Marcus Eriksson |
| [CASSANDRA-8967](https://issues.apache.org/jira/browse/CASSANDRA-8967) | Allow RolesCache to be invalidated |  Major | . | Brandon Williams | Brandon Williams |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5918](https://issues.apache.org/jira/browse/CASSANDRA-5918) | Remove CQL2 entirely from Cassandra 3.0 |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6933](https://issues.apache.org/jira/browse/CASSANDRA-6933) | Optimise Read Comparison Costs in collectTimeOrderedData |  Minor | . | Benedict | Benedict |
| [CASSANDRA-6962](https://issues.apache.org/jira/browse/CASSANDRA-6962) | examine shortening path length post-5202 |  Major | Local Write-Read Paths | Brandon Williams | Yuki Morishita |
| [CASSANDRA-7675](https://issues.apache.org/jira/browse/CASSANDRA-7675) | Prevent javac warning '\_' used as an identifier |  Trivial | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-7690](https://issues.apache.org/jira/browse/CASSANDRA-7690) | build.xml - exclude \*.ps1 from fixcrlf |  Trivial | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-6851](https://issues.apache.org/jira/browse/CASSANDRA-6851) | Improve anticompaction after incremental repair |  Minor | . | Marcus Eriksson | Russell Spitzer |
| [CASSANDRA-7811](https://issues.apache.org/jira/browse/CASSANDRA-7811) | Add USING to CREATE FUNCTION syntax |  Trivial | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-7812](https://issues.apache.org/jira/browse/CASSANDRA-7812) | DROP FUNCTION drops all function with a given name |  Major | . | Sylvain Lebresne | Robert Stupp |
| [CASSANDRA-7883](https://issues.apache.org/jira/browse/CASSANDRA-7883) | Allow plugging JEMalloc for off-heap memtables |  Major | . | Jay Patel | Jay Patel |
| [CASSANDRA-7901](https://issues.apache.org/jira/browse/CASSANDRA-7901) | Implement -f functionality in stop-server.bat |  Minor | . | Philip Thompson | Philip Thompson |
| [CASSANDRA-7748](https://issues.apache.org/jira/browse/CASSANDRA-7748) | Get Windows command-line flags in-line with linux |  Trivial | Packaging | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-7818](https://issues.apache.org/jira/browse/CASSANDRA-7818) | Improve compaction logging |  Minor | . | Marcus Eriksson | Mihai Suteu |
| [CASSANDRA-4762](https://issues.apache.org/jira/browse/CASSANDRA-4762) | Support IN clause for any clustering column |  Major | CQL | T Jake Luciani | Benjamin Lerer |
| [CASSANDRA-7935](https://issues.apache.org/jira/browse/CASSANDRA-7935) | Make assassinate a first class command |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-7769](https://issues.apache.org/jira/browse/CASSANDRA-7769) | Implement pg-style dollar syntax for string constants |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-7920](https://issues.apache.org/jira/browse/CASSANDRA-7920) | Remove cassandra-cli in Cassandra 3.0 |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-8011](https://issues.apache.org/jira/browse/CASSANDRA-8011) | Fail on large batch sizes |  Minor | Configuration | Patrick McFadin | Carl Yeksigian |
| [CASSANDRA-7929](https://issues.apache.org/jira/browse/CASSANDRA-7929) | Improve CAS propose CQL query |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-8046](https://issues.apache.org/jira/browse/CASSANDRA-8046) | Set base C\* version in debs and strip -N, ~textN, +textN |  Trivial | Packaging | Michael Shuler | Michael Shuler |
| [CASSANDRA-7316](https://issues.apache.org/jira/browse/CASSANDRA-7316) | Windows: address potential JVM swapping |  Minor | Lifecycle | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-7838](https://issues.apache.org/jira/browse/CASSANDRA-7838) | Warn user when OS settings are poor / integrate sigar |  Minor | . | T Jake Luciani | AMIT KUMAR |
| [CASSANDRA-6455](https://issues.apache.org/jira/browse/CASSANDRA-6455) | Improve concurrency of repair process |  Minor | Streaming and Messaging | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-7460](https://issues.apache.org/jira/browse/CASSANDRA-7460) | Send source sstable level when bootstrapping or replacing nodes |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-7692](https://issues.apache.org/jira/browse/CASSANDRA-7692) | Upgrade Cassandra Java Driver to 2.1 |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-7820](https://issues.apache.org/jira/browse/CASSANDRA-7820) | Remove fat client mode |  Major | . | Jonathan Ellis | Brandon Williams |
| [CASSANDRA-7997](https://issues.apache.org/jira/browse/CASSANDRA-7997) | Improve the ability to run the CassandraDaemon as a managed service |  Minor | . | Heiko Braun | Heiko Braun |
| [CASSANDRA-8151](https://issues.apache.org/jira/browse/CASSANDRA-8151) | Add build support for JMH microbenchmarks |  Minor | Testing | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-7443](https://issues.apache.org/jira/browse/CASSANDRA-7443) | Extend Descriptor to include a format value and refactor reader/writer apis |  Major | Local Write-Read Paths | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-7586](https://issues.apache.org/jira/browse/CASSANDRA-7586) | Mark SSTables as repaired after full repairs |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-7979](https://issues.apache.org/jira/browse/CASSANDRA-7979) | Acceptable time skew for C\* |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-8305](https://issues.apache.org/jira/browse/CASSANDRA-8305) | add check of the system wall clock time at startup |  Minor | . | Jason Brown | Jason Brown |
| [CASSANDRA-7813](https://issues.apache.org/jira/browse/CASSANDRA-7813) | Decide how to deal with conflict between native and user-defined functions |  Major | . | Sylvain Lebresne | Robert Stupp |
| [CASSANDRA-7827](https://issues.apache.org/jira/browse/CASSANDRA-7827) | Work around for output name restriction when using MultipleOutputs with CqlBulkOutputFormat |  Minor | . | Paul Pak | Paul Pak |
| [CASSANDRA-5483](https://issues.apache.org/jira/browse/CASSANDRA-5483) | Repair tracing |  Minor | Tools | Yuki Morishita | Ben Chan |
| [CASSANDRA-6993](https://issues.apache.org/jira/browse/CASSANDRA-6993) | Windows: remove mmap'ed I/O for index files and force standard file access |  Minor | Local Write-Read Paths | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-8261](https://issues.apache.org/jira/browse/CASSANDRA-8261) | Clean up schema metadata classes |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-8473](https://issues.apache.org/jira/browse/CASSANDRA-8473) | Secondary index support for key-value pairs in CQL3 maps |  Major | Secondary Indexes | Samuel Klock | Samuel Klock |
| [CASSANDRA-7886](https://issues.apache.org/jira/browse/CASSANDRA-7886) | Coordinator should not wait for read timeouts when replicas hit Exceptions |  Minor | . | Christian Spriegel | Christian Spriegel |
| [CASSANDRA-7039](https://issues.apache.org/jira/browse/CASSANDRA-7039) | DirectByteBuffer compatible LZ4 methods |  Minor | . | Benedict | Branimir Lambov |
| [CASSANDRA-8464](https://issues.apache.org/jira/browse/CASSANDRA-8464) | Support direct buffer decompression for reads |  Major | Local Write-Read Paths | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-8560](https://issues.apache.org/jira/browse/CASSANDRA-8560) | Make CassandraException be an unchecked exception |  Minor | . | Sylvain Lebresne | Robert Stupp |
| [CASSANDRA-7438](https://issues.apache.org/jira/browse/CASSANDRA-7438) | Serializing Row cache alternative (Fully off heap) |  Major | . | Vijay | Robert Stupp |
| [CASSANDRA-7705](https://issues.apache.org/jira/browse/CASSANDRA-7705) | Safer Resource Management |  Major | . | Benedict | Benedict |
| [CASSANDRA-7855](https://issues.apache.org/jira/browse/CASSANDRA-7855) | Genralize use of IN for compound partition keys |  Minor | CQL | Sylvain Lebresne | Benjamin Lerer |
| [CASSANDRA-7096](https://issues.apache.org/jira/browse/CASSANDRA-7096) | Evaluate murmur hash of token only once |  Minor | . | Benedict | Branimir Lambov |
| [CASSANDRA-8836](https://issues.apache.org/jira/browse/CASSANDRA-8836) | Factor out CRC32Ex into a separate maven dependency |  Major | Configuration, Packaging | Ariel Weisberg | T Jake Luciani |
| [CASSANDRA-8730](https://issues.apache.org/jira/browse/CASSANDRA-8730) | Optimize UUIDType comparisons |  Major | . | J.B. Langston | Benedict |
| [CASSANDRA-8793](https://issues.apache.org/jira/browse/CASSANDRA-8793) | Avoid memory allocation when searching index summary |  Minor | . | Benedict | Benedict |
| [CASSANDRA-8375](https://issues.apache.org/jira/browse/CASSANDRA-8375) | Cleanup of generics in bounds serialization |  Trivial | . | Branimir Lambov | Branimir Lambov |
| [CASSANDRA-8759](https://issues.apache.org/jira/browse/CASSANDRA-8759) | Make TimeUUIDType extend UUIDType |  Trivial | . | Benedict | Benedict |
| [CASSANDRA-8901](https://issues.apache.org/jira/browse/CASSANDRA-8901) | Generalize progress reporting between tools and a server |  Minor | Observability, Tools | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-8162](https://issues.apache.org/jira/browse/CASSANDRA-8162) | Log client address in query trace |  Minor | CQL | Tyler Hobbs | Stefania |
| [CASSANDRA-8709](https://issues.apache.org/jira/browse/CASSANDRA-8709) | Convert SequentialWriter from using RandomAccessFile to nio channel |  Major | Local Write-Read Paths | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-8972](https://issues.apache.org/jira/browse/CASSANDRA-8972) | Make EchoMessage a singleton |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-8976](https://issues.apache.org/jira/browse/CASSANDRA-8976) | Remove BINARY from DROPPABLE\_VERBS |  Trivial | . | Chris Lohfink | Dave Brosius |
| [CASSANDRA-8980](https://issues.apache.org/jira/browse/CASSANDRA-8980) | UDF shouldn't log errors at ERROR |  Trivial | . | Sylvain Lebresne | Robert Stupp |
| [CASSANDRA-8936](https://issues.apache.org/jira/browse/CASSANDRA-8936) | Remove IRequestSink, clean up IMessageSink use |  Trivial | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-9015](https://issues.apache.org/jira/browse/CASSANDRA-9015) | Add eml files to gitignore |  Trivial | Tools | Patrick Abeya | Patrick Abeya |
| [CASSANDRA-8149](https://issues.apache.org/jira/browse/CASSANDRA-8149) | bump metrics-reporter-config dependency |  Major | Configuration, Packaging | Pierre-Yves Ritschard | T Jake Luciani |
| [CASSANDRA-8988](https://issues.apache.org/jira/browse/CASSANDRA-8988) | Optimise IntervalTree |  Minor | . | Benedict | Benedict |
| [CASSANDRA-6809](https://issues.apache.org/jira/browse/CASSANDRA-6809) | Compressed Commit Log |  Minor | . | Benedict | Branimir Lambov |
| [CASSANDRA-8729](https://issues.apache.org/jira/browse/CASSANDRA-8729) | Commitlog causes read before write when overwriting |  Major | . | Ariel Weisberg |  |
| [CASSANDRA-8236](https://issues.apache.org/jira/browse/CASSANDRA-8236) | Delay "node up" and "node added" notifications until native protocol server is started |  Major | CQL | Tyler Hobbs | Stefania |
| [CASSANDRA-8920](https://issues.apache.org/jira/browse/CASSANDRA-8920) | Optimise sequential overlap visitation for checking tombstone retention in compaction |  Minor | . | Benedict | Benedict |
| [CASSANDRA-8359](https://issues.apache.org/jira/browse/CASSANDRA-8359) | Make DTCS consider removing SSTables much more frequently |  Minor | . | Björn Hegerfors | Björn Hegerfors |
| [CASSANDRA-7272](https://issues.apache.org/jira/browse/CASSANDRA-7272) | Add "Major" Compaction to LCS and split sstables during STCS major compaction |  Minor | . | T Jake Luciani | Marcus Eriksson |
| [CASSANDRA-8893](https://issues.apache.org/jira/browse/CASSANDRA-8893) | RandomAccessReader should share its FileChannel with all instances (via SegmentedFile) |  Major | Local Write-Read Paths | Benedict | Stefania |
| [CASSANDRA-8952](https://issues.apache.org/jira/browse/CASSANDRA-8952) | Remove transient RandomAccessFile usage |  Minor | Local Write-Read Paths | Joshua McKenzie | Stefania |
| [CASSANDRA-9029](https://issues.apache.org/jira/browse/CASSANDRA-9029) | Add utility class to support for rate limiting a given log statement |  Major | Observability | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-9112](https://issues.apache.org/jira/browse/CASSANDRA-9112) | Remove ternary construction of SegmentedFile.Builder in readers |  Trivial | . | Benedict | Benedict |
| [CASSANDRA-8789](https://issues.apache.org/jira/browse/CASSANDRA-8789) | OutboundTcpConnectionPool should route messages to sockets by size not type |  Major | Streaming and Messaging | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-9123](https://issues.apache.org/jira/browse/CASSANDRA-9123) | Add generate-idea-files target to build.xml |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-9046](https://issues.apache.org/jira/browse/CASSANDRA-9046) | Allow Cassandra config to be updated to restart Daemon without unloading classes |  Major | Configuration | Emmanuel Hugonnet | Emmanuel Hugonnet |
| [CASSANDRA-9176](https://issues.apache.org/jira/browse/CASSANDRA-9176) | drop out of column finding loop on success for altertable statement w/ drop column |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-8872](https://issues.apache.org/jira/browse/CASSANDRA-8872) | fix nodetool names that reference column families |  Trivial | . | Jon Haddad | Dave Brosius |
| [CASSANDRA-9262](https://issues.apache.org/jira/browse/CASSANDRA-9262) | IncomingTcpConnection thread is not named |  Trivial | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-9242](https://issues.apache.org/jira/browse/CASSANDRA-9242) | Add PerfDisableSharedMem to default JVM params |  Major | Configuration | Matt Stump | Ariel Weisberg |
| [CASSANDRA-9261](https://issues.apache.org/jira/browse/CASSANDRA-9261) | Prepare and Snapshot for repairs should use higher timeouts for expiring map |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-8358](https://issues.apache.org/jira/browse/CASSANDRA-8358) | Bundled tools shouldn't be using Thrift API |  Major | . | Aleksey Yeschenko | Philip Thompson |
| [CASSANDRA-9298](https://issues.apache.org/jira/browse/CASSANDRA-9298) | CollationController.collectAllData can be too conservative with sstable elimination |  Trivial | . | Benedict | Benedict |
| [CASSANDRA-9314](https://issues.apache.org/jira/browse/CASSANDRA-9314) | Overload SecondaryIndex#indexes to accept the column definition |  Major | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-9322](https://issues.apache.org/jira/browse/CASSANDRA-9322) | Possible overlap with LCS and including non-compacting sstables |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9063](https://issues.apache.org/jira/browse/CASSANDRA-9063) | Upgrade snappy library |  Minor | Configuration | Tyler Hobbs | T Jake Luciani |
| [CASSANDRA-9368](https://issues.apache.org/jira/browse/CASSANDRA-9368) | Remove MessagingService.allNodesAtLeast21 and related digest logic |  Trivial | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-9219](https://issues.apache.org/jira/browse/CASSANDRA-9219) | Follow-ups to 7523: protect old clients from new type values and reject empty ByteBuffer |  Minor | CQL | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-7212](https://issues.apache.org/jira/browse/CASSANDRA-7212) | Allow to switch user within CQLSH session |  Major | CQL | Jose Martinez Poblete | Carl Yeksigian |
| [CASSANDRA-8374](https://issues.apache.org/jira/browse/CASSANDRA-8374) | Better support of null for UDF |  Major | . | Sylvain Lebresne | Robert Stupp |
| [CASSANDRA-9214](https://issues.apache.org/jira/browse/CASSANDRA-9214) | Add Data Type Serialization information for date and time types to v4 protocol spec |  Trivial | Documentation and Website | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-9183](https://issues.apache.org/jira/browse/CASSANDRA-9183) | Failure detector should detect and ignore local pauses |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-8771](https://issues.apache.org/jira/browse/CASSANDRA-8771) | Remove commit log segment recycling |  Major | Local Write-Read Paths | Ariel Weisberg | Ariel Weisberg |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4050](https://issues.apache.org/jira/browse/CASSANDRA-4050) | Rewrite RandomAccessReader to use FileChannel / nio to address Windows file access violations |  Minor | Local Write-Read Paths | Jim Newsham | Joshua McKenzie |
| [CASSANDRA-6283](https://issues.apache.org/jira/browse/CASSANDRA-6283) | Windows 7 data files kept open / can't be deleted after compaction. |  Major | Compaction, Local Write-Read Paths | Andreas Schnitzerling | Joshua McKenzie |
| [CASSANDRA-7250](https://issues.apache.org/jira/browse/CASSANDRA-7250) | Make incremental repair default in 3.0 |  Minor | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-7279](https://issues.apache.org/jira/browse/CASSANDRA-7279) | MultiSliceTest.test\_with\_overlap\* unit tests failing in trunk |  Minor | Testing | Michael Shuler | Sylvain Lebresne |
| [CASSANDRA-7060](https://issues.apache.org/jira/browse/CASSANDRA-7060) | Clean up unit tests on Windows |  Minor | Testing | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-7397](https://issues.apache.org/jira/browse/CASSANDRA-7397) | BatchlogManagerTest unit test failing |  Major | Testing | Michael Shuler | Carl Yeksigian |
| [CASSANDRA-7480](https://issues.apache.org/jira/browse/CASSANDRA-7480) | CommitLogTest.testCommitFailurePolicy\_stop Fails w/ NPE |  Major | Testing | Caleb Rackliffe |  |
| [CASSANDRA-7390](https://issues.apache.org/jira/browse/CASSANDRA-7390) | MoveTest fails intermittently |  Minor | Distributed Metadata, Testing | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-7760](https://issues.apache.org/jira/browse/CASSANDRA-7760) | Fix unit tests in trunk that didn't define their own schemas |  Minor | Testing | Lyuben Todorov | Lyuben Todorov |
| [CASSANDRA-7430](https://issues.apache.org/jira/browse/CASSANDRA-7430) | generate-eclipse-files target on JDK 8 causes ReferenceError: "importClass" is not defined |  Trivial | . | Kirk True |  |
| [CASSANDRA-7781](https://issues.apache.org/jira/browse/CASSANDRA-7781) | UDF class methods are not verified to be static |  Minor | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-7740](https://issues.apache.org/jira/browse/CASSANDRA-7740) | Parsing of UDF body is broken |  Major | . | Sylvain Lebresne | Robert Stupp |
| [CASSANDRA-7671](https://issues.apache.org/jira/browse/CASSANDRA-7671) | cqlsh: Error when printing results of conditional updates |  Major | Tools | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-7809](https://issues.apache.org/jira/browse/CASSANDRA-7809) | UDF cleanups (#7395 follow-up) |  Major | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-7917](https://issues.apache.org/jira/browse/CASSANDRA-7917) | deprecate the yaml snitch |  Trivial | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-6926](https://issues.apache.org/jira/browse/CASSANDRA-6926) | StorageService exposes two different objects for probability tracing |  Minor | . | Nicolas Favre-Felix | Nitzan Volman |
| [CASSANDRA-7891](https://issues.apache.org/jira/browse/CASSANDRA-7891) | Select an element inside a UDT throws an index error |  Major | . | Patrick McFadin | Philip Thompson |
| [CASSANDRA-7991](https://issues.apache.org/jira/browse/CASSANDRA-7991) | RowIndexEntryTest and SelectWithTokenFunctionTest fail in trunk |  Major | Testing | Carl Yeksigian | Carl Yeksigian |
| [CASSANDRA-7924](https://issues.apache.org/jira/browse/CASSANDRA-7924) | Optimization of Java UDFs |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-7986](https://issues.apache.org/jira/browse/CASSANDRA-7986) | The Pig tests cannot run on Cygwin on Windows |  Minor | Testing | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-8045](https://issues.apache.org/jira/browse/CASSANDRA-8045) | Current check for cygwin environment in cassandra.ps1 is too greedy |  Trivial | Packaging | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-8044](https://issues.apache.org/jira/browse/CASSANDRA-8044) | stop-server.bat fails to call powershell if there's a space in the directory name |  Trivial | Packaging | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-8063](https://issues.apache.org/jira/browse/CASSANDRA-8063) | Consider removing UDF-as-class functionality |  Major | . | Sylvain Lebresne | Robert Stupp |
| [CASSANDRA-8090](https://issues.apache.org/jira/browse/CASSANDRA-8090) | NullPointerException when using prepared statements |  Major | CQL | Carl Yeksigian | Benjamin Lerer |
| [CASSANDRA-8229](https://issues.apache.org/jira/browse/CASSANDRA-8229) | Aggregate functions do not return expected values or metadata |  Major | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-8291](https://issues.apache.org/jira/browse/CASSANDRA-8291) | Parent repair session is not removed in remote node |  Minor | Streaming and Messaging | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-8208](https://issues.apache.org/jira/browse/CASSANDRA-8208) | Inconsistent failure handling with repair |  Major | Streaming and Messaging | Marcus Eriksson | Yuki Morishita |
| [CASSANDRA-8309](https://issues.apache.org/jira/browse/CASSANDRA-8309) | Windows: RepairOptionTest fails on trunk |  Trivial | Testing | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-8244](https://issues.apache.org/jira/browse/CASSANDRA-8244) | Token, DecoratedKey, RowPosition and all bound types should not make any hidden references to the database partitioner |  Minor | . | Branimir Lambov | Branimir Lambov |
| [CASSANDRA-7563](https://issues.apache.org/jira/browse/CASSANDRA-7563) | UserType, TupleType and collections in UDFs |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-8392](https://issues.apache.org/jira/browse/CASSANDRA-8392) | CQLTester buggy after CASSANDRA-7563 |  Major | Testing | Robert Stupp | Robert Stupp |
| [CASSANDRA-7981](https://issues.apache.org/jira/browse/CASSANDRA-7981) | Refactor SelectStatement |  Major | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-8426](https://issues.apache.org/jira/browse/CASSANDRA-8426) | CollectionsTest has some commented tests |  Trivial | Testing | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-8419](https://issues.apache.org/jira/browse/CASSANDRA-8419) | NPE in SelectStatement |  Major | CQL | Philip Thompson | Benjamin Lerer |
| [CASSANDRA-7873](https://issues.apache.org/jira/browse/CASSANDRA-7873) | Replace AbstractRowResolver.replies with collection with tailored properties |  Major | . | Philip Thompson | Benedict |
| [CASSANDRA-8326](https://issues.apache.org/jira/browse/CASSANDRA-8326) | Cqlsh cannot connect in trunk |  Major | . | Philip Thompson | Tyler Hobbs |
| [CASSANDRA-8427](https://issues.apache.org/jira/browse/CASSANDRA-8427) | An error is written to System.out when UserTypesTest is run |  Minor | Testing | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-7016](https://issues.apache.org/jira/browse/CASSANDRA-7016) | Allow mixing token and partition key restrictions |  Minor | CQL | Jonathan Halliday | Benjamin Lerer |
| [CASSANDRA-8578](https://issues.apache.org/jira/browse/CASSANDRA-8578) | LeveledCompactionStrategyTest.testGrouperLevels failings |  Minor | Testing | Philip Thompson | Marcus Eriksson |
| [CASSANDRA-8598](https://issues.apache.org/jira/browse/CASSANDRA-8598) | Windows - SSTableRewriterTest fails on trunk |  Minor | Local Write-Read Paths, Testing | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-6706](https://issues.apache.org/jira/browse/CASSANDRA-6706) | Duplicate rows returned when in clause has repeated values |  Major | CQL | Gavin Casey | Benjamin Lerer |
| [CASSANDRA-8677](https://issues.apache.org/jira/browse/CASSANDRA-8677) | rpc\_interface and listen\_interface generate NPE on startup when specified interface doesn't exist |  Major | Configuration | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-8702](https://issues.apache.org/jira/browse/CASSANDRA-8702) | LatencyMetrics is reporting total latency in nanoseconds rather than microseconds |  Major | Observability | Mike Adamson | T Jake Luciani |
| [CASSANDRA-8268](https://issues.apache.org/jira/browse/CASSANDRA-8268) | Token serialization should not assume all tokens are created by the database partitioner |  Minor | . | Branimir Lambov | Branimir Lambov |
| [CASSANDRA-8293](https://issues.apache.org/jira/browse/CASSANDRA-8293) | Restore Commitlogs throws exception on startup on trunk |  Major | Lifecycle, Local Write-Read Paths | Philip Thompson | Joshua McKenzie |
| [CASSANDRA-8308](https://issues.apache.org/jira/browse/CASSANDRA-8308) | Windows: Commitlog access violations on unit tests |  Minor | Local Write-Read Paths, Testing | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-7737](https://issues.apache.org/jira/browse/CASSANDRA-7737) | Update CQL doc for UDF |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-8418](https://issues.apache.org/jira/browse/CASSANDRA-8418) | Queries that require allow filtering are working without it |  Minor | CQL | Philip Thompson | Benjamin Lerer |
| [CASSANDRA-8853](https://issues.apache.org/jira/browse/CASSANDRA-8853) | adding existing table at node startup |  Major | Distributed Metadata, Lifecycle | Jim Witschey | Sam Tunnicliffe |
| [CASSANDRA-8540](https://issues.apache.org/jira/browse/CASSANDRA-8540) | CQL: Describe table does not show index interval properties set on create. |  Major | . | Rekha Joshi | Rekha Joshi |
| [CASSANDRA-9065](https://issues.apache.org/jira/browse/CASSANDRA-9065) | SSTableExportTest can fail due broken assertion |  Major | Testing | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-9072](https://issues.apache.org/jira/browse/CASSANDRA-9072) | MessagePayloadTest.resetCqlQueryHandlerField fails on Java 7 |  Major | . | Ariel Weisberg | Robert Stupp |
| [CASSANDRA-9055](https://issues.apache.org/jira/browse/CASSANDRA-9055) | FunctionExecutionException results in error log about unexpected error |  Major | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-8276](https://issues.apache.org/jira/browse/CASSANDRA-8276) | Duplicate values in an IN restriction on the partition key column can break paging |  Major | . | Pierre Laporte | Benjamin Lerer |
| [CASSANDRA-9082](https://issues.apache.org/jira/browse/CASSANDRA-9082) | sstableloader error on trunk due to loading read meter |  Major | Tools | Tyler Hobbs | Benedict |
| [CASSANDRA-9037](https://issues.apache.org/jira/browse/CASSANDRA-9037) | Terminal UDFs evaluated at prepare time throw protocol version error |  Major | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-9099](https://issues.apache.org/jira/browse/CASSANDRA-9099) | Validation compaction not working for parallel repair |  Major | Streaming and Messaging | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-8670](https://issues.apache.org/jira/browse/CASSANDRA-8670) | Large columns + NIO memory pooling causes excessive direct memory usage |  Major | Compaction, Local Write-Read Paths, Streaming and Messaging | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-8992](https://issues.apache.org/jira/browse/CASSANDRA-8992) | CommitLogTest hangs intermittently |  Major | Testing | Michael Shuler | Branimir Lambov |
| [CASSANDRA-9019](https://issues.apache.org/jira/browse/CASSANDRA-9019) | GCInspector detected GC before ThreadPools are initialized |  Major | Tools | Philip Thompson | Yuki Morishita |
| [CASSANDRA-8962](https://issues.apache.org/jira/browse/CASSANDRA-8962) | Fix SSTableRewriterTest on Windows |  Trivial | Testing, Tools | Joshua McKenzie | Stefania |
| [CASSANDRA-9117](https://issues.apache.org/jira/browse/CASSANDRA-9117) | LEAK DETECTED during repair, startup |  Major | . | Tyler Hobbs | Marcus Eriksson |
| [CASSANDRA-9134](https://issues.apache.org/jira/browse/CASSANDRA-9134) | Fix leak detected errors in unit tests |  Minor | Local Write-Read Paths, Testing | Stefania | Stefania |
| [CASSANDRA-9062](https://issues.apache.org/jira/browse/CASSANDRA-9062) | Investigate failing collection indexing dtests |  Major | Testing | Tyler Hobbs | Sam Tunnicliffe |
| [CASSANDRA-9225](https://issues.apache.org/jira/browse/CASSANDRA-9225) | CompactionAwareWriterTest is broken with compression |  Trivial | . | Sylvain Lebresne | Marcus Eriksson |
| [CASSANDRA-9239](https://issues.apache.org/jira/browse/CASSANDRA-9239) | OHCProvider doesn't write ByteBuffers with non-zero position correctly |  Minor | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-9138](https://issues.apache.org/jira/browse/CASSANDRA-9138) | BlacklistingCompactionsTest failing on test-compression target |  Major | Testing | Philip Thompson | Marcus Eriksson |
| [CASSANDRA-9235](https://issues.apache.org/jira/browse/CASSANDRA-9235) | Max sstable size in leveled manifest is an int, creating large sstables overflows this and breaks LCS |  Major | Compaction | Sergey Maznichenko | Marcus Eriksson |
| [CASSANDRA-9238](https://issues.apache.org/jira/browse/CASSANDRA-9238) | Race condition after shutdown gossip message |  Minor | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-9124](https://issues.apache.org/jira/browse/CASSANDRA-9124) | GCInspector logs very different times after CASSANDRA-7638 |  Minor | Observability | Jeremiah Jordan | Ariel Weisberg |
| [CASSANDRA-9234](https://issues.apache.org/jira/browse/CASSANDRA-9234) | Disable single-sstable tombstone compactions for DTCS |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9249](https://issues.apache.org/jira/browse/CASSANDRA-9249) | Resetting local schema can cause assertion error |  Minor | Distributed Metadata | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-9201](https://issues.apache.org/jira/browse/CASSANDRA-9201) | Race condition on creating system\_auth.roles and using it on startup |  Major | Distributed Metadata | Tyler Hobbs | Sam Tunnicliffe |
| [CASSANDRA-9189](https://issues.apache.org/jira/browse/CASSANDRA-9189) | DROP ROLE shouldn't cache information about non-existent roles |  Major | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-9166](https://issues.apache.org/jira/browse/CASSANDRA-9166) | Prepared statements using functions in collection literals aren't invalidated when functions are dropped |  Major | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-9151](https://issues.apache.org/jira/browse/CASSANDRA-9151) | Anti-compaction is blocking ANTI\_ENTROPY stage |  Major | Streaming and Messaging | sankalp kohli | Yuki Morishita |
| [CASSANDRA-9260](https://issues.apache.org/jira/browse/CASSANDRA-9260) | Threadpool in RepairSession is not shutdown on failure |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-9290](https://issues.apache.org/jira/browse/CASSANDRA-9290) | Fix possible NPE in Scrubber |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-8049](https://issues.apache.org/jira/browse/CASSANDRA-8049) | Explicitly examine current C\* state on startup to detect incompatibilities before upgrade |  Major | Lifecycle | Aleksey Yeschenko | Sam Tunnicliffe |
| [CASSANDRA-9335](https://issues.apache.org/jira/browse/CASSANDRA-9335) | JVM\_EXTRA\_OPTS not getting picked up by windows startup environment |  Major | Packaging | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-9321](https://issues.apache.org/jira/browse/CASSANDRA-9321) | Aggregate UDFs allow SFUNC return type to differ from STYPE if FFUNC specified |  Major | . | Zachary Kurey | Robert Stupp |
| [CASSANDRA-8267](https://issues.apache.org/jira/browse/CASSANDRA-8267) | Only stream from unrepaired sstables during incremental repair |  Major | Streaming and Messaging | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9197](https://issues.apache.org/jira/browse/CASSANDRA-9197) | Startup slowdown due to preloading jemalloc |  Minor | . | Sylvain Lebresne | Robert Stupp |
| [CASSANDRA-9145](https://issues.apache.org/jira/browse/CASSANDRA-9145) | First RepairJob unregisters RepairSession from FailureDetector |  Major | Streaming and Messaging | sankalp kohli | Yuki Morishita |
| [CASSANDRA-9190](https://issues.apache.org/jira/browse/CASSANDRA-9190) | Map keys aren't properly serialized as strings in SELECT JSON queries |  Major | CQL | Gianluca Righetto | Tyler Hobbs |
| [CASSANDRA-9300](https://issues.apache.org/jira/browse/CASSANDRA-9300) | token-generator - generated tokens too long |  Minor | Tools | Robert Stupp | Robert Stupp |
| [CASSANDRA-9352](https://issues.apache.org/jira/browse/CASSANDRA-9352) | Update all references to 3.0 for 2.2 release |  Minor | Packaging | Aleksey Yeschenko | Sam Tunnicliffe |
| [CASSANDRA-9383](https://issues.apache.org/jira/browse/CASSANDRA-9383) | isPure is no longer used in Functions |  Major | CQL | Carl Yeksigian | Carl Yeksigian |
| [CASSANDRA-9366](https://issues.apache.org/jira/browse/CASSANDRA-9366) | make Functions.declared thread-safe |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-9380](https://issues.apache.org/jira/browse/CASSANDRA-9380) | ColumnFamilySplit length field is not serialized |  Major | . | Mike Adamson | Philip Thompson |
| [CASSANDRA-9390](https://issues.apache.org/jira/browse/CASSANDRA-9390) | AbstractColumnFamilyInputFormat doesn't work with mixed case keyspaces |  Major | . | Mike Adamson | Philip Thompson |
| [CASSANDRA-9381](https://issues.apache.org/jira/browse/CASSANDRA-9381) | AbstractColumnFamilyInputFormat is incorrectly interpreting token values when building ColumnFamilySplit |  Major | . | Mike Adamson | Philip Thompson |
| [CASSANDRA-9374](https://issues.apache.org/jira/browse/CASSANDRA-9374) | Remove thrift dependency in stress schema creation |  Minor | Tools | Ryan McGuire | T Jake Luciani |
| [CASSANDRA-9097](https://issues.apache.org/jira/browse/CASSANDRA-9097) | Repeated incremental nodetool repair results in failed repairs due to running anticompaction |  Minor | Streaming and Messaging | Gustav Munkby | Yuki Morishita |
| [CASSANDRA-9396](https://issues.apache.org/jira/browse/CASSANDRA-9396) | Canonical view of compacting SSTables not working as expected |  Major | . | Yuki Morishita | Benedict |
| [CASSANDRA-9295](https://issues.apache.org/jira/browse/CASSANDRA-9295) | Streaming not holding on to refs long enough. |  Major | Streaming and Messaging | Jeremiah Jordan | Yuki Morishita |
| [CASSANDRA-9405](https://issues.apache.org/jira/browse/CASSANDRA-9405) | AssertionError: Data component is missing |  Major | . | Tyler Hobbs | Marcus Eriksson |
| [CASSANDRA-9334](https://issues.apache.org/jira/browse/CASSANDRA-9334) | Returning null from a trigger does not abort the write |  Major | Coordination | Brandon Williams | Sam Tunnicliffe |
| [CASSANDRA-8216](https://issues.apache.org/jira/browse/CASSANDRA-8216) | Select Count with Limit returns wrong value |  Major | . | Philip Thompson | Benjamin Lerer |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6968](https://issues.apache.org/jira/browse/CASSANDRA-6968) | Reduce Unit Test Times Due to Schema Loading |  Minor | Testing | Tyler Hobbs | Lyuben Todorov |
| [CASSANDRA-7226](https://issues.apache.org/jira/browse/CASSANDRA-7226) | get code coverage working again (cobertura or other) |  Major | . | Russ Hatch | Russ Hatch |
| [CASSANDRA-6969](https://issues.apache.org/jira/browse/CASSANDRA-6969) | Use Unsafe Mutations Where Possible in Unit Tests |  Minor | Testing | Tyler Hobbs | Lyuben Todorov |
| [CASSANDRA-8622](https://issues.apache.org/jira/browse/CASSANDRA-8622) | All of pig-test is failing in trunk |  Major | . | Philip Thompson | Brandon Williams |
| [CASSANDRA-9053](https://issues.apache.org/jira/browse/CASSANDRA-9053) | Convert dtests that use cassandra-cli to Thrift API |  Major | Testing | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-9125](https://issues.apache.org/jira/browse/CASSANDRA-9125) | cqlsh: add tests for INSERT and UPDATE completion |  Minor | Tools | Jim Witschey | Jim Witschey |
| [CASSANDRA-9155](https://issues.apache.org/jira/browse/CASSANDRA-9155) | cqlsh: add tests for DELETE, CREATE TABLE/COLUMNFAMILY, DROP KEYSPACE |  Minor | . | Jim Witschey | Jim Witschey |
| [CASSANDRA-9248](https://issues.apache.org/jira/browse/CASSANDRA-9248) | Tests for cqlsh parser |  Minor | . | Jim Witschey | Jim Witschey |
| [CASSANDRA-9104](https://issues.apache.org/jira/browse/CASSANDRA-9104) | Unit test failures, trunk + Windows |  Major | Testing | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-9276](https://issues.apache.org/jira/browse/CASSANDRA-9276) | StreamingTransferTest fails under test-compression due to bad assertion |  Major | Streaming and Messaging | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-9275](https://issues.apache.org/jira/browse/CASSANDRA-9275) | ReadMessageTest.testNoCommitLog fails with test-compression |  Major | . | Ariel Weisberg | Branimir Lambov |
| [CASSANDRA-9277](https://issues.apache.org/jira/browse/CASSANDRA-9277) | SSTableRewriterTest.testFileRemoval fails with test-compression |  Major | Testing | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-9287](https://issues.apache.org/jira/browse/CASSANDRA-9287) | CQLSSTableWriterLongTest is failing |  Major | Testing | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-9278](https://issues.apache.org/jira/browse/CASSANDRA-9278) | LeveledCompactionStrategytest.testGrouperLevels fails with test-compression |  Major | Testing | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-9376](https://issues.apache.org/jira/browse/CASSANDRA-9376) | Investigate using asynchronous logback config for tests to avoid timeouts and reduce test time |  Major | Testing | Ariel Weisberg | Ariel Weisberg |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7195](https://issues.apache.org/jira/browse/CASSANDRA-7195) | ColumnFamilyStoreTest unit tests fails on Windows |  Minor | Testing | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-7389](https://issues.apache.org/jira/browse/CASSANDRA-7389) | BitSetTest fails on Windows |  Minor | . | Joshua McKenzie | Ala' Alkhaldi |
| [CASSANDRA-7708](https://issues.apache.org/jira/browse/CASSANDRA-7708) | UDF schema change events/results |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-7653](https://issues.apache.org/jira/browse/CASSANDRA-7653) | Add role based access control to Cassandra |  Major | CQL, Distributed Metadata | Mike Adamson | Sam Tunnicliffe |
| [CASSANDRA-8710](https://issues.apache.org/jira/browse/CASSANDRA-8710) | row cache: remove annoying classpath from "jemalloc not found" message |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-8650](https://issues.apache.org/jira/browse/CASSANDRA-8650) | Creation and maintenance of roles should not require superuser status |  Major | CQL, Distributed Metadata | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-7216](https://issues.apache.org/jira/browse/CASSANDRA-7216) | Creating database resources automatically grants creator full permissions |  Minor | CQL, Distributed Metadata | Oded Peer | Sam Tunnicliffe |
| [CASSANDRA-8528](https://issues.apache.org/jira/browse/CASSANDRA-8528) | Add an ExecutionException to the protocol |  Major | . | Sylvain Lebresne | Robert Stupp |
| [CASSANDRA-8760](https://issues.apache.org/jira/browse/CASSANDRA-8760) | CQL inconsistencies with CREATE ROLE options |  Trivial | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-8849](https://issues.apache.org/jira/browse/CASSANDRA-8849) | ListUsersStatement should consider inherited superuser status |  Minor | CQL, Distributed Metadata | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-8714](https://issues.apache.org/jira/browse/CASSANDRA-8714) | row-cache: use preloaded jemalloc w/ Unsafe |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-8761](https://issues.apache.org/jira/browse/CASSANDRA-8761) | Make custom role options accessible from IRoleManager |  Minor | CQL, Distributed Metadata | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-7523](https://issues.apache.org/jira/browse/CASSANDRA-7523) | add date and time types |  Minor | CQL | Jonathan Ellis | Joshua McKenzie |
| [CASSANDRA-7660](https://issues.apache.org/jira/browse/CASSANDRA-7660) | Indicate PK columns in "prepared" native protocol responses |  Major | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-8850](https://issues.apache.org/jira/browse/CASSANDRA-8850) | clean up options syntax for create/alter role |  Minor | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-8838](https://issues.apache.org/jira/browse/CASSANDRA-8838) | Resumable bootstrap streaming |  Minor | Streaming and Messaging | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-8553](https://issues.apache.org/jira/browse/CASSANDRA-8553) | Add a key-value payload for third party usage |  Major | . | Sergio Bossa | Robert Stupp |
| [CASSANDRA-8942](https://issues.apache.org/jira/browse/CASSANDRA-8942) | Keep node up even when bootstrap is failed (and provide tool to resume bootstrap) |  Minor | Streaming and Messaging | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-9058](https://issues.apache.org/jira/browse/CASSANDRA-9058) | Expand harness to test against all compaction strategies |  Major | . | Philip Thompson | Philip Thompson |
| [CASSANDRA-7807](https://issues.apache.org/jira/browse/CASSANDRA-7807) | Push notification when tracing completes for an operation |  Minor | . | Tyler Hobbs | Robert Stupp |
| [CASSANDRA-7557](https://issues.apache.org/jira/browse/CASSANDRA-7557) | User permissions for UDFs |  Major | CQL, Distributed Metadata | Tyler Hobbs | Sam Tunnicliffe |
| [CASSANDRA-7304](https://issues.apache.org/jira/browse/CASSANDRA-7304) | Ability to distinguish between NULL and UNSET values in Prepared Statements |  Major | . | Drew Kutcharian | Oded Peer |
| [CASSANDRA-8930](https://issues.apache.org/jira/browse/CASSANDRA-8930) | Add a warn notification for clients |  Major | CQL | Carl Yeksigian | Carl Yeksigian |
| [CASSANDRA-8951](https://issues.apache.org/jira/browse/CASSANDRA-8951) | Add smallint (and possibly byte) type |  Major | CQL, Distributed Metadata | Sylvain Lebresne | Benjamin Lerer |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7369](https://issues.apache.org/jira/browse/CASSANDRA-7369) | A minor space is missing in response message from cassandra |  Trivial | . | Usman | Dave Brosius |
| [CASSANDRA-7874](https://issues.apache.org/jira/browse/CASSANDRA-7874) | Validate functionality of different JSR-223 providers in UDFs |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-5657](https://issues.apache.org/jira/browse/CASSANDRA-5657) | Upgrade metrics lib and remove deprecated metrics |  Major | Observability, Tools | Jonathan Ellis | T Jake Luciani |
| [CASSANDRA-5174](https://issues.apache.org/jira/browse/CASSANDRA-5174) | expose nodetool scrub for 2Is |  Minor | Tools | Jason Brown | Stefania |
| [CASSANDRA-8995](https://issues.apache.org/jira/browse/CASSANDRA-8995) | Move NodeToolCmd subclasses to their own package |  Trivial | . | Yuki Morishita | Dave Brosius |
| [CASSANDRA-9319](https://issues.apache.org/jira/browse/CASSANDRA-9319) | Don't start Thrift RPC server by default in 2.2 |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-8043](https://issues.apache.org/jira/browse/CASSANDRA-8043) | Native Protocol V4 |  Major | . | Sylvain Lebresne |  |
| [CASSANDRA-9404](https://issues.apache.org/jira/browse/CASSANDRA-9404) | Add a flag in cassandra.yaml to enable UDFs |  Critical | . | Robert Stupp | Robert Stupp |


