
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.3 - 2009-07-16



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-71](https://issues.apache.org/jira/browse/CASSANDRA-71) | Range query support |  Major | . | Jonathan Ellis | Jonathan Ellis |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-75](https://issues.apache.org/jira/browse/CASSANDRA-75) | wire up memtable mbean |  Major | . | Eric Evans | Eric Evans |
| [CASSANDRA-51](https://issues.apache.org/jira/browse/CASSANDRA-51) |  Memory footprint for memtable |  Major | . | Sandeep Tata | Eric Evans |
| [CASSANDRA-29](https://issues.apache.org/jira/browse/CASSANDRA-29) | Change value to binary from string |  Major | . | Johan Oskarsson | Jonathan Ellis |
| [CASSANDRA-88](https://issues.apache.org/jira/browse/CASSANDRA-88) | clean up CassandraServer and StorageProxy |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-66](https://issues.apache.org/jira/browse/CASSANDRA-66) | get\_column throws exception on missing column |  Major | . | Jun Rao | Jun Rao |
| [CASSANDRA-94](https://issues.apache.org/jira/browse/CASSANDRA-94) | Remove mix of ip-address and hostnames |  Minor | . | Per Mellqvist | Jonathan Ellis |
| [CASSANDRA-32](https://issues.apache.org/jira/browse/CASSANDRA-32) | Create junit xml files |  Critical | . | Johan Oskarsson | Johan Oskarsson |
| [CASSANDRA-108](https://issues.apache.org/jira/browse/CASSANDRA-108) | Add license file and basic readme |  Major | . | Johan Oskarsson | Johan Oskarsson |
| [CASSANDRA-107](https://issues.apache.org/jira/browse/CASSANDRA-107) | Create release ant target |  Major | . | Johan Oskarsson | Johan Oskarsson |
| [CASSANDRA-65](https://issues.apache.org/jira/browse/CASSANDRA-65) | Support for non-hash based partitioners |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-113](https://issues.apache.org/jira/browse/CASSANDRA-113) | Remove unused variables |  Trivial | . | Johan Oskarsson | Johan Oskarsson |
| [CASSANDRA-121](https://issues.apache.org/jira/browse/CASSANDRA-121) | non-java code shouldn't be in org.apache namespace |  Major | . | Jonathan Ellis |  |
| [CASSANDRA-117](https://issues.apache.org/jira/browse/CASSANDRA-117) | Add quickstart |  Major | . | Johan Oskarsson | Johan Oskarsson |
| [CASSANDRA-127](https://issues.apache.org/jira/browse/CASSANDRA-127) | Add notice file |  Major | . | Johan Oskarsson |  |
| [CASSANDRA-118](https://issues.apache.org/jira/browse/CASSANDRA-118) | make the out-of-the-box config 1GB-friendly |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-130](https://issues.apache.org/jira/browse/CASSANDRA-130) | uniform logging of read/write paths |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-131](https://issues.apache.org/jira/browse/CASSANDRA-131) | use exceptions for reporting \*insertBlocking failures |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-135](https://issues.apache.org/jira/browse/CASSANDRA-135) | move queue length code into jmx |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-159](https://issues.apache.org/jira/browse/CASSANDRA-159) | avoid repetetive sorting in CFS.getColumnFamilyFromDisk |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-190](https://issues.apache.org/jira/browse/CASSANDRA-190) | Add souce distribution to release target |  Blocker | . | Johan Oskarsson | Johan Oskarsson |
| [CASSANDRA-191](https://issues.apache.org/jira/browse/CASSANDRA-191) | Adapt naming to incubator guidelines |  Blocker | . | Johan Oskarsson | Johan Oskarsson |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-57](https://issues.apache.org/jira/browse/CASSANDRA-57) | testGetCompactionBuckets sometimes fail |  Major | . | Johan Oskarsson | Jonathan Ellis |
| [CASSANDRA-82](https://issues.apache.org/jira/browse/CASSANDRA-82) | ColumnComparatorFactoryTest test fails |  Major | . | Jun Rao | Jun Rao |
| [CASSANDRA-72](https://issues.apache.org/jira/browse/CASSANDRA-72) | Thrift swallows certain classes of network errors |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-9](https://issues.apache.org/jira/browse/CASSANDRA-9) | Cassandra silently loses data when a single row gets large (under "heavy load") |  Major | . | Neophytos Demetriou | Jonathan Ellis |
| [CASSANDRA-59](https://issues.apache.org/jira/browse/CASSANDRA-59) | testNameSort fails |  Major | . | Johan Oskarsson | Jonathan Ellis |
| [CASSANDRA-68](https://issues.apache.org/jira/browse/CASSANDRA-68) | Bloom filters have much higher false-positive rate than expected |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-84](https://issues.apache.org/jira/browse/CASSANDRA-84) | Bug removing supercolumn |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-33](https://issues.apache.org/jira/browse/CASSANDRA-33) | Bugs in tombstone handling in remove code |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-91](https://issues.apache.org/jira/browse/CASSANDRA-91) | SuperColumn.getSubColumn fails assertion when subcolumn is not present |  Major | . | Sandeep Tata | Sandeep Tata |
| [CASSANDRA-14](https://issues.apache.org/jira/browse/CASSANDRA-14) | The initial arrive time should not be set to zero |  Major | . | zhu han | Jonathan Ellis |
| [CASSANDRA-13](https://issues.apache.org/jira/browse/CASSANDRA-13) | fix the EndPointState time bookmarking in Gossiper |  Major | . | zhu han | Jonathan Ellis |
| [CASSANDRA-81](https://issues.apache.org/jira/browse/CASSANDRA-81) | get\_slice ignores the "start" parameter |  Major | . | Sandeep Tata | Sandeep Tata |
| [CASSANDRA-87](https://issues.apache.org/jira/browse/CASSANDRA-87) | read repair of tombstones on columnfamilies and supercolumns |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-98](https://issues.apache.org/jira/browse/CASSANDRA-98) | Reads (get\_column) miss data or return stale values if a memtable is being flushed |  Major | . | Sandeep Tata | Sandeep Tata |
| [CASSANDRA-109](https://issues.apache.org/jira/browse/CASSANDRA-109) | Missing null check for READ-REPAIR header |  Minor | . | Per Mellqvist | Per Mellqvist |
| [CASSANDRA-37](https://issues.apache.org/jira/browse/CASSANDRA-37) | Remove JDK-derived code from project |  Critical | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-97](https://issues.apache.org/jira/browse/CASSANDRA-97) | race condition prevents startup under Xen vm |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-114](https://issues.apache.org/jira/browse/CASSANDRA-114) | exception when a node is joining |  Major | . | nk11 | Jonathan Ellis |
| [CASSANDRA-125](https://issues.apache.org/jira/browse/CASSANDRA-125) | Antlr checks broken |  Major | . | Johan Oskarsson |  |
| [CASSANDRA-120](https://issues.apache.org/jira/browse/CASSANDRA-120) | mutation replies are not correctly deserialized by originator |  Critical | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-76](https://issues.apache.org/jira/browse/CASSANDRA-76) | Don't rely on flushkey\_ special value to force flush |  Major | . | Jonathan Ellis | Eric Evans |
| [CASSANDRA-34](https://issues.apache.org/jira/browse/CASSANDRA-34) | Hinted handoff rows never get deleted |  Major | . | Jonathan Ellis | Jun Rao |
| [CASSANDRA-78](https://issues.apache.org/jira/browse/CASSANDRA-78) | Interrupted recovery requires manual intervention to fix |  Critical | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-124](https://issues.apache.org/jira/browse/CASSANDRA-124) | NullPointerException in consistency manager after a failed node rejoins |  Critical | . | Mark Robson | Jonathan Ellis |
| [CASSANDRA-129](https://issues.apache.org/jira/browse/CASSANDRA-129) | \`show config file\` in cli causes server to throw NPE |  Minor | . | Jeff Hodges | Sandeep Tata |
| [CASSANDRA-143](https://issues.apache.org/jira/browse/CASSANDRA-143) | r/m \`Leader\` from the web interface |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-164](https://issues.apache.org/jira/browse/CASSANDRA-164) | Fix junit related build issues |  Major | . | Johan Oskarsson | Johan Oskarsson |
| [CASSANDRA-161](https://issues.apache.org/jira/browse/CASSANDRA-161) | occasional CME in getKeyRange |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-156](https://issues.apache.org/jira/browse/CASSANDRA-156) | error reading key until first use of the HTTP interface |  Minor | . | Mark Robson | Jonathan Ellis |
| [CASSANDRA-167](https://issues.apache.org/jira/browse/CASSANDRA-167) | Fix cobertura coverage report |  Major | . | Johan Oskarsson | Johan Oskarsson |
| [CASSANDRA-165](https://issues.apache.org/jira/browse/CASSANDRA-165) | RejectedExecutionException in getKeyRange |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-204](https://issues.apache.org/jira/browse/CASSANDRA-204) | Replayed log data is not flushed before logs are wiped |  Critical | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-219](https://issues.apache.org/jira/browse/CASSANDRA-219) | weakreadremote has high response return (\>100ms) |  Major | . | Chris Goffinet | Jonathan Ellis |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-77](https://issues.apache.org/jira/browse/CASSANDRA-77) | Need RangeFilter that filters by column name so that min \<= name \<= max |  Major | . | Sandeep Tata | Sandeep Tata |
| [CASSANDRA-104](https://issues.apache.org/jira/browse/CASSANDRA-104) | Move test code |  Major | . | Johan Oskarsson | Johan Oskarsson |
| [CASSANDRA-103](https://issues.apache.org/jira/browse/CASSANDRA-103) | Move generated thrift code |  Major | . | Johan Oskarsson |  |
| [CASSANDRA-112](https://issues.apache.org/jira/browse/CASSANDRA-112) | Move non generated source |  Major | . | Johan Oskarsson | Johan Oskarsson |
| [CASSANDRA-105](https://issues.apache.org/jira/browse/CASSANDRA-105) | Move generated cql code |  Major | . | Johan Oskarsson | Johan Oskarsson |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-99](https://issues.apache.org/jira/browse/CASSANDRA-99) | Simplify default storage-conf.xml |  Minor | . | Sandeep Tata | Sandeep Tata |
| [CASSANDRA-175](https://issues.apache.org/jira/browse/CASSANDRA-175) | Add missing license headers |  Major | . | Johan Oskarsson | Johan Oskarsson |
| [CASSANDRA-179](https://issues.apache.org/jira/browse/CASSANDRA-179) | Remove ASM library |  Blocker | . | Johan Oskarsson | Johan Oskarsson |
| [CASSANDRA-178](https://issues.apache.org/jira/browse/CASSANDRA-178) | Remove JSAP |  Blocker | . | Johan Oskarsson | Jonathan Ellis |
| [CASSANDRA-177](https://issues.apache.org/jira/browse/CASSANDRA-177) | Remove Primitive Collections for Java |  Blocker | . | Johan Oskarsson | Jonathan Ellis |
| [CASSANDRA-186](https://issues.apache.org/jira/browse/CASSANDRA-186) | Figure out if activation.jar is used |  Major | . | Johan Oskarsson | Johan Oskarsson |
| [CASSANDRA-205](https://issues.apache.org/jira/browse/CASSANDRA-205) | Add required notices |  Major | . | Johan Oskarsson | Johan Oskarsson |
| [CASSANDRA-206](https://issues.apache.org/jira/browse/CASSANDRA-206) | Add license header to python and bash scripts |  Blocker | . | Johan Oskarsson | Johan Oskarsson |
| [CASSANDRA-207](https://issues.apache.org/jira/browse/CASSANDRA-207) | Change wiki url in readme |  Blocker | . | Johan Oskarsson | Johan Oskarsson |
| [CASSANDRA-216](https://issues.apache.org/jira/browse/CASSANDRA-216) | file describing bugs / limitations in 0.3 |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-222](https://issues.apache.org/jira/browse/CASSANDRA-222) | r/m touch from 0.3 |  Major | . | Jonathan Ellis | Jonathan Ellis |


