
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.8.4 - 2011-08-11



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2221](https://issues.apache.org/jira/browse/CASSANDRA-2221) | 'show create' commands on the CLI to export schema |  Minor | Tools | Jeremy Hanna | amorton |
| [CASSANDRA-2892](https://issues.apache.org/jira/browse/CASSANDRA-2892) | Don't "replicate\_on\_write" with RF=1 |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3004](https://issues.apache.org/jira/browse/CASSANDRA-3004) | Once a message has been dropped, cassandra logs total messages dropped and tpstats every 5s forever |  Minor | . | Brandon Williams | Jonathan Ellis |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2972](https://issues.apache.org/jira/browse/CASSANDRA-2972) | nodetool netstats progress does not update on receiving side |  Minor | . | Wojciech Meler | Yuki Morishita |
| [CASSANDRA-1777](https://issues.apache.org/jira/browse/CASSANDRA-1777) | The describe\_host API method is misleading in that it returns the interface associated with gossip traffic |  Major | . | Nate McCall | Brandon Williams |
| [CASSANDRA-2523](https://issues.apache.org/jira/browse/CASSANDRA-2523) | Distributed test scripts not working with Whirr 0.4.0 |  Major | . | Stu Hood | Michael Allen |
| [CASSANDRA-2785](https://issues.apache.org/jira/browse/CASSANDRA-2785) | should export JAVA variable in the bin/cassandra and use that in the cassandra-env.sh when check for the java version |  Minor | Packaging | Jackson Chung | paul cannon |
| [CASSANDRA-2992](https://issues.apache.org/jira/browse/CASSANDRA-2992) | Cassandra doesn't start on Red Hat Linux due to hardcoded JAVA\_HOME |  Minor | Packaging | Taras Puchko | paul cannon |
| [CASSANDRA-2996](https://issues.apache.org/jira/browse/CASSANDRA-2996) | Fix NPE in getRangeToRpcaddressMap |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2990](https://issues.apache.org/jira/browse/CASSANDRA-2990) | We should refuse query for counters at CL.ANY |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2993](https://issues.apache.org/jira/browse/CASSANDRA-2993) | Issues with parameters being escaped correctly in Python CQL |  Major | . | Blake Visin | Tyler Hobbs |
| [CASSANDRA-3006](https://issues.apache.org/jira/browse/CASSANDRA-3006) | Enormous counter |  Major | . | Boris Yen | Sylvain Lebresne |
| [CASSANDRA-3013](https://issues.apache.org/jira/browse/CASSANDRA-3013) | CQL counter support is undocumented |  Minor | CQL, Documentation and Website | Eric Evans | Eric Evans |
| [CASSANDRA-3011](https://issues.apache.org/jira/browse/CASSANDRA-3011) | Error upgrading when replication\_factor is stored in strategy\_options |  Minor | . | Jonathan Ellis | Jonathan Ellis |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2925](https://issues.apache.org/jira/browse/CASSANDRA-2925) | The JDBC Suite should provide a simple DataSource implementation |  Minor | . | Rick Shaw | Rick Shaw |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2998](https://issues.apache.org/jira/browse/CASSANDRA-2998) | Remove dependency on old version of cdh3 in any builds |  Major | . | Jeremy Hanna | Nate McCall |


