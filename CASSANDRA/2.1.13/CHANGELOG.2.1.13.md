
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1.13 - 2016-02-08



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9303](https://issues.apache.org/jira/browse/CASSANDRA-9303) | Match cassandra-loader options in COPY FROM |  Critical | Tools | Jonathan Ellis | Stefania |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9474](https://issues.apache.org/jira/browse/CASSANDRA-9474) | Validate dc information on startup |  Major | . | Marcus Olsson | Marcus Olsson |
| [CASSANDRA-9302](https://issues.apache.org/jira/browse/CASSANDRA-9302) | Optimize cqlsh COPY FROM, part 3 |  Critical | Tools | Jonathan Ellis | Stefania |
| [CASSANDRA-9294](https://issues.apache.org/jira/browse/CASSANDRA-9294) | Streaming errors should log the root cause |  Major | Streaming and Messaging | Brandon Williams | Paulo Motta |
| [CASSANDRA-10847](https://issues.apache.org/jira/browse/CASSANDRA-10847) | Log a message when refusing a major compaction due to incremental repairedAt status |  Trivial | Compaction | Brandon Williams | Marcus Eriksson |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10585](https://issues.apache.org/jira/browse/CASSANDRA-10585) | SSTablesPerReadHistogram seems wrong when row cache hit happend |  Minor | Observability | Ivan Burmistrov | Ivan Burmistrov |
| [CASSANDRA-8805](https://issues.apache.org/jira/browse/CASSANDRA-8805) | runWithCompactionsDisabled only cancels compactions, which is not the only source of markCompacted |  Major | Compaction | Benedict | Carl Yeksigian |
| [CASSANDRA-9179](https://issues.apache.org/jira/browse/CASSANDRA-9179) | Unable to "point in time" restore if table/cf has been recreated |  Major | CQL, Distributed Metadata | Jon Moses | Branimir Lambov |
| [CASSANDRA-10700](https://issues.apache.org/jira/browse/CASSANDRA-10700) | 2.1 sstableloader will fail if there are collections in the schema tables |  Major | Tools | Tyler Hobbs | T Jake Luciani |
| [CASSANDRA-10593](https://issues.apache.org/jira/browse/CASSANDRA-10593) | Unintended interactions between commitlog archiving and commitlog recycling |  Major | Local Write-Read Paths | J.B. Langston | Ariel Weisberg |
| [CASSANDRA-10701](https://issues.apache.org/jira/browse/CASSANDRA-10701) | stop referring to batches as atomic |  Minor | . | Jon Haddad | Sylvain Lebresne |
| [CASSANDRA-10854](https://issues.apache.org/jira/browse/CASSANDRA-10854) | cqlsh COPY FROM csv having line with more than one consecutive  ',' delimiter  is throwing 'list index out of range' |  Minor | Tools | Puspendu Banerjee | Stefania |
| [CASSANDRA-10875](https://issues.apache.org/jira/browse/CASSANDRA-10875) | cqlsh fails to decode utf-8 characters for text typed columns. |  Minor | Tools | Yasuharu Goto | Yasuharu Goto |
| [CASSANDRA-10831](https://issues.apache.org/jira/browse/CASSANDRA-10831) | Fix the way we replace sstables after anticompaction |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9309](https://issues.apache.org/jira/browse/CASSANDRA-9309) | Wrong interpretation of Config.getOutboundBindAny depending on using SSL or not |  Major | Streaming and Messaging | Casey Marshall | Yuki Morishita |
| [CASSANDRA-8708](https://issues.apache.org/jira/browse/CASSANDRA-8708) | inter\_dc\_stream\_throughput\_outbound\_megabits\_per\_sec to defaults to unlimited |  Major | Streaming and Messaging | Adam Hattrell | Jeremy Hanna |
| [CASSANDRA-10887](https://issues.apache.org/jira/browse/CASSANDRA-10887) | Pending range calculator gives wrong pending ranges for moves |  Critical | Coordination | Richard Low | sankalp kohli |
| [CASSANDRA-8072](https://issues.apache.org/jira/browse/CASSANDRA-8072) | Exception during startup: Unable to gossip with any seeds |  Major | Lifecycle | Ryan Springer | Stefania |
| [CASSANDRA-10676](https://issues.apache.org/jira/browse/CASSANDRA-10676) | AssertionError in CompactionExecutor |  Major | Compaction | mlowicki | Carl Yeksigian |
| [CASSANDRA-10477](https://issues.apache.org/jira/browse/CASSANDRA-10477) | java.lang.AssertionError in StorageProxy.submitHint |  Major | Local Write-Read Paths | Severin Leonhardt | Ariel Weisberg |
| [CASSANDRA-10686](https://issues.apache.org/jira/browse/CASSANDRA-10686) | cqlsh schema refresh on timeout dtest is flaky |  Minor | Testing, Tools | Joel Knighton | Paulo Motta |
| [CASSANDRA-10961](https://issues.apache.org/jira/browse/CASSANDRA-10961) | Not enough bytes error when add nodes to cluster |  Major | Streaming and Messaging | xiaost | Paulo Motta |
| [CASSANDRA-10839](https://issues.apache.org/jira/browse/CASSANDRA-10839) | cqlsh failed to format value bytearray |  Minor | Tools | Severin Leonhardt | Stefania |
| [CASSANDRA-10997](https://issues.apache.org/jira/browse/CASSANDRA-10997) | cqlsh\_copy\_tests failing en mass when vnodes are disabled |  Major | Tools | Philip Thompson | Stefania |
| [CASSANDRA-11005](https://issues.apache.org/jira/browse/CASSANDRA-11005) | Split consistent range movement flag |  Trivial | Configuration | sankalp kohli | sankalp kohli |
| [CASSANDRA-10829](https://issues.apache.org/jira/browse/CASSANDRA-10829) | cleanup + repair generates a lot of logs |  Major | Streaming and Messaging | Fabien Rousseau | Marcus Eriksson |
| [CASSANDRA-10909](https://issues.apache.org/jira/browse/CASSANDRA-10909) | NPE in ActiveRepairService |  Major | Streaming and Messaging | Eduard Tudenhoefner | Marcus Eriksson |
| [CASSANDRA-10969](https://issues.apache.org/jira/browse/CASSANDRA-10969) | long-running cluster sees bad gossip generation when a node restarts |  Major | Coordination | T. David Hudson | Joel Knighton |
| [CASSANDRA-11007](https://issues.apache.org/jira/browse/CASSANDRA-11007) | Exception when running nodetool info during bootstrap |  Minor | Tools | T Jake Luciani | Yuki Morishita |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10799](https://issues.apache.org/jira/browse/CASSANDRA-10799) | 2 cqlshlib tests still failing with cythonized driver installation |  Major | Testing | Stefania | Stefania |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10844](https://issues.apache.org/jira/browse/CASSANDRA-10844) | failed\_bootstrap\_wiped\_node\_can\_join\_test is failing |  Major | Streaming and Messaging, Testing | Philip Thompson | Joel Knighton |
| [CASSANDRA-10938](https://issues.apache.org/jira/browse/CASSANDRA-10938) | test\_bulk\_round\_trip\_blogposts is failing occasionally |  Major | Tools | Stefania | Stefania |


