
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.0.0 rc2 - 2015-10-19



### IMPORTANT ISSUES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9602](https://issues.apache.org/jira/browse/CASSANDRA-9602) | EACH\_QUORUM READ support needed |  Major | CQL | Scott Guminy | Carl Yeksigian |


### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10241](https://issues.apache.org/jira/browse/CASSANDRA-10241) | Keep a separate production debug log for troubleshooting |  Major | Configuration | Jonathan Ellis | Paulo Motta |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9855](https://issues.apache.org/jira/browse/CASSANDRA-9855) | Make page\_size configurable in cqlsh |  Minor | . | Sylvain Lebresne | Ryan McGuire |
| [CASSANDRA-9833](https://issues.apache.org/jira/browse/CASSANDRA-9833) | Save space in WriteCallbackInfo |  Minor | . | Benedict | Benedict |
| [CASSANDRA-10429](https://issues.apache.org/jira/browse/CASSANDRA-10429) | Flush schema tables after local schema change |  Minor | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-10323](https://issues.apache.org/jira/browse/CASSANDRA-10323) | Add more MaterializedView metrics |  Major | . | T Jake Luciani | Chris Lohfink |
| [CASSANDRA-10436](https://issues.apache.org/jira/browse/CASSANDRA-10436) | Index selection should be weighted in favour of custom expressions |  Major | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-10256](https://issues.apache.org/jira/browse/CASSANDRA-10256) | document commitlog segment size's relationship to max write size |  Trivial | Configuration | Chris Burroughs | Tushar Agrawal |
| [CASSANDRA-10438](https://issues.apache.org/jira/browse/CASSANDRA-10438) | Overwrites of rows in memtable produce incorrect deltas for indexing |  Major | CQL, Local Write-Read Paths | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-10378](https://issues.apache.org/jira/browse/CASSANDRA-10378) | Make skipping more efficient |  Major | . | Benedict | Sylvain Lebresne |
| [CASSANDRA-10242](https://issues.apache.org/jira/browse/CASSANDRA-10242) | Validate rack information on startup |  Major | Configuration | Jonathan Ellis | Carl Yeksigian |
| [CASSANDRA-10403](https://issues.apache.org/jira/browse/CASSANDRA-10403) | Consider reverting to CMS GC on 3.0 |  Major | Configuration | Joshua McKenzie | Paulo Motta |
| [CASSANDRA-8970](https://issues.apache.org/jira/browse/CASSANDRA-8970) | Allow custom time\_format on cqlsh COPY TO |  Trivial | Tools | Aaron Ploetz | Aaron Ploetz |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10074](https://issues.apache.org/jira/browse/CASSANDRA-10074) | cqlsh HELP SELECT\_EXPR gives outdated incorrect information |  Trivial | Tools | Jim Meyer | Philip Thompson |
| [CASSANDRA-10369](https://issues.apache.org/jira/browse/CASSANDRA-10369) | cqlsh prompt includes name of keyspace after failed \`use\` statement |  Minor | . | Jim Witschey | Robert Stupp |
| [CASSANDRA-10228](https://issues.apache.org/jira/browse/CASSANDRA-10228) | JVMStabilityInspector should inspect cause and suppressed exceptions |  Major | . | Benedict | Paul MacIntosh |
| [CASSANDRA-10113](https://issues.apache.org/jira/browse/CASSANDRA-10113) | Undroppable messages can be dropped if message queue gets large |  Minor | Streaming and Messaging | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-10141](https://issues.apache.org/jira/browse/CASSANDRA-10141) | UFPureScriptTest fails with pre-3.0 java-driver |  Major | . | Joshua McKenzie | Robert Stupp |
| [CASSANDRA-10052](https://issues.apache.org/jira/browse/CASSANDRA-10052) | Misleading down-node push notifications when rpc\_address is shared |  Major | CQL | Sharvanath Pathak | Stefania |
| [CASSANDRA-10347](https://issues.apache.org/jira/browse/CASSANDRA-10347) | Bulk Loader API could not tolerate even node failure |  Major | Tools | Shenghua Wan | Paulo Motta |
| [CASSANDRA-10289](https://issues.apache.org/jira/browse/CASSANDRA-10289) | Fix cqlshlib tests |  Major | Testing | Jim Witschey | Jim Witschey |
| [CASSANDRA-9774](https://issues.apache.org/jira/browse/CASSANDRA-9774) | fix sstableverify dtest |  Blocker | . | Jim Witschey | Sylvain Lebresne |
| [CASSANDRA-10435](https://issues.apache.org/jira/browse/CASSANDRA-10435) | Index selectivity calculation is incorrect |  Major | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-10293](https://issues.apache.org/jira/browse/CASSANDRA-10293) | Re-populate token metadata after commit log replay |  Minor | Coordination, Lifecycle | Alan Boudreault | Paulo Motta |
| [CASSANDRA-10437](https://issues.apache.org/jira/browse/CASSANDRA-10437) | Remove offheap\_objects option until 9472 re-introduces them |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-10413](https://issues.apache.org/jira/browse/CASSANDRA-10413) | Replaying materialized view updates from commitlog after node decommission crashes Cassandra |  Critical | Coordination, Materialized Views | Joel Knighton | Joel Knighton |
| [CASSANDRA-10443](https://issues.apache.org/jira/browse/CASSANDRA-10443) | CQLSStableWriter example fails on 3.0rc1 |  Major | Tools | Jonathan Shook | Carl Yeksigian |
| [CASSANDRA-10424](https://issues.apache.org/jira/browse/CASSANDRA-10424) | Altering base table column with materialized view causes unexpected server error. |  Major | Coordination, Materialized Views | Greg Bestland | Carl Yeksigian |
| [CASSANDRA-10095](https://issues.apache.org/jira/browse/CASSANDRA-10095) | Fix dtests on 3.0 branch on Windows |  Major | Testing | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-10434](https://issues.apache.org/jira/browse/CASSANDRA-10434) | Problem upgrading to 3.0 with UDA |  Major | . | Sylvain Lebresne | Robert Stupp |
| [CASSANDRA-10484](https://issues.apache.org/jira/browse/CASSANDRA-10484) | cqlsh pg-style-strings broken |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-10483](https://issues.apache.org/jira/browse/CASSANDRA-10483) | ThriftConversion doesn't filter custom index options correctly |  Major | Distributed Metadata | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-10275](https://issues.apache.org/jira/browse/CASSANDRA-10275) | Allow LOCAL\_JMX to be easily overridden |  Trivial | . | Thorkild Stray | Thorkild Stray |
| [CASSANDRA-10219](https://issues.apache.org/jira/browse/CASSANDRA-10219) | KeyCache deserialization doesn't properly read indexed entries |  Major | . | Sylvain Lebresne | Branimir Lambov |
| [CASSANDRA-10231](https://issues.apache.org/jira/browse/CASSANDRA-10231) | Null status entries on nodes that crash during decommission of a different node |  Major | Distributed Metadata, Materialized Views | Joel Knighton | Joel Knighton |
| [CASSANDRA-10427](https://issues.apache.org/jira/browse/CASSANDRA-10427) | compactionstats 'completed' field not updating |  Critical | . | T Jake Luciani | Marcus Eriksson |
| [CASSANDRA-10412](https://issues.apache.org/jira/browse/CASSANDRA-10412) | Could not initialize class org.apache.cassandra.config.DatabaseDescriptor |  Minor | Configuration | Eric Simmons | Carl Yeksigian |
| [CASSANDRA-10503](https://issues.apache.org/jira/browse/CASSANDRA-10503) | NPE in MVs on update |  Critical | . | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-10447](https://issues.apache.org/jira/browse/CASSANDRA-10447) | Stop TeeingAppender on shutdown hook |  Major | Observability | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-10523](https://issues.apache.org/jira/browse/CASSANDRA-10523) | Distinguish Infinity and -Infinity in cqlsh result formatting |  Minor | Tools | Adam Holmberg | Adam Holmberg |
| [CASSANDRA-10525](https://issues.apache.org/jira/browse/CASSANDRA-10525) | Hints directory not created on debian packaged install |  Major | Configuration | Paulo Motta | Paulo Motta |
| [CASSANDRA-10363](https://issues.apache.org/jira/browse/CASSANDRA-10363) | NullPointerException returned with select ttl(value), IN, ORDER BY and paging off |  Minor | CQL | Sucwinder Bassi | Benjamin Lerer |
| [CASSANDRA-10174](https://issues.apache.org/jira/browse/CASSANDRA-10174) | Range tombstones through thrift don't handle static columns probably |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-10518](https://issues.apache.org/jira/browse/CASSANDRA-10518) | initialDirectories passed into ColumnFamilyStore contructor |  Minor | . | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-10529](https://issues.apache.org/jira/browse/CASSANDRA-10529) | Channel.size() is costly, mutually exclusive, and on the critical path |  Major | Local Write-Read Paths | Benedict | Stefania |
| [CASSANDRA-10536](https://issues.apache.org/jira/browse/CASSANDRA-10536) | Batch statements with multiple updates to partition error when table is indexed |  Major | CQL | Tyler Hobbs | Sam Tunnicliffe |
| [CASSANDRA-10543](https://issues.apache.org/jira/browse/CASSANDRA-10543) | Self-reference leak in SegmentedFile |  Critical | Local Write-Read Paths | Sam Tunnicliffe | Sam Tunnicliffe |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10103](https://issues.apache.org/jira/browse/CASSANDRA-10103) | Windows dtest 3.0: incremental\_repair\_test.py:TestIncRepair.sstable\_repairedset\_test fails |  Major | Streaming and Messaging | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-10386](https://issues.apache.org/jira/browse/CASSANDRA-10386) | Windows dtest 3.0: replication\_test fails on Windows |  Major | Testing | Joshua McKenzie | Stefania |
| [CASSANDRA-10417](https://issues.apache.org/jira/browse/CASSANDRA-10417) | Windows dtest 3.0: creating\_and\_dropping\_table\_with\_2ary\_indexes\_test |  Major | Testing | Jim Witschey | Paulo Motta |
| [CASSANDRA-10416](https://issues.apache.org/jira/browse/CASSANDRA-10416) | Windows dtest 3.0: clustering\_order\_test dtest fails |  Major | Testing | Jim Witschey | Yuki Morishita |
| [CASSANDRA-10418](https://issues.apache.org/jira/browse/CASSANDRA-10418) | Windows dtest 3.0: cqlsh CLEAR/CLS tests fail |  Major | Tools | Jim Witschey | Joshua McKenzie |
| [CASSANDRA-10451](https://issues.apache.org/jira/browse/CASSANDRA-10451) | Fix batch\_test.TestBatch.logged\_batch\_gcgs\_below\_threshold\_{multi,single}\_table\_test on CassCI |  Major | Testing | Jim Witschey | Paulo Motta |
| [CASSANDRA-10453](https://issues.apache.org/jira/browse/CASSANDRA-10453) | Fix compression\_cql\_options\_test dtest |  Major | Testing | Jim Witschey | Paulo Motta |
| [CASSANDRA-10456](https://issues.apache.org/jira/browse/CASSANDRA-10456) | fix deprecated\_repair\_test dtests |  Major | . | Jim Witschey | Jim Witschey |
| [CASSANDRA-10455](https://issues.apache.org/jira/browse/CASSANDRA-10455) | Fix pep8 compliance in cqlsh.py |  Major | . | Jim Witschey | Philip Thompson |
| [CASSANDRA-10460](https://issues.apache.org/jira/browse/CASSANDRA-10460) | Fix materialized\_views\_test.py:TestMaterializedViews.complex\_mv\_select\_statements\_test |  Major | Materialized Views, Testing | Jim Witschey | Joel Knighton |
| [CASSANDRA-10385](https://issues.apache.org/jira/browse/CASSANDRA-10385) | Fix eclipse-warning report |  Trivial | . | T Jake Luciani | Branimir Lambov |
| [CASSANDRA-10205](https://issues.apache.org/jira/browse/CASSANDRA-10205) | decommissioned\_wiped\_node\_can\_join\_test fails on Jenkins |  Major | Distributed Metadata, Testing | Stefania | Stefania |
| [CASSANDRA-10457](https://issues.apache.org/jira/browse/CASSANDRA-10457) | fix failing jmx\_metrics dtest |  Major | Testing | Jim Witschey | Carl Yeksigian |
| [CASSANDRA-10475](https://issues.apache.org/jira/browse/CASSANDRA-10475) | skip protocol v2 test on versions that don't support it |  Major | . | Jim Witschey | Jim Witschey |
| [CASSANDRA-10459](https://issues.apache.org/jira/browse/CASSANDRA-10459) | Fix largecolumn\_test.py:TestLargeColumn.cleanup\_test dtest |  Major | Testing | Jim Witschey | Paulo Motta |
| [CASSANDRA-10463](https://issues.apache.org/jira/browse/CASSANDRA-10463) | Fix failing compaction tests |  Major | Testing | Jim Witschey | Yuki Morishita |
| [CASSANDRA-10454](https://issues.apache.org/jira/browse/CASSANDRA-10454) | fix failing cqlsh COPY dtests |  Major | . | Jim Witschey | Jim Witschey |
| [CASSANDRA-10522](https://issues.apache.org/jira/browse/CASSANDRA-10522) | counter upgrade dtest fails on 3.0 with JVM assertions disabled |  Major | Testing | Andrew Hust | Yuki Morishita |
| [CASSANDRA-10471](https://issues.apache.org/jira/browse/CASSANDRA-10471) | fix flapping empty\_in\_test dtest |  Major | . | Jim Witschey | Sylvain Lebresne |
| [CASSANDRA-10468](https://issues.apache.org/jira/browse/CASSANDRA-10468) | Fix class-casting error in mixed clusters for 2.2-\>3.0 upgrades |  Major | . | Jim Witschey | Sylvain Lebresne |
| [CASSANDRA-10473](https://issues.apache.org/jira/browse/CASSANDRA-10473) | fix failing dtest for select distinct with static columns on 2.2-\>3.0 upgrade path |  Major | CQL | Jim Witschey | Benjamin Lerer |
| [CASSANDRA-10415](https://issues.apache.org/jira/browse/CASSANDRA-10415) | Fix cqlsh bugs |  Major | Tools | Jim Witschey | Stefania |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10542](https://issues.apache.org/jira/browse/CASSANDRA-10542) | Deprecate Pig support in 2.2 and remove it in 3.0 |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |


