
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra  3.0.0 rc2 Release Notes

These release notes cover new developer and user-facing incompatibilities, important issues, features, and major improvements.


---

* [CASSANDRA-9602](https://issues.apache.org/jira/browse/CASSANDRA-9602) | *Major* | **EACH\_QUORUM READ support needed**

EACH\_QUORUM consistency for READ should be added.

This bug https://issues.apache.org/jira/browse/CASSANDRA-3272 says it is not needed ever, however I have a use case where I need it.  I think the decision made was incorrect. Here's why...
 
 My application has two key pieces:
 
 # \*End user actions\* which add/modify data in the system.  End users typically access the application from only one Data Center and only see their own data
# \*Scheduled business logic tasks\* which run from any node in any data center.  These tasks process data added by the end users in an asynchronous way
 
 \*End user actions must have the highest degree of availability.\*  Users must always be able to add data to the system.  The data will be processed later.  To support this, end user actions will use \*LOCAL\_QUORUM Read and Write Consistency\*.
 
 Scheduled tasks don't need to have a high degree of availability but MUST operate on the most up to date data.  \*The tasks will run with EACH\_QUORUM\* to ensure that no matter how many data centers we have, we always READ the latest data.  This approach allows us some amount of fault tolerance. 
 
 The problem is that EACH\_QUORUM is not a valid READ consistency level.  This mean I have no alternative but to use ALL.  ALL will work, but is not the best since it offers support for ZERO failures.  I would prefer EACH\_QUORUM since it can support some failures in each data center.



