
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1.0 - 2014-09-11



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7635](https://issues.apache.org/jira/browse/CASSANDRA-7635) | Make hinted\_handoff\_throttle\_in\_kb configurable via nodetool |  Minor | Tools | Matt Stump | Lyuben Todorov |
| [CASSANDRA-7717](https://issues.apache.org/jira/browse/CASSANDRA-7717) | cassandra-stress: add sample yamls to distro |  Minor | Packaging | Robert Stupp | T Jake Luciani |
| [CASSANDRA-7726](https://issues.apache.org/jira/browse/CASSANDRA-7726) | Give CRR a default input\_cql Statement |  Major | . | Russell Spitzer | Mike Adamson |
| [CASSANDRA-6126](https://issues.apache.org/jira/browse/CASSANDRA-6126) | Set MALLOC\_ARENA\_MAX in cassandra-env.sh for new glibc per-thread allocator |  Minor | Packaging | J. Ryan Earl | Brandon Williams |
| [CASSANDRA-7753](https://issues.apache.org/jira/browse/CASSANDRA-7753) | Level compaction for Paxos table |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-7857](https://issues.apache.org/jira/browse/CASSANDRA-7857) | Ability to freeze UDT |  Major | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-7650](https://issues.apache.org/jira/browse/CASSANDRA-7650) | Expose auto\_bootstrap as a system property override |  Trivial | Configuration | Joe Hohertz | Joe Hohertz |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6283](https://issues.apache.org/jira/browse/CASSANDRA-6283) | Windows 7 data files kept open / can't be deleted after compaction. |  Major | Compaction, Local Write-Read Paths | Andreas Schnitzerling | Joshua McKenzie |
| [CASSANDRA-7669](https://issues.apache.org/jira/browse/CASSANDRA-7669) | nodetool fails to connect when ipv6 host is specified |  Trivial | Tools | Adam Holmberg | Adam Holmberg |
| [CASSANDRA-7687](https://issues.apache.org/jira/browse/CASSANDRA-7687) | cqlsh DESC CLUSTER fails retrieving ring information |  Minor | Tools | Adam Holmberg | Adam Holmberg |
| [CASSANDRA-7685](https://issues.apache.org/jira/browse/CASSANDRA-7685) | Prepared marker for collections inside UDT do not handle null values |  Major | . | Olivier Michallat | Olivier Michallat |
| [CASSANDRA-7670](https://issues.apache.org/jira/browse/CASSANDRA-7670) | selecting field from empty UDT cell using dot notation triggers exception |  Minor | . | Russ Hatch | Mikhail Stepura |
| [CASSANDRA-7663](https://issues.apache.org/jira/browse/CASSANDRA-7663) | Removing a seed causes previously removed seeds to reappear |  Major | . | Richard Low | Brandon Williams |
| [CASSANDRA-7684](https://issues.apache.org/jira/browse/CASSANDRA-7684) | flush makes rows invisible to cluster key equality query |  Major | . | Jonathan Halliday | Sylvain Lebresne |
| [CASSANDRA-7691](https://issues.apache.org/jira/browse/CASSANDRA-7691) | Match error message with "tombstone\_failure\_threshold" config parameter |  Trivial | . | Nikita Vetoshkin | Nikita Vetoshkin |
| [CASSANDRA-7694](https://issues.apache.org/jira/browse/CASSANDRA-7694) | Expected Compaction Interruption is logged as ERROR |  Minor | Compaction | Philip Thompson | Marcus Eriksson |
| [CASSANDRA-7707](https://issues.apache.org/jira/browse/CASSANDRA-7707) | blobAs() function results not validated |  Critical | CQL | Tyler Hobbs | Sylvain Lebresne |
| [CASSANDRA-7700](https://issues.apache.org/jira/browse/CASSANDRA-7700) | Unexpected exception in RangeTombstoneList.insertFrom during Python Driver Duration Test |  Major | . | Rick Smith | Sylvain Lebresne |
| [CASSANDRA-7599](https://issues.apache.org/jira/browse/CASSANDRA-7599) | Dtest on low cardinality secondary indexes failing in 2.1 |  Major | Secondary Indexes, Testing | Shawn Kumar | Tyler Hobbs |
| [CASSANDRA-7695](https://issues.apache.org/jira/browse/CASSANDRA-7695) | Inserting the same row in parallel causes bad data to be returned to the client |  Blocker | . | Johan Bjork | Norman Maurer |
| [CASSANDRA-7478](https://issues.apache.org/jira/browse/CASSANDRA-7478) | StorageService.getJoiningNodes returns duplicate ips |  Major | . | Nick Bailey | Jonathan Ellis |
| [CASSANDRA-7745](https://issues.apache.org/jira/browse/CASSANDRA-7745) | Background LCS compactions stall with pending compactions remaining |  Minor | Compaction | J.B. Langston | Yuki Morishita |
| [CASSANDRA-7765](https://issues.apache.org/jira/browse/CASSANDRA-7765) | Don't use strict endpoint selection for range movements where RF == node count |  Minor | Coordination | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-7561](https://issues.apache.org/jira/browse/CASSANDRA-7561) | On DROP we should invalidate CounterKeyCache as well as Key/Row cache |  Minor | . | Benedict | Aleksey Yeschenko |
| [CASSANDRA-7477](https://issues.apache.org/jira/browse/CASSANDRA-7477) | JSON to SSTable import failing |  Major | . | Kishan Karunaratne | Mikhail Stepura |
| [CASSANDRA-7763](https://issues.apache.org/jira/browse/CASSANDRA-7763) | cql\_tests static\_with\_empty\_clustering test failure |  Major | . | Ryan McGuire | Sylvain Lebresne |
| [CASSANDRA-7744](https://issues.apache.org/jira/browse/CASSANDRA-7744) | Dropping the last collection column turns CompoundSparseCellNameType$WithCollection into CompoundDenseCellNameType |  Major | . | Aleksey Yeschenko | Sylvain Lebresne |
| [CASSANDRA-7787](https://issues.apache.org/jira/browse/CASSANDRA-7787) | 2i index indexing the cql3 row marker throws NPE |  Minor | . | Berenguer Blasi | Berenguer Blasi |
| [CASSANDRA-7792](https://issues.apache.org/jira/browse/CASSANDRA-7792) | Fix cqlsh null handling in COPY FROM |  Minor | Tools | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-7798](https://issues.apache.org/jira/browse/CASSANDRA-7798) | Empty clustering column not caught for CQL3 update to compact storage counter table |  Major | . | Jeremiah Jordan | Aleksey Yeschenko |
| [CASSANDRA-7795](https://issues.apache.org/jira/browse/CASSANDRA-7795) | Make StreamReceiveTask thread safe and GC friendly |  Minor | Streaming and Messaging | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-7797](https://issues.apache.org/jira/browse/CASSANDRA-7797) | Cannot alter ReversedType(DateType) clustering column to ReversedType(TimestampType) |  Major | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-7806](https://issues.apache.org/jira/browse/CASSANDRA-7806) | cqlsh: Fix column name formatting for functions, [applied], udt fields |  Minor | Tools | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-7803](https://issues.apache.org/jira/browse/CASSANDRA-7803) | When compaction is interrupted, it leaves locked, undeletable files |  Major | Compaction | Scooletz | Marcus Eriksson |
| [CASSANDRA-7808](https://issues.apache.org/jira/browse/CASSANDRA-7808) | LazilyCompactedRow incorrectly handles row tombstones |  Major | . | Richard Low | Richard Low |
| [CASSANDRA-7802](https://issues.apache.org/jira/browse/CASSANDRA-7802) | Need to export JVM\_OPTS from init.d script |  Minor | Packaging | Matt Robenolt | Matt Robenolt |
| [CASSANDRA-7832](https://issues.apache.org/jira/browse/CASSANDRA-7832) | A partition key-only CQL3 table mistakenly considered to be thrift compatible |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-7810](https://issues.apache.org/jira/browse/CASSANDRA-7810) | tombstones gc'd before being locally applied |  Major | Compaction | Jonathan Halliday | Marcus Eriksson |
| [CASSANDRA-7437](https://issues.apache.org/jira/browse/CASSANDRA-7437) |  Ensure writes have completed after dropping a table, before recycling commit log segments (CASSANDRA-7437) |  Minor | . | Benedict | Benedict |
| [CASSANDRA-7836](https://issues.apache.org/jira/browse/CASSANDRA-7836) | Data loss after moving tokens |  Critical | . | Philip Thompson | Tyler Hobbs |
| [CASSANDRA-7834](https://issues.apache.org/jira/browse/CASSANDRA-7834) | Case sensitivity problem in cqlsh when specifying keyspace in select |  Minor | Tools | Philip Thompson | Tyler Hobbs |
| [CASSANDRA-7831](https://issues.apache.org/jira/browse/CASSANDRA-7831) | recreating a counter column after dropping it leaves in unusable state |  Major | . | Peter Mädel | Aleksey Yeschenko |
| [CASSANDRA-7145](https://issues.apache.org/jira/browse/CASSANDRA-7145) | FileNotFoundException during compaction |  Major | Compaction | PJ | Marcus Eriksson |
| [CASSANDRA-7847](https://issues.apache.org/jira/browse/CASSANDRA-7847) | Allow quoted identifiers for triggers' names |  Minor | . | Mikhail Stepura | Mikhail Stepura |
| [CASSANDRA-7869](https://issues.apache.org/jira/browse/CASSANDRA-7869) | NPE while freezing a tuple containing a list |  Major | . | Olivier Michallat | Olivier Michallat |
| [CASSANDRA-7879](https://issues.apache.org/jira/browse/CASSANDRA-7879) | Internal c\* datatypes exposed via jmx and method signatures changed in 2.1 |  Trivial | Observability | Philip S Doctor | Marcus Eriksson |
| [CASSANDRA-7863](https://issues.apache.org/jira/browse/CASSANDRA-7863) | cqlsh: Use frozen\<\> for tuples and UDTs in DESCRIBE output |  Major | Tools | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-7892](https://issues.apache.org/jira/browse/CASSANDRA-7892) | Hashmap IllegalStateException in anticompaction |  Major | . | Johan Bjork | Mikhail Stepura |
| [CASSANDRA-7895](https://issues.apache.org/jira/browse/CASSANDRA-7895) | ALTER TYPE \<name\> RENAME TO \<name\> no longer parses as valid cql |  Minor | . | Russ Hatch | Mikhail Stepura |
| [CASSANDRA-7094](https://issues.apache.org/jira/browse/CASSANDRA-7094) | cqlsh: DESCRIBE is not case-insensitive |  Trivial | Tools | Karl Mueller | Philip Thompson |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7718](https://issues.apache.org/jira/browse/CASSANDRA-7718) | dtest cql\_tests.py:TestCQL.cql3\_insert\_thrift\_test fails intermittently |  Trivial | Testing | Michael Shuler | Philip Thompson |
| [CASSANDRA-5687](https://issues.apache.org/jira/browse/CASSANDRA-5687) | Move C\* Python Thrift tests into dtests |  Minor | . | Ryan McGuire | Ryan McGuire |


