
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.1.1 - 2012-06-04



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2506](https://issues.apache.org/jira/browse/CASSANDRA-2506) | Push read repair setting down to the DC-level |  Minor | . | Brandon Williams | Vijay |
| [CASSANDRA-3936](https://issues.apache.org/jira/browse/CASSANDRA-3936) | Gossip should have a 'goodbye' command to indicate shutdown |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3912](https://issues.apache.org/jira/browse/CASSANDRA-3912) | repair user provided custom token range (support incremental repair controlled by external agent) |  Major | . | Peter Schuller | Peter Schuller |
| [CASSANDRA-4211](https://issues.apache.org/jira/browse/CASSANDRA-4211) | Add sstablemetadata debugging tool |  Minor | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1974](https://issues.apache.org/jira/browse/CASSANDRA-1974) | PFEPS-like snitch that uses gossip instead of a property file |  Minor | . | Brandon Williams | Brandon Williams |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3721](https://issues.apache.org/jira/browse/CASSANDRA-3721) | Staggering repair |  Minor | . | Vijay | Vijay |
| [CASSANDRA-3412](https://issues.apache.org/jira/browse/CASSANDRA-3412) | make nodetool ring ownership smarter |  Minor | . | Jackson Chung | Vijay |
| [CASSANDRA-3949](https://issues.apache.org/jira/browse/CASSANDRA-3949) | [patch] presize arraylists where possible |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-3610](https://issues.apache.org/jira/browse/CASSANDRA-3610) | Checksum improvement for CommitLog |  Minor | . | Vijay | Vijay |
| [CASSANDRA-3012](https://issues.apache.org/jira/browse/CASSANDRA-3012) | cassandra-cli list \<CF\> limit number of columns returned |  Minor | . | Aaron Turner | Dave Brosius |
| [CASSANDRA-3971](https://issues.apache.org/jira/browse/CASSANDRA-3971) | [patch] make hasSameReplication safer/more readable |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-3969](https://issues.apache.org/jira/browse/CASSANDRA-3969) | [patch] remove duplicate code |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-2261](https://issues.apache.org/jira/browse/CASSANDRA-2261) | During Compaction, Corrupt SSTables with rows that cause failures should be identified and blacklisted. |  Minor | . | Benjamin Coverston | Pavel Yaskevich |
| [CASSANDRA-3978](https://issues.apache.org/jira/browse/CASSANDRA-3978) | [patch] quell all the warnings around DecoratorKey and generics |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-4080](https://issues.apache.org/jira/browse/CASSANDRA-4080) | Cut down on the comparisons needed during shouldPurge and needDeserialize |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4072](https://issues.apache.org/jira/browse/CASSANDRA-4072) | Clean up DataOutputBuffer |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4098](https://issues.apache.org/jira/browse/CASSANDRA-4098) | Listing wide-rows from CLI crashes Cassandra |  Minor | Tools | Brian ONeill | Brian ONeill |
| [CASSANDRA-3966](https://issues.apache.org/jira/browse/CASSANDRA-3966) | KeyCacheKey and RowCacheKey to use raw byte[] |  Minor | . | Vijay | Vijay |
| [CASSANDRA-4092](https://issues.apache.org/jira/browse/CASSANDRA-4092) | Allow getting a simple Token-\>node map over thrift |  Major | . | Nick Bailey | Sam Tunnicliffe |
| [CASSANDRA-2635](https://issues.apache.org/jira/browse/CASSANDRA-2635) | make cache skipping optional |  Minor | . | Peter Schuller | Harish Doddi |
| [CASSANDRA-4146](https://issues.apache.org/jira/browse/CASSANDRA-4146) | sstableloader should detect and report failures |  Minor | Tools | Manish Zope | Brandon Williams |
| [CASSANDRA-4153](https://issues.apache.org/jira/browse/CASSANDRA-4153) | Optimize truncate when snapshots are disabled or keyspace not durable |  Minor | . | Christian Spriegel | Christian Spriegel |
| [CASSANDRA-4079](https://issues.apache.org/jira/browse/CASSANDRA-4079) | Check SSTable range before running cleanup |  Minor | . | Benjamin Coverston | Jonathan Ellis |
| [CASSANDRA-556](https://issues.apache.org/jira/browse/CASSANDRA-556) | nodeprobe snapshot to support specific column families |  Minor | . | Chris Were | Dave Brosius |
| [CASSANDRA-4155](https://issues.apache.org/jira/browse/CASSANDRA-4155) | Make possible to authenticate user when loading data to Cassandra with BulkRecordWriter. |  Minor | . | Michał Michalski | Michał Michalski |
| [CASSANDRA-4052](https://issues.apache.org/jira/browse/CASSANDRA-4052) | Add way to force the cassandra-cli to refresh it's schema |  Minor | Tools | Tupshin Harper | Dave Brosius |
| [CASSANDRA-4174](https://issues.apache.org/jira/browse/CASSANDRA-4174) | Unnecessary compaction happens when streaming |  Minor | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-3837](https://issues.apache.org/jira/browse/CASSANDRA-3837) | [patch] improve performance of JdbcDecimal.decompose |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-4184](https://issues.apache.org/jira/browse/CASSANDRA-4184) | Make identifier and value grammar for CQL3 stricter |  Major | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2889](https://issues.apache.org/jira/browse/CASSANDRA-2889) | Avoids having replicate on write tasks stacking up at CL.ONE |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4173](https://issues.apache.org/jira/browse/CASSANDRA-4173) | cqlsh: in cql3 mode, use cql3 quoting when outputting cql |  Minor | Tools | paul cannon | paul cannon |
| [CASSANDRA-4186](https://issues.apache.org/jira/browse/CASSANDRA-4186) | CQL3: make some keywords unreserved |  Minor | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4187](https://issues.apache.org/jira/browse/CASSANDRA-4187) | CQL3: move {max/min}\_compaction\_thresholds to compaction options |  Trivial | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4222](https://issues.apache.org/jira/browse/CASSANDRA-4222) | Improve serializing off-heap cache by not using finalizers |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-4194](https://issues.apache.org/jira/browse/CASSANDRA-4194) | CQL3: improve experience with time uuid |  Minor | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4224](https://issues.apache.org/jira/browse/CASSANDRA-4224) | more user friendly error messages from nodetool on common connection failures |  Trivial | . | Noa Resare | Noa Resare |
| [CASSANDRA-4197](https://issues.apache.org/jira/browse/CASSANDRA-4197) | Index caching |  Minor | . | Jonathan Ellis | Yuki Morishita |
| [CASSANDRA-1991](https://issues.apache.org/jira/browse/CASSANDRA-1991) | CFS.maybeSwitchMemtable() calls CommitLog.instance.getContext(), which may block, under flusher lock write lock |  Major | . | Peter Schuller | Jonathan Ellis |
| [CASSANDRA-4142](https://issues.apache.org/jira/browse/CASSANDRA-4142) | OOM Exception during repair session with LeveledCompactionStrategy |  Major | . | Romain Hardouin | Sylvain Lebresne |
| [CASSANDRA-4178](https://issues.apache.org/jira/browse/CASSANDRA-4178) | JNA link failure logging confusing on OSX |  Trivial | . | Noa Resare | Noa Resare |
| [CASSANDRA-1404](https://issues.apache.org/jira/browse/CASSANDRA-1404) | Improve pre-cleanup estimate of disk space required |  Minor | . | Stu Hood | Yuki Morishita |
| [CASSANDRA-4010](https://issues.apache.org/jira/browse/CASSANDRA-4010) | Report thrift status in nodetool info |  Minor | Tools | Radim Kolar | Dave Brosius |
| [CASSANDRA-4242](https://issues.apache.org/jira/browse/CASSANDRA-4242) | Name of parameters should be available in CqlPreparedResult |  Minor | . | Pierre Chalamet | Pierre Chalamet |
| [CASSANDRA-4198](https://issues.apache.org/jira/browse/CASSANDRA-4198) | cqlsh: update recognized syntax for cql3 |  Minor | Tools | paul cannon | paul cannon |
| [CASSANDRA-4280](https://issues.apache.org/jira/browse/CASSANDRA-4280) | 2I CFs should inherit parent compaction settings |  Trivial | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4238](https://issues.apache.org/jira/browse/CASSANDRA-4238) | Pig secondary index usage could be improved |  Major | Secondary Indexes | Brandon Williams | Brandon Williams |
| [CASSANDRA-4199](https://issues.apache.org/jira/browse/CASSANDRA-4199) | There should be an easy way to find out which sstables a key lives in |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-4301](https://issues.apache.org/jira/browse/CASSANDRA-4301) | minor changes in pig examples |  Trivial | . | Fabien Rousseau |  |
| [CASSANDRA-4264](https://issues.apache.org/jira/browse/CASSANDRA-4264) | live ratio limit 64.0 is way too low |  Trivial | . | Radim Kolar | Sylvain Lebresne |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3867](https://issues.apache.org/jira/browse/CASSANDRA-3867) | Disablethrift and Enablethrift can leaves behind zombie connections on THSHA server |  Major | . | Vijay | Vijay |
| [CASSANDRA-3948](https://issues.apache.org/jira/browse/CASSANDRA-3948) | rename RandomAccessReader.MAX\_BYTES\_IN\_PAGE\_CACHE |  Major | . | Peter Schuller | Pavel Yaskevich |
| [CASSANDRA-3993](https://issues.apache.org/jira/browse/CASSANDRA-3993) | JavaDoc fix for org.apache.cassandra.db.filter.QueryFilter |  Trivial | Documentation and Website | Dmitry Petrashko | Dmitry Petrashko |
| [CASSANDRA-4056](https://issues.apache.org/jira/browse/CASSANDRA-4056) | [patch] guard against npe due to null sstable |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-3690](https://issues.apache.org/jira/browse/CASSANDRA-3690) | Streaming CommitLog backup |  Minor | Tools | Vijay | Vijay |
| [CASSANDRA-4100](https://issues.apache.org/jira/browse/CASSANDRA-4100) | Make scrub and cleanup operations throttled |  Minor | . | Vijay | Vijay |
| [CASSANDRA-4045](https://issues.apache.org/jira/browse/CASSANDRA-4045) | BOF fails when some nodes are down |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3909](https://issues.apache.org/jira/browse/CASSANDRA-3909) | Pig should handle wide rows |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-4166](https://issues.apache.org/jira/browse/CASSANDRA-4166) | BulkRecordWriter constructor crashes on wrong default parameter in conf.get() which is letter instead of number |  Major | . | Michał Michalski |  |
| [CASSANDRA-4181](https://issues.apache.org/jira/browse/CASSANDRA-4181) | Hadoop on CF with ColumnCounter columns fails |  Minor | . | Marco Cova | Marco Cova |
| [CASSANDRA-4190](https://issues.apache.org/jira/browse/CASSANDRA-4190) | Apparent data loss using super columns and row cache via ConcurrentLinkedHashCacheProvider |  Major | . | Mina Naguib | Sylvain Lebresne |
| [CASSANDRA-4168](https://issues.apache.org/jira/browse/CASSANDRA-4168) | "Setup" section of tools/stress/README.txt needs update |  Minor | Tools | Tyler Patterson | Brandon Williams |
| [CASSANDRA-4116](https://issues.apache.org/jira/browse/CASSANDRA-4116) | check most recent TS values in SSTables when a row tombstone has already been encountered |  Major | . | Matthew F. Dennis | Sam Tunnicliffe |
| [CASSANDRA-4160](https://issues.apache.org/jira/browse/CASSANDRA-4160) | ORDER BY ... DESC reverses comparrison predicates in WHERE |  Major | CQL | Jonas Dohse | Sylvain Lebresne |
| [CASSANDRA-4185](https://issues.apache.org/jira/browse/CASSANDRA-4185) | Minor CQL3 fixes |  Minor | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4202](https://issues.apache.org/jira/browse/CASSANDRA-4202) | CQL 3.0 prepare\_cql\_query fails on "BEGIN BATCH" |  Minor | CQL | Sean Billig | Sylvain Lebresne |
| [CASSANDRA-3741](https://issues.apache.org/jira/browse/CASSANDRA-3741) | OOMs because delete operations are not accounted |  Major | . | Vitalii Tymchyshyn | Andriy Kolyadenko |
| [CASSANDRA-3985](https://issues.apache.org/jira/browse/CASSANDRA-3985) | Ensure a directory is selected for Compaction |  Minor | . | amorton | Jonathan Ellis |
| [CASSANDRA-4205](https://issues.apache.org/jira/browse/CASSANDRA-4205) | SSTables are not updated with max timestamp on upgradesstables/compaction leading to non-optimal performance. |  Critical | . | Thorkild Stray | Jonathan Ellis |
| [CASSANDRA-4200](https://issues.apache.org/jira/browse/CASSANDRA-4200) | Move error on 1 node cluster |  Major | . | Nick Bailey | Sylvain Lebresne |
| [CASSANDRA-4183](https://issues.apache.org/jira/browse/CASSANDRA-4183) | Fix dependency versions in generated pos |  Major | Packaging | Stephen Connolly | Stephen Connolly |
| [CASSANDRA-4164](https://issues.apache.org/jira/browse/CASSANDRA-4164) | Cqlsh should support DESCRIBE on cql3-style composite CFs |  Major | . | Nick Bailey | paul cannon |
| [CASSANDRA-4046](https://issues.apache.org/jira/browse/CASSANDRA-4046) | when creating keyspace with simple strategy, it should only acception "replication\_factor" as an option |  Minor | . | Jackson Chung | Dave Brosius |
| [CASSANDRA-4061](https://issues.apache.org/jira/browse/CASSANDRA-4061) | Decommission should take a token |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-4192](https://issues.apache.org/jira/browse/CASSANDRA-4192) | CQL3: fix index dropping and assign default name if none provided at index creation |  Major | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4219](https://issues.apache.org/jira/browse/CASSANDRA-4219) | Problem with creating keyspace after drop |  Major | . | Jeff Williams | Pavel Yaskevich |
| [CASSANDRA-4230](https://issues.apache.org/jira/browse/CASSANDRA-4230) | Deleting a CF always produces an error and that CF remains in an unknown state |  Major | . | André Cruz | Pavel Yaskevich |
| [CASSANDRA-4250](https://issues.apache.org/jira/browse/CASSANDRA-4250) | Missing arrayOffset in FBUtilities.hash |  Critical | . | Richard Low | Richard Low |
| [CASSANDRA-4249](https://issues.apache.org/jira/browse/CASSANDRA-4249) | LOGGING: Info log is not displaying number of rows read from saved cache at startup |  Trivial | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-4246](https://issues.apache.org/jira/browse/CASSANDRA-4246) | cql3 ORDER BY not ordering |  Major | . | paul cannon | Sylvain Lebresne |
| [CASSANDRA-4252](https://issues.apache.org/jira/browse/CASSANDRA-4252) | Set operation mode to MOVING earlier |  Minor | . | Nick Bailey | Jonathan Ellis |
| [CASSANDRA-4233](https://issues.apache.org/jira/browse/CASSANDRA-4233) | overlapping sstables in leveled compaction strategy |  Major | . | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-4223](https://issues.apache.org/jira/browse/CASSANDRA-4223) | Non Unique Streaming session ID's |  Major | . | amorton | amorton |
| [CASSANDRA-4259](https://issues.apache.org/jira/browse/CASSANDRA-4259) | Bug in SSTableReader.getSampleIndexesForRanges(...) causes uneven InputSplits generation for Hadoop mappers |  Major | . | Bartłomiej Romański | Bartłomiej Romański |
| [CASSANDRA-4255](https://issues.apache.org/jira/browse/CASSANDRA-4255) | concurrent modif ex when repair is run on LCS |  Minor | . | Jackson Chung | Jonathan Ellis |
| [CASSANDRA-4256](https://issues.apache.org/jira/browse/CASSANDRA-4256) | Include stress tool in debian packaging |  Minor | Tools | Nick Bailey | Nick Bailey |
| [CASSANDRA-4263](https://issues.apache.org/jira/browse/CASSANDRA-4263) | LeveledManifest.maxBytesForLevel calculates wrong for sstable\_size\_in\_mb larger than 512m |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-4269](https://issues.apache.org/jira/browse/CASSANDRA-4269) | Setting column metadata for non-string comparator CFs breaks |  Trivial | . | Tyler Hobbs | Sylvain Lebresne |
| [CASSANDRA-4115](https://issues.apache.org/jira/browse/CASSANDRA-4115) | UNREACHABLE schema after decommissioning a non-seed node |  Blocker | . | Tyler Patterson | Brandon Williams |
| [CASSANDRA-4266](https://issues.apache.org/jira/browse/CASSANDRA-4266) | Validate compression parameters |  Minor | . | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-4201](https://issues.apache.org/jira/browse/CASSANDRA-4201) | Preserve commitlog size cap when recycling segments at startup |  Minor | . | Pierre Chalamet | Jonathan Ellis |
| [CASSANDRA-4270](https://issues.apache.org/jira/browse/CASSANDRA-4270) | long-test broken due to incorrect config option |  Minor | Testing | Tyler Patterson | Tyler Patterson |
| [CASSANDRA-4257](https://issues.apache.org/jira/browse/CASSANDRA-4257) | CQL3 range query with secondary index fails |  Minor | CQL, Secondary Indexes | Sean Billig | Sylvain Lebresne |
| [CASSANDRA-4279](https://issues.apache.org/jira/browse/CASSANDRA-4279) | kick off background compaction when min/max changed |  Trivial | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4287](https://issues.apache.org/jira/browse/CASSANDRA-4287) | SizeTieredCompactionStrategy.getBuckets is quadradic in the number of sstables |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4221](https://issues.apache.org/jira/browse/CASSANDRA-4221) | Error while deleting a columnfamily that is being compacted. |  Major | . | Tyler Patterson | Pavel Yaskevich |
| [CASSANDRA-4278](https://issues.apache.org/jira/browse/CASSANDRA-4278) | Can't specify certain keyspace properties in CQL |  Minor | . | paul cannon | Sylvain Lebresne |
| [CASSANDRA-4276](https://issues.apache.org/jira/browse/CASSANDRA-4276) | Multiple SLF4J bindings warning when running stress |  Major | Tools | Mike Bulman | Nick Bailey |
| [CASSANDRA-4296](https://issues.apache.org/jira/browse/CASSANDRA-4296) | CQL3: create table don't always validate access to the right keyspace |  Minor | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4291](https://issues.apache.org/jira/browse/CASSANDRA-4291) | Oversize integer in CQL throws NumberFormatException |  Minor | CQL | Scott Sadler | Dave Brosius |
| [CASSANDRA-4302](https://issues.apache.org/jira/browse/CASSANDRA-4302) | cassandra-stress scripts should be executable |  Trivial | Tools | Tyler Hobbs | Yuki Morishita |
| [CASSANDRA-3882](https://issues.apache.org/jira/browse/CASSANDRA-3882) | avoid distributed deadlock in migration stage |  Major | . | Peter Schuller | Richard Low |
| [CASSANDRA-4294](https://issues.apache.org/jira/browse/CASSANDRA-4294) | Upgrading encounters: 'SimpleStrategy requires a replication\_factor strategy option.' and refuses to start |  Major | . | Brandon Williams | Sylvain Lebresne |
| [CASSANDRA-4262](https://issues.apache.org/jira/browse/CASSANDRA-4262) | 1.1 does not preserve compatibility w/ index queries against 1.0 nodes |  Critical | . | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-4150](https://issues.apache.org/jira/browse/CASSANDRA-4150) | Allow larger cache capacities than 2GB |  Major | . | Vijay | Vijay |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4004](https://issues.apache.org/jira/browse/CASSANDRA-4004) | Add support for ReversedType |  Trivial | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3771](https://issues.apache.org/jira/browse/CASSANDRA-3771) | Allow paging through non-ordered partitioner results in CQL3 |  Minor | CQL | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-3982](https://issues.apache.org/jira/browse/CASSANDRA-3982) | Explore not returning range ghosts |  Major | CQL | Sylvain Lebresne | Sylvain Lebresne |


