
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra  2.1.6 Release Notes

These release notes cover new developer and user-facing incompatibilities, important issues, features, and major improvements.


---

* [CASSANDRA-9489](https://issues.apache.org/jira/browse/CASSANDRA-9489) | *Major* | **Testcase failure: testExpiredTombstones(org.apache.cassandra.cql3.SliceQueryFilterWithTombstonesTest)**

While executing the test cases, I observed the following failure,

[junit] ERROR 08:42:40 Scanned over 100 tombstones in cql\_test\_keyspace.table\_4; query aborted (see tombstone\_failure\_threshold)
    [junit] ERROR 08:42:40 Scanned over 100 tombstones in cql\_test\_keyspace.table\_4; query aborted (see tombstone\_failure\_threshold)
    [junit] ------------- ---------------- ---------------
    [junit] Testcase: testExpiredTombstones(org.apache.cassandra.cql3.SliceQueryFilterWithTombstonesTest):      FAILED
    [junit] null
    [junit] junit.framework.AssertionFailedError: null
    [junit]     at org.apache.cassandra.cql3.SliceQueryFilterWithTombstonesTest.testExpiredTombstones(SliceQueryFilterWithTombstonesTest.java:155)



