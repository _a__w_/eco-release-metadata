
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1.6 - 2015-06-08



### IMPORTANT ISSUES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9489](https://issues.apache.org/jira/browse/CASSANDRA-9489) | Testcase failure: testExpiredTombstones(org.apache.cassandra.cql3.SliceQueryFilterWithTombstonesTest) |  Major | Testing | Pallavi Bhardwaj |  |


### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5969](https://issues.apache.org/jira/browse/CASSANDRA-5969) | Allow JVM\_OPTS to be passed to sstablescrub |  Major | Tools | Adam Hattrell | Stefania |
| [CASSANDRA-9436](https://issues.apache.org/jira/browse/CASSANDRA-9436) | Expose rpc\_address and broadcast\_address of each Cassandra node |  Major | Distributed Metadata | Piotr Kołaczkowski | Carl Yeksigian |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9029](https://issues.apache.org/jira/browse/CASSANDRA-9029) | Add utility class to support for rate limiting a given log statement |  Major | Observability | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-9262](https://issues.apache.org/jira/browse/CASSANDRA-9262) | IncomingTcpConnection thread is not named |  Trivial | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-9140](https://issues.apache.org/jira/browse/CASSANDRA-9140) | Scrub should handle corrupted compressed chunks |  Major | Tools | Tyler Hobbs | Stefania |
| [CASSANDRA-9261](https://issues.apache.org/jira/browse/CASSANDRA-9261) | Prepare and Snapshot for repairs should use higher timeouts for expiring map |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-8561](https://issues.apache.org/jira/browse/CASSANDRA-8561) | Tombstone log warning does not log partition key |  Major | . | Jens Rantil | Lyuben Todorov |
| [CASSANDRA-8773](https://issues.apache.org/jira/browse/CASSANDRA-8773) | cassandra-stress should validate its results in "user" mode |  Major | Tools | Benedict | Benedict |
| [CASSANDRA-8717](https://issues.apache.org/jira/browse/CASSANDRA-8717) | Top-k queries with custom secondary indexes |  Minor | Secondary Indexes | Andrés de la Peña | Andrés de la Peña |
| [CASSANDRA-9298](https://issues.apache.org/jira/browse/CASSANDRA-9298) | CollationController.collectAllData can be too conservative with sstable elimination |  Trivial | . | Benedict | Benedict |
| [CASSANDRA-9314](https://issues.apache.org/jira/browse/CASSANDRA-9314) | Overload SecondaryIndex#indexes to accept the column definition |  Major | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-9322](https://issues.apache.org/jira/browse/CASSANDRA-9322) | Possible overlap with LCS and including non-compacting sstables |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9286](https://issues.apache.org/jira/browse/CASSANDRA-9286) | Add Keyspace/Table details to CollectionType.java error message |  Minor | Observability | sequoyha pelletier | Carl Yeksigian |
| [CASSANDRA-9364](https://issues.apache.org/jira/browse/CASSANDRA-9364) | Add a warning when a high setting for rpc\_max\_threads is specified for HSHA |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-9282](https://issues.apache.org/jira/browse/CASSANDRA-9282) | Warn on unlogged batches |  Major | CQL | Jonathan Ellis | T Jake Luciani |
| [CASSANDRA-7212](https://issues.apache.org/jira/browse/CASSANDRA-7212) | Allow to switch user within CQLSH session |  Major | CQL | Jose Martinez Poblete | Carl Yeksigian |
| [CASSANDRA-9397](https://issues.apache.org/jira/browse/CASSANDRA-9397) | Wrong gc\_grace\_seconds used in anticompaction |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9417](https://issues.apache.org/jira/browse/CASSANDRA-9417) | Do not swallow RejectedExecutionExceptions at shutdown |  Minor | Lifecycle | Sergio Bossa | T Jake Luciani |
| [CASSANDRA-8603](https://issues.apache.org/jira/browse/CASSANDRA-8603) | Cut tombstone memory footprint in half for cql deletes |  Major | Local Write-Read Paths | Dominic Letz | Benjamin Lerer |
| [CASSANDRA-9183](https://issues.apache.org/jira/browse/CASSANDRA-9183) | Failure detector should detect and ignore local pauses |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-9107](https://issues.apache.org/jira/browse/CASSANDRA-9107) | More accurate row count estimates |  Major | . | Chris Lohfink | Chris Lohfink |
| [CASSANDRA-9224](https://issues.apache.org/jira/browse/CASSANDRA-9224) | Figure out a better default float precision rule for cqlsh |  Major | Tools | Tyler Hobbs | Stefania |
| [CASSANDRA-9504](https://issues.apache.org/jira/browse/CASSANDRA-9504) | Odd performance numbers comparing 2.0.15 and 2.1+ |  Major | . | Philip Thompson | Jonathan Ellis |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9148](https://issues.apache.org/jira/browse/CASSANDRA-9148) | Issue when modifying UDT |  Major | . | Oskar Kjellin | Jeff Jirsa |
| [CASSANDRA-9192](https://issues.apache.org/jira/browse/CASSANDRA-9192) | Tuple columns with UDTs not updated when the UDT is altered |  Minor | . | Tyler Hobbs | Benjamin Lerer |
| [CASSANDRA-9198](https://issues.apache.org/jira/browse/CASSANDRA-9198) | Deleting from an empty list produces an error |  Minor | CQL | Olivier Michallat | Jeff Jirsa |
| [CASSANDRA-9238](https://issues.apache.org/jira/browse/CASSANDRA-9238) | Race condition after shutdown gossip message |  Minor | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-9124](https://issues.apache.org/jira/browse/CASSANDRA-9124) | GCInspector logs very different times after CASSANDRA-7638 |  Minor | Observability | Jeremiah Jordan | Ariel Weisberg |
| [CASSANDRA-9234](https://issues.apache.org/jira/browse/CASSANDRA-9234) | Disable single-sstable tombstone compactions for DTCS |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9195](https://issues.apache.org/jira/browse/CASSANDRA-9195) | PITR commitlog replay only actually replays mutation every other time |  Major | . | Jon Moses | Branimir Lambov |
| [CASSANDRA-9249](https://issues.apache.org/jira/browse/CASSANDRA-9249) | Resetting local schema can cause assertion error |  Minor | Distributed Metadata | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-9194](https://issues.apache.org/jira/browse/CASSANDRA-9194) | Delete-only workloads crash Cassandra |  Major | . | Robert Wille | Benedict |
| [CASSANDRA-9281](https://issues.apache.org/jira/browse/CASSANDRA-9281) | Index selection during rebuild fails with certain table layouts. |  Major | Streaming and Messaging | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-9151](https://issues.apache.org/jira/browse/CASSANDRA-9151) | Anti-compaction is blocking ANTI\_ENTROPY stage |  Major | Streaming and Messaging | sankalp kohli | Yuki Morishita |
| [CASSANDRA-9251](https://issues.apache.org/jira/browse/CASSANDRA-9251) | Dropping a table while compacting causes exceptions |  Minor | . | T Jake Luciani | Benedict |
| [CASSANDRA-9290](https://issues.apache.org/jira/browse/CASSANDRA-9290) | Fix possible NPE in Scrubber |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-8051](https://issues.apache.org/jira/browse/CASSANDRA-8051) | Add SERIAL and LOCAL\_SERIAL consistency levels to cqlsh |  Minor | Tools | Nicolas Favre-Felix | Carl Yeksigian |
| [CASSANDRA-9057](https://issues.apache.org/jira/browse/CASSANDRA-9057) | index validation fails for non-indexed column |  Major | CQL | Eric Evans | Carl Yeksigian |
| [CASSANDRA-9136](https://issues.apache.org/jira/browse/CASSANDRA-9136) | Improve error handling when table is queried before the schema has fully propagated |  Major | . | Russell Spitzer | Tyler Hobbs |
| [CASSANDRA-8606](https://issues.apache.org/jira/browse/CASSANDRA-8606) | sstablesplit does not remove original sstable |  Major | . | Marcus Eriksson | Branimir Lambov |
| [CASSANDRA-9335](https://issues.apache.org/jira/browse/CASSANDRA-9335) | JVM\_EXTRA\_OPTS not getting picked up by windows startup environment |  Major | Packaging | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-9339](https://issues.apache.org/jira/browse/CASSANDRA-9339) | Commit Log completed tasks incremented during getCompletedTasks |  Minor | . | graham sanderson | graham sanderson |
| [CASSANDRA-9299](https://issues.apache.org/jira/browse/CASSANDRA-9299) | Fix counting of tombstones towards TombstoneOverwhelmingException |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-9361](https://issues.apache.org/jira/browse/CASSANDRA-9361) | Add all possible consistency levels to Cassandra-Stress and make LOCAL\_ONE the default one |  Minor | Tools | Mario Lazaro | Mario Lazaro |
| [CASSANDRA-8564](https://issues.apache.org/jira/browse/CASSANDRA-8564) | Harmless exception logged as ERROR |  Minor | Lifecycle | Philip Thompson | T Jake Luciani |
| [CASSANDRA-9300](https://issues.apache.org/jira/browse/CASSANDRA-9300) | token-generator - generated tokens too long |  Minor | Tools | Robert Stupp | Robert Stupp |
| [CASSANDRA-9310](https://issues.apache.org/jira/browse/CASSANDRA-9310) | Table change response returns as keyspace change response |  Major | CQL | Kishan Karunaratne | Carl Yeksigian |
| [CASSANDRA-8940](https://issues.apache.org/jira/browse/CASSANDRA-8940) | Inconsistent select count and select distinct |  Major | Local Write-Read Paths | Frens Jan Rumph | Benjamin Lerer |
| [CASSANDRA-9097](https://issues.apache.org/jira/browse/CASSANDRA-9097) | Repeated incremental nodetool repair results in failed repairs due to running anticompaction |  Minor | Streaming and Messaging | Gustav Munkby | Yuki Morishita |
| [CASSANDRA-9396](https://issues.apache.org/jira/browse/CASSANDRA-9396) | Canonical view of compacting SSTables not working as expected |  Major | . | Yuki Morishita | Benedict |
| [CASSANDRA-9295](https://issues.apache.org/jira/browse/CASSANDRA-9295) | Streaming not holding on to refs long enough. |  Major | Streaming and Messaging | Jeremiah Jordan | Yuki Morishita |
| [CASSANDRA-9334](https://issues.apache.org/jira/browse/CASSANDRA-9334) | Returning null from a trigger does not abort the write |  Major | Coordination | Brandon Williams | Sam Tunnicliffe |
| [CASSANDRA-9406](https://issues.apache.org/jira/browse/CASSANDRA-9406) | Add Option to Not Validate Atoms During Scrub |  Minor | Tools | Jordan West | Jordan West |
| [CASSANDRA-9466](https://issues.apache.org/jira/browse/CASSANDRA-9466) | CacheService.KeyCacheSerializer affects cache hit rate |  Minor | Tools | sankalp kohli | sankalp kohli |
| [CASSANDRA-8502](https://issues.apache.org/jira/browse/CASSANDRA-8502) | Static columns returning null for pages after first |  Major | . | Flavien Charlon | Tyler Hobbs |
| [CASSANDRA-9508](https://issues.apache.org/jira/browse/CASSANDRA-9508) | Avoid early opening of compaction results when doing anticompaction |  Critical | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9485](https://issues.apache.org/jira/browse/CASSANDRA-9485) | RangeTombstoneListTest.addAllRandomTest failed on trunk |  Minor | Local Write-Read Paths | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-9492](https://issues.apache.org/jira/browse/CASSANDRA-9492) | Error message changes based on jdk used |  Major | Testing | Philip Thompson | Carl Yeksigian |
| [CASSANDRA-9478](https://issues.apache.org/jira/browse/CASSANDRA-9478) | SSTables are not always properly marked suspected |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-9388](https://issues.apache.org/jira/browse/CASSANDRA-9388) | Truncate can (in theory) lead to corrupt responses to client or segfault |  Minor | . | Benedict | Benedict |
| [CASSANDRA-9486](https://issues.apache.org/jira/browse/CASSANDRA-9486) | LazilyCompactedRow accumulates all expired RangeTombstones |  Critical | . | Benedict | Sylvain Lebresne |
| [CASSANDRA-9071](https://issues.apache.org/jira/browse/CASSANDRA-9071) | CQLSSTableWriter gives java.lang.AssertionError: Empty partition |  Major | Tools | Ajit Joglekar | Fabien Rousseau |
| [CASSANDRA-8487](https://issues.apache.org/jira/browse/CASSANDRA-8487) | system.schema\_columns sometimes missing for 'system' keyspace |  Minor | . | Adam Holmberg | Aleksey Yeschenko |
| [CASSANDRA-9083](https://issues.apache.org/jira/browse/CASSANDRA-9083) | cqlsh COPY functionality doesn't work together with SOURCE or with cqlsh -f |  Minor | Tools | Joseph Chu | Tyler Hobbs |
| [CASSANDRA-9132](https://issues.apache.org/jira/browse/CASSANDRA-9132) | resumable\_bootstrap\_test can hang |  Major | Testing | Tyler Hobbs | Yuki Morishita |
| [CASSANDRA-9451](https://issues.apache.org/jira/browse/CASSANDRA-9451) | Startup message response for unsupported protocol versions is incorrect |  Major | CQL | Jorge Bay | Stefania |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9288](https://issues.apache.org/jira/browse/CASSANDRA-9288) | LongLeveledCompactionStrategyTest is failing |  Major | Testing | Ariel Weisberg | Stefania |
| [CASSANDRA-9287](https://issues.apache.org/jira/browse/CASSANDRA-9287) | CQLSSTableWriterLongTest is failing |  Major | Testing | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-9306](https://issues.apache.org/jira/browse/CASSANDRA-9306) | Test coverage for cqlsh COPY |  Major | . | Tyler Hobbs | Jim Witschey |
| [CASSANDRA-8656](https://issues.apache.org/jira/browse/CASSANDRA-8656) | long-test LongLeveledCompactionStrategyTest flaps in 2.0 |  Minor | Testing | Michael Shuler | Stefania |
| [CASSANDRA-9271](https://issues.apache.org/jira/browse/CASSANDRA-9271) | IndexSummaryManagerTest.testCompactionRace times out periodically |  Trivial | Testing | Ariel Weisberg | Ariel Weisberg |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7558](https://issues.apache.org/jira/browse/CASSANDRA-7558) | Document users and permissions in CQL docs |  Minor | Documentation and Website | Tyler Hobbs | Sam Tunnicliffe |


