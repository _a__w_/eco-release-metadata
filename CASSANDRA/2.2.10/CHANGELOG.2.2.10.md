
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.2.10 - 2017-06-26



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13230](https://issues.apache.org/jira/browse/CASSANDRA-13230) | Build RPM packages for release |  Major | Packaging | Michael Shuler | Stefan Podkowinski |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13388](https://issues.apache.org/jira/browse/CASSANDRA-13388) | Add hosted CI config files (such as Circle and TravisCI) for easier developer testing |  Major | Testing | Jeff Jirsa | Jeff Jirsa |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12100](https://issues.apache.org/jira/browse/CASSANDRA-12100) | Compactions are stuck after TRUNCATE |  Major | Compaction | Stefano Ortolani | Stefania |
| [CASSANDRA-13018](https://issues.apache.org/jira/browse/CASSANDRA-13018) | Exceptions encountered calling getSeeds() breaks OTC thread |  Major | Streaming and Messaging | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-13222](https://issues.apache.org/jira/browse/CASSANDRA-13222) | Paging with reverse queries and static columns may return incorrectly sized pages |  Major | CQL, Local Write-Read Paths | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-13108](https://issues.apache.org/jira/browse/CASSANDRA-13108) | Uncaught exeption stack traces not logged |  Trivial | Core | Christopher Batey | Christopher Batey |
| [CASSANDRA-12497](https://issues.apache.org/jira/browse/CASSANDRA-12497) | COPY ... TO STDOUT regression in 2.2.7 |  Major | Tools | Max Bowsher | Márton Salomváry |
| [CASSANDRA-12202](https://issues.apache.org/jira/browse/CASSANDRA-12202) | LongLeveledCompactionStrategyTest flapping in 2.1, 2.2, 3.0 |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-13090](https://issues.apache.org/jira/browse/CASSANDRA-13090) | Coalescing strategy sleeps too much |  Major | Streaming and Messaging | Corentin Chary | Corentin Chary |
| [CASSANDRA-13232](https://issues.apache.org/jira/browse/CASSANDRA-13232) | "multiple versions of ant detected in path for junit" printed for every junit test case spawned by "ant test" |  Major | Build | Michael Kjellman | Michael Kjellman |
| [CASSANDRA-13278](https://issues.apache.org/jira/browse/CASSANDRA-13278) | Update build.xml and build.properties.default maven repos |  Minor | Build | Michael Shuler | Robert Stupp |
| [CASSANDRA-12886](https://issues.apache.org/jira/browse/CASSANDRA-12886) | Streaming failed due to SSL Socket connection reset |  Major | . | Bing Wu | Paulo Motta |
| [CASSANDRA-13231](https://issues.apache.org/jira/browse/CASSANDRA-13231) | org.apache.cassandra.db.DirectoriesTest(testStandardDirs) unit test failing |  Major | . | Michael Kjellman | Michael Kjellman |
| [CASSANDRA-13053](https://issues.apache.org/jira/browse/CASSANDRA-13053) | GRANT/REVOKE on table without keyspace performs permissions check incorrectly |  Minor | CQL | Sam Tunnicliffe | Aleksey Yeschenko |
| [CASSANDRA-13282](https://issues.apache.org/jira/browse/CASSANDRA-13282) | Commitlog replay may fail if last mutation is within 4 bytes of end of segment |  Major | Core | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-13343](https://issues.apache.org/jira/browse/CASSANDRA-13343) | Wrong logger name in AnticompactionTask |  Trivial | . | Simon Zhou | Simon Zhou |
| [CASSANDRA-13153](https://issues.apache.org/jira/browse/CASSANDRA-13153) | Reappeared Data when Mixing Incremental and Full Repairs |  Major | Compaction, Tools | Amanda Debrot | Stefan Podkowinski |
| [CASSANDRA-12653](https://issues.apache.org/jira/browse/CASSANDRA-12653) | In-flight shadow round requests |  Minor | Distributed Metadata | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-12773](https://issues.apache.org/jira/browse/CASSANDRA-12773) | cassandra-stress error for one way SSL |  Major | Tools | Jane Deng | Stefan Podkowinski |
| [CASSANDRA-13316](https://issues.apache.org/jira/browse/CASSANDRA-13316) | Build error because of dependent jar (byteman-install-3.0.3.jar) currupted |  Major | Testing | Sam Ding |  |
| [CASSANDRA-12825](https://issues.apache.org/jira/browse/CASSANDRA-12825) | testall failure in org.apache.cassandra.db.compaction.CompactionsCQLTest.testTriggerMinorCompactionDTCS-compression |  Major | . | Sean McCarthy | Marcus Eriksson |
| [CASSANDRA-13103](https://issues.apache.org/jira/browse/CASSANDRA-13103) | incorrect jvm metric names |  Minor | Observability | Alain Rastoul | Alain Rastoul |
| [CASSANDRA-13364](https://issues.apache.org/jira/browse/CASSANDRA-13364) | Cqlsh COPY fails importing Map\<String,List\<String\>\>, ParseError unhashable type list |  Major | Tools | Nicolae Namolovan | Stefania |
| [CASSANDRA-13413](https://issues.apache.org/jira/browse/CASSANDRA-13413) | Run more test targets on CircleCI |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-13147](https://issues.apache.org/jira/browse/CASSANDRA-13147) | Secondary index query on partition key columns might not return all the rows. |  Major | Secondary Indexes | Benjamin Lerer | Andrés de la Peña |
| [CASSANDRA-13393](https://issues.apache.org/jira/browse/CASSANDRA-13393) | Fix weightedSize() for row-cache reported by JMX and NodeTool |  Minor | Tools | Fuud | Fuud |
| [CASSANDRA-13092](https://issues.apache.org/jira/browse/CASSANDRA-13092) | Debian package does not install the hostpot\_compiler command file |  Trivial | Packaging | Jan Urbański | Michael Shuler |
| [CASSANDRA-13466](https://issues.apache.org/jira/browse/CASSANDRA-13466) | Set javac encoding to utf-8 in 2.1 and 2.2. |  Major | Build | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-12841](https://issues.apache.org/jira/browse/CASSANDRA-12841) | testall failure in org.apache.cassandra.db.compaction.NeverPurgeTest.minorNeverPurgeTombstonesTest-compression |  Major | . | Sean McCarthy | Marcus Eriksson |
| [CASSANDRA-13119](https://issues.apache.org/jira/browse/CASSANDRA-13119) | dtest failure upgrade\_tests.upgrade\_supercolumns\_test.TestSCUpgrade.upgrade\_super\_columns\_through\_all\_versions\_test |  Critical | Core, Testing | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-13412](https://issues.apache.org/jira/browse/CASSANDRA-13412) | Update of column with TTL results in secondary index not returning row |  Major | Secondary Indexes | Enrique Bautista Barahona | Andrés de la Peña |
| [CASSANDRA-13209](https://issues.apache.org/jira/browse/CASSANDRA-13209) | test failure in cqlsh\_tests.cqlsh\_copy\_tests.CqlshCopyTest.test\_bulk\_round\_trip\_blogposts\_with\_max\_connections |  Major | . | Michael Shuler | Kurt Greaves |
| [CASSANDRA-11381](https://issues.apache.org/jira/browse/CASSANDRA-11381) | Node running with join\_ring=false and authentication can not serve requests |  Major | . | mck | mck |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13434](https://issues.apache.org/jira/browse/CASSANDRA-13434) | Add PID file directive in /etc/init.d/cassandra |  Major | Packaging | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-13435](https://issues.apache.org/jira/browse/CASSANDRA-13435) | Incorrect status check use when stopping Cassandra |  Major | Packaging | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-13490](https://issues.apache.org/jira/browse/CASSANDRA-13490) | RPM Spec - disable binary check, improve readme instructions |  Major | Packaging | martin a langhoff | Stefan Podkowinski |
| [CASSANDRA-13493](https://issues.apache.org/jira/browse/CASSANDRA-13493) | RPM Init: Service startup ordering |  Major | Packaging | martin a langhoff | Stefan Podkowinski |
| [CASSANDRA-13440](https://issues.apache.org/jira/browse/CASSANDRA-13440) | Sign RPM artifacts |  Major | Packaging | Stefan Podkowinski | Michael Shuler |


