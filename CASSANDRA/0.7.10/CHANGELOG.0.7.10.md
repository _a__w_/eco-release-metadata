
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.7.10 - 2011-10-31



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2691](https://issues.apache.org/jira/browse/CASSANDRA-2691) | If an upload to central repository fails for an artifact, retry a couple of times before killing the build |  Major | . | Stephen Connolly | Stephen Connolly |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3164](https://issues.apache.org/jira/browse/CASSANDRA-3164) | GCInspector still not avoiding divide by zero |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3222](https://issues.apache.org/jira/browse/CASSANDRA-3222) | cfhistograms is transposed/wrong again |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3358](https://issues.apache.org/jira/browse/CASSANDRA-3358) | 2GB row size limit in ColumnIndex offset calculation |  Major | . | Thomas Richter | Thomas Richter |


