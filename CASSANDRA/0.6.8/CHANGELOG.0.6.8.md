
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.6.8 - 2010-11-12



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1719](https://issues.apache.org/jira/browse/CASSANDRA-1719) | improve read repair |  Minor | . | Jonathan Ellis | Jonathan Ellis |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1713](https://issues.apache.org/jira/browse/CASSANDRA-1713) | Windows batch files use incorrect paths |  Minor | Packaging | Vladimir Loncar | Vladimir Loncar |
| [CASSANDRA-1722](https://issues.apache.org/jira/browse/CASSANDRA-1722) | Unbounded key range only ever scans first node in ring |  Major | . | Jonathan Ellis | Stu Hood |
| [CASSANDRA-1727](https://issues.apache.org/jira/browse/CASSANDRA-1727) | Read repair IndexOutOfBoundsException |  Major | . | Jonathan Ellis | Jonathan Ellis |


