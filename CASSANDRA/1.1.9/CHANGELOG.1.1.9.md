
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.1.9 - 2013-01-18



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4767](https://issues.apache.org/jira/browse/CASSANDRA-4767) | Need some indication of node repair success or failure |  Minor | Tools | Ahmed Bashir | Yuki Morishita |
| [CASSANDRA-5135](https://issues.apache.org/jira/browse/CASSANDRA-5135) | calculatePendingRanges could be asynchronous |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-5109](https://issues.apache.org/jira/browse/CASSANDRA-5109) | convert default marshallers list to map for better readability |  Trivial | . | Dave Brosius | Dave Brosius |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4492](https://issues.apache.org/jira/browse/CASSANDRA-4492) | HintsColumnFamily compactions hang when using multithreaded compaction |  Minor | . | Jason Harvey | Carl Yeksigian |
| [CASSANDRA-5053](https://issues.apache.org/jira/browse/CASSANDRA-5053) | not possible to change crc\_check\_chance |  Minor | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-5079](https://issues.apache.org/jira/browse/CASSANDRA-5079) | Compaction deletes ExpiringColumns in Secondary Indexes |  Major | Secondary Indexes | amorton | amorton |
| [CASSANDRA-5087](https://issues.apache.org/jira/browse/CASSANDRA-5087) | Changing from higher to lower compaction throughput causes long (multi hour) pause in large compactions |  Minor | . | J.B. Langston | J.B. Langston |
| [CASSANDRA-5110](https://issues.apache.org/jira/browse/CASSANDRA-5110) | NodeCmd misspells "positives" |  Trivial | . | Janne Jalkanen | Janne Jalkanen |
| [CASSANDRA-4564](https://issues.apache.org/jira/browse/CASSANDRA-4564) | MoveTest madness |  Minor | Testing | Eric Evans | Liu Yu |
| [CASSANDRA-5098](https://issues.apache.org/jira/browse/CASSANDRA-5098) | CassandraStorage doesn't decode name in widerow mode |  Major | . | Justen Walker | Brandon Williams |
| [CASSANDRA-5088](https://issues.apache.org/jira/browse/CASSANDRA-5088) | Major compaction IOException in 1.1.8 |  Major | . | Karl Mueller | Jonathan Ellis |
| [CASSANDRA-5089](https://issues.apache.org/jira/browse/CASSANDRA-5089) | get\_range\_slices does not validate end\_token |  Trivial | . | Aleksey Pesternikov | Jonathan Ellis |
| [CASSANDRA-5118](https://issues.apache.org/jira/browse/CASSANDRA-5118) | user defined compaction is broken |  Minor | . | Michael Kjellman | Yuki Morishita |
| [CASSANDRA-5106](https://issues.apache.org/jira/browse/CASSANDRA-5106) | Stock example for using pig throws InvalidRequestException(why:Start token sorts after end token) |  Major | . | janwar dinata | Jonathan Ellis |
| [CASSANDRA-5145](https://issues.apache.org/jira/browse/CASSANDRA-5145) | CQL3 BATCH authorization caching bug |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5099](https://issues.apache.org/jira/browse/CASSANDRA-5099) | Since 1.1, get\_count sometimes returns value smaller than actual column count |  Major | . | Jason Harvey | Yuki Morishita |
| [CASSANDRA-5137](https://issues.apache.org/jira/browse/CASSANDRA-5137) | Make sure SSTables left over from compaction get deleted and logged |  Minor | . | Yuki Morishita | Yuki Morishita |


