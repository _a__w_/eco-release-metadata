
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.6.7 - 2010-11-10



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1696](https://issues.apache.org/jira/browse/CASSANDRA-1696) | add --start-key to stress.py |  Major | Tools | Jonathan Ellis | Jonathan Ellis |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1622](https://issues.apache.org/jira/browse/CASSANDRA-1622) | optimize quorum read |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1636](https://issues.apache.org/jira/browse/CASSANDRA-1636) | Default JVM settings in cassandra.bat need some TLC |  Major | . | Nate McCall | Nate McCall |
| [CASSANDRA-1677](https://issues.apache.org/jira/browse/CASSANDRA-1677) | Log type of dropped messages |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1660](https://issues.apache.org/jira/browse/CASSANDRA-1660) | Log GC/tpstats info when messages are dropped. |  Minor | . | Brandon Williams | Jonathan Ellis |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1604](https://issues.apache.org/jira/browse/CASSANDRA-1604) | Database Descriptor has log message that mashes words |  Trivial | . | Edward Capriolo | Edward Capriolo |
| [CASSANDRA-1628](https://issues.apache.org/jira/browse/CASSANDRA-1628) | RedHat init script status is a no-op |  Minor | . | Adam Gray | Adam Gray |
| [CASSANDRA-1656](https://issues.apache.org/jira/browse/CASSANDRA-1656) | Hinted handoff does not replay data |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1638](https://issues.apache.org/jira/browse/CASSANDRA-1638) | initMetadata does not load partitioner from disk |  Minor | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-1686](https://issues.apache.org/jira/browse/CASSANDRA-1686) | o.a.c.dht.AbstractBounds missing serialVersionUID |  Minor | Tools | Eric Evans | Eric Evans |
| [CASSANDRA-1676](https://issues.apache.org/jira/browse/CASSANDRA-1676) | Avoid dropping messages off the client request path |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1697](https://issues.apache.org/jira/browse/CASSANDRA-1697) | Startup can fail if DNS lookup fails for seed node |  Trivial | . | Edward Capriolo | Jonathan Ellis |
| [CASSANDRA-1694](https://issues.apache.org/jira/browse/CASSANDRA-1694) | fix jna errno reporting |  Minor | . | Jonathan Ellis | Jonathan Ellis |


