
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.2.0 beta 2 - 2012-11-09



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4261](https://issues.apache.org/jira/browse/CASSANDRA-4261) | [patch] Support consistency-latency prediction in nodetool |  Minor | Tools | Peter Bailis | Peter Bailis |
| [CASSANDRA-4310](https://issues.apache.org/jira/browse/CASSANDRA-4310) | Multiple independent Level Compactions in Parallel |  Major | . | sankalp kohli | Yuki Morishita |
| [CASSANDRA-4285](https://issues.apache.org/jira/browse/CASSANDRA-4285) | Atomic, eventually-consistent batches |  Major | CQL | Jonathan Ellis | Aleksey Yeschenko |
| [CASSANDRA-4239](https://issues.apache.org/jira/browse/CASSANDRA-4239) | Support Thrift SSL socket |  Minor | CQL | Jonathan Ellis | Jason Brown |
| [CASSANDRA-4049](https://issues.apache.org/jira/browse/CASSANDRA-4049) | Add generic way of adding SSTable components required custom compaction strategy |  Minor | . | Piotr Kołaczkowski | Piotr Kołaczkowski |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4449](https://issues.apache.org/jira/browse/CASSANDRA-4449) | Make prepared statement global rather than connection based |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4729](https://issues.apache.org/jira/browse/CASSANDRA-4729) | remove vestiges of Thrift unframed mode |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4712](https://issues.apache.org/jira/browse/CASSANDRA-4712) | bin/sstableloader should support authentication |  Major | Tools | Alexis |  |
| [CASSANDRA-4747](https://issues.apache.org/jira/browse/CASSANDRA-4747) | Add possibility to start repair only inside local data center |  Minor | . | Alexey Zotov | Alexey Zotov |
| [CASSANDRA-4738](https://issues.apache.org/jira/browse/CASSANDRA-4738) | Improve CQL3 batchlog support |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4416](https://issues.apache.org/jira/browse/CASSANDRA-4416) | Include metadata for system keyspace itself in schema\_\* tables |  Minor | . | paul cannon | Jonathan Ellis |
| [CASSANDRA-4710](https://issues.apache.org/jira/browse/CASSANDRA-4710) | High key hashing overhead for index scans when using RandomPartitioner |  Minor | . | Daniel Norberg | Daniel Norberg |
| [CASSANDRA-4732](https://issues.apache.org/jira/browse/CASSANDRA-4732) | clean up unnecessary copies on KeyCache path |  Trivial | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4351](https://issues.apache.org/jira/browse/CASSANDRA-4351) | Consider storing more informations on peers in system tables |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4684](https://issues.apache.org/jira/browse/CASSANDRA-4684) | Binary protocol: inform clients of schema changes |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4723](https://issues.apache.org/jira/browse/CASSANDRA-4723) | Improve write timeout exceptions |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4488](https://issues.apache.org/jira/browse/CASSANDRA-4488) | update cqlsh recognized syntax (for tab completion, etc) |  Minor | Tools | paul cannon | Aleksey Yeschenko |
| [CASSANDRA-4704](https://issues.apache.org/jira/browse/CASSANDRA-4704) | SizeTieredcompaction configurable bucket size |  Minor | . | Radim Kolar | Radim Kolar |
| [CASSANDRA-4661](https://issues.apache.org/jira/browse/CASSANDRA-4661) | cassandra-cli: allow Double value type to be inserted to a column |  Trivial | . | Yuhan Zhang | Dave Brosius |
| [CASSANDRA-2485](https://issues.apache.org/jira/browse/CASSANDRA-2485) | improve authentication log |  Major | . | Shotaro Kamio | Dave Brosius |
| [CASSANDRA-4761](https://issues.apache.org/jira/browse/CASSANDRA-4761) | Async hints delivery |  Major | . | Alexey Zotov | Alexey Zotov |
| [CASSANDRA-4691](https://issues.apache.org/jira/browse/CASSANDRA-4691) | Upgrade pig to 0.10 |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3597](https://issues.apache.org/jira/browse/CASSANDRA-3597) | cqlsh: use libedit when readline isn't available, if possible |  Minor | Tools | paul cannon | Aleksey Yeschenko |
| [CASSANDRA-4824](https://issues.apache.org/jira/browse/CASSANDRA-4824) | Pack consistency level in binary protocol |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4814](https://issues.apache.org/jira/browse/CASSANDRA-4814) | Always gossip DC and Rack information |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4821](https://issues.apache.org/jira/browse/CASSANDRA-4821) | Update PureJavaCrc32 |  Minor | . | Vijay | Vijay |
| [CASSANDRA-4812](https://issues.apache.org/jira/browse/CASSANDRA-4812) | Require enabling cross-node timeouts |  Minor | . | Jonathan Ellis | Vijay |
| [CASSANDRA-4852](https://issues.apache.org/jira/browse/CASSANDRA-4852) | make trace output pretty |  Minor | Tools | Jonathan Ellis | Aleksey Yeschenko |
| [CASSANDRA-4862](https://issues.apache.org/jira/browse/CASSANDRA-4862) | Include session information in tracing activity output |  Major | . | Jonathan Ellis | Aleksey Yeschenko |
| [CASSANDRA-4859](https://issues.apache.org/jira/browse/CASSANDRA-4859) | Include number of entries in CachedService |  Minor | . | Chris Burroughs | Chris Burroughs |
| [CASSANDRA-4865](https://issues.apache.org/jira/browse/CASSANDRA-4865) | Off-heap bloom filters |  Major | . | Jonathan Ellis | Vijay |
| [CASSANDRA-4876](https://issues.apache.org/jira/browse/CASSANDRA-4876) | Make bloom filters optional by default for LCS |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4904](https://issues.apache.org/jira/browse/CASSANDRA-4904) | log index scan subject in CompositesSearcher |  Minor | . | Jonathan Ellis | Sylvain Lebresne |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4695](https://issues.apache.org/jira/browse/CASSANDRA-4695) | CompactionsTest fails with timeout |  Minor | Testing | Alexey Zotov | Alexey Zotov |
| [CASSANDRA-833](https://issues.apache.org/jira/browse/CASSANDRA-833) | fix consistencylevel during bootstrap |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4703](https://issues.apache.org/jira/browse/CASSANDRA-4703) | o.a.c.service.StorageProxy - compilation issue |  Major | . | Karthik K | Karthik K |
| [CASSANDRA-4649](https://issues.apache.org/jira/browse/CASSANDRA-4649) | Cassandra 1.2 should not accept CQL version "3.0.0-beta1" |  Minor | CQL | paul cannon | Sylvain Lebresne |
| [CASSANDRA-4720](https://issues.apache.org/jira/browse/CASSANDRA-4720) | cqlsh fails to format timeuuid values |  Major | Tools | paul cannon | paul cannon |
| [CASSANDRA-4706](https://issues.apache.org/jira/browse/CASSANDRA-4706) | CQL3 CREATE TABLE with set and counter causes java.lang.IllegalArgumentException |  Minor | . | Jonathan Rudenberg | Sylvain Lebresne |
| [CASSANDRA-4685](https://issues.apache.org/jira/browse/CASSANDRA-4685) | Fix scrubbing of CQL3 created tables |  Major | . | Brandon Williams | Sylvain Lebresne |
| [CASSANDRA-4648](https://issues.apache.org/jira/browse/CASSANDRA-4648) | Unable to start Cassandra with simple authentication enabled |  Major | . | John Sanda | Sylvain Lebresne |
| [CASSANDRA-4727](https://issues.apache.org/jira/browse/CASSANDRA-4727) | Avoid ConcurrentModificationExceptions on relocateTokens |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-4728](https://issues.apache.org/jira/browse/CASSANDRA-4728) | NPE with some load of writes, but possible snitch setting issue for a cluster |  Minor | . | Alex Liu | Jonathan Ellis |
| [CASSANDRA-4737](https://issues.apache.org/jira/browse/CASSANDRA-4737) | doc/native\_protocol.txt isn't up to date |  Trivial | . | Christoph Hack | Sylvain Lebresne |
| [CASSANDRA-4751](https://issues.apache.org/jira/browse/CASSANDRA-4751) | User rpc\_address for binary protocol and change default port |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4739](https://issues.apache.org/jira/browse/CASSANDRA-4739) | Prepared Statements don't support collections |  Major | . | Jonathan Rudenberg | Sylvain Lebresne |
| [CASSANDRA-4792](https://issues.apache.org/jira/browse/CASSANDRA-4792) | Digest mismatch doesn't wait for writes as intended |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4674](https://issues.apache.org/jira/browse/CASSANDRA-4674) | cqlsh COPY TO and COPY FROM don't work with cql3 |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-4786](https://issues.apache.org/jira/browse/CASSANDRA-4786) | NPE in migration stage after creating an index |  Major | . | Brandon Williams | Sylvain Lebresne |
| [CASSANDRA-4796](https://issues.apache.org/jira/browse/CASSANDRA-4796) | composite indexes don't always return results they should |  Major | . | Brandon Williams | Sylvain Lebresne |
| [CASSANDRA-4810](https://issues.apache.org/jira/browse/CASSANDRA-4810) | unit test failing under long-test |  Minor | Testing | Bill Bucher | Yuki Morishita |
| [CASSANDRA-4807](https://issues.apache.org/jira/browse/CASSANDRA-4807) | Compaction progress counts more than 100% |  Minor | . | Omid Aladini | Yuki Morishita |
| [CASSANDRA-4804](https://issues.apache.org/jira/browse/CASSANDRA-4804) | Wrong assumption for KeyRange about range.end\_token in get\_range\_slices(). |  Minor | CQL | Nikolay | Nikolay |
| [CASSANDRA-4765](https://issues.apache.org/jira/browse/CASSANDRA-4765) | StackOverflowError in CompactionExecutor thread |  Major | . | Ahmed Bashir | Jonathan Ellis |
| [CASSANDRA-4823](https://issues.apache.org/jira/browse/CASSANDRA-4823) | Fix cqlsh after move of CL to the protocol level |  Major | Tools | Sylvain Lebresne | Aleksey Yeschenko |
| [CASSANDRA-4826](https://issues.apache.org/jira/browse/CASSANDRA-4826) | Subcolumn slice ends not respected |  Major | . | Tyler Hobbs | Vijay |
| [CASSANDRA-4755](https://issues.apache.org/jira/browse/CASSANDRA-4755) | Bulk loader won't work with CQL3 |  Major | Tools | Nick Bailey | Aleksey Yeschenko |
| [CASSANDRA-4833](https://issues.apache.org/jira/browse/CASSANDRA-4833) | get\_count with 'count' param between 1024 and ~actual column count fails |  Major | . | Tyler Hobbs | Yuki Morishita |
| [CASSANDRA-4793](https://issues.apache.org/jira/browse/CASSANDRA-4793) | Commitlog files replayed but not in the order of their ids |  Minor | . | Fabien Rousseau | Fabien Rousseau |
| [CASSANDRA-4672](https://issues.apache.org/jira/browse/CASSANDRA-4672) | \_TRACE verb is not droppable which causes an AssertionError |  Minor | . | David Alves | David Alves |
| [CASSANDRA-4788](https://issues.apache.org/jira/browse/CASSANDRA-4788) | streaming can put files in the wrong location |  Major | . | Brandon Williams | Yuki Morishita |
| [CASSANDRA-4783](https://issues.apache.org/jira/browse/CASSANDRA-4783) | AE in cql3 select |  Major | . | Brandon Williams | Sylvain Lebresne |
| [CASSANDRA-4764](https://issues.apache.org/jira/browse/CASSANDRA-4764) | Clean out STREAM\_STAGE vestiges |  Minor | . | Nick Bailey | Jonathan Ellis |
| [CASSANDRA-4808](https://issues.apache.org/jira/browse/CASSANDRA-4808) | nodetool doesnt work well with negative tokens |  Minor | . | Vijay | Vijay |
| [CASSANDRA-4846](https://issues.apache.org/jira/browse/CASSANDRA-4846) | BulkLoader throws NPE at start up |  Major | . | Yuki Morishita | Jonathan Ellis |
| [CASSANDRA-4816](https://issues.apache.org/jira/browse/CASSANDRA-4816) | Broken get\_paged\_slice |  Major | . | Piotr Kołaczkowski | Piotr Kołaczkowski |
| [CASSANDRA-4835](https://issues.apache.org/jira/browse/CASSANDRA-4835) | Appending/Prepending items to list using BATCH |  Minor | . | Krzysztof Cieslinski Cognitum | Sylvain Lebresne |
| [CASSANDRA-4811](https://issues.apache.org/jira/browse/CASSANDRA-4811) | Some cqlsh help topics don't work (select, create, insert and anything else that is a cql statement) |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-4776](https://issues.apache.org/jira/browse/CASSANDRA-4776) | Don't require quotes for true and false |  Minor | . | John Sanda | Sylvain Lebresne |
| [CASSANDRA-4822](https://issues.apache.org/jira/browse/CASSANDRA-4822) | CQL3: Allow renaming PK columns to ease upgrade from thrift |  Minor | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4864](https://issues.apache.org/jira/browse/CASSANDRA-4864) | CQL2 CREATE COLUMNFAMILY checks wrong permission |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-4863](https://issues.apache.org/jira/browse/CASSANDRA-4863) | cqlsh can't describe system tables |  Major | Tools | Jonathan Ellis | Aleksey Yeschenko |
| [CASSANDRA-4878](https://issues.apache.org/jira/browse/CASSANDRA-4878) | No entry in TypesMap for InetAddressType |  Trivial | CQL | Rick Shaw | Rick Shaw |
| [CASSANDRA-4781](https://issues.apache.org/jira/browse/CASSANDRA-4781) | Sometimes Cassandra starts compacting system-shema\_columns cf repeatedly until the node is killed |  Major | . | Aleksey Yeschenko | Yuki Morishita |
| [CASSANDRA-4746](https://issues.apache.org/jira/browse/CASSANDRA-4746) | cqlsh timestamp formatting is broken - displays wrong timezone info (at least on Ubuntu) |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-4850](https://issues.apache.org/jira/browse/CASSANDRA-4850) | RuntimeException when bootstrapping a node without an explicitely set token |  Major | . | Sylvain Lebresne | Pavel Yaskevich |
| [CASSANDRA-4881](https://issues.apache.org/jira/browse/CASSANDRA-4881) | Force provided columns in clustering key order in 'CLUSTERING ORDER BY' |  Major | . | Patrick McFadin | Sylvain Lebresne |
| [CASSANDRA-4884](https://issues.apache.org/jira/browse/CASSANDRA-4884) | Composites index bug |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3306](https://issues.apache.org/jira/browse/CASSANDRA-3306) | Failed streaming may cause duplicate SSTable reference |  Major | . | Radim Kolar | Yuki Morishita |
| [CASSANDRA-4882](https://issues.apache.org/jira/browse/CASSANDRA-4882) | Short read protection don't count columns correctly for CQL3 |  Major | . | Patrick McFadin | Sylvain Lebresne |
| [CASSANDRA-4890](https://issues.apache.org/jira/browse/CASSANDRA-4890) | Don't allow prepared marker inside collections |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4892](https://issues.apache.org/jira/browse/CASSANDRA-4892) | no need to keep tombstones in HintsColumnFamily |  Minor | . | Matthew F. Dennis | Jonathan Ellis |
| [CASSANDRA-4800](https://issues.apache.org/jira/browse/CASSANDRA-4800) | cqlsh help is obsolete for cql3 |  Minor | Tools | Jonathan Ellis | Aleksey Yeschenko |
| [CASSANDRA-4645](https://issues.apache.org/jira/browse/CASSANDRA-4645) | (CQL3) Re-allow order by on non-selected columns |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4909](https://issues.apache.org/jira/browse/CASSANDRA-4909) | Bug when composite index is created in a table having collections |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4679](https://issues.apache.org/jira/browse/CASSANDRA-4679) | Fix binary protocol NEW\_NODE event |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4545](https://issues.apache.org/jira/browse/CASSANDRA-4545) | add cql support for batchlog |  Major | . | Jonathan Ellis | Aleksey Yeschenko |
| [CASSANDRA-4609](https://issues.apache.org/jira/browse/CASSANDRA-4609) | Add thrift transport factory impl to cassandra-cli |  Minor | . | T Jake Luciani | Jason Brown |
| [CASSANDRA-4608](https://issues.apache.org/jira/browse/CASSANDRA-4608) | Add thrift server factory to CassandraDaemon |  Major | . | T Jake Luciani | Jason Brown |
| [CASSANDRA-4667](https://issues.apache.org/jira/browse/CASSANDRA-4667) | optimize memtable deletions for batchlog |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-4584](https://issues.apache.org/jira/browse/CASSANDRA-4584) | Add cqlsh syntax to enable request tracing |  Major | . | Jonathan Ellis | Aleksey Yeschenko |
| [CASSANDRA-4610](https://issues.apache.org/jira/browse/CASSANDRA-4610) | Add thrift transport factory impl to cqlsh |  Minor | . | T Jake Luciani | Aleksey Yeschenko |
| [CASSANDRA-4443](https://issues.apache.org/jira/browse/CASSANDRA-4443) | shuffle utility for vnodes |  Major | . | Brandon Williams | Eric Evans |
| [CASSANDRA-4699](https://issues.apache.org/jira/browse/CASSANDRA-4699) | Add TRACE support to binary protocol |  Major | CQL | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-4636](https://issues.apache.org/jira/browse/CASSANDRA-4636) | Create atomic batch dtests |  Major | . | Jonathan Ellis | Aleksey Yeschenko |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4734](https://issues.apache.org/jira/browse/CASSANDRA-4734) | Move CQL3 consistency to protocol |  Major | CQL | Jonathan Ellis | Sylvain Lebresne |


