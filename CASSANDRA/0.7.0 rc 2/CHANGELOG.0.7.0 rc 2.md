
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.7.0 rc 2 - 2010-12-09



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1780](https://issues.apache.org/jira/browse/CASSANDRA-1780) | periodic + flush commitlog mode |  Minor | . | Jonathan Ellis | Jonathan Ellis |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1767](https://issues.apache.org/jira/browse/CASSANDRA-1767) | cli to/from string needs to move into AbstractType |  Major | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1773](https://issues.apache.org/jira/browse/CASSANDRA-1773) | Prevent creation of column\_metadata inconsistent with comparator |  Minor | . | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1789](https://issues.apache.org/jira/browse/CASSANDRA-1789) | Clean up (and make sane) key/row cache loading logspam |  Minor | . | Jon Hermes | Matthew F. Dennis |
| [CASSANDRA-1793](https://issues.apache.org/jira/browse/CASSANDRA-1793) | [patch] cleanup minor field usage that can be moved to static or locals |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-1794](https://issues.apache.org/jira/browse/CASSANDRA-1794) | [patch] avoid map look ups in a loop by using entrySet |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-1795](https://issues.apache.org/jira/browse/CASSANDRA-1795) | [patch] avoid unnecessary use of synchronized collection for non escaped local varables |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-1803](https://issues.apache.org/jira/browse/CASSANDRA-1803) | fail fast in strongRead when insufficient replicas are available to satisfy ConsistencyLevel |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1807](https://issues.apache.org/jira/browse/CASSANDRA-1807) | make it easier to turn on the JVM GC debug logging |  Minor | . | Matthew F. Dennis | Matthew F. Dennis |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1662](https://issues.apache.org/jira/browse/CASSANDRA-1662) | RPM spec file should create saved\_caches directory |  Minor | Packaging | Dave Revell | Dave Revell |
| [CASSANDRA-1665](https://issues.apache.org/jira/browse/CASSANDRA-1665) | JMX threads leak in NodeProbe |  Minor | Tools | Bill Au | Bill Au |
| [CASSANDRA-1759](https://issues.apache.org/jira/browse/CASSANDRA-1759) | Column MetaData: Index\_name should not be allowed if index\_type is not set. |  Trivial | . | Jon Hermes | Jon Hermes |
| [CASSANDRA-1768](https://issues.apache.org/jira/browse/CASSANDRA-1768) | UnsupportedOperationException in system\_update\_column\_family |  Major | . | T Jake Luciani | Gary Dusbabek |
| [CASSANDRA-1764](https://issues.apache.org/jira/browse/CASSANDRA-1764) | NPE on system\_update\_cf when adding an index to a column without existing metadata |  Major | . | Tyler Hobbs | Gary Dusbabek |
| [CASSANDRA-1762](https://issues.apache.org/jira/browse/CASSANDRA-1762) | UpdateKeyspace does not modify strategy or strategy\_options (until restart) |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1771](https://issues.apache.org/jira/browse/CASSANDRA-1771) | Script in debian/cassandra.in.sh is significantly out of date |  Minor | . | Nate McCall | Eric Evans |
| [CASSANDRA-1770](https://issues.apache.org/jira/browse/CASSANDRA-1770) | cassandra-cli help output should mention the need for semicolons |  Minor | Tools | paul cannon | Pavel Yaskevich |
| [CASSANDRA-1772](https://issues.apache.org/jira/browse/CASSANDRA-1772) | debian initscript sometimes mistakenly thinks it failed, gives extraneous output |  Minor | Packaging | paul cannon | paul cannon |
| [CASSANDRA-1774](https://issues.apache.org/jira/browse/CASSANDRA-1774) | ColumnFamilyOutputFormat only writes one column (per key) |  Major | . | mck | mck |
| [CASSANDRA-1782](https://issues.apache.org/jira/browse/CASSANDRA-1782) | Example session in README is missing semicolons |  Minor | Documentation and Website | Justin Azoff | Justin Azoff |
| [CASSANDRA-1785](https://issues.apache.org/jira/browse/CASSANDRA-1785) | [patch] use long math, if long values are expected. |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-1786](https://issues.apache.org/jira/browse/CASSANDRA-1786) | [patch] use cross platform new lines in printf calls |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-1754](https://issues.apache.org/jira/browse/CASSANDRA-1754) | SSTable Import/Export doesn't support ExpiringColumns |  Minor | Tools | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-1790](https://issues.apache.org/jira/browse/CASSANDRA-1790) | Booting fails on Windows 7/Windows 2003 because of a file rename failure |  Blocker | . | Ramon Rockx | Jonathan Ellis |
| [CASSANDRA-1781](https://issues.apache.org/jira/browse/CASSANDRA-1781) | Internal error processing get\_range\_slices |  Major | . | Patrik Modesto | Stu Hood |
| [CASSANDRA-1804](https://issues.apache.org/jira/browse/CASSANDRA-1804) | Quorum calculation code needs to use polymorphic Strategy method for determining replica counts |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1809](https://issues.apache.org/jira/browse/CASSANDRA-1809) | cli executeGetWithConditions is not case-insensitive for CF names |  Minor | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1810](https://issues.apache.org/jira/browse/CASSANDRA-1810) | cli support index\_type enum names in column\_metadata |  Minor | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1813](https://issues.apache.org/jira/browse/CASSANDRA-1813) | return invalidrequest when client attempts to create secondary index on supercolumns |  Minor | Secondary Indexes | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1801](https://issues.apache.org/jira/browse/CASSANDRA-1801) | bytebuffers of column data written locally prevent GC of original thrift mutation byte[] |  Major | . | Matthew F. Dennis | T Jake Luciani |
| [CASSANDRA-1816](https://issues.apache.org/jira/browse/CASSANDRA-1816) | cassandra deb should depend on adduser |  Minor | Packaging | paul cannon | paul cannon |
| [CASSANDRA-1826](https://issues.apache.org/jira/browse/CASSANDRA-1826) | system\_create\_cf() makes a SuperCF when column\_type is Standard and subcolumn\_comparator\_type is present |  Major | . | Tyler Hobbs |  |
| [CASSANDRA-1852](https://issues.apache.org/jira/browse/CASSANDRA-1852) | When restarting Cassandra I get this error: java.io.IOError: java.io.IOException: Failed to delete  ..data\\system\\LocationInfo-e-1-Data.db |  Major | . | aadel |  |


