
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.1.10 - 2013-02-15



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5155](https://issues.apache.org/jira/browse/CASSANDRA-5155) | Make Ec2Region's datacenter name configurable |  Minor | . | Jason Brown | Jason Brown |
| [CASSANDRA-5170](https://issues.apache.org/jira/browse/CASSANDRA-5170) | ConcurrentModificationException in getBootstrapSource |  Major | . | Jonathan Ellis | Kirk True |
| [CASSANDRA-5262](https://issues.apache.org/jira/browse/CASSANDRA-5262) | Don't generate compaction statistics if logging isn't enabled |  Trivial | . | Dave Brosius | Dave Brosius |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5166](https://issues.apache.org/jira/browse/CASSANDRA-5166) | Saved key cache is not loaded when opening ColumnFamily |  Minor | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-5175](https://issues.apache.org/jira/browse/CASSANDRA-5175) | Unbounded (?) thread growth connecting to an removed node |  Minor | . | Janne Jalkanen | Vijay |
| [CASSANDRA-5153](https://issues.apache.org/jira/browse/CASSANDRA-5153) | max client timestamp |  Major | . | yangwei | Jonathan Ellis |
| [CASSANDRA-5168](https://issues.apache.org/jira/browse/CASSANDRA-5168) | word count example fails with InvalidRequestException(why:Start key's token sorts after end token) |  Minor | . | Jeremy Hanna | Brandon Williams |
| [CASSANDRA-5188](https://issues.apache.org/jira/browse/CASSANDRA-5188) | o.a.c.hadoop.ConfigHelper should support setting Thrift frame and max message sizes. |  Minor | . | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-5203](https://issues.apache.org/jira/browse/CASSANDRA-5203) | Repair command should report error when replica node is dead |  Trivial | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-5235](https://issues.apache.org/jira/browse/CASSANDRA-5235) | Error when executing a file contains CQL statement in cqlsh |  Minor | Tools | Shamim Ahmed | Aleksey Yeschenko |
| [CASSANDRA-5068](https://issues.apache.org/jira/browse/CASSANDRA-5068) | CLONE - Once a host has been hinted to, log messages for it repeat every 10 mins even if no hints are delivered |  Minor | . | Peter Haggerty | Brandon Williams |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5157](https://issues.apache.org/jira/browse/CASSANDRA-5157) | mac friendly cassandra-env.sh |  Trivial | Configuration | Cathy Daw | Cathy Daw |


