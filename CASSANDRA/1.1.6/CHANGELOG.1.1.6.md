
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.1.6 - 2012-10-15



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4611](https://issues.apache.org/jira/browse/CASSANDRA-4611) | Add AlterKeyspace statement |  Minor | CQL | Sylvain Lebresne | Sylvain Lebresne |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4490](https://issues.apache.org/jira/browse/CASSANDRA-4490) | Improve IAuthority interface by introducing fine-grained access permissions and grant/revoke commands. |  Minor | . | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-4642](https://issues.apache.org/jira/browse/CASSANDRA-4642) | Add offline scrub to debian package |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4454](https://issues.apache.org/jira/browse/CASSANDRA-4454) | Add a notice on cqlsh startup about CQL2/3 switches |  Minor | Tools | Michaël Figuière | paul cannon |
| [CASSANDRA-4073](https://issues.apache.org/jira/browse/CASSANDRA-4073) | cqlsh: flush CAPTURE output after each command |  Minor | . | Tyler Patterson | paul cannon |
| [CASSANDRA-4666](https://issues.apache.org/jira/browse/CASSANDRA-4666) | Add multi-line support to cqlsh history buffer |  Minor | Tools | Matthew Horsfall (alh) |  |
| [CASSANDRA-4656](https://issues.apache.org/jira/browse/CASSANDRA-4656) | StorageProxy histograms |  Minor | . | Alexey Zotov | Alexey Zotov |
| [CASSANDRA-4697](https://issues.apache.org/jira/browse/CASSANDRA-4697) | incorrect debug message in LeveledManifest.java |  Trivial | . | Radim Kolar | Yuki Morishita |
| [CASSANDRA-4034](https://issues.apache.org/jira/browse/CASSANDRA-4034) | cqlsh: Alphabetize the "Miscellaneous Help Topics" section |  Minor | . | Tyler Patterson | Aleksey Yeschenko |
| [CASSANDRA-4423](https://issues.apache.org/jira/browse/CASSANDRA-4423) | auto completion in cqlsh should work when using fully qualified name |  Minor | Tools | Jackson Chung | Aleksey Yeschenko |
| [CASSANDRA-3460](https://issues.apache.org/jira/browse/CASSANDRA-3460) | Increase precision of elapsed time in casandra-cli |  Minor | Tools | Radim Kolar | Radim Kolar |
| [CASSANDRA-4712](https://issues.apache.org/jira/browse/CASSANDRA-4712) | bin/sstableloader should support authentication |  Major | Tools | Alexis |  |
| [CASSANDRA-4661](https://issues.apache.org/jira/browse/CASSANDRA-4661) | cassandra-cli: allow Double value type to be inserted to a column |  Trivial | . | Yuhan Zhang | Dave Brosius |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4634](https://issues.apache.org/jira/browse/CASSANDRA-4634) | cqlsh --color option doesn't allow you to disable colors |  Trivial | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-4322](https://issues.apache.org/jira/browse/CASSANDRA-4322) | Error in CLI when updating keyspace |  Minor | . | Richard Low | Pavel Yaskevich |
| [CASSANDRA-4669](https://issues.apache.org/jira/browse/CASSANDRA-4669) | Empty .cqlsh\_history file causes cqlsh to crash on startup. |  Minor | Tools | Brian ONeill | Brian ONeill |
| [CASSANDRA-3841](https://issues.apache.org/jira/browse/CASSANDRA-3841) | long-test timing out |  Minor | Testing | Michael Allen | Jason Brown |
| [CASSANDRA-4594](https://issues.apache.org/jira/browse/CASSANDRA-4594) | COPY TO and COPY FROM don't default to consistent ordering of columns |  Minor | . | Tyler Patterson | paul cannon |
| [CASSANDRA-4689](https://issues.apache.org/jira/browse/CASSANDRA-4689) | Log error when using IN together with ORDER BY |  Major | . | Tyler Patterson | Pavel Yaskevich |
| [CASSANDRA-4688](https://issues.apache.org/jira/browse/CASSANDRA-4688) | comments and error messages use wrong method names |  Minor | . | Philip Crotwell | Philip Crotwell |
| [CASSANDRA-4499](https://issues.apache.org/jira/browse/CASSANDRA-4499) | CassandraStorage allow\_deletes doesn't work in Hadoop cluster |  Major | . | Jeremiah Johnson | Brandon Williams |
| [CASSANDRA-4708](https://issues.apache.org/jira/browse/CASSANDRA-4708) | StorageProxy slow-down and memory leak |  Major | . | Daniel Norberg | Daniel Norberg |
| [CASSANDRA-4700](https://issues.apache.org/jira/browse/CASSANDRA-4700) | cql 2 counter validations need to use default consistencylevel if none is explicitly given |  Minor | . | Nitesh Kumar | Nitesh Kumar |
| [CASSANDRA-4721](https://issues.apache.org/jira/browse/CASSANDRA-4721) | Have Cassandra return the right error for keyspaces with dots |  Trivial | . | Joaquin Casares | Jonathan Ellis |
| [CASSANDRA-4334](https://issues.apache.org/jira/browse/CASSANDRA-4334) | cqlsh tab completion error in "CREATE KEYSPACE" |  Minor | . | Tyler Patterson | Aleksey Yeschenko |
| [CASSANDRA-4724](https://issues.apache.org/jira/browse/CASSANDRA-4724) | Some operations of HintedHandOffManager bean have wrong output |  Minor | Tools | Alexey Zotov | Alexey Zotov |
| [CASSANDRA-4698](https://issues.apache.org/jira/browse/CASSANDRA-4698) | Keyspace disappears when upgrading node from cassandra-1.1.1 to cassandra-1.1.5 |  Major | . | Tyler Patterson | Pavel Yaskevich |
| [CASSANDRA-4675](https://issues.apache.org/jira/browse/CASSANDRA-4675) | NPE in NTS when using LQ against a node (DC) that doesn't have replica |  Minor | . | Jackson Chung | Jonathan Ellis |
| [CASSANDRA-4716](https://issues.apache.org/jira/browse/CASSANDRA-4716) | CQL3 predicate logic is reversed when used on a reversed column |  Major | . | T Jake Luciani | Sylvain Lebresne |
| [CASSANDRA-4717](https://issues.apache.org/jira/browse/CASSANDRA-4717) | cqlsh fails to format values of ReversedType-wrapped classes |  Minor | Tools | paul cannon | Sylvain Lebresne |
| [CASSANDRA-4754](https://issues.apache.org/jira/browse/CASSANDRA-4754) | There is an inconsistency between default configuration in cassandra.yaml and java code |  Trivial | . | Alexey Zotov | Alexey Zotov |
| [CASSANDRA-4759](https://issues.apache.org/jira/browse/CASSANDRA-4759) | CQL3 Predicate logic bug when using composite columns |  Major | . | T Jake Luciani | Sylvain Lebresne |
| [CASSANDRA-4752](https://issues.apache.org/jira/browse/CASSANDRA-4752) | Drop keyspace causes schema disagreement |  Major | . | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-4770](https://issues.apache.org/jira/browse/CASSANDRA-4770) | (CQL3) data type not in lowercase are not handled correctly. |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4657](https://issues.apache.org/jira/browse/CASSANDRA-4657) | cql version race condition with rpc\_server\_type: sync |  Minor | . | Emmanuel Courreges | Jonathan Ellis |
| [CASSANDRA-4778](https://issues.apache.org/jira/browse/CASSANDRA-4778) | leveled compaction does less work in L0 than intended |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4772](https://issues.apache.org/jira/browse/CASSANDRA-4772) | HintedHandoff fails to deliver hints after first repaired node |  Major | . | Alexey Zotov | Alexey Zotov |
| [CASSANDRA-4709](https://issues.apache.org/jira/browse/CASSANDRA-4709) | (CQL3) Missing validation for IN queries on column not part of the PK |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4782](https://issues.apache.org/jira/browse/CASSANDRA-4782) | Commitlog not replayed after restart |  Critical | . | Fabien Rousseau | Jonathan Ellis |
| [CASSANDRA-4789](https://issues.apache.org/jira/browse/CASSANDRA-4789) | CassandraStorage.getNextWide produces corrupt data |  Major | . | Will Oberman | Will Oberman |
| [CASSANDRA-4792](https://issues.apache.org/jira/browse/CASSANDRA-4792) | Digest mismatch doesn't wait for writes as intended |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4749](https://issues.apache.org/jira/browse/CASSANDRA-4749) | Possible problem with widerow in Pig URI |  Major | . | Will Oberman | Brandon Williams |
| [CASSANDRA-4771](https://issues.apache.org/jira/browse/CASSANDRA-4771) | Setting TTL to Integer.MAX causes columns to not be persisted. |  Major | . | Todd Nine | Dave Brosius |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4668](https://issues.apache.org/jira/browse/CASSANDRA-4668) | Add ITransportFactory to cassandra-thrift jar |  Major | Packaging | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-4677](https://issues.apache.org/jira/browse/CASSANDRA-4677) | Change description of nodetool ring command |  Trivial | Tools | Alexey Zotov | Alexey Zotov |


