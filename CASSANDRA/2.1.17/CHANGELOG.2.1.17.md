
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1.17 - 2017-02-21



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12967](https://issues.apache.org/jira/browse/CASSANDRA-12967) | Spec for Cassandra RPM Build |  Major | Packaging | Bhuvan Rawal | Michael Shuler |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12765](https://issues.apache.org/jira/browse/CASSANDRA-12765) | SSTable ignored incorrectly with partition level tombstone |  Major | Local Write-Read Paths | Cameron Zemek | Cameron Zemek |
| [CASSANDRA-12802](https://issues.apache.org/jira/browse/CASSANDRA-12802) | RecoveryManagerTruncateTest fails in 2.1 |  Major | Local Write-Read Paths, Testing | Stefania | Stefania |
| [CASSANDRA-13037](https://issues.apache.org/jira/browse/CASSANDRA-13037) | DropKeyspaceCommitLogRecycleTest.testRecycle times out in 2.1 and 2.2 |  Major | Testing | Stefania | Stefania |
| [CASSANDRA-12959](https://issues.apache.org/jira/browse/CASSANDRA-12959) | copy from csv import wrong values with udt having set when fields are not specified in correct order in csv |  Major | Tools | Quentin Ambard | Stefania |
| [CASSANDRA-12856](https://issues.apache.org/jira/browse/CASSANDRA-12856) | dtest failure in replication\_test.SnitchConfigurationUpdateTest.test\_cannot\_restart\_with\_different\_rack |  Major | . | Sean McCarthy | Stefania |
| [CASSANDRA-13017](https://issues.apache.org/jira/browse/CASSANDRA-13017) | DISTINCT queries on partition keys and static column might not return all the results |  Major | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-13114](https://issues.apache.org/jira/browse/CASSANDRA-13114) | Upgrade netty to 4.0.44 to fix memory leak with client encryption |  Blocker | . | Tom van der Woerdt | Stefan Podkowinski |
| [CASSANDRA-13159](https://issues.apache.org/jira/browse/CASSANDRA-13159) | Coalescing strategy can enter infinite loop |  Major | Streaming and Messaging | Corentin Chary | Corentin Chary |
| [CASSANDRA-13204](https://issues.apache.org/jira/browse/CASSANDRA-13204) | Thread Leak in OutboundTcpConnection |  Major | . | sankalp kohli | Jason Brown |
| [CASSANDRA-13211](https://issues.apache.org/jira/browse/CASSANDRA-13211) | Use portable stderr for java error in startup |  Major | . | Max Bowsher | Michael Shuler |


