
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.2.7 - 2013-07-26



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5337](https://issues.apache.org/jira/browse/CASSANDRA-5337) | vnode-aware replacenode command |  Major | . | Jonathan Ellis | Brandon Williams |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5700](https://issues.apache.org/jira/browse/CASSANDRA-5700) | compact storage value restriction message confusing |  Trivial | . | Dave Brosius | Jonathan Ellis |
| [CASSANDRA-5719](https://issues.apache.org/jira/browse/CASSANDRA-5719) | Expire entries out of ThriftSessionManager |  Minor | . | Jason Brown | Jason Brown |
| [CASSANDRA-5728](https://issues.apache.org/jira/browse/CASSANDRA-5728) | Check if native server is running before starting it up |  Major | . | Manoj Mainali | Manoj Mainali |
| [CASSANDRA-5735](https://issues.apache.org/jira/browse/CASSANDRA-5735) | Add binary server status to the node information |  Minor | . | Manoj Mainali | Manoj Mainali |
| [CASSANDRA-5734](https://issues.apache.org/jira/browse/CASSANDRA-5734) | Print usage when command is not specified in nodetool instead of connection failure |  Minor | Tools | Manoj Mainali | Manoj Mainali |
| [CASSANDRA-5520](https://issues.apache.org/jira/browse/CASSANDRA-5520) | Query tracing session info inconsistent with events info |  Minor | Tools | Ilya Kirnos | Tyler Hobbs |
| [CASSANDRA-5677](https://issues.apache.org/jira/browse/CASSANDRA-5677) | Performance improvements of RangeTombstones/IntervalTree |  Minor | . | Fabien Rousseau | Sylvain Lebresne |
| [CASSANDRA-5773](https://issues.apache.org/jira/browse/CASSANDRA-5773) | print cluster name in describe cluster |  Trivial | . | Philip Crotwell | Philip Crotwell |
| [CASSANDRA-5555](https://issues.apache.org/jira/browse/CASSANDRA-5555) | Allow sstableloader to handle a larger number of files |  Major | Tools | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-5776](https://issues.apache.org/jira/browse/CASSANDRA-5776) | Print nodetool commands in sorted order |  Minor | Tools | Manoj Mainali | Manoj Mainali |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5696](https://issues.apache.org/jira/browse/CASSANDRA-5696) | Upgrading to cassandra-1.2 with a dead LEFT state from 1.1 causes problems |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-5675](https://issues.apache.org/jira/browse/CASSANDRA-5675) | cqlsh shouldn't display "null" for empty values |  Minor | . | Sylvain Lebresne | Aleksey Yeschenko |
| [CASSANDRA-5692](https://issues.apache.org/jira/browse/CASSANDRA-5692) | Race condition in detecting version on a mixed 1.1/1.2 cluster |  Minor | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-5706](https://issues.apache.org/jira/browse/CASSANDRA-5706) | OOM while loading key cache at startup |  Major | . | Fabien Rousseau | Fabien Rousseau |
| [CASSANDRA-5234](https://issues.apache.org/jira/browse/CASSANDRA-5234) | Table created through CQL3 are not accessble to Pig 0.10 |  Major | . | Shamim Ahmed | Alex Liu |
| [CASSANDRA-5247](https://issues.apache.org/jira/browse/CASSANDRA-5247) | cassandra-cli should exit with error-exit status on all errors which cause it to exit. |  Minor | Tools | Robert P. Thille | Steve Peters |
| [CASSANDRA-5712](https://issues.apache.org/jira/browse/CASSANDRA-5712) | Reverse slice queries can skip range tombstones |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5454](https://issues.apache.org/jira/browse/CASSANDRA-5454) | Changing column\_index\_size\_in\_kb on different nodes might corrupt files |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5342](https://issues.apache.org/jira/browse/CASSANDRA-5342) | ancestors are not cleared in SSTableMetadata after compactions are done and old SSTables are removed |  Major | . | Wei Zhu | Marcus Eriksson |
| [CASSANDRA-5697](https://issues.apache.org/jira/browse/CASSANDRA-5697) | cqlsh doesn't allow semicolons in BATCH statements |  Minor | Tools | Russell Spitzer | Aleksey Yeschenko |
| [CASSANDRA-5573](https://issues.apache.org/jira/browse/CASSANDRA-5573) | Querying with an empty (impossible) range returns incorrect results |  Major | . | Mike Schrag | Sylvain Lebresne |
| [CASSANDRA-5753](https://issues.apache.org/jira/browse/CASSANDRA-5753) | cassandra-shuffle is not available for windows |  Trivial | Tools | Radim Kolar | Jonathan Ellis |
| [CASSANDRA-5760](https://issues.apache.org/jira/browse/CASSANDRA-5760) | cqlsh DESCRIBE should properly describe CUSTOM secondary indexes |  Minor | Secondary Indexes | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5763](https://issues.apache.org/jira/browse/CASSANDRA-5763) | CqlPagingRecordReader should quote table and column identifiers |  Minor | . | Piotr Kołaczkowski | Piotr Kołaczkowski |
| [CASSANDRA-5704](https://issues.apache.org/jira/browse/CASSANDRA-5704) | add cassandra.unsafesystem property (Truncate flushes to disk again in 1.2, even with durable\_writes=false) |  Minor | . | Christian Spriegel | Jonathan Ellis |
| [CASSANDRA-5771](https://issues.apache.org/jira/browse/CASSANDRA-5771) | Small bug in Range.intersects(Bound) |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5766](https://issues.apache.org/jira/browse/CASSANDRA-5766) | The output of the describe command does not necessarily give you the right DDL to re-create the CF |  Major | Tools | Steven Lowenthal | Aleksey Yeschenko |
| [CASSANDRA-5768](https://issues.apache.org/jira/browse/CASSANDRA-5768) | If a Seed can't be contacted, a new node comes up as a cluster of 1 |  Minor | . | Andy Cobley | Brandon Williams |
| [CASSANDRA-5769](https://issues.apache.org/jira/browse/CASSANDRA-5769) | Not all STATUS\_CHANGE UP events reported via the native protocol |  Minor | CQL | Duncan Sands | Sylvain Lebresne |
| [CASSANDRA-5781](https://issues.apache.org/jira/browse/CASSANDRA-5781) | Providing multiple keys to sstable2json results in invalid json |  Minor | Tools | Chris Lohfink | Chris Lohfink |
| [CASSANDRA-5779](https://issues.apache.org/jira/browse/CASSANDRA-5779) | sudden NPE while accessing Cassandra with CQL driver |  Minor | . | Irina Chernushina | Sylvain Lebresne |
| [CASSANDRA-5762](https://issues.apache.org/jira/browse/CASSANDRA-5762) | Lost row marker after TTL expires |  Critical | . | Taner Catakli | Sylvain Lebresne |
| [CASSANDRA-5795](https://issues.apache.org/jira/browse/CASSANDRA-5795) | now() is being rejected in INSERTs when inside collections |  Major | . | Aleksey Yeschenko | Sylvain Lebresne |
| [CASSANDRA-5799](https://issues.apache.org/jira/browse/CASSANDRA-5799) | Column can expire while lazy compacting it... |  Major | . | Fabien Rousseau | Sylvain Lebresne |
| [CASSANDRA-5716](https://issues.apache.org/jira/browse/CASSANDRA-5716) | Remark on cassandra-5273 : Hanging system after OutOfMemory. Server cannot die due to uncaughtException handling |  Minor | . | Ignace Desimpel | Jonathan Ellis |


