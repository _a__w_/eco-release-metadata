
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1 rc4 - 2014-07-19



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7517](https://issues.apache.org/jira/browse/CASSANDRA-7517) | Support starting of daemon from within powershell |  Minor | Packaging | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-7528](https://issues.apache.org/jira/browse/CASSANDRA-7528) | certificate not validated for internode SSL encryption. |  Major | . | Joseph Clark | Brandon Williams |
| [CASSANDRA-7551](https://issues.apache.org/jira/browse/CASSANDRA-7551) | improve 2.1 flush defaults |  Major | . | Jonathan Ellis | Jonathan Ellis |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7454](https://issues.apache.org/jira/browse/CASSANDRA-7454) | NPE When Prepared Statement ID is not Found |  Major | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-7521](https://issues.apache.org/jira/browse/CASSANDRA-7521) | Anti-compaction proceeds if any part of the repair failed |  Blocker | . | Jason Brown | Jason Brown |
| [CASSANDRA-7470](https://issues.apache.org/jira/browse/CASSANDRA-7470) | java.lang.AssertionError are causing cql responses to always go to streamId = 0 instead of the request sending streamId |  Trivial | CQL | Dominic Letz | Tyler Hobbs |
| [CASSANDRA-7535](https://issues.apache.org/jira/browse/CASSANDRA-7535) | Coverage analysis for range queries |  Major | . | David Semeria | Tyler Hobbs |
| [CASSANDRA-7503](https://issues.apache.org/jira/browse/CASSANDRA-7503) | Windows - better communication on start-up failure |  Minor | . | Joshua McKenzie | Ala' Alkhaldi |
| [CASSANDRA-7541](https://issues.apache.org/jira/browse/CASSANDRA-7541) | Windows: IOException when repairing a range of tokens |  Minor | Streaming and Messaging | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-7539](https://issues.apache.org/jira/browse/CASSANDRA-7539) | DROP INDEX response is missing table name |  Major | CQL | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-7527](https://issues.apache.org/jira/browse/CASSANDRA-7527) | Bump CQL version and update doc for 2.1 |  Major | . | Sylvain Lebresne | Tyler Hobbs |
| [CASSANDRA-7508](https://issues.apache.org/jira/browse/CASSANDRA-7508) | nodetool prints error if dirs don't exist. |  Minor | Tools | Jeremiah Jordan | Tyler Hobbs |
| [CASSANDRA-7509](https://issues.apache.org/jira/browse/CASSANDRA-7509) | conditional updates don't work from cqlsh |  Blocker | Tools | Jeremiah Jordan | Tyler Hobbs |
| [CASSANDRA-7525](https://issues.apache.org/jira/browse/CASSANDRA-7525) | Querying by multiple secondary indexes gives java.lang.IllegalArgumentException on some cases |  Major | Secondary Indexes | Tuukka Mustonen | Sam Tunnicliffe |
| [CASSANDRA-7564](https://issues.apache.org/jira/browse/CASSANDRA-7564) | cqlsh does not connect if PasswordAuthenticator is enabled |  Major | Tools | Mike Adamson | Mike Adamson |
| [CASSANDRA-7200](https://issues.apache.org/jira/browse/CASSANDRA-7200) | word count broken |  Major | . | Brandon Williams | Ala' Alkhaldi |


