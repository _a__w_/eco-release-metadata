
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.0.8 - 2012-02-27



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3759](https://issues.apache.org/jira/browse/CASSANDRA-3759) | [patch] don't allow dropping the system keyspace |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-3790](https://issues.apache.org/jira/browse/CASSANDRA-3790) | [patch] allow compaction score to be a floating pt value |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-3703](https://issues.apache.org/jira/browse/CASSANDRA-3703) | log Compaction Active tasks in StatusLogger instead of n/a |  Minor | . | Jackson Chung | Jonathan Ellis |
| [CASSANDRA-3809](https://issues.apache.org/jira/browse/CASSANDRA-3809) | Show Index Options in CLI |  Minor | . | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-3842](https://issues.apache.org/jira/browse/CASSANDRA-3842) | Make CLI \`show schema\` output data into the file as well is display it to user. |  Minor | . | Pavel Yaskevich | Pavel Yaskevich |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3737](https://issues.apache.org/jira/browse/CASSANDRA-3737) | Its impossible to removetoken joining down node |  Major | . | Vitalii Tymchyshyn | Brandon Williams |
| [CASSANDRA-3736](https://issues.apache.org/jira/browse/CASSANDRA-3736) | -Dreplace\_token leaves old node (IP) in the gossip with the token. |  Major | . | Jackson Chung | Vijay |
| [CASSANDRA-3726](https://issues.apache.org/jira/browse/CASSANDRA-3726) | cqlsh and cassandra-cli show keys differently for data created via stress tool |  Minor | . | Cathy Daw | paul cannon |
| [CASSANDRA-3738](https://issues.apache.org/jira/browse/CASSANDRA-3738) | sstable2json doesn't work for secondary index sstable due to partitioner mismatch |  Major | Secondary Indexes | Shotaro Kamio | Yuki Morishita |
| [CASSANDRA-3764](https://issues.apache.org/jira/browse/CASSANDRA-3764) | cqlsh doesn't error out immediately for use of invalid keyspace |  Minor | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3789](https://issues.apache.org/jira/browse/CASSANDRA-3789) | [patch] fix bad validator lookup (bad key type) |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-3744](https://issues.apache.org/jira/browse/CASSANDRA-3744) | Nodetool.bat double quotes classpath |  Minor | Tools | Nick Bailey | Nick Bailey |
| [CASSANDRA-3374](https://issues.apache.org/jira/browse/CASSANDRA-3374) | CQL can't create column with compression or that use leveled compaction |  Minor | CQL | Sylvain Lebresne | Pavel Yaskevich |
| [CASSANDRA-3251](https://issues.apache.org/jira/browse/CASSANDRA-3251) | CassandraStorage uses comparator for both super column names and sub column names. |  Major | . | Dana H. P'Simer, Jr. | Pavel Yaskevich |
| [CASSANDRA-3751](https://issues.apache.org/jira/browse/CASSANDRA-3751) | Possible livelock during commit log playback |  Major | . | John Chakerian | Jonathan Ellis |
| [CASSANDRA-3835](https://issues.apache.org/jira/browse/CASSANDRA-3835) | FB.broadcastAddress fixes and Soft reset on Ec2MultiRegionSnitch.reconnect |  Minor | . | Vijay | Vijay |
| [CASSANDRA-3838](https://issues.apache.org/jira/browse/CASSANDRA-3838) | Repair Streaming hangs between multiple regions |  Minor | . | Vijay | Jason Brown |
| [CASSANDRA-3846](https://issues.apache.org/jira/browse/CASSANDRA-3846) | cqlsh can't show data under python2.5, python2.6 |  Minor | Tools | paul cannon | paul cannon |
| [CASSANDRA-3847](https://issues.apache.org/jira/browse/CASSANDRA-3847) | Pig should throw a useful error when the destination CF doesn't exist |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3844](https://issues.apache.org/jira/browse/CASSANDRA-3844) | Truncate leaves behind non-CFS backed secondary indexes |  Minor | Secondary Indexes | T Jake Luciani | Pavel Yaskevich |
| [CASSANDRA-3677](https://issues.apache.org/jira/browse/CASSANDRA-3677) | NPE during HH delivery when gossip turned off on target |  Trivial | . | Radim Kolar | Brandon Williams |
| [CASSANDRA-3876](https://issues.apache.org/jira/browse/CASSANDRA-3876) | nodetool removetoken force causes an inconsistent state |  Major | . | Sam Overton | Sam Overton |
| [CASSANDRA-3803](https://issues.apache.org/jira/browse/CASSANDRA-3803) | snapshot-before-compaction snapshots entire keyspace |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3843](https://issues.apache.org/jira/browse/CASSANDRA-3843) | Unnecessary  ReadRepair request during RangeScan |  Major | . | Philip Andronov | Jonathan Ellis |
| [CASSANDRA-3867](https://issues.apache.org/jira/browse/CASSANDRA-3867) | Disablethrift and Enablethrift can leaves behind zombie connections on THSHA server |  Major | . | Vijay | Vijay |
| [CASSANDRA-3886](https://issues.apache.org/jira/browse/CASSANDRA-3886) | Pig can't store some types after loading them |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3417](https://issues.apache.org/jira/browse/CASSANDRA-3417) | InvocationTargetException ConcurrentModificationException at startup |  Minor | . | Joaquin Casares | Peter Schuller |
| [CASSANDRA-3712](https://issues.apache.org/jira/browse/CASSANDRA-3712) | Can't cleanup after I moved a token. |  Major | . | Herve Nicol | Yuki Morishita |
| [CASSANDRA-3371](https://issues.apache.org/jira/browse/CASSANDRA-3371) | Cassandra inferred schema and actual data don't match |  Major | . | Pete Warden | Brandon Williams |
| [CASSANDRA-3905](https://issues.apache.org/jira/browse/CASSANDRA-3905) | fix typo in nodetool help for repair |  Trivial | . | Peter Schuller | Peter Schuller |
| [CASSANDRA-3913](https://issues.apache.org/jira/browse/CASSANDRA-3913) | Incorrect InetAddress equality test |  Trivial | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3915](https://issues.apache.org/jira/browse/CASSANDRA-3915) | Fix LazilyCompactedRowTest |  Minor | Testing | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3874](https://issues.apache.org/jira/browse/CASSANDRA-3874) | cqlsh: handle situation where data can't be deserialized as expected |  Minor | Tools | paul cannon | paul cannon |
| [CASSANDRA-3939](https://issues.apache.org/jira/browse/CASSANDRA-3939) | occasional failure of CliTest |  Minor | Testing | Eric Evans | Eric Evans |
| [CASSANDRA-3870](https://issues.apache.org/jira/browse/CASSANDRA-3870) | Internal error processing batch\_mutate: java.util.ConcurrentModificationException on CounterColumn |  Major | . | Viktor Jevdokimov | amorton |
| [CASSANDRA-3933](https://issues.apache.org/jira/browse/CASSANDRA-3933) | ./bin/cqlsh \`describe keyspace \<keyspace\>\` command doesn't work when ColumnFamily row\_cache\_provider wasn't specified. |  Minor | . | Pavel Yaskevich | paul cannon |
| [CASSANDRA-3767](https://issues.apache.org/jira/browse/CASSANDRA-3767) | cqlsh is missing cqlshlib (ImportError: No module named cqlshlib) |  Major | Tools | Matthew O'Riordan | paul cannon |
| [CASSANDRA-3934](https://issues.apache.org/jira/browse/CASSANDRA-3934) | Short read protection is broken |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3628](https://issues.apache.org/jira/browse/CASSANDRA-3628) | Make Pig/CassandraStorage delete functionality disabled by default and configurable |  Major | . | Jeremy Hanna | Brandon Williams |


