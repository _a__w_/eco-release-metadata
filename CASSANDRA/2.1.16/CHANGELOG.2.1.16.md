
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1.16 - 2016-10-10



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12040](https://issues.apache.org/jira/browse/CASSANDRA-12040) |   If a level compaction fails due to no space it should schedule the next one |  Minor | . | sankalp kohli | sankalp kohli |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12112](https://issues.apache.org/jira/browse/CASSANDRA-12112) | Tombstone histogram not accounting for partition deletions |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-11349](https://issues.apache.org/jira/browse/CASSANDRA-11349) | MerkleTree mismatch when multiple range tombstones exists for the same partition and interval |  Major | . | Fabien Rousseau | Branimir Lambov |
| [CASSANDRA-11850](https://issues.apache.org/jira/browse/CASSANDRA-11850) | cannot use cql since upgrading python to 2.7.11+ |  Major | Tools | Andrew Madison | Stefania |
| [CASSANDRA-11828](https://issues.apache.org/jira/browse/CASSANDRA-11828) | Commit log needs to track unflushed intervals rather than positions |  Major | Local Write-Read Paths | Branimir Lambov | Branimir Lambov |
| [CASSANDRA-12127](https://issues.apache.org/jira/browse/CASSANDRA-12127) | Queries with empty ByteBuffer values in clustering column restrictions fail for non-composite compact tables |  Major | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-11866](https://issues.apache.org/jira/browse/CASSANDRA-11866) | nodetool repair does not obey the column family parameter when -st and -et are provided (subrange repair) |  Major | Tools | Shiva Venkateswaran | Vinay Chella |
| [CASSANDRA-11363](https://issues.apache.org/jira/browse/CASSANDRA-11363) | High Blocked NTR When Connecting |  Major | Coordination | Russell Bradberry | T Jake Luciani |
| [CASSANDRA-12420](https://issues.apache.org/jira/browse/CASSANDRA-12420) | Duplicated Key in IN clause with a small fetch size will run forever |  Major | CQL | ZhaoYang | Tyler Hobbs |


