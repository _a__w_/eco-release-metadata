
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1.1 - 2014-10-24



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3569](https://issues.apache.org/jira/browse/CASSANDRA-3569) | Failure detector downs should not break streams |  Major | Streaming and Messaging | Peter Schuller | Joshua McKenzie |
| [CASSANDRA-6927](https://issues.apache.org/jira/browse/CASSANDRA-6927) | Create a CQL3 based bulk OutputFormat |  Minor | . | Paul Pak | Paul Pak |
| [CASSANDRA-7069](https://issues.apache.org/jira/browse/CASSANDRA-7069) | Prevent operator mistakes due to simultaneous bootstrap |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-7719](https://issues.apache.org/jira/browse/CASSANDRA-7719) | Add PreparedStatements related metrics |  Minor | Observability | Michaël Figuière | T Jake Luciani |
| [CASSANDRA-7131](https://issues.apache.org/jira/browse/CASSANDRA-7131) | Add command line option for cqlshrc file path |  Trivial | Tools | Jeremiah Jordan |  |
| [CASSANDRA-7988](https://issues.apache.org/jira/browse/CASSANDRA-7988) | 2.1 broke cqlsh for IPv6 |  Major | . | Josh Wright | Tyler Hobbs |
| [CASSANDRA-7507](https://issues.apache.org/jira/browse/CASSANDRA-7507) | OOM creates unreliable state - die instantly better |  Minor | Observability | Karl Mueller | Joshua McKenzie |
| [CASSANDRA-6602](https://issues.apache.org/jira/browse/CASSANDRA-6602) | Compaction improvements to optimize time series data |  Major | . | Tupshin Harper | Björn Hegerfors |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7444](https://issues.apache.org/jira/browse/CASSANDRA-7444) | Performance drops when creating large amount of tables |  Minor | . | Jose Martinez Poblete | Aleksey Yeschenko |
| [CASSANDRA-5263](https://issues.apache.org/jira/browse/CASSANDRA-5263) | Increase merkle tree depth as needed |  Major | Streaming and Messaging | Ahmed Bashir | Yuki Morishita |
| [CASSANDRA-6755](https://issues.apache.org/jira/browse/CASSANDRA-6755) | Optimise CellName/Composite comparisons for NativeCell |  Minor | Local Write-Read Paths | Benedict | T Jake Luciani |
| [CASSANDRA-7516](https://issues.apache.org/jira/browse/CASSANDRA-7516) | Configurable client timeout for cqlsh |  Major | Tools | Ryan McGuire | Ryan McGuire |
| [CASSANDRA-7417](https://issues.apache.org/jira/browse/CASSANDRA-7417) | Allow network configuration on interfaces instead of addresses |  Minor | Configuration | Matt Kennedy | Matt Kennedy |
| [CASSANDRA-7515](https://issues.apache.org/jira/browse/CASSANDRA-7515) | Clean up remains of non-working stack-allocation optimisation |  Trivial | . | Aleksey Yeschenko | Benedict |
| [CASSANDRA-7111](https://issues.apache.org/jira/browse/CASSANDRA-7111) | Include snippet of CQL query near error in SyntaxError messages |  Major | CQL, Tools | Tyler Hobbs | Benjamin Lerer |
| [CASSANDRA-7435](https://issues.apache.org/jira/browse/CASSANDRA-7435) | Add -D command-line parsing to Windows powershell launch scripts |  Trivial | Packaging | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-6910](https://issues.apache.org/jira/browse/CASSANDRA-6910) | Better table structure display in cqlsh |  Minor | Tools | Tupshin Harper | Mihai Suteu |
| [CASSANDRA-7575](https://issues.apache.org/jira/browse/CASSANDRA-7575) | Custom 2i validation |  Minor | CQL | Andrés de la Peña | Andrés de la Peña |
| [CASSANDRA-7432](https://issues.apache.org/jira/browse/CASSANDRA-7432) | Add new CMS GC flags to cassandra\_env.sh for JVM later than 1.7.0\_60 |  Major | Packaging | graham sanderson | Brandon Williams |
| [CASSANDRA-7595](https://issues.apache.org/jira/browse/CASSANDRA-7595) | EmbeddedCassandraService class should provide a stop method |  Minor | . | Mirko Tschäni | Lyuben Todorov |
| [CASSANDRA-7749](https://issues.apache.org/jira/browse/CASSANDRA-7749) | Windows cqlsh: prompt for install of pyreadline if missing during cqlsh init |  Trivial | Tools | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-7714](https://issues.apache.org/jira/browse/CASSANDRA-7714) | Add new CMS GC flags to Windows startup scripts for JVM later than 1.7.0\_60 |  Major | Packaging | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-7263](https://issues.apache.org/jira/browse/CASSANDRA-7263) | Improve the layout of the output of compactionstats |  Trivial | Tools | Nicolas Lalevée | Nicolas Lalevée |
| [CASSANDRA-7641](https://issues.apache.org/jira/browse/CASSANDRA-7641) | cqlsh should automatically disable tracing when selecting from system\_traces |  Minor | Tools | Brandon Williams | Philip Thompson |
| [CASSANDRA-7680](https://issues.apache.org/jira/browse/CASSANDRA-7680) | Make DatabaseDescriptor IPv6 compatible |  Trivial | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-7405](https://issues.apache.org/jira/browse/CASSANDRA-7405) | Optimize cqlsh COPY TO and COPY FROM |  Major | . | Aleksey Yeschenko | Mikhail Stepura |
| [CASSANDRA-7788](https://issues.apache.org/jira/browse/CASSANDRA-7788) | Improve PasswordAuthenticator default super user setup |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-7785](https://issues.apache.org/jira/browse/CASSANDRA-7785) | cqlsh - display the current logged-in user. |  Trivial | Tools | Aaron Ploetz |  |
| [CASSANDRA-7606](https://issues.apache.org/jira/browse/CASSANDRA-7606) | Add IF [NOT] EXISTS to CREATE/DROP trigger |  Minor | . | Robert Stupp | Benjamin Lerer |
| [CASSANDRA-6839](https://issues.apache.org/jira/browse/CASSANDRA-6839) | Support non equal conditions (for LWT) |  Minor | . | Sylvain Lebresne | Tyler Hobbs |
| [CASSANDRA-7789](https://issues.apache.org/jira/browse/CASSANDRA-7789) | cqlsh COPY Command should display progress |  Minor | . | Michaël Figuière | Mikhail Stepura |
| [CASSANDRA-7864](https://issues.apache.org/jira/browse/CASSANDRA-7864) | Repair should do less work when RF=1 |  Minor | Streaming and Messaging | Tyler Hobbs | Yuki Morishita |
| [CASSANDRA-7514](https://issues.apache.org/jira/browse/CASSANDRA-7514) | Support paging in cqlsh |  Minor | . | Sylvain Lebresne | Mikhail Stepura |
| [CASSANDRA-7901](https://issues.apache.org/jira/browse/CASSANDRA-7901) | Implement -f functionality in stop-server.bat |  Minor | . | Philip Thompson | Philip Thompson |
| [CASSANDRA-7519](https://issues.apache.org/jira/browse/CASSANDRA-7519) | Further stress improvements to generate more realistic workloads |  Minor | Tools | Benedict | Benedict |
| [CASSANDRA-7824](https://issues.apache.org/jira/browse/CASSANDRA-7824) | cqlsh completion for triggers |  Minor | . | Sylvain Lebresne | Mikhail Stepura |
| [CASSANDRA-7736](https://issues.apache.org/jira/browse/CASSANDRA-7736) | Clean-up, justify (and reduce) each use of @Inline |  Minor | Configuration | Benedict | T Jake Luciani |
| [CASSANDRA-7748](https://issues.apache.org/jira/browse/CASSANDRA-7748) | Get Windows command-line flags in-line with linux |  Trivial | Packaging | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-7468](https://issues.apache.org/jira/browse/CASSANDRA-7468) | Add time-based execution to cassandra-stress |  Minor | Tools | Matt Kennedy | Matt Kennedy |
| [CASSANDRA-7851](https://issues.apache.org/jira/browse/CASSANDRA-7851) | C\* PID file should be readable by mere users |  Minor | Packaging | Michael Shuler | Michael Shuler |
| [CASSANDRA-5433](https://issues.apache.org/jira/browse/CASSANDRA-5433) | Nodetool - Human Readable |  Trivial | . | Brooke Bryan | Michael Nelson |
| [CASSANDRA-7916](https://issues.apache.org/jira/browse/CASSANDRA-7916) | Stress should collect and cross-cluster GC statistics |  Minor | Tools | Benedict | Benedict |
| [CASSANDRA-7934](https://issues.apache.org/jira/browse/CASSANDRA-7934) | Remove FBUtilities.threadLocalRandom |  Minor | . | Benedict | Benedict |
| [CASSANDRA-7716](https://issues.apache.org/jira/browse/CASSANDRA-7716) | cassandra-stress: provide better error messages |  Trivial | Tools | Robert Stupp | T Jake Luciani |
| [CASSANDRA-7450](https://issues.apache.org/jira/browse/CASSANDRA-7450) | Make repair -pr work within a datacenter |  Major | Streaming and Messaging | Sylvain Lebresne | Paulo Motta |
| [CASSANDRA-7921](https://issues.apache.org/jira/browse/CASSANDRA-7921) | Provide visibility into prepared statement churn |  Minor | . | Nate McCall | Nate McCall |
| [CASSANDRA-7968](https://issues.apache.org/jira/browse/CASSANDRA-7968) | permissions\_validity\_in\_ms should be settable via JMX |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-7849](https://issues.apache.org/jira/browse/CASSANDRA-7849) | Server logged error messages (in binary protocol) for unexpected exceptions could be more helpful |  Major | . | graham sanderson | graham sanderson |
| [CASSANDRA-7930](https://issues.apache.org/jira/browse/CASSANDRA-7930) | Warn when evicting prepared statements from cache |  Major | . | Robbie Strickland | Robbie Strickland |
| [CASSANDRA-7977](https://issues.apache.org/jira/browse/CASSANDRA-7977) | Allow invalidating permissions cache |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-7963](https://issues.apache.org/jira/browse/CASSANDRA-7963) | stress should ignore rate limiting for warmup |  Minor | Tools | Brandon Williams | Benedict |
| [CASSANDRA-7978](https://issues.apache.org/jira/browse/CASSANDRA-7978) | CrcCheckChance should not be stored as part of an sstable |  Minor | Distributed Metadata, Local Write-Read Paths | sankalp kohli | T Jake Luciani |
| [CASSANDRA-7819](https://issues.apache.org/jira/browse/CASSANDRA-7819) | In progress compactions should not prevent deletion of stale sstables |  Minor | . | Benedict | Benedict |
| [CASSANDRA-7028](https://issues.apache.org/jira/browse/CASSANDRA-7028) | Allow C\* to compile under java 8 |  Minor | . | Dave Brosius | Branimir Lambov |
| [CASSANDRA-8046](https://issues.apache.org/jira/browse/CASSANDRA-8046) | Set base C\* version in debs and strip -N, ~textN, +textN |  Trivial | Packaging | Michael Shuler | Michael Shuler |
| [CASSANDRA-7909](https://issues.apache.org/jira/browse/CASSANDRA-7909) | Do not exit nodetool repair when receiving JMX NOTIF\_LOST |  Trivial | Tools | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-7316](https://issues.apache.org/jira/browse/CASSANDRA-7316) | Windows: address potential JVM swapping |  Minor | Lifecycle | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-7761](https://issues.apache.org/jira/browse/CASSANDRA-7761) | Upgrade netty and enable epoll event loop |  Minor | CQL | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-7676](https://issues.apache.org/jira/browse/CASSANDRA-7676) | bin/cassandra should complain if $JAVA is empty or not an executable |  Trivial | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-8088](https://issues.apache.org/jira/browse/CASSANDRA-8088) | Notify subscribers when a column family is truncated |  Minor | Observability | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-7776](https://issues.apache.org/jira/browse/CASSANDRA-7776) | Allow multiple MR jobs to concurrently write to the same column family from the same node using CqlBulkOutputFormat |  Minor | . | Paul Pak | Paul Pak |
| [CASSANDRA-7923](https://issues.apache.org/jira/browse/CASSANDRA-7923) | When preparing a statement, do not parse the provided string if we already have the parsed statement cached |  Minor | . | Benedict | Tyler Hobbs |
| [CASSANDRA-8111](https://issues.apache.org/jira/browse/CASSANDRA-8111) | Create backup directories for commitlog archiving during startup |  Trivial | . | Jan Karlsson |  |
| [CASSANDRA-8021](https://issues.apache.org/jira/browse/CASSANDRA-8021) | Improve cqlsh autocomplete for alter keyspace |  Minor | . | Philip Thompson | Rajanarayanan Thottuvaikkatumana |
| [CASSANDRA-7777](https://issues.apache.org/jira/browse/CASSANDRA-7777) | Ability to clean up local sstable files after they've been loaded by the CqlBulkRecordWriter |  Minor | . | Paul Pak | Paul Pak |
| [CASSANDRA-7341](https://issues.apache.org/jira/browse/CASSANDRA-7341) | Emit metrics related to CAS/Paxos |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-8113](https://issues.apache.org/jira/browse/CASSANDRA-8113) | Gossip should ignore generation numbers too far in the future |  Major | . | Richard Low | Jason Brown |
| [CASSANDRA-7658](https://issues.apache.org/jira/browse/CASSANDRA-7658) | stress connects to all nodes when it shouldn't |  Minor | Tools | Brandon Williams | Benedict |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5345](https://issues.apache.org/jira/browse/CASSANDRA-5345) | Potential problem with GarbageCollectorMXBean |  Minor | Observability | Matt Byrd | Joshua McKenzie |
| [CASSANDRA-7588](https://issues.apache.org/jira/browse/CASSANDRA-7588) | cqlsh error for query against collection index - list index out of range |  Major | . | dan jatnieks | Robert Stupp |
| [CASSANDRA-6454](https://issues.apache.org/jira/browse/CASSANDRA-6454) | Pig support for hadoop CqlInputFormat |  Major | . | Alex Liu | Alex Liu |
| [CASSANDRA-7611](https://issues.apache.org/jira/browse/CASSANDRA-7611) | incomplete CREATE/DROP USER help and tab completion |  Trivial | Documentation and Website | K. B. Hahn | Michał Michalski |
| [CASSANDRA-7582](https://issues.apache.org/jira/browse/CASSANDRA-7582) | 2.1 multi-dc upgrade errors |  Critical | . | Ryan McGuire |  |
| [CASSANDRA-7608](https://issues.apache.org/jira/browse/CASSANDRA-7608) | StressD can't create keyspaces with Write Command |  Minor | Tools | Russell Spitzer | Russell Spitzer |
| [CASSANDRA-7524](https://issues.apache.org/jira/browse/CASSANDRA-7524) | cqlsh fails when version number parts are not int |  Trivial | Tools | Alexander Bulaev | Mikhail Stepura |
| [CASSANDRA-7689](https://issues.apache.org/jira/browse/CASSANDRA-7689) | addSerializedKeyspace/UDT (prevent CASSANDRA-5631 for UDTs) |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-7498](https://issues.apache.org/jira/browse/CASSANDRA-7498) | sstable2json exports keys in a wrong format |  Minor | Tools | Alexey Zotov | Alexey Zotov |
| [CASSANDRA-7569](https://issues.apache.org/jira/browse/CASSANDRA-7569) | IndexOutOfBoundsException when building SyntaxError message snippet |  Minor | CQL | Tyler Hobbs | Benjamin Lerer |
| [CASSANDRA-7229](https://issues.apache.org/jira/browse/CASSANDRA-7229) | Hadoop2 jobs throw java.lang.IncompatibleClassChangeError |  Major | . | Mariusz Kryński | Mariusz Kryński |
| [CASSANDRA-7570](https://issues.apache.org/jira/browse/CASSANDRA-7570) | CqlPagingRecordReader is broken |  Major | . | Brandon Williams | Alex Liu |
| [CASSANDRA-7398](https://issues.apache.org/jira/browse/CASSANDRA-7398) | Missing flexibility to have file://\<server\>/\<etc\> vs. file:/// when loading config file cassandra.yaml |  Minor | Packaging | Marco Tulio Avila Cerón | Joshua McKenzie |
| [CASSANDRA-7628](https://issues.apache.org/jira/browse/CASSANDRA-7628) | Tools java driver needs to be updated |  Minor | Tools | Brandon Williams | Sylvain Lebresne |
| [CASSANDRA-7430](https://issues.apache.org/jira/browse/CASSANDRA-7430) | generate-eclipse-files target on JDK 8 causes ReferenceError: "importClass" is not defined |  Trivial | . | Kirk True |  |
| [CASSANDRA-7613](https://issues.apache.org/jira/browse/CASSANDRA-7613) | Error when tracing query with 2.1 cqlsh |  Major | . | T Jake Luciani | Robert Stupp |
| [CASSANDRA-7585](https://issues.apache.org/jira/browse/CASSANDRA-7585) | cassandra sstableloader connection refused with inter\_node\_encryption |  Major | Tools | Samphel Norden | Yuki Morishita |
| [CASSANDRA-7477](https://issues.apache.org/jira/browse/CASSANDRA-7477) | JSON to SSTable import failing |  Major | . | Kishan Karunaratne | Mikhail Stepura |
| [CASSANDRA-7252](https://issues.apache.org/jira/browse/CASSANDRA-7252) | RingCache cannot be configured to use local DC only |  Major | . | Robbie Strickland | Robbie Strickland |
| [CASSANDRA-7499](https://issues.apache.org/jira/browse/CASSANDRA-7499) | Unable to update list element by index using CAS condition |  Major | CQL | DOAN DuyHai | Sylvain Lebresne |
| [CASSANDRA-7577](https://issues.apache.org/jira/browse/CASSANDRA-7577) | cqlsh: CTRL-R history search not working on OSX |  Minor | . | Robert Stupp | Aleksey Yeschenko |
| [CASSANDRA-7506](https://issues.apache.org/jira/browse/CASSANDRA-7506) | querying secondary index using complete collection should warn/error |  Major | Secondary Indexes | Russ Hatch | Tyler Hobbs |
| [CASSANDRA-7774](https://issues.apache.org/jira/browse/CASSANDRA-7774) | CqlRecordReader creates wrong cluster if the first replica of a split is down |  Major | . | Jacek Lewandowski | Jacek Lewandowski |
| [CASSANDRA-7671](https://issues.apache.org/jira/browse/CASSANDRA-7671) | cqlsh: Error when printing results of conditional updates |  Major | Tools | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-7659](https://issues.apache.org/jira/browse/CASSANDRA-7659) | cqlsh: DESCRIBE KEYSPACE should order types according to cross-type dependencies |  Minor | Tools | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-7600](https://issues.apache.org/jira/browse/CASSANDRA-7600) | Schema change notifications sent for no-op DDL statements |  Minor | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-7815](https://issues.apache.org/jira/browse/CASSANDRA-7815) | cqlsh: COPY FROM cannot be interrupted with ctrl-c |  Minor | Tools | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-7799](https://issues.apache.org/jira/browse/CASSANDRA-7799) | cassandra-env.sh doesn't detect openjdk version numbers |  Minor | . | Ryan McGuire | Brandon Williams |
| [CASSANDRA-7804](https://issues.apache.org/jira/browse/CASSANDRA-7804) | Confusing error message when condition is set on primary key column |  Minor | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-7847](https://issues.apache.org/jira/browse/CASSANDRA-7847) | Allow quoted identifiers for triggers' names |  Minor | . | Mikhail Stepura | Mikhail Stepura |
| [CASSANDRA-7711](https://issues.apache.org/jira/browse/CASSANDRA-7711) | composite column not sliced when using IN clause on (other) composite columns |  Minor | CQL | Frens Jan Rumph | Benjamin Lerer |
| [CASSANDRA-7862](https://issues.apache.org/jira/browse/CASSANDRA-7862) | KeyspaceTest fails with heap\_buffers |  Minor | Local Write-Read Paths | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-7892](https://issues.apache.org/jira/browse/CASSANDRA-7892) | Hashmap IllegalStateException in anticompaction |  Major | . | Johan Bjork | Mikhail Stepura |
| [CASSANDRA-7895](https://issues.apache.org/jira/browse/CASSANDRA-7895) | ALTER TYPE \<name\> RENAME TO \<name\> no longer parses as valid cql |  Minor | . | Russ Hatch | Mikhail Stepura |
| [CASSANDRA-7828](https://issues.apache.org/jira/browse/CASSANDRA-7828) | New node cannot be joined if a value in composite type column is dropped (description updated) |  Major | . | Igor Zubchenok | Mikhail Stepura |
| [CASSANDRA-7856](https://issues.apache.org/jira/browse/CASSANDRA-7856) | Xss in test |  Minor | Testing | Cyril Scetbon | Tyler Hobbs |
| [CASSANDRA-7912](https://issues.apache.org/jira/browse/CASSANDRA-7912) | YamlFileNetworkTopologySnitch doesn't switch to dc-internal ip's anymore |  Major | . | Holger Bornträger | Holger Bornträger |
| [CASSANDRA-7917](https://issues.apache.org/jira/browse/CASSANDRA-7917) | deprecate the yaml snitch |  Trivial | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-7913](https://issues.apache.org/jira/browse/CASSANDRA-7913) | Don't try to set repairedAt on old sstables |  Minor | Streaming and Messaging | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-7889](https://issues.apache.org/jira/browse/CASSANDRA-7889) | Fix "java.lang.IllegalStateException: No configured daemon" in tests |  Trivial | . | Sylvain Lebresne | Steven Nelson |
| [CASSANDRA-7941](https://issues.apache.org/jira/browse/CASSANDRA-7941) | Fix bin/cassandra cassandra.logdir option in debian package |  Major | Packaging | Michael Shuler | Michael Shuler |
| [CASSANDRA-7784](https://issues.apache.org/jira/browse/CASSANDRA-7784) | DROP table leaves the counter and row cache in a temporarily inconsistent state that, if saved during, will cause an exception to be thrown |  Minor | . | Benedict | Aleksey Yeschenko |
| [CASSANDRA-7945](https://issues.apache.org/jira/browse/CASSANDRA-7945) | new cassanrda-stress does not work with NetworkTopologyStrategy |  Trivial | Tools | Yuki Morishita | Benedict |
| [CASSANDRA-7566](https://issues.apache.org/jira/browse/CASSANDRA-7566) | DROP TABLE should also drop prepared statements associated to |  Major | . | Ben Hood | Viju Kothuvatiparambil |
| [CASSANDRA-7940](https://issues.apache.org/jira/browse/CASSANDRA-7940) | Gossip only node removal can race with FD.convict |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-7967](https://issues.apache.org/jira/browse/CASSANDRA-7967) | Include schema\_triggers CF in readable system resources |  Major | . | Adam Holmberg | Adam Holmberg |
| [CASSANDRA-6125](https://issues.apache.org/jira/browse/CASSANDRA-6125) | Race condition in Gossip propagation |  Major | . | Sergio Bossa | Brandon Williams |
| [CASSANDRA-7932](https://issues.apache.org/jira/browse/CASSANDRA-7932) | Corrupt SSTable Cleanup Leak Resources |  Major | . | Benedict | Benedict |
| [CASSANDRA-7891](https://issues.apache.org/jira/browse/CASSANDRA-7891) | Select an element inside a UDT throws an index error |  Major | . | Patrick McFadin | Philip Thompson |
| [CASSANDRA-7946](https://issues.apache.org/jira/browse/CASSANDRA-7946) | NPE when streaming data to a joining node and dropping table in cluster |  Major | Streaming and Messaging | DOAN DuyHai | Yuki Morishita |
| [CASSANDRA-6075](https://issues.apache.org/jira/browse/CASSANDRA-6075) | The token function should allow column identifiers in the correct order only |  Minor | CQL | Michaël Figuière | Benjamin Lerer |
| [CASSANDRA-7995](https://issues.apache.org/jira/browse/CASSANDRA-7995) | sstablerepairedset should take more that one sstable as an argument |  Major | Tools | Nick Bailey | Carl Yeksigian |
| [CASSANDRA-7878](https://issues.apache.org/jira/browse/CASSANDRA-7878) | Fix wrong progress reporting when streaming uncompressed SSTable w/ CRC check |  Trivial | Streaming and Messaging, Tools | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-7939](https://issues.apache.org/jira/browse/CASSANDRA-7939) | checkForEndpointCollision should ignore joining nodes |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-7983](https://issues.apache.org/jira/browse/CASSANDRA-7983) | nodetool repair triggers OOM |  Major | Tools | Jose Martinez Poblete | Jimmy Mårdell |
| [CASSANDRA-8007](https://issues.apache.org/jira/browse/CASSANDRA-8007) | sstableloader throws exceptions when used with '-cph' |  Minor | Streaming and Messaging, Tools | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-6904](https://issues.apache.org/jira/browse/CASSANDRA-6904) | commitlog segments may not be archived after restart |  Major | Lifecycle, Local Write-Read Paths | Jonathan Ellis | Sam Tunnicliffe |
| [CASSANDRA-7956](https://issues.apache.org/jira/browse/CASSANDRA-7956) | "nodetool compactionhistory" crashes because of low heap size (GC overhead limit exceeded) |  Trivial | . | Nikolai Grigoriev | Michael Shuler |
| [CASSANDRA-7992](https://issues.apache.org/jira/browse/CASSANDRA-7992) | Arithmetic overflow sorting commit log segments on replay |  Major | . | Oleg Anastasyev | Oleg Anastasyev |
| [CASSANDRA-7885](https://issues.apache.org/jira/browse/CASSANDRA-7885) | Make native Server start() method block until the server is properly bound |  Minor | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-8034](https://issues.apache.org/jira/browse/CASSANDRA-8034) | AssertionError when STOP:ing compactions |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-8013](https://issues.apache.org/jira/browse/CASSANDRA-8013) | AssertionError on RangeTombstoneList.diff |  Major | . | Phil Yang | Phil Yang |
| [CASSANDRA-8020](https://issues.apache.org/jira/browse/CASSANDRA-8020) | nodetool repair on Cassandra 2.1.0 indexed tables returns java exception about creating snapshots |  Major | Tools | Jeronimo A Barros | Yuki Morishita |
| [CASSANDRA-7375](https://issues.apache.org/jira/browse/CASSANDRA-7375) | nodetool units wrong for streamthroughput |  Minor | . | Mike Heffner |  |
| [CASSANDRA-8040](https://issues.apache.org/jira/browse/CASSANDRA-8040) | Add bash-completion to debian/control Build-Depends |  Trivial | Packaging | Michael Shuler | Michael Shuler |
| [CASSANDRA-8045](https://issues.apache.org/jira/browse/CASSANDRA-8045) | Current check for cygwin environment in cassandra.ps1 is too greedy |  Trivial | Packaging | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-8044](https://issues.apache.org/jira/browse/CASSANDRA-8044) | stop-server.bat fails to call powershell if there's a space in the directory name |  Trivial | Packaging | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-7239](https://issues.apache.org/jira/browse/CASSANDRA-7239) | Nodetool Status Reports Negative Load With VNodes Disabled |  Major | Observability, Tools | Russell Spitzer | Marcus Eriksson |
| [CASSANDRA-7899](https://issues.apache.org/jira/browse/CASSANDRA-7899) | SSL does not work in cassandra-cli |  Major | Tools | Zdenek Ott | Jason Brown |
| [CASSANDRA-8033](https://issues.apache.org/jira/browse/CASSANDRA-8033) | Filtering for CONTAINS on sets is broken |  Major | . | Tuukka Mustonen | Tyler Hobbs |
| [CASSANDRA-7990](https://issues.apache.org/jira/browse/CASSANDRA-7990) | CompoundDenseCellNameType AssertionError and BoundedComposite to CellName ClasCastException |  Minor | . | Christian Spriegel | Tyler Hobbs |
| [CASSANDRA-8057](https://issues.apache.org/jira/browse/CASSANDRA-8057) | Record the real messaging version in all cases in OutboundTcpConnection |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-7734](https://issues.apache.org/jira/browse/CASSANDRA-7734) | Schema pushes (seemingly) randomly not happening |  Major | . | graham sanderson | Aleksey Yeschenko |
| [CASSANDRA-7993](https://issues.apache.org/jira/browse/CASSANDRA-7993) | Fat client nodes dont schedule schema pull on connect |  Major | . | Oleg Anastasyev | Oleg Anastasyev |
| [CASSANDRA-7616](https://issues.apache.org/jira/browse/CASSANDRA-7616) | sstablesplit creates new sstable when size is less than split size |  Minor | Tools | Tyler Hobbs | Changsu Jiang |
| [CASSANDRA-7256](https://issues.apache.org/jira/browse/CASSANDRA-7256) | Error when dropping keyspace. |  Major | . | Steven Lowenthal | Aleksey Yeschenko |
| [CASSANDRA-8058](https://issues.apache.org/jira/browse/CASSANDRA-8058) | local consistency level during boostrap (may cause a write timeout on each write request) |  Major | . | Nicolas DOUILLET | Nicolas DOUILLET |
| [CASSANDRA-7546](https://issues.apache.org/jira/browse/CASSANDRA-7546) | AtomicSortedColumns.addAllWithSizeDelta has a spin loop that allocates memory |  Major | . | graham sanderson | graham sanderson |
| [CASSANDRA-8066](https://issues.apache.org/jira/browse/CASSANDRA-8066) | High Heap Consumption due to high number of SSTableReader |  Major | Compaction, Local Write-Read Paths | Benoit Lacelle | T Jake Luciani |
| [CASSANDRA-8101](https://issues.apache.org/jira/browse/CASSANDRA-8101) | Invalid ASCII and UTF-8 chars not rejected in CQL string literals |  Critical | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-8104](https://issues.apache.org/jira/browse/CASSANDRA-8104) | frozen broken / frozen collections in frozen tuple type |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-8118](https://issues.apache.org/jira/browse/CASSANDRA-8118) | Exceptions while decoding native protocol messages are not handled correctly |  Major | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-8073](https://issues.apache.org/jira/browse/CASSANDRA-8073) | Exception filtering data in a collection and compound primary key |  Major | . | Ozéias Sant'ana | Tyler Hobbs |
| [CASSANDRA-8105](https://issues.apache.org/jira/browse/CASSANDRA-8105) | NPE for null embedded UDT inside set |  Major | . | Andy Ketskes | Robert Stupp |
| [CASSANDRA-8108](https://issues.apache.org/jira/browse/CASSANDRA-8108) | Errors paging DISTINCT queries on static columns |  Major | . | David Hearnden | Tyler Hobbs |
| [CASSANDRA-6998](https://issues.apache.org/jira/browse/CASSANDRA-6998) | HintedHandoff - expired hints may block future hints deliveries |  Major | . | Scooletz | Aleksey Yeschenko |
| [CASSANDRA-8062](https://issues.apache.org/jira/browse/CASSANDRA-8062) | IllegalArgumentException passing blob as tuple value element in list |  Major | . | Bill Mitchell | Tyler Hobbs |
| [CASSANDRA-7446](https://issues.apache.org/jira/browse/CASSANDRA-7446) | Batchlog should be streamed to a different node on decom |  Major | . | Aleksey Yeschenko | Branimir Lambov |
| [CASSANDRA-6430](https://issues.apache.org/jira/browse/CASSANDRA-6430) | DELETE with IF \<field\>=\<value\> clause doesn't work properly if more then one row are going to be deleted |  Major | . | Dmitriy Ukhlov | Tyler Hobbs |
| [CASSANDRA-8054](https://issues.apache.org/jira/browse/CASSANDRA-8054) | EXECUTE request with skipMetadata=false gets no metadata in response |  Major | CQL | Olivier Michallat | Sylvain Lebresne |
| [CASSANDRA-8027](https://issues.apache.org/jira/browse/CASSANDRA-8027) | Assertion error in CompressionParameters |  Major | Distributed Metadata | Carl Yeksigian | T Jake Luciani |
| [CASSANDRA-8146](https://issues.apache.org/jira/browse/CASSANDRA-8146) | Fix validation of collections in TriggerExecutor |  Major | Coordination | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-8131](https://issues.apache.org/jira/browse/CASSANDRA-8131) | Short-circuited query results from collection index query |  Major | CQL | Catalin Alexandru Zamfir | Benjamin Lerer |
| [CASSANDRA-8157](https://issues.apache.org/jira/browse/CASSANDRA-8157) | Opening results early with leveled compactions broken |  Critical | Compaction | Marcus Eriksson | Marcus Eriksson |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6312](https://issues.apache.org/jira/browse/CASSANDRA-6312) | Create dtest suite for user types |  Major | . | Ryan McGuire | Russ Hatch |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7673](https://issues.apache.org/jira/browse/CASSANDRA-7673) | Update cqlsh tests to support new colored output |  Minor | Tools | Mikhail Stepura | Mikhail Stepura |
| [CASSANDRA-7972](https://issues.apache.org/jira/browse/CASSANDRA-7972) | Add "CREATE INDEX ... ON ..(KEYS())" syntax to cqlsh and CQL.textile |  Minor | Tools | Mikhail Stepura | Philip Thompson |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6599](https://issues.apache.org/jira/browse/CASSANDRA-6599) | CQL updates should support "column = column - { key1, key2, ... }" syntax for removing map elements |  Minor | . | Gavin | Benjamin Lerer |
| [CASSANDRA-7173](https://issues.apache.org/jira/browse/CASSANDRA-7173) | Make "nodetool [ring\|status] system" return a message that ownership is nonsense |  Trivial | Tools | Jeremy Hanna | Steven Nelson |
| [CASSANDRA-7703](https://issues.apache.org/jira/browse/CASSANDRA-7703) | Fix failing cqlsh tests |  Major | Testing | Tyler Hobbs | Tyler Hobbs |


