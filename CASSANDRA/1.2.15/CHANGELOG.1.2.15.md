
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.2.15 - 2014-02-07



### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6648](https://issues.apache.org/jira/browse/CASSANDRA-6648) | Race condition during node bootstrapping |  Critical | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-6647](https://issues.apache.org/jira/browse/CASSANDRA-6647) | Config param [compaction\_throughput\_mb\_per\_sec] unsafe calculation. |  Trivial | . | Platon Vai | Platon Vai |


