
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.2.13 - 2018-08-01



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-14183](https://issues.apache.org/jira/browse/CASSANDRA-14183) | CVE-2017-5929 Security vulnerability and redefine default log rotation policy |  Major | Libraries | Thiago Veronezi | Thiago Veronezi |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-14318](https://issues.apache.org/jira/browse/CASSANDRA-14318) | Fix query pager DEBUG log leak causing hit in paged reads throughput |  Major | . | Alexander Dejanovski | Alexander Dejanovski |
| [CASSANDRA-13891](https://issues.apache.org/jira/browse/CASSANDRA-13891) | fromJson(null) throws java.lang.NullPointerException on Cassandra |  Minor | CQL | Marcel Villet | Edward Ribeiro |
| [CASSANDRA-14284](https://issues.apache.org/jira/browse/CASSANDRA-14284) | Chunk checksum test needs to occur before uncompress to avoid JVM crash |  Major | Core | Gil Tene | Benjamin Lerer |
| [CASSANDRA-14286](https://issues.apache.org/jira/browse/CASSANDRA-14286) | IndexOutOfBoundsException with SELECT JSON using IN and ORDER BY |  Major | CQL | Szymon Acedański | Francisco Fernandez |
| [CASSANDRA-14411](https://issues.apache.org/jira/browse/CASSANDRA-14411) | Use Bounds instead of Range to represent sstable first/last token when checking how to anticompact sstables |  Major | Repair | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-12743](https://issues.apache.org/jira/browse/CASSANDRA-12743) | Assertion error while running compaction |  Major | Compaction | Jean-Baptiste Le Duigou | Jay Zhuang |
| [CASSANDRA-11551](https://issues.apache.org/jira/browse/CASSANDRA-11551) | Incorrect counting of pending messages in OutboundTcpConnection |  Minor | . | Robert Stupp | Jaydeepkumar Chovatia |
| [CASSANDRA-14238](https://issues.apache.org/jira/browse/CASSANDRA-14238) | Flaky Unittest: org.apache.cassandra.db.compaction.BlacklistingCompactionsTest |  Minor | Testing | Jay Zhuang | Marcus Eriksson |
| [CASSANDRA-14423](https://issues.apache.org/jira/browse/CASSANDRA-14423) | SSTables stop being compacted |  Blocker | Compaction | Kurt Greaves | Kurt Greaves |
| [CASSANDRA-14603](https://issues.apache.org/jira/browse/CASSANDRA-14603) | [dtest] read\_repair\_test.TestReadRepair |  Major | Testing | Jason Brown | Sam Tunnicliffe |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-14240](https://issues.apache.org/jira/browse/CASSANDRA-14240) | Backport circleci yaml |  Trivial | . | Jason Brown | Jason Brown |


