
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.0.3 - 2013-11-25



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5894](https://issues.apache.org/jira/browse/CASSANDRA-5894) | CQL-aware SSTableWriter |  Minor | Tools | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-6135](https://issues.apache.org/jira/browse/CASSANDRA-6135) | Add beforeChange Notification to Gossiper State. |  Major | . | Benjamin Coverston | Sergio Bossa |
| [CASSANDRA-6109](https://issues.apache.org/jira/browse/CASSANDRA-6109) | Consider coldness in STCS compaction |  Major | . | Jonathan Ellis | Tyler Hobbs |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6241](https://issues.apache.org/jira/browse/CASSANDRA-6241) | Assertion on MmappedSegmentedFile.floor doesn't tell us the path (filename) |  Trivial | . | Jackson Chung | Jonathan Ellis |
| [CASSANDRA-6248](https://issues.apache.org/jira/browse/CASSANDRA-6248) | Add IRequestSink interface as an analogue to IMessageSink for coordinator-local requests |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6249](https://issues.apache.org/jira/browse/CASSANDRA-6249) | Keep memtable data size updated even during flush; add a method to calculate total memtables size (incl pending flush) |  Trivial | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6252](https://issues.apache.org/jira/browse/CASSANDRA-6252) | Provide hooks around CQL2/CQL3 statement execution and preparation |  Minor | . | Aleksey Yeschenko | Sam Tunnicliffe |
| [CASSANDRA-6273](https://issues.apache.org/jira/browse/CASSANDRA-6273) | nodetool should get default JMX port from cassandra-env.sh |  Trivial | Tools | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-6290](https://issues.apache.org/jira/browse/CASSANDRA-6290) | Switch to JDK7's StandardCharsets |  Minor | . | Blair Zajac | Blair Zajac |
| [CASSANDRA-6244](https://issues.apache.org/jira/browse/CASSANDRA-6244) | calculatePendingRanges could be asynchronous on 1.2 too |  Major | . | Ryan Fowler | Ryan Fowler |
| [CASSANDRA-4809](https://issues.apache.org/jira/browse/CASSANDRA-4809) | Allow restoring specific column families from archived commitlog |  Major | . | Nick Bailey | Lyuben Todorov |
| [CASSANDRA-6386](https://issues.apache.org/jira/browse/CASSANDRA-6386) | FD mean calculation performance improvement |  Minor | . | Quentin Conner | Jonathan Ellis |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6232](https://issues.apache.org/jira/browse/CASSANDRA-6232) | Installation shouldn't fail if /etc/sysctl.d/cassandra is deleted |  Minor | Packaging | Faidon Liambotis | Brandon Williams |
| [CASSANDRA-6180](https://issues.apache.org/jira/browse/CASSANDRA-6180) | NPE in CqlRecordWriter: Related to AbstractCassandraStorage handling null values |  Major | . | Henning Kropp | Alex Liu |
| [CASSANDRA-6182](https://issues.apache.org/jira/browse/CASSANDRA-6182) | Unable to modify column\_metadata via thrift |  Major | . | Nick Bailey | Sylvain Lebresne |
| [CASSANDRA-6190](https://issues.apache.org/jira/browse/CASSANDRA-6190) | Cassandra 2.0 won't start up with Java 7u40 with Client JVM.  (works on Server JVM, and both JVMs 7u25) |  Major | Configuration | Steven Lowenthal | Brandon Williams |
| [CASSANDRA-6242](https://issues.apache.org/jira/browse/CASSANDRA-6242) | CQL LIST USERS does nothing after a user is created. |  Minor | Tools | Ben Sykes | Mikhail Stepura |
| [CASSANDRA-6247](https://issues.apache.org/jira/browse/CASSANDRA-6247) | CAS updates should require P.MODIFY AND P.SELECT |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6254](https://issues.apache.org/jira/browse/CASSANDRA-6254) | Thrift's prepare\_cql\*\_query() should validate login |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5988](https://issues.apache.org/jira/browse/CASSANDRA-5988) | Make hint TTL customizable |  Major | . | Oleg Kibirev | Vishy Kasar |
| [CASSANDRA-6258](https://issues.apache.org/jira/browse/CASSANDRA-6258) | Need the root clause in FBUtilities.classForName when there is exception loading class |  Major | . | Jackson Chung | Jackson Chung |
| [CASSANDRA-6274](https://issues.apache.org/jira/browse/CASSANDRA-6274) | fixes for compacting larger-than-memory rows |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-6267](https://issues.apache.org/jira/browse/CASSANDRA-6267) | restrict max num\_tokens to something Gossip can handle |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-6270](https://issues.apache.org/jira/browse/CASSANDRA-6270) | SERIAL consistency in errors to v1 protocol driver |  Minor | . | Theo Hultberg | Sylvain Lebresne |
| [CASSANDRA-6260](https://issues.apache.org/jira/browse/CASSANDRA-6260) | Cassandra 2.0.1 OutOfMemoryError:  Requested array size exceeds VM limit |  Minor | . | Prateek | Mikhail Stepura |
| [CASSANDRA-6277](https://issues.apache.org/jira/browse/CASSANDRA-6277) | AE in PrecompactedRow.update(PrecompactedRow.java:171) |  Minor | . | Viliam Holub | Jonathan Ellis |
| [CASSANDRA-6287](https://issues.apache.org/jira/browse/CASSANDRA-6287) | Avoid flushing compaction\_history after each operation |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5981](https://issues.apache.org/jira/browse/CASSANDRA-5981) | Netty frame length exception when storing data to Cassandra using binary protocol |  Minor | . | Justin Sweeney | Sylvain Lebresne |
| [CASSANDRA-6297](https://issues.apache.org/jira/browse/CASSANDRA-6297) | Gossiper blocks when updating tokens and turns node down |  Major | . | Sergio Bossa | Jonathan Ellis |
| [CASSANDRA-6299](https://issues.apache.org/jira/browse/CASSANDRA-6299) | Serialization bug in PagedRangeCommand |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6140](https://issues.apache.org/jira/browse/CASSANDRA-6140) | Cassandra-cli backward compatibility issue with Cassandra 2.0.1 |  Major | . | DOAN DuyHai | Sylvain Lebresne |
| [CASSANDRA-6302](https://issues.apache.org/jira/browse/CASSANDRA-6302) | make CqlPagingRecordReader more robust to failures |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-6301](https://issues.apache.org/jira/browse/CASSANDRA-6301) | nodetool fails with java.lang.UnsatisfiedLinkError (cannot find libjemalloc.so) when JEMalloc is configured |  Trivial | Tools | Nikolai Grigoriev |  |
| [CASSANDRA-6308](https://issues.apache.org/jira/browse/CASSANDRA-6308) | Thread leak caused in creating OutboundTcpConnectionPool |  Minor | . | Minh Do | Minh Do |
| [CASSANDRA-6238](https://issues.apache.org/jira/browse/CASSANDRA-6238) | LOCAL\_ONE doesn't work for SimpleStrategy |  Major | . | Alex Liu | Alex Liu |
| [CASSANDRA-6317](https://issues.apache.org/jira/browse/CASSANDRA-6317) | cqlsh should handle 'null' as session duration |  Minor | . | Matt Jurik | Mikhail Stepura |
| [CASSANDRA-6322](https://issues.apache.org/jira/browse/CASSANDRA-6322) | Classcast Exception thrown when under load |  Major | . | Jinder Aujla | Jonathan Ellis |
| [CASSANDRA-6338](https://issues.apache.org/jira/browse/CASSANDRA-6338) | Make gossip tolerate slow Gossip tasks |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-6343](https://issues.apache.org/jira/browse/CASSANDRA-6343) | C\* throws AssertionError when using paging and reverse ordering |  Major | . | Alexey Chumakov | Sylvain Lebresne |
| [CASSANDRA-6337](https://issues.apache.org/jira/browse/CASSANDRA-6337) | Set minTimestamp correctly to be able to drop expired sstables |  Minor | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-6327](https://issues.apache.org/jira/browse/CASSANDRA-6327) | select with "in" clause wrongly returns empty result |  Major | . | Duncan Sands | Sylvain Lebresne |
| [CASSANDRA-6341](https://issues.apache.org/jira/browse/CASSANDRA-6341) | After executing abnormal cql statement, not working  (hang) |  Minor | . | Kim Yong Hwan | Sylvain Lebresne |
| [CASSANDRA-6349](https://issues.apache.org/jira/browse/CASSANDRA-6349) | IOException in MessagingService.run() causes orphaned storage server socket |  Major | . | Steven Halaka | Mikhail Stepura |
| [CASSANDRA-6359](https://issues.apache.org/jira/browse/CASSANDRA-6359) | sstableloader does not free off-heap memory for index summary |  Minor | Tools | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-6358](https://issues.apache.org/jira/browse/CASSANDRA-6358) | SSTable read meter sync not cancelled when reader is closed |  Major | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-6351](https://issues.apache.org/jira/browse/CASSANDRA-6351) | When dropping a CF, row cache is not invalidated |  Minor | . | Fabien Rousseau | Fabien Rousseau |
| [CASSANDRA-6369](https://issues.apache.org/jira/browse/CASSANDRA-6369) | Fix prepared statement size computation |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6380](https://issues.apache.org/jira/browse/CASSANDRA-6380) | SSTableReader.loadSummary may leave an open file |  Minor | . | Mikhail Stepura | Mikhail Stepura |
| [CASSANDRA-6275](https://issues.apache.org/jira/browse/CASSANDRA-6275) | 2.0.x leaks file handles |  Major | . | Mikhail Mazurskiy | graham sanderson |
| [CASSANDRA-6385](https://issues.apache.org/jira/browse/CASSANDRA-6385) | FD phi estimator initial conditions |  Minor | . | Quentin Conner | Jonathan Ellis |
| [CASSANDRA-6374](https://issues.apache.org/jira/browse/CASSANDRA-6374) | AssertionError for rows with zero columns |  Major | . | Anton Gorbunov | Jonathan Ellis |
| [CASSANDRA-6333](https://issues.apache.org/jira/browse/CASSANDRA-6333) | ArrayIndexOutOfBound when using count(\*) with over 10,000 rows |  Major | . | Tyler Tolley | Sylvain Lebresne |
| [CASSANDRA-6342](https://issues.apache.org/jira/browse/CASSANDRA-6342) | cassandra document errata |  Trivial | . | Kim Yong Hwan | Lyuben Todorov |
| [CASSANDRA-6316](https://issues.apache.org/jira/browse/CASSANDRA-6316) | json2sstable breaks on RangeTombstone |  Minor | . | Matt Jurik | Lyuben Todorov |
| [CASSANDRA-6325](https://issues.apache.org/jira/browse/CASSANDRA-6325) | AssertionError on startup reading saved Serializing row cache |  Minor | . | Chris Burroughs | Mikhail Stepura |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6293](https://issues.apache.org/jira/browse/CASSANDRA-6293) | CASSANDRA-6107 causes EmbeddedCassandraService to fail |  Minor | . | Michael Oczkowski | Mikhail Stepura |
| [CASSANDRA-6003](https://issues.apache.org/jira/browse/CASSANDRA-6003) | CQL: Support NaN and inifinities in Double literals |  Minor | . | Alex Cruise | Sylvain Lebresne |


