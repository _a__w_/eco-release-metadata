
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.8.1 - 2011-06-28



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2355](https://issues.apache.org/jira/browse/CASSANDRA-2355) | Allow parameters for comparator |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2231](https://issues.apache.org/jira/browse/CASSANDRA-2231) | Add CompositeType comparer to the comparers provided in org.apache.cassandra.db.marshal |  Minor | . | Ed Anuff | Sylvain Lebresne |
| [CASSANDRA-2537](https://issues.apache.org/jira/browse/CASSANDRA-2537) | CQL: Support for batch insert/delete |  Minor | . | Cathy Daw | Pavel Yaskevich |
| [CASSANDRA-2616](https://issues.apache.org/jira/browse/CASSANDRA-2616) | Add "DROP INDEX" command to CLI |  Major | . | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-2531](https://issues.apache.org/jira/browse/CASSANDRA-2531) | There should be an easy way to see what DC and rack a node belongs to |  Trivial | Tools | Brandon Williams | Alex Araujo |
| [CASSANDRA-2617](https://issues.apache.org/jira/browse/CASSANDRA-2617) | Add "DROP INDEX" command to CQL |  Major | CQL | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-2733](https://issues.apache.org/jira/browse/CASSANDRA-2733) | nodetool ring with EC2Snitch, NPE checking for the zone and dc |  Minor | . | Vijay | Vijay |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2555](https://issues.apache.org/jira/browse/CASSANDRA-2555) | Explicit timestamp support for CQL |  Major | CQL | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-2553](https://issues.apache.org/jira/browse/CASSANDRA-2553) | Add IN support to CQL SELECT, UPDATE statements |  Major | CQL | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-2583](https://issues.apache.org/jira/browse/CASSANDRA-2583) | optimize batches containing multiple updates to the same rows |  Minor | CQL | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-2639](https://issues.apache.org/jira/browse/CASSANDRA-2639) | Make cassandra.yaml seed requirements a small bit clearer |  Trivial | . | Joaquin Casares | Joaquin Casares |
| [CASSANDRA-2411](https://issues.apache.org/jira/browse/CASSANDRA-2411) | log why a SSTable is being deleted |  Minor | . | Robert Coli | Jonathan Ellis |
| [CASSANDRA-2659](https://issues.apache.org/jira/browse/CASSANDRA-2659) | Improve forceDeserialize/getCompactedRow encapsulation |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1278](https://issues.apache.org/jira/browse/CASSANDRA-1278) | Make bulk loading into Cassandra less crappy, more pluggable |  Major | Tools | Jeremy Hanna | Sylvain Lebresne |
| [CASSANDRA-2693](https://issues.apache.org/jira/browse/CASSANDRA-2693) | Renaming/Dropping Keyspace shouldn't touch hints |  Trivial | . | Nicholas Telford | Nicholas Telford |
| [CASSANDRA-2697](https://issues.apache.org/jira/browse/CASSANDRA-2697) | "insufficient space to compact even the two smallest files, aborting" |  Trivial | . | Muga Nishizawa |  |
| [CASSANDRA-2683](https://issues.apache.org/jira/browse/CASSANDRA-2683) | Allow writes to bypass the commit log for certain keyspaces |  Major | . | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-2714](https://issues.apache.org/jira/browse/CASSANDRA-2714) | Throttle migration replay |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2716](https://issues.apache.org/jira/browse/CASSANDRA-2716) | avoid allocating a new serializer per ColumnFamily (row) |  Trivial | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2722](https://issues.apache.org/jira/browse/CASSANDRA-2722) | nodetool statusthrift |  Trivial | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-2267](https://issues.apache.org/jira/browse/CASSANDRA-2267) | stress.java daemon mode |  Major | . | Brandon Williams | Pavel Yaskevich |
| [CASSANDRA-2702](https://issues.apache.org/jira/browse/CASSANDRA-2702) | Extend Row Cache Provider support |  Minor | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-2654](https://issues.apache.org/jira/browse/CASSANDRA-2654) | Work around native heap leak in sun.nio.ch.Util affecting IncomingTcpConnection |  Major | . | Hannes Schmidt | Hannes Schmidt |
| [CASSANDRA-2743](https://issues.apache.org/jira/browse/CASSANDRA-2743) | add TABLE as a CQL alias for COLUMNFAMILY |  Minor | CQL | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-2386](https://issues.apache.org/jira/browse/CASSANDRA-2386) | sstable2json does not work on snapshot without moving the files |  Minor | Tools | Aslak Dirdal | Patricio Echague |
| [CASSANDRA-2756](https://issues.apache.org/jira/browse/CASSANDRA-2756) | Make CQL CREATE KS/CF not return until the operation is complete |  Minor | CQL | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-2715](https://issues.apache.org/jira/browse/CASSANDRA-2715) | simplify schema reconciliation |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2530](https://issues.apache.org/jira/browse/CASSANDRA-2530) | Additional AbstractType data type definitions to enrich CQL |  Trivial | . | Rick Shaw | Rick Shaw |
| [CASSANDRA-2788](https://issues.apache.org/jira/browse/CASSANDRA-2788) | Add startup option renew the NodeId (for counters) |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2807](https://issues.apache.org/jira/browse/CASSANDRA-2807) | ColumnFamilyInputFormat configuration should support multiple initial addresses |  Minor | . | Greg Katz | mck |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2631](https://issues.apache.org/jira/browse/CASSANDRA-2631) | Replaying a commitlog entry from a dropped keyspace will cause an error |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2637](https://issues.apache.org/jira/browse/CASSANDRA-2637) | bloom filter true positives not counted unless key cache is enabled |  Trivial | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2633](https://issues.apache.org/jira/browse/CASSANDRA-2633) | Keys get lost in bootstrap |  Critical | . | Richard Low | Richard Low |
| [CASSANDRA-2651](https://issues.apache.org/jira/browse/CASSANDRA-2651) | Inferred Rack and DC Values Should be Unsigned |  Minor | . | Jerry Pisk |  |
| [CASSANDRA-2660](https://issues.apache.org/jira/browse/CASSANDRA-2660) | BRAF.sync() bug can cause massive commit log write magnification |  Minor | . | Peter Schuller | Peter Schuller |
| [CASSANDRA-2668](https://issues.apache.org/jira/browse/CASSANDRA-2668) | don't perform HH to client-mode nodes |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2685](https://issues.apache.org/jira/browse/CASSANDRA-2685) | NPE in Table.createReplicationStrategy during sends from HintedHandOffManager |  Minor | . | Marko Mikulicic | Jonathan Ellis |
| [CASSANDRA-2680](https://issues.apache.org/jira/browse/CASSANDRA-2680) | range scan doesn't repair missing rows |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2597](https://issues.apache.org/jira/browse/CASSANDRA-2597) | inconsistent implementation of 'cumulative distribution function' for Exponential Distribution |  Minor | . | Jonathan Ellis | paul cannon |
| [CASSANDRA-2675](https://issues.apache.org/jira/browse/CASSANDRA-2675) | java.io.IOError: java.io.EOFException with version 0.7.6 |  Minor | . | rene kochen | Sylvain Lebresne |
| [CASSANDRA-2718](https://issues.apache.org/jira/browse/CASSANDRA-2718) | NPE in SSTableWriter when no ReplayPosition availible |  Trivial | . | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-2721](https://issues.apache.org/jira/browse/CASSANDRA-2721) | nodetool statusthrift exception while node starts up |  Trivial | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-2644](https://issues.apache.org/jira/browse/CASSANDRA-2644) | Make bootstrap retry |  Major | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-2723](https://issues.apache.org/jira/browse/CASSANDRA-2723) | Rows that don't exist get cached |  Minor | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-2673](https://issues.apache.org/jira/browse/CASSANDRA-2673) | AssertionError post truncate |  Minor | . | Marko Mikulicic | Jonathan Ellis |
| [CASSANDRA-2730](https://issues.apache.org/jira/browse/CASSANDRA-2730) | exception generate when using same index names |  Minor | . | BAONING WU |  |
| [CASSANDRA-2590](https://issues.apache.org/jira/browse/CASSANDRA-2590) | row delete breaks read repair |  Minor | . | amorton | amorton |
| [CASSANDRA-2744](https://issues.apache.org/jira/browse/CASSANDRA-2744) | stress.jar is not executable |  Minor | Tools | Tyler Hobbs | Pavel Yaskevich |
| [CASSANDRA-2765](https://issues.apache.org/jira/browse/CASSANDRA-2765) | DataTracker.View.MarkCompacting adds ALL sstables and marks them as compacting |  Major | . | Benjamin Coverston | Benjamin Coverston |
| [CASSANDRA-2752](https://issues.apache.org/jira/browse/CASSANDRA-2752) | repair fails with java.io.EOFException |  Critical | . | Terje Marthinussen | Jonathan Ellis |
| [CASSANDRA-2759](https://issues.apache.org/jira/browse/CASSANDRA-2759) | Scrub could lose increments and replicate that loss |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2767](https://issues.apache.org/jira/browse/CASSANDRA-2767) | ConcurrentModificationException in AntiEntropyService.getNeighbors() |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2758](https://issues.apache.org/jira/browse/CASSANDRA-2758) | nodetool repair never finishes. Loops forever through merkle trees? |  Minor | . | Terje Marthinussen | Sylvain Lebresne |
| [CASSANDRA-2669](https://issues.apache.org/jira/browse/CASSANDRA-2669) | Scrub does not close files |  Minor | Tools | Daniel Doubleday | Jonathan Ellis |
| [CASSANDRA-2781](https://issues.apache.org/jira/browse/CASSANDRA-2781) | regression: exposing cache size through MBean |  Minor | . | Chris Burroughs | Chris Burroughs |
| [CASSANDRA-2766](https://issues.apache.org/jira/browse/CASSANDRA-2766) | ConcurrentModificationException during node recovery |  Major | . | Terje Marthinussen | Jonathan Ellis |
| [CASSANDRA-2787](https://issues.apache.org/jira/browse/CASSANDRA-2787) | java agent option missing in cassandra.bat file |  Minor | Packaging | rene kochen | rene kochen |
| [CASSANDRA-2797](https://issues.apache.org/jira/browse/CASSANDRA-2797) | Repair hangs if a neighbor has nothing to send |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2801](https://issues.apache.org/jira/browse/CASSANDRA-2801) | Tombstone are not purged when the row is in only one sstable |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2831](https://issues.apache.org/jira/browse/CASSANDRA-2831) | Creating or updating CF key\_validation\_class with the CLI doesn't works |  Major | . | Silvère Lestang |  |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2476](https://issues.apache.org/jira/browse/CASSANDRA-2476) | CQL support for column TTLs |  Major | CQL | Eric Evans | Pavel Yaskevich |
| [CASSANDRA-1709](https://issues.apache.org/jira/browse/CASSANDRA-1709) | CQL keyspace and column family management |  Minor | CQL | Eric Evans | Pavel Yaskevich |
| [CASSANDRA-2473](https://issues.apache.org/jira/browse/CASSANDRA-2473) | CQL support for counters |  Major | CQL | Eric Evans | Pavel Yaskevich |
| [CASSANDRA-2480](https://issues.apache.org/jira/browse/CASSANDRA-2480) | Named keys / virtual columns |  Major | CQL | Eric Evans | Pavel Yaskevich |


