
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.6.5 - 2010-08-27



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-981](https://issues.apache.org/jira/browse/CASSANDRA-981) | Dynamic endpoint snitch |  Major | . | Jonathan Ellis | Brandon Williams |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1061](https://issues.apache.org/jira/browse/CASSANDRA-1061) | GCInspector uses com.sun.management - Exception under IBM JDK |  Major | . | Davanum Srinivas | Gary Dusbabek |
| [CASSANDRA-1284](https://issues.apache.org/jira/browse/CASSANDRA-1284) | Log summary of dropped messages instead of spamming log with warnings |  Minor | . | Jonathan Ellis | Brandon Williams |
| [CASSANDRA-1220](https://issues.apache.org/jira/browse/CASSANDRA-1220) | Improve Bloom filter efficiency |  Minor | . | Coda Hale | Coda Hale |
| [CASSANDRA-1358](https://issues.apache.org/jira/browse/CASSANDRA-1358) | Clogged RRS/RMS stages can hold up processing of gossip messages and request acks |  Major | . | Mike Malone | Jonathan Ellis |
| [CASSANDRA-1386](https://issues.apache.org/jira/browse/CASSANDRA-1386) | Even faster UUID comparisons |  Minor | . | Folke Behrens | Folke Behrens |
| [CASSANDRA-1393](https://issues.apache.org/jira/browse/CASSANDRA-1393) | Faster LongType comparison |  Major | . | Folke Behrens | Folke Behrens |
| [CASSANDRA-1214](https://issues.apache.org/jira/browse/CASSANDRA-1214) | Force linux to not swap the JVM |  Major | . | James Golick | Jonathan Ellis |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1042](https://issues.apache.org/jira/browse/CASSANDRA-1042) | ColumnFamilyRecordReader returns duplicate rows |  Major | . | Joost Ouwerkerk | Jonathan Ellis |
| [CASSANDRA-1145](https://issues.apache.org/jira/browse/CASSANDRA-1145) | Reading with CL \> ONE returns multiple copies of the same column per key. |  Major | . | AJ Slater | Jeremy Hanna |
| [CASSANDRA-1377](https://issues.apache.org/jira/browse/CASSANDRA-1377) | NPE aborts streaming operations for keyspaces with hyphens ('-') in their names |  Major | . | Ben Hoyt | Gary Dusbabek |
| [CASSANDRA-1235](https://issues.apache.org/jira/browse/CASSANDRA-1235) | BytesType and batch mutate causes encoded bytes of non-printable characters to be dropped |  Critical | . | Todd Nine | Folke Behrens |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1202](https://issues.apache.org/jira/browse/CASSANDRA-1202) | Debian Package cannot configure jmx port |  Trivial | Packaging | AJ Slater | Antoine Toulme |


