
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.0.0 rc1 - 2015-09-21



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10217](https://issues.apache.org/jira/browse/CASSANDRA-10217) | Support custom query expressions in SELECT |  Major | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-9664](https://issues.apache.org/jira/browse/CASSANDRA-9664) | Allow MV's select statements to be more complex |  Major | CQL, Materialized Views | Carl Yeksigian | Tyler Hobbs |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9650](https://issues.apache.org/jira/browse/CASSANDRA-9650) | CRC32Factory hack can be removed in trunk |  Minor | Local Write-Read Paths | Benedict | T Jake Luciani |
| [CASSANDRA-10126](https://issues.apache.org/jira/browse/CASSANDRA-10126) | Column subset serialization uses an unnecessary -1L for large subsets |  Trivial | . | Benedict | Benedict |
| [CASSANDRA-10131](https://issues.apache.org/jira/browse/CASSANDRA-10131) | consistently sort DCs in nodetool:status |  Minor | . | Chris Burroughs | Chris Burroughs |
| [CASSANDRA-10218](https://issues.apache.org/jira/browse/CASSANDRA-10218) | Remove unnecessary use of streams in IndexTransactions |  Major | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-9590](https://issues.apache.org/jira/browse/CASSANDRA-9590) | Support for both encrypted and unencrypted native transport connections |  Major | . | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-10232](https://issues.apache.org/jira/browse/CASSANDRA-10232) | Small optimizations in index entry serialization |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-10215](https://issues.apache.org/jira/browse/CASSANDRA-10215) | Reduce redundant secondary index selection lookups |  Major | CQL, Secondary Indexes | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-9761](https://issues.apache.org/jira/browse/CASSANDRA-9761) | Delay auth setup until peers are upgraded |  Major | . | Sam Tunnicliffe | Sylvain Lebresne |
| [CASSANDRA-8630](https://issues.apache.org/jira/browse/CASSANDRA-8630) | Faster sequential IO (on compaction, streaming, etc) |  Major | Tools | Oleg Anastasyev | Stefania |
| [CASSANDRA-10314](https://issues.apache.org/jira/browse/CASSANDRA-10314) | Update index file format |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-10312](https://issues.apache.org/jira/browse/CASSANDRA-10312) | Enable custom 2i to opt out of post-streaming rebuilds |  Major | CQL, Streaming and Messaging | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-7410](https://issues.apache.org/jira/browse/CASSANDRA-7410) | Pig support for BulkOutputFormat as a parameter in url |  Minor | . | Alex Liu | Alex Liu |
| [CASSANDRA-10316](https://issues.apache.org/jira/browse/CASSANDRA-10316) | Improve ColumnDefinition comparison performance |  Major | . | Benedict | Benedict |
| [CASSANDRA-10266](https://issues.apache.org/jira/browse/CASSANDRA-10266) | Introduce direct unit test coverage for Rows |  Major | . | Benedict | Blake Eggleston |
| [CASSANDRA-9446](https://issues.apache.org/jira/browse/CASSANDRA-9446) | Failure detector should ignore local pauses per endpoint |  Minor | . | sankalp kohli | Brandon Williams |
| [CASSANDRA-10351](https://issues.apache.org/jira/browse/CASSANDRA-10351) | We should use unsignedVInt, instead of VInt methods, for size serializations |  Trivial | . | Benedict | Benedict |
| [CASSANDRA-10230](https://issues.apache.org/jira/browse/CASSANDRA-10230) | Remove coordinator batchlog from materialized views |  Major | Coordination, Materialized Views | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-10330](https://issues.apache.org/jira/browse/CASSANDRA-10330) | Gossipinfo could return more useful information |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-10216](https://issues.apache.org/jira/browse/CASSANDRA-10216) | Remove target type from internal index metadata |  Major | Distributed Metadata | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-9632](https://issues.apache.org/jira/browse/CASSANDRA-9632) | Preserve the Names of Query Parameters in QueryOptions |  Minor | CQL | stephen mallette | Benjamin Lerer |
| [CASSANDRA-9921](https://issues.apache.org/jira/browse/CASSANDRA-9921) | Combine MV schema definition with MV table definition |  Major | Distributed Metadata, Materialized Views | Carl Yeksigian | Carl Yeksigian |
| [CASSANDRA-9961](https://issues.apache.org/jira/browse/CASSANDRA-9961) | cqlsh should have DESCRIBE MATERIALIZED VIEW |  Major | Materialized Views, Tools | Carl Yeksigian | Stefania |
| [CASSANDRA-10322](https://issues.apache.org/jira/browse/CASSANDRA-10322) | skipBytes is used extensively, but is slow |  Trivial | . | Benedict | Benedict |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9942](https://issues.apache.org/jira/browse/CASSANDRA-9942) | SStableofflinerevel and sstablelevelreset don't have windows versions |  Major | Tools | Philip Thompson | Paulo Motta |
| [CASSANDRA-10127](https://issues.apache.org/jira/browse/CASSANDRA-10127) | Make naming for secondary indexes consistent |  Major | CQL, Secondary Indexes, Tools | Sam Tunnicliffe | Benjamin Lerer |
| [CASSANDRA-10220](https://issues.apache.org/jira/browse/CASSANDRA-10220) | Memtables do not handle column changes for their PartitionColumns |  Major | . | Benedict | Sylvain Lebresne |
| [CASSANDRA-9689](https://issues.apache.org/jira/browse/CASSANDRA-9689) | keyspace does not show in describe list, if create query times out |  Major | Tools | Roopesh | Paulo Motta |
| [CASSANDRA-10240](https://issues.apache.org/jira/browse/CASSANDRA-10240) | sstableexpiredblockers can throw FileNotFound exceptions |  Major | Tools | Brandon Williams | Marcus Eriksson |
| [CASSANDRA-10209](https://issues.apache.org/jira/browse/CASSANDRA-10209) | Missing role manager in cassandra.yaml causes unexpected behaviour |  Minor | Configuration | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-10274](https://issues.apache.org/jira/browse/CASSANDRA-10274) | Assertion Errors when interrupting Cleanup |  Critical | . | Jeff Jirsa | Benedict |
| [CASSANDRA-9916](https://issues.apache.org/jira/browse/CASSANDRA-9916) | batch\_mutate failing on trunk |  Major | . | Mike Adamson | Blake Eggleston |
| [CASSANDRA-10284](https://issues.apache.org/jira/browse/CASSANDRA-10284) | Revert test.timeout back to 60 seconds in 3.0+ |  Minor | Testing | Michael Shuler | Michael Shuler |
| [CASSANDRA-9838](https://issues.apache.org/jira/browse/CASSANDRA-9838) | Unable to update an element in a static list |  Major | . | Mahesh Datt |  |
| [CASSANDRA-10282](https://issues.apache.org/jira/browse/CASSANDRA-10282) | cqlsh exception when starting with "--debug" option |  Minor | Tools | Ryan Okelberry | Adam Holmberg |
| [CASSANDRA-10270](https://issues.apache.org/jira/browse/CASSANDRA-10270) | Cassandra stops compacting |  Critical | Compaction | Adam Bliss | Marcus Eriksson |
| [CASSANDRA-10299](https://issues.apache.org/jira/browse/CASSANDRA-10299) | Issue with sstable selection when anti-compacting |  Major | . | Marcus Olsson | Marcus Olsson |
| [CASSANDRA-10237](https://issues.apache.org/jira/browse/CASSANDRA-10237) | CFS.loadNewSSTables() broken for pre-3.0 sstables |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-10066](https://issues.apache.org/jira/browse/CASSANDRA-10066) | Bring cqlsh into PEP8 compliance |  Minor | . | Philip Thompson | Philip Thompson |
| [CASSANDRA-10261](https://issues.apache.org/jira/browse/CASSANDRA-10261) | Materialized Views Timestamp issues |  Major | Coordination, Materialized Views | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-10032](https://issues.apache.org/jira/browse/CASSANDRA-10032) | Windows 3.0 utest parity |  Major | Testing | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-10272](https://issues.apache.org/jira/browse/CASSANDRA-10272) | BATCH statement is broken in cqlsh |  Major | Tools | Vovodroid | Stefania |
| [CASSANDRA-10318](https://issues.apache.org/jira/browse/CASSANDRA-10318) | Update cqlsh COPY for new internal driver serialization interface |  Major | Tools | Adam Holmberg | Stefania |
| [CASSANDRA-10067](https://issues.apache.org/jira/browse/CASSANDRA-10067) | Hadoop2 jobs throw java.lang.IncompatibleClassChangeError |  Major | . | Ashley Taylor | Brandon Williams |
| [CASSANDRA-9964](https://issues.apache.org/jira/browse/CASSANDRA-9964) | Document post-2.1 caching table options syntax |  Minor | Documentation and Website | Aleksey Yeschenko | Paulo Motta |
| [CASSANDRA-10343](https://issues.apache.org/jira/browse/CASSANDRA-10343) | Erroneous partition deletion events are delivered to indexers |  Major | CQL, Local Write-Read Paths | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-9758](https://issues.apache.org/jira/browse/CASSANDRA-9758) | nodetool compactionhistory NPE |  Minor | . | Pierre N. |  |
| [CASSANDRA-10155](https://issues.apache.org/jira/browse/CASSANDRA-10155) | 2i key cache load fails |  Major | Local Write-Read Paths | Robert Stupp | Ariel Weisberg |
| [CASSANDRA-10361](https://issues.apache.org/jira/browse/CASSANDRA-10361) | DropTableStatement do not throw an error if the table is a view |  Major | CQL, Materialized Views | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-10366](https://issues.apache.org/jira/browse/CASSANDRA-10366) | Added gossip states can shadow older unseen states |  Critical | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-9585](https://issues.apache.org/jira/browse/CASSANDRA-9585) | Make "truncate table X" an alias for "truncate X" |  Trivial | CQL | J.B. Langston | Benjamin Lerer |
| [CASSANDRA-10359](https://issues.apache.org/jira/browse/CASSANDRA-10359) | Saved caches use ambigous keyspace and CF name to identify tables |  Major | Local Write-Read Paths | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-10339](https://issues.apache.org/jira/browse/CASSANDRA-10339) | Prevent ALTER TYPE from creating circular references |  Minor | . | Olivier Michallat | Robert Stupp |
| [CASSANDRA-10124](https://issues.apache.org/jira/browse/CASSANDRA-10124) | Support for multi-column indexes |  Major | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-10362](https://issues.apache.org/jira/browse/CASSANDRA-10362) | Potential bugs in MV |  Major | Coordination, Materialized Views | Sylvain Lebresne | Carl Yeksigian |
| [CASSANDRA-10254](https://issues.apache.org/jira/browse/CASSANDRA-10254) | 3.0 paging states are incompatible with pre-3.0 nodes |  Major | CQL | Blake Eggleston | Sylvain Lebresne |
| [CASSANDRA-10238](https://issues.apache.org/jira/browse/CASSANDRA-10238) | Consolidating racks violates the RF contract |  Critical | Coordination | Brandon Williams | Stefania |
| [CASSANDRA-10198](https://issues.apache.org/jira/browse/CASSANDRA-10198) | 3.0 hints should be streamed on decomission |  Major | . | Aleksey Yeschenko | Marcus Eriksson |
| [CASSANDRA-10301](https://issues.apache.org/jira/browse/CASSANDRA-10301) | Search for items past end of descending BTreeSearchIterator can fail |  Blocker | . | Benedict | Benedict |
| [CASSANDRA-9839](https://issues.apache.org/jira/browse/CASSANDRA-9839) | Move crc\_check\_chance out of compressions options |  Minor | Configuration | Aleksey Yeschenko | Paulo Motta |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9708](https://issues.apache.org/jira/browse/CASSANDRA-9708) | Serialize ClusteringPrefixes in batches |  Major | . | Benedict | Benedict |
| [CASSANDRA-10033](https://issues.apache.org/jira/browse/CASSANDRA-10033) | Windows utest 3.0: KeyCacheTest failure |  Major | . | Joshua McKenzie | Paulo Motta |
| [CASSANDRA-10034](https://issues.apache.org/jira/browse/CASSANDRA-10034) | Windows utest 3.0: testDateCompatibility failing intermittently |  Major | . | Joshua McKenzie | Paulo Motta |
| [CASSANDRA-10223](https://issues.apache.org/jira/browse/CASSANDRA-10223) | Windows utest 3.0: CompactionsCQLTest flaky |  Minor | Compaction, Testing | Paulo Motta | Paulo Motta |
| [CASSANDRA-10109](https://issues.apache.org/jira/browse/CASSANDRA-10109) | Windows dtest 3.0: ttl\_test.py failures |  Major | Local Write-Read Paths | Joshua McKenzie | Stefania |
| [CASSANDRA-10035](https://issues.apache.org/jira/browse/CASSANDRA-10035) | Windows utest 3.0: TransactionLogsTest failure |  Major | Local Write-Read Paths, Testing | Joshua McKenzie | Stefania |
| [CASSANDRA-6717](https://issues.apache.org/jira/browse/CASSANDRA-6717) | Modernize schema tables |  Major | . | Sylvain Lebresne | Aleksey Yeschenko |
| [CASSANDRA-10286](https://issues.apache.org/jira/browse/CASSANDRA-10286) | Windows utest 3.0: org.apache.cassandra.io.sstable.SSTableLoaderTest.testLoadingIncompleteSSTable |  Major | Local Write-Read Paths | Philip Thompson | Stefania |
| [CASSANDRA-10338](https://issues.apache.org/jira/browse/CASSANDRA-10338) | Secondary index large values dtest failing on 3.0 |  Major | CQL, Secondary Indexes | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-10267](https://issues.apache.org/jira/browse/CASSANDRA-10267) | Failing tests in upgrade\_trests.paging\_test |  Major | . | Sylvain Lebresne | Blake Eggleston |
| [CASSANDRA-10354](https://issues.apache.org/jira/browse/CASSANDRA-10354) | Invalid paging with DISTINCT on static and IN |  Major | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-10373](https://issues.apache.org/jira/browse/CASSANDRA-10373) | Intermitent failures of upgrade\_tests.cql\_tests:TestCQL.compact\_metadata\_test |  Major | . | Sylvain Lebresne | Blake Eggleston |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9771](https://issues.apache.org/jira/browse/CASSANDRA-9771) | Revert CASSANDRA-9542 (allow native functions in UDA) |  Blocker | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-10146](https://issues.apache.org/jira/browse/CASSANDRA-10146) | Deprecate v1 and v2 protocol in 2.2, drop support in 3.0 |  Major | CQL | Tyler Hobbs | Benjamin Lerer |
| [CASSANDRA-10349](https://issues.apache.org/jira/browse/CASSANDRA-10349) | Rename DataOutputBuffer.getFilePointer() to position() |  Trivial | . | Robert Stupp | Robert Stupp |


