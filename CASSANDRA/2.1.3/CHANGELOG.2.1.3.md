
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1.3 - 2015-02-17



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6952](https://issues.apache.org/jira/browse/CASSANDRA-6952) | Cannot bind variables to USE statements |  Minor | CQL | Matt Stump | Benjamin Lerer |
| [CASSANDRA-8664](https://issues.apache.org/jira/browse/CASSANDRA-8664) | New cqlsh formatter for cassandra-driver type supporting nested, frozen collections |  Minor | Tools | Adam Holmberg | Adam Holmberg |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7801](https://issues.apache.org/jira/browse/CASSANDRA-7801) | A successful INSERT with CAS does not always store data in the DB after a DELETE |  Major | . | Martin Fransson | Sylvain Lebresne |
| [CASSANDRA-8183](https://issues.apache.org/jira/browse/CASSANDRA-8183) | improve PropertyFileSnitch output |  Trivial | Configuration | Liang Xie | Liang Xie |
| [CASSANDRA-8277](https://issues.apache.org/jira/browse/CASSANDRA-8277) | Eclipse project: re-use test classes from an ant build |  Trivial | Tools | Mikhail Stepura | Mikhail Stepura |
| [CASSANDRA-8221](https://issues.apache.org/jira/browse/CASSANDRA-8221) | Specify keyspace in error message when streaming fails due to missing replicas |  Trivial | . | Tyler Hobbs | Rajanarayanan Thottuvaikkatumana |
| [CASSANDRA-7859](https://issues.apache.org/jira/browse/CASSANDRA-7859) | Extend freezing to collections |  Major | . | Sylvain Lebresne | Tyler Hobbs |
| [CASSANDRA-8305](https://issues.apache.org/jira/browse/CASSANDRA-8305) | add check of the system wall clock time at startup |  Minor | . | Jason Brown | Jason Brown |
| [CASSANDRA-8055](https://issues.apache.org/jira/browse/CASSANDRA-8055) | Centralize shared executors |  Minor | Local Write-Read Paths | T Jake Luciani | Sam Tunnicliffe |
| [CASSANDRA-7386](https://issues.apache.org/jira/browse/CASSANDRA-7386) | JBOD threshold to prevent unbalanced disk utilization |  Minor | . | Chris Lohfink | Robert Stupp |
| [CASSANDRA-8329](https://issues.apache.org/jira/browse/CASSANDRA-8329) | LeveledCompactionStrategy should split large files across data directories when compacting |  Major | Compaction | J.B. Langston | Marcus Eriksson |
| [CASSANDRA-8228](https://issues.apache.org/jira/browse/CASSANDRA-8228) | Log malfunctioning host on prepareForRepair |  Trivial | . | Juho Mäkinen | Rajanarayanan Thottuvaikkatumana |
| [CASSANDRA-7897](https://issues.apache.org/jira/browse/CASSANDRA-7897) | NodeTool command to display OffHeap memory usage |  Minor | Observability, Tools | Vijay | Benjamin Lerer |
| [CASSANDRA-8415](https://issues.apache.org/jira/browse/CASSANDRA-8415) | Reduce maxHintsInProgress |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-8417](https://issues.apache.org/jira/browse/CASSANDRA-8417) | Default base\_time\_seconds in DTCS is almost always too large |  Major | . | Jonathan Ellis | Björn Hegerfors |
| [CASSANDRA-7947](https://issues.apache.org/jira/browse/CASSANDRA-7947) | Change error message when RR times out |  Minor | Observability | Brandon Williams | Sam Tunnicliffe |
| [CASSANDRA-7926](https://issues.apache.org/jira/browse/CASSANDRA-7926) | Stress can OOM on merging of timing samples |  Minor | Tools | Benedict | Benedict |
| [CASSANDRA-7964](https://issues.apache.org/jira/browse/CASSANDRA-7964) | cassandra-stress over schema should support multiple simultaneous inserts over the same seed |  Minor | Tools | Benedict | Benedict |
| [CASSANDRA-7882](https://issues.apache.org/jira/browse/CASSANDRA-7882) | Memtable slab allocation should scale logarithmically to improve occupancy rate |  Major | . | Jay Patel | Benedict |
| [CASSANDRA-7985](https://issues.apache.org/jira/browse/CASSANDRA-7985) | stress tool doesn't support auth |  Minor | Tools | Ashic Mahtab | Mike Adamson |
| [CASSANDRA-8223](https://issues.apache.org/jira/browse/CASSANDRA-8223) | Make comments/default paths in cassandra-env.ps1 more Windows-centric |  Trivial | Packaging | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-6993](https://issues.apache.org/jira/browse/CASSANDRA-6993) | Windows: remove mmap'ed I/O for index files and force standard file access |  Minor | Local Write-Read Paths | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-8507](https://issues.apache.org/jira/browse/CASSANDRA-8507) | Improved Consistency Level Feedback in cqlsh |  Minor | Tools | Adam Holmberg | Adam Holmberg |
| [CASSANDRA-8193](https://issues.apache.org/jira/browse/CASSANDRA-8193) | Multi-DC parallel snapshot repair |  Minor | . | Jimmy Mårdell | Jimmy Mårdell |
| [CASSANDRA-8192](https://issues.apache.org/jira/browse/CASSANDRA-8192) | Better error logging on corrupt compressed SSTables: currently AssertionError in Memory.java |  Minor | Local Write-Read Paths, Observability | Andreas Schnitzerling | Joshua McKenzie |
| [CASSANDRA-8194](https://issues.apache.org/jira/browse/CASSANDRA-8194) | Reading from Auth table should not be in the request path |  Minor | Distributed Metadata | Vishy Kasar | Sam Tunnicliffe |
| [CASSANDRA-8627](https://issues.apache.org/jira/browse/CASSANDRA-8627) | Support Total/Recent latency histogram metrics for range slices |  Major | Observability | Chris Lohfink | Sam Tunnicliffe |
| [CASSANDRA-7974](https://issues.apache.org/jira/browse/CASSANDRA-7974) | Enable tooling to detect hot partitions |  Major | . | Brandon Williams | Chris Lohfink |
| [CASSANDRA-8414](https://issues.apache.org/jira/browse/CASSANDRA-8414) | Avoid loops over array backed iterators that call iter.remove() |  Major | . | Richard Low | Jimmy Mårdell |
| [CASSANDRA-8662](https://issues.apache.org/jira/browse/CASSANDRA-8662) | Switch cfhistograms from using yammer metrics in 2.1 |  Major | . | Chris Lohfink | Chris Lohfink |
| [CASSANDRA-8666](https://issues.apache.org/jira/browse/CASSANDRA-8666) | Simplify logic of ABSC#BatchRemoveIterator#commit() |  Trivial | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-7705](https://issues.apache.org/jira/browse/CASSANDRA-7705) | Safer Resource Management |  Major | . | Benedict | Benedict |
| [CASSANDRA-8638](https://issues.apache.org/jira/browse/CASSANDRA-8638) | CQLSH -f option should ignore BOM in files |  Trivial | Tools | Sotirios Delimanolis | Abhishek Gupta |
| [CASSANDRA-8685](https://issues.apache.org/jira/browse/CASSANDRA-8685) | Consider upgrade to thrift 0.9.2 (or later) |  Minor | Configuration, Packaging | Adam Hattrell | T Jake Luciani |
| [CASSANDRA-8610](https://issues.apache.org/jira/browse/CASSANDRA-8610) | Allow IF EXISTS for UPDATE statements |  Minor | CQL | DOAN DuyHai | Prajakta Bhosale |
| [CASSANDRA-8742](https://issues.apache.org/jira/browse/CASSANDRA-8742) | add default\_time\_to\_live to CQL doc |  Minor | Documentation and Website | Jon Haddad | Jon Haddad |
| [CASSANDRA-8648](https://issues.apache.org/jira/browse/CASSANDRA-8648) | cassandra-stress should support whitespace inside parameters |  Trivial | Tools | Benedict | Benedict |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7704](https://issues.apache.org/jira/browse/CASSANDRA-7704) | FileNotFoundException during STREAM-OUT triggers 100% CPU usage |  Major | . | Rick Branson | Benedict |
| [CASSANDRA-8028](https://issues.apache.org/jira/browse/CASSANDRA-8028) | Unable to compute when histogram overflowed |  Major | Tools | Gianluca Borello | Carl Yeksigian |
| [CASSANDRA-8211](https://issues.apache.org/jira/browse/CASSANDRA-8211) | Overlapping sstables in L1+ |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-8294](https://issues.apache.org/jira/browse/CASSANDRA-8294) | Wrong command description for nodetool disablehandoff |  Trivial | Tools | DOAN DuyHai | Yuki Morishita |
| [CASSANDRA-8297](https://issues.apache.org/jira/browse/CASSANDRA-8297) | Milliseconds since epoch used for tracing tables |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-8265](https://issues.apache.org/jira/browse/CASSANDRA-8265) | Disable SSLv3 for POODLE |  Major | . | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-8019](https://issues.apache.org/jira/browse/CASSANDRA-8019) | Windows Unit tests and Dtests erroring due to sstable deleting task error |  Major | Local Write-Read Paths | Philip Thompson | Joshua McKenzie |
| [CASSANDRA-8291](https://issues.apache.org/jira/browse/CASSANDRA-8291) | Parent repair session is not removed in remote node |  Minor | Streaming and Messaging | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-8243](https://issues.apache.org/jira/browse/CASSANDRA-8243) | DTCS can leave time-overlaps, limiting ability to expire entire SSTables |  Minor | . | Björn Hegerfors | Björn Hegerfors |
| [CASSANDRA-8286](https://issues.apache.org/jira/browse/CASSANDRA-8286) | Regression in ORDER BY |  Major | . | Philip Thompson | Tyler Hobbs |
| [CASSANDRA-8264](https://issues.apache.org/jira/browse/CASSANDRA-8264) | Problems with multicolumn relations and COMPACT STORAGE |  Major | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-8302](https://issues.apache.org/jira/browse/CASSANDRA-8302) | Filtering for CONTAINS (KEY) on frozen collection clustering columns within a partition does not work |  Minor | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-8231](https://issues.apache.org/jira/browse/CASSANDRA-8231) | Wrong size of cached prepared statements |  Major | CQL | Jaroslav Kamenik | Benjamin Lerer |
| [CASSANDRA-8280](https://issues.apache.org/jira/browse/CASSANDRA-8280) | Cassandra crashing on inserting data over 64K into indexed strings |  Critical | CQL | Cristian Marinescu | Sam Tunnicliffe |
| [CASSANDRA-7538](https://issues.apache.org/jira/browse/CASSANDRA-7538) | Truncate of a CF should also delete Paxos CF |  Minor | Coordination | sankalp kohli | Sam Tunnicliffe |
| [CASSANDRA-8181](https://issues.apache.org/jira/browse/CASSANDRA-8181) | Intermittent failure of SSTableImportTest unit test |  Major | Testing | Michael Shuler | Benjamin Lerer |
| [CASSANDRA-8320](https://issues.apache.org/jira/browse/CASSANDRA-8320) | 2.1.2: NullPointerException in SSTableWriter |  Major | Local Write-Read Paths | Evgeny Pasynkov | Marcus Eriksson |
| [CASSANDRA-8395](https://issues.apache.org/jira/browse/CASSANDRA-8395) | typo in sstablerepairedset |  Trivial | . | David Sauer | David Sauer |
| [CASSANDRA-8122](https://issues.apache.org/jira/browse/CASSANDRA-8122) | Undeclare throwable exception while executing 'nodetool netstats localhost' |  Minor | Tools | Vishal Mehta | Carl Yeksigian |
| [CASSANDRA-8386](https://issues.apache.org/jira/browse/CASSANDRA-8386) | Make sure we release references to sstables after incremental repair |  Major | Streaming and Messaging | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-8018](https://issues.apache.org/jira/browse/CASSANDRA-8018) | Cassandra seems to insert twice in custom PerColumnSecondaryIndex |  Major | . | Pavel Chlupacek | Benjamin Lerer |
| [CASSANDRA-8401](https://issues.apache.org/jira/browse/CASSANDRA-8401) | dropping a CF doesn't remove the latency-sampling task |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-8423](https://issues.apache.org/jira/browse/CASSANDRA-8423) | Error during start up on windows |  Minor | Packaging | Philip Thompson | Joshua McKenzie |
| [CASSANDRA-8400](https://issues.apache.org/jira/browse/CASSANDRA-8400) | nodetool cfstats is missing "Number of Keys (estimate)" |  Minor | Tools | Sebastian Estevez | Lyuben Todorov |
| [CASSANDRA-8321](https://issues.apache.org/jira/browse/CASSANDRA-8321) | SStablesplit behavior changed |  Minor | Tools | Philip Thompson | Marcus Eriksson |
| [CASSANDRA-8459](https://issues.apache.org/jira/browse/CASSANDRA-8459) | "autocompaction" on reads can prevent memtable space reclaimation |  Major | . | Benedict | Benedict |
| [CASSANDRA-8383](https://issues.apache.org/jira/browse/CASSANDRA-8383) | Memtable flush may expire records from the commit log that are in a later memtable |  Critical | . | Benedict | Benedict |
| [CASSANDRA-8332](https://issues.apache.org/jira/browse/CASSANDRA-8332) | Null pointer after droping keyspace |  Minor | Distributed Metadata | Chris Lohfink | T Jake Luciani |
| [CASSANDRA-8451](https://issues.apache.org/jira/browse/CASSANDRA-8451) | NPE when writetime() or ttl() are nested inside function call |  Minor | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-8410](https://issues.apache.org/jira/browse/CASSANDRA-8410) | Select with many IN values on clustering columns can result in a StackOverflowError |  Major | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-8455](https://issues.apache.org/jira/browse/CASSANDRA-8455) | IndexOutOfBoundsException when building SyntaxError message snippet |  Minor | CQL | Tyler Hobbs | Benjamin Lerer |
| [CASSANDRA-8452](https://issues.apache.org/jira/browse/CASSANDRA-8452) | Add missing systems to FBUtilities.isUnix, add FBUtilities.isWindows |  Minor | . | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-8393](https://issues.apache.org/jira/browse/CASSANDRA-8393) | support quoted identifiers for index names |  Major | . | Jonathan Halliday | Benjamin Lerer |
| [CASSANDRA-8253](https://issues.apache.org/jira/browse/CASSANDRA-8253) | cassandra-stress 2.1 doesn't support LOCAL\_ONE |  Minor | . | J.B. Langston | Liang Xie |
| [CASSANDRA-8458](https://issues.apache.org/jira/browse/CASSANDRA-8458) | Don't give out positions in an sstable beyond its first/last tokens |  Major | Local Write-Read Paths | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-8432](https://issues.apache.org/jira/browse/CASSANDRA-8432) | Standalone Scrubber broken for LCS |  Minor | Compaction, Tools | Carl Yeksigian | Marcus Eriksson |
| [CASSANDRA-8370](https://issues.apache.org/jira/browse/CASSANDRA-8370) | cqlsh doesn't handle LIST statements correctly |  Minor | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-8463](https://issues.apache.org/jira/browse/CASSANDRA-8463) | Constant compaction under LCS |  Major | Compaction | Rick Branson | Marcus Eriksson |
| [CASSANDRA-8288](https://issues.apache.org/jira/browse/CASSANDRA-8288) | cqlsh describe needs to show 'sstable\_compression': '' |  Major | Tools | Jeremiah Jordan | Tyler Hobbs |
| [CASSANDRA-7623](https://issues.apache.org/jira/browse/CASSANDRA-7623) | Altering keyspace truncates DESCRIBE output until you reconnect. |  Minor | . | Ryan McGuire | Tyler Hobbs |
| [CASSANDRA-8373](https://issues.apache.org/jira/browse/CASSANDRA-8373) | MOVED\_NODE Topology Change event is never emitted |  Minor | . | Adam Holmberg | Adam Holmberg |
| [CASSANDRA-8510](https://issues.apache.org/jira/browse/CASSANDRA-8510) | CompactionManager.submitMaximal may leak resources |  Major | Compaction | Benedict | Marcus Eriksson |
| [CASSANDRA-8429](https://issues.apache.org/jira/browse/CASSANDRA-8429) | Some keys unreadable during compaction |  Major | . | Ariel Weisberg | Benedict |
| [CASSANDRA-8408](https://issues.apache.org/jira/browse/CASSANDRA-8408) | limit appears to replace page size under certain conditions |  Minor | . | Russ Hatch | Tyler Hobbs |
| [CASSANDRA-8285](https://issues.apache.org/jira/browse/CASSANDRA-8285) | Move all hints related tasks to hints private executor |  Major | . | Pierre Laporte | Aleksey Yeschenko |
| [CASSANDRA-8427](https://issues.apache.org/jira/browse/CASSANDRA-8427) | An error is written to System.out when UserTypesTest is run |  Minor | Testing | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-8524](https://issues.apache.org/jira/browse/CASSANDRA-8524) | Cassandra stress user defined writes should populate sequentially |  Trivial | Tools | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-8525](https://issues.apache.org/jira/browse/CASSANDRA-8525) | Bloom Filter truePositive counter not updated on key cache hit |  Major | Local Write-Read Paths | Tyler Hobbs | Marcus Eriksson |
| [CASSANDRA-8532](https://issues.apache.org/jira/browse/CASSANDRA-8532) | Fix calculation of expected write size during compaction |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-7910](https://issues.apache.org/jira/browse/CASSANDRA-7910) | wildcard prepared statements are incorrect after a column is added to the table |  Minor | . | Oded Peer | Tyler Hobbs |
| [CASSANDRA-8512](https://issues.apache.org/jira/browse/CASSANDRA-8512) | cqlsh unusable after encountering schema mismatch |  Minor | Tools | Jeff Brewster | Tyler Hobbs |
| [CASSANDRA-8474](https://issues.apache.org/jira/browse/CASSANDRA-8474) | Error in commit log allocator thread |  Minor | . | Philip Thompson | Benedict |
| [CASSANDRA-8315](https://issues.apache.org/jira/browse/CASSANDRA-8315) | cassandra-env.sh doesn't handle correctly non numeric JDK versions |  Trivial | . | Michaël Figuière | Michael Shuler |
| [CASSANDRA-8245](https://issues.apache.org/jira/browse/CASSANDRA-8245) | Cassandra nodes periodically die in 2-DC configuration |  Minor | . | Oleg Poleshuk | Brandon Williams |
| [CASSANDRA-8570](https://issues.apache.org/jira/browse/CASSANDRA-8570) | org.apache.cassandra.db.compaction.CompactionsPurgeTest failing |  Major | Compaction, Testing | Philip Thompson | Marcus Eriksson |
| [CASSANDRA-8499](https://issues.apache.org/jira/browse/CASSANDRA-8499) | Ensure SSTableWriter cleans up properly after failure |  Major | . | Benedict | Benedict |
| [CASSANDRA-8462](https://issues.apache.org/jira/browse/CASSANDRA-8462) | Upgrading a 2.0 to 2.1 breaks CFMetaData on 2.0 nodes |  Major | . | Rick Branson | Aleksey Yeschenko |
| [CASSANDRA-8399](https://issues.apache.org/jira/browse/CASSANDRA-8399) | Reference Counter exception when dropping user type |  Major | Local Write-Read Paths | Philip Thompson | Joshua McKenzie |
| [CASSANDRA-6983](https://issues.apache.org/jira/browse/CASSANDRA-6983) | DirectoriesTest fails when run as root |  Minor | Testing | Brandon Williams | Alan Boudreault |
| [CASSANDRA-8365](https://issues.apache.org/jira/browse/CASSANDRA-8365) | CamelCase name is used as index name instead of lowercase |  Minor | CQL | Pierre Laporte | Benjamin Lerer |
| [CASSANDRA-8537](https://issues.apache.org/jira/browse/CASSANDRA-8537) | ConcurrentModificationException while executing 'nodetool cleanup' |  Major | Tools | Noureddine Chatti | Marcus Eriksson |
| [CASSANDRA-8490](https://issues.apache.org/jira/browse/CASSANDRA-8490) | DISTINCT queries with LIMITs or paging are incorrect when partitions are deleted |  Major | . | Frank Limstrand | Tyler Hobbs |
| [CASSANDRA-8588](https://issues.apache.org/jira/browse/CASSANDRA-8588) | Fix DropTypeStatements isusedBy for maps (typo ignored values) |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-8562](https://issues.apache.org/jira/browse/CASSANDRA-8562) | Fix checking available disk space before compaction starts |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-8579](https://issues.apache.org/jira/browse/CASSANDRA-8579) | sstablemetadata can't load org.apache.cassandra.tools.SSTableMetadataViewer |  Minor | Tools | Jimmy Mårdell | Jimmy Mårdell |
| [CASSANDRA-8577](https://issues.apache.org/jira/browse/CASSANDRA-8577) | Values of set types not loading correctly into Pig |  Major | . | Oksana Danylyshyn | Artem Aliev |
| [CASSANDRA-8599](https://issues.apache.org/jira/browse/CASSANDRA-8599) | Refactor or fix CqlStorage |  Major | . | Brandon Williams | Alex Liu |
| [CASSANDRA-7269](https://issues.apache.org/jira/browse/CASSANDRA-7269) | Make CqlInputFormat and CqlRecordReader consistent with comments |  Minor | . | Jeremy Hanna | Rekha Joshi |
| [CASSANDRA-8558](https://issues.apache.org/jira/browse/CASSANDRA-8558) | deleted row still can be selected out |  Blocker | . | zhaoyan | Sylvain Lebresne |
| [CASSANDRA-8355](https://issues.apache.org/jira/browse/CASSANDRA-8355) | NPE when passing wrong argument in ALTER TABLE statement |  Minor | CQL | Pierre Laporte | Benjamin Lerer |
| [CASSANDRA-8550](https://issues.apache.org/jira/browse/CASSANDRA-8550) | Internal pagination in CQL3 index queries creating substantial overhead |  Major | . | Samuel Klock | Tyler Hobbs |
| [CASSANDRA-8292](https://issues.apache.org/jira/browse/CASSANDRA-8292) | From Pig: org.apache.cassandra.exceptions.ConfigurationException: Expecting URI in variable: [cassandra.config].  Please prefix the file with file:/// for local files or file://\<server\>/ for remote files. |  Major | . | Brandon Kearby | Joshua McKenzie |
| [CASSANDRA-8618](https://issues.apache.org/jira/browse/CASSANDRA-8618) | Password stored in cqlshrc file does not work with % character |  Trivial | . | Johnny Miller | Johnny Miller |
| [CASSANDRA-8281](https://issues.apache.org/jira/browse/CASSANDRA-8281) | CQLSSTableWriter close does not work |  Major | CQL | Xu Zhongxing | Benjamin Lerer |
| [CASSANDRA-8640](https://issues.apache.org/jira/browse/CASSANDRA-8640) | Paxos requires all nodes for CAS |  Major | . | Anthony Cozzie | Anthony Cozzie |
| [CASSANDRA-8641](https://issues.apache.org/jira/browse/CASSANDRA-8641) | Repair causes a large number of tiny SSTables |  Major | . | Flavien Charlon |  |
| [CASSANDRA-8421](https://issues.apache.org/jira/browse/CASSANDRA-8421) | Cassandra 2.1.1 & Cassandra 2.1.2 UDT not returning value for LIST type as UDT |  Major | CQL | madheswaran | Benjamin Lerer |
| [CASSANDRA-8647](https://issues.apache.org/jira/browse/CASSANDRA-8647) | Unify ARE#makeDataRequests() and ARE#makeDigestRequests() |  Trivial | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-8652](https://issues.apache.org/jira/browse/CASSANDRA-8652) | DROP TABLE should also drop BATCH prepared statements associated to it |  Major | . | Edward Ribeiro | Edward Ribeiro |
| [CASSANDRA-8316](https://issues.apache.org/jira/browse/CASSANDRA-8316) |  "Did not get positive replies from all endpoints" error on incremental repair |  Major | Streaming and Messaging | Loic Lambiel | Marcus Eriksson |
| [CASSANDRA-8608](https://issues.apache.org/jira/browse/CASSANDRA-8608) | Fix cassandra-stress bug introduced by 7964, and cleanup related code a little |  Major | . | Benedict | Benedict |
| [CASSANDRA-8372](https://issues.apache.org/jira/browse/CASSANDRA-8372) | cqlsh - throws TypeError on SELECT of a map with blob keys |  Minor | Tools | Nathan Cormier | Tyler Hobbs |
| [CASSANDRA-8563](https://issues.apache.org/jira/browse/CASSANDRA-8563) | cqlsh broken for some thrift created tables. |  Major | Tools | Jeremiah Jordan | Tyler Hobbs |
| [CASSANDRA-8580](https://issues.apache.org/jira/browse/CASSANDRA-8580) | AssertionErrors after activating unchecked\_tombstone\_compaction with leveled compaction |  Major | Compaction | Björn Hachmann | Marcus Eriksson |
| [CASSANDRA-8514](https://issues.apache.org/jira/browse/CASSANDRA-8514) | ArrayIndexOutOfBoundsException in nodetool cfhistograms |  Major | Tools | Philip Thompson | Benjamin Lerer |
| [CASSANDRA-8677](https://issues.apache.org/jira/browse/CASSANDRA-8677) | rpc\_interface and listen\_interface generate NPE on startup when specified interface doesn't exist |  Major | Configuration | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-8635](https://issues.apache.org/jira/browse/CASSANDRA-8635) | STCS cold sstable omission does not handle overwrites without reads |  Critical | Compaction | Tyler Hobbs | Marcus Eriksson |
| [CASSANDRA-8695](https://issues.apache.org/jira/browse/CASSANDRA-8695) | thrift column definition list sometimes immutable |  Major | . | Chris Lockfort | Chris Lockfort |
| [CASSANDRA-8688](https://issues.apache.org/jira/browse/CASSANDRA-8688) | Standalone sstableupgrade tool throws exception |  Minor | Tools | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-8668](https://issues.apache.org/jira/browse/CASSANDRA-8668) | We don't enforce offheap memory constraints; regression introduced by 7882 |  Major | . | Benedict | Benedict |
| [CASSANDRA-8676](https://issues.apache.org/jira/browse/CASSANDRA-8676) | commitlog\_periodic\_queue\_size should not exist in 2.1+ |  Trivial | . | Benedict | Benedict |
| [CASSANDRA-8619](https://issues.apache.org/jira/browse/CASSANDRA-8619) | using CQLSSTableWriter gives ConcurrentModificationException |  Major | Tools | Igor Berman | Benedict |
| [CASSANDRA-8623](https://issues.apache.org/jira/browse/CASSANDRA-8623) | sstablesplit fails \*randomly\* with Data component is missing |  Major | Tools | Alan Boudreault | Marcus Eriksson |
| [CASSANDRA-8693](https://issues.apache.org/jira/browse/CASSANDRA-8693) | QueryProcessor never removes internal statements from its cache |  Major | . | Edward Ribeiro | Edward Ribeiro |
| [CASSANDRA-8704](https://issues.apache.org/jira/browse/CASSANDRA-8704) | IllegalStateException when running SSTableLoader |  Major | Streaming and Messaging | Philip Thompson | Marcus Eriksson |
| [CASSANDRA-8687](https://issues.apache.org/jira/browse/CASSANDRA-8687) | Keyspace should also check Config.isClientMode |  Minor | Tools | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-8694](https://issues.apache.org/jira/browse/CASSANDRA-8694) | Repair of empty keyspace hangs rather than ignoring the request |  Minor | . | Ryan McGuire | Jeff Jirsa |
| [CASSANDRA-8733](https://issues.apache.org/jira/browse/CASSANDRA-8733) | List prepend reverses item order |  Major | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-8448](https://issues.apache.org/jira/browse/CASSANDRA-8448) | "Comparison method violates its general contract" in AbstractEndpointSnitch |  Major | . | J.B. Langston | Brandon Williams |
| [CASSANDRA-8719](https://issues.apache.org/jira/browse/CASSANDRA-8719) | Using thrift HSHA with offheap\_objects appears to corrupt data |  Major | . | Randy Fradin | Benedict |
| [CASSANDRA-8726](https://issues.apache.org/jira/browse/CASSANDRA-8726) | throw OOM in Memory if we fail to allocate |  Minor | . | Benedict | Benedict |
| [CASSANDRA-8267](https://issues.apache.org/jira/browse/CASSANDRA-8267) | Only stream from unrepaired sstables during incremental repair |  Major | Streaming and Messaging | Marcus Eriksson | Marcus Eriksson |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-8678](https://issues.apache.org/jira/browse/CASSANDRA-8678) | CREATE TABLE accepts value for default\_time\_to\_live on counter table |  Minor | Tools | Aaron Ploetz | Jeff Jirsa |
| [CASSANDRA-7933](https://issues.apache.org/jira/browse/CASSANDRA-7933) | Update cassandra-stress README |  Minor | . | Benedict | Philip Thompson |


