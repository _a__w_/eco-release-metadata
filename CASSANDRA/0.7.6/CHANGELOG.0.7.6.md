
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.7.6 - 2011-05-18



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2404](https://issues.apache.org/jira/browse/CASSANDRA-2404) | if out of disk space reclaim compacted SSTables during memtable flush |  Minor | . | amorton | Jonathan Ellis |
| [CASSANDRA-2602](https://issues.apache.org/jira/browse/CASSANDRA-2602) | Add check in SSTable.componentsFor() for temp files |  Minor | . | amorton | amorton |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2554](https://issues.apache.org/jira/browse/CASSANDRA-2554) | Move gossip heartbeats [back] to its own thread |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2552](https://issues.apache.org/jira/browse/CASSANDRA-2552) | ReadResponseResolver Race |  Major | . | Stu Hood | Jonathan Ellis |
| [CASSANDRA-2556](https://issues.apache.org/jira/browse/CASSANDRA-2556) | DatacenterReadResolver not triggering repair |  Minor | . | Stu Hood | Jonathan Ellis |
| [CASSANDRA-2578](https://issues.apache.org/jira/browse/CASSANDRA-2578) | stress performance is artificially limited |  Major | Tools | Brandon Williams | Pavel Yaskevich |
| [CASSANDRA-2536](https://issues.apache.org/jira/browse/CASSANDRA-2536) | Schema disagreements when using connections to multiple hosts |  Minor | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-2571](https://issues.apache.org/jira/browse/CASSANDRA-2571) | Check for null super column for SC CF in ThriftValidation (and always validate the sc key) |  Major | . | Mike Bulman | Sylvain Lebresne |
| [CASSANDRA-2562](https://issues.apache.org/jira/browse/CASSANDRA-2562) | Parent POM does not get deployed to the maven repository |  Major | . | Stephen Connolly | Stephen Connolly |
| [CASSANDRA-2487](https://issues.apache.org/jira/browse/CASSANDRA-2487) | Pig example script no longer working |  Minor | . | Jeremy Hanna | Jeremy Hanna |
| [CASSANDRA-2581](https://issues.apache.org/jira/browse/CASSANDRA-2581) | Rebuffer called excessively during seeks |  Minor | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-2595](https://issues.apache.org/jira/browse/CASSANDRA-2595) | Tame excessive logging during repairs |  Trivial | . | Daniel Doubleday | Daniel Doubleday |
| [CASSANDRA-2596](https://issues.apache.org/jira/browse/CASSANDRA-2596) | include indexes in snapshots |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2618](https://issues.apache.org/jira/browse/CASSANDRA-2618) | DynamicSnitch race in adding latencies |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-2619](https://issues.apache.org/jira/browse/CASSANDRA-2619) | secondary index not dropped until restart |  Major | Secondary Indexes | Jackson Chung | Jonathan Ellis |
| [CASSANDRA-2627](https://issues.apache.org/jira/browse/CASSANDRA-2627) | Don't allow {LOCAL\|EACH}\_QUORUM unless strategy is NTS |  Trivial | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2625](https://issues.apache.org/jira/browse/CASSANDRA-2625) | auto bootstrapping a node into a cluster without a schema silently fails |  Minor | . | amorton | Sylvain Lebresne |
| [CASSANDRA-2632](https://issues.apache.org/jira/browse/CASSANDRA-2632) | ConfigurationException when starting a node after deleting LocationInfo SStables |  Minor | . | amorton | amorton |
| [CASSANDRA-2631](https://issues.apache.org/jira/browse/CASSANDRA-2631) | Replaying a commitlog entry from a dropped keyspace will cause an error |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2637](https://issues.apache.org/jira/browse/CASSANDRA-2637) | bloom filter true positives not counted unless key cache is enabled |  Trivial | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2638](https://issues.apache.org/jira/browse/CASSANDRA-2638) | Migrations announce on startup attempts to set local gossip state before gossiper is started. |  Major | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-2633](https://issues.apache.org/jira/browse/CASSANDRA-2633) | Keys get lost in bootstrap |  Critical | . | Richard Low | Richard Low |
| [CASSANDRA-2628](https://issues.apache.org/jira/browse/CASSANDRA-2628) | Empty Result with Secondary Index Queries with "limit 1" |  Major | Secondary Indexes | Muga Nishizawa | Sylvain Lebresne |
| [CASSANDRA-2623](https://issues.apache.org/jira/browse/CASSANDRA-2623) | CLI escaped single quote parsing gives errors |  Minor | Tools | rday | Pavel Yaskevich |
| [CASSANDRA-2401](https://issues.apache.org/jira/browse/CASSANDRA-2401) | getColumnFamily() return null, which is not checked in ColumnFamilyStore.java scan() method, causing Timeout Exception in query |  Major | . | Tey Kar Shiang | Jonathan Ellis |
| [CASSANDRA-2481](https://issues.apache.org/jira/browse/CASSANDRA-2481) | C\* .deb installs C\* init.d scripts such that C\* comes up before mdadm and related |  Minor | Packaging | Matthew F. Dennis | paul cannon |


