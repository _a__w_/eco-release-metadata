
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.3 - 2016-02-09



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6696](https://issues.apache.org/jira/browse/CASSANDRA-6696) | Partition sstables by token range |  Major | Compaction | sankalp kohli | Marcus Eriksson |
| [CASSANDRA-10140](https://issues.apache.org/jira/browse/CASSANDRA-10140) | Enable GC logging by default |  Minor | Configuration | Chris Lohfink | Chris Lohfink |
| [CASSANDRA-10847](https://issues.apache.org/jira/browse/CASSANDRA-10847) | Log a message when refusing a major compaction due to incremental repairedAt status |  Trivial | Compaction | Brandon Williams | Marcus Eriksson |
| [CASSANDRA-10924](https://issues.apache.org/jira/browse/CASSANDRA-10924) | Pass base table's metadata to Index.validateOptions |  Minor | CQL, Local Write-Read Paths | Andrés de la Peña | Andrés de la Peña |
| [CASSANDRA-7925](https://issues.apache.org/jira/browse/CASSANDRA-7925) | TimeUUID LSB should be unique per process, not just per machine |  Major | . | Peter Mädel | Sylvain Lebresne |
| [CASSANDRA-10025](https://issues.apache.org/jira/browse/CASSANDRA-10025) | Allow compaction throttle to be real time |  Minor | . | sankalp kohli | Soumava Ghosh |
| [CASSANDRA-11054](https://issues.apache.org/jira/browse/CASSANDRA-11054) | Added support for IBM zSystems architecture (s390x) |  Minor | Observability, Testing | Meerabo Shah |  |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10817](https://issues.apache.org/jira/browse/CASSANDRA-10817) | DROP USER is not case-sensitive |  Minor | CQL | Mike Adamson | Marcus Eriksson |
| [CASSANDRA-10902](https://issues.apache.org/jira/browse/CASSANDRA-10902) | Skip saved cache directory when checking SSTables at startup |  Major | Configuration | Carl Yeksigian | Carl Yeksigian |
| [CASSANDRA-9258](https://issues.apache.org/jira/browse/CASSANDRA-9258) | Range movement causes CPU & performance impact |  Major | . | Rick Branson | Dikang Gu |
| [CASSANDRA-8708](https://issues.apache.org/jira/browse/CASSANDRA-8708) | inter\_dc\_stream\_throughput\_outbound\_megabits\_per\_sec to defaults to unlimited |  Major | Streaming and Messaging | Adam Hattrell | Jeremy Hanna |
| [CASSANDRA-10975](https://issues.apache.org/jira/browse/CASSANDRA-10975) | Histogram buckets exposed in jmx are sorted by count |  Major | Observability | Chris Lohfink | Chris Lohfink |
| [CASSANDRA-10977](https://issues.apache.org/jira/browse/CASSANDRA-10977) | MV view\_tombstone\_test is failing on trunk |  Major | Materialized Views, Tools | Alan Boudreault |  |
| [CASSANDRA-10887](https://issues.apache.org/jira/browse/CASSANDRA-10887) | Pending range calculator gives wrong pending ranges for moves |  Critical | Coordination | Richard Low | sankalp kohli |
| [CASSANDRA-8072](https://issues.apache.org/jira/browse/CASSANDRA-8072) | Exception during startup: Unable to gossip with any seeds |  Major | Lifecycle | Ryan Springer | Stefania |
| [CASSANDRA-10743](https://issues.apache.org/jira/browse/CASSANDRA-10743) | Failed upgradesstables (upgrade from 2.2.2 to 3.0.0) |  Major | Local Write-Read Paths | Gábor Auth | Sylvain Lebresne |
| [CASSANDRA-10954](https://issues.apache.org/jira/browse/CASSANDRA-10954) | [Regression] Error when removing list element with UPDATE statement |  Major | Local Write-Read Paths | DOAN DuyHai | Sylvain Lebresne |
| [CASSANDRA-10676](https://issues.apache.org/jira/browse/CASSANDRA-10676) | AssertionError in CompactionExecutor |  Major | Compaction | mlowicki | Carl Yeksigian |
| [CASSANDRA-10959](https://issues.apache.org/jira/browse/CASSANDRA-10959) | missing timeout option propagation in cqlsh (cqlsh.py) |  Minor | Tools | Julien Blondeau | Julien Blondeau |
| [CASSANDRA-10477](https://issues.apache.org/jira/browse/CASSANDRA-10477) | java.lang.AssertionError in StorageProxy.submitHint |  Major | Local Write-Read Paths | Severin Leonhardt | Ariel Weisberg |
| [CASSANDRA-10686](https://issues.apache.org/jira/browse/CASSANDRA-10686) | cqlsh schema refresh on timeout dtest is flaky |  Minor | Testing, Tools | Joel Knighton | Paulo Motta |
| [CASSANDRA-10961](https://issues.apache.org/jira/browse/CASSANDRA-10961) | Not enough bytes error when add nodes to cluster |  Major | Streaming and Messaging | xiaost | Paulo Motta |
| [CASSANDRA-9465](https://issues.apache.org/jira/browse/CASSANDRA-9465) | No client warning on tombstone threshold |  Minor | Observability | Adam Holmberg | Carl Yeksigian |
| [CASSANDRA-11013](https://issues.apache.org/jira/browse/CASSANDRA-11013) | Stale entries not purged from CompositesIndex |  Major | Local Write-Read Paths | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-11010](https://issues.apache.org/jira/browse/CASSANDRA-11010) | Bootstrap hangs on 3.2 |  Blocker | Streaming and Messaging | T Jake Luciani | Paulo Motta |
| [CASSANDRA-10980](https://issues.apache.org/jira/browse/CASSANDRA-10980) | nodetool scrub NPEs when keyspace isn't specified |  Trivial | Compaction, Tools | Will Hayworth | Yuki Morishita |
| [CASSANDRA-10997](https://issues.apache.org/jira/browse/CASSANDRA-10997) | cqlsh\_copy\_tests failing en mass when vnodes are disabled |  Major | Tools | Philip Thompson | Stefania |
| [CASSANDRA-11005](https://issues.apache.org/jira/browse/CASSANDRA-11005) | Split consistent range movement flag |  Trivial | Configuration | sankalp kohli | sankalp kohli |
| [CASSANDRA-10947](https://issues.apache.org/jira/browse/CASSANDRA-10947) | Remained files in the hints folder |  Major | Coordination, Local Write-Read Paths | Gábor Auth | Aleksey Yeschenko |
| [CASSANDRA-10829](https://issues.apache.org/jira/browse/CASSANDRA-10829) | cleanup + repair generates a lot of logs |  Major | Streaming and Messaging | Fabien Rousseau | Marcus Eriksson |
| [CASSANDRA-11021](https://issues.apache.org/jira/browse/CASSANDRA-11021) | Inserting static column fails with secondary index on clustering key |  Major | Local Write-Read Paths, Secondary Indexes | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-10909](https://issues.apache.org/jira/browse/CASSANDRA-10909) | NPE in ActiveRepairService |  Major | Streaming and Messaging | Eduard Tudenhoefner | Marcus Eriksson |
| [CASSANDRA-10979](https://issues.apache.org/jira/browse/CASSANDRA-10979) | LCS doesn't do L0 STC on new tables while an L0-\>L1 compaction is in progress |  Major | Compaction | Jeff Ferland | Carl Yeksigian |
| [CASSANDRA-10969](https://issues.apache.org/jira/browse/CASSANDRA-10969) | long-running cluster sees bad gossip generation when a node restarts |  Major | Coordination | T. David Hudson | Joel Knighton |
| [CASSANDRA-10955](https://issues.apache.org/jira/browse/CASSANDRA-10955) | Multi-partitions queries with ORDER BY can result in a NPE |  Major | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-10948](https://issues.apache.org/jira/browse/CASSANDRA-10948) | CQLSH error when trying to insert non-ascii statement |  Minor | Tools | Matthieu Nantern | Matthieu Nantern |
| [CASSANDRA-11007](https://issues.apache.org/jira/browse/CASSANDRA-11007) | Exception when running nodetool info during bootstrap |  Minor | Tools | T Jake Luciani | Yuki Morishita |
| [CASSANDRA-9949](https://issues.apache.org/jira/browse/CASSANDRA-9949) | maxPurgeableTimestamp needs to check memtables too |  Major | Local Write-Read Paths | Jonathan Ellis | Stefania |
| [CASSANDRA-10688](https://issues.apache.org/jira/browse/CASSANDRA-10688) | Stack overflow from SSTableReader$InstanceTidier.runOnClose in Leak Detector |  Major | Local Write-Read Paths, Testing | Jeremiah Jordan | Ariel Weisberg |
| [CASSANDRA-11045](https://issues.apache.org/jira/browse/CASSANDRA-11045) | Legacy SSTables for KEYS indexes can't be read in 3.0+ |  Critical | Distributed Metadata, Local Write-Read Paths | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-11046](https://issues.apache.org/jira/browse/CASSANDRA-11046) | Existing indexes are always rebuilt on upgrade to 3.0 |  Critical | Distributed Metadata | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-11003](https://issues.apache.org/jira/browse/CASSANDRA-11003) | cqlsh: LWT operations not handled correctly in 3.0+ |  Major | Tools | Eduard Tudenhoefner | Eduard Tudenhoefner |
| [CASSANDRA-11034](https://issues.apache.org/jira/browse/CASSANDRA-11034) | consistent\_reads\_after\_move\_test is failing on trunk |  Blocker | Testing | Philip Thompson | Marcus Eriksson |
| [CASSANDRA-11071](https://issues.apache.org/jira/browse/CASSANDRA-11071) | Invalidate legacy schema CFSs at startup |  Minor | Distributed Metadata, Lifecycle | Sam Tunnicliffe | Mike Adamson |
| [CASSANDRA-11026](https://issues.apache.org/jira/browse/CASSANDRA-11026) | OOM due to HeapByteBuffer instances |  Major | . | Maxim Podkolzine | Sylvain Lebresne |
| [CASSANDRA-11102](https://issues.apache.org/jira/browse/CASSANDRA-11102) | Data lost during compaction |  Blocker | Compaction | Jaroslav Kamenik | Sylvain Lebresne |
| [CASSANDRA-11087](https://issues.apache.org/jira/browse/CASSANDRA-11087) | Queries on compact storage tables in mixed version clusters can return incorrect results |  Major | Coordination | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-11104](https://issues.apache.org/jira/browse/CASSANDRA-11104) | KeysSearcher doesn't filter results by key range |  Major | Local Write-Read Paths | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-11027](https://issues.apache.org/jira/browse/CASSANDRA-11027) | Duplicate column familiy initialization |  Major | Distributed Metadata | Sergio Bossa | Sam Tunnicliffe |
| [CASSANDRA-10779](https://issues.apache.org/jira/browse/CASSANDRA-10779) | Mutations do not block for completion under view lock contention |  Major | Local Write-Read Paths, Materialized Views | Will Zhang | Carl Yeksigian |
| [CASSANDRA-10910](https://issues.apache.org/jira/browse/CASSANDRA-10910) | Materialized view remained rows |  Major | Coordination, Materialized Views | Gábor Auth | Carl Yeksigian |
| [CASSANDRA-10944](https://issues.apache.org/jira/browse/CASSANDRA-10944) | ERROR [CompactionExecutor] CassandraDaemon.java  Exception in thread |  Major | Compaction, Local Write-Read Paths | Alexey Ivanchin | Sylvain Lebresne |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10932](https://issues.apache.org/jira/browse/CASSANDRA-10932) | pushed\_notifications\_test.py schema\_changes\_test is failing |  Major | . | Philip Thompson | Sylvain Lebresne |
| [CASSANDRA-10938](https://issues.apache.org/jira/browse/CASSANDRA-10938) | test\_bulk\_round\_trip\_blogposts is failing occasionally |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-10762](https://issues.apache.org/jira/browse/CASSANDRA-10762) | select\_distinct\_with\_deletions\_test failing in mixed version cluster |  Major | CQL | Philip Thompson | Sylvain Lebresne |


