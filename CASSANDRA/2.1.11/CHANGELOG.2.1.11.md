
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1.11 - 2015-10-16



### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10161](https://issues.apache.org/jira/browse/CASSANDRA-10161) | Composite case-sensitive primary key: first item is not quoted in DESCRIBE TABLE |  Minor | Tools | Julien Moreau | Carl Yeksigian |
| [CASSANDRA-10275](https://issues.apache.org/jira/browse/CASSANDRA-10275) | Allow LOCAL\_JMX to be easily overridden |  Trivial | . | Thorkild Stray | Thorkild Stray |
| [CASSANDRA-10219](https://issues.apache.org/jira/browse/CASSANDRA-10219) | KeyCache deserialization doesn't properly read indexed entries |  Major | . | Sylvain Lebresne | Branimir Lambov |
| [CASSANDRA-10478](https://issues.apache.org/jira/browse/CASSANDRA-10478) | Seek position is not within mmap segment |  Critical | . | Omri Iluz | Benedict |
| [CASSANDRA-10507](https://issues.apache.org/jira/browse/CASSANDRA-10507) | Update cqlsh copy for new internal driver serialization interface (2.1 and 2.2) |  Major | Tools | Stefania | Stefania |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10205](https://issues.apache.org/jira/browse/CASSANDRA-10205) | decommissioned\_wiped\_node\_can\_join\_test fails on Jenkins |  Major | Distributed Metadata, Testing | Stefania | Stefania |


