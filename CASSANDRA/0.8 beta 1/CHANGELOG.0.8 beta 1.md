
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.8 beta 1 - 2011-04-22



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1669](https://issues.apache.org/jira/browse/CASSANDRA-1669) | Pluggable SeedProvider |  Major | . | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-1567](https://issues.apache.org/jira/browse/CASSANDRA-1567) | Provide configurable encryption support for internode communication |  Major | . | Nirmal Ranganathan | Nirmal Ranganathan |
| [CASSANDRA-2093](https://issues.apache.org/jira/browse/CASSANDRA-2093) | add countercolumn support to SSTableExport |  Minor | . | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-2124](https://issues.apache.org/jira/browse/CASSANDRA-2124) | JDBC driver for CQL |  Minor | CQL | Eric Evans | Vivek Mishra |
| [CASSANDRA-1791](https://issues.apache.org/jira/browse/CASSANDRA-1791) | Return name of snapshot directory after creating it and allow passing name to clearsnapshot |  Minor | . | paul cannon | Nick Bailey |
| [CASSANDRA-2156](https://issues.apache.org/jira/browse/CASSANDRA-2156) | Compaction Throttling |  Major | . | Stu Hood | Stu Hood |
| [CASSANDRA-1703](https://issues.apache.org/jira/browse/CASSANDRA-1703) | CQL 1.0 |  Minor | CQL | Eric Evans | Eric Evans |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1800](https://issues.apache.org/jira/browse/CASSANDRA-1800) | avoid double RowMutation serialization on write. |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-1937](https://issues.apache.org/jira/browse/CASSANDRA-1937) | Keep partitioned counters ("contexts") sorted |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-926](https://issues.apache.org/jira/browse/CASSANDRA-926) | remove alternative RPC interface using Avro |  Minor | . | Eric Evans | Eric Evans |
| [CASSANDRA-1944](https://issues.apache.org/jira/browse/CASSANDRA-1944) | Support CL.QUORUM and CL.ALL for counter writes |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-1933](https://issues.apache.org/jira/browse/CASSANDRA-1933) | generate human-readable json |  Minor | . | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1936](https://issues.apache.org/jira/browse/CASSANDRA-1936) | Fit partitioned counter directly into CounterColumn.value |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-1855](https://issues.apache.org/jira/browse/CASSANDRA-1855) | New default JMX Port |  Minor | Tools | Eric Evans | Eric Evans |
| [CASSANDRA-2092](https://issues.apache.org/jira/browse/CASSANDRA-2092) | Refactor gossiper |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-1015](https://issues.apache.org/jira/browse/CASSANDRA-1015) | Internal Messaging should be backwards compatible |  Critical | . | Ryan King | Gary Dusbabek |
| [CASSANDRA-2133](https://issues.apache.org/jira/browse/CASSANDRA-2133) | Improve "from the future" error message |  Minor | . | Stu Hood | Stu Hood |
| [CASSANDRA-1255](https://issues.apache.org/jira/browse/CASSANDRA-1255) | Explore interning keys and column names |  Minor | . | Stu Hood | Jonathan Ellis |
| [CASSANDRA-1848](https://issues.apache.org/jira/browse/CASSANDRA-1848) | Separate thrift and avro classes from cassandra's jar |  Trivial | Packaging | Tristan Tarrant | Eric Evans |
| [CASSANDRA-2299](https://issues.apache.org/jira/browse/CASSANDRA-2299) | Add counters to stress tool |  Minor | . | T Jake Luciani | Sylvain Lebresne |
| [CASSANDRA-1938](https://issues.apache.org/jira/browse/CASSANDRA-1938) | Use UUID as node identifiers in counters instead of IP addresses |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2288](https://issues.apache.org/jira/browse/CASSANDRA-2288) | AES Counter Repair Improvements |  Minor | . | Alan Liang | Alan Liang |
| [CASSANDRA-2272](https://issues.apache.org/jira/browse/CASSANDRA-2272) | Refactor Key and Row caches handling code |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2340](https://issues.apache.org/jira/browse/CASSANDRA-2340) | Easy means to set Maven repository locations |  Minor | Tools | Jürgen Hermann | Jürgen Hermann |
| [CASSANDRA-1906](https://issues.apache.org/jira/browse/CASSANDRA-1906) | Sanitize configuration code |  Minor | . | Jon Hermes | Jon Hermes |
| [CASSANDRA-2302](https://issues.apache.org/jira/browse/CASSANDRA-2302) | JDBC driver needs ResultSet.getMetaData() |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-2344](https://issues.apache.org/jira/browse/CASSANDRA-2344) | generate CQL python driver artifacts for release |  Minor | Packaging | Eric Evans | Eric Evans |
| [CASSANDRA-2341](https://issues.apache.org/jira/browse/CASSANDRA-2341) | Cli support for counters |  Minor | Tools | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2345](https://issues.apache.org/jira/browse/CASSANDRA-2345) | CLI: Units on show keyspaces output |  Minor | . | Jon Hermes | Pavel Yaskevich |
| [CASSANDRA-2284](https://issues.apache.org/jira/browse/CASSANDRA-2284) | Make changes to the set of memtables and sstables of a cfstore atomic |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2311](https://issues.apache.org/jira/browse/CASSANDRA-2311) | type validated row keys |  Major | . | Eric Evans | Jon Hermes |
| [CASSANDRA-2361](https://issues.apache.org/jira/browse/CASSANDRA-2361) | AES depends on java serialization |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-2374](https://issues.apache.org/jira/browse/CASSANDRA-2374) | baseline CQL drivers at version 1.0.0 |  Minor | CQL, Packaging | Eric Evans | Eric Evans |
| [CASSANDRA-2384](https://issues.apache.org/jira/browse/CASSANDRA-2384) | Merge Mutation and CounterMutation thrift structure |  Major | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-1942](https://issues.apache.org/jira/browse/CASSANDRA-1942) | upgrade to high-scale-lib (home of NBHM and NBHS) 1.1.2 |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1969](https://issues.apache.org/jira/browse/CASSANDRA-1969) | Use BB for row cache - To Improve GC performance. |  Minor | . | Vijay | Vijay |
| [CASSANDRA-2006](https://issues.apache.org/jira/browse/CASSANDRA-2006) | Serverwide caps on memtable thresholds |  Major | . | Stu Hood | Jonathan Ellis |
| [CASSANDRA-2439](https://issues.apache.org/jira/browse/CASSANDRA-2439) | CQL should allow no compression (UTF8 strings?). |  Major | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-2233](https://issues.apache.org/jira/browse/CASSANDRA-2233) | Add unified UUIDType |  Minor | . | Ed Anuff | Ed Anuff |
| [CASSANDRA-2440](https://issues.apache.org/jira/browse/CASSANDRA-2440) | Merge ColumnOrSuperColumn and Counter thrift structure to avoid most of the counter API call duplication |  Major | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2191](https://issues.apache.org/jira/browse/CASSANDRA-2191) | Multithread across compaction buckets |  Critical | . | Stu Hood | Stu Hood |
| [CASSANDRA-2396](https://issues.apache.org/jira/browse/CASSANDRA-2396) | Allow key name metadata |  Major | CQL | Jonathan Ellis | Jon Hermes |
| [CASSANDRA-2342](https://issues.apache.org/jira/browse/CASSANDRA-2342) | Add range slice support for counters |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2446](https://issues.apache.org/jira/browse/CASSANDRA-2446) | Remove unnecessary IOException from IColumnIterator.getColumnFamily |  Trivial | . | Stu Hood | Stu Hood |
| [CASSANDRA-2008](https://issues.apache.org/jira/browse/CASSANDRA-2008) | CLI help incorrect in places |  Trivial | . | amorton | amorton |
| [CASSANDRA-2106](https://issues.apache.org/jira/browse/CASSANDRA-2106) | Warn or fail when read repair is disabled with the dynamic snitch |  Minor | . | Stu Hood | Jonathan Ellis |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1903](https://issues.apache.org/jira/browse/CASSANDRA-1903) | NullPointerException from o.a.c.db.ReplicateOnWriteTask |  Major | . | Eric Evans | Kelvin Kakugawa |
| [CASSANDRA-1981](https://issues.apache.org/jira/browse/CASSANDRA-1981) | The writing of statistics in SSTableWrite.Builder has been mistakenly removed by #1072 |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-1989](https://issues.apache.org/jira/browse/CASSANDRA-1989) | Cannot parse generation after restart |  Major | . | Stu Hood | Pavel Yaskevich |
| [CASSANDRA-1999](https://issues.apache.org/jira/browse/CASSANDRA-1999) | Fix misuses of ByteBufferUtil.string() |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2001](https://issues.apache.org/jira/browse/CASSANDRA-2001) | 0.7 migrations/schema serializations are incompatible with trunk |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-1964](https://issues.apache.org/jira/browse/CASSANDRA-1964) | MutationTest of the distributed-test suite fails |  Minor | . | Michael Allen | Stu Hood |
| [CASSANDRA-2067](https://issues.apache.org/jira/browse/CASSANDRA-2067) | refactor o.a.c.utils.UUIDGen to allow creating type 1 UUIDs for a given time |  Major | . | Eric Evans | Eric Evans |
| [CASSANDRA-2095](https://issues.apache.org/jira/browse/CASSANDRA-2095) | SST counter repair |  Major | . | Kelvin Kakugawa | Kelvin Kakugawa |
| [CASSANDRA-2099](https://issues.apache.org/jira/browse/CASSANDRA-2099) | UUID generation when specifying date-times is broken |  Minor | . | Eric Evans | Eric Evans |
| [CASSANDRA-2101](https://issues.apache.org/jira/browse/CASSANDRA-2101) | support deletes in counters |  Major | . | Kelvin Kakugawa | Kelvin Kakugawa |
| [CASSANDRA-2140](https://issues.apache.org/jira/browse/CASSANDRA-2140) | versioning isn't going to work when forwarding messages |  Major | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-2152](https://issues.apache.org/jira/browse/CASSANDRA-2152) | Encryption options are not validated correctly in DatabaseDescriptor |  Major | . | Nate McCall | Nate McCall |
| [CASSANDRA-2155](https://issues.apache.org/jira/browse/CASSANDRA-2155) | Fix counter bug (regression from svn commit r1068504) |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2159](https://issues.apache.org/jira/browse/CASSANDRA-2159) | Fix build of stress.java in trunk |  Trivial | . | Sylvain Lebresne |  |
| [CASSANDRA-2163](https://issues.apache.org/jira/browse/CASSANDRA-2163) | UnavailableException after bootstrapping a previously decommissioned node |  Major | . | Sylvain Lebresne | Brandon Williams |
| [CASSANDRA-2178](https://issues.apache.org/jira/browse/CASSANDRA-2178) | Memtable Flush writers doesn't actually flush in parallel |  Minor | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-2188](https://issues.apache.org/jira/browse/CASSANDRA-2188) | sstable2json generates invalid json for "paged" rows |  Major | Tools | Shotaro Kamio | Pavel Yaskevich |
| [CASSANDRA-2236](https://issues.apache.org/jira/browse/CASSANDRA-2236) | Cli does not support updating replicate\_on\_write |  Trivial | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-2291](https://issues.apache.org/jira/browse/CASSANDRA-2291) | Ant build script in contrib/stress fails. |  Minor | . | Jahangir Mohammed | Jonathan Ellis |
| [CASSANDRA-2289](https://issues.apache.org/jira/browse/CASSANDRA-2289) | Replicate on write NPE for empty row |  Minor | . | Stu Hood | Stu Hood |
| [CASSANDRA-2022](https://issues.apache.org/jira/browse/CASSANDRA-2022) | Twisted driver for CQL |  Minor | CQL | Eric Evans | Brandon Williams |
| [CASSANDRA-2235](https://issues.apache.org/jira/browse/CASSANDRA-2235) | Counter AES performance issue |  Major | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-2351](https://issues.apache.org/jira/browse/CASSANDRA-2351) | Null CF comments should be allowed |  Minor | . | Gary Dusbabek | Jon Hermes |
| [CASSANDRA-2352](https://issues.apache.org/jira/browse/CASSANDRA-2352) | zero-length strings should result in zero-length ByteBuffers |  Major | CQL | Eric Evans | Eric Evans |
| [CASSANDRA-2105](https://issues.apache.org/jira/browse/CASSANDRA-2105) | Fix the read race condition in CFStore for counters |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2321](https://issues.apache.org/jira/browse/CASSANDRA-2321) | disallow to querying a counter CF with non-counter operation |  Minor | . | Mubarak Seyed | Sylvain Lebresne |
| [CASSANDRA-2409](https://issues.apache.org/jira/browse/CASSANDRA-2409) | Add INSERT support to CQL |  Major | CQL | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-2422](https://issues.apache.org/jira/browse/CASSANDRA-2422) | SSTables per read metric is incorrect |  Minor | . | Stu Hood | Stu Hood |
| [CASSANDRA-2437](https://issues.apache.org/jira/browse/CASSANDRA-2437) | ClusterTool throws exception for get\_endpoints |  Major | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-2313](https://issues.apache.org/jira/browse/CASSANDRA-2313) | CommutativeRowIndexer always read full row in memory |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2039](https://issues.apache.org/jira/browse/CASSANDRA-2039) | LazilyCompactedRow doesn't add CFInfo to digest |  Minor | . | Richard Low | Richard Low |
| [CASSANDRA-2432](https://issues.apache.org/jira/browse/CASSANDRA-2432) | add key\_validation\_class support to cli |  Minor | Documentation and Website | Ernesto Revilla | Pavel Yaskevich |
| [CASSANDRA-2445](https://issues.apache.org/jira/browse/CASSANDRA-2445) | Support SQL data types in CQL |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2324](https://issues.apache.org/jira/browse/CASSANDRA-2324) | Repair transfers more data than necessary |  Major | . | Brandon Williams | Sylvain Lebresne |
| [CASSANDRA-2448](https://issues.apache.org/jira/browse/CASSANDRA-2448) | Remove loadbalance command |  Minor | Tools | Nick Bailey | Nick Bailey |
| [CASSANDRA-2441](https://issues.apache.org/jira/browse/CASSANDRA-2441) | Cassandra crashes with segmentation fault on Debian 5.0 and Ubuntu 10.10 |  Critical | . | Pavel Yaskevich | Jonathan Ellis |
| [CASSANDRA-2458](https://issues.apache.org/jira/browse/CASSANDRA-2458) | cli divides read repair chance by 100 |  Minor | . | amorton | amorton |
| [CASSANDRA-2326](https://issues.apache.org/jira/browse/CASSANDRA-2326) | stress.java indexed range slicing is broken |  Trivial | . | Brandon Williams | Pavel Yaskevich |
| [CASSANDRA-2461](https://issues.apache.org/jira/browse/CASSANDRA-2461) | LongCompactionSpeedTest fails |  Major | . | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-2462](https://issues.apache.org/jira/browse/CASSANDRA-2462) | Fix build for distributed and stress tests |  Minor | Testing, Tools | Stu Hood | Stu Hood |
| [CASSANDRA-2482](https://issues.apache.org/jira/browse/CASSANDRA-2482) | Make compaction type an enum |  Trivial | . | Nick Bailey | Nick Bailey |
| [CASSANDRA-2454](https://issues.apache.org/jira/browse/CASSANDRA-2454) | Possible deadlock for counter mutations |  Major | . | Stu Hood | Kelvin Kakugawa |
| [CASSANDRA-2467](https://issues.apache.org/jira/browse/CASSANDRA-2467) | key validator not getting set when adding a keyspace |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-2450](https://issues.apache.org/jira/browse/CASSANDRA-2450) | incompatibility w/ 0.7 schemas |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2410](https://issues.apache.org/jira/browse/CASSANDRA-2410) | JDBC ResultSet does not honor column value typing for the CF and uses default validator for all column value types. |  Minor | CQL | Rick Shaw | Gary Dusbabek |
| [CASSANDRA-2493](https://issues.apache.org/jira/browse/CASSANDRA-2493) | CQL does not preserve column order in select statement |  Minor | CQL | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2420](https://issues.apache.org/jira/browse/CASSANDRA-2420) | row cache / streaming aren't aware of each other |  Minor | . | Matthew F. Dennis | Sylvain Lebresne |
| [CASSANDRA-2486](https://issues.apache.org/jira/browse/CASSANDRA-2486) | preserve KsDef backwards compatibility for Thrift clients |  Major | CQL | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2290](https://issues.apache.org/jira/browse/CASSANDRA-2290) | Repair hangs if one of the neighbor is dead |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2501](https://issues.apache.org/jira/browse/CASSANDRA-2501) | Error running cqlsh from .tar file -- global name 'SchemaDisagreementException' is not defined |  Major | . | Cathy Daw | Jonathan Ellis |
| [CASSANDRA-2505](https://issues.apache.org/jira/browse/CASSANDRA-2505) | cqlsh can't decode column names/values |  Major | CQL, Tools | Eric Evans | Eric Evans |
| [CASSANDRA-2488](https://issues.apache.org/jira/browse/CASSANDRA-2488) | cqlsh errors on comments that end with a semicolon |  Minor | Tools | Geoff Greer | Eric Evans |
| [CASSANDRA-2507](https://issues.apache.org/jira/browse/CASSANDRA-2507) | Python CQL driver does not decode most values |  Major | CQL | Eric Evans | Tyler Hobbs |
| [CASSANDRA-2509](https://issues.apache.org/jira/browse/CASSANDRA-2509) | Update py\_test to use strategy\_options |  Major | Tools | Robert Jackson | Robert Jackson |
| [CASSANDRA-2510](https://issues.apache.org/jira/browse/CASSANDRA-2510) | Word count example won't compile |  Minor | . | Jeremy Hanna | Jeremy Hanna |
| [CASSANDRA-2511](https://issues.apache.org/jira/browse/CASSANDRA-2511) | Need to forward merge parts of r1088800 to make the pig CassandraStorage build |  Major | . | Jeremy Hanna | Jeremy Hanna |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2005](https://issues.apache.org/jira/browse/CASSANDRA-2005) | Distributed canary test for counters |  Major | CQL | Stu Hood | Stu Hood |
| [CASSANDRA-2366](https://issues.apache.org/jira/browse/CASSANDRA-2366) | Remove more uses of SSTableUtils.writeRaw |  Major | . | Stu Hood | Stu Hood |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1823](https://issues.apache.org/jira/browse/CASSANDRA-1823) | move init script from contrib/redhat to redhat/ |  Minor | Packaging | Jonathan Ellis | Nick Bailey |
| [CASSANDRA-1072](https://issues.apache.org/jira/browse/CASSANDRA-1072) | Increment counters |  Major | . | Johan Oskarsson | Kelvin Kakugawa |
| [CASSANDRA-1913](https://issues.apache.org/jira/browse/CASSANDRA-1913) | move CQL from avro to thrift |  Minor | CQL | Eric Evans | Eric Evans |
| [CASSANDRA-1716](https://issues.apache.org/jira/browse/CASSANDRA-1716) | CQL column family truncation (aka TRUNCATE) |  Minor | CQL | Eric Evans | Eric Evans |
| [CASSANDRA-1704](https://issues.apache.org/jira/browse/CASSANDRA-1704) | CQL reads (aka SELECT) |  Minor | CQL | Eric Evans | Eric Evans |
| [CASSANDRA-1705](https://issues.apache.org/jira/browse/CASSANDRA-1705) | CQL writes (aka UPDATE) |  Minor | CQL | Eric Evans | Eric Evans |
| [CASSANDRA-1706](https://issues.apache.org/jira/browse/CASSANDRA-1706) | CQL deletes (aka DELETE) |  Minor | CQL | Eric Evans | Eric Evans |
| [CASSANDRA-1949](https://issues.apache.org/jira/browse/CASSANDRA-1949) | Message translation |  Major | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-2025](https://issues.apache.org/jira/browse/CASSANDRA-2025) | generalized way of expressing hierarchical values |  Minor | CQL | Eric Evans | Eric Evans |
| [CASSANDRA-1708](https://issues.apache.org/jira/browse/CASSANDRA-1708) | CQL authentication |  Minor | CQL | Eric Evans | Eric Evans |
| [CASSANDRA-1427](https://issues.apache.org/jira/browse/CASSANDRA-1427) | Optimize loadbalance/move for moves within the current range |  Major | . | Nick Bailey | Pavel Yaskevich |
| [CASSANDRA-2262](https://issues.apache.org/jira/browse/CASSANDRA-2262) | use o.a.c.marshal.\*Type for CQL |  Minor | CQL | Eric Evans | Gary Dusbabek |
| [CASSANDRA-1710](https://issues.apache.org/jira/browse/CASSANDRA-1710) | Java driver for CQL |  Minor | CQL | Eric Evans | Eric Evans |
| [CASSANDRA-2277](https://issues.apache.org/jira/browse/CASSANDRA-2277) | parameter substitution for Java CQL driver |  Major | CQL | Eric Evans | Gary Dusbabek |
| [CASSANDRA-2265](https://issues.apache.org/jira/browse/CASSANDRA-2265) | Fix distributed tests after updating Guava to 08 |  Blocker | . | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-1711](https://issues.apache.org/jira/browse/CASSANDRA-1711) | Python driver for CQL |  Minor | CQL | Eric Evans | Eric Evans |
| [CASSANDRA-2027](https://issues.apache.org/jira/browse/CASSANDRA-2027) | term definitions |  Minor | CQL | Eric Evans | Eric Evans |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1960](https://issues.apache.org/jira/browse/CASSANDRA-1960) | Make NetworkTopologyStrategy the default |  Minor | . | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-2024](https://issues.apache.org/jira/browse/CASSANDRA-2024) | Replace FBUtilities.byteBufferToInt with ByteBufferUtil.toInt |  Major | . | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-2050](https://issues.apache.org/jira/browse/CASSANDRA-2050) | AbstractDaemon unnecessarily uses jetty interface |  Minor | CQL | Nate McCall | Nate McCall |
| [CASSANDRA-2249](https://issues.apache.org/jira/browse/CASSANDRA-2249) | Use correct build version in build.xml |  Trivial | Packaging | Norman Maurer | Norman Maurer |
| [CASSANDRA-1257](https://issues.apache.org/jira/browse/CASSANDRA-1257) | Remove @Override annotations when implementing interfaces |  Trivial | . | Jeremy Hanna | Tommy Tynjä |
| [CASSANDRA-2412](https://issues.apache.org/jira/browse/CASSANDRA-2412) | Upgrade to thrift 0.6 |  Trivial | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-1263](https://issues.apache.org/jira/browse/CASSANDRA-1263) | Push replication factor down to the replication strategy |  Minor | . | Jeremy Hanna | Jon Hermes |
| [CASSANDRA-2402](https://issues.apache.org/jira/browse/CASSANDRA-2402) | Python dbapi driver for CQL |  Major | . | Jon Hermes | Jon Hermes |


