
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.2.2 - 2015-10-05



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10199](https://issues.apache.org/jira/browse/CASSANDRA-10199) | Warn on tiny disks instead of failing startup |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-10241](https://issues.apache.org/jira/browse/CASSANDRA-10241) | Keep a separate production debug log for troubleshooting |  Major | Configuration | Jonathan Ellis | Paulo Motta |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-8611](https://issues.apache.org/jira/browse/CASSANDRA-8611) | give streaming\_socket\_timeout\_in\_ms a non-zero default |  Major | . | Jeremy Hanna | Robert Coli |
| [CASSANDRA-10131](https://issues.apache.org/jira/browse/CASSANDRA-10131) | consistently sort DCs in nodetool:status |  Minor | . | Chris Burroughs | Chris Burroughs |
| [CASSANDRA-10222](https://issues.apache.org/jira/browse/CASSANDRA-10222) | Periodically attempt to delete failed snapshot deletions on Windows |  Major | Local Write-Read Paths | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-9761](https://issues.apache.org/jira/browse/CASSANDRA-9761) | Delay auth setup until peers are upgraded |  Major | . | Sam Tunnicliffe | Sylvain Lebresne |
| [CASSANDRA-7410](https://issues.apache.org/jira/browse/CASSANDRA-7410) | Pig support for BulkOutputFormat as a parameter in url |  Minor | . | Alex Liu | Alex Liu |
| [CASSANDRA-9446](https://issues.apache.org/jira/browse/CASSANDRA-9446) | Failure detector should ignore local pauses per endpoint |  Minor | . | sankalp kohli | Brandon Williams |
| [CASSANDRA-10330](https://issues.apache.org/jira/browse/CASSANDRA-10330) | Gossipinfo could return more useful information |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-9632](https://issues.apache.org/jira/browse/CASSANDRA-9632) | Preserve the Names of Query Parameters in QueryOptions |  Minor | CQL | stephen mallette | Benjamin Lerer |
| [CASSANDRA-9855](https://issues.apache.org/jira/browse/CASSANDRA-9855) | Make page\_size configurable in cqlsh |  Minor | . | Sylvain Lebresne | Ryan McGuire |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9414](https://issues.apache.org/jira/browse/CASSANDRA-9414) | Windows: deleteWithConfirm errors on various unit tests |  Minor | Local Write-Read Paths, Testing | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-9872](https://issues.apache.org/jira/browse/CASSANDRA-9872) | only check KeyCache when it is enabled |  Minor | . | Chris Burroughs | Chris Burroughs |
| [CASSANDRA-10057](https://issues.apache.org/jira/browse/CASSANDRA-10057) | RepairMessageVerbHandler.java:95 - Cannot start multiple repair sessions over the same sstables |  Major | Streaming and Messaging | Victor Trac | Yuki Morishita |
| [CASSANDRA-10206](https://issues.apache.org/jira/browse/CASSANDRA-10206) | Incorrect handling of end-of stream leading to infinite loop in streaming session |  Major | . | Alexey Burylov |  |
| [CASSANDRA-10239](https://issues.apache.org/jira/browse/CASSANDRA-10239) | Failure to launch on Windows with spaces in directory. |  Major | Packaging | vyacheslav zaslavskiy | Joshua McKenzie |
| [CASSANDRA-8741](https://issues.apache.org/jira/browse/CASSANDRA-8741) | Running a drain before a decommission apparently the wrong thing to do |  Trivial | . | Casey Marshall | Jan Karlsson |
| [CASSANDRA-9689](https://issues.apache.org/jira/browse/CASSANDRA-9689) | keyspace does not show in describe list, if create query times out |  Major | Tools | Roopesh | Paulo Motta |
| [CASSANDRA-10265](https://issues.apache.org/jira/browse/CASSANDRA-10265) | Properly deserialize PREPARE\_GLOBAL\_MESSAGE and update NEWS.txt about repair defaults in 2.2 |  Major | Streaming and Messaging | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-10240](https://issues.apache.org/jira/browse/CASSANDRA-10240) | sstableexpiredblockers can throw FileNotFound exceptions |  Major | Tools | Brandon Williams | Marcus Eriksson |
| [CASSANDRA-10209](https://issues.apache.org/jira/browse/CASSANDRA-10209) | Missing role manager in cassandra.yaml causes unexpected behaviour |  Minor | Configuration | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-10274](https://issues.apache.org/jira/browse/CASSANDRA-10274) | Assertion Errors when interrupting Cleanup |  Critical | . | Jeff Jirsa | Benedict |
| [CASSANDRA-9838](https://issues.apache.org/jira/browse/CASSANDRA-9838) | Unable to update an element in a static list |  Major | . | Mahesh Datt |  |
| [CASSANDRA-10282](https://issues.apache.org/jira/browse/CASSANDRA-10282) | cqlsh exception when starting with "--debug" option |  Minor | Tools | Ryan Okelberry | Adam Holmberg |
| [CASSANDRA-10270](https://issues.apache.org/jira/browse/CASSANDRA-10270) | Cassandra stops compacting |  Critical | Compaction | Adam Bliss | Marcus Eriksson |
| [CASSANDRA-10299](https://issues.apache.org/jira/browse/CASSANDRA-10299) | Issue with sstable selection when anti-compacting |  Major | . | Marcus Olsson | Marcus Olsson |
| [CASSANDRA-10296](https://issues.apache.org/jira/browse/CASSANDRA-10296) | Aggregates aren't resolved properly for reversed types |  Major | CQL | Carl Yeksigian | Carl Yeksigian |
| [CASSANDRA-10066](https://issues.apache.org/jira/browse/CASSANDRA-10066) | Bring cqlsh into PEP8 compliance |  Minor | . | Philip Thompson | Philip Thompson |
| [CASSANDRA-10272](https://issues.apache.org/jira/browse/CASSANDRA-10272) | BATCH statement is broken in cqlsh |  Major | Tools | Vovodroid | Stefania |
| [CASSANDRA-10067](https://issues.apache.org/jira/browse/CASSANDRA-10067) | Hadoop2 jobs throw java.lang.IncompatibleClassChangeError |  Major | . | Ashley Taylor | Brandon Williams |
| [CASSANDRA-9964](https://issues.apache.org/jira/browse/CASSANDRA-9964) | Document post-2.1 caching table options syntax |  Minor | Documentation and Website | Aleksey Yeschenko | Paulo Motta |
| [CASSANDRA-9758](https://issues.apache.org/jira/browse/CASSANDRA-9758) | nodetool compactionhistory NPE |  Minor | . | Pierre N. |  |
| [CASSANDRA-10155](https://issues.apache.org/jira/browse/CASSANDRA-10155) | 2i key cache load fails |  Major | Local Write-Read Paths | Robert Stupp | Ariel Weisberg |
| [CASSANDRA-10277](https://issues.apache.org/jira/browse/CASSANDRA-10277) | Empty BATCH throws exception in Cassandra |  Minor | . | Vovodroid |  |
| [CASSANDRA-10366](https://issues.apache.org/jira/browse/CASSANDRA-10366) | Added gossip states can shadow older unseen states |  Critical | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-9585](https://issues.apache.org/jira/browse/CASSANDRA-9585) | Make "truncate table X" an alias for "truncate X" |  Trivial | CQL | J.B. Langston | Benjamin Lerer |
| [CASSANDRA-10359](https://issues.apache.org/jira/browse/CASSANDRA-10359) | Saved caches use ambigous keyspace and CF name to identify tables |  Major | Local Write-Read Paths | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-10339](https://issues.apache.org/jira/browse/CASSANDRA-10339) | Prevent ALTER TYPE from creating circular references |  Minor | . | Olivier Michallat | Robert Stupp |
| [CASSANDRA-10238](https://issues.apache.org/jira/browse/CASSANDRA-10238) | Consolidating racks violates the RF contract |  Critical | Coordination | Brandon Williams | Stefania |
| [CASSANDRA-10352](https://issues.apache.org/jira/browse/CASSANDRA-10352) | Paging with DISTINCT and IN can throw ClassCastException |  Major | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-10369](https://issues.apache.org/jira/browse/CASSANDRA-10369) | cqlsh prompt includes name of keyspace after failed \`use\` statement |  Minor | . | Jim Witschey | Robert Stupp |
| [CASSANDRA-10228](https://issues.apache.org/jira/browse/CASSANDRA-10228) | JVMStabilityInspector should inspect cause and suppressed exceptions |  Major | . | Benedict | Paul MacIntosh |
| [CASSANDRA-10113](https://issues.apache.org/jira/browse/CASSANDRA-10113) | Undroppable messages can be dropped if message queue gets large |  Minor | Streaming and Messaging | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-10052](https://issues.apache.org/jira/browse/CASSANDRA-10052) | Misleading down-node push notifications when rpc\_address is shared |  Major | CQL | Sharvanath Pathak | Stefania |
| [CASSANDRA-10357](https://issues.apache.org/jira/browse/CASSANDRA-10357) | mmap file boundary selection is broken for some large files |  Major | . | Benedict | Benedict |
| [CASSANDRA-10347](https://issues.apache.org/jira/browse/CASSANDRA-10347) | Bulk Loader API could not tolerate even node failure |  Major | Tools | Shenghua Wan | Paulo Motta |


