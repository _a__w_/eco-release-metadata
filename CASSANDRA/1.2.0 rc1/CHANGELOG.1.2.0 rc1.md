
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.2.0 rc1 - 2012-12-17



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4790](https://issues.apache.org/jira/browse/CASSANDRA-4790) | Allow property override of available processors value |  Minor | . | Ahmed Bashir | Brandon Williams |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5029](https://issues.apache.org/jira/browse/CASSANDRA-5029) | Add back bloom filter to LCS |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5027](https://issues.apache.org/jira/browse/CASSANDRA-5027) | rename rpc\_timeout options to request\_timeout |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5003](https://issues.apache.org/jira/browse/CASSANDRA-5003) | Update IAuthenticator to allow dynamic user creation and removal |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5026](https://issues.apache.org/jira/browse/CASSANDRA-5026) | Reduce log spam from counter shard warnings |  Minor | . | Jonathan Ellis | Sylvain Lebresne |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5022](https://issues.apache.org/jira/browse/CASSANDRA-5022) | Can't prepare an UPDATE query with a counter column (CQL3) |  Minor | . | Aleksey Yeschenko | Sylvain Lebresne |
| [CASSANDRA-5016](https://issues.apache.org/jira/browse/CASSANDRA-5016) | Can't prepare an INSERT query |  Major | . | Aleksey Yeschenko | Sylvain Lebresne |
| [CASSANDRA-5032](https://issues.apache.org/jira/browse/CASSANDRA-5032) | Downed node loses its host-id |  Major | . | T Jake Luciani | Brandon Williams |
| [CASSANDRA-5012](https://issues.apache.org/jira/browse/CASSANDRA-5012) | alter table alter causes TSocket read 0 bytes |  Major | . | K. B. Hahn | Sylvain Lebresne |
| [CASSANDRA-5002](https://issues.apache.org/jira/browse/CASSANDRA-5002) | UUIGen should never use another host IP for its node part |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5008](https://issues.apache.org/jira/browse/CASSANDRA-5008) | "show schema" command in cassandra-cli generates wrong "index\_options" values. |  Minor | . | Thierry Boileau | Yuki Morishita |
| [CASSANDRA-5030](https://issues.apache.org/jira/browse/CASSANDRA-5030) | IndexHelper.IndexFor call throws AOB exception when passing multiple slices |  Major | . | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-5040](https://issues.apache.org/jira/browse/CASSANDRA-5040) | Can't insert only a key in CQL3 |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4827](https://issues.apache.org/jira/browse/CASSANDRA-4827) | cqlsh --cql3 unable to describe CF created with cli |  Minor | Tools | Jonathan Ellis | Aleksey Yeschenko |
| [CASSANDRA-5046](https://issues.apache.org/jira/browse/CASSANDRA-5046) | cqlsh doesn't show correct timezone when SELECTing a column of type TIMESTAMP |  Minor | . | B. Todd Burruss | Aleksey Yeschenko |
| [CASSANDRA-5017](https://issues.apache.org/jira/browse/CASSANDRA-5017) | Preparing UPDATE queries with collections returns suboptimal metadata |  Minor | . | Aleksey Yeschenko | Sylvain Lebresne |
| [CASSANDRA-5049](https://issues.apache.org/jira/browse/CASSANDRA-5049) | Auth.setup() is called too early |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5050](https://issues.apache.org/jira/browse/CASSANDRA-5050) | Cql3 token queries broken |  Major | . | T Jake Luciani | T Jake Luciani |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4997](https://issues.apache.org/jira/browse/CASSANDRA-4997) | Remove multithreaded\_compaction from cassandra.yaml |  Trivial | . | Jeremy Hanna | Jonathan Ellis |


