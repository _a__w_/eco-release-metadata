
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.2.0 rc1 - 2015-06-08



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7207](https://issues.apache.org/jira/browse/CASSANDRA-7207) | nodetool: Allow to stop a specific compaction |  Minor | Tools | PJ | Lyuben Todorov |
| [CASSANDRA-5969](https://issues.apache.org/jira/browse/CASSANDRA-5969) | Allow JVM\_OPTS to be passed to sstablescrub |  Major | Tools | Adam Hattrell | Stefania |
| [CASSANDRA-9436](https://issues.apache.org/jira/browse/CASSANDRA-9436) | Expose rpc\_address and broadcast\_address of each Cassandra node |  Major | Distributed Metadata | Piotr Kołaczkowski | Carl Yeksigian |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-8241](https://issues.apache.org/jira/browse/CASSANDRA-8241) | Use ecj [was: javac] instead of javassist |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-8984](https://issues.apache.org/jira/browse/CASSANDRA-8984) | Introduce Transactional API for behaviours that can corrupt system state |  Major | . | Benedict | Benedict |
| [CASSANDRA-9397](https://issues.apache.org/jira/browse/CASSANDRA-9397) | Wrong gc\_grace\_seconds used in anticompaction |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-8603](https://issues.apache.org/jira/browse/CASSANDRA-8603) | Cut tombstone memory footprint in half for cql deletes |  Major | Local Write-Read Paths | Dominic Letz | Benjamin Lerer |
| [CASSANDRA-9107](https://issues.apache.org/jira/browse/CASSANDRA-9107) | More accurate row count estimates |  Major | . | Chris Lohfink | Chris Lohfink |
| [CASSANDRA-9224](https://issues.apache.org/jira/browse/CASSANDRA-9224) | Figure out a better default float precision rule for cqlsh |  Major | Tools | Tyler Hobbs | Stefania |
| [CASSANDRA-9096](https://issues.apache.org/jira/browse/CASSANDRA-9096) | Improve ByteBuffer compression interface |  Major | . | Branimir Lambov | Branimir Lambov |
| [CASSANDRA-9350](https://issues.apache.org/jira/browse/CASSANDRA-9350) | Commit log archiving can use ln instead of cp now that segments are not recycled |  Trivial | . | Ariel Weisberg | Branimir Lambov |
| [CASSANDRA-9431](https://issues.apache.org/jira/browse/CASSANDRA-9431) | Static Analysis to warn on unsafe use of Autocloseable instances |  Major | Configuration, Testing | Benedict | T Jake Luciani |
| [CASSANDRA-9370](https://issues.apache.org/jira/browse/CASSANDRA-9370) | Clean up gossiper logic for old versions |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-9504](https://issues.apache.org/jira/browse/CASSANDRA-9504) | Odd performance numbers comparing 2.0.15 and 2.1+ |  Major | . | Philip Thompson | Jonathan Ellis |
| [CASSANDRA-9095](https://issues.apache.org/jira/browse/CASSANDRA-9095) | Compressed commit log should measure compressed space used |  Minor | . | Branimir Lambov | Branimir Lambov |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9194](https://issues.apache.org/jira/browse/CASSANDRA-9194) | Delete-only workloads crash Cassandra |  Major | . | Robert Wille | Benedict |
| [CASSANDRA-9406](https://issues.apache.org/jira/browse/CASSANDRA-9406) | Add Option to Not Validate Atoms During Scrub |  Minor | Tools | Jordan West | Jordan West |
| [CASSANDRA-9429](https://issues.apache.org/jira/browse/CASSANDRA-9429) | Revert CASSANDRA-7807 (tracing completion client notifications) |  Minor | . | Adam Holmberg | Robert Stupp |
| [CASSANDRA-9419](https://issues.apache.org/jira/browse/CASSANDRA-9419) | CqlRecordWriter does not adequately determine if driver failed to connect due to interrupt |  Major | . | Philip Thompson | Philip Thompson |
| [CASSANDRA-9422](https://issues.apache.org/jira/browse/CASSANDRA-9422) | SystemKeyspaceTest fails with non-release version string |  Major | Testing | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-9409](https://issues.apache.org/jira/browse/CASSANDRA-9409) | Ensure that UDF and UDAs are keyspace-isolated |  Major | . | Aleksey Yeschenko | Robert Stupp |
| [CASSANDRA-9399](https://issues.apache.org/jira/browse/CASSANDRA-9399) | cqlsh support for native protocol v4 features |  Major | Tools | Aleksey Yeschenko | Stefania |
| [CASSANDRA-8568](https://issues.apache.org/jira/browse/CASSANDRA-8568) | Extend Transactional API to sstable lifecycle management |  Major | . | Benedict | Benedict |
| [CASSANDRA-9442](https://issues.apache.org/jira/browse/CASSANDRA-9442) | Pig tests failing in 2.2 and trunk |  Major | . | Philip Thompson | Philip Thompson |
| [CASSANDRA-9410](https://issues.apache.org/jira/browse/CASSANDRA-9410) | Fix? jar conflict with ecj |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-9438](https://issues.apache.org/jira/browse/CASSANDRA-9438) | Repair version gating will do the wrong thing in rc/beta dev builds |  Major | Streaming and Messaging | Jeremiah Jordan | Yuki Morishita |
| [CASSANDRA-9466](https://issues.apache.org/jira/browse/CASSANDRA-9466) | CacheService.KeyCacheSerializer affects cache hit rate |  Minor | Tools | sankalp kohli | sankalp kohli |
| [CASSANDRA-9480](https://issues.apache.org/jira/browse/CASSANDRA-9480) | Example UDFs don't work |  Minor | Documentation and Website | Christopher Batey | Robert Stupp |
| [CASSANDRA-8502](https://issues.apache.org/jira/browse/CASSANDRA-8502) | Static columns returning null for pages after first |  Major | . | Flavien Charlon | Tyler Hobbs |
| [CASSANDRA-9441](https://issues.apache.org/jira/browse/CASSANDRA-9441) | Reject frozen\<\> types in UDF |  Major | CQL | Aleksey Yeschenko | Benjamin Lerer |
| [CASSANDRA-9508](https://issues.apache.org/jira/browse/CASSANDRA-9508) | Avoid early opening of compaction results when doing anticompaction |  Critical | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9485](https://issues.apache.org/jira/browse/CASSANDRA-9485) | RangeTombstoneListTest.addAllRandomTest failed on trunk |  Minor | Local Write-Read Paths | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-9492](https://issues.apache.org/jira/browse/CASSANDRA-9492) | Error message changes based on jdk used |  Major | Testing | Philip Thompson | Carl Yeksigian |
| [CASSANDRA-9478](https://issues.apache.org/jira/browse/CASSANDRA-9478) | SSTables are not always properly marked suspected |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-9515](https://issues.apache.org/jira/browse/CASSANDRA-9515) | Bytes map encoding does not match protocol spec |  Major | CQL | Olivier Michallat | Benjamin Lerer |
| [CASSANDRA-9521](https://issues.apache.org/jira/browse/CASSANDRA-9521) | Cannot start dclocal repairs |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9388](https://issues.apache.org/jira/browse/CASSANDRA-9388) | Truncate can (in theory) lead to corrupt responses to client or segfault |  Minor | . | Benedict | Benedict |
| [CASSANDRA-9486](https://issues.apache.org/jira/browse/CASSANDRA-9486) | LazilyCompactedRow accumulates all expired RangeTombstones |  Critical | . | Benedict | Sylvain Lebresne |
| [CASSANDRA-9531](https://issues.apache.org/jira/browse/CASSANDRA-9531) | NullPointerException logged when running MessagePayloadTest |  Minor | Local Write-Read Paths | Benjamin Lerer | T Jake Luciani |
| [CASSANDRA-9534](https://issues.apache.org/jira/browse/CASSANDRA-9534) | Repair data not always saved to system\_distributed |  Major | . | Jim Witschey | Marcus Eriksson |
| [CASSANDRA-8609](https://issues.apache.org/jira/browse/CASSANDRA-8609) | Remove depency of hadoop to internals (Cell/CellName) |  Major | Local Write-Read Paths | Sylvain Lebresne | Sam Tunnicliffe |
| [CASSANDRA-9457](https://issues.apache.org/jira/browse/CASSANDRA-9457) | Empty INITCOND treated as null in aggregate |  Minor | . | Olivier Michallat | Robert Stupp |
| [CASSANDRA-9400](https://issues.apache.org/jira/browse/CASSANDRA-9400) | smallint, tinyint, date + time types in UDFs |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-8487](https://issues.apache.org/jira/browse/CASSANDRA-8487) | system.schema\_columns sometimes missing for 'system' keyspace |  Minor | . | Adam Holmberg | Aleksey Yeschenko |
| [CASSANDRA-9083](https://issues.apache.org/jira/browse/CASSANDRA-9083) | cqlsh COPY functionality doesn't work together with SOURCE or with cqlsh -f |  Minor | Tools | Joseph Chu | Tyler Hobbs |
| [CASSANDRA-9551](https://issues.apache.org/jira/browse/CASSANDRA-9551) | Bug in CassandraRoleManager.collectRoles() |  Major | Distributed Metadata | Aleksey Yeschenko | Sam Tunnicliffe |
| [CASSANDRA-9132](https://issues.apache.org/jira/browse/CASSANDRA-9132) | resumable\_bootstrap\_test can hang |  Major | Testing | Tyler Hobbs | Yuki Morishita |
| [CASSANDRA-9451](https://issues.apache.org/jira/browse/CASSANDRA-9451) | Startup message response for unsupported protocol versions is incorrect |  Major | CQL | Jorge Bay | Stefania |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9306](https://issues.apache.org/jira/browse/CASSANDRA-9306) | Test coverage for cqlsh COPY |  Major | . | Tyler Hobbs | Jim Witschey |
| [CASSANDRA-9432](https://issues.apache.org/jira/browse/CASSANDRA-9432) | SliceQueryFilterWithTombstonesTest.testExpiredTombstones is failing infrequently |  Major | Testing | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-9403](https://issues.apache.org/jira/browse/CASSANDRA-9403) | Experiment with skipping file syncs during unit tests to reduce test time |  Major | Testing | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-9443](https://issues.apache.org/jira/browse/CASSANDRA-9443) | UFTest & UFIdentificationTest are failing in the CI environment |  Major | Testing | Sam Tunnicliffe | Ariel Weisberg |
| [CASSANDRA-8656](https://issues.apache.org/jira/browse/CASSANDRA-8656) | long-test LongLeveledCompactionStrategyTest flaps in 2.0 |  Minor | Testing | Michael Shuler | Stefania |
| [CASSANDRA-9537](https://issues.apache.org/jira/browse/CASSANDRA-9537) | Sstables created with MockSchema occasionally triggers assertions |  Major | Testing | Stefania | Stefania |
| [CASSANDRA-9271](https://issues.apache.org/jira/browse/CASSANDRA-9271) | IndexSummaryManagerTest.testCompactionRace times out periodically |  Trivial | Testing | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-9514](https://issues.apache.org/jira/browse/CASSANDRA-9514) | TrackerTest.testAddSSTables fails in 2.2 and trunk |  Major | Testing | Michael Shuler | Stefania |
| [CASSANDRA-9561](https://issues.apache.org/jira/browse/CASSANDRA-9561) | Lifecycle unit tests fail when run in parallel |  Major | Local Write-Read Paths, Testing | Stefania | Stefania |
| [CASSANDRA-9463](https://issues.apache.org/jira/browse/CASSANDRA-9463) | ant test-all results incomplete when parsed |  Major | Testing | Michael Shuler | Ariel Weisberg |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7558](https://issues.apache.org/jira/browse/CASSANDRA-7558) | Document users and permissions in CQL docs |  Minor | Documentation and Website | Tyler Hobbs | Sam Tunnicliffe |
| [CASSANDRA-9493](https://issues.apache.org/jira/browse/CASSANDRA-9493) | Integrate snapshot 2.2rc Java Driver |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-9546](https://issues.apache.org/jira/browse/CASSANDRA-9546) | Deprecate SSTableSimpleWriter/SSTableSimpleUnsortedWriter in 2.2 |  Major | . | Sylvain Lebresne | Sylvain Lebresne |


