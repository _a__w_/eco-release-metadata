
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1 rc3 - 2014-07-10



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6146](https://issues.apache.org/jira/browse/CASSANDRA-6146) | CQL-native stress |  Major | Tools | Jonathan Ellis | T Jake Luciani |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7411](https://issues.apache.org/jira/browse/CASSANDRA-7411) | Node enables vnodes when bounced |  Minor | . | Philip Thompson | Philip Thompson |
| [CASSANDRA-7345](https://issues.apache.org/jira/browse/CASSANDRA-7345) | Unfinished or inflight CAS are always done at QUORUM |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-7495](https://issues.apache.org/jira/browse/CASSANDRA-7495) | Remove traces of populate\_io\_cache\_on\_flush and replicate\_on\_write |  Trivial | . | Aleksey Yeschenko | Aleksey Yeschenko |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7371](https://issues.apache.org/jira/browse/CASSANDRA-7371) | DELETEs get lost |  Blocker | . | Robert Stupp | T Jake Luciani |
| [CASSANDRA-7428](https://issues.apache.org/jira/browse/CASSANDRA-7428) | clean out DD.inMemoryCompactionLimit |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-7394](https://issues.apache.org/jira/browse/CASSANDRA-7394) | Fix CollationController#collectTimeOrderedData() mostRecentRowTombstone tracking |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-7442](https://issues.apache.org/jira/browse/CASSANDRA-7442) | Parallelize unit test runner |  Minor | Testing | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-7440](https://issues.apache.org/jira/browse/CASSANDRA-7440) | Unrecognized opcode or flag causes ArrayIndexOutOfBoundsException |  Minor | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-7459](https://issues.apache.org/jira/browse/CASSANDRA-7459) | CqlRecordWriter doesn't close socket connections |  Major | . | Peter Williams | Peter Williams |
| [CASSANDRA-7451](https://issues.apache.org/jira/browse/CASSANDRA-7451) | Launch scripts on Windows don't handle spaces gracefully |  Minor | Packaging | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-7429](https://issues.apache.org/jira/browse/CASSANDRA-7429) | merge commit\_failure\_policy to 2.1 |  Major | . | Jonathan Ellis | Benedict |
| [CASSANDRA-7476](https://issues.apache.org/jira/browse/CASSANDRA-7476) | Add support for Snappy compression of PPC64, Big Endian |  Minor | . | Iheanyi Ekechukwu | Jonathan Ellis |
| [CASSANDRA-7465](https://issues.apache.org/jira/browse/CASSANDRA-7465) | DecoratedKey assertion error on reads |  Blocker | . | Jason Brown | T Jake Luciani |
| [CASSANDRA-7493](https://issues.apache.org/jira/browse/CASSANDRA-7493) | Remove traces of rows\_per\_partition\_to\_cache |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-7472](https://issues.apache.org/jira/browse/CASSANDRA-7472) | Prepared marker for collections inside UDT are not properly handled |  Major | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-7496](https://issues.apache.org/jira/browse/CASSANDRA-7496) | ClassCastException in MessagingService |  Major | . | Jason Brown | Jason Brown |
| [CASSANDRA-7254](https://issues.apache.org/jira/browse/CASSANDRA-7254) | NPE on startup if another Cassandra instance is already running |  Minor | . | Tyler Hobbs | Brandon Williams |
| [CASSANDRA-7502](https://issues.apache.org/jira/browse/CASSANDRA-7502) | NoHostAvailableException when preparing a statement with 'CONTAINS' |  Major | CQL | Fabrice Larcher | Sylvain Lebresne |
| [CASSANDRA-7484](https://issues.apache.org/jira/browse/CASSANDRA-7484) | Throw exception on unknown UDT field |  Minor | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-7403](https://issues.apache.org/jira/browse/CASSANDRA-7403) | Reconciliation doesn't consider fields specific to expiring cells |  Major | . | Sam Tunnicliffe | Benedict |
| [CASSANDRA-7505](https://issues.apache.org/jira/browse/CASSANDRA-7505) | cassandra fails to start if WMI memory query fails on Windows |  Trivial | Packaging | Joshua McKenzie | Joshua McKenzie |


