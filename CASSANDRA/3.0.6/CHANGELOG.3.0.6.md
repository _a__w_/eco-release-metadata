
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.0.6 - 2016-05-13



### IMPORTANT ISSUES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11574](https://issues.apache.org/jira/browse/CASSANDRA-11574) | clqsh: COPY FROM throws TypeError with Cython extensions enabled |  Major | Tools | Mahafuzur Rahman | Stefania |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10120](https://issues.apache.org/jira/browse/CASSANDRA-10120) | When specifying both num\_tokens and initial\_token, error out if the numbers don't match |  Minor | . | Jeremy Hanna | Roman Pogribnyi |
| [CASSANDRA-11410](https://issues.apache.org/jira/browse/CASSANDRA-11410) | Option to specify ProtocolVersion in cassandra-stress |  Minor | Testing, Tools | mck | mck |
| [CASSANDRA-11412](https://issues.apache.org/jira/browse/CASSANDRA-11412) | Many sstablescanners opened during repair |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-11593](https://issues.apache.org/jira/browse/CASSANDRA-11593) | getFunctions methods produce too much garbage |  Major | . | Benjamin Lerer | Benjamin Lerer |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11462](https://issues.apache.org/jira/browse/CASSANDRA-11462) | IncomingStreamingConnection version check message wrong |  Trivial | . | Robert Stupp |  |
| [CASSANDRA-11329](https://issues.apache.org/jira/browse/CASSANDRA-11329) | Indexers are not informed when expired rows are encountered in compaction |  Major | Compaction | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-11515](https://issues.apache.org/jira/browse/CASSANDRA-11515) | C\* won't launch with whitespace in path on Windows |  Trivial | . | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-11470](https://issues.apache.org/jira/browse/CASSANDRA-11470) | dtest failure in materialized\_views\_test.TestMaterializedViews.base\_replica\_repair\_test |  Major | . | Philip Thompson | Stefania |
| [CASSANDRA-11430](https://issues.apache.org/jira/browse/CASSANDRA-11430) | Add legacy notifications backward-support on deprecated repair methods |  Major | Observability, Streaming and Messaging | Nick Bailey | Paulo Motta |
| [CASSANDRA-11529](https://issues.apache.org/jira/browse/CASSANDRA-11529) | Checking if an unlogged batch is local is inefficient |  Critical | Coordination | Paulo Motta | Stefania |
| [CASSANDRA-11532](https://issues.apache.org/jira/browse/CASSANDRA-11532) | CqlConfigHelper requires both truststore and keystore to work with SSL encryption |  Major | . | Jacek Lewandowski | Jacek Lewandowski |
| [CASSANDRA-11485](https://issues.apache.org/jira/browse/CASSANDRA-11485) | ArithmeticException in avgFunctionForDecimal |  Minor | CQL | Nico Haller | Robert Stupp |
| [CASSANDRA-11553](https://issues.apache.org/jira/browse/CASSANDRA-11553) | hadoop.cql3.CqlRecordWriter does not close cluster on reconnect |  Major | Tools | Artem Aliev | Artem Aliev |
| [CASSANDRA-11620](https://issues.apache.org/jira/browse/CASSANDRA-11620) | help text in sstabledump references row keys when it should say partition keys |  Minor | Tools | Jon Haddad | Jon Haddad |
| [CASSANDRA-11523](https://issues.apache.org/jira/browse/CASSANDRA-11523) | server side exception on secondary index query through thrift |  Major | Core, Secondary Indexes | Ivan Georgiev | Sam Tunnicliffe |
| [CASSANDRA-11474](https://issues.apache.org/jira/browse/CASSANDRA-11474) | cqlsh: COPY FROM should use regular inserts for single statement batches |  Minor | Tools | Stefania | Stefania |
| [CASSANDRA-11549](https://issues.apache.org/jira/browse/CASSANDRA-11549) | cqlsh: COPY FROM ignores NULL values in conversion |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-11627](https://issues.apache.org/jira/browse/CASSANDRA-11627) | Streaming and other ops should filter out all LocalStrategy keyspaces, not just system keyspaces |  Minor | Streaming and Messaging | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-11633](https://issues.apache.org/jira/browse/CASSANDRA-11633) | cqlsh COPY FROM fails with []{} chars in UDT/tuple fields/values |  Minor | Tools | Robert Stupp | Robert Stupp |
| [CASSANDRA-11628](https://issues.apache.org/jira/browse/CASSANDRA-11628) | Fix the regression to CASSANDRA-3983 that got introduced by CASSANDRA-10679 |  Major | Tools | Wei Deng | Wei Deng |
| [CASSANDRA-9935](https://issues.apache.org/jira/browse/CASSANDRA-9935) | Repair fails with RuntimeException |  Major | . | mlowicki | Paulo Motta |
| [CASSANDRA-11654](https://issues.apache.org/jira/browse/CASSANDRA-11654) | sstabledump is not able to properly print out SSTable that may contain historical (but "shadowed") row tombstone |  Major | Tools | Wei Deng | Yuki Morishita |
| [CASSANDRA-11642](https://issues.apache.org/jira/browse/CASSANDRA-11642) | sstabledump and sstableverify need to be added to deb packages |  Major | Tools | Attila Szucs | T Jake Luciani |
| [CASSANDRA-11630](https://issues.apache.org/jira/browse/CASSANDRA-11630) | Make cython optional in pylib/setup.py |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-11631](https://issues.apache.org/jira/browse/CASSANDRA-11631) | cqlsh COPY FROM fails for null values with non-prepared statements |  Minor | Tools | Robert Stupp | Stefania |
| [CASSANDRA-11660](https://issues.apache.org/jira/browse/CASSANDRA-11660) | Dubious call to remove in CompactionManager.java |  Minor | Compaction | Max Schaefer | Marcus Eriksson |
| [CASSANDRA-11137](https://issues.apache.org/jira/browse/CASSANDRA-11137) | JSON datetime formatting needs timezone |  Major | CQL | Stefania | Alex Petrov |
| [CASSANDRA-11502](https://issues.apache.org/jira/browse/CASSANDRA-11502) | Fix denseness and column metadata updates coming from Thrift |  Minor | Distributed Metadata | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-11600](https://issues.apache.org/jira/browse/CASSANDRA-11600) | Don't require HEAP\_NEW\_SIZE to be set when using G1 |  Minor | . | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-11669](https://issues.apache.org/jira/browse/CASSANDRA-11669) | RangeName queries might not return all the results |  Major | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-11510](https://issues.apache.org/jira/browse/CASSANDRA-11510) | Clustering key and secondary index |  Major | CQL, Secondary Indexes | Julien Muller | Sam Tunnicliffe |
| [CASSANDRA-11655](https://issues.apache.org/jira/browse/CASSANDRA-11655) | sstabledump doesn't print out tombstone information for deleted collection column |  Major | Tools | Wei Deng | Chris Lohfink |
| [CASSANDRA-11629](https://issues.apache.org/jira/browse/CASSANDRA-11629) | java.lang.UnsupportedOperationException when selecting rows with counters |  Major | . | Arnd Hannemann | Alex Petrov |
| [CASSANDRA-11618](https://issues.apache.org/jira/browse/CASSANDRA-11618) | Removing an element from map\<any type, tinyint/smallint\> corrupts commitlog |  Critical | Local Write-Read Paths | Artem Chudinov | Sylvain Lebresne |
| [CASSANDRA-11602](https://issues.apache.org/jira/browse/CASSANDRA-11602) | Materialized View Does Not Have Static Columns |  Major | Coordination, Materialized Views | Ravishankar Rajendran | Carl Yeksigian |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10853](https://issues.apache.org/jira/browse/CASSANDRA-10853) | deb package migration to dh\_python2 |  Major | Packaging | Michael Shuler | Michael Shuler |


