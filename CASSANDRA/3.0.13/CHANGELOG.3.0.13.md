
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.0.13 - 2017-04-14



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13238](https://issues.apache.org/jira/browse/CASSANDRA-13238) | Add actual row output to assertEmpty error message |  Trivial | . | Alex Petrov | Alex Petrov |
| [CASSANDRA-13388](https://issues.apache.org/jira/browse/CASSANDRA-13388) | Add hosted CI config files (such as Circle and TravisCI) for easier developer testing |  Major | Testing | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-13060](https://issues.apache.org/jira/browse/CASSANDRA-13060) | NPE in StorageService.java while bootstrapping |  Trivial | . | Jay Zhuang | Jay Zhuang |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13053](https://issues.apache.org/jira/browse/CASSANDRA-13053) | GRANT/REVOKE on table without keyspace performs permissions check incorrectly |  Minor | CQL | Sam Tunnicliffe | Aleksey Yeschenko |
| [CASSANDRA-12513](https://issues.apache.org/jira/browse/CASSANDRA-12513) | IOException (No such file or directory) closing MessagingService's server socket (locally) |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-13305](https://issues.apache.org/jira/browse/CASSANDRA-13305) | Slice.isEmpty() returns false for some empty slices |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-13282](https://issues.apache.org/jira/browse/CASSANDRA-13282) | Commitlog replay may fail if last mutation is within 4 bytes of end of segment |  Major | Core | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-13343](https://issues.apache.org/jira/browse/CASSANDRA-13343) | Wrong logger name in AnticompactionTask |  Trivial | . | Simon Zhou | Simon Zhou |
| [CASSANDRA-13320](https://issues.apache.org/jira/browse/CASSANDRA-13320) | upgradesstables fails after upgrading from 2.1.x to 3.0.11 |  Major | . | Zhongxiang Zheng | Sam Tunnicliffe |
| [CASSANDRA-13246](https://issues.apache.org/jira/browse/CASSANDRA-13246) | Querying by secondary index on collection column returns NullPointerException sometimes |  Major | Local Write-Read Paths, Secondary Indexes | hochung |  |
| [CASSANDRA-13153](https://issues.apache.org/jira/browse/CASSANDRA-13153) | Reappeared Data when Mixing Incremental and Full Repairs |  Major | Compaction, Tools | Amanda Debrot | Stefan Podkowinski |
| [CASSANDRA-12653](https://issues.apache.org/jira/browse/CASSANDRA-12653) | In-flight shadow round requests |  Minor | Distributed Metadata | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-13340](https://issues.apache.org/jira/browse/CASSANDRA-13340) | Bugs handling range tombstones in the sstable iterators |  Critical | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-12773](https://issues.apache.org/jira/browse/CASSANDRA-12773) | cassandra-stress error for one way SSL |  Major | Tools | Jane Deng | Stefan Podkowinski |
| [CASSANDRA-13337](https://issues.apache.org/jira/browse/CASSANDRA-13337) | Dropping column results in "corrupt" SSTable |  Major | Compaction | Jonas Borgström | Sylvain Lebresne |
| [CASSANDRA-13274](https://issues.apache.org/jira/browse/CASSANDRA-13274) | Fix code to not exchange schema across major versions |  Critical | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-13384](https://issues.apache.org/jira/browse/CASSANDRA-13384) | Legacy caching options can prevent 3.0 upgrade |  Minor | Distributed Metadata | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-13341](https://issues.apache.org/jira/browse/CASSANDRA-13341) | Legacy deserializer can create empty range tombstones |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-13389](https://issues.apache.org/jira/browse/CASSANDRA-13389) | Possible NPE on upgrade to 3.0/3.X in case of IO errors |  Major | Lifecycle | Stefania | Stefania |
| [CASSANDRA-12825](https://issues.apache.org/jira/browse/CASSANDRA-12825) | testall failure in org.apache.cassandra.db.compaction.CompactionsCQLTest.testTriggerMinorCompactionDTCS-compression |  Major | . | Sean McCarthy | Marcus Eriksson |
| [CASSANDRA-13103](https://issues.apache.org/jira/browse/CASSANDRA-13103) | incorrect jvm metric names |  Minor | Observability | Alain Rastoul | Alain Rastoul |
| [CASSANDRA-13020](https://issues.apache.org/jira/browse/CASSANDRA-13020) | Hint delivery fails when prefer\_local enabled |  Major | Streaming and Messaging | Aleksandr Ivanov | Stefan Podkowinski |
| [CASSANDRA-13395](https://issues.apache.org/jira/browse/CASSANDRA-13395) | Expired rows without regular column data can crash upgradesstables |  Major | Local Write-Read Paths | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-13400](https://issues.apache.org/jira/browse/CASSANDRA-13400) | java.lang.ArithmeticException: / by zero when index is created on table with clustering columns only |  Major | Local Write-Read Paths | Andrés de la Peña | Andrés de la Peña |
| [CASSANDRA-13405](https://issues.apache.org/jira/browse/CASSANDRA-13405) | ViewBuilder can miss data due to sstable generation filter |  Major | Local Write-Read Paths, Materialized Views | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-13410](https://issues.apache.org/jira/browse/CASSANDRA-13410) | nodetool upgradesstables/scrub/compact ignores system tables |  Minor | . | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-12213](https://issues.apache.org/jira/browse/CASSANDRA-12213) | dtest failure in write\_failures\_test.TestWriteFailures.test\_paxos\_any |  Major | Distributed Metadata | Craig Kodman | Stefania |
| [CASSANDRA-13364](https://issues.apache.org/jira/browse/CASSANDRA-13364) | Cqlsh COPY fails importing Map\<String,List\<String\>\>, ParseError unhashable type list |  Major | Tools | Nicolae Namolovan | Stefania |
| [CASSANDRA-13413](https://issues.apache.org/jira/browse/CASSANDRA-13413) | Run more test targets on CircleCI |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-13147](https://issues.apache.org/jira/browse/CASSANDRA-13147) | Secondary index query on partition key columns might not return all the rows. |  Major | Secondary Indexes | Benjamin Lerer | Andrés de la Peña |
| [CASSANDRA-13393](https://issues.apache.org/jira/browse/CASSANDRA-13393) | Fix weightedSize() for row-cache reported by JMX and NodeTool |  Minor | Tools | Fuud | Fuud |
| [CASSANDRA-13092](https://issues.apache.org/jira/browse/CASSANDRA-13092) | Debian package does not install the hostpot\_compiler command file |  Trivial | Packaging | Jan Urbański | Michael Shuler |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13059](https://issues.apache.org/jira/browse/CASSANDRA-13059) | dtest failure in upgrade\_tests.upgrade\_through\_versions\_test.TestUpgrade\_current\_2\_1\_x\_To\_indev\_3\_x.rolling\_upgrade\_test |  Major | Testing | Sean McCarthy | Benjamin Lerer |


