
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.0.8 - 2014-05-29



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6487](https://issues.apache.org/jira/browse/CASSANDRA-6487) | Log WARN on large batch sizes |  Minor | . | Patrick McFadin | Lyuben Todorov |
| [CASSANDRA-7047](https://issues.apache.org/jira/browse/CASSANDRA-7047) | TriggerExecutor should group mutations by row key |  Major | . | Sergio Bossa | Aleksey Yeschenko |
| [CASSANDRA-7087](https://issues.apache.org/jira/browse/CASSANDRA-7087) | Use JMX\_PORT for the RMI port to simplify nodetool connectivity |  Minor | Configuration | Chris Lohfink | Chris Lohfink |
| [CASSANDRA-7090](https://issues.apache.org/jira/browse/CASSANDRA-7090) | Add ability to set/get logging levels to nodetool |  Minor | Tools | Jackson Chung | Jackson Chung |
| [CASSANDRA-7072](https://issues.apache.org/jira/browse/CASSANDRA-7072) | Allow better specification of rack properties file |  Minor | Configuration | Patrick Ziegler | Patrick Ziegler |
| [CASSANDRA-7132](https://issues.apache.org/jira/browse/CASSANDRA-7132) | Add a new Snitch for Google Cloud Platform |  Minor | . | Brian Lynch | Brian Lynch |
| [CASSANDRA-7142](https://issues.apache.org/jira/browse/CASSANDRA-7142) | Suggest CTRL-C/D or semicolon after three blank lines in cqlsh |  Trivial | Tools | Tyler Hobbs | Mikhail Stepura |
| [CASSANDRA-7169](https://issues.apache.org/jira/browse/CASSANDRA-7169) | reduce garbage in BatchlogManager.replayMutations |  Minor | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-7242](https://issues.apache.org/jira/browse/CASSANDRA-7242) | More compaction visibility into thread pool and per CF |  Minor | . | Chris Lohfink | Chris Lohfink |
| [CASSANDRA-7147](https://issues.apache.org/jira/browse/CASSANDRA-7147) | Add a new snitch for Apache Cloudstack platforms |  Minor | . | Pierre-Yves Ritschard | Pierre-Yves Ritschard |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6999](https://issues.apache.org/jira/browse/CASSANDRA-6999) | Batchlog replay should account for CF truncation records |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-7018](https://issues.apache.org/jira/browse/CASSANDRA-7018) | cqlsh failing to handle utf8 decode errors in certain cases |  Minor | Tools | Colin B. | Mikhail Stepura |
| [CASSANDRA-7052](https://issues.apache.org/jira/browse/CASSANDRA-7052) | Query on compact storage with limit returns extra rows |  Major | . | Stuart Freeman | Sylvain Lebresne |
| [CASSANDRA-7053](https://issues.apache.org/jira/browse/CASSANDRA-7053) | USING TIMESTAMP for batches does not work |  Major | . | Robert Supencheck | Mikhail Stepura |
| [CASSANDRA-7038](https://issues.apache.org/jira/browse/CASSANDRA-7038) | Nodetool rebuild\_index requires named indexes argument |  Trivial | Tools | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-7055](https://issues.apache.org/jira/browse/CASSANDRA-7055) | Broken CQL Version number in 2.0.7 |  Trivial | . | Michaël Figuière | Sylvain Lebresne |
| [CASSANDRA-7050](https://issues.apache.org/jira/browse/CASSANDRA-7050) | AbstractColumnFamilyInputFormat & AbstractColumnFamilyOutputFormat throw NPE if username is provided but password is null |  Minor | . | Mike Adamson | Mike Adamson |
| [CASSANDRA-6949](https://issues.apache.org/jira/browse/CASSANDRA-6949) | Performance regression in tombstone heavy workloads |  Major | . | Jeremiah Jordan | Sam Tunnicliffe |
| [CASSANDRA-6939](https://issues.apache.org/jira/browse/CASSANDRA-6939) | LOCAL\_SERIAL use QUORAM consistency level to read rows in the read path |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-7058](https://issues.apache.org/jira/browse/CASSANDRA-7058) | HHOM and BM direct delivery should not cause hints to be written on timeout |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6996](https://issues.apache.org/jira/browse/CASSANDRA-6996) | Setting severity via JMX broken |  Minor | Tools | Rick Branson | Vijay |
| [CASSANDRA-7074](https://issues.apache.org/jira/browse/CASSANDRA-7074) | CFMetaData sometimes fails to return the ColumnDefinition given a full internal cell name |  Major | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-6994](https://issues.apache.org/jira/browse/CASSANDRA-6994) | SerializingCache doesn't always cleanup its references in case of exception |  Minor | . | Benedict | Benedict |
| [CASSANDRA-7082](https://issues.apache.org/jira/browse/CASSANDRA-7082) | Nodetool status always displays the first token instead of the number of vnodes |  Minor | Tools | Jivko Donev | Brandon Williams |
| [CASSANDRA-7073](https://issues.apache.org/jira/browse/CASSANDRA-7073) | Plug holes in resource release when wiring up StreamSession |  Minor | . | Benedict | Benedict |
| [CASSANDRA-7091](https://issues.apache.org/jira/browse/CASSANDRA-7091) | java.lang.NullPointerException  at org.apache.cassandra.db.ColumnFamilyStore.scrubDataDirectories(ColumnFamilyStore.java:437) |  Major | . | Vivek Mishra | Vivek Mishra |
| [CASSANDRA-6751](https://issues.apache.org/jira/browse/CASSANDRA-6751) | Setting -Dcassandra.fd\_initial\_value\_ms Results in NPE |  Minor | . | Tyler Hobbs | Dave Brosius |
| [CASSANDRA-6942](https://issues.apache.org/jira/browse/CASSANDRA-6942) | Parameters Were Removed from Tracing Session |  Major | . | Tyler Hobbs | Dave Brosius |
| [CASSANDRA-6546](https://issues.apache.org/jira/browse/CASSANDRA-6546) | disablethrift results in unclosed file descriptors |  Minor | . | Jason Harvey | Mikhail Stepura |
| [CASSANDRA-7081](https://issues.apache.org/jira/browse/CASSANDRA-7081) | select writetime(colname) returns 0 for static columns |  Major | . | Nicolas Favre-Felix | Sylvain Lebresne |
| [CASSANDRA-7126](https://issues.apache.org/jira/browse/CASSANDRA-7126) | relocate doesn't update system.peers correctly |  Blocker | . | T Jake Luciani | Brandon Williams |
| [CASSANDRA-7149](https://issues.apache.org/jira/browse/CASSANDRA-7149) | SimpleCondition await bug |  Minor | . | Jianwei Zhang | Jianwei Zhang |
| [CASSANDRA-6831](https://issues.apache.org/jira/browse/CASSANDRA-6831) | Updates to COMPACT STORAGE tables via cli drop CQL information |  Minor | . | Russell Bradberry | Sylvain Lebresne |
| [CASSANDRA-7143](https://issues.apache.org/jira/browse/CASSANDRA-7143) | shuffle broken on 2.0 |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-7177](https://issues.apache.org/jira/browse/CASSANDRA-7177) | Starting threads in the OutboundTcpConnectionPool constructor causes race conditions |  Major | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-7187](https://issues.apache.org/jira/browse/CASSANDRA-7187) | Disabling compaction via schema is broken |  Minor | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-7228](https://issues.apache.org/jira/browse/CASSANDRA-7228) | Closing CqlRecordReader doesn't release all resources |  Minor | . | Mariusz Kryński | Mariusz Kryński |
| [CASSANDRA-7221](https://issues.apache.org/jira/browse/CASSANDRA-7221) | CqlRecordReader does not work with Password authentication |  Major | . | Jacek Lewandowski | Jacek Lewandowski |
| [CASSANDRA-7155](https://issues.apache.org/jira/browse/CASSANDRA-7155) | Followup to 6914: null handling, duplicate column in resultSet and cleanup |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6285](https://issues.apache.org/jira/browse/CASSANDRA-6285) | 2.0 HSHA server introduces corrupt data |  Critical | . | David Sauer | Pavel Yaskevich |
| [CASSANDRA-7100](https://issues.apache.org/jira/browse/CASSANDRA-7100) | Regression in 2.0.x - Cql3 reader returns duplicate rows if the cluster column is reversed |  Major | . | Rohit Rai | Alex Liu |
| [CASSANDRA-7093](https://issues.apache.org/jira/browse/CASSANDRA-7093) | ConfigHelper.setInputColumnFamily incompatible with upper-cased keyspaces since 2.0.7 |  Major | . | Maxime Nay | Alex Liu |
| [CASSANDRA-6950](https://issues.apache.org/jira/browse/CASSANDRA-6950) | Secondary index query fails with tc range query when ordered by DESC |  Major | Secondary Indexes | Andre Campeau | Sylvain Lebresne |
| [CASSANDRA-6525](https://issues.apache.org/jira/browse/CASSANDRA-6525) | Cannot select data which using "WHERE" |  Major | . | Silence Chow | Tyler Hobbs |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7084](https://issues.apache.org/jira/browse/CASSANDRA-7084) | o.a.c.db.RecoveryManagerTest.testNothingToRecover Unit Test Flaps in 2.0 |  Minor | Testing | Michael Shuler | Benedict |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7123](https://issues.apache.org/jira/browse/CASSANDRA-7123) | BATCH documentation should be explicit about ordering guarantees |  Minor | Documentation and Website | Tyler Hobbs | Tyler Hobbs |


