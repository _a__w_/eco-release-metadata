
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.2.17 - 2014-06-30



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6344](https://issues.apache.org/jira/browse/CASSANDRA-6344) | When running CQLSH with file input, exit with error status code if script fails |  Major | Tools | Branden Visser | Mikhail Stepura |
| [CASSANDRA-7273](https://issues.apache.org/jira/browse/CASSANDRA-7273) | expose global ColumnFamily metrics |  Minor | . | Richard Wagner | Chris Lohfink |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6972](https://issues.apache.org/jira/browse/CASSANDRA-6972) | Throw an ERROR when auto\_bootstrap: true and bootstrapping node is listed in seeds |  Minor | . | Darla Baker | Brandon Williams |
| [CASSANDRA-7134](https://issues.apache.org/jira/browse/CASSANDRA-7134) | Add a new Snitch for Google Cloud Platform - V1.2 Branch |  Minor | . | Brian Lynch | Brian Lynch |
| [CASSANDRA-7182](https://issues.apache.org/jira/browse/CASSANDRA-7182) | no need to query for local tokens twice in a row |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-7063](https://issues.apache.org/jira/browse/CASSANDRA-7063) | Raise the streaming phi threshold check in 1.2 |  Minor | . | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-7191](https://issues.apache.org/jira/browse/CASSANDRA-7191) | reduce needless garbage/thrashing in pending range calculation |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-7257](https://issues.apache.org/jira/browse/CASSANDRA-7257) | only query once for local host id on join |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-7333](https://issues.apache.org/jira/browse/CASSANDRA-7333) | gossipinfo should include the generation |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-6539](https://issues.apache.org/jira/browse/CASSANDRA-6539) | Track metrics at a keyspace level as well as column family level |  Minor | . | Nick Bailey | Brandon Williams |
| [CASSANDRA-7266](https://issues.apache.org/jira/browse/CASSANDRA-7266) | Allow cqlsh shell ignore .cassandra permission errors and not fail to open the cqlsh shell. |  Minor | Tools | Carolyn Jung |  |
| [CASSANDRA-7356](https://issues.apache.org/jira/browse/CASSANDRA-7356) | Add a more ops friendly replace\_address flag |  Major | . | Richard Low | Brandon Williams |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6822](https://issues.apache.org/jira/browse/CASSANDRA-6822) | Never ending batch replay after dropping column family |  Major | . | Duncan Sands | Aleksey Yeschenko |
| [CASSANDRA-6787](https://issues.apache.org/jira/browse/CASSANDRA-6787) | assassinate should continue when the endpoint vanishes |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-5995](https://issues.apache.org/jira/browse/CASSANDRA-5995) | Cassandra-shuffle causes NumberFormatException |  Major | Tools | William Montaz | Lyuben Todorov |
| [CASSANDRA-6920](https://issues.apache.org/jira/browse/CASSANDRA-6920) | LatencyMetrics can return infinity |  Major | . | Nick Bailey | Benedict |
| [CASSANDRA-6971](https://issues.apache.org/jira/browse/CASSANDRA-6971) | Schedule schema pulls onChange |  Major | . | Russ Hatch | Brandon Williams |
| [CASSANDRA-6980](https://issues.apache.org/jira/browse/CASSANDRA-6980) | Non-droppable verbs shouldn't be dropped from OTC |  Major | . | Richard Low | Jason Brown |
| [CASSANDRA-7025](https://issues.apache.org/jira/browse/CASSANDRA-7025) | RejectedExecutionException when stopping a node after drain |  Trivial | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-6999](https://issues.apache.org/jira/browse/CASSANDRA-6999) | Batchlog replay should account for CF truncation records |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-7018](https://issues.apache.org/jira/browse/CASSANDRA-7018) | cqlsh failing to handle utf8 decode errors in certain cases |  Minor | Tools | Colin B. | Mikhail Stepura |
| [CASSANDRA-7038](https://issues.apache.org/jira/browse/CASSANDRA-7038) | Nodetool rebuild\_index requires named indexes argument |  Trivial | Tools | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-6841](https://issues.apache.org/jira/browse/CASSANDRA-6841) | ConcurrentModificationException in commit-log-writer after local schema reset |  Minor | . | Pas | Benedict |
| [CASSANDRA-7058](https://issues.apache.org/jira/browse/CASSANDRA-7058) | HHOM and BM direct delivery should not cause hints to be written on timeout |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6476](https://issues.apache.org/jira/browse/CASSANDRA-6476) | Assertion error in MessagingService.addCallback |  Major | . | Theo Hultberg | Brandon Williams |
| [CASSANDRA-6994](https://issues.apache.org/jira/browse/CASSANDRA-6994) | SerializingCache doesn't always cleanup its references in case of exception |  Minor | . | Benedict | Benedict |
| [CASSANDRA-7082](https://issues.apache.org/jira/browse/CASSANDRA-7082) | Nodetool status always displays the first token instead of the number of vnodes |  Minor | Tools | Jivko Donev | Brandon Williams |
| [CASSANDRA-6751](https://issues.apache.org/jira/browse/CASSANDRA-6751) | Setting -Dcassandra.fd\_initial\_value\_ms Results in NPE |  Minor | . | Tyler Hobbs | Dave Brosius |
| [CASSANDRA-6546](https://issues.apache.org/jira/browse/CASSANDRA-6546) | disablethrift results in unclosed file descriptors |  Minor | . | Jason Harvey | Mikhail Stepura |
| [CASSANDRA-7126](https://issues.apache.org/jira/browse/CASSANDRA-7126) | relocate doesn't update system.peers correctly |  Blocker | . | T Jake Luciani | Brandon Williams |
| [CASSANDRA-7149](https://issues.apache.org/jira/browse/CASSANDRA-7149) | SimpleCondition await bug |  Minor | . | Jianwei Zhang | Jianwei Zhang |
| [CASSANDRA-6831](https://issues.apache.org/jira/browse/CASSANDRA-6831) | Updates to COMPACT STORAGE tables via cli drop CQL information |  Minor | . | Russell Bradberry | Sylvain Lebresne |
| [CASSANDRA-7122](https://issues.apache.org/jira/browse/CASSANDRA-7122) | Replacement nodes have null entries in system.peers |  Major | . | Richard Low | Brandon Williams |
| [CASSANDRA-7170](https://issues.apache.org/jira/browse/CASSANDRA-7170) | socket should enable keepalive in MessagingService.SocketThread |  Minor | . | Jianwei Zhang | Jianwei Zhang |
| [CASSANDRA-6562](https://issues.apache.org/jira/browse/CASSANDRA-6562) | sstablemetadata{.bat} location inconsistency |  Trivial | Tools | Chris Burroughs | Brandon Williams |
| [CASSANDRA-7105](https://issues.apache.org/jira/browse/CASSANDRA-7105) | SELECT with IN on final column of composite and compound primary key fails |  Major | . | Bill Mitchell | Sylvain Lebresne |
| [CASSANDRA-7319](https://issues.apache.org/jira/browse/CASSANDRA-7319) | Fix availability validation for LOCAL\_ONE CL |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-7144](https://issues.apache.org/jira/browse/CASSANDRA-7144) | Fix handling of empty counter replication mutations |  Major | . | Maxime Lamothe-Brassard | Aleksey Yeschenko |
| [CASSANDRA-7336](https://issues.apache.org/jira/browse/CASSANDRA-7336) | RepairTask doesn't send a correct message in a JMX notifcation in case of IllegalArgumentException |  Minor | . | Mikhail Stepura | Mikhail Stepura |
| [CASSANDRA-6788](https://issues.apache.org/jira/browse/CASSANDRA-6788) | Race condition silently kills thrift server |  Major | . | Christian Rolf | Christian Rolf |
| [CASSANDRA-7380](https://issues.apache.org/jira/browse/CASSANDRA-7380) | Native protocol needs keepalive, we should add it |  Major | . | Jose Martinez Poblete | Brandon Williams |
| [CASSANDRA-7268](https://issues.apache.org/jira/browse/CASSANDRA-7268) | Secondary Index can miss data without an error |  Major | Secondary Indexes | Jeremiah Jordan | Sam Tunnicliffe |
| [CASSANDRA-7318](https://issues.apache.org/jira/browse/CASSANDRA-7318) | Unable to truncate column family on node which has been decommissioned and re-bootstrapped |  Minor | . | Thomas Whiteway | Brandon Williams |
| [CASSANDRA-7307](https://issues.apache.org/jira/browse/CASSANDRA-7307) | New nodes mark dead nodes as up for 10 minutes |  Major | . | Richard Low | Brandon Williams |
| [CASSANDRA-7407](https://issues.apache.org/jira/browse/CASSANDRA-7407) | COPY command does not work properly with collections causing failure to import data |  Major | . | Jose Martinez Poblete | Mikhail Stepura |
| [CASSANDRA-7373](https://issues.apache.org/jira/browse/CASSANDRA-7373) | Commit logs no longer deleting and MemtablePostFlusher pending growing |  Major | . | Francois Richard | Mikhail Stepura |
| [CASSANDRA-7399](https://issues.apache.org/jira/browse/CASSANDRA-7399) | cqlsh: describe table shows wrong data type for CompositeType |  Major | Tools | Robert Stupp | Mikhail Stepura |


