
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.0 beta 2 - 2013-07-25



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5435](https://issues.apache.org/jira/browse/CASSANDRA-5435) | Support range tombstones from thrift |  Minor | CQL | Edward Capriolo | Edward Capriolo |
| [CASSANDRA-5765](https://issues.apache.org/jira/browse/CASSANDRA-5765) | add an lz4 implementation of FrameCompressor for lz4 compressed native cql protocol |  Minor | CQL | Aaron Daubman | Sylvain Lebresne |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5744](https://issues.apache.org/jira/browse/CASSANDRA-5744) | Cleanup AbstractType/TypeSerializer classes |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5677](https://issues.apache.org/jira/browse/CASSANDRA-5677) | Performance improvements of RangeTombstones/IntervalTree |  Minor | . | Fabien Rousseau | Sylvain Lebresne |
| [CASSANDRA-5759](https://issues.apache.org/jira/browse/CASSANDRA-5759) | Consistencyfy CQL3 create/alter/drop column family/table statement class names |  Trivial | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-4686](https://issues.apache.org/jira/browse/CASSANDRA-4686) | update hadoop version in wordcount exampe |  Trivial | . | Philip Crotwell | Philip Crotwell |
| [CASSANDRA-5773](https://issues.apache.org/jira/browse/CASSANDRA-5773) | print cluster name in describe cluster |  Trivial | . | Philip Crotwell | Philip Crotwell |
| [CASSANDRA-5555](https://issues.apache.org/jira/browse/CASSANDRA-5555) | Allow sstableloader to handle a larger number of files |  Major | Tools | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-5715](https://issues.apache.org/jira/browse/CASSANDRA-5715) | CAS on 'primary key only' table |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5772](https://issues.apache.org/jira/browse/CASSANDRA-5772) | Support streaming of SSTables created in older version's format |  Minor | Streaming and Messaging | Yuki Morishita | Yuki Morishita |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5171](https://issues.apache.org/jira/browse/CASSANDRA-5171) | Save EC2Snitch topology information in system table |  Critical | . | Vijay | Vijay |
| [CASSANDRA-5573](https://issues.apache.org/jira/browse/CASSANDRA-5573) | Querying with an empty (impossible) range returns incorrect results |  Major | . | Mike Schrag | Sylvain Lebresne |
| [CASSANDRA-5756](https://issues.apache.org/jira/browse/CASSANDRA-5756) | shuffle disable subcommand not recognised |  Major | Tools | Radim Kolar | Dave Brosius |
| [CASSANDRA-5753](https://issues.apache.org/jira/browse/CASSANDRA-5753) | cassandra-shuffle is not available for windows |  Trivial | Tools | Radim Kolar | Jonathan Ellis |
| [CASSANDRA-5749](https://issues.apache.org/jira/browse/CASSANDRA-5749) | DESC TABLE omits some column family settings |  Minor | . | Ryan McGuire | Aleksey Yeschenko |
| [CASSANDRA-5764](https://issues.apache.org/jira/browse/CASSANDRA-5764) | CommitLogReplayer should calculate checksums differently for \< 2.0 |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-5704](https://issues.apache.org/jira/browse/CASSANDRA-5704) | add cassandra.unsafesystem property (Truncate flushes to disk again in 1.2, even with durable\_writes=false) |  Minor | . | Christian Spriegel | Jonathan Ellis |
| [CASSANDRA-5757](https://issues.apache.org/jira/browse/CASSANDRA-5757) | assertionError in repair |  Major | . | Radim Kolar | Sylvain Lebresne |
| [CASSANDRA-5542](https://issues.apache.org/jira/browse/CASSANDRA-5542) | BulkLoader is broken in trunk |  Major | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-5746](https://issues.apache.org/jira/browse/CASSANDRA-5746) | HHOM.countPendingHints is a trap for the unwary |  Major | Tools | Jonathan Ellis | Tyler Hobbs |
| [CASSANDRA-5768](https://issues.apache.org/jira/browse/CASSANDRA-5768) | If a Seed can't be contacted, a new node comes up as a cluster of 1 |  Minor | . | Andy Cobley | Brandon Williams |
| [CASSANDRA-5770](https://issues.apache.org/jira/browse/CASSANDRA-5770) | Minor bugs in the native protocol v2 on 2.0.0-beta1 |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5778](https://issues.apache.org/jira/browse/CASSANDRA-5778) | Native protocol event don't respect the protocol version |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5786](https://issues.apache.org/jira/browse/CASSANDRA-5786) | Thrift cas() method crashes if input columns are not sorted. |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5787](https://issues.apache.org/jira/browse/CASSANDRA-5787) | Commit#updatesWithPaxosTime should also update the row and range tombstones |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5782](https://issues.apache.org/jira/browse/CASSANDRA-5782) | ConcurrentModificationException during streaming |  Major | . | Brandon Williams | Sylvain Lebresne |
| [CASSANDRA-5788](https://issues.apache.org/jira/browse/CASSANDRA-5788) | StorageProxy#cas() doesn't order columns names correctly when querying |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5775](https://issues.apache.org/jira/browse/CASSANDRA-5775) | Fix broken streaming retry |  Major | . | Yuki Morishita | Yuki Morishita |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5574](https://issues.apache.org/jira/browse/CASSANDRA-5574) | Add trigger examples |  Trivial | . | Vijay | Vijay |


