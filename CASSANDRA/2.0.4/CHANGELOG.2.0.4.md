
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.0.4 - 2013-12-30



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6395](https://issues.apache.org/jira/browse/CASSANDRA-6395) | Add the ability to query by TimeUUIDs with milisecond granularity |  Minor | . | Lorcan Coyle | Lorcan Coyle |
| [CASSANDRA-6218](https://issues.apache.org/jira/browse/CASSANDRA-6218) | Repair should allow repairing particular data centers to reduce WAN usage |  Minor | Tools | sankalp kohli | Jimmy Mårdell |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5598](https://issues.apache.org/jira/browse/CASSANDRA-5598) | Need invalid exception when submitting a prepared statement with too many markers |  Minor | . | Anne Sullivan | Lyuben Todorov |
| [CASSANDRA-6391](https://issues.apache.org/jira/browse/CASSANDRA-6391) | Expose a total memtable size metric for a CF |  Trivial | . | Nick Bailey | Aleksey Yeschenko |
| [CASSANDRA-6425](https://issues.apache.org/jira/browse/CASSANDRA-6425) | cqlsh cannot found cql driver when invoked through link |  Trivial | Tools | Yinyin L | Yinyin L |
| [CASSANDRA-6410](https://issues.apache.org/jira/browse/CASSANDRA-6410) | gossip memory usage improvement |  Minor | . | Quentin Conner | Jonathan Ellis |
| [CASSANDRA-6268](https://issues.apache.org/jira/browse/CASSANDRA-6268) | Poor performance of Hadoop if any DC is using VNodes |  Major | . | Piotr Kołaczkowski | Piotr Kołaczkowski |
| [CASSANDRA-6404](https://issues.apache.org/jira/browse/CASSANDRA-6404) | Tools emit ERRORs and WARNINGs about missing javaagent |  Minor | Tools | Sam Tunnicliffe | Sam Tunnicliffe |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6289](https://issues.apache.org/jira/browse/CASSANDRA-6289) | Murmur3Partitioner doesn't yield proper ownership calculation |  Minor | Tools | Jackson Chung | Jonathan Ellis |
| [CASSANDRA-6396](https://issues.apache.org/jira/browse/CASSANDRA-6396) | debian init searches for jdk6 explicitly |  Minor | Packaging | Brandon Williams | Brandon Williams |
| [CASSANDRA-6284](https://issues.apache.org/jira/browse/CASSANDRA-6284) | Wrong tracking of minLevel in Leveled Compaction Strategy causing serious performance problems |  Major | . | Jiri Horky | Jiri Horky |
| [CASSANDRA-6172](https://issues.apache.org/jira/browse/CASSANDRA-6172) | COPY TO command doesn't escape single quote in collections |  Minor | Tools | Ivan Mykhailov | Mikhail Stepura |
| [CASSANDRA-6345](https://issues.apache.org/jira/browse/CASSANDRA-6345) | Endpoint cache invalidation causes CPU spike (on vnode rings?) |  Major | . | Rick Branson | Jonathan Ellis |
| [CASSANDRA-6409](https://issues.apache.org/jira/browse/CASSANDRA-6409) | gossip performance improvement at node startup |  Major | . | Quentin Conner | Jonathan Ellis |
| [CASSANDRA-6415](https://issues.apache.org/jira/browse/CASSANDRA-6415) | Snapshot repair blocks for ever if something happens to the "I made my snapshot" response |  Major | . | Jeremiah Jordan | Yuki Morishita |
| [CASSANDRA-6413](https://issues.apache.org/jira/browse/CASSANDRA-6413) | Saved KeyCache prints success to log; but no file present |  Minor | . | Chris Burroughs | Mikhail Stepura |
| [CASSANDRA-6416](https://issues.apache.org/jira/browse/CASSANDRA-6416) | MoveTest fails in 1.2+ |  Major | Testing | Jonathan Ellis | Michael Shuler |
| [CASSANDRA-6427](https://issues.apache.org/jira/browse/CASSANDRA-6427) | Counter writes shouldn't be resubmitted after timeouts |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6459](https://issues.apache.org/jira/browse/CASSANDRA-6459) | Concurrency issue in Directories.getOrCreate() |  Major | . | Ryan Fowler | Ryan Fowler |
| [CASSANDRA-6462](https://issues.apache.org/jira/browse/CASSANDRA-6462) | cleanup ClassCastException |  Major | Tools | Andreas Schnitzerling | Jonathan Ellis |
| [CASSANDRA-6481](https://issues.apache.org/jira/browse/CASSANDRA-6481) | Batchlog endpoint candidates should be picked randomly, not sorted by proximity |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6309](https://issues.apache.org/jira/browse/CASSANDRA-6309) | Pig CqlStorage generates  ERROR 1108: Duplicate schema alias |  Major | . | Thunder Stumpges | Alex Liu |
| [CASSANDRA-6420](https://issues.apache.org/jira/browse/CASSANDRA-6420) | CassandraStorage should not assume all DataBags are DefaultDataBags |  Major | . | Mike Spertus | Mike Spertus |
| [CASSANDRA-6483](https://issues.apache.org/jira/browse/CASSANDRA-6483) | Possible Collections.sort assertion failure in STCS.filterColdSSTables |  Major | . | graham sanderson | Tyler Hobbs |
| [CASSANDRA-6485](https://issues.apache.org/jira/browse/CASSANDRA-6485) | NPE in calculateNaturalEndpoints |  Major | . | Russell Spitzer | Jonathan Ellis |
| [CASSANDRA-6488](https://issues.apache.org/jira/browse/CASSANDRA-6488) | Batchlog writes consume unnecessarily large amounts of CPU on vnodes clusters |  Major | . | Rick Branson | Rick Branson |
| [CASSANDRA-6496](https://issues.apache.org/jira/browse/CASSANDRA-6496) | Endless L0 LCS compactions |  Major | . | Nikolai Grigoriev | Jonathan Ellis |
| [CASSANDRA-6468](https://issues.apache.org/jira/browse/CASSANDRA-6468) | Runaway thread for SSL socket |  Minor | . | Mikhail Stepura | Mikhail Stepura |
| [CASSANDRA-6464](https://issues.apache.org/jira/browse/CASSANDRA-6464) | Paging queries with IN on the partition key is broken |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6378](https://issues.apache.org/jira/browse/CASSANDRA-6378) | sstableloader does not support client encryption on Cassandra 2.0 |  Major | . | David Laube | Sam Tunnicliffe |
| [CASSANDRA-6510](https://issues.apache.org/jira/browse/CASSANDRA-6510) | Don't drop local mutations without a hint |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6419](https://issues.apache.org/jira/browse/CASSANDRA-6419) | Setting max\_hint\_window\_in\_ms explicitly to null causes problems with JMX view |  Minor | Configuration | Nate McCall | Nate McCall |
| [CASSANDRA-6447](https://issues.apache.org/jira/browse/CASSANDRA-6447) | SELECT someColumns FROM table results in AssertionError in AbstractQueryPager.discardFirst |  Major | . | Julien Aymé | Sylvain Lebresne |
| [CASSANDRA-6521](https://issues.apache.org/jira/browse/CASSANDRA-6521) | Thrift should validate SliceRange start and finish lengths |  Major | . | Ben Bromhead | Ben Bromhead |
| [CASSANDRA-6527](https://issues.apache.org/jira/browse/CASSANDRA-6527) | Random tombstones after adding a CF with sstableloader |  Critical | . | Bartłomiej Romański | Yuki Morishita |
| [CASSANDRA-6669](https://issues.apache.org/jira/browse/CASSANDRA-6669) | NullPointerException(s) on startup if StorageServiceShutdownHook fails |  Minor | . | Andre Campeau |  |
| [CASSANDRA-6403](https://issues.apache.org/jira/browse/CASSANDRA-6403) | Division by zero Exception in HintedHandoff and CompactionExecutor |  Major | . | Gabriel Wicke |  |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4899](https://issues.apache.org/jira/browse/CASSANDRA-4899) | add gitignore |  Trivial | . | Radim Kolar | Michael Shuler |


