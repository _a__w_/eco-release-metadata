
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.4 - 2009-09-27



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-172](https://issues.apache.org/jira/browse/CASSANDRA-172) | A improved and more general version of get\_slice |  Major | . | Jun Rao | Jun Rao |
| [CASSANDRA-53](https://issues.apache.org/jira/browse/CASSANDRA-53) | Add new call to API: get\_slice\_by\_name\_range |  Major | . | Sandeep Tata | Sandeep Tata |
| [CASSANDRA-79](https://issues.apache.org/jira/browse/CASSANDRA-79) | Multi-table support |  Critical | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-259](https://issues.apache.org/jira/browse/CASSANDRA-259) | LRU cache for key positions |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-272](https://issues.apache.org/jira/browse/CASSANDRA-272) | Enhance how metrics are exposed |  Minor | Tools | Sammy Yu | Sammy Yu |
| [CASSANDRA-253](https://issues.apache.org/jira/browse/CASSANDRA-253) | add capability to query CFS attributes to nodeprobe |  Minor | Tools | Eric Evans | Eric Evans |
| [CASSANDRA-284](https://issues.apache.org/jira/browse/CASSANDRA-284) | ant target(s) for thrift code generation |  Minor | Tools | Eric Evans | Michael Greene |
| [CASSANDRA-146](https://issues.apache.org/jira/browse/CASSANDRA-146) | Better windows integration |  Minor | Tools | Alexander Vushkan | Michael Greene |
| [CASSANDRA-302](https://issues.apache.org/jira/browse/CASSANDRA-302) | check user input for comparator sanity |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-185](https://issues.apache.org/jira/browse/CASSANDRA-185) | user-defined column ordering |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-279](https://issues.apache.org/jira/browse/CASSANDRA-279) | finish snapshot support |  Major | . | Jonathan Ellis | Sammy Yu |
| [CASSANDRA-232](https://issues.apache.org/jira/browse/CASSANDRA-232) | Interface does not allow specification of weak vs quorum reads |  Major | . | Sandeep Tata | Sammy Yu |
| [CASSANDRA-323](https://issues.apache.org/jira/browse/CASSANDRA-323) | Make Strategy and the EndpointSnitch pluggable |  Major | . | Sammy Yu | Sammy Yu |
| [CASSANDRA-213](https://issues.apache.org/jira/browse/CASSANDRA-213) | complete bootstrap code |  Critical | . | Jonathan Ellis | Sandeep Tata |
| [CASSANDRA-360](https://issues.apache.org/jira/browse/CASSANDRA-360) | Expose out each threadpool's pendingtasks via nodeprobe |  Major | Tools | Sammy Yu | Sammy Yu |
| [CASSANDRA-301](https://issues.apache.org/jira/browse/CASSANDRA-301) | Support a maven release |  Major | Tools | Hiram Chirino | Hiram Chirino |
| [CASSANDRA-369](https://issues.apache.org/jira/browse/CASSANDRA-369) | Expose commitlog queue size |  Major | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-382](https://issues.apache.org/jira/browse/CASSANDRA-382) | Enhance nodeprobe ring command to display right side of the range |  Major | Tools | Sammy Yu | Sammy Yu |
| [CASSANDRA-242](https://issues.apache.org/jira/browse/CASSANDRA-242) | Implement method to "evenly" split a Range |  Major | . | Stu Hood | Stu Hood |
| [CASSANDRA-422](https://issues.apache.org/jira/browse/CASSANDRA-422) | Add example of EndPointSnitch to contrib |  Major | . | Sammy Yu | Sammy Yu |
| [CASSANDRA-505](https://issues.apache.org/jira/browse/CASSANDRA-505) | turn nodeprobe flush\_binary into nodeprobe flush |  Major | Tools | Jonathan Ellis | Gary Dusbabek |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-152](https://issues.apache.org/jira/browse/CASSANDRA-152) | allow restricting get\_key\_range to a subset of columnfamilies |  Critical | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-115](https://issues.apache.org/jira/browse/CASSANDRA-115) | Make cassandra crash-only |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-198](https://issues.apache.org/jira/browse/CASSANDRA-198) | r/m deprecated getColumnFamilyMap |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-218](https://issues.apache.org/jira/browse/CASSANDRA-218) | exit if OOM occurs |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-228](https://issues.apache.org/jira/browse/CASSANDRA-228) | Improve the readability of the DBManager.instance() method |  Major | . | Edward Ribeiro | Edward Ribeiro |
| [CASSANDRA-188](https://issues.apache.org/jira/browse/CASSANDRA-188) | raise error if supercolumn is configured as time-sorted |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-202](https://issues.apache.org/jira/browse/CASSANDRA-202) | Refactor different getRow calls in Table.java to more specific name and add more logging information |  Major | . | Jun Rao | Jun Rao |
| [CASSANDRA-36](https://issues.apache.org/jira/browse/CASSANDRA-36) | Log runtime stats for analysis by ops |  Major | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-244](https://issues.apache.org/jira/browse/CASSANDRA-244) | Innefficient use of String constructor |  Major | . | Edward Ribeiro | Edward Ribeiro |
| [CASSANDRA-246](https://issues.apache.org/jira/browse/CASSANDRA-246) | Efficient way of creating wrapper classes |  Major | . | Edward Ribeiro | Edward Ribeiro |
| [CASSANDRA-243](https://issues.apache.org/jira/browse/CASSANDRA-243) | more commitlogheader improvement |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-224](https://issues.apache.org/jira/browse/CASSANDRA-224) | Encapsulate SSTable, SequenceFile better |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-254](https://issues.apache.org/jira/browse/CASSANDRA-254) | clean up sstable constructors |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-249](https://issues.apache.org/jira/browse/CASSANDRA-249) | Add unit test for gms package |  Trivial | . | Johan Oskarsson | Johan Oskarsson |
| [CASSANDRA-211](https://issues.apache.org/jira/browse/CASSANDRA-211) | web ui sucks |  Major | . | Jonathan Ellis | Eric Evans |
| [CASSANDRA-258](https://issues.apache.org/jira/browse/CASSANDRA-258) | Enhance describeTable to return Map of Column Family so that it can be programatically parsed |  Minor | Tools | Sammy Yu | Sammy Yu |
| [CASSANDRA-273](https://issues.apache.org/jira/browse/CASSANDRA-273) | Collection of smalls improvements |  Minor | . | Edward Ribeiro | Edward Ribeiro |
| [CASSANDRA-251](https://issues.apache.org/jira/browse/CASSANDRA-251) | Restore ColumnSort=Time on Super Columnfamilies |  Major | . | Evan Weaver | Jonathan Ellis |
| [CASSANDRA-277](https://issues.apache.org/jira/browse/CASSANDRA-277) | combine \_range slice methods into main ones |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-173](https://issues.apache.org/jira/browse/CASSANDRA-173) | add getPendingTasks to CFSMBean |  Minor | . | Jonathan Ellis | Eric Evans |
| [CASSANDRA-290](https://issues.apache.org/jira/browse/CASSANDRA-290) | Superfluous .jar files should be removed |  Minor | . | Ian Eure | Ian Eure |
| [CASSANDRA-274](https://issues.apache.org/jira/browse/CASSANDRA-274) | bin/cassandra-cli enhancement |  Minor | Tools | Anthony Molinaro | Eric Evans |
| [CASSANDRA-74](https://issues.apache.org/jira/browse/CASSANDRA-74) | Random port (5555) hardcoded |  Minor | . | Per Mellqvist | Jonathan Ellis |
| [CASSANDRA-217](https://issues.apache.org/jira/browse/CASSANDRA-217) | remove old get\_slice; replace with get\_slice\_from |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-139](https://issues.apache.org/jira/browse/CASSANDRA-139) | thrift API should use lists instead of colon-delimited strings to specify column path |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-276](https://issues.apache.org/jira/browse/CASSANDRA-276) | use subdirectory-per-table for data files |  Major | . | Jonathan Ellis | Arin Sarkissian |
| [CASSANDRA-303](https://issues.apache.org/jira/browse/CASSANDRA-303) | allow get\_slice to operate on SC subcolumns |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-310](https://issues.apache.org/jira/browse/CASSANDRA-310) | Add perl namespace to thrift interface |  Major | . | Anthony Molinaro | Anthony Molinaro |
| [CASSANDRA-240](https://issues.apache.org/jira/browse/CASSANDRA-240) | Remove time-sorted columns |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-288](https://issues.apache.org/jira/browse/CASSANDRA-288) | split thrift listening config from gossip ListenAddress |  Major | . | Ian Eure | Eric Evans |
| [CASSANDRA-312](https://issues.apache.org/jira/browse/CASSANDRA-312) | All Thrift methods should follow Cassandra's naming conventions |  Minor | . | Michael Greene | Michael Greene |
| [CASSANDRA-300](https://issues.apache.org/jira/browse/CASSANDRA-300) | Enumerate useful consistency options for read/write calls |  Major | . | Michael Greene | Sammy Yu |
| [CASSANDRA-174](https://issues.apache.org/jira/browse/CASSANDRA-174) | reduce logging overhead |  Critical | . | Jonathan Ellis | Chris Goffinet |
| [CASSANDRA-271](https://issues.apache.org/jira/browse/CASSANDRA-271) | Rename \<table\> to \<keyspace\> |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-330](https://issues.apache.org/jira/browse/CASSANDRA-330) | encapsulate SequenceFile, AbstractWriter better |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-334](https://issues.apache.org/jira/browse/CASSANDRA-334) | Temporally Ordered Sub columns in a SuperColumnFamily |  Major | . | Michael Koziarski | Jonathan Ellis |
| [CASSANDRA-346](https://issues.apache.org/jira/browse/CASSANDRA-346) | Improve the speed of RandomPartitioner comparator which will help flushing of BMT and compaction |  Major | . | Sammy Yu | Sammy Yu |
| [CASSANDRA-332](https://issues.apache.org/jira/browse/CASSANDRA-332) | Clean up SSTableSliceIterator to not echo data around DataOutput/Inputs |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-339](https://issues.apache.org/jira/browse/CASSANDRA-339) | Better out-of-the-box defaults for performance |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-329](https://issues.apache.org/jira/browse/CASSANDRA-329) | combine thrift slice methods |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-296](https://issues.apache.org/jira/browse/CASSANDRA-296) | remove CQL |  Major | . | Jonathan Ellis | Eric Evans |
| [CASSANDRA-325](https://issues.apache.org/jira/browse/CASSANDRA-325) | Column bloomfilter not exploited |  Major | . | Jun Rao | Jonathan Ellis |
| [CASSANDRA-311](https://issues.apache.org/jira/browse/CASSANDRA-311) | replace\`isAscending\` bool with \`reversed\` |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-355](https://issues.apache.org/jira/browse/CASSANDRA-355) | add tuneable for read buffer sizes |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-233](https://issues.apache.org/jira/browse/CASSANDRA-233) | Serializing the CF name to the sstable for every key is unnecessary |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-358](https://issues.apache.org/jira/browse/CASSANDRA-358) | SystemTable.initMetadata throws an NPE when called twice |  Major | . | Jeff Hodges | Bill de hOra |
| [CASSANDRA-195](https://issues.apache.org/jira/browse/CASSANDRA-195) | Improve bootstrap algorithm |  Major | . | Sandeep Tata | Sandeep Tata |
| [CASSANDRA-364](https://issues.apache.org/jira/browse/CASSANDRA-364) | clean up systemtable.java |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-376](https://issues.apache.org/jira/browse/CASSANDRA-376) | sanity check start, finish slice args |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-388](https://issues.apache.org/jira/browse/CASSANDRA-388) | merge batch\_insert, batch\_insert\_super |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-387](https://issues.apache.org/jira/browse/CASSANDRA-387) | Upgrade to Thrift 2009-08-19+ and introduce dependency on slf4j |  Major | . | Michael Greene | Jonathan Ellis |
| [CASSANDRA-402](https://issues.apache.org/jira/browse/CASSANDRA-402) | Add mbean to adjust log4j output level at runtime |  Major | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-407](https://issues.apache.org/jira/browse/CASSANDRA-407) | Add maxThreshold to minor compaction |  Major | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-419](https://issues.apache.org/jira/browse/CASSANDRA-419) | Set the name for unnamed threads to ease debugging |  Major | . | Sammy Yu | Sammy Yu |
| [CASSANDRA-450](https://issues.apache.org/jira/browse/CASSANDRA-450) | update 0.4 CHANGES |  Minor | Documentation and Website | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-504](https://issues.apache.org/jira/browse/CASSANDRA-504) | Reduce GC overhead |  Minor | . | Brandon Williams | Brandon Williams |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-141](https://issues.apache.org/jira/browse/CASSANDRA-141) | forceFlush skips flush when there are pending operations |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-184](https://issues.apache.org/jira/browse/CASSANDRA-184) | intermittent OneCompactionTest failure |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-201](https://issues.apache.org/jira/browse/CASSANDRA-201) | get\_slice\_from forces iterating all columns and leaks file handlers with exception |  Major | . | Jun Rao | Jun Rao |
| [CASSANDRA-209](https://issues.apache.org/jira/browse/CASSANDRA-209) | Trunk build set to wrong version |  Blocker | Tools | Johan Oskarsson | Johan Oskarsson |
| [CASSANDRA-214](https://issues.apache.org/jira/browse/CASSANDRA-214) | Incorrect parameter names in Thrift interface |  Minor | . | Thorsten von Eicken | Jonathan Ellis |
| [CASSANDRA-208](https://issues.apache.org/jira/browse/CASSANDRA-208) | OOM intermittently during compaction |  Critical | . | Jiansheng Huang | Jonathan Ellis |
| [CASSANDRA-230](https://issues.apache.org/jira/browse/CASSANDRA-230) | Race in ChecksumManager.instance() |  Major | . | Stu Hood | Jonathan Ellis |
| [CASSANDRA-229](https://issues.apache.org/jira/browse/CASSANDRA-229) | cassandra-cli is not executable |  Minor | Tools | Edward Capriolo | Eric Evans |
| [CASSANDRA-236](https://issues.apache.org/jira/browse/CASSANDRA-236) | Bug in Range.contains(Token) |  Minor | . | Stu Hood | Stu Hood |
| [CASSANDRA-239](https://issues.apache.org/jira/browse/CASSANDRA-239) | Version name not set correctly |  Trivial | Tools | Sandeep Tata | Sandeep Tata |
| [CASSANDRA-199](https://issues.apache.org/jira/browse/CASSANDRA-199) | Row.columnFamilies\_ is initialized to Hashtable in one place, HashMap in another |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-247](https://issues.apache.org/jira/browse/CASSANDRA-247) | Thrift interface uses reserved keyword "end" |  Major | . | Stephen Judkins | Stephen Judkins |
| [CASSANDRA-220](https://issues.apache.org/jira/browse/CASSANDRA-220) | TCP writes get stuck |  Major | . | Jun Rao | Jun Rao |
| [CASSANDRA-255](https://issues.apache.org/jira/browse/CASSANDRA-255) | Supercolumn deserialization bug |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-223](https://issues.apache.org/jira/browse/CASSANDRA-223) | time-based slicing does not work correctly w/ "historical" memtables |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-264](https://issues.apache.org/jira/browse/CASSANDRA-264) | log replay bugs |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-266](https://issues.apache.org/jira/browse/CASSANDRA-266) | Thrift validation bugs |  Major | . | Evan Weaver | Jonathan Ellis |
| [CASSANDRA-262](https://issues.apache.org/jira/browse/CASSANDRA-262) | get\_slice needs to allow returning all columns |  Major | . | Chris Goffinet | Jonathan Ellis |
| [CASSANDRA-261](https://issues.apache.org/jira/browse/CASSANDRA-261) | get\_slice needs offset + limit |  Major | . | Chris Goffinet | Jun Rao |
| [CASSANDRA-280](https://issues.apache.org/jira/browse/CASSANDRA-280) | scanning multiple CFs at once for range queries is a misfeature |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-278](https://issues.apache.org/jira/browse/CASSANDRA-278) | functional test errors on OSX |  Major | Tools | Eric Evans | Eric Evans |
| [CASSANDRA-285](https://issues.apache.org/jira/browse/CASSANDRA-285) | Add PendingTasks in nodeprobe |  Major | Tools | Sammy Yu | Sammy Yu |
| [CASSANDRA-282](https://issues.apache.org/jira/browse/CASSANDRA-282) | system tests fail straingely if an instance is running |  Major | Tools | Eric Evans | Michael Greene |
| [CASSANDRA-283](https://issues.apache.org/jira/browse/CASSANDRA-283) | Cassandra leaks FDs |  Major | . | Ian Eure | Jonathan Ellis |
| [CASSANDRA-294](https://issues.apache.org/jira/browse/CASSANDRA-294) | missing lib/license/google-collect-1.0-rc1.jar.LICENSE |  Major | . | Eric Evans | Eric Evans |
| [CASSANDRA-287](https://issues.apache.org/jira/browse/CASSANDRA-287) | Make iterator-based read code the One True Path |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-286](https://issues.apache.org/jira/browse/CASSANDRA-286) | slice offset breaks read repair |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-297](https://issues.apache.org/jira/browse/CASSANDRA-297) | remove analytics package |  Minor | . | Eric Evans | Eric Evans |
| [CASSANDRA-306](https://issues.apache.org/jira/browse/CASSANDRA-306) | Make nosetest retrieve the stdout and stderr message when Cassandra fails to start |  Major | Tools | Sammy Yu | Sammy Yu |
| [CASSANDRA-307](https://issues.apache.org/jira/browse/CASSANDRA-307) | Running on Windows XP, get "Incorrect number of parameters: and" |  Minor | Tools | Jason Carver | Michael Greene |
| [CASSANDRA-196](https://issues.apache.org/jira/browse/CASSANDRA-196) | Doing a descending range still returns columns in ascending order |  Major | . | Jonathan Ellis | Jun Rao |
| [CASSANDRA-313](https://issues.apache.org/jira/browse/CASSANDRA-313) | File descriptor leak in CommitLog |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-317](https://issues.apache.org/jira/browse/CASSANDRA-317) | cassandra-cli describe table exception. |  Trivial | Tools | Gasol Wu | Gasol Wu |
| [CASSANDRA-182](https://issues.apache.org/jira/browse/CASSANDRA-182) | CommitLog.add doesn't really force to disk |  Major | . | Sandeep Tata | Jonathan Ellis |
| [CASSANDRA-326](https://issues.apache.org/jira/browse/CASSANDRA-326) | get\_string\_list\_property("tables") does not include CompareSubcolumnsWith |  Major | . | Evan Weaver | Evan Weaver |
| [CASSANDRA-111](https://issues.apache.org/jira/browse/CASSANDRA-111) | Mixed line endings in the codebase |  Major | . | Jeff Hodges | Jeff Hodges |
| [CASSANDRA-263](https://issues.apache.org/jira/browse/CASSANDRA-263) | get\_slice needs to support desc from last column |  Major | . | Chris Goffinet | Jun Rao |
| [CASSANDRA-212](https://issues.apache.org/jira/browse/CASSANDRA-212) | Range queries do not yet span multiple nodes |  Major | . | Jonathan Ellis | Arin Sarkissian |
| [CASSANDRA-340](https://issues.apache.org/jira/browse/CASSANDRA-340) | Default configuration broken with UUID change |  Minor | . | Michael Greene | Michael Greene |
| [CASSANDRA-341](https://issues.apache.org/jira/browse/CASSANDRA-341) | supercolumn indexing is broken |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-349](https://issues.apache.org/jira/browse/CASSANDRA-349) | better error handling for CommitLogSyncDelay |  Minor | . | Eric Evans | Jonathan Ellis |
| [CASSANDRA-353](https://issues.apache.org/jira/browse/CASSANDRA-353) | Snapshot should include index and bloomfile |  Major | Tools | Sammy Yu | Sammy Yu |
| [CASSANDRA-356](https://issues.apache.org/jira/browse/CASSANDRA-356) | Count parameter has no effect on subcolumns. |  Major | . | Evan Weaver | Jonathan Ellis |
| [CASSANDRA-241](https://issues.apache.org/jira/browse/CASSANDRA-241) | move daemon to framed transport (thrift) |  Major | . | Eric Evans | Eric Evans |
| [CASSANDRA-357](https://issues.apache.org/jira/browse/CASSANDRA-357) | CompareSubcolumnsWith= has no effect |  Major | . | Evan Weaver | Jonathan Ellis |
| [CASSANDRA-354](https://issues.apache.org/jira/browse/CASSANDRA-354) | BUGS.txt cites bugs which are now fixed |  Minor | Documentation and Website | Mark Robson | Jonathan Ellis |
| [CASSANDRA-359](https://issues.apache.org/jira/browse/CASSANDRA-359) | CFS readStats\_ and diskReadStats\_ are missing |  Major | Tools | Sammy Yu | Sammy Yu |
| [CASSANDRA-348](https://issues.apache.org/jira/browse/CASSANDRA-348) | Range scan over two nodes returns wrong data |  Major | . | Mark Robson | Jonathan Ellis |
| [CASSANDRA-362](https://issues.apache.org/jira/browse/CASSANDRA-362) | SystemTable is not persisted across reboots |  Major | . | Sammy Yu | Sammy Yu |
| [CASSANDRA-366](https://issues.apache.org/jira/browse/CASSANDRA-366) | Invalid Java identified by JDK7 in StorageProxy.java |  Major | . | Michael Greene | Michael Greene |
| [CASSANDRA-367](https://issues.apache.org/jira/browse/CASSANDRA-367) | CommitLog does not flush on writes |  Blocker | . | Chris Goffinet | Jonathan Ellis |
| [CASSANDRA-378](https://issues.apache.org/jira/browse/CASSANDRA-378) | Missing or incorrect SVN properties in http://svn.apache.org/repos/asf/incubator/cassandra/tags/cassandra-0.4.0-beta1/ |  Major | Tools | Sebb | Sebb |
| [CASSANDRA-372](https://issues.apache.org/jira/browse/CASSANDRA-372) | changelog doesn't mention snapshot support |  Trivial | Documentation and Website | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-373](https://issues.apache.org/jira/browse/CASSANDRA-373) | storage-conf.xml reformatting |  Major | . | Eric Evans | Eric Evans |
| [CASSANDRA-384](https://issues.apache.org/jira/browse/CASSANDRA-384) | LongType should be network-endian |  Major | . | Evan Weaver | Jonathan Ellis |
| [CASSANDRA-383](https://issues.apache.org/jira/browse/CASSANDRA-383) | StorageProxy.insertBlocking does not perform hinted handoff |  Major | . | Sandeep Tata | Sandeep Tata |
| [CASSANDRA-370](https://issues.apache.org/jira/browse/CASSANDRA-370) | Commit log replay issues |  Critical | . | James Golick | Jonathan Ellis |
| [CASSANDRA-391](https://issues.apache.org/jira/browse/CASSANDRA-391) | better default logging paths |  Minor | Documentation and Website | Eric Evans | Eric Evans |
| [CASSANDRA-394](https://issues.apache.org/jira/browse/CASSANDRA-394) | FileUtils.getUsedDiskSpace is inaccurate |  Major | . | Sammy Yu | Sammy Yu |
| [CASSANDRA-395](https://issues.apache.org/jira/browse/CASSANDRA-395) | BootstrapTest occasionally fails |  Major | . | Jonathan Ellis | Sandeep Tata |
| [CASSANDRA-337](https://issues.apache.org/jira/browse/CASSANDRA-337) | Make BinaryMemtable work |  Major | . | Jonathan Ellis | Chris Goffinet |
| [CASSANDRA-399](https://issues.apache.org/jira/browse/CASSANDRA-399) | Consisteny Level of ZERO blocks for ack on Commit Log |  Minor | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-392](https://issues.apache.org/jira/browse/CASSANDRA-392) | Deadlock with SelectorManager.doProcess and TcpConnection.write |  Major | . | Sammy Yu | Jun Rao |
| [CASSANDRA-410](https://issues.apache.org/jira/browse/CASSANDRA-410) | flexjson.jar -\> flexjson-1.7.jar |  Minor | . | Eric Evans | Eric Evans |
| [CASSANDRA-411](https://issues.apache.org/jira/browse/CASSANDRA-411) | NOTICE.txt out of data for third-party libs |  Major | . | Eric Evans | Eric Evans |
| [CASSANDRA-343](https://issues.apache.org/jira/browse/CASSANDRA-343) | SSTable gets removed if index file is missing |  Major | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-405](https://issues.apache.org/jira/browse/CASSANDRA-405) | Race condition with ConcurrentLinkedHashMap |  Major | . | Chris Goffinet | Jonathan Ellis |
| [CASSANDRA-308](https://issues.apache.org/jira/browse/CASSANDRA-308) | Thrift client forwarding the null keys to the servers |  Minor | . | Vijay | Jonathan Ellis |
| [CASSANDRA-418](https://issues.apache.org/jira/browse/CASSANDRA-418) | SSTable generation clash during compaction |  Major | . | Sammy Yu | Jonathan Ellis |
| [CASSANDRA-421](https://issues.apache.org/jira/browse/CASSANDRA-421) | nodeprobe outputs incorrectly ordered ring |  Minor | Tools | Eric Evans | Eric Evans |
| [CASSANDRA-425](https://issues.apache.org/jira/browse/CASSANDRA-425) | AssertionError during initial compaction |  Critical | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-429](https://issues.apache.org/jira/browse/CASSANDRA-429) | more missing svn properties |  Minor | . | Eric Evans | Eric Evans |
| [CASSANDRA-415](https://issues.apache.org/jira/browse/CASSANDRA-415) | NOTICE should not reference pom.xml for dev list |  Minor | Documentation and Website | Eric Evans | Eric Evans |
| [CASSANDRA-428](https://issues.apache.org/jira/browse/CASSANDRA-428) | source for (unreleased )commons javaflow jar |  Minor | . | Eric Evans | Eric Evans |
| [CASSANDRA-371](https://issues.apache.org/jira/browse/CASSANDRA-371) | LICENSE.txt should mention lib/licenses |  Major | . | Eric Evans | Eric Evans |
| [CASSANDRA-440](https://issues.apache.org/jira/browse/CASSANDRA-440) | get\_key\_range problems when a node is down |  Major | . | Simon Smith | Jonathan Ellis |
| [CASSANDRA-448](https://issues.apache.org/jira/browse/CASSANDRA-448) | README.txt inaccuracies |  Minor | Documentation and Website | Eric Evans | Paul Querna |
| [CASSANDRA-458](https://issues.apache.org/jira/browse/CASSANDRA-458) | Null pointer exception in doIndexing(ColumnIndexer.java:142) |  Major | . | Teodor Sigaev | Jonathan Ellis |
| [CASSANDRA-460](https://issues.apache.org/jira/browse/CASSANDRA-460) | java.lang.NegativeArraySizeException being thrown for large column names |  Major | . | T Jake Luciani | Jonathan Ellis |
| [CASSANDRA-455](https://issues.apache.org/jira/browse/CASSANDRA-455) | DebuggableScheduledThreadPoolExecutor only schedules a task once |  Major | . | Jun Rao | Jonathan Ellis |
| [CASSANDRA-459](https://issues.apache.org/jira/browse/CASSANDRA-459) | Commitlog segments don't get deleted |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-462](https://issues.apache.org/jira/browse/CASSANDRA-462) | Read repair happens on every quorum read |  Major | . | Edmond Lau | Jonathan Ellis |
| [CASSANDRA-473](https://issues.apache.org/jira/browse/CASSANDRA-473) | Major compaction still leaves large set of files |  Major | . | Chris Goffinet | Jonathan Ellis |
| [CASSANDRA-484](https://issues.apache.org/jira/browse/CASSANDRA-484) | subcolumn-reading code doesn't properly account for tombstoned columnfamily |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-478](https://issues.apache.org/jira/browse/CASSANDRA-478) | NPE during read repair |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-486](https://issues.apache.org/jira/browse/CASSANDRA-486) | StringTokenizer throws NoSuchElementException |  Minor | . | Eric Lubow | Jonathan Ellis |
| [CASSANDRA-430](https://issues.apache.org/jira/browse/CASSANDRA-430) | pom.xml out of date |  Minor | Tools | Eric Evans | Niall Pemberton |
| [CASSANDRA-490](https://issues.apache.org/jira/browse/CASSANDRA-490) | FBUtilities.bytesToHex and FBUtilities.hexToBytes are not inverses |  Major | . | Sandeep Tata | Sandeep Tata |
| [CASSANDRA-487](https://issues.apache.org/jira/browse/CASSANDRA-487) | Message Serializer slows down/stops responding |  Major | . | Sammy Yu | Sammy Yu |
| [CASSANDRA-493](https://issues.apache.org/jira/browse/CASSANDRA-493) | error with utf8 columnfamilies |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-518](https://issues.apache.org/jira/browse/CASSANDRA-518) | get\_slice declares NotFoundException but does not throw it |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-508](https://issues.apache.org/jira/browse/CASSANDRA-508) | unable to get entire supercolumn |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-501](https://issues.apache.org/jira/browse/CASSANDRA-501) | Bootstrap broken in 0.4.1 |  Major | . | Sandeep Tata | Sandeep Tata |
| [CASSANDRA-533](https://issues.apache.org/jira/browse/CASSANDRA-533) | Need to close files in loadBloomFilter and loadIndexFile |  Major | . | Tim Freeman | Tim Freeman |
| [CASSANDRA-316](https://issues.apache.org/jira/browse/CASSANDRA-316) | antrl generated files should not be included in the source release |  Major | Tools | Eric Evans | Eric Evans |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-210](https://issues.apache.org/jira/browse/CASSANDRA-210) | add a unit test for get\_slice\_from |  Major | . | Jun Rao | Jun Rao |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-154](https://issues.apache.org/jira/browse/CASSANDRA-154) | Change thrift interface to allow #endpoints as a parameter to insert calls |  Major | . | Sandeep Tata | Sandeep Tata |
| [CASSANDRA-237](https://issues.apache.org/jira/browse/CASSANDRA-237) | test and cleanup CommitLogHeader |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-235](https://issues.apache.org/jira/browse/CASSANDRA-235) | Move system CFs into own Table |  Major | . | Jonathan Ellis | Jonathan Ellis |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-148](https://issues.apache.org/jira/browse/CASSANDRA-148) | wiki client code samples |  Major | Documentation and Website | Jonathan Ellis | Chris Goffinet |
| [CASSANDRA-176](https://issues.apache.org/jira/browse/CASSANDRA-176) | Add \<libname\>.LICENSE files |  Major | . | Johan Oskarsson | Johan Oskarsson |
| [CASSANDRA-221](https://issues.apache.org/jira/browse/CASSANDRA-221) | add changelog |  Major | Documentation and Website | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-227](https://issues.apache.org/jira/browse/CASSANDRA-227) | Remove unused Constants class |  Major | . | Edward Ribeiro | Jonathan Ellis |
| [CASSANDRA-142](https://issues.apache.org/jira/browse/CASSANDRA-142) | vet Setup page and remove it |  Minor | Documentation and Website | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-281](https://issues.apache.org/jira/browse/CASSANDRA-281) | Spelling fixes, correct command names, and other pedantry |  Trivial | . | Michael Greene | Michael Greene |
| [CASSANDRA-291](https://issues.apache.org/jira/browse/CASSANDRA-291) | Zookeeper is never actually used, outdated jar and references should be removed until useful |  Trivial | . | Michael Greene | Michael Greene |
| [CASSANDRA-62](https://issues.apache.org/jira/browse/CASSANDRA-62) | Stop accepting emails to Google Groups for Cassandra |  Major | Documentation and Website | Jeff Hammerbacher | Jeff Hammerbacher |
| [CASSANDRA-27](https://issues.apache.org/jira/browse/CASSANDRA-27) | Add jira components |  Major | . | Johan Oskarsson | Jonathan Ellis |
| [CASSANDRA-304](https://issues.apache.org/jira/browse/CASSANDRA-304) | allow subcolumns of supercolumns to be any comparator type |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-321](https://issues.apache.org/jira/browse/CASSANDRA-321) |  update changelog for 0.4 |  Major | Documentation and Website | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-345](https://issues.apache.org/jira/browse/CASSANDRA-345) | move getKeyRange into CFS |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-361](https://issues.apache.org/jira/browse/CASSANDRA-361) | Clean up references to Cql |  Minor | . | Michael Greene | Michael Greene |
| [CASSANDRA-365](https://issues.apache.org/jira/browse/CASSANDRA-365) | add license headers for new-in-0.4 files |  Major | Documentation and Website | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-379](https://issues.apache.org/jira/browse/CASSANDRA-379) | Remove @author tags from source |  Trivial | Documentation and Website | Michael Greene | Michael Greene |
| [CASSANDRA-380](https://issues.apache.org/jira/browse/CASSANDRA-380) | Remove Groovy binary until we need to use it |  Major | . | Michael Greene | Michael Greene |
| [CASSANDRA-393](https://issues.apache.org/jira/browse/CASSANDRA-393) | rename storageendpoint overloads for clarity |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-400](https://issues.apache.org/jira/browse/CASSANDRA-400) | Switch default comparator to BytesType |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-398](https://issues.apache.org/jira/browse/CASSANDRA-398) | Submit BinaryMemtable Example for contrib/ |  Minor | . | Chris Goffinet | Chris Goffinet |


