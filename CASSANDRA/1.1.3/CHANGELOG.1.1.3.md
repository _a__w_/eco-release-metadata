
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.1.3 - 2012-08-05



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3709](https://issues.apache.org/jira/browse/CASSANDRA-3709) | Add a token generator to standard tools |  Minor | Tools | Jonathan Ellis | paul cannon |
| [CASSANDRA-4434](https://issues.apache.org/jira/browse/CASSANDRA-4434) | add COPY TO |  Major | . | Jonathan Ellis | paul cannon |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4408](https://issues.apache.org/jira/browse/CASSANDRA-4408) | Don't log mx4j stuff at info |  Trivial | . | Nick Bailey | Nick Bailey |
| [CASSANDRA-4422](https://issues.apache.org/jira/browse/CASSANDRA-4422) | Humor 32bit JVMs |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3047](https://issues.apache.org/jira/browse/CASSANDRA-3047) | implementations of IPartitioner.describeOwnership() are not DC aware |  Trivial | Tools | amorton | David Alves |
| [CASSANDRA-4327](https://issues.apache.org/jira/browse/CASSANDRA-4327) | Sorting results when using IN() |  Minor | . | Stephen Powis | Pavel Yaskevich |
| [CASSANDRA-4144](https://issues.apache.org/jira/browse/CASSANDRA-4144) | Pig: Composite column support for row key |  Minor | . | janwar dinata | Dirkjan Bussink |
| [CASSANDRA-4398](https://issues.apache.org/jira/browse/CASSANDRA-4398) | Incorrect english for cassandra-cli help |  Trivial | Tools | Aurelien Derouineau | Tommy Tynjä |
| [CASSANDRA-4447](https://issues.apache.org/jira/browse/CASSANDRA-4447) | enable jamm for OpenJDK \>= 1.6.0.23 |  Trivial | Packaging | Ilya Shipitsin | paul cannon |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4403](https://issues.apache.org/jira/browse/CASSANDRA-4403) | cleanup uses global partitioner to estimate ranges in index sstables |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4399](https://issues.apache.org/jira/browse/CASSANDRA-4399) | use data size ratio in liveRatio instead of live size : serialized throughput |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4400](https://issues.apache.org/jira/browse/CASSANDRA-4400) | Correctly catch exception when Snappy cannot be loaded |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4420](https://issues.apache.org/jira/browse/CASSANDRA-4420) | Possible schema corruption with cql 3.0 |  Major | . | bert Passek | Pavel Yaskevich |
| [CASSANDRA-4396](https://issues.apache.org/jira/browse/CASSANDRA-4396) | Subcolumns not removed when compacting tombstoned super column |  Minor | . | Nick Bailey | Jonathan Ellis |
| [CASSANDRA-4432](https://issues.apache.org/jira/browse/CASSANDRA-4432) | Change nanoTime() to currentTimeInMillis() in schema related code. |  Major | . | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-4401](https://issues.apache.org/jira/browse/CASSANDRA-4401) | If processor is missing from /proc/cpuinfo, cassandra will not start |  Minor | Packaging | Andy Cobley |  |
| [CASSANDRA-4380](https://issues.apache.org/jira/browse/CASSANDRA-4380) | CQLSH: describe command doesn't output valid CQL command. |  Major | . | Nick Bailey | paul cannon |
| [CASSANDRA-4309](https://issues.apache.org/jira/browse/CASSANDRA-4309) | CQL3: cqlsh exception running "describe schema" |  Minor | Tools | Cathy Daw | paul cannon |
| [CASSANDRA-4074](https://issues.apache.org/jira/browse/CASSANDRA-4074) | cqlsh: Tab completion should not suggest consistency level ANY for select statements |  Trivial | . | Tyler Patterson | paul cannon |
| [CASSANDRA-4411](https://issues.apache.org/jira/browse/CASSANDRA-4411) | Assertion with LCS compaction |  Major | . | Anton Winter | Sylvain Lebresne |
| [CASSANDRA-4439](https://issues.apache.org/jira/browse/CASSANDRA-4439) | Updating column family using cassandra-cli results in "Cannot modify index name" |  Minor | Tools | Alex Schultz | Pavel Yaskevich |
| [CASSANDRA-4271](https://issues.apache.org/jira/browse/CASSANDRA-4271) | Exit status of bin/cassandra without -f is wrong |  Minor | . | Tyler Hobbs | paul cannon |
| [CASSANDRA-3636](https://issues.apache.org/jira/browse/CASSANDRA-3636) | cassandra 1.0.x breakes APT on debian OpenVZ |  Minor | Packaging | Zenek Kraweznik | paul cannon |
| [CASSANDRA-4456](https://issues.apache.org/jira/browse/CASSANDRA-4456) | AssertionError in ColumnFamilyStore.getOverlappingSSTables() during repair |  Major | . | Mike Heffner | Sylvain Lebresne |
| [CASSANDRA-4459](https://issues.apache.org/jira/browse/CASSANDRA-4459) | pig driver casts ints as bytearray |  Major | . | Cathy Daw | Brandon Williams |
| [CASSANDRA-4452](https://issues.apache.org/jira/browse/CASSANDRA-4452) | remove RangeKeySample from attributes in jmx |  Minor | . | Jan Prach | Jan Prach |
| [CASSANDRA-4436](https://issues.apache.org/jira/browse/CASSANDRA-4436) | Counters in columns don't preserve correct values after cluster restart |  Major | . | Peter Velas | Sylvain Lebresne |
| [CASSANDRA-4385](https://issues.apache.org/jira/browse/CASSANDRA-4385) | bug when trying to describe a cf in a pre cql3 case sensitive keyspace |  Minor | . | Al Tobey | paul cannon |
| [CASSANDRA-4462](https://issues.apache.org/jira/browse/CASSANDRA-4462) | upgradesstables strips active data from sstables |  Major | . | Mike Heffner | Sylvain Lebresne |
| [CASSANDRA-4469](https://issues.apache.org/jira/browse/CASSANDRA-4469) | Fix online help in cqlsh for COPY commands |  Trivial | Tools | paul cannon | paul cannon |
| [CASSANDRA-4470](https://issues.apache.org/jira/browse/CASSANDRA-4470) | cqlsh COPY FROM without explicit column names is broken |  Major | Tools | paul cannon | paul cannon |
| [CASSANDRA-4427](https://issues.apache.org/jira/browse/CASSANDRA-4427) | Restarting a failed bootstrap instajoins the ring |  Major | . | Brandon Williams | Jonathan Ellis |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3633](https://issues.apache.org/jira/browse/CASSANDRA-3633) | update stress to support prepared statements |  Minor | CQL | Eric Evans | David Alves |


