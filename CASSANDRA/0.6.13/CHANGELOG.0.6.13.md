
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.6.13 - 2011-04-18



### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2212](https://issues.apache.org/jira/browse/CASSANDRA-2212) | Cannot get range slice of super columns in reversed order |  Major | . | Muga Nishizawa | Sylvain Lebresne |
| [CASSANDRA-2269](https://issues.apache.org/jira/browse/CASSANDRA-2269) | OOM in the Thrift thread doesn't shut down server |  Minor | CQL | Jonathan Ellis | Jonathan Ellis |


