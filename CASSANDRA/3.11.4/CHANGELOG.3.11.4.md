
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.11.4 - Unreleased (as of 2018-10-20)



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-14713](https://issues.apache.org/jira/browse/CASSANDRA-14713) | Update docker image used for testing |  Major | Testing | Stefan Podkowinski | Stefan Podkowinski |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-14660](https://issues.apache.org/jira/browse/CASSANDRA-14660) | Improve TokenMetaData cache populating performance for large cluster |  Critical | Coordination | Pengchao Wang | Pengchao Wang |
| [CASSANDRA-14824](https://issues.apache.org/jira/browse/CASSANDRA-14824) | Expand range tombstone validation checks to multiple interim request stages |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13262](https://issues.apache.org/jira/browse/CASSANDRA-13262) | Incorrect cqlsh results when selecting same columns multiple times |  Minor | Tools | Stefan Podkowinski | Murukesh Mohanan |
| [CASSANDRA-14468](https://issues.apache.org/jira/browse/CASSANDRA-14468) | "Unable to parse targets for index" on upgrade to Cassandra 3.0.10-3.0.16 |  Major | . | Wade Simmons | Jordan West |
| [CASSANDRA-14377](https://issues.apache.org/jira/browse/CASSANDRA-14377) | Returning invalid JSON for NaN and Infinity float values |  Minor | CQL | Piotr Sarna | Benjamin Lerer |
| [CASSANDRA-14522](https://issues.apache.org/jira/browse/CASSANDRA-14522) | sstableloader should use discovered broadcast address to connect intra-cluster |  Major | Tools | Jeremy Hanna | Jeremy |
| [CASSANDRA-14638](https://issues.apache.org/jira/browse/CASSANDRA-14638) | Column result order can change in 'SELECT \*' results when upgrading from 2.1 to 3.0 causing response corruption for queries using prepared statements when static columns are used |  Major | . | Andy Tolbert | Aleksey Yeschenko |
| [CASSANDRA-14657](https://issues.apache.org/jira/browse/CASSANDRA-14657) | Handle failures in upgradesstables/cleanup/relocate |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-14766](https://issues.apache.org/jira/browse/CASSANDRA-14766) | DESC order reads can fail to return the last Unfiltered in the partition in a legacy sstable |  Major | Local Write-Read Paths | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-14672](https://issues.apache.org/jira/browse/CASSANDRA-14672) | After deleting data in 3.11.3, reads fail with "open marker and close marker have different deletion times" |  Blocker | Local Write-Read Paths | Spiros Ioannou | Aleksey Yeschenko |
| [CASSANDRA-14794](https://issues.apache.org/jira/browse/CASSANDRA-14794) | Avoid calling iter.next() in a loop when notifying indexers about range tombstones |  Major | . | Marcus Eriksson | Marcus Eriksson |


