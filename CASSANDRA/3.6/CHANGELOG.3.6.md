
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.6 - 2016-06-06



### IMPORTANT ISSUES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11574](https://issues.apache.org/jira/browse/CASSANDRA-11574) | clqsh: COPY FROM throws TypeError with Cython extensions enabled |  Major | Tools | Mahafuzur Rahman | Stefania |


### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9220](https://issues.apache.org/jira/browse/CASSANDRA-9220) | Hostname verification for node-to-node encryption |  Major | . | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-10411](https://issues.apache.org/jira/browse/CASSANDRA-10411) | Add/drop multiple columns in one ALTER TABLE statement |  Minor | . | Bryn Cooke | Amit Singh Chowdhery |
| [CASSANDRA-9967](https://issues.apache.org/jira/browse/CASSANDRA-9967) | Determine if a Materialized View is finished building, without having to query each node |  Minor | Coordination, Materialized Views, Observability | Alan Boudreault | Carl Yeksigian |
| [CASSANDRA-10818](https://issues.apache.org/jira/browse/CASSANDRA-10818) | Allow instantiation of UDTs and tuples in UDFs |  Minor | CQL | Robert Stupp | Robert Stupp |
| [CASSANDRA-10091](https://issues.apache.org/jira/browse/CASSANDRA-10091) | Integrated JMX authn & authz |  Minor | Configuration, Lifecycle, Observability | Jan Karlsson | Sam Tunnicliffe |
| [CASSANDRA-10805](https://issues.apache.org/jira/browse/CASSANDRA-10805) | Additional Compaction Logging |  Minor | Compaction, Observability | Carl Yeksigian | Carl Yeksigian |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11207](https://issues.apache.org/jira/browse/CASSANDRA-11207) | Can not remove TTL on table with default\_time\_to\_live |  Major | CQL | Matthieu Nantern | Benjamin Lerer |
| [CASSANDRA-11059](https://issues.apache.org/jira/browse/CASSANDRA-11059) | In cqlsh show static columns in a different color |  Minor | Tools | Cédric Hernalsteens | Pavel Trukhanov |
| [CASSANDRA-8890](https://issues.apache.org/jira/browse/CASSANDRA-8890) | Enhance cassandra-env.sh to handle Java version output in case of OpenJDK icedtea" |  Minor | Configuration | Sumod Pawgi | Brandon Williams |
| [CASSANDRA-10112](https://issues.apache.org/jira/browse/CASSANDRA-10112) | Refuse to start and print txn log information in case of disk corruption |  Major | Local Write-Read Paths | Stefania | Stefania |
| [CASSANDRA-11274](https://issues.apache.org/jira/browse/CASSANDRA-11274) | cqlsh: interpret CQL type for formatting blob types |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-10876](https://issues.apache.org/jira/browse/CASSANDRA-10876) | Alter behavior of batch WARN and fail on single partition batches |  Minor | . | Patrick McFadin | Sylvain Lebresne |
| [CASSANDRA-10099](https://issues.apache.org/jira/browse/CASSANDRA-10099) | Improve concurrency in CompactionStrategyManager |  Major | . | Yuki Morishita | Marcus Eriksson |
| [CASSANDRA-10508](https://issues.apache.org/jira/browse/CASSANDRA-10508) | Remove hard-coded SSL cipher suites and protocols |  Major | . | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-9588](https://issues.apache.org/jira/browse/CASSANDRA-9588) | Make sstableofflinerelevel print stats before relevel |  Trivial | Tools | Jens Rantil | Marcus Eriksson |
| [CASSANDRA-11244](https://issues.apache.org/jira/browse/CASSANDRA-11244) | Add repair options to repair\_history table |  Minor | Coordination | Paulo Motta | Marcus Eriksson |
| [CASSANDRA-8958](https://issues.apache.org/jira/browse/CASSANDRA-8958) | Add client to cqlsh SHOW\_SESSION |  Minor | Tools | Stefania | Stefania |
| [CASSANDRA-10340](https://issues.apache.org/jira/browse/CASSANDRA-10340) | Stress should exit with non-zero status after failure |  Minor | Tools | Paulo Motta | Stefania |
| [CASSANDRA-11255](https://issues.apache.org/jira/browse/CASSANDRA-11255) | COPY TO should have higher double precision |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-9421](https://issues.apache.org/jira/browse/CASSANDRA-9421) | TriggerExecutor should not wrap all execution exceptions in a RuntimeException |  Minor | . | Sam Tunnicliffe | Branimir Lambov |
| [CASSANDRA-7159](https://issues.apache.org/jira/browse/CASSANDRA-7159) | sstablemetadata command should print some more stuff |  Trivial | Tools | Jeremiah Jordan | Marcus Eriksson |
| [CASSANDRA-11337](https://issues.apache.org/jira/browse/CASSANDRA-11337) | Add --hex-format option to nodetool getsstables |  Minor | Tools | Paulo Motta | Paulo Motta |
| [CASSANDRA-11392](https://issues.apache.org/jira/browse/CASSANDRA-11392) | Add auto import java.util for UDF code block |  Minor | CQL | DOAN DuyHai | DOAN DuyHai |
| [CASSANDRA-7739](https://issues.apache.org/jira/browse/CASSANDRA-7739) | cassandra-stress: cannot handle "value-less" tables |  Major | . | Robert Stupp |  |
| [CASSANDRA-11420](https://issues.apache.org/jira/browse/CASSANDRA-11420) | Add the JMX metrics to track the write amplification of C\* |  Minor | . | Dikang Gu | Dikang Gu |
| [CASSANDRA-8888](https://issues.apache.org/jira/browse/CASSANDRA-8888) | Compress only inter-dc traffic by default |  Major | Streaming and Messaging | Matt Stump | Kaide Mu |
| [CASSANDRA-11372](https://issues.apache.org/jira/browse/CASSANDRA-11372) | Make CQL grammar more easily extensible |  Major | CQL | Mike Adamson | Mike Adamson |
| [CASSANDRA-11096](https://issues.apache.org/jira/browse/CASSANDRA-11096) | Upgrade netty to \>= 4.0.34 |  Major | CQL | Brandon Williams | Benjamin Lerer |
| [CASSANDRA-9692](https://issues.apache.org/jira/browse/CASSANDRA-9692) | Print sensible units for all log messages |  Minor | Observability | Benedict | Giampaolo |
| [CASSANDRA-8777](https://issues.apache.org/jira/browse/CASSANDRA-8777) | Streaming operations should log both endpoint and port associated with the operation |  Major | . | Jeremy Hanna | Kaide Mu |
| [CASSANDRA-11456](https://issues.apache.org/jira/browse/CASSANDRA-11456) | support for PreparedStatement with LIKE |  Minor | CQL | Pavel Yaskevich | Sam Tunnicliffe |
| [CASSANDRA-11312](https://issues.apache.org/jira/browse/CASSANDRA-11312) | DatabaseDescriptor#applyConfig should log stacktrace in case of Eception during seed provider creation |  Minor | Configuration | Andrzej Ludwikowski | Andrzej Ludwikowski |
| [CASSANDRA-11434](https://issues.apache.org/jira/browse/CASSANDRA-11434) | Support EQ/PREFIX queries in CONTAINS mode without tokenization by augmenting SA metadata per term |  Major | sasi | Pavel Yaskevich | Jordan West |
| [CASSANDRA-11486](https://issues.apache.org/jira/browse/CASSANDRA-11486) | Duplicate logging of merkle tree request |  Trivial | . | Marcus Olsson | Marcus Olsson |
| [CASSANDRA-11183](https://issues.apache.org/jira/browse/CASSANDRA-11183) | Enable SASI index for static columns |  Minor | sasi | DOAN DuyHai | DOAN DuyHai |
| [CASSANDRA-10120](https://issues.apache.org/jira/browse/CASSANDRA-10120) | When specifying both num\_tokens and initial\_token, error out if the numbers don't match |  Minor | . | Jeremy Hanna | Roman Pogribnyi |
| [CASSANDRA-9348](https://issues.apache.org/jira/browse/CASSANDRA-9348) | Nodetool move output should be more user friendly if bad token is supplied |  Trivial | . | sequoyha pelletier | Abhishek Verma |
| [CASSANDRA-11507](https://issues.apache.org/jira/browse/CASSANDRA-11507) | Nodetool proxyhistograms is missing CAS stats |  Minor | Tools | Christopher Batey | Christopher Batey |
| [CASSANDRA-10649](https://issues.apache.org/jira/browse/CASSANDRA-10649) | Improve field-checking and error reporting in cassandra.yaml |  Minor | Configuration | sandeep thakur | Benjamin Lerer |
| [CASSANDRA-11295](https://issues.apache.org/jira/browse/CASSANDRA-11295) | Make custom filtering more extensible via custom classes |  Minor | Local Write-Read Paths | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-11051](https://issues.apache.org/jira/browse/CASSANDRA-11051) | Make LZ4 Compression Level Configurable |  Major | Compaction | Michael Kjellman | Michael Kjellman |
| [CASSANDRA-7423](https://issues.apache.org/jira/browse/CASSANDRA-7423) | Allow updating individual subfields of UDT |  Major | CQL | Tupshin Harper | Tyler Hobbs |
| [CASSANDRA-11526](https://issues.apache.org/jira/browse/CASSANDRA-11526) | Make ResultSetBuilder.rowToJson public |  Major | Core | Jeremiah Jordan | Berenguer Blasi |
| [CASSANDRA-7017](https://issues.apache.org/jira/browse/CASSANDRA-7017) | allow per-partition LIMIT clause in cql |  Major | . | Jonathan Halliday | Alex Petrov |
| [CASSANDRA-8006](https://issues.apache.org/jira/browse/CASSANDRA-8006) | Add User defined type support to CQLSSTableWriter |  Minor | . | Yuki Morishita |  |
| [CASSANDRA-11428](https://issues.apache.org/jira/browse/CASSANDRA-11428) | Eliminate Allocations |  Minor | . | T Jake Luciani | Nitsan Wakart |
| [CASSANDRA-11354](https://issues.apache.org/jira/browse/CASSANDRA-11354) | PrimaryKeyRestrictionSet should be refactored |  Major | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-11437](https://issues.apache.org/jira/browse/CASSANDRA-11437) | Make number of cores used by cqlsh COPY visible to testing code |  Minor | Testing | Jim Witschey | Stefania |
| [CASSANDRA-11410](https://issues.apache.org/jira/browse/CASSANDRA-11410) | Option to specify ProtocolVersion in cassandra-stress |  Minor | Testing, Tools | mck | mck |
| [CASSANDRA-11591](https://issues.apache.org/jira/browse/CASSANDRA-11591) | Cassandra-stress doesn't support explicit datacenter for DCAwareRoundRobinPolicy |  Minor | Tools | Christopher Batey | Christopher Batey |
| [CASSANDRA-5977](https://issues.apache.org/jira/browse/CASSANDRA-5977) | Structure for cfstats output (JSON, YAML, or XML) |  Minor | Tools | Alyssa Kwan | Shogo Hoshii |
| [CASSANDRA-11571](https://issues.apache.org/jira/browse/CASSANDRA-11571) | Optimize the overlapping lookup, by calculating all the bounds in advance. |  Major | Compaction | Dikang Gu | Dikang Gu |
| [CASSANDRA-11310](https://issues.apache.org/jira/browse/CASSANDRA-11310) | Allow filtering on clustering columns for queries without secondary indexes |  Major | CQL, Secondary Indexes | Benjamin Lerer | Alex Petrov |
| [CASSANDRA-11412](https://issues.apache.org/jira/browse/CASSANDRA-11412) | Many sstablescanners opened during repair |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-10406](https://issues.apache.org/jira/browse/CASSANDRA-10406) | Nodetool supports to rebuild from specific ranges. |  Major | Tools | Dikang Gu | Dikang Gu |
| [CASSANDRA-11206](https://issues.apache.org/jira/browse/CASSANDRA-11206) | Support large partitions on the 3.0 sstable format |  Major | Local Write-Read Paths | Jonathan Ellis | Robert Stupp |
| [CASSANDRA-10134](https://issues.apache.org/jira/browse/CASSANDRA-10134) | Always require replace\_address to replace existing address |  Major | Distributed Metadata | Tyler Hobbs | Sam Tunnicliffe |
| [CASSANDRA-11213](https://issues.apache.org/jira/browse/CASSANDRA-11213) | Improve ClusteringPrefix hierarchy |  Major | . | Sylvain Lebresne | Branimir Lambov |
| [CASSANDRA-11352](https://issues.apache.org/jira/browse/CASSANDRA-11352) | Include units of metrics in the cassandra-stress tool |  Minor | Tools | Rajath Subramanyam | Giampaolo |
| [CASSANDRA-11593](https://issues.apache.org/jira/browse/CASSANDRA-11593) | getFunctions methods produce too much garbage |  Major | . | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-11555](https://issues.apache.org/jira/browse/CASSANDRA-11555) | Make prepared statement cache size configurable |  Minor | CQL | Robert Stupp | Robert Stupp |
| [CASSANDRA-11567](https://issues.apache.org/jira/browse/CASSANDRA-11567) | Update netty version |  Minor | . | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-10624](https://issues.apache.org/jira/browse/CASSANDRA-10624) | Support UDT in CQLSSTableWriter |  Major | CQL | Sylvain Lebresne | Alex Petrov |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11086](https://issues.apache.org/jira/browse/CASSANDRA-11086) | trunk eclipse-warnings |  Minor | Testing | Michael Shuler | Jason Brown |
| [CASSANDRA-11357](https://issues.apache.org/jira/browse/CASSANDRA-11357) | ClientWarningsTest fails after single partition batch warning changes |  Trivial | Testing | Joel Knighton | Joel Knighton |
| [CASSANDRA-11226](https://issues.apache.org/jira/browse/CASSANDRA-11226) | nodetool tablestats' keyspace-level metrics are wrong/misleading |  Minor | Tools | Tyler Hobbs | Sylvain Lebresne |
| [CASSANDRA-11480](https://issues.apache.org/jira/browse/CASSANDRA-11480) | Fix uptodate check for antlr files after 11372 |  Trivial | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-11462](https://issues.apache.org/jira/browse/CASSANDRA-11462) | IncomingStreamingConnection version check message wrong |  Trivial | . | Robert Stupp |  |
| [CASSANDRA-11329](https://issues.apache.org/jira/browse/CASSANDRA-11329) | Indexers are not informed when expired rows are encountered in compaction |  Major | Compaction | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-11515](https://issues.apache.org/jira/browse/CASSANDRA-11515) | C\* won't launch with whitespace in path on Windows |  Trivial | . | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-11524](https://issues.apache.org/jira/browse/CASSANDRA-11524) | Cqlsh default cqlver needs bumped |  Major | Tools | Philip Thompson | Philip Thompson |
| [CASSANDRA-11470](https://issues.apache.org/jira/browse/CASSANDRA-11470) | dtest failure in materialized\_views\_test.TestMaterializedViews.base\_replica\_repair\_test |  Major | . | Philip Thompson | Stefania |
| [CASSANDRA-11430](https://issues.apache.org/jira/browse/CASSANDRA-11430) | Add legacy notifications backward-support on deprecated repair methods |  Major | Observability, Streaming and Messaging | Nick Bailey | Paulo Motta |
| [CASSANDRA-11529](https://issues.apache.org/jira/browse/CASSANDRA-11529) | Checking if an unlogged batch is local is inefficient |  Critical | Coordination | Paulo Motta | Stefania |
| [CASSANDRA-11532](https://issues.apache.org/jira/browse/CASSANDRA-11532) | CqlConfigHelper requires both truststore and keystore to work with SSL encryption |  Major | . | Jacek Lewandowski | Jacek Lewandowski |
| [CASSANDRA-11556](https://issues.apache.org/jira/browse/CASSANDRA-11556) | PER PARTITION LIMIT does not work properly for multi-partition query with ORDER BY |  Major | . | Benjamin Lerer | Alex Petrov |
| [CASSANDRA-11485](https://issues.apache.org/jira/browse/CASSANDRA-11485) | ArithmeticException in avgFunctionForDecimal |  Minor | CQL | Nico Haller | Robert Stupp |
| [CASSANDRA-11513](https://issues.apache.org/jira/browse/CASSANDRA-11513) | Result set is not unique on primary key (cql) |  Major | Local Write-Read Paths | Tianshi Wang | Joel Knighton |
| [CASSANDRA-11553](https://issues.apache.org/jira/browse/CASSANDRA-11553) | hadoop.cql3.CqlRecordWriter does not close cluster on reconnect |  Major | Tools | Artem Aliev | Artem Aliev |
| [CASSANDRA-11620](https://issues.apache.org/jira/browse/CASSANDRA-11620) | help text in sstabledump references row keys when it should say partition keys |  Minor | Tools | Jon Haddad | Jon Haddad |
| [CASSANDRA-11523](https://issues.apache.org/jira/browse/CASSANDRA-11523) | server side exception on secondary index query through thrift |  Major | Core, Secondary Indexes | Ivan Georgiev | Sam Tunnicliffe |
| [CASSANDRA-11474](https://issues.apache.org/jira/browse/CASSANDRA-11474) | cqlsh: COPY FROM should use regular inserts for single statement batches |  Minor | Tools | Stefania | Stefania |
| [CASSANDRA-11549](https://issues.apache.org/jira/browse/CASSANDRA-11549) | cqlsh: COPY FROM ignores NULL values in conversion |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-11627](https://issues.apache.org/jira/browse/CASSANDRA-11627) | Streaming and other ops should filter out all LocalStrategy keyspaces, not just system keyspaces |  Minor | Streaming and Messaging | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-11544](https://issues.apache.org/jira/browse/CASSANDRA-11544) | NullPointerException if metrics reporter config file doesn't exist |  Minor | Configuration | Christopher Batey | Christopher Batey |
| [CASSANDRA-10756](https://issues.apache.org/jira/browse/CASSANDRA-10756) | Timeout failures in NativeTransportService.testConcurrentDestroys unit test |  Major | Lifecycle, Testing | Joel Knighton | Alex Petrov |
| [CASSANDRA-11633](https://issues.apache.org/jira/browse/CASSANDRA-11633) | cqlsh COPY FROM fails with []{} chars in UDT/tuple fields/values |  Minor | Tools | Robert Stupp | Robert Stupp |
| [CASSANDRA-11628](https://issues.apache.org/jira/browse/CASSANDRA-11628) | Fix the regression to CASSANDRA-3983 that got introduced by CASSANDRA-10679 |  Major | Tools | Wei Deng | Wei Deng |
| [CASSANDRA-9935](https://issues.apache.org/jira/browse/CASSANDRA-9935) | Repair fails with RuntimeException |  Major | . | mlowicki | Paulo Motta |
| [CASSANDRA-11654](https://issues.apache.org/jira/browse/CASSANDRA-11654) | sstabledump is not able to properly print out SSTable that may contain historical (but "shadowed") row tombstone |  Major | Tools | Wei Deng | Yuki Morishita |
| [CASSANDRA-11642](https://issues.apache.org/jira/browse/CASSANDRA-11642) | sstabledump and sstableverify need to be added to deb packages |  Major | Tools | Attila Szucs | T Jake Luciani |
| [CASSANDRA-11630](https://issues.apache.org/jira/browse/CASSANDRA-11630) | Make cython optional in pylib/setup.py |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-11631](https://issues.apache.org/jira/browse/CASSANDRA-11631) | cqlsh COPY FROM fails for null values with non-prepared statements |  Minor | Tools | Robert Stupp | Stefania |
| [CASSANDRA-11646](https://issues.apache.org/jira/browse/CASSANDRA-11646) | SSTableWriter output discrepancy |  Major | . | T Jake Luciani | Stefania |
| [CASSANDRA-11660](https://issues.apache.org/jira/browse/CASSANDRA-11660) | Dubious call to remove in CompactionManager.java |  Minor | Compaction | Max Schaefer | Marcus Eriksson |
| [CASSANDRA-11137](https://issues.apache.org/jira/browse/CASSANDRA-11137) | JSON datetime formatting needs timezone |  Major | CQL | Stefania | Alex Petrov |
| [CASSANDRA-11502](https://issues.apache.org/jira/browse/CASSANDRA-11502) | Fix denseness and column metadata updates coming from Thrift |  Minor | Distributed Metadata | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-11600](https://issues.apache.org/jira/browse/CASSANDRA-11600) | Don't require HEAP\_NEW\_SIZE to be set when using G1 |  Minor | . | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-11669](https://issues.apache.org/jira/browse/CASSANDRA-11669) | RangeName queries might not return all the results |  Major | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-11510](https://issues.apache.org/jira/browse/CASSANDRA-11510) | Clustering key and secondary index |  Major | CQL, Secondary Indexes | Julien Muller | Sam Tunnicliffe |
| [CASSANDRA-11603](https://issues.apache.org/jira/browse/CASSANDRA-11603) | PER PARTITION LIMIT does not work properly for SinglePartition |  Major | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-11655](https://issues.apache.org/jira/browse/CASSANDRA-11655) | sstabledump doesn't print out tombstone information for deleted collection column |  Major | Tools | Wei Deng | Chris Lohfink |
| [CASSANDRA-11609](https://issues.apache.org/jira/browse/CASSANDRA-11609) | Nested UDTs cause error when migrating 2.x schema to trunk |  Major | Distributed Metadata | Russ Hatch | Tyler Hobbs |
| [CASSANDRA-11573](https://issues.apache.org/jira/browse/CASSANDRA-11573) | cqlsh fails with undefined symbol: PyUnicodeUCS2\_DecodeUTF8 |  Major | . | Oli Schacher | Michael Shuler |
| [CASSANDRA-11391](https://issues.apache.org/jira/browse/CASSANDRA-11391) | "class declared as inner class" error when using UDF |  Critical | CQL | DOAN DuyHai | Robert Stupp |
| [CASSANDRA-11629](https://issues.apache.org/jira/browse/CASSANDRA-11629) | java.lang.UnsupportedOperationException when selecting rows with counters |  Major | . | Arnd Hannemann | Alex Petrov |
| [CASSANDRA-11618](https://issues.apache.org/jira/browse/CASSANDRA-11618) | Removing an element from map\<any type, tinyint/smallint\> corrupts commitlog |  Critical | Local Write-Read Paths | Artem Chudinov | Sylvain Lebresne |
| [CASSANDRA-11602](https://issues.apache.org/jira/browse/CASSANDRA-11602) | Materialized View Does Not Have Static Columns |  Major | Coordination, Materialized Views | Ravishankar Rajendran | Carl Yeksigian |
| [CASSANDRA-11640](https://issues.apache.org/jira/browse/CASSANDRA-11640) | javadoc for UnfilteredSerializer is not formatted correctly |  Minor | . | Wei Deng | Wei Deng |
| [CASSANDRA-11710](https://issues.apache.org/jira/browse/CASSANDRA-11710) | Cassandra dies with OOM when running stress |  Major | . | Marcus Eriksson | Branimir Lambov |
| [CASSANDRA-11725](https://issues.apache.org/jira/browse/CASSANDRA-11725) | Check for unnecessary JMX port setting in env vars at startup |  Minor | Lifecycle | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-11613](https://issues.apache.org/jira/browse/CASSANDRA-11613) | dtest failure in upgrade\_tests.cql\_tests.TestCQLNodes2RF1\_2\_2\_HEAD\_UpTo\_Trunk.more\_user\_types\_test |  Major | Local Write-Read Paths | Russ Hatch | Tyler Hobbs |
| [CASSANDRA-11905](https://issues.apache.org/jira/browse/CASSANDRA-11905) | Index building fails to start CFS.readOrdering when reading |  Critical | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-11763](https://issues.apache.org/jira/browse/CASSANDRA-11763) | Failure to read non-shallow pre-3.0 sstable index entries |  Major | . | Philip Thompson | Robert Stupp |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11675](https://issues.apache.org/jira/browse/CASSANDRA-11675) | multiple dtest failure in cqlsh\_tests.cqlsh\_copy\_tests.CqlshCopyTest |  Major | . | Russ Hatch | DS Test Eng |
| [CASSANDRA-11676](https://issues.apache.org/jira/browse/CASSANDRA-11676) | dtest failure in cqlsh\_tests.cqlsh\_copy\_tests.CqlshCopyTest.test\_copy\_from\_with\_large\_cql\_rows |  Major | . | Russ Hatch | Jim Witschey |
| [CASSANDRA-11799](https://issues.apache.org/jira/browse/CASSANDRA-11799) | dtest failure in cqlsh\_tests.cqlsh\_tests.TestCqlsh.test\_unicode\_syntax\_error |  Major | Testing | Philip Thompson | Tyler Hobbs |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5863](https://issues.apache.org/jira/browse/CASSANDRA-5863) | In process (uncompressed) page cache |  Major | . | T Jake Luciani | Branimir Lambov |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10853](https://issues.apache.org/jira/browse/CASSANDRA-10853) | deb package migration to dh\_python2 |  Major | Packaging | Michael Shuler | Michael Shuler |


