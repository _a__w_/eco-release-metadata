
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.0.16 - 2015-06-22



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9455](https://issues.apache.org/jira/browse/CASSANDRA-9455) | Rely on TCP keepalive vs failure detector for streaming connections |  Major | . | Omid Aladini | Omid Aladini |
| [CASSANDRA-5969](https://issues.apache.org/jira/browse/CASSANDRA-5969) | Allow JVM\_OPTS to be passed to sstablescrub |  Major | Tools | Adam Hattrell | Stefania |
| [CASSANDRA-9436](https://issues.apache.org/jira/browse/CASSANDRA-9436) | Expose rpc\_address and broadcast\_address of each Cassandra node |  Major | Distributed Metadata | Piotr Kołaczkowski | Carl Yeksigian |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7212](https://issues.apache.org/jira/browse/CASSANDRA-7212) | Allow to switch user within CQLSH session |  Major | CQL | Jose Martinez Poblete | Carl Yeksigian |
| [CASSANDRA-9529](https://issues.apache.org/jira/browse/CASSANDRA-9529) | Standardize quoting in Unix scripts |  Minor | Tools | Stefania | Stefania |
| [CASSANDRA-9571](https://issues.apache.org/jira/browse/CASSANDRA-9571) | Set HAS\_MORE\_PAGES flag when PagingState is null |  Minor | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-9496](https://issues.apache.org/jira/browse/CASSANDRA-9496) | ArrivalWindow should use primitives |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-9532](https://issues.apache.org/jira/browse/CASSANDRA-9532) | Provide access to select statement's real column definitions |  Major | CQL | mck | Sam Tunnicliffe |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9300](https://issues.apache.org/jira/browse/CASSANDRA-9300) | token-generator - generated tokens too long |  Minor | Tools | Robert Stupp | Robert Stupp |
| [CASSANDRA-9310](https://issues.apache.org/jira/browse/CASSANDRA-9310) | Table change response returns as keyspace change response |  Major | CQL | Kishan Karunaratne | Carl Yeksigian |
| [CASSANDRA-8940](https://issues.apache.org/jira/browse/CASSANDRA-8940) | Inconsistent select count and select distinct |  Major | Local Write-Read Paths | Frens Jan Rumph | Benjamin Lerer |
| [CASSANDRA-9334](https://issues.apache.org/jira/browse/CASSANDRA-9334) | Returning null from a trigger does not abort the write |  Major | Coordination | Brandon Williams | Sam Tunnicliffe |
| [CASSANDRA-9406](https://issues.apache.org/jira/browse/CASSANDRA-9406) | Add Option to Not Validate Atoms During Scrub |  Minor | Tools | Jordan West | Jordan West |
| [CASSANDRA-9466](https://issues.apache.org/jira/browse/CASSANDRA-9466) | CacheService.KeyCacheSerializer affects cache hit rate |  Minor | Tools | sankalp kohli | sankalp kohli |
| [CASSANDRA-9411](https://issues.apache.org/jira/browse/CASSANDRA-9411) | Bound statement executions fail after adding a collection-type column |  Major | CQL | Jorge Bay | Benjamin Lerer |
| [CASSANDRA-9488](https://issues.apache.org/jira/browse/CASSANDRA-9488) | CrcCheckChanceTest.testChangingCrcCheckChance fails with stack overflow |  Minor | Local Write-Read Paths | Ariel Weisberg | T Jake Luciani |
| [CASSANDRA-8502](https://issues.apache.org/jira/browse/CASSANDRA-8502) | Static columns returning null for pages after first |  Major | . | Flavien Charlon | Tyler Hobbs |
| [CASSANDRA-9485](https://issues.apache.org/jira/browse/CASSANDRA-9485) | RangeTombstoneListTest.addAllRandomTest failed on trunk |  Minor | Local Write-Read Paths | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-9478](https://issues.apache.org/jira/browse/CASSANDRA-9478) | SSTables are not always properly marked suspected |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-9486](https://issues.apache.org/jira/browse/CASSANDRA-9486) | LazilyCompactedRow accumulates all expired RangeTombstones |  Critical | . | Benedict | Sylvain Lebresne |
| [CASSANDRA-9564](https://issues.apache.org/jira/browse/CASSANDRA-9564) | Backport CASSANDRA-9057 to 2.0 |  Major | . | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-9572](https://issues.apache.org/jira/browse/CASSANDRA-9572) | DateTieredCompactionStrategy fails to combine SSTables correctly when TTL is used. |  Major | Compaction | Antti Nissinen | Marcus Eriksson |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9306](https://issues.apache.org/jira/browse/CASSANDRA-9306) | Test coverage for cqlsh COPY |  Major | . | Tyler Hobbs | Jim Witschey |
| [CASSANDRA-8656](https://issues.apache.org/jira/browse/CASSANDRA-8656) | long-test LongLeveledCompactionStrategyTest flaps in 2.0 |  Minor | Testing | Michael Shuler | Stefania |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7558](https://issues.apache.org/jira/browse/CASSANDRA-7558) | Document users and permissions in CQL docs |  Minor | Documentation and Website | Tyler Hobbs | Sam Tunnicliffe |


