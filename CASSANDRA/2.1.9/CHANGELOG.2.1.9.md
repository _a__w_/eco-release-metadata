
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1.9 - 2015-08-28



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9601](https://issues.apache.org/jira/browse/CASSANDRA-9601) | Allow an initial connection timeout to be set in cqlsh |  Major | Tools | Mike Adamson | Stefania |
| [CASSANDRA-9591](https://issues.apache.org/jira/browse/CASSANDRA-9591) | Scrub (recover) sstables even when -Index.db is missing |  Major | . | mck | mck |
| [CASSANDRA-9544](https://issues.apache.org/jira/browse/CASSANDRA-9544) | Allow specification of TLS protocol to use for cqlsh |  Major | Tools | Jesse Szwedko | Jesse Szwedko |
| [CASSANDRA-9682](https://issues.apache.org/jira/browse/CASSANDRA-9682) | setting log4j.logger.org.apache.cassandra=DEBUG causes keyspace username/password to show up in system.log |  Major | Observability | Victor Chen | Sam Tunnicliffe |
| [CASSANDRA-9793](https://issues.apache.org/jira/browse/CASSANDRA-9793) | Log when messages are dropped due to cross\_node\_timeout |  Major | Streaming and Messaging | Brandon Williams | Stefania |
| [CASSANDRA-9827](https://issues.apache.org/jira/browse/CASSANDRA-9827) | Add consistency level to tracing ouput |  Minor | . | Alec Grieser |  |
| [CASSANDRA-9896](https://issues.apache.org/jira/browse/CASSANDRA-9896) | Add ability to disable commitlog recycling |  Major | . | Brandon Williams | Benedict |
| [CASSANDRA-9533](https://issues.apache.org/jira/browse/CASSANDRA-9533) | Make batch commitlog mode easier to tune |  Major | . | Jonathan Ellis | Benedict |
| [CASSANDRA-10015](https://issues.apache.org/jira/browse/CASSANDRA-10015) | Create tool to debug why expired sstables are not getting dropped |  Major | Tools | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-10004](https://issues.apache.org/jira/browse/CASSANDRA-10004) | Allow changing default encoding on cqlsh |  Minor | Tools | Paulo Motta | Paulo Motta |
| [CASSANDRA-10083](https://issues.apache.org/jira/browse/CASSANDRA-10083) | Revert AutoSavingCache.IStreamFactory to return OutputStream |  Major | . | Blake Eggleston | Blake Eggleston |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9662](https://issues.apache.org/jira/browse/CASSANDRA-9662) | compactionManager reporting wrong pendingtasks |  Minor | CQL | Tony Xu | Yuki Morishita |
| [CASSANDRA-9760](https://issues.apache.org/jira/browse/CASSANDRA-9760) | clientutil-jar and tests broken |  Minor | Configuration | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-9740](https://issues.apache.org/jira/browse/CASSANDRA-9740) | Can't transition from write survey to normal mode |  Trivial | . | Jason Brown | Jason Brown |
| [CASSANDRA-9686](https://issues.apache.org/jira/browse/CASSANDRA-9686) | FSReadError and LEAK DETECTED after upgrading |  Major | Local Write-Read Paths | Andreas Schnitzerling | Stefania |
| [CASSANDRA-7973](https://issues.apache.org/jira/browse/CASSANDRA-7973) | cqlsh connect error "member\_descriptor' object is not callable" |  Minor | Tools | Digant Modha | Stefania |
| [CASSANDRA-9519](https://issues.apache.org/jira/browse/CASSANDRA-9519) | CASSANDRA-8448 Doesn't seem to be fixed |  Major | . | Jeremiah Jordan | Sylvain Lebresne |
| [CASSANDRA-9837](https://issues.apache.org/jira/browse/CASSANDRA-9837) | Fix broken logging for "empty" flushes in Memtable |  Trivial | . | Michael McKibben |  |
| [CASSANDRA-9858](https://issues.apache.org/jira/browse/CASSANDRA-9858) | SelectStatement.Parameters fields should be inspectable by custom indexes and query handlers |  Major | . | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-7357](https://issues.apache.org/jira/browse/CASSANDRA-7357) | Cleaning up snapshots on startup leftover from repairs that got interrupted |  Minor | Streaming and Messaging | Stephen Johnson | Paulo Motta |
| [CASSANDRA-9582](https://issues.apache.org/jira/browse/CASSANDRA-9582) | MarshalException after upgrading to 2.1.6 |  Major | . | Tom van den Berge | Sylvain Lebresne |
| [CASSANDRA-9765](https://issues.apache.org/jira/browse/CASSANDRA-9765) | checkForEndpointCollision fails for legitimate collisions |  Major | Distributed Metadata | Richard Low | Stefania |
| [CASSANDRA-8735](https://issues.apache.org/jira/browse/CASSANDRA-8735) | Batch log replication is not randomized when there are only 2 racks |  Minor | . | Yuki Morishita | Mihai Suteu |
| [CASSANDRA-9849](https://issues.apache.org/jira/browse/CASSANDRA-9849) | Paging changes to 2.1/2.2 for backward compatibilty with 3.0 |  Major | . | Sylvain Lebresne | Tyler Hobbs |
| [CASSANDRA-9382](https://issues.apache.org/jira/browse/CASSANDRA-9382) | Snapshot file descriptors not getting purged (possible fd leak) |  Major | Local Write-Read Paths | Mark Curtis | Yuki Morishita |
| [CASSANDRA-9899](https://issues.apache.org/jira/browse/CASSANDRA-9899) | If compaction is disabled in schema, you can't enable a single node through nodetool |  Major | Compaction | Jeremiah Jordan | Marcus Eriksson |
| [CASSANDRA-7757](https://issues.apache.org/jira/browse/CASSANDRA-7757) | Possible Atomicity Violations in StreamSession and ThriftSessionManager |  Minor | Streaming and Messaging | Diogo Sousa | Paulo Motta |
| [CASSANDRA-9871](https://issues.apache.org/jira/browse/CASSANDRA-9871) | Cannot replace token does not exist - DN node removed as Fat Client |  Major | Distributed Metadata | Sebastian Estevez | Stefania |
| [CASSANDRA-9959](https://issues.apache.org/jira/browse/CASSANDRA-9959) | Expected bloom filter size should not be an int |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9963](https://issues.apache.org/jira/browse/CASSANDRA-9963) | Compaction not starting for new tables |  Major | Compaction | Jeremiah Jordan | Marcus Eriksson |
| [CASSANDRA-9031](https://issues.apache.org/jira/browse/CASSANDRA-9031) | nodetool info -T throws ArrayOutOfBounds when the node has not joined the cluster |  Major | Tools | Ron Kuris | Yuki Morishita |
| [CASSANDRA-9962](https://issues.apache.org/jira/browse/CASSANDRA-9962) | WaitQueueTest is flakey |  Minor | . | Benedict | Benedict |
| [CASSANDRA-9998](https://issues.apache.org/jira/browse/CASSANDRA-9998) | LEAK DETECTED with snapshot/sequential repairs |  Major | Streaming and Messaging | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-9129](https://issues.apache.org/jira/browse/CASSANDRA-9129) | HintedHandoff in pending state forever after upgrading to 2.0.14 from 2.0.11 and 2.0.12 |  Major | Observability | Russ Lavoie | Sam Tunnicliffe |
| [CASSANDRA-9777](https://issues.apache.org/jira/browse/CASSANDRA-9777) | If you have a ~/.cqlshrc and a ~/.cassandra/cqlshrc, cqlsh will overwrite the latter with the former |  Major | . | Jon Moses | David Kua |
| [CASSANDRA-10000](https://issues.apache.org/jira/browse/CASSANDRA-10000) | Dates before 1970-01-01 are not formatted correctly on cqlsh\\Windows |  Minor | Tools | Paulo Motta | Paulo Motta |
| [CASSANDRA-9965](https://issues.apache.org/jira/browse/CASSANDRA-9965) | Add new JMX methods to change local compaction strategy |  Minor | Compaction | Aleksey Yeschenko | Marcus Eriksson |
| [CASSANDRA-8515](https://issues.apache.org/jira/browse/CASSANDRA-8515) | Commit log stop policy not enforced correctly during startup |  Minor | Coordination, Lifecycle | Richard Low | Paulo Motta |
| [CASSANDRA-9683](https://issues.apache.org/jira/browse/CASSANDRA-9683) | Get mucher higher load and latencies after upgrading from 2.1.6 to cassandra 2.1.7 |  Major | Compaction, Local Write-Read Paths | Loic Lambiel | Ariel Weisberg |
| [CASSANDRA-9882](https://issues.apache.org/jira/browse/CASSANDRA-9882) | DTCS (maybe other strategies) can block flushing when there are lots of sstables |  Major | Compaction | Jeremiah Jordan | Marcus Eriksson |
| [CASSANDRA-10048](https://issues.apache.org/jira/browse/CASSANDRA-10048) | cassandra-stress - Decimal is a BigInt not a Double |  Major | . | Sebastian Estevez |  |
| [CASSANDRA-10168](https://issues.apache.org/jira/browse/CASSANDRA-10168) | CassandraAuthorizer.authorize must throw exception when lookup of any auth table fails |  Major | . | Vishy Kasar | Vishy Kasar |
| [CASSANDRA-9898](https://issues.apache.org/jira/browse/CASSANDRA-9898) | cqlsh crashes if it load a utf-8 file. |  Minor | Tools | Yasuharu Goto | Yasuharu Goto |
| [CASSANDRA-9460](https://issues.apache.org/jira/browse/CASSANDRA-9460) | NullPointerException Creating Digest |  Major | Local Write-Read Paths | Tyler Hobbs | T Jake Luciani |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7342](https://issues.apache.org/jira/browse/CASSANDRA-7342) | CAS writes do not have hint functionality. |  Major | . | sankalp kohli | sankalp kohli |


