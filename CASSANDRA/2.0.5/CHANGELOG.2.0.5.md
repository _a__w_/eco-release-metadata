
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.0.5 - 2014-02-07



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6286](https://issues.apache.org/jira/browse/CASSANDRA-6286) | Add JMX auth password file location info to cassandra-env.sh |  Trivial | Configuration | Peter Halliday | Peter Halliday |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4268](https://issues.apache.org/jira/browse/CASSANDRA-4268) | Expose full stop() operation through JMX |  Minor | . | Tyler Hobbs | Lyuben Todorov |
| [CASSANDRA-6158](https://issues.apache.org/jira/browse/CASSANDRA-6158) | Nodetool command to purge hints |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-2238](https://issues.apache.org/jira/browse/CASSANDRA-2238) | Allow nodetool to print out hostnames given an option |  Trivial | Tools | Joaquin Casares | Daneel S. Yaitskov |
| [CASSANDRA-6480](https://issues.apache.org/jira/browse/CASSANDRA-6480) | Custom secondary index options in CQL3 |  Minor | Secondary Indexes | Andrés de la Peña | Aleksey Yeschenko |
| [CASSANDRA-4288](https://issues.apache.org/jira/browse/CASSANDRA-4288) | prevent thrift server from starting before gossip has settled |  Major | . | Peter Schuller | Chris Burroughs |
| [CASSANDRA-6580](https://issues.apache.org/jira/browse/CASSANDRA-6580) | Deadcode in AtomicSortedColumns |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6595](https://issues.apache.org/jira/browse/CASSANDRA-6595) | Avoid special casing received/expected counts in CAS timeout exceptions |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6505](https://issues.apache.org/jira/browse/CASSANDRA-6505) | counters++ global shards 2.0 back port |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6597](https://issues.apache.org/jira/browse/CASSANDRA-6597) | Tombstoned cells warning log improvement |  Trivial | . | Kévin LOVATO | Kévin LOVATO |
| [CASSANDRA-6609](https://issues.apache.org/jira/browse/CASSANDRA-6609) | Reduce Bloom Filter Garbage Allocation |  Major | . | Benedict | Benedict |
| [CASSANDRA-6637](https://issues.apache.org/jira/browse/CASSANDRA-6637) | Add AbstractCompactionStrategy.startup() method |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6641](https://issues.apache.org/jira/browse/CASSANDRA-6641) | Switch stress to use ITransportFactory to obtain client connections |  Minor | Tools | Sam Tunnicliffe | Sam Tunnicliffe |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5701](https://issues.apache.org/jira/browse/CASSANDRA-5701) | Apache.Cassandra.Cassandra.get\_count will disconnect but not throw InvalidRequestException when column family is not exist. |  Minor | CQL | yuemaoxing | Lyuben Todorov |
| [CASSANDRA-6531](https://issues.apache.org/jira/browse/CASSANDRA-6531) | Failure to start after unclean shutdown - java.lang.IllegalArgumentException: bufferSize must be positive |  Major | . | Nikolai Grigoriev | Jonathan Ellis |
| [CASSANDRA-6086](https://issues.apache.org/jira/browse/CASSANDRA-6086) | Node refuses to start with exception in ColumnFamilyStore.removeUnfinishedCompactionLeftovers when find that some to be removed files are already removed |  Major | . | Oleg Anastasyev | Tyler Hobbs |
| [CASSANDRA-6131](https://issues.apache.org/jira/browse/CASSANDRA-6131) | JAVA\_HOME on cassandra-env.sh is ignored on Debian packages |  Major | Packaging | Sebastián Lacuesta | Eric Evans |
| [CASSANDRA-6550](https://issues.apache.org/jira/browse/CASSANDRA-6550) | C\* should be able to throttle batchlog processing |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6529](https://issues.apache.org/jira/browse/CASSANDRA-6529) | sstableloader shows no progress or errors when pointed at a bad directory |  Minor | Tools | Tyler Hobbs | Marcus Eriksson |
| [CASSANDRA-6554](https://issues.apache.org/jira/browse/CASSANDRA-6554) | During upgrade from 1.2 -\> 2.0, upgraded node sees other nodes as Down |  Major | . | Michael Shuler | Brandon Williams |
| [CASSANDRA-6053](https://issues.apache.org/jira/browse/CASSANDRA-6053) | system.peers table not updated after decommissioning nodes in C\* 2.0 |  Major | . | Guyon Moree | Tyler Hobbs |
| [CASSANDRA-6407](https://issues.apache.org/jira/browse/CASSANDRA-6407) | CQL/Thrift request hangs forever when querying more than certain amount of data |  Major | . | Nikolai Grigoriev | Pavel Yaskevich |
| [CASSANDRA-6373](https://issues.apache.org/jira/browse/CASSANDRA-6373) | describe\_ring hangs with hsha thrift server |  Major | . | Nick Bailey | Pavel Yaskevich |
| [CASSANDRA-6567](https://issues.apache.org/jira/browse/CASSANDRA-6567) | StackOverflowError with big IN list |  Minor | . | Dmitry Shohov | Sylvain Lebresne |
| [CASSANDRA-6571](https://issues.apache.org/jira/browse/CASSANDRA-6571) | Quickly restarted nodes can list others as down indefinitely |  Major | . | Richard Low | sankalp kohli |
| [CASSANDRA-6495](https://issues.apache.org/jira/browse/CASSANDRA-6495) | LOCAL\_SERIAL  use QUORUM consistency level to validate expected columns |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-6465](https://issues.apache.org/jira/browse/CASSANDRA-6465) | DES scores fluctuate too much for cache pinning |  Minor | . | Chris Burroughs | Tyler Hobbs |
| [CASSANDRA-6584](https://issues.apache.org/jira/browse/CASSANDRA-6584) | LOCAL\_SERIAL doesn't work from Thrift |  Major | CQL | Nicolas Favre-Felix | Brandon Williams |
| [CASSANDRA-6598](https://issues.apache.org/jira/browse/CASSANDRA-6598) | upgradesstables does not upgrade indexes causing startup error. |  Major | . | Ryan McGuire | Brandon Williams |
| [CASSANDRA-6569](https://issues.apache.org/jira/browse/CASSANDRA-6569) | Batchlog replays copy the entire batchlog table into the heap |  Major | . | Rick Branson | Aleksey Yeschenko |
| [CASSANDRA-6604](https://issues.apache.org/jira/browse/CASSANDRA-6604) | AssertionError: Verb COUNTER\_MUTATION should not legally be dropped |  Major | . | Bartłomiej Romański |  |
| [CASSANDRA-4375](https://issues.apache.org/jira/browse/CASSANDRA-4375) | FD incorrectly using RPC timeout to ignore gossip heartbeats |  Major | . | Peter Schuller | Brandon Williams |
| [CASSANDRA-6564](https://issues.apache.org/jira/browse/CASSANDRA-6564) | Gossiper failed with ArrayIndexOutOfBoundsException |  Minor | . | Shao-Chuan Wang | Tyler Hobbs |
| [CASSANDRA-6618](https://issues.apache.org/jira/browse/CASSANDRA-6618) | NullPointerException while stopping/draining if native transport wasn't started |  Minor | . | Ravi Prasad | Ravi Prasad |
| [CASSANDRA-6210](https://issues.apache.org/jira/browse/CASSANDRA-6210) | Repair hangs when a new datacenter is added to a cluster |  Major | . | Russell Spitzer | Yuki Morishita |
| [CASSANDRA-6606](https://issues.apache.org/jira/browse/CASSANDRA-6606) | avoid maybeResolveForRepair twice |  Minor | . | yangwei | yangwei |
| [CASSANDRA-6629](https://issues.apache.org/jira/browse/CASSANDRA-6629) | Coordinator's "java.lang.ArrayIndexOutOfBoundsException: -1" with CL \> 1 |  Major | CQL | Roman Skvazh | Mikhail Stepura |
| [CASSANDRA-6555](https://issues.apache.org/jira/browse/CASSANDRA-6555) | AbstractQueryPager.DiscardFirst is still broken |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6470](https://issues.apache.org/jira/browse/CASSANDRA-6470) | ArrayIndexOutOfBoundsException on range query from client |  Major | . | Enrico Scalavino | Sylvain Lebresne |
| [CASSANDRA-6635](https://issues.apache.org/jira/browse/CASSANDRA-6635) | Rebuilding secondary indexes leaks SSTable references |  Major | Secondary Indexes | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-6615](https://issues.apache.org/jira/browse/CASSANDRA-6615) | Changing the IP of a node on a live cluster leaves gossip infos and throws Exceptions |  Major | . | Fabien Rousseau | Brandon Williams |
| [CASSANDRA-6638](https://issues.apache.org/jira/browse/CASSANDRA-6638) | SSTableScanner can Skip Rows with vnodes |  Blocker | . | Tyler Hobbs | Sylvain Lebresne |
| [CASSANDRA-6503](https://issues.apache.org/jira/browse/CASSANDRA-6503) | sstables from stalled repair sessions become live after a reboot and can resurrect deleted data |  Minor | . | Jeremiah Jordan | Jason Brown |
| [CASSANDRA-6517](https://issues.apache.org/jira/browse/CASSANDRA-6517) | Loss of secondary index entries if nodetool cleanup called before compaction |  Major | CQL, Secondary Indexes | Christoph Werres | Sam Tunnicliffe |
| [CASSANDRA-5930](https://issues.apache.org/jira/browse/CASSANDRA-5930) | Offline scrubs can choke on broken files |  Minor | . | Jeremiah Jordan | Tyler Hobbs |
| [CASSANDRA-6628](https://issues.apache.org/jira/browse/CASSANDRA-6628) | Cassandra crashes on Solaris sparcv9 using java 64bit |  Major | . | Dmitry Shohov | Dmitry Shohov |
| [CASSANDRA-6648](https://issues.apache.org/jira/browse/CASSANDRA-6648) | Race condition during node bootstrapping |  Critical | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-6418](https://issues.apache.org/jira/browse/CASSANDRA-6418) | auto\_snapshots are not removable via 'nodetool clearsnapshot' |  Minor | Tools | J. Ryan Earl | Lyuben Todorov |
| [CASSANDRA-6592](https://issues.apache.org/jira/browse/CASSANDRA-6592) | IllegalArgumentException when Preparing Statements |  Critical | . | Tyler Hobbs | Sylvain Lebresne |


