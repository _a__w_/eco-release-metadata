
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.6.4 - 2010-07-31



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-685](https://issues.apache.org/jira/browse/CASSANDRA-685) | add backpressure to StorageProxy |  Minor | . | Jonathan Ellis | Jonathan Ellis |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1229](https://issues.apache.org/jira/browse/CASSANDRA-1229) | avoid queuing multiple hint deliveries for the same endpoint |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1232](https://issues.apache.org/jira/browse/CASSANDRA-1232) | UTF8Type.compare() is slow and dangerous |  Major | . | Folke Behrens | Nick Bailey |
| [CASSANDRA-1260](https://issues.apache.org/jira/browse/CASSANDRA-1260) | make streaming, HH threads low-priority as well |  Minor | . | Jonathan Ellis | Tyler Hobbs |
| [CASSANDRA-1277](https://issues.apache.org/jira/browse/CASSANDRA-1277) | improve logging messages for DiskAccessMode in 0.6 and trunk |  Trivial | . | Robert Coli | Robert Coli |
| [CASSANDRA-1275](https://issues.apache.org/jira/browse/CASSANDRA-1275) | Log threadpool stats after excessive GC |  Trivial | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-1138](https://issues.apache.org/jira/browse/CASSANDRA-1138) | remove Gossiper.MAX\_GOSSIP\_PACKET\_SIZE |  Minor | . | Jonathan Ellis | Brandon Williams |
| [CASSANDRA-1280](https://issues.apache.org/jira/browse/CASSANDRA-1280) | make storage-conf.xml optional for Hadoop jobs |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1047](https://issues.apache.org/jira/browse/CASSANDRA-1047) | Get hadoop ColumnFamily metadata from describe\_keyspace instead of config file |  Major | . | Jonathan Ellis | Jonathan Ellis |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1259](https://issues.apache.org/jira/browse/CASSANDRA-1259) | incorrect package name for property\_snitch class files |  Minor | . | Eric Evans | Eric Evans |
| [CASSANDRA-1262](https://issues.apache.org/jira/browse/CASSANDRA-1262) | stress.py stdev option should be float not int |  Trivial | . | Oren Benjamin | Brandon Williams |
| [CASSANDRA-1289](https://issues.apache.org/jira/browse/CASSANDRA-1289) | GossipTimerTask stops running if an Exception occurs |  Major | . | Wade Simmons | Brandon Williams |
| [CASSANDRA-1297](https://issues.apache.org/jira/browse/CASSANDRA-1297) | commitlog recover bug |  Critical | . | jingfengtan | jingfengtan |
| [CASSANDRA-1221](https://issues.apache.org/jira/browse/CASSANDRA-1221) | loadbalance operation never completes on a 3 node cluster |  Major | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-1236](https://issues.apache.org/jira/browse/CASSANDRA-1236) | when I start up the cassandra-cli, a ClassNotFoundException occured:java.lang.ClassNotFoundException: org.apache.cassandra.cli.CliMain |  Minor | Tools | ialand | Gary Dusbabek |
| [CASSANDRA-1093](https://issues.apache.org/jira/browse/CASSANDRA-1093) | BinaryMemtable interface silently dropping data. |  Minor | . | Toby Jungen | Jonathan Ellis |
| [CASSANDRA-1317](https://issues.apache.org/jira/browse/CASSANDRA-1317) | StorageProxy ignores snitch when determining whether to do a local read |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1323](https://issues.apache.org/jira/browse/CASSANDRA-1323) | digest mismatches are processed serially |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1274](https://issues.apache.org/jira/browse/CASSANDRA-1274) | Exception while recovering commitlog when debug logging enabled |  Minor | . | Johan Oskarsson | Matthew F. Dennis |
| [CASSANDRA-1316](https://issues.apache.org/jira/browse/CASSANDRA-1316) | Read repair does not always work correctly |  Major | . | Brandon Williams | Brandon Williams |


