
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1 rc2 - 2014-06-26



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7273](https://issues.apache.org/jira/browse/CASSANDRA-7273) | expose global ColumnFamily metrics |  Minor | . | Richard Wagner | Chris Lohfink |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6934](https://issues.apache.org/jira/browse/CASSANDRA-6934) | Optimise Byte + CellName comparisons |  Major | . | Benedict | Benedict |
| [CASSANDRA-7328](https://issues.apache.org/jira/browse/CASSANDRA-7328) | Use LOCAL\_ONE for non-su auth queries |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6556](https://issues.apache.org/jira/browse/CASSANDRA-6556) | Update Pig dependency to latest stable before releasing 2.1 |  Major | . | Jeremy Hanna | Brandon Williams |
| [CASSANDRA-7314](https://issues.apache.org/jira/browse/CASSANDRA-7314) | Add keyspace option to DROP INDEX statement |  Minor | CQL | Andrés de la Peña | Andrés de la Peña |
| [CASSANDRA-7338](https://issues.apache.org/jira/browse/CASSANDRA-7338) | CFS.getRangeSlice should update latency metrics |  Trivial | . | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-7327](https://issues.apache.org/jira/browse/CASSANDRA-7327) | Reduce new CQL test time |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-7136](https://issues.apache.org/jira/browse/CASSANDRA-7136) | Change default paths to CASSANDRA\_HOME instead of /var |  Major | . | Jonathan Ellis | Tyler Hobbs |
| [CASSANDRA-7359](https://issues.apache.org/jira/browse/CASSANDRA-7359) | Optimize locking in PaxosState |  Minor | . | sankalp kohli | Aleksey Yeschenko |
| [CASSANDRA-7366](https://issues.apache.org/jira/browse/CASSANDRA-7366) | Use node's hostId instead of generating counterId-s |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-7333](https://issues.apache.org/jira/browse/CASSANDRA-7333) | gossipinfo should include the generation |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-7348](https://issues.apache.org/jira/browse/CASSANDRA-7348) | Modify system tcp keepalive settings on Windows install scripts |  Minor | Packaging | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-6539](https://issues.apache.org/jira/browse/CASSANDRA-6539) | Track metrics at a keyspace level as well as column family level |  Minor | . | Nick Bailey | Brandon Williams |
| [CASSANDRA-7236](https://issues.apache.org/jira/browse/CASSANDRA-7236) | Extend connection backlog for MessageService |  Major | . | Oleg Anastasyev | Oleg Anastasyev |
| [CASSANDRA-7266](https://issues.apache.org/jira/browse/CASSANDRA-7266) | Allow cqlsh shell ignore .cassandra permission errors and not fail to open the cqlsh shell. |  Minor | Tools | Carolyn Jung |  |
| [CASSANDRA-7346](https://issues.apache.org/jira/browse/CASSANDRA-7346) | Modify reconcile logic to always pick a tombstone over a counter cell |  Minor | . | Richard Low | Aleksey Yeschenko |
| [CASSANDRA-7351](https://issues.apache.org/jira/browse/CASSANDRA-7351) | Make BEGIN COUNTER BATCH syntax optional |  Trivial | CQL | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-7356](https://issues.apache.org/jira/browse/CASSANDRA-7356) | Add a more ops friendly replace\_address flag |  Major | . | Richard Low | Brandon Williams |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6777](https://issues.apache.org/jira/browse/CASSANDRA-6777) | 2.1 w/java-driver 2.0 and stress write using thrift results in ArithmeticException / by zero errors |  Minor | Tools | dan jatnieks | dan jatnieks |
| [CASSANDRA-5897](https://issues.apache.org/jira/browse/CASSANDRA-5897) | GossipingPropertyFileSnitch does not auto-reload local rack/dc |  Major | . | Jeremy Hanna | Daniel Shelepov |
| [CASSANDRA-5978](https://issues.apache.org/jira/browse/CASSANDRA-5978) | stressd broken by ClientEncriptionOptions |  Minor | Tools | Jeremiah Jordan | Benedict |
| [CASSANDRA-6824](https://issues.apache.org/jira/browse/CASSANDRA-6824) | fix help text for stress counterwrite |  Trivial | . | Russ Hatch | Benedict |
| [CASSANDRA-5927](https://issues.apache.org/jira/browse/CASSANDRA-5927) | stress skipKeys option does not appear to do anything |  Minor | Tools | Chris Burroughs | Benedict |
| [CASSANDRA-6798](https://issues.apache.org/jira/browse/CASSANDRA-6798) | fix artifacts task to strip out DOS crlf |  Minor | Packaging | Peter | Peter |
| [CASSANDRA-6863](https://issues.apache.org/jira/browse/CASSANDRA-6863) | Incorrect read repair of range thombstones |  Major | . | Oleg Anastasyev | Oleg Anastasyev |
| [CASSANDRA-7033](https://issues.apache.org/jira/browse/CASSANDRA-7033) | new stress errors on reads with native protocol |  Minor | . | Jason Brown | Benedict |
| [CASSANDRA-7177](https://issues.apache.org/jira/browse/CASSANDRA-7177) | Starting threads in the OutboundTcpConnectionPool constructor causes race conditions |  Major | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-5818](https://issues.apache.org/jira/browse/CASSANDRA-5818) | Duplicated error messages on directory creation error at startup |  Trivial | . | Michaël Figuière | Lyuben Todorov |
| [CASSANDRA-7325](https://issues.apache.org/jira/browse/CASSANDRA-7325) | Cqlsh counts non-empty lines for "Blank lines" warning |  Minor | . | Mikhail Stepura | Mikhail Stepura |
| [CASSANDRA-7323](https://issues.apache.org/jira/browse/CASSANDRA-7323) | NPE in StreamTransferTask.createMessageForRetry |  Minor | Streaming and Messaging | Rick Branson | Joshua McKenzie |
| [CASSANDRA-7329](https://issues.apache.org/jira/browse/CASSANDRA-7329) | repair sessions not cleared out of nodetool netstats output on completion |  Minor | Streaming and Messaging, Tools | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-7245](https://issues.apache.org/jira/browse/CASSANDRA-7245) | Out-of-Order keys with stress + CQL3 |  Major | . | Pavel Yaskevich | T Jake Luciani |
| [CASSANDRA-7354](https://issues.apache.org/jira/browse/CASSANDRA-7354) | Divide by zero in hinted handoff |  Trivial | . | Duncan Sands | Aleksey Yeschenko |
| [CASSANDRA-7340](https://issues.apache.org/jira/browse/CASSANDRA-7340) | CqlRecordWriter doesn't have user/password authentication |  Major | . | Alex Liu | Alex Liu |
| [CASSANDRA-6523](https://issues.apache.org/jira/browse/CASSANDRA-6523) | "Unable to contact any seeds!" with multi-DC cluster and listen != broadcast address |  Minor | . | Chris Burroughs | Brandon Williams |
| [CASSANDRA-7337](https://issues.apache.org/jira/browse/CASSANDRA-7337) | Protocol batches wrongly ignores conditions |  Major | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-7383](https://issues.apache.org/jira/browse/CASSANDRA-7383) | java.lang.IndexOutOfBoundsException |  Major | . | Thomas Zimmer |  |
| [CASSANDRA-7380](https://issues.apache.org/jira/browse/CASSANDRA-7380) | Native protocol needs keepalive, we should add it |  Major | . | Jose Martinez Poblete | Brandon Williams |
| [CASSANDRA-7372](https://issues.apache.org/jira/browse/CASSANDRA-7372) | Exception when querying a composite-keyed table with a collection index |  Major | . | Ghais Issa | Mikhail Stepura |
| [CASSANDRA-7365](https://issues.apache.org/jira/browse/CASSANDRA-7365) | some compactions do not works under windows (file in use during rename) |  Major | Compaction, Local Write-Read Paths | Radim Kolar | Joshua McKenzie |
| [CASSANDRA-6397](https://issues.apache.org/jira/browse/CASSANDRA-6397) | removenode outputs confusing non-error |  Trivial | Tools | Ryan McGuire | Kirk True |
| [CASSANDRA-7268](https://issues.apache.org/jira/browse/CASSANDRA-7268) | Secondary Index can miss data without an error |  Major | Secondary Indexes | Jeremiah Jordan | Sam Tunnicliffe |
| [CASSANDRA-7353](https://issues.apache.org/jira/browse/CASSANDRA-7353) | Java heap being set too large on Windows with 32-bit JVM |  Minor | Packaging | Philip Thompson | Joshua McKenzie |
| [CASSANDRA-7318](https://issues.apache.org/jira/browse/CASSANDRA-7318) | Unable to truncate column family on node which has been decommissioned and re-bootstrapped |  Minor | . | Thomas Whiteway | Brandon Williams |
| [CASSANDRA-6449](https://issues.apache.org/jira/browse/CASSANDRA-6449) | Tools error out if they can't make ~/.cassandra |  Major | Tools | Jeremiah Jordan | Kirk True |
| [CASSANDRA-7381](https://issues.apache.org/jira/browse/CASSANDRA-7381) | Snappy Compression does not work with PowerPC 64-bit, Little Endian |  Minor | . | Iheanyi Ekechukwu | Iheanyi Ekechukwu |
| [CASSANDRA-7397](https://issues.apache.org/jira/browse/CASSANDRA-7397) | BatchlogManagerTest unit test failing |  Major | Testing | Michael Shuler | Carl Yeksigian |
| [CASSANDRA-7364](https://issues.apache.org/jira/browse/CASSANDRA-7364) | assert error in StorageProxy.submitHint |  Minor | . | Radim Kolar | Aleksey Yeschenko |
| [CASSANDRA-7307](https://issues.apache.org/jira/browse/CASSANDRA-7307) | New nodes mark dead nodes as up for 10 minutes |  Major | . | Richard Low | Brandon Williams |
| [CASSANDRA-7193](https://issues.apache.org/jira/browse/CASSANDRA-7193) | rows\_per\_partition\_to\_cache is not reflected in table DESCRIBE |  Minor | . | Ryan McGuire | Marcus Eriksson |
| [CASSANDRA-7407](https://issues.apache.org/jira/browse/CASSANDRA-7407) | COPY command does not work properly with collections causing failure to import data |  Major | . | Jose Martinez Poblete | Mikhail Stepura |
| [CASSANDRA-7373](https://issues.apache.org/jira/browse/CASSANDRA-7373) | Commit logs no longer deleting and MemtablePostFlusher pending growing |  Major | . | Francois Richard | Mikhail Stepura |
| [CASSANDRA-6766](https://issues.apache.org/jira/browse/CASSANDRA-6766) | allow now() -\> uuid compatibility |  Minor | CQL | Jonathan Ellis | Tyler Hobbs |
| [CASSANDRA-7421](https://issues.apache.org/jira/browse/CASSANDRA-7421) | CompoundSparseCellName/WithCollection report smaller heap sizes than actual |  Trivial | . | Dave Brosius | Benedict |
| [CASSANDRA-7399](https://issues.apache.org/jira/browse/CASSANDRA-7399) | cqlsh: describe table shows wrong data type for CompositeType |  Major | Tools | Robert Stupp | Mikhail Stepura |
| [CASSANDRA-7512](https://issues.apache.org/jira/browse/CASSANDRA-7512) | DROP KEYSPACE IF EXISTS fails if the KS does not exist |  Major | . | Fabrice Larcher |  |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5821](https://issues.apache.org/jira/browse/CASSANDRA-5821) | Test new ConcurrentLinkedHashMap implementation (1.4RC) |  Major | . | Ryan McGuire | Ryan McGuire |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7334](https://issues.apache.org/jira/browse/CASSANDRA-7334) | RecoveryManagerTest times out on Windows |  Minor | . | Joshua McKenzie | Ala' Alkhaldi |
| [CASSANDRA-7335](https://issues.apache.org/jira/browse/CASSANDRA-7335) | GossipingPropertyFileSnitchTest fails on Windows |  Minor | Testing | Joshua McKenzie | Joshua McKenzie |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7413](https://issues.apache.org/jira/browse/CASSANDRA-7413) | Native Protocol V3 CREATE Response |  Major | CQL | Robert Stupp | Sylvain Lebresne |


