
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.0 rc1 - 2013-08-08



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5823](https://issues.apache.org/jira/browse/CASSANDRA-5823) | nodetool history logging |  Minor | Tools | Jason Brown | Jason Brown |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5626](https://issues.apache.org/jira/browse/CASSANDRA-5626) | Support empty IN queries |  Minor | . | Alexander Solovyev | Aleksey Yeschenko |
| [CASSANDRA-5819](https://issues.apache.org/jira/browse/CASSANDRA-5819) | Add a native\_protocol\_version column to the system.local table |  Trivial | . | Michaël Figuière | Jason Brown |
| [CASSANDRA-5822](https://issues.apache.org/jira/browse/CASSANDRA-5822) | Keep previous index\_interval value when upgrading to 2.0 |  Minor | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-5837](https://issues.apache.org/jira/browse/CASSANDRA-5837) | [patch] improve performance of DecimalSerializer.serialize |  Trivial | . | Julien Aymé | Julien Aymé |
| [CASSANDRA-5845](https://issues.apache.org/jira/browse/CASSANDRA-5845) | Don't pull schema from higher major nodes; don't push schema to lower major nodes |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5801](https://issues.apache.org/jira/browse/CASSANDRA-5801) | AE during validation compaction |  Major | . | Brandon Williams | Sylvain Lebresne |
| [CASSANDRA-5698](https://issues.apache.org/jira/browse/CASSANDRA-5698) | cqlsh should support collections in COPY FROM |  Minor | Tools | Hiroshi Kise | Aleksey Yeschenko |
| [CASSANDRA-5805](https://issues.apache.org/jira/browse/CASSANDRA-5805) | CQL 'set' returns incorrect value |  Critical | . | Yuki Morishita | Sylvain Lebresne |
| [CASSANDRA-5797](https://issues.apache.org/jira/browse/CASSANDRA-5797) | DC-local CAS |  Minor | CQL | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-5758](https://issues.apache.org/jira/browse/CASSANDRA-5758) | Any exception hangs repair |  Major | . | Radim Kolar |  |
| [CASSANDRA-5804](https://issues.apache.org/jira/browse/CASSANDRA-5804) | AntiEntropySession fails when OutboundTcpConnection receives IOException |  Major | . | Ashley Winters | Ashley Winters |
| [CASSANDRA-5812](https://issues.apache.org/jira/browse/CASSANDRA-5812) | CQLSH Windows: TypeError: argument of type 'NoneType' is not iterable |  Minor | Tools | mick delaney | Aleksey Yeschenko |
| [CASSANDRA-5814](https://issues.apache.org/jira/browse/CASSANDRA-5814) | RowIndexEntry.deletionTime raises UnsupportedOperationException when upgrading to 1.2.7 |  Major | . | Brett Hoerner | Jonathan Ellis |
| [CASSANDRA-5827](https://issues.apache.org/jira/browse/CASSANDRA-5827) | Expose setters for consistency level in Hadoop config helper |  Trivial | . | Manoj Mainali | Manoj Mainali |
| [CASSANDRA-5802](https://issues.apache.org/jira/browse/CASSANDRA-5802) | NPE in HH metrics |  Major | . | Brandon Williams | Tyler Hobbs |
| [CASSANDRA-5824](https://issues.apache.org/jira/browse/CASSANDRA-5824) | Fix quoting in CqlPagingRecordReader and CqlRecordWriter |  Major | . | Alex Liu | Alex Liu |
| [CASSANDRA-5670](https://issues.apache.org/jira/browse/CASSANDRA-5670) | running compact on an index did not compact two index files into one |  Minor | Tools | Cathy Daw | Jason Brown |
| [CASSANDRA-5792](https://issues.apache.org/jira/browse/CASSANDRA-5792) | Buffer Underflow during streaming |  Major | . | Brandon Williams | Yuki Morishita |
| [CASSANDRA-5826](https://issues.apache.org/jira/browse/CASSANDRA-5826) | Fix trigger directory detection code |  Major | . | Aleksey Yeschenko | Vijay |
| [CASSANDRA-5806](https://issues.apache.org/jira/browse/CASSANDRA-5806) | NPE during repair |  Minor | . | Radim Kolar | Yuki Morishita |


