
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.2.12 - 2013-11-25



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4191](https://issues.apache.org/jira/browse/CASSANDRA-4191) | Add \`nodetool cfstats \<ks\> \<cf\>\` abilities |  Minor | . | Joaquin Casares | Lyuben Todorov |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6145](https://issues.apache.org/jira/browse/CASSANDRA-6145) | Include windows batch files for recently added shell commands (and some older ones) |  Trivial | Packaging | Sven Delmas | Sven Delmas |
| [CASSANDRA-6241](https://issues.apache.org/jira/browse/CASSANDRA-6241) | Assertion on MmappedSegmentedFile.floor doesn't tell us the path (filename) |  Trivial | . | Jackson Chung | Jonathan Ellis |
| [CASSANDRA-6273](https://issues.apache.org/jira/browse/CASSANDRA-6273) | nodetool should get default JMX port from cassandra-env.sh |  Trivial | Tools | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-6244](https://issues.apache.org/jira/browse/CASSANDRA-6244) | calculatePendingRanges could be asynchronous on 1.2 too |  Major | . | Ryan Fowler | Ryan Fowler |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6217](https://issues.apache.org/jira/browse/CASSANDRA-6217) | replace doesn't clean up system.peers if you have a new IP |  Major | . | Jeremiah Jordan | Brandon Williams |
| [CASSANDRA-6114](https://issues.apache.org/jira/browse/CASSANDRA-6114) | Pig with widerows=true and batch size = 1 works incorrectly |  Minor | . | Alex Liu | Alex Liu |
| [CASSANDRA-6185](https://issues.apache.org/jira/browse/CASSANDRA-6185) | Can't update int column to blob type. |  Minor | . | Nick Bailey | Sylvain Lebresne |
| [CASSANDRA-6196](https://issues.apache.org/jira/browse/CASSANDRA-6196) | Add compaction, compression to cqlsh tab completion for CREATE TABLE |  Minor | Tools | Jonathan Ellis | Mikhail Stepura |
| [CASSANDRA-5815](https://issues.apache.org/jira/browse/CASSANDRA-5815) | NPE from migration manager |  Minor | . | Vishy Kasar | Brandon Williams |
| [CASSANDRA-6181](https://issues.apache.org/jira/browse/CASSANDRA-6181) | Replaying a commit led to java.lang.StackOverflowError and node crash |  Critical | . | Jeffrey Damick | Sylvain Lebresne |
| [CASSANDRA-6232](https://issues.apache.org/jira/browse/CASSANDRA-6232) | Installation shouldn't fail if /etc/sysctl.d/cassandra is deleted |  Minor | Packaging | Faidon Liambotis | Brandon Williams |
| [CASSANDRA-6180](https://issues.apache.org/jira/browse/CASSANDRA-6180) | NPE in CqlRecordWriter: Related to AbstractCassandraStorage handling null values |  Major | . | Henning Kropp | Alex Liu |
| [CASSANDRA-6254](https://issues.apache.org/jira/browse/CASSANDRA-6254) | Thrift's prepare\_cql\*\_query() should validate login |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5988](https://issues.apache.org/jira/browse/CASSANDRA-5988) | Make hint TTL customizable |  Major | . | Oleg Kibirev | Vishy Kasar |
| [CASSANDRA-6258](https://issues.apache.org/jira/browse/CASSANDRA-6258) | Need the root clause in FBUtilities.classForName when there is exception loading class |  Major | . | Jackson Chung | Jackson Chung |
| [CASSANDRA-6267](https://issues.apache.org/jira/browse/CASSANDRA-6267) | restrict max num\_tokens to something Gossip can handle |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-6272](https://issues.apache.org/jira/browse/CASSANDRA-6272) | sstableloader data distribution is broken since 1.2.7 |  Minor | Tools | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-6297](https://issues.apache.org/jira/browse/CASSANDRA-6297) | Gossiper blocks when updating tokens and turns node down |  Major | . | Sergio Bossa | Jonathan Ellis |
| [CASSANDRA-6302](https://issues.apache.org/jira/browse/CASSANDRA-6302) | make CqlPagingRecordReader more robust to failures |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-6308](https://issues.apache.org/jira/browse/CASSANDRA-6308) | Thread leak caused in creating OutboundTcpConnectionPool |  Minor | . | Minh Do | Minh Do |
| [CASSANDRA-6238](https://issues.apache.org/jira/browse/CASSANDRA-6238) | LOCAL\_ONE doesn't work for SimpleStrategy |  Major | . | Alex Liu | Alex Liu |
| [CASSANDRA-6317](https://issues.apache.org/jira/browse/CASSANDRA-6317) | cqlsh should handle 'null' as session duration |  Minor | . | Matt Jurik | Mikhail Stepura |
| [CASSANDRA-6324](https://issues.apache.org/jira/browse/CASSANDRA-6324) | No "echo off" in cqlsh.bat |  Trivial | Tools | Mikhail Stepura | Mikhail Stepura |
| [CASSANDRA-6330](https://issues.apache.org/jira/browse/CASSANDRA-6330) | LIMIT fetches one less than requested value |  Major | . | Branden Visser | Sylvain Lebresne |
| [CASSANDRA-6341](https://issues.apache.org/jira/browse/CASSANDRA-6341) | After executing abnormal cql statement, not working  (hang) |  Minor | . | Kim Yong Hwan | Sylvain Lebresne |
| [CASSANDRA-6351](https://issues.apache.org/jira/browse/CASSANDRA-6351) | When dropping a CF, row cache is not invalidated |  Minor | . | Fabien Rousseau | Fabien Rousseau |
| [CASSANDRA-6369](https://issues.apache.org/jira/browse/CASSANDRA-6369) | Fix prepared statement size computation |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6370](https://issues.apache.org/jira/browse/CASSANDRA-6370) | Updating cql created table through cassandra-cli transform it into a compact storage table |  Critical | . | Alain RODRIGUEZ | Sylvain Lebresne |
| [CASSANDRA-6347](https://issues.apache.org/jira/browse/CASSANDRA-6347) | LOCAL\_ONE code in the native protocol is not the same in C\* 1.2 and C\* 2.0 |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6375](https://issues.apache.org/jira/browse/CASSANDRA-6375) | Unit test failures on 1.2 branch |  Blocker | . | Sylvain Lebresne |  |
| [CASSANDRA-5377](https://issues.apache.org/jira/browse/CASSANDRA-5377) | MemoryMeter miscalculating memtable live ratio |  Minor | . | Ahmed Bashir | Jonathan Ellis |
| [CASSANDRA-6325](https://issues.apache.org/jira/browse/CASSANDRA-6325) | AssertionError on startup reading saved Serializing row cache |  Minor | . | Chris Burroughs | Mikhail Stepura |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6293](https://issues.apache.org/jira/browse/CASSANDRA-6293) | CASSANDRA-6107 causes EmbeddedCassandraService to fail |  Minor | . | Michael Oczkowski | Mikhail Stepura |
| [CASSANDRA-6214](https://issues.apache.org/jira/browse/CASSANDRA-6214) | Make LOCAL\_ONE the default consistency for cassandra.consistencylevel.[read\|write] |  Major | . | Jeremy Hanna | Alex Liu |


