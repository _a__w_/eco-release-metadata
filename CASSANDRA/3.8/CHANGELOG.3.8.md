
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.8 - 2016-09-29



### IMPORTANT ISSUES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12107](https://issues.apache.org/jira/browse/CASSANDRA-12107) | Fix range scans for table with live static rows |  Major | CQL | Sharvanath Pathak | Sharvanath Pathak |


### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-8844](https://issues.apache.org/jira/browse/CASSANDRA-8844) | Change Data Capture (CDC) |  Critical | Coordination, Local Write-Read Paths | Tupshin Harper | Joshua McKenzie |
| [CASSANDRA-11327](https://issues.apache.org/jira/browse/CASSANDRA-11327) | Maintain a histogram of times when writes are blocked due to no available memory |  Major | Core | Ariel Weisberg | Ariel Weisberg |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9766](https://issues.apache.org/jira/browse/CASSANDRA-9766) | Faster Streaming |  Major | Streaming and Messaging | Alexei K | T Jake Luciani |
| [CASSANDRA-11425](https://issues.apache.org/jira/browse/CASSANDRA-11425) | Add prepared query parameter to trace for "Execute CQL3 prepared query" session |  Minor | CQL | Yasuharu Goto | Yasuharu Goto |
| [CASSANDRA-11517](https://issues.apache.org/jira/browse/CASSANDRA-11517) | o.a.c.utils.UUIDGen could handle contention better |  Minor | Core | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-11623](https://issues.apache.org/jira/browse/CASSANDRA-11623) | Compactions w/ Short Rows Spending Time in getOnDiskFilePointer |  Minor | . | Tom Petracca | Tom Petracca |
| [CASSANDRA-11269](https://issues.apache.org/jira/browse/CASSANDRA-11269) | Improve UDF compilation error messages |  Major | CQL | Robert Stupp | Robert Stupp |
| [CASSANDRA-11807](https://issues.apache.org/jira/browse/CASSANDRA-11807) | Add modern arguments to build.xml javac |  Minor | Testing | Edward Capriolo | Edward Capriolo |
| [CASSANDRA-11869](https://issues.apache.org/jira/browse/CASSANDRA-11869) | build: use correct netty version sources |  Trivial | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-11114](https://issues.apache.org/jira/browse/CASSANDRA-11114) | Document which type conversions are allowed |  Minor | CQL, Documentation and Website | Sylvain Lebresne | Giampaolo |
| [CASSANDRA-11503](https://issues.apache.org/jira/browse/CASSANDRA-11503) | Need a tool to detect what percentage of SSTables on a node have been repaired when using incremental repairs. |  Minor | Tools | Sean Usher | Chris Lohfink |
| [CASSANDRA-11717](https://issues.apache.org/jira/browse/CASSANDRA-11717) | ParsedStatement.Prepared#partitionKeyBindIndexes should be of type short[] |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-11853](https://issues.apache.org/jira/browse/CASSANDRA-11853) | Improve Cassandra-Stress latency measurement |  Major | Tools | Nitsan Wakart | Nitsan Wakart |
| [CASSANDRA-9811](https://issues.apache.org/jira/browse/CASSANDRA-9811) | Tie counter shards' logical clock value to timestamps |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-9666](https://issues.apache.org/jira/browse/CASSANDRA-9666) | Provide an alternative to DTCS |  Major | Compaction | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-11719](https://issues.apache.org/jira/browse/CASSANDRA-11719) | Add bind variables to trace |  Minor | . | Robert Stupp | Mahdi Mohammadinasab |
| [CASSANDRA-10532](https://issues.apache.org/jira/browse/CASSANDRA-10532) | Allow LWT operation on static column with only partition keys |  Major | CQL | DOAN DuyHai | Carl Yeksigian |
| [CASSANDRA-10783](https://issues.apache.org/jira/browse/CASSANDRA-10783) | Allow literal value as parameter of UDF & UDA |  Minor | CQL | DOAN DuyHai | Sylvain Lebresne |
| [CASSANDRA-11933](https://issues.apache.org/jira/browse/CASSANDRA-11933) | Cache local ranges when calculating repair neighbors |  Major | Core | Cyril Scetbon | Mahdi Mohammadinasab |
| [CASSANDRA-11569](https://issues.apache.org/jira/browse/CASSANDRA-11569) | Track message latency across DCs |  Minor | Observability | Chris Lohfink | Chris Lohfink |
| [CASSANDRA-11576](https://issues.apache.org/jira/browse/CASSANDRA-11576) | Add support for JNA mlockall(2) on POWER |  Minor | Core | Rei Odaira | Rei Odaira |
| [CASSANDRA-11798](https://issues.apache.org/jira/browse/CASSANDRA-11798) | Allow specification of 'time' column value as number in CQL query. |  Minor | CQL | Andy Tolbert | Alex Petrov |
| [CASSANDRA-11966](https://issues.apache.org/jira/browse/CASSANDRA-11966) | When SEPWorker assigned work, set thread name to match pool |  Minor | Observability | Chris Lohfink | Chris Lohfink |
| [CASSANDRA-11971](https://issues.apache.org/jira/browse/CASSANDRA-11971) | More uses of DataOutputBuffer.RECYCLER |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-11957](https://issues.apache.org/jira/browse/CASSANDRA-11957) | Implement seek() of org.apache.cassandra.db.commitlog.EncryptedFileSegmentInputStream |  Critical | Coordination, Local Write-Read Paths | Imran Chaudhry | Imran Chaudhry |
| [CASSANDRA-11969](https://issues.apache.org/jira/browse/CASSANDRA-11969) | Prevent duplicate ctx.channel().attr() call |  Trivial | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-11755](https://issues.apache.org/jira/browse/CASSANDRA-11755) | nodetool info should run with "readonly" jmx access |  Minor | Observability | Jérôme Mainaud | Jérôme Mainaud |
| [CASSANDRA-11937](https://issues.apache.org/jira/browse/CASSANDRA-11937) | Clean up buffer trimming large buffers in DataOutputBuffer after the Netty upgrade |  Major | . | Alex Petrov | Alex Petrov |
| [CASSANDRA-11911](https://issues.apache.org/jira/browse/CASSANDRA-11911) | CQLSSTableWriter should allow for unset fields |  Major | Core | Matt Kopit | Alex Petrov |
| [CASSANDRA-12080](https://issues.apache.org/jira/browse/CASSANDRA-12080) | More detailed compaction log |  Trivial | . | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-12034](https://issues.apache.org/jira/browse/CASSANDRA-12034) | Special handling for Netty's direct memory allocation failure |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-11715](https://issues.apache.org/jira/browse/CASSANDRA-11715) | Make GCInspector's MIN\_LOG\_DURATION configurable |  Minor | . | Brandon Williams | Jeff Jirsa |
| [CASSANDRA-12032](https://issues.apache.org/jira/browse/CASSANDRA-12032) | Update to Netty 4.0.39 |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-12114](https://issues.apache.org/jira/browse/CASSANDRA-12114) | Cassandra startup takes an hour because of N\*N operation |  Major | Core | Tom van der Woerdt | Jeff Jirsa |
| [CASSANDRA-12180](https://issues.apache.org/jira/browse/CASSANDRA-12180) | Should be able to override compaction space check |  Trivial | Compaction | sankalp kohli | sankalp kohli |
| [CASSANDRA-12181](https://issues.apache.org/jira/browse/CASSANDRA-12181) | Include table name in "Cannot get comparator" exception |  Trivial | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-12040](https://issues.apache.org/jira/browse/CASSANDRA-12040) |   If a level compaction fails due to no space it should schedule the next one |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-8700](https://issues.apache.org/jira/browse/CASSANDRA-8700) | replace the wiki with docs in the git repo |  Blocker | Documentation and Website | Jon Haddad | Sylvain Lebresne |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11546](https://issues.apache.org/jira/browse/CASSANDRA-11546) | Stress doesn't respect case-sensitive column names when building insert queries |  Trivial | Tools | Joel Knighton | Giampaolo |
| [CASSANDRA-11272](https://issues.apache.org/jira/browse/CASSANDRA-11272) | NullPointerException (NPE) during bootstrap startup in StorageService.java |  Major | Lifecycle | Jason Kania | Alex Petrov |
| [CASSANDRA-11867](https://issues.apache.org/jira/browse/CASSANDRA-11867) | Possible memory leak in NIODataInputStream |  Minor | Local Write-Read Paths | Robert Stupp | Robert Stupp |
| [CASSANDRA-9530](https://issues.apache.org/jira/browse/CASSANDRA-9530) | SSTable corruption can trigger OOM |  Major | Local Write-Read Paths | Sylvain Lebresne | Alex Petrov |
| [CASSANDRA-11718](https://issues.apache.org/jira/browse/CASSANDRA-11718) | entry-weighers in QueryProcessor should respect partitionKeyBindIndexes field |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-11912](https://issues.apache.org/jira/browse/CASSANDRA-11912) | Remove DatabaseDescriptor import from AbstractType |  Major | Configuration, Core | Jeremiah Jordan | Alex Petrov |
| [CASSANDRA-11838](https://issues.apache.org/jira/browse/CASSANDRA-11838) | dtest failure in largecolumn\_test:TestLargeColumn.cleanup\_test |  Major | . | Philip Thompson | Alex Petrov |
| [CASSANDRA-11984](https://issues.apache.org/jira/browse/CASSANDRA-11984) | StorageService shutdown hook should use a volatile variable |  Major | Core | Edward Capriolo | Edward Capriolo |
| [CASSANDRA-11749](https://issues.apache.org/jira/browse/CASSANDRA-11749) | CQLSH gets SSL exception following a COPY FROM |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-11886](https://issues.apache.org/jira/browse/CASSANDRA-11886) | Streaming will miss sections for early opened sstables during compaction |  Critical | . | Stefan Podkowinski | Marcus Eriksson |
| [CASSANDRA-11604](https://issues.apache.org/jira/browse/CASSANDRA-11604) | select on table fails after changing user defined type in map |  Major | CQL | Andreas Jaekle | Alex Petrov |
| [CASSANDRA-11481](https://issues.apache.org/jira/browse/CASSANDRA-11481) | Example metrics config has DroppedMetrics |  Minor | Documentation and Website | Christopher Batey | Christopher Batey |
| [CASSANDRA-11948](https://issues.apache.org/jira/browse/CASSANDRA-11948) | Wrong ByteBuffer comparisons in TimeTypeTest |  Minor | Testing | Rei Odaira | Rei Odaira |
| [CASSANDRA-11961](https://issues.apache.org/jira/browse/CASSANDRA-11961) | Nonfatal NPE in CompactionMetrics |  Minor | . | Robert Stupp | Achal Shah |
| [CASSANDRA-12002](https://issues.apache.org/jira/browse/CASSANDRA-12002) | SSTable tools mishandling LocalPartitioner |  Minor | Tools | Chris Lohfink | Chris Lohfink |
| [CASSANDRA-11038](https://issues.apache.org/jira/browse/CASSANDRA-11038) | Is node being restarted treated as node joining? |  Minor | Distributed Metadata | cheng ren | Sam Tunnicliffe |
| [CASSANDRA-12023](https://issues.apache.org/jira/browse/CASSANDRA-12023) | Schema upgrade bug with super columns |  Critical | Distributed Metadata | Jeremiah Jordan | Aleksey Yeschenko |
| [CASSANDRA-9842](https://issues.apache.org/jira/browse/CASSANDRA-9842) | Inconsistent behavior for '= null' conditions on static columns |  Major | CQL | Chandra Sekar | Alex Petrov |
| [CASSANDRA-11920](https://issues.apache.org/jira/browse/CASSANDRA-11920) | bloom\_filter\_fp\_chance needs to be validated up front |  Minor | Lifecycle, Local Write-Read Paths | ADARSH KUMAR | Arindam Gupta |
| [CASSANDRA-12026](https://issues.apache.org/jira/browse/CASSANDRA-12026) | update NEWS.txt to explain system schema exceptions during partial cluster upgrade |  Major | . | Russ Hatch | Joshua McKenzie |
| [CASSANDRA-11991](https://issues.apache.org/jira/browse/CASSANDRA-11991) | On clock skew, paxos may "corrupt" the node clock |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-11913](https://issues.apache.org/jira/browse/CASSANDRA-11913) | BufferUnderFlowException in CompressorTest |  Minor | Testing | Rei Odaira | Rei Odaira |
| [CASSANDRA-11882](https://issues.apache.org/jira/browse/CASSANDRA-11882) | Clustering Key with ByteBuffer size \> 64k throws Assertion Error |  Major | CQL, Streaming and Messaging | Lerh Chuan Low | Lerh Chuan Low |
| [CASSANDRA-11696](https://issues.apache.org/jira/browse/CASSANDRA-11696) | Incremental repairs can mark too many ranges as repaired |  Critical | Streaming and Messaging | Joel Knighton | Marcus Eriksson |
| [CASSANDRA-12077](https://issues.apache.org/jira/browse/CASSANDRA-12077) | NPE when trying to get sstables for anticompaction |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-11878](https://issues.apache.org/jira/browse/CASSANDRA-11878) | dtest failure in upgrade\_tests.cql\_tests.TestCQLNodes2RF1\_Upgrade\_current\_3\_x\_To\_indev\_3\_x.select\_key\_in\_test |  Major | . | Sean McCarthy | Marcus Eriksson |
| [CASSANDRA-12082](https://issues.apache.org/jira/browse/CASSANDRA-12082) | CommitLogStressTest failing post-CASSANDRA-8844 |  Minor | . | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-12078](https://issues.apache.org/jira/browse/CASSANDRA-12078) | [SASI] Move skip\_stop\_words filter BEFORE stemming |  Major | sasi | DOAN DuyHai | DOAN DuyHai |
| [CASSANDRA-11993](https://issues.apache.org/jira/browse/CASSANDRA-11993) | Cannot read Snappy compressed tables with 3.6 |  Major | Local Write-Read Paths | Nimi Wariboko Jr. | Branimir Lambov |
| [CASSANDRA-12083](https://issues.apache.org/jira/browse/CASSANDRA-12083) | Race condition during system.roles column family creation |  Major | Core | Sharvanath Pathak | Sam Tunnicliffe |
| [CASSANDRA-11854](https://issues.apache.org/jira/browse/CASSANDRA-11854) | Remove finished streaming connections from MessagingService |  Major | Streaming and Messaging | Paulo Motta | Paulo Motta |
| [CASSANDRA-12043](https://issues.apache.org/jira/browse/CASSANDRA-12043) | Syncing most recent commit in CAS across replicas can cause all CAS queries in the CQL partition to fail |  Major | . | sankalp kohli | Sylvain Lebresne |
| [CASSANDRA-11973](https://issues.apache.org/jira/browse/CASSANDRA-11973) | Is MemoryUtil.getShort() supposed to return a sign-extended or non-sign-extended value? |  Minor | Core | Rei Odaira | Rei Odaira |
| [CASSANDRA-11820](https://issues.apache.org/jira/browse/CASSANDRA-11820) | Altering a column's type causes EOF |  Major | Local Write-Read Paths | Carl Yeksigian | Sylvain Lebresne |
| [CASSANDRA-12090](https://issues.apache.org/jira/browse/CASSANDRA-12090) | Digest mismatch if static column is NULL |  Major | Streaming and Messaging | Tommy Stendahl | Tommy Stendahl |
| [CASSANDRA-12073](https://issues.apache.org/jira/browse/CASSANDRA-12073) | [SASI] PREFIX search on CONTAINS/NonTokenizer mode returns only partial results |  Major | CQL | DOAN DuyHai | DOAN DuyHai |
| [CASSANDRA-12044](https://issues.apache.org/jira/browse/CASSANDRA-12044) | Materialized view definition regression in clustering key |  Major | CQL, Materialized Views | Michael Mior | Carl Yeksigian |
| [CASSANDRA-11944](https://issues.apache.org/jira/browse/CASSANDRA-11944) | sstablesInBounds might not actually give all sstables within the bounds due to having start positions moved in sstables |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-11996](https://issues.apache.org/jira/browse/CASSANDRA-11996) | SSTableSet.CANONICAL can miss sstables |  Critical | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-12098](https://issues.apache.org/jira/browse/CASSANDRA-12098) | dtest failure in secondary\_indexes\_test.TestSecondaryIndexes.test\_only\_coordinator\_chooses\_index\_for\_query |  Major | Secondary Indexes | Sean McCarthy | Sam Tunnicliffe |
| [CASSANDRA-11414](https://issues.apache.org/jira/browse/CASSANDRA-11414) | dtest failure in bootstrap\_test.TestBootstrap.resumable\_bootstrap\_test |  Major | Testing | Philip Thompson | Paulo Motta |
| [CASSANDRA-11733](https://issues.apache.org/jira/browse/CASSANDRA-11733) | SSTableReversedIterator ignores range tombstones |  Major | Local Write-Read Paths | Dave Brosius | Sylvain Lebresne |
| [CASSANDRA-12146](https://issues.apache.org/jira/browse/CASSANDRA-12146) | Use dedicated executor for sending JMX notifications |  Major | Observability | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-12072](https://issues.apache.org/jira/browse/CASSANDRA-12072) | dtest failure in auth\_test.TestAuthRoles.udf\_permissions\_in\_selection\_test |  Major | Streaming and Messaging | Sean McCarthy | Joel Knighton |
| [CASSANDRA-12145](https://issues.apache.org/jira/browse/CASSANDRA-12145) | Cassandra Stress histogram log is empty if there's only a single operation |  Minor | Tools | Nitsan Wakart | Nitsan Wakart |
| [CASSANDRA-11950](https://issues.apache.org/jira/browse/CASSANDRA-11950) | dtest failure in consistency\_test.TestAvailability.test\_network\_topology\_strategy\_each\_quorum |  Major | . | Sean McCarthy | Stefania |
| [CASSANDRA-11315](https://issues.apache.org/jira/browse/CASSANDRA-11315) | Fix upgrading sparse tables that are incorrectly marked as dense |  Major | Distributed Metadata | Dominik Keil | Aleksey Yeschenko |
| [CASSANDRA-12147](https://issues.apache.org/jira/browse/CASSANDRA-12147) | Static thrift tables with non UTF8Type comparators can have column names converted incorrectly |  Major | Distributed Metadata | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-12105](https://issues.apache.org/jira/browse/CASSANDRA-12105) | ThriftServer.stop is not thread safe |  Minor | . | Brian Wawok | Brian Wawok |
| [CASSANDRA-12155](https://issues.apache.org/jira/browse/CASSANDRA-12155) | proposeCallback.java is too spammy for debug.log |  Minor | Observability | Wei Deng | Wei Deng |
| [CASSANDRA-11393](https://issues.apache.org/jira/browse/CASSANDRA-11393) | dtest failure in upgrade\_tests.upgrade\_through\_versions\_test.ProtoV3Upgrade\_2\_1\_UpTo\_3\_0\_HEAD.rolling\_upgrade\_test |  Major | Coordination, Streaming and Messaging | Philip Thompson | Benjamin Lerer |
| [CASSANDRA-12198](https://issues.apache.org/jira/browse/CASSANDRA-12198) | Deadlock in CDC during segment flush |  Blocker | Local Write-Read Paths | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-12193](https://issues.apache.org/jira/browse/CASSANDRA-12193) | dtest failure in upgrade\_tests.cql\_tests.TestCQLNodes3RF3\_Upgrade\_current\_2\_1\_x\_To\_indev\_3\_0\_x.noncomposite\_static\_cf\_test |  Major | Coordination | Sean McCarthy | Alex Petrov |
| [CASSANDRA-12191](https://issues.apache.org/jira/browse/CASSANDRA-12191) | dtest failure in upgrade\_tests.cql\_tests.TestCQLNodes3RF3\_Upgrade\_current\_2\_1\_x\_To\_indev\_3\_x.cql3\_non\_compound\_range\_tombstones\_test |  Major | . | Sean McCarthy |  |
| [CASSANDRA-12144](https://issues.apache.org/jira/browse/CASSANDRA-12144) | Undeletable / duplicate rows after upgrading from 2.2.4 to 3.0.7 |  Major | . | Stanislav Vishnevskiy | Alex Petrov |
| [CASSANDRA-12214](https://issues.apache.org/jira/browse/CASSANDRA-12214) |  cqlshlib test failure: cqlshlib.test.remove\_test\_db |  Major | Testing | Craig Kodman | Stefania |
| [CASSANDRA-12109](https://issues.apache.org/jira/browse/CASSANDRA-12109) | Configuring SSL for JMX connections forces requirement of local truststore |  Major | Configuration, Lifecycle, Observability | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-11988](https://issues.apache.org/jira/browse/CASSANDRA-11988) | NullPointerException when reading/compacting table |  Major | Compaction | Nimi Wariboko Jr. | Sylvain Lebresne |
| [CASSANDRA-12189](https://issues.apache.org/jira/browse/CASSANDRA-12189) | $$ escaped string literals are not handled correctly in cqlsh |  Major | Tools | Mike Adamson | Mike Adamson |
| [CASSANDRA-11464](https://issues.apache.org/jira/browse/CASSANDRA-11464) | C\* doesn't respond to OPTIONS request containing low protocol number |  Major | Core | Sandeep Tamhankar | Tyler Hobbs |
| [CASSANDRA-11850](https://issues.apache.org/jira/browse/CASSANDRA-11850) | cannot use cql since upgrading python to 2.7.11+ |  Major | Tools | Andrew Madison | Stefania |
| [CASSANDRA-11979](https://issues.apache.org/jira/browse/CASSANDRA-11979) | cqlsh copyutil should get host metadata by connected address |  Minor | Tools | Adam Holmberg | Stefania |
| [CASSANDRA-11980](https://issues.apache.org/jira/browse/CASSANDRA-11980) | Reads at EACH\_QUORUM not respecting the level with read repair or speculative retry active |  Major | Coordination | Aleksey Yeschenko | Carl Yeksigian |
| [CASSANDRA-12247](https://issues.apache.org/jira/browse/CASSANDRA-12247) | AssertionError with MVs on updating a row that isn't indexed due to a null value |  Critical | Local Write-Read Paths | Benjamin Roth | Sylvain Lebresne |
| [CASSANDRA-12278](https://issues.apache.org/jira/browse/CASSANDRA-12278) | Cassandra not working with Java 8u102 on Windows |  Major | Core | Thomas Atwood | Paulo Motta |
| [CASSANDRA-10939](https://issues.apache.org/jira/browse/CASSANDRA-10939) | Add missing jvm options to cassandra-env.ps1 |  Minor | Configuration | Paulo Motta | Paulo Motta |
| [CASSANDRA-12219](https://issues.apache.org/jira/browse/CASSANDRA-12219) | Lost counter writes in compact table and static columns |  Major | . | Brian Wawok | Sylvain Lebresne |
| [CASSANDRA-12263](https://issues.apache.org/jira/browse/CASSANDRA-12263) | Exception when computing read-repair for range tombstones |  Major | Coordination | Artem Rokhin | Sylvain Lebresne |
| [CASSANDRA-12277](https://issues.apache.org/jira/browse/CASSANDRA-12277) | Extend testing infrastructure to handle expected intermittent flaky tests - see ReplicationAwareTokenAllocatorTest.testNewCluster |  Minor | . | Joshua McKenzie | Branimir Lambov |
| [CASSANDRA-11465](https://issues.apache.org/jira/browse/CASSANDRA-11465) | dtest failure in cql\_tracing\_test.TestCqlTracing.tracing\_unknown\_impl\_test |  Major | Observability | Philip Thompson | Stefania |
| [CASSANDRA-12071](https://issues.apache.org/jira/browse/CASSANDRA-12071) | Regression in flushing throughput under load after CASSANDRA-6696 |  Major | Local Write-Read Paths | Ariel Weisberg | Marcus Eriksson |
| [CASSANDRA-12359](https://issues.apache.org/jira/browse/CASSANDRA-12359) | BlacklistingCompactionsTest.testBlacklistingWithSizeTieredCompactionStrategy is flaky |  Major | Testing | Joel Knighton | Stefania |
| [CASSANDRA-12236](https://issues.apache.org/jira/browse/CASSANDRA-12236) | RTE from new CDC column breaks in flight queries. |  Major | . | Jeremiah Jordan | Sylvain Lebresne |
| [CASSANDRA-11823](https://issues.apache.org/jira/browse/CASSANDRA-11823) | Creating a table leads to a race with GraphiteReporter |  Minor | Observability | Stefano Ortolani | Edward Ribeiro |
| [CASSANDRA-12205](https://issues.apache.org/jira/browse/CASSANDRA-12205) | nodetool tablestats sstable count missing. |  Major | Tools | Cameron MacMinn | Edward Ribeiro |
| [CASSANDRA-12336](https://issues.apache.org/jira/browse/CASSANDRA-12336) | NullPointerException during compaction on table with static columns |  Major | Compaction | Evan Prothro | Sylvain Lebresne |
| [CASSANDRA-11828](https://issues.apache.org/jira/browse/CASSANDRA-11828) | Commit log needs to track unflushed intervals rather than positions |  Major | Local Write-Read Paths | Branimir Lambov | Branimir Lambov |
| [CASSANDRA-12312](https://issues.apache.org/jira/browse/CASSANDRA-12312) | Restore JVM metric export for metric reporters |  Major | . | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-12371](https://issues.apache.org/jira/browse/CASSANDRA-12371) | INSERT JSON - numbers not accepted for smallint and tinyint |  Minor | CQL | Paweł Rychlik | Paweł Rychlik |
| [CASSANDRA-12335](https://issues.apache.org/jira/browse/CASSANDRA-12335) | Super columns are broken after upgrading to 3.0 on thrift |  Major | Core | Jeremiah Jordan | Sylvain Lebresne |
| [CASSANDRA-11726](https://issues.apache.org/jira/browse/CASSANDRA-11726) | IndexOutOfBoundsException when selecting (distinct) row ids from counter table. |  Major | Core | Jaroslav Kamenik | Sylvain Lebresne |
| [CASSANDRA-10992](https://issues.apache.org/jira/browse/CASSANDRA-10992) | Hanging streaming sessions |  Major | Streaming and Messaging | mlowicki | Paulo Motta |
| [CASSANDRA-12249](https://issues.apache.org/jira/browse/CASSANDRA-12249) | dtest failure in upgrade\_tests.paging\_test.TestPagingDataNodes3RF3\_Upgrade\_current\_3\_0\_x\_To\_indev\_3\_x.basic\_paging\_test |  Major | Coordination | Philip Thompson | Tyler Hobbs |
| [CASSANDRA-11752](https://issues.apache.org/jira/browse/CASSANDRA-11752) | histograms/metrics in 2.2 do not appear recency biased |  Major | Core | Chris Burroughs | Per Otterström |
| [CASSANDRA-12100](https://issues.apache.org/jira/browse/CASSANDRA-12100) | Compactions are stuck after TRUNCATE |  Major | Compaction | Stefano Ortolani | Stefania |
| [CASSANDRA-9507](https://issues.apache.org/jira/browse/CASSANDRA-9507) | range metrics are not updated for timeout and unavailable in StorageProxy |  Minor | Observability | sankalp kohli | Nachiket Patil |
| [CASSANDRA-11356](https://issues.apache.org/jira/browse/CASSANDRA-11356) | EC2MRS ignores broadcast\_rpc\_address setting in cassandra.yaml |  Major | Core | Thanh | Paulo Motta |
| [CASSANDRA-12445](https://issues.apache.org/jira/browse/CASSANDRA-12445) | StreamingTransferTest.testTransferRangeTombstones failure |  Major | Streaming and Messaging, Testing | Joel Knighton | Paulo Motta |
| [CASSANDRA-11345](https://issues.apache.org/jira/browse/CASSANDRA-11345) | Assertion Errors "Memory was freed" during streaming |  Major | Streaming and Messaging | Jean-Francois Gosselin | Paulo Motta |
| [CASSANDRA-12251](https://issues.apache.org/jira/browse/CASSANDRA-12251) | Move migration tasks to non-periodic queue, assure flush executor shutdown after non-periodic executor |  Major | Lifecycle | Philip Thompson | Alex Petrov |
| [CASSANDRA-12348](https://issues.apache.org/jira/browse/CASSANDRA-12348) | Flaky failures in SSTableRewriterTest.basicTest2/getPositionsTest |  Major | Testing | Joel Knighton | Stefania |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12010](https://issues.apache.org/jira/browse/CASSANDRA-12010) | UserTypesTest# is failing on trunk |  Major | Testing | Alex Petrov | Alex Petrov |
| [CASSANDRA-12070](https://issues.apache.org/jira/browse/CASSANDRA-12070) | dtest failure in cqlsh\_tests.cqlsh\_copy\_tests.CqlshCopyTest.test\_data\_validation\_on\_read\_template |  Major | Tools | Sean McCarthy | Tyler Hobbs |
| [CASSANDRA-12130](https://issues.apache.org/jira/browse/CASSANDRA-12130) | SASI related tests failing since CASSANDRA-11820 |  Major | . | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-12123](https://issues.apache.org/jira/browse/CASSANDRA-12123) | dtest failure in upgrade\_tests.cql\_tests.TestCQLNodes3RF3\_Upgrade\_next\_2\_1\_x\_To\_current\_3\_x.cql3\_non\_compound\_range\_tombstones\_test |  Major | Coordination | Philip Thompson | Tyler Hobbs |
| [CASSANDRA-12206](https://issues.apache.org/jira/browse/CASSANDRA-12206) | CommitLogTest.replaySimple failure |  Major | . | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-12207](https://issues.apache.org/jira/browse/CASSANDRA-12207) | CommitLogStressTest.testFixedSize times out |  Major | . | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-12218](https://issues.apache.org/jira/browse/CASSANDRA-12218) | IndexSummaryManagerTest.testCancelIndex is flaky |  Major | . | Joshua McKenzie | Sam Tunnicliffe |
| [CASSANDRA-12282](https://issues.apache.org/jira/browse/CASSANDRA-12282) | SSTablesIteratedTest.testDeletionOnIndexedSSTableASC-compression failure |  Major | . | Joshua McKenzie | Stefania |
| [CASSANDRA-12353](https://issues.apache.org/jira/browse/CASSANDRA-12353) | CustomIndexTest can still fail due to async cleanup |  Minor | . | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-12379](https://issues.apache.org/jira/browse/CASSANDRA-12379) | CQLSH completion test broken by #12236 |  Major | Tools | Sylvain Lebresne | Stefania |
| [CASSANDRA-11701](https://issues.apache.org/jira/browse/CASSANDRA-11701) | [windows] dtest failure in cqlsh\_tests.cqlsh\_copy\_tests.CqlshCopyTest.test\_reading\_with\_skip\_and\_max\_rows |  Major | Tools | Russ Hatch | Stefania |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11578](https://issues.apache.org/jira/browse/CASSANDRA-11578) | remove DatabaseDescriptor dependency from FileUtil |  Minor | Local Write-Read Paths | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-11579](https://issues.apache.org/jira/browse/CASSANDRA-11579) | remove DatabaseDescriptor dependency from SequentialWriter |  Minor | Local Write-Read Paths | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-12041](https://issues.apache.org/jira/browse/CASSANDRA-12041) | Add CDC to describe table |  Major | Tools | Carl Yeksigian | Stefania |
| [CASSANDRA-12331](https://issues.apache.org/jira/browse/CASSANDRA-12331) | Unreleased Resource: Sockets |  Major | Streaming and Messaging | Eduardo Aguinaga | Arunkumar M |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12239](https://issues.apache.org/jira/browse/CASSANDRA-12239) | Add mshuler's key FE4B2BDA to dist/cassandra/KEYS |  Blocker | Packaging | Michael Shuler | Michael Shuler |


