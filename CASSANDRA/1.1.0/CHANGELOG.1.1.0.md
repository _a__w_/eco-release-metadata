
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.1.0 - 2012-04-24



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2503](https://issues.apache.org/jira/browse/CASSANDRA-2503) | Eagerly re-write data at read time ("superseding / defragmenting") |  Major | . | Stu Hood | Jonathan Ellis |
| [CASSANDRA-3452](https://issues.apache.org/jira/browse/CASSANDRA-3452) | Create an 'infinite bootstrap' mode for sampling live traffic |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-1600](https://issues.apache.org/jira/browse/CASSANDRA-1600) | Merge get\_indexed\_slices with get\_range\_slices |  Major | CQL | Stu Hood | Sylvain Lebresne |
| [CASSANDRA-2749](https://issues.apache.org/jira/browse/CASSANDRA-2749) | fine-grained control over data directories |  Minor | . | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-3523](https://issues.apache.org/jira/browse/CASSANDRA-3523) | support ALTER of CF attributes in CQL |  Critical | CQL, Tools | Jonathan Ellis | paul cannon |
| [CASSANDRA-2878](https://issues.apache.org/jira/browse/CASSANDRA-2878) | Allow map/reduce to use server-side query filters |  Critical | . | mck | Jonathan Ellis |
| [CASSANDRA-3657](https://issues.apache.org/jira/browse/CASSANDRA-3657) | Allow extending CompositeType comparator |  Critical | CQL | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-2475](https://issues.apache.org/jira/browse/CASSANDRA-2475) | Prepared statements |  Critical | CQL | Eric Evans | Rick Shaw |
| [CASSANDRA-2963](https://issues.apache.org/jira/browse/CASSANDRA-2963) | Add a convenient way to reset a node's schema |  Minor | Tools | Brandon Williams | Yuki Morishita |
| [CASSANDRA-3583](https://issues.apache.org/jira/browse/CASSANDRA-3583) | Add "rebuild index" JMX command |  Minor | Tools | Jonathan Ellis | Vijay |
| [CASSANDRA-3710](https://issues.apache.org/jira/browse/CASSANDRA-3710) | Add a configuration option to disable snapshots |  Minor | . | Brandon Williams | Dave Brosius |
| [CASSANDRA-2477](https://issues.apache.org/jira/browse/CASSANDRA-2477) | CQL support for describing keyspaces / column familes |  Minor | CQL | Eric Evans |  |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3271](https://issues.apache.org/jira/browse/CASSANDRA-3271) | off-heap cache to use sun.misc.Unsafe instead of JNA |  Minor | . | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-3005](https://issues.apache.org/jira/browse/CASSANDRA-3005) | OutboundTcpConnection's sending queue grows unboundedly without any backpressure logic |  Major | . | Melvin Wang | Melvin Wang |
| [CASSANDRA-3434](https://issues.apache.org/jira/browse/CASSANDRA-3434) | Explore using Guava (or guava inspired) faster bytes comparison |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2407](https://issues.apache.org/jira/browse/CASSANDRA-2407) | Compaction thread should try to empty a bucket before moving on |  Minor | . | Stu Hood | Jonathan Ellis |
| [CASSANDRA-1740](https://issues.apache.org/jira/browse/CASSANDRA-1740) | Nodetool commands to query and stop compaction, repair, cleanup and scrub |  Minor | Tools | Chip Salzenberg | Vijay |
| [CASSANDRA-3045](https://issues.apache.org/jira/browse/CASSANDRA-3045) | Update ColumnFamilyOutputFormat to use new bulkload API |  Minor | . | Jonathan Ellis | Brandon Williams |
| [CASSANDRA-3248](https://issues.apache.org/jira/browse/CASSANDRA-3248) | CommitLog writer should call fdatasync instead of fsync |  Major | . | Zhu Han | Rick Branson |
| [CASSANDRA-3538](https://issues.apache.org/jira/browse/CASSANDRA-3538) | Remove columns shadowed by a deleted container even when we cannot purge |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3561](https://issues.apache.org/jira/browse/CASSANDRA-3561) | Make incremental\_backups setting mutable via JMX |  Trivial | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3411](https://issues.apache.org/jira/browse/CASSANDRA-3411) | Pre-allocated, Recycled Commitlog Segment Files |  Minor | . | Rick Branson | Rick Branson |
| [CASSANDRA-3494](https://issues.apache.org/jira/browse/CASSANDRA-3494) | Streaming is mono-threaded (the bulk loader too by extension) |  Minor | . | Sylvain Lebresne | Peter Schuller |
| [CASSANDRA-3545](https://issues.apache.org/jira/browse/CASSANDRA-3545) | Fix very low Secondary Index performance |  Major | Secondary Indexes | Evgeny Ryabitskiy | Sylvain Lebresne |
| [CASSANDRA-3602](https://issues.apache.org/jira/browse/CASSANDRA-3602) | Remove test/distributed |  Trivial | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3476](https://issues.apache.org/jira/browse/CASSANDRA-3476) | Allow user to override jvm options picked by cassandra-env.sh |  Minor | . | Radim Kolar | Radim Kolar |
| [CASSANDRA-2876](https://issues.apache.org/jira/browse/CASSANDRA-2876) | JDBC 1.1 Roadmap of Enhancements |  Minor | . | Rick Shaw | Rick Shaw |
| [CASSANDRA-3619](https://issues.apache.org/jira/browse/CASSANDRA-3619) | Use a separate writer thread for the SSTableSimpleUnsortedWriter |  Minor | Tools | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3143](https://issues.apache.org/jira/browse/CASSANDRA-3143) | Global caches (key/row) |  Minor | . | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-2988](https://issues.apache.org/jira/browse/CASSANDRA-2988) | Improve SSTableReader.load() when loading index files |  Minor | . | Melvin Wang | Melvin Wang |
| [CASSANDRA-3664](https://issues.apache.org/jira/browse/CASSANDRA-3664) | [patch] fix some obvious javadoc issues generated via ant javadoc |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-3611](https://issues.apache.org/jira/browse/CASSANDRA-3611) | Make checksum on a compressed blocks optional |  Minor | . | Vijay | Vijay |
| [CASSANDRA-3679](https://issues.apache.org/jira/browse/CASSANDRA-3679) | clean up multithreaded streaming to use a ConcurrentMap instead of explicit locking |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3667](https://issues.apache.org/jira/browse/CASSANDRA-3667) | We need a way to deactivate row/key caching on a per-cf basis. |  Major | . | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-3197](https://issues.apache.org/jira/browse/CASSANDRA-3197) | Separate input and output connection details in ConfigHelper |  Minor | . | mck | mck |
| [CASSANDRA-3725](https://issues.apache.org/jira/browse/CASSANDRA-3725) | Switch stress tool to using micros |  Trivial | Tools | Cathy Daw | Yuki Morishita |
| [CASSANDRA-3747](https://issues.apache.org/jira/browse/CASSANDRA-3747) | Remove EndpointState.hasToken |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3688](https://issues.apache.org/jira/browse/CASSANDRA-3688) | [patch] avoid map lookups in loops by using entrysets |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-3689](https://issues.apache.org/jira/browse/CASSANDRA-3689) | [path] minor cleanup of compiler warnings |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-3760](https://issues.apache.org/jira/browse/CASSANDRA-3760) | Move the decompose() methods from o.a.c.db.marshal.\*Type to o.a.c.cql.jdbc.Jdbc\* |  Major | . | Rick Shaw | Rick Shaw |
| [CASSANDRA-1391](https://issues.apache.org/jira/browse/CASSANDRA-1391) | Allow Concurrent Schema Migrations |  Critical | . | Stu Hood | Pavel Yaskevich |
| [CASSANDRA-3743](https://issues.apache.org/jira/browse/CASSANDRA-3743) | Lower memory consumption used by index sampling |  Major | . | Radim Kolar | Radim Kolar |
| [CASSANDRA-3559](https://issues.apache.org/jira/browse/CASSANDRA-3559) | CFMetaData conversions to Thrift/Native schema should be inverse one of the other |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3264](https://issues.apache.org/jira/browse/CASSANDRA-3264) | Add wide row paging for ColumnFamilyInputFormat and ColumnFamilyOutputFormat |  Major | . | T Jake Luciani | Jonathan Ellis |
| [CASSANDRA-3716](https://issues.apache.org/jira/browse/CASSANDRA-3716) | Clean up isMarkedForDelete / getLocalDeletionTime |  Major | . | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-3692](https://issues.apache.org/jira/browse/CASSANDRA-3692) | Always use microsecond timestamps in the System Table |  Minor | . | amorton | amorton |
| [CASSANDRA-3568](https://issues.apache.org/jira/browse/CASSANDRA-3568) | cassandra-cli and nodetool should connect to localhost by default |  Minor | Tools | Rick Branson | Rick Branson |
| [CASSANDRA-3479](https://issues.apache.org/jira/browse/CASSANDRA-3479) | add read from/write to file support to cqlsh |  Minor | . | Jonathan Ellis | paul cannon |
| [CASSANDRA-3753](https://issues.apache.org/jira/browse/CASSANDRA-3753) | Update CqlPreparedResult to provide type information |  Critical | CQL | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-3798](https://issues.apache.org/jira/browse/CASSANDRA-3798) | get\_count paging often asks for a page uselessly |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3507](https://issues.apache.org/jira/browse/CASSANDRA-3507) | Proposal: separate cqlsh from CQL drivers |  Minor | Packaging, Tools | paul cannon | paul cannon |
| [CASSANDRA-3458](https://issues.apache.org/jira/browse/CASSANDRA-3458) | Add cqlsh to deb and rpm packaging |  Minor | Packaging | paul cannon | paul cannon |
| [CASSANDRA-3757](https://issues.apache.org/jira/browse/CASSANDRA-3757) | cqlsh: update syntax for tab completions |  Minor | Tools | paul cannon | paul cannon |
| [CASSANDRA-3823](https://issues.apache.org/jira/browse/CASSANDRA-3823) | [patch] remove bogus assert - never false |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-3793](https://issues.apache.org/jira/browse/CASSANDRA-3793) | remove deprecated KsDef.replication\_factor field |  Minor | CQL | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3840](https://issues.apache.org/jira/browse/CASSANDRA-3840) | Use java.io.tmpdir as default output location for BulkRecordWriter |  Major | . | Erik Forsberg | Erik Forsberg |
| [CASSANDRA-3639](https://issues.apache.org/jira/browse/CASSANDRA-3639) | Move streams too many data |  Minor | . | Fabien Rousseau | Fabien Rousseau |
| [CASSANDRA-3781](https://issues.apache.org/jira/browse/CASSANDRA-3781) | CQL support for changing row key type in ALTER TABLE |  Major | . | Rick Branson | Sylvain Lebresne |
| [CASSANDRA-3871](https://issues.apache.org/jira/browse/CASSANDRA-3871) | Turn compression on by default |  Major | . | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-3625](https://issues.apache.org/jira/browse/CASSANDRA-3625) | Do something about DynamicCompositeType |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2917](https://issues.apache.org/jira/browse/CASSANDRA-2917) | expose calculate midrange for token in jmx |  Minor | Tools | Jackson Chung | Sam Tunnicliffe |
| [CASSANDRA-3869](https://issues.apache.org/jira/browse/CASSANDRA-3869) | [patch] don't duplicate ByteBuffers when hashing |  Major | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-3879](https://issues.apache.org/jira/browse/CASSANDRA-3879) | [patch] fix typo in cql-selectstatement assert |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-2893](https://issues.apache.org/jira/browse/CASSANDRA-2893) | Add row-level isolation |  Minor | . | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-3877](https://issues.apache.org/jira/browse/CASSANDRA-3877) | Make secondary indexes inherit compression (and maybe other properties) from their base CFS |  Minor | Secondary Indexes | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3891](https://issues.apache.org/jira/browse/CASSANDRA-3891) | [patch] reduce duplicate lookups, allocations, interaction with threadlocals |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-3904](https://issues.apache.org/jira/browse/CASSANDRA-3904) | do not generate NPE on aborted stream-out sessions |  Minor | . | Peter Schuller | Peter Schuller |
| [CASSANDRA-3815](https://issues.apache.org/jira/browse/CASSANDRA-3815) | Alllow compression setting adjustment via JMX |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3754](https://issues.apache.org/jira/browse/CASSANDRA-3754) | Add TTL support to BulkOutputFormat |  Minor | . | Brandon Williams | Samarth Gahire |
| [CASSANDRA-3873](https://issues.apache.org/jira/browse/CASSANDRA-3873) | cqlsh: tab-complete key alias along with column names for ALTER COLUMNFAMILY [name] ALTER |  Trivial | Tools | paul cannon | paul cannon |
| [CASSANDRA-3956](https://issues.apache.org/jira/browse/CASSANDRA-3956) | [patch] minor javadoc fixes |  Trivial | . | Dave Brosius |  |
| [CASSANDRA-3958](https://issues.apache.org/jira/browse/CASSANDRA-3958) | Remove random HH delay |  Trivial | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3959](https://issues.apache.org/jira/browse/CASSANDRA-3959) | [patch] report bad meta data field in cli instead of silently ignoring |  Trivial | Tools | Dave Brosius | Dave Brosius |
| [CASSANDRA-3953](https://issues.apache.org/jira/browse/CASSANDRA-3953) | Replace deprecated and removed CfDef and KsDef attributes in thrift spec |  Minor | CQL | paul cannon | paul cannon |
| [CASSANDRA-3950](https://issues.apache.org/jira/browse/CASSANDRA-3950) | support trickling fsync() on writes |  Major | . | Peter Schuller | Peter Schuller |
| [CASSANDRA-3976](https://issues.apache.org/jira/browse/CASSANDRA-3976) | [patch[ don't compare byte arrays with == |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-3671](https://issues.apache.org/jira/browse/CASSANDRA-3671) | provide JMX counters for unavailables/timeouts for reads and writes |  Minor | Tools | Peter Schuller | Peter Schuller |
| [CASSANDRA-3952](https://issues.apache.org/jira/browse/CASSANDRA-3952) | avoid quadratic startup time in LeveledManifest |  Minor | . | Jonathan Ellis | Dave Brosius |
| [CASSANDRA-3792](https://issues.apache.org/jira/browse/CASSANDRA-3792) | add type information to new schema\_ columnfamilies |  Major | . | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-4017](https://issues.apache.org/jira/browse/CASSANDRA-4017) | Unify migrations |  Major | . | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-3859](https://issues.apache.org/jira/browse/CASSANDRA-3859) | Add Progress Reporting to Cassandra OutputFormats |  Minor | Tools | Samarth Gahire | Brandon Williams |
| [CASSANDRA-4063](https://issues.apache.org/jira/browse/CASSANDRA-4063) | Expose nodetool cfhistograms for secondary index CFs |  Minor | Secondary Indexes, Tools | Tyler Hobbs | Brandon Williams |
| [CASSANDRA-4037](https://issues.apache.org/jira/browse/CASSANDRA-4037) | Move CfDef and KsDef validation to CFMetaData and KSMetaData |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4067](https://issues.apache.org/jira/browse/CASSANDRA-4067) | Report lifetime compaction throughput |  Trivial | Tools | Jonathan Ellis | Brandon Williams |
| [CASSANDRA-3937](https://issues.apache.org/jira/browse/CASSANDRA-3937) | nodetool describering should report the schema version |  Trivial | . | Brandon Williams |  |
| [CASSANDRA-4023](https://issues.apache.org/jira/browse/CASSANDRA-4023) | Improve BloomFilter deserialization performance |  Minor | . | Joaquin Casares | Yuki Morishita |
| [CASSANDRA-4087](https://issues.apache.org/jira/browse/CASSANDRA-4087) | Improve out-of-the-box cache settings |  Major | . | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-4076](https://issues.apache.org/jira/browse/CASSANDRA-4076) | Remove get{Indexed,Sliced}ReadBufferSizeInKB methods |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4088](https://issues.apache.org/jira/browse/CASSANDRA-4088) | Respect 1.0 cache settings as much as possible when upgrading |  Major | . | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-4107](https://issues.apache.org/jira/browse/CASSANDRA-4107) | fix broken link in cassandra-env.sh |  Major | . | Ilya Shipitsin | Ilya Shipitsin |
| [CASSANDRA-3951](https://issues.apache.org/jira/browse/CASSANDRA-3951) | make thrift interface "backwards compat" guarantee more specific |  Minor | CQL | paul cannon | paul cannon |
| [CASSANDRA-4110](https://issues.apache.org/jira/browse/CASSANDRA-4110) | Relax path length requirement for non-Windows platforms |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4103](https://issues.apache.org/jira/browse/CASSANDRA-4103) | Add stress tool to binaries |  Minor | Tools | Rick Branson | Vijay |
| [CASSANDRA-4140](https://issues.apache.org/jira/browse/CASSANDRA-4140) | Build stress classes in a location that allows tools/stress/bin/stress to find them |  Trivial | Tools | Nick Bailey | Vijay |
| [CASSANDRA-4157](https://issues.apache.org/jira/browse/CASSANDRA-4157) | Allow KS + CF names up to 48 characters |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4167](https://issues.apache.org/jira/browse/CASSANDRA-4167) | nodetool compactionstats displays the compaction's remaining time |  Trivial | Tools | Fabien Rousseau | Fabien Rousseau |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3272](https://issues.apache.org/jira/browse/CASSANDRA-3272) | READ Operation with CL=EACH\_QUORUM succeed when a DC is down (RF=3) |  Minor | . | Jackson Chung | Jonathan Ellis |
| [CASSANDRA-3116](https://issues.apache.org/jira/browse/CASSANDRA-3116) | Compactions can (seriously) delay schema migrations |  Major | . | Eric Evans | Jonathan Ellis |
| [CASSANDRA-3445](https://issues.apache.org/jira/browse/CASSANDRA-3445) | recognize that "SELECT first ... \*" isn't really "SELECT \*" |  Minor | CQL | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-3410](https://issues.apache.org/jira/browse/CASSANDRA-3410) | inconsistent rejection of CL.ANY on reads |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3503](https://issues.apache.org/jira/browse/CASSANDRA-3503) | IncomingStreamReader uses socket.getRemoteSocketAddress() which might be diffrent than FB.getBroadcastAddress() |  Minor | . | Vijay | Vijay |
| [CASSANDRA-1034](https://issues.apache.org/jira/browse/CASSANDRA-1034) | Remove assumption that Key to Token is one-to-one |  Minor | . | Stu Hood | Sylvain Lebresne |
| [CASSANDRA-3543](https://issues.apache.org/jira/browse/CASSANDRA-3543) | Commit Log Allocator deadlock after first start with empty commitlog directory |  Major | . | Brandon Williams | Rick Branson |
| [CASSANDRA-3557](https://issues.apache.org/jira/browse/CASSANDRA-3557) | Commit Log segments are not recycled |  Major | . | Rick Branson | Rick Branson |
| [CASSANDRA-3574](https://issues.apache.org/jira/browse/CASSANDRA-3574) | AssertionError in DK |  Major | . | Brandon Williams | Sylvain Lebresne |
| [CASSANDRA-3566](https://issues.apache.org/jira/browse/CASSANDRA-3566) | Cache saving broken on windows |  Minor | . | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-3577](https://issues.apache.org/jira/browse/CASSANDRA-3577) | TimeoutException When using QuorumEach or ALL consistency on Multi-DC |  Major | . | Vijay | Vijay |
| [CASSANDRA-3582](https://issues.apache.org/jira/browse/CASSANDRA-3582) | UserInterruptedException is poorly encapsulated |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3604](https://issues.apache.org/jira/browse/CASSANDRA-3604) | Bad code in org.apache.cassandra.cql.QueryProcessor |  Major | . | Zoltan Farkas | Sylvain Lebresne |
| [CASSANDRA-3250](https://issues.apache.org/jira/browse/CASSANDRA-3250) | fsync the directory after new sstable or commit log segment are created |  Minor | . | Zhu Han | Pavel Yaskevich |
| [CASSANDRA-3651](https://issues.apache.org/jira/browse/CASSANDRA-3651) | Truncate shouldn't rethrow timeouts as UA |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3658](https://issues.apache.org/jira/browse/CASSANDRA-3658) | Fix smallish problems find by FindBugs |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3638](https://issues.apache.org/jira/browse/CASSANDRA-3638) | rangeSlice may iterate the whole memtable while just query one row . This may seriously affect the  performance . |  Minor | . | MaHaiyang | Sylvain Lebresne |
| [CASSANDRA-3615](https://issues.apache.org/jira/browse/CASSANDRA-3615) | CommitLog BufferOverflowException |  Major | . | Rick Branson | Rick Branson |
| [CASSANDRA-2805](https://issues.apache.org/jira/browse/CASSANDRA-2805) | Clean up mbeans that return Internal Cassandra types |  Minor | . | Nick Bailey | Nick Bailey |
| [CASSANDRA-3641](https://issues.apache.org/jira/browse/CASSANDRA-3641) | inconsistent/corrupt counters w/ broken shards never converge |  Major | . | Peter Schuller | Peter Schuller |
| [CASSANDRA-3691](https://issues.apache.org/jira/browse/CASSANDRA-3691) | LeveledCompactionStrategy is broken because of generation pre-allocation in LeveledManifest. |  Major | . | Pavel Yaskevich | Sylvain Lebresne |
| [CASSANDRA-3694](https://issues.apache.org/jira/browse/CASSANDRA-3694) | ClassCastException during hinted handoff |  Major | . | Brandon Williams | Sylvain Lebresne |
| [CASSANDRA-3736](https://issues.apache.org/jira/browse/CASSANDRA-3736) | -Dreplace\_token leaves old node (IP) in the gossip with the token. |  Major | . | Jackson Chung | Vijay |
| [CASSANDRA-3766](https://issues.apache.org/jira/browse/CASSANDRA-3766) | KeysIndex is broken by CASSANDRA-1600 |  Trivial | . | Philip Andronov | Philip Andronov |
| [CASSANDRA-3752](https://issues.apache.org/jira/browse/CASSANDRA-3752) | bulk loader no longer finds sstables |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3765](https://issues.apache.org/jira/browse/CASSANDRA-3765) | hadoop word count example is unable to output to cassandra with default settings |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3784](https://issues.apache.org/jira/browse/CASSANDRA-3784) | RangeTest.java compilation error on Range.rangeSet in Eclipse (not Ant or IntelliJ) |  Trivial | Testing | Kirk True | Kirk True |
| [CASSANDRA-3786](https://issues.apache.org/jira/browse/CASSANDRA-3786) | [patch] fix bad comparison of IColumn to ByteBuffer |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-3788](https://issues.apache.org/jira/browse/CASSANDRA-3788) | [patch] fix bad comparison in hadoop cf recorder reader |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-3787](https://issues.apache.org/jira/browse/CASSANDRA-3787) | [patch] fix bad comparison of column name against \* or 1 |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-3806](https://issues.apache.org/jira/browse/CASSANDRA-3806) | merge from 1.0 (aa20c7206cdc1efc1983466de05c224eccac1084) breaks build |  Major | . | Peter Schuller | Peter Schuller |
| [CASSANDRA-3483](https://issues.apache.org/jira/browse/CASSANDRA-3483) | Support bringing up a new datacenter to existing cluster without repair |  Major | . | Chris Goffinet | Peter Schuller |
| [CASSANDRA-3812](https://issues.apache.org/jira/browse/CASSANDRA-3812) | Can't start a node with row\_cache\_size\_in\_mb=1 |  Major | . | Tyler Patterson | Yuki Morishita |
| [CASSANDRA-3824](https://issues.apache.org/jira/browse/CASSANDRA-3824) | [patch] add missing break in nodecmd's command dispatching for SETSTREAMTHROUGHPUT |  Trivial | Tools | Dave Brosius | Dave Brosius |
| [CASSANDRA-3835](https://issues.apache.org/jira/browse/CASSANDRA-3835) | FB.broadcastAddress fixes and Soft reset on Ec2MultiRegionSnitch.reconnect |  Minor | . | Vijay | Vijay |
| [CASSANDRA-3826](https://issues.apache.org/jira/browse/CASSANDRA-3826) | Pig cannot use output formats other than CFOF |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3839](https://issues.apache.org/jira/browse/CASSANDRA-3839) | BulkOutputFormat binds to wrong client address when client is Dual-stack and server is IPv6 |  Major | . | Erik Forsberg | Brandon Williams |
| [CASSANDRA-3828](https://issues.apache.org/jira/browse/CASSANDRA-3828) | BulkOutputFormat shouldn't need flags to specify the output type |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3850](https://issues.apache.org/jira/browse/CASSANDRA-3850) | get\_indexed\_slices losts index expressions |  Major | . | Philip Andronov |  |
| [CASSANDRA-3849](https://issues.apache.org/jira/browse/CASSANDRA-3849) | Saved CF row cache breaks when upgrading to 1.1 |  Major | . | Tyler Patterson | Pavel Yaskevich |
| [CASSANDRA-3735](https://issues.apache.org/jira/browse/CASSANDRA-3735) | Fix "Unable to create hard link" SSTableReaderTest error messages |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3827](https://issues.apache.org/jira/browse/CASSANDRA-3827) | nosetests / system tests fail |  Major | Testing | Michael Allen | Pavel Yaskevich |
| [CASSANDRA-3863](https://issues.apache.org/jira/browse/CASSANDRA-3863) | Nodetool ring output not sorted by token order |  Minor | Tools | Michael Allen | Yuki Morishita |
| [CASSANDRA-3803](https://issues.apache.org/jira/browse/CASSANDRA-3803) | snapshot-before-compaction snapshots entire keyspace |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3832](https://issues.apache.org/jira/browse/CASSANDRA-3832) | gossip stage backed up due to migration manager future de-ref |  Blocker | . | Peter Schuller | Peter Schuller |
| [CASSANDRA-3831](https://issues.apache.org/jira/browse/CASSANDRA-3831) | scaling to large clusters in GossipStage impossible due to calculatePendingRanges |  Major | . | Peter Schuller | Peter Schuller |
| [CASSANDRA-2851](https://issues.apache.org/jira/browse/CASSANDRA-2851) | hex-to-bytes conversion accepts invalid inputs silently |  Minor | . | David Allsopp | Sylvain Lebresne |
| [CASSANDRA-3888](https://issues.apache.org/jira/browse/CASSANDRA-3888) | remove no-longer-valid values from ColumnFamilyArgument enum |  Major | Tools | Peter Schuller | Peter Schuller |
| [CASSANDRA-3872](https://issues.apache.org/jira/browse/CASSANDRA-3872) | Sub-columns removal is broken in 1.1 |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3740](https://issues.apache.org/jira/browse/CASSANDRA-3740) | While using BulkOutputFormat  unneccessarily look for the cassandra.yaml file. |  Major | . | Samarth Gahire | Brandon Williams |
| [CASSANDRA-3906](https://issues.apache.org/jira/browse/CASSANDRA-3906) | BulkRecordWriter throws NPE for counter columns |  Major | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-3907](https://issues.apache.org/jira/browse/CASSANDRA-3907) | Support compression using BulkWriter |  Major | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-3864](https://issues.apache.org/jira/browse/CASSANDRA-3864) | Unit tests failures in 1.1 |  Major | . | Sylvain Lebresne | Brandon Williams |
| [CASSANDRA-3915](https://issues.apache.org/jira/browse/CASSANDRA-3915) | Fix LazilyCompactedRowTest |  Minor | Testing | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3917](https://issues.apache.org/jira/browse/CASSANDRA-3917) | System test failures in 1.1 |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3922](https://issues.apache.org/jira/browse/CASSANDRA-3922) | streaming from all (not one) neighbors during rebuild/bootstrap |  Blocker | . | Peter Schuller | Peter Schuller |
| [CASSANDRA-3921](https://issues.apache.org/jira/browse/CASSANDRA-3921) | Compaction doesn't clear out expired tombstones from SerializingCache |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3903](https://issues.apache.org/jira/browse/CASSANDRA-3903) | Intermittent unexpected errors: possibly race condition around CQL parser? |  Major | . | paul cannon | Sylvain Lebresne |
| [CASSANDRA-3931](https://issues.apache.org/jira/browse/CASSANDRA-3931) | gossipers notion of schema differs from reality as reported by the nodes in question |  Major | . | Peter Schuller | Brandon Williams |
| [CASSANDRA-3944](https://issues.apache.org/jira/browse/CASSANDRA-3944) | BulkRecordWriter will throw NullExceptions if no data is sent with the reducer |  Major | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-3821](https://issues.apache.org/jira/browse/CASSANDRA-3821) | Counters in super columns don't preserve correct values after cluster restart |  Major | . | Tyler Patterson | Sylvain Lebresne |
| [CASSANDRA-3934](https://issues.apache.org/jira/browse/CASSANDRA-3934) | Short read protection is broken |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3940](https://issues.apache.org/jira/browse/CASSANDRA-3940) | mergeShardsChance deprecated; remove from thrift? |  Trivial | CQL | paul cannon | Sylvain Lebresne |
| [CASSANDRA-3884](https://issues.apache.org/jira/browse/CASSANDRA-3884) | Intermittent SchemaDisagreementException |  Major | . | Tyler Patterson | Pavel Yaskevich |
| [CASSANDRA-3804](https://issues.apache.org/jira/browse/CASSANDRA-3804) | upgrade problems from 1.0 to trunk |  Major | . | Tyler Patterson | Pavel Yaskevich |
| [CASSANDRA-3537](https://issues.apache.org/jira/browse/CASSANDRA-3537) | ExpiringMap timer is not exception-proof |  Minor | . | MaHaiyang | Jonathan Ellis |
| [CASSANDRA-3955](https://issues.apache.org/jira/browse/CASSANDRA-3955) | HintedHandoff won't compact a single sstable, resulting in repeated log messages |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3963](https://issues.apache.org/jira/browse/CASSANDRA-3963) | Exception durint start up after updating cassandra |  Major | . | Mariusz | Pavel Yaskevich |
| [CASSANDRA-3962](https://issues.apache.org/jira/browse/CASSANDRA-3962) | CassandraStorage can't cast fields from a CF correctly |  Major | . | Janne Jalkanen | Brandon Williams |
| [CASSANDRA-3973](https://issues.apache.org/jira/browse/CASSANDRA-3973) | Pig CounterColumnFamily support |  Major | . | Janne Jalkanen | Janne Jalkanen |
| [CASSANDRA-3972](https://issues.apache.org/jira/browse/CASSANDRA-3972) | HintedHandoff fails to deliver any hints |  Blocker | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3862](https://issues.apache.org/jira/browse/CASSANDRA-3862) | RowCache misses Updates |  Major | . | Daniel Doubleday | Sylvain Lebresne |
| [CASSANDRA-3975](https://issues.apache.org/jira/browse/CASSANDRA-3975) | Hints Should Be Dropped When Missing CFid Implies Deleted ColumnFamily |  Major | . | Chris Herron | Jonathan Ellis |
| [CASSANDRA-3797](https://issues.apache.org/jira/browse/CASSANDRA-3797) | StorageProxy static initialization not triggered until thrift requests come in |  Minor | . | Peter Schuller | Peter Schuller |
| [CASSANDRA-3684](https://issues.apache.org/jira/browse/CASSANDRA-3684) | Composite Column Support for PIG |  Major | . | Benjamin Coverston | Janne Jalkanen |
| [CASSANDRA-3981](https://issues.apache.org/jira/browse/CASSANDRA-3981) | Don't include original exception class name in CQL message |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3986](https://issues.apache.org/jira/browse/CASSANDRA-3986) | Cli shouldn't call FBU.getBroadcastAddress needlessly |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3509](https://issues.apache.org/jira/browse/CASSANDRA-3509) | cassandra-cli shows org.apache.Cassandra.XXX in example help for replication strategy but it should be cassandra with a lowercase c |  Trivial | . | Matthew F. Dennis | Tommy Tynjä |
| [CASSANDRA-3984](https://issues.apache.org/jira/browse/CASSANDRA-3984) | missing documents for caching in 1.1 |  Major | Documentation and Website | Vijay | Vijay |
| [CASSANDRA-3957](https://issues.apache.org/jira/browse/CASSANDRA-3957) | Supercolumn serialization assertion failure |  Major | . | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-3989](https://issues.apache.org/jira/browse/CASSANDRA-3989) | nodetool cleanup/scrub/upgradesstables promotes all sstables to next level (LeveledCompaction) |  Minor | . | Maki Watanabe | Sylvain Lebresne |
| [CASSANDRA-4019](https://issues.apache.org/jira/browse/CASSANDRA-4019) | java.util.ConcurrentModificationException in Gossiper |  Minor | . | Thibaut | Brandon Williams |
| [CASSANDRA-4026](https://issues.apache.org/jira/browse/CASSANDRA-4026) | EC2 snitch incorrectly reports regions |  Major | . | Todd Nine | Vijay |
| [CASSANDRA-3926](https://issues.apache.org/jira/browse/CASSANDRA-3926) | all column validator options are not represented in cli help |  Minor | Documentation and Website | Jeremy Hanna | Kirk True |
| [CASSANDRA-4044](https://issues.apache.org/jira/browse/CASSANDRA-4044) | examples/simple\_authenticator README doesn't mention JVM args |  Trivial | Documentation and Website | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-3954](https://issues.apache.org/jira/browse/CASSANDRA-3954) | Exceptions during start up after schema disagreement |  Major | . | Mariusz | Pavel Yaskevich |
| [CASSANDRA-3967](https://issues.apache.org/jira/browse/CASSANDRA-3967) | Test fixes for Windows |  Minor | Testing | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4031](https://issues.apache.org/jira/browse/CASSANDRA-4031) | Exceptions during inserting emtpy string as column value on indexed column |  Major | . | Mariusz | Yuki Morishita |
| [CASSANDRA-4042](https://issues.apache.org/jira/browse/CASSANDRA-4042) | add "caching" to CQL CF options |  Minor | . | Pavel Yaskevich | Sylvain Lebresne |
| [CASSANDRA-4086](https://issues.apache.org/jira/browse/CASSANDRA-4086) | decom should shut thrift down |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-4033](https://issues.apache.org/jira/browse/CASSANDRA-4033) | cqlsh: double wide unicode chars cause incorrect padding in select output |  Trivial | Tools | Tyler Patterson | paul cannon |
| [CASSANDRA-4003](https://issues.apache.org/jira/browse/CASSANDRA-4003) | cqlsh still failing to handle decode errors in some column names |  Minor | Tools | paul cannon | paul cannon |
| [CASSANDRA-3996](https://issues.apache.org/jira/browse/CASSANDRA-3996) | Keys index skips results |  Major | . | Dmitry Petrashko | Pavel Yaskevich |
| [CASSANDRA-3612](https://issues.apache.org/jira/browse/CASSANDRA-3612) | CQL inserting blank key. |  Minor | CQL | samal | paul cannon |
| [CASSANDRA-4095](https://issues.apache.org/jira/browse/CASSANDRA-4095) | Internal error processing get\_slice (NullPointerException) |  Minor | . | John Laban | Jonathan Ellis |
| [CASSANDRA-4077](https://issues.apache.org/jira/browse/CASSANDRA-4077) | ScrubTest failing on current 1.1.0 branch |  Major | Testing | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-4096](https://issues.apache.org/jira/browse/CASSANDRA-4096) | mlockall() returned code is ignored w/o assertions |  Minor | . | Peter Schuller | Jonathan Ellis |
| [CASSANDRA-4090](https://issues.apache.org/jira/browse/CASSANDRA-4090) | cqlsh can't handle python being a python3 |  Trivial | Tools | Andrew Ash | Andrew Ash |
| [CASSANDRA-3732](https://issues.apache.org/jira/browse/CASSANDRA-3732) | Update POM generation after migration to git |  Minor | Packaging | Sylvain Lebresne | Stephen Connolly |
| [CASSANDRA-4111](https://issues.apache.org/jira/browse/CASSANDRA-4111) | Serializing cache can cause Segfault in 1.1 |  Major | . | Vijay | Vijay |
| [CASSANDRA-4114](https://issues.apache.org/jira/browse/CASSANDRA-4114) | Default read\_repair\_chance value is wrong |  Trivial | Tools | Manoj Mainali | Jonathan Ellis |
| [CASSANDRA-4128](https://issues.apache.org/jira/browse/CASSANDRA-4128) | stress tool hangs forever on timeout or error |  Minor | Tools | Tyler Patterson | Pavel Yaskevich |
| [CASSANDRA-4093](https://issues.apache.org/jira/browse/CASSANDRA-4093) | schema\_\* CFs do not respect column comparator which leads to CLI commands failure. |  Major | Tools | Dave Brosius | Sylvain Lebresne |
| [CASSANDRA-4136](https://issues.apache.org/jira/browse/CASSANDRA-4136) | get\_paged\_slices doesn't reset startColumn after first row |  Critical | . | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-4032](https://issues.apache.org/jira/browse/CASSANDRA-4032) | memtable.updateLiveRatio() is blocking, causing insane latencies for writes |  Major | . | Peter Schuller | Peter Schuller |
| [CASSANDRA-3883](https://issues.apache.org/jira/browse/CASSANDRA-3883) | CFIF WideRowIterator only returns batch size columns |  Major | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-4051](https://issues.apache.org/jira/browse/CASSANDRA-4051) | Stream sessions can only fail via the FailureDetector |  Major | . | Brandon Williams | Yuki Morishita |
| [CASSANDRA-4141](https://issues.apache.org/jira/browse/CASSANDRA-4141) | Looks like Serializing cache broken in 1.1 |  Major | . | Vijay | Vijay |
| [CASSANDRA-4145](https://issues.apache.org/jira/browse/CASSANDRA-4145) | NullPointerException when using sstableloader with PropertyFileSnitch configured |  Minor | Tools | Ji Cheng | Ji Cheng |
| [CASSANDRA-4154](https://issues.apache.org/jira/browse/CASSANDRA-4154) | CFRR wide row iterator does not handle tombstones well |  Major | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-4156](https://issues.apache.org/jira/browse/CASSANDRA-4156) | CQL should support CL.TWO |  Minor | CQL | paul cannon | Matthew F. Dennis |
| [CASSANDRA-3946](https://issues.apache.org/jira/browse/CASSANDRA-3946) | BulkRecordWriter shouldn't stream any empty data/index files that might be created at end of flush |  Minor | . | Chris Goffinet | Yuki Morishita |
| [CASSANDRA-4065](https://issues.apache.org/jira/browse/CASSANDRA-4065) | Bogus MemoryMeter liveRatio calculations |  Minor | . | Daniel Doubleday | Daniel Doubleday |
| [CASSANDRA-4161](https://issues.apache.org/jira/browse/CASSANDRA-4161) | CQL 3.0 does not work in cqlsh with uppercase SELECT |  Minor | Tools | Jonas Dohse | Jonas Dohse |
| [CASSANDRA-4163](https://issues.apache.org/jira/browse/CASSANDRA-4163) | CQL3 ALTER TABLE command causes NPE |  Major | . | K. B. Hahn | paul cannon |
| [CASSANDRA-4170](https://issues.apache.org/jira/browse/CASSANDRA-4170) | cql3 ALTER TABLE ALTER TYPE has no effect |  Major | CQL | paul cannon | Sylvain Lebresne |
| [CASSANDRA-4171](https://issues.apache.org/jira/browse/CASSANDRA-4171) | cql3 ALTER TABLE foo WITH default\_validation=int has no effect |  Trivial | CQL | paul cannon | Sylvain Lebresne |
| [CASSANDRA-4099](https://issues.apache.org/jira/browse/CASSANDRA-4099) | IncomingTCPConnection recognizes from by doing socket.getInetAddress() instead of BroadCastAddress |  Minor | . | Vijay | Vijay |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3717](https://issues.apache.org/jira/browse/CASSANDRA-3717) | remove contrib/javautils |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3742](https://issues.apache.org/jira/browse/CASSANDRA-3742) | Allow to do a range slice with a limit on the number of column across all rows |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3634](https://issues.apache.org/jira/browse/CASSANDRA-3634) | compare string vs. binary prepared statement parameters |  Minor | CQL | Eric Evans | Eric Evans |
| [CASSANDRA-3749](https://issues.apache.org/jira/browse/CASSANDRA-3749) | Allow rangeSlice queries to be start/end inclusive/exclusive |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3713](https://issues.apache.org/jira/browse/CASSANDRA-3713) | move pig from contrib into src and examples |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3791](https://issues.apache.org/jira/browse/CASSANDRA-3791) | Support query by names for compact CF |  Minor | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3778](https://issues.apache.org/jira/browse/CASSANDRA-3778) | KEY IN (...) queries do not work |  Major | CQL | Eric Evans | Sylvain Lebresne |
| [CASSANDRA-3925](https://issues.apache.org/jira/browse/CASSANDRA-3925) | ORDER BY syntax |  Minor | CQL | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-3676](https://issues.apache.org/jira/browse/CASSANDRA-3676) | Add snaptree dependency to maven central and update pom |  Major | . | T Jake Luciani | Stephen Connolly |
| [CASSANDRA-3990](https://issues.apache.org/jira/browse/CASSANDRA-3990) | cqlsh support for CQL 3 |  Minor | CQL | Sylvain Lebresne | paul cannon |
| [CASSANDRA-3785](https://issues.apache.org/jira/browse/CASSANDRA-3785) | Support slice with exclusive start and stop |  Major | . | Sylvain Lebresne | Sylvain Lebresne |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3567](https://issues.apache.org/jira/browse/CASSANDRA-3567) | remove unmaintained redhat rpm packaging |  Minor | Packaging | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3622](https://issues.apache.org/jira/browse/CASSANDRA-3622) | clean up openbitset |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3213](https://issues.apache.org/jira/browse/CASSANDRA-3213) | Upgrade Thrift |  Trivial | . | Jake Farrell | Jake Farrell |
| [CASSANDRA-1805](https://issues.apache.org/jira/browse/CASSANDRA-1805) | refactor and remove contrib/ |  Minor | . | Jonathan Ellis |  |
| [CASSANDRA-3914](https://issues.apache.org/jira/browse/CASSANDRA-3914) | Remove py\_stress |  Trivial | Tools | Brandon Williams | Brandon Williams |


