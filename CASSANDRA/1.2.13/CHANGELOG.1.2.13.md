
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.2.13 - 2013-12-20



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6234](https://issues.apache.org/jira/browse/CASSANDRA-6234) | Add metrics for native protocols |  Major | . | Adam Hattrell | Mikhail Stepura |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6386](https://issues.apache.org/jira/browse/CASSANDRA-6386) | FD mean calculation performance improvement |  Minor | . | Quentin Conner | Jonathan Ellis |
| [CASSANDRA-5598](https://issues.apache.org/jira/browse/CASSANDRA-5598) | Need invalid exception when submitting a prepared statement with too many markers |  Minor | . | Anne Sullivan | Lyuben Todorov |
| [CASSANDRA-6268](https://issues.apache.org/jira/browse/CASSANDRA-6268) | Poor performance of Hadoop if any DC is using VNodes |  Major | . | Piotr Kołaczkowski | Piotr Kołaczkowski |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6329](https://issues.apache.org/jira/browse/CASSANDRA-6329) | example pig script doesn't run |  Trivial | . | Jeremy Hanna | Jeremy Hanna |
| [CASSANDRA-6385](https://issues.apache.org/jira/browse/CASSANDRA-6385) | FD phi estimator initial conditions |  Minor | . | Quentin Conner | Jonathan Ellis |
| [CASSANDRA-5750](https://issues.apache.org/jira/browse/CASSANDRA-5750) | CLI can show bad DESCRIBE for CQL3 CF if given the CF explicitly |  Minor | . | Ryan McGuire | Sylvain Lebresne |
| [CASSANDRA-6396](https://issues.apache.org/jira/browse/CASSANDRA-6396) | debian init searches for jdk6 explicitly |  Minor | Packaging | Brandon Williams | Brandon Williams |
| [CASSANDRA-6172](https://issues.apache.org/jira/browse/CASSANDRA-6172) | COPY TO command doesn't escape single quote in collections |  Minor | Tools | Ivan Mykhailov | Mikhail Stepura |
| [CASSANDRA-6342](https://issues.apache.org/jira/browse/CASSANDRA-6342) | cassandra document errata |  Trivial | . | Kim Yong Hwan | Lyuben Todorov |
| [CASSANDRA-6345](https://issues.apache.org/jira/browse/CASSANDRA-6345) | Endpoint cache invalidation causes CPU spike (on vnode rings?) |  Major | . | Rick Branson | Jonathan Ellis |
| [CASSANDRA-6409](https://issues.apache.org/jira/browse/CASSANDRA-6409) | gossip performance improvement at node startup |  Major | . | Quentin Conner | Jonathan Ellis |
| [CASSANDRA-6316](https://issues.apache.org/jira/browse/CASSANDRA-6316) | json2sstable breaks on RangeTombstone |  Minor | . | Matt Jurik | Lyuben Todorov |
| [CASSANDRA-6415](https://issues.apache.org/jira/browse/CASSANDRA-6415) | Snapshot repair blocks for ever if something happens to the "I made my snapshot" response |  Major | . | Jeremiah Jordan | Yuki Morishita |
| [CASSANDRA-5428](https://issues.apache.org/jira/browse/CASSANDRA-5428) | CQL3 don't validate that collections haven't more than 64K elements |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6413](https://issues.apache.org/jira/browse/CASSANDRA-6413) | Saved KeyCache prints success to log; but no file present |  Minor | . | Chris Burroughs | Mikhail Stepura |
| [CASSANDRA-6416](https://issues.apache.org/jira/browse/CASSANDRA-6416) | MoveTest fails in 1.2+ |  Major | Testing | Jonathan Ellis | Michael Shuler |
| [CASSANDRA-6459](https://issues.apache.org/jira/browse/CASSANDRA-6459) | Concurrency issue in Directories.getOrCreate() |  Major | . | Ryan Fowler | Ryan Fowler |
| [CASSANDRA-6481](https://issues.apache.org/jira/browse/CASSANDRA-6481) | Batchlog endpoint candidates should be picked randomly, not sorted by proximity |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6309](https://issues.apache.org/jira/browse/CASSANDRA-6309) | Pig CqlStorage generates  ERROR 1108: Duplicate schema alias |  Major | . | Thunder Stumpges | Alex Liu |
| [CASSANDRA-6485](https://issues.apache.org/jira/browse/CASSANDRA-6485) | NPE in calculateNaturalEndpoints |  Major | . | Russell Spitzer | Jonathan Ellis |
| [CASSANDRA-6488](https://issues.apache.org/jira/browse/CASSANDRA-6488) | Batchlog writes consume unnecessarily large amounts of CPU on vnodes clusters |  Major | . | Rick Branson | Rick Branson |


