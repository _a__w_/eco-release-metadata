
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.6.6 - 2010-10-14



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1417](https://issues.apache.org/jira/browse/CASSANDRA-1417) | add cache save/load |  Major | . | Jonathan Ellis | Matthew F. Dennis |
| [CASSANDRA-1579](https://issues.apache.org/jira/browse/CASSANDRA-1579) | tuneable column size for stress.py |  Major | . | Jonathan Ellis | Tyler Hobbs |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1465](https://issues.apache.org/jira/browse/CASSANDRA-1465) | optimize OutboundTcpConnectionPool by removing synchronization, string comparisons |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1481](https://issues.apache.org/jira/browse/CASSANDRA-1481) | PropertyFileEndPointSnitch synchronization is a bottleneck |  Minor | . | Jonathan Ellis | Chris Goffinet |
| [CASSANDRA-1479](https://issues.apache.org/jira/browse/CASSANDRA-1479) | nodetool compacts all keyspaces |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-1371](https://issues.apache.org/jira/browse/CASSANDRA-1371) | replace createHardLink with JNA wrapper to link (posix) / CreateHardLink (windows) |  Minor | . | Jonathan Ellis | Brandon Williams |
| [CASSANDRA-1074](https://issues.apache.org/jira/browse/CASSANDRA-1074) | check bloom filters to make minor compaction able to delete (some) tombstones |  Major | . | Robert Coli | Sylvain Lebresne |
| [CASSANDRA-1226](https://issues.apache.org/jira/browse/CASSANDRA-1226) | make DTPE handle exceptions the same way as CassandraDaemon |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1524](https://issues.apache.org/jira/browse/CASSANDRA-1524) | remove requirement for unnecessary quoting in cli |  Major | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1558](https://issues.apache.org/jira/browse/CASSANDRA-1558) | add memtable, cache to what GCInspector logs |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1526](https://issues.apache.org/jira/browse/CASSANDRA-1526) | Make cassandra sampling and startup faster |  Minor | . | Edward Capriolo | Jonathan Ellis |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1429](https://issues.apache.org/jira/browse/CASSANDRA-1429) | The dynamic snitch can't be used with network topology strategies |  Major | . | Sylvain Lebresne | Jonathan Ellis |
| [CASSANDRA-1432](https://issues.apache.org/jira/browse/CASSANDRA-1432) | java.util.NoSuchElementException when returning a node to the cluster |  Minor | . | amorton | Jonathan Ellis |
| [CASSANDRA-1446](https://issues.apache.org/jira/browse/CASSANDRA-1446) | cassandra-cli still relies on cassandra.in.sh instead of cassandra-env.sh |  Trivial | . | Brandon Williams | Eric Evans |
| [CASSANDRA-1463](https://issues.apache.org/jira/browse/CASSANDRA-1463) | Failed bootstrap can cause NPE in batch\_mutate on every node, taking down the entire cluster |  Major | . | David King | Jonathan Ellis |
| [CASSANDRA-1348](https://issues.apache.org/jira/browse/CASSANDRA-1348) | Failed to delete commitlog when restarting service |  Minor | . | Viktor Jevdokimov | Jonathan Ellis |
| [CASSANDRA-1458](https://issues.apache.org/jira/browse/CASSANDRA-1458) | SSTable cleanup killed by IllegalStateException |  Minor | . | Christopher Gist | Jonathan Ellis |
| [CASSANDRA-1492](https://issues.apache.org/jira/browse/CASSANDRA-1492) | Upgrade from 0.6.3 to 0.6.5, exception when replay commitlog |  Major | . | Zhong Li | Jonathan Ellis |
| [CASSANDRA-1467](https://issues.apache.org/jira/browse/CASSANDRA-1467) | replication factor exceeds number of endpoints, when attempting to join a new node (but cluster has enough running nodes to fulfill RF) |  Major | . | Josep M. Blanquer | Gary Dusbabek |
| [CASSANDRA-1512](https://issues.apache.org/jira/browse/CASSANDRA-1512) | cassandra will replay the last mutation in a commitlog when it shouldn't |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1528](https://issues.apache.org/jira/browse/CASSANDRA-1528) | Cassandra holds a socket in CLOSE\_WAIT on the storage port |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-1447](https://issues.apache.org/jira/browse/CASSANDRA-1447) | SimpleAuthenticator MD5 support |  Minor | . | Stu Hood | Nirmal Ranganathan |
| [CASSANDRA-1550](https://issues.apache.org/jira/browse/CASSANDRA-1550) | enable/disable HH via JMX |  Minor | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1330](https://issues.apache.org/jira/browse/CASSANDRA-1330) | AssertionError: discard at CommitLogContext(file=...) is not after last flush at  ... |  Major | . | Viliam Holub | Jonathan Ellis |
| [CASSANDRA-1547](https://issues.apache.org/jira/browse/CASSANDRA-1547) | lost+found directories cause problems for cassandra |  Minor | . | Matthew F. Dennis | Matthew F. Dennis |
| [CASSANDRA-1575](https://issues.apache.org/jira/browse/CASSANDRA-1575) | suggest avoiding broken openjdk6 on Debian as build-dep |  Minor | Packaging | Peter Schuller | Eric Evans |
| [CASSANDRA-1561](https://issues.apache.org/jira/browse/CASSANDRA-1561) | Disallow bootstrapping to a token that is already owned by a live node |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1442](https://issues.apache.org/jira/browse/CASSANDRA-1442) | Get Range Slices is broken |  Critical | . | Moleza Moleza | Stu Hood |


