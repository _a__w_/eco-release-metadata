
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.6.2 - 2010-05-28



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-894](https://issues.apache.org/jira/browse/CASSANDRA-894) | Disable Hinted Handoff Option |  Minor | . | Chris Goffinet | Ryan King |
| [CASSANDRA-1060](https://issues.apache.org/jira/browse/CASSANDRA-1060) | make concurrent\_reads, concurrent\_writes configurable at runtime via JMX |  Trivial | . | Jonathan Ellis | Roger Schildmeijer |
| [CASSANDRA-1090](https://issues.apache.org/jira/browse/CASSANDRA-1090) | make it possible to tell when repair has finished |  Major | . | Jonathan Ellis | Stu Hood |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-992](https://issues.apache.org/jira/browse/CASSANDRA-992) | fix contrib/word\_count build |  Major | . | Jonathan Ellis | Jeremy Hanna |
| [CASSANDRA-1039](https://issues.apache.org/jira/browse/CASSANDRA-1039) | Make read repair deterministic to enable unique ID creation function in Cassandra |  Minor | . | Roland Hänel | Roland Hänel |
| [CASSANDRA-1096](https://issues.apache.org/jira/browse/CASSANDRA-1096) | Sequential splits causing unbalanced MapReduce load |  Minor | . | Joost Ouwerkerk | Joost Ouwerkerk |
| [CASSANDRA-1100](https://issues.apache.org/jira/browse/CASSANDRA-1100) | better defaults for flush sorter + writer executor queue sizes |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1051](https://issues.apache.org/jira/browse/CASSANDRA-1051) | Create sstable2json.bat and json2sstable.bat for working on windows |  Minor | Tools | dopsun |  |
| [CASSANDRA-1114](https://issues.apache.org/jira/browse/CASSANDRA-1114) | start script occasionally causes flapping when used with runit |  Minor | . | Cliff Moon | Cliff Moon |
| [CASSANDRA-1113](https://issues.apache.org/jira/browse/CASSANDRA-1113) | Please include a windows batch file to execute the node tool |  Minor | Tools | Bill T. | Gary Dusbabek |
| [CASSANDRA-1053](https://issues.apache.org/jira/browse/CASSANDRA-1053) | Expose setting of phi in the FailureDetector |  Trivial | . | Brandon Williams | Brandon Williams |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-948](https://issues.apache.org/jira/browse/CASSANDRA-948) | Cannot Start Cassandra Under Windows |  Major | . | Nick Berardi | Gary Dusbabek |
| [CASSANDRA-1063](https://issues.apache.org/jira/browse/CASSANDRA-1063) | tombstone-only rows in sstables can be ignored |  Major | . | Stu Hood | Jonathan Ellis |
| [CASSANDRA-1056](https://issues.apache.org/jira/browse/CASSANDRA-1056) | size of row in spanned index entries does not include key bytes |  Trivial | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1076](https://issues.apache.org/jira/browse/CASSANDRA-1076) | StreamingService.StreamDestinations never empties |  Minor | . | Gary Dusbabek | Stu Hood |
| [CASSANDRA-1085](https://issues.apache.org/jira/browse/CASSANDRA-1085) | Minor SliceRange documentation fix |  Trivial | Documentation and Website | Manish Singh | Manish Singh |
| [CASSANDRA-1079](https://issues.apache.org/jira/browse/CASSANDRA-1079) | Cache capacity settings done via nodetool get reset on memtable flushes |  Major | . | Ryan King | Jonathan Ellis |
| [CASSANDRA-1081](https://issues.apache.org/jira/browse/CASSANDRA-1081) | Thrift sockets leak in 0.6 hadoop interface |  Minor | . | gabriele renzi | Johan Oskarsson |
| [CASSANDRA-1005](https://issues.apache.org/jira/browse/CASSANDRA-1005) | cassandra-cli doesn't work with system allowed column family names |  Minor | . | James Mello | Eric Evans |
| [CASSANDRA-1059](https://issues.apache.org/jira/browse/CASSANDRA-1059) | Exception when run "get Keyspace1.Standard1" command in the CLI |  Minor | Tools | ESSOUSSI Jamel | Eric Evans |
| [CASSANDRA-1049](https://issues.apache.org/jira/browse/CASSANDRA-1049) | SlicePredicate does not always round-trip correctly |  Major | . | Jonathan Ellis | Jonathan Ellis |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1064](https://issues.apache.org/jira/browse/CASSANDRA-1064) | Update readme with correct URLs |  Trivial | . | Jeremy Hanna | Jeremy Hanna |
| [CASSANDRA-1089](https://issues.apache.org/jira/browse/CASSANDRA-1089) | minor improvements to stress.py |  Minor | . | Jonathan Ellis | Brandon Williams |


