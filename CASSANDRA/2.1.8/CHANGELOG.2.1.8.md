
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1.8 - 2015-07-09



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9090](https://issues.apache.org/jira/browse/CASSANDRA-9090) | Allow JMX over SSL directly from nodetool |  Major | Tools | Philip Thompson |  |
| [CASSANDRA-7814](https://issues.apache.org/jira/browse/CASSANDRA-7814) | enable describe on indices |  Minor | Tools | radha | Stefania |
| [CASSANDRA-9603](https://issues.apache.org/jira/browse/CASSANDRA-9603) | Expose private listen\_address in system.local |  Major | Distributed Metadata | Piotr Kołaczkowski | Carl Yeksigian |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9567](https://issues.apache.org/jira/browse/CASSANDRA-9567) | Windows does not handle ipv6 addresses |  Major | . | Philip Thompson | Philip Thompson |
| [CASSANDRA-9637](https://issues.apache.org/jira/browse/CASSANDRA-9637) | CFS selectAndReference Blocks for Compaction |  Major | . | Russell Spitzer | Benedict |
| [CASSANDRA-9540](https://issues.apache.org/jira/browse/CASSANDRA-9540) | Cql IN query wrong on rows with values bigger than 64kb |  Major | CQL | Mathijs Vogelzang | Carl Yeksigian |
| [CASSANDRA-9559](https://issues.apache.org/jira/browse/CASSANDRA-9559) | IndexOutOfBoundsException inserting into TupleType |  Major | CQL | dan jatnieks | Benjamin Lerer |
| [CASSANDRA-9527](https://issues.apache.org/jira/browse/CASSANDRA-9527) | Cannot create secondary index on a table WITH COMPACT STORAGE |  Minor | CQL, Secondary Indexes | fuggy\_yama | Benjamin Lerer |
| [CASSANDRA-9385](https://issues.apache.org/jira/browse/CASSANDRA-9385) | cqlsh autocomplete does not work for DTCS min\_threshold |  Trivial | . | Sebastian Estevez | Alex Buck |
| [CASSANDRA-9631](https://issues.apache.org/jira/browse/CASSANDRA-9631) | Unnecessary required filtering for query on indexed clustering key |  Major | CQL | Kevin Deldycke | Benjamin Lerer |
| [CASSANDRA-9649](https://issues.apache.org/jira/browse/CASSANDRA-9649) | Paxos ballot in StorageProxy could clash |  Minor | Coordination | Stefania | Stefania |
| [CASSANDRA-9064](https://issues.apache.org/jira/browse/CASSANDRA-9064) | [LeveledCompactionStrategy] cqlsh can't run cql produced by its own describe table statement |  Major | CQL | Sujeet Gholap | Benjamin Lerer |
| [CASSANDRA-9560](https://issues.apache.org/jira/browse/CASSANDRA-9560) | Changing durable\_writes on a keyspace is only applied after restart of node |  Major | Distributed Metadata | Fred A | Carl Yeksigian |
| [CASSANDRA-9693](https://issues.apache.org/jira/browse/CASSANDRA-9693) | cqlsh DESCRIBE KEYSPACES reporting error 'None not found in keyspaces' after keyspaces |  Major | Tools | Mike Adamson | Mike Adamson |
| [CASSANDRA-9681](https://issues.apache.org/jira/browse/CASSANDRA-9681) | Memtable heap size grows and many long GC pauses are triggered |  Critical | . | mlowicki | Benedict |
| [CASSANDRA-9636](https://issues.apache.org/jira/browse/CASSANDRA-9636) | Duplicate columns in selection causes AssertionError |  Major | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-9700](https://issues.apache.org/jira/browse/CASSANDRA-9700) | StreamSession selects canonical sstables, but uses the non-canonical instance |  Major | . | Benedict | Benedict |
| [CASSANDRA-9656](https://issues.apache.org/jira/browse/CASSANDRA-9656) | Strong circular-reference leaks |  Critical | . | Benedict | Benedict |
| [CASSANDRA-9643](https://issues.apache.org/jira/browse/CASSANDRA-9643) | Warn when an extra-large partition is compacted |  Major | Compaction | Jonathan Ellis | Stefania |
| [CASSANDRA-9727](https://issues.apache.org/jira/browse/CASSANDRA-9727) | AuthSuccess NPE |  Major | . | Pierre N. |  |
| [CASSANDRA-10260](https://issues.apache.org/jira/browse/CASSANDRA-10260) | NPE in SSTableReader.invalidateCacheKey |  Major | Coordination, Local Write-Read Paths | Jeff Jirsa | Paulo Motta |


