
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.0 beta 1 - 2013-07-12



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4237](https://issues.apache.org/jira/browse/CASSANDRA-4237) | Add back 0.8-style memtable\_lifetime feature |  Minor | . | Jonathan Ellis | Yuki Morishita |
| [CASSANDRA-3974](https://issues.apache.org/jira/browse/CASSANDRA-3974) | Per-CF TTL |  Minor | . | Jonathan Ellis | Kirk True |
| [CASSANDRA-3648](https://issues.apache.org/jira/browse/CASSANDRA-3648) | Repair should validate checksums before streaming |  Minor | . | Jonathan Ellis | Vijay |
| [CASSANDRA-4554](https://issues.apache.org/jira/browse/CASSANDRA-4554) | Log when a node is down longer than the hint window and we stop saving hints |  Minor | . | Jonathan Ellis | Vijay |
| [CASSANDRA-5045](https://issues.apache.org/jira/browse/CASSANDRA-5045) | Add Configuration Loader to DatabaseDescriptor |  Minor | . | Michael Guymon | Sylvain Lebresne |
| [CASSANDRA-5339](https://issues.apache.org/jira/browse/CASSANDRA-5339) | YAML network topology snitch supporting preferred addresses |  Minor | . | Eric Dong | Eric Dong |
| [CASSANDRA-1311](https://issues.apache.org/jira/browse/CASSANDRA-1311) | Triggers |  Major | . | Maxim Grinev | Vijay |
| [CASSANDRA-5514](https://issues.apache.org/jira/browse/CASSANDRA-5514) | Track min/max clustered values per sstable to optimize reads |  Major | CQL | Jonathan Ellis | Marcus Eriksson |
| [CASSANDRA-5062](https://issues.apache.org/jira/browse/CASSANDRA-5062) | Support CAS |  Major | CQL | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5636](https://issues.apache.org/jira/browse/CASSANDRA-5636) | Add row count to select output a la psql |  Minor | Tools | Jonathan Ellis | Aleksey Yeschenko |
| [CASSANDRA-2737](https://issues.apache.org/jira/browse/CASSANDRA-2737) | CQL: support IF EXISTS extension for DROP commands (table, keyspace, index) |  Trivial | . | Cathy Daw | Michał Michalski |
| [CASSANDRA-4415](https://issues.apache.org/jira/browse/CASSANDRA-4415) | Add cursor API/auto paging to the native CQL protocol |  Major | . | Sylvain Lebresne | Sylvain Lebresne |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3997](https://issues.apache.org/jira/browse/CASSANDRA-3997) | Make SerializingCache Memory Pluggable |  Minor | . | Vijay | Vijay |
| [CASSANDRA-3961](https://issues.apache.org/jira/browse/CASSANDRA-3961) | Make index\_interval configurable per column family |  Major | . | Radim Kolar | Radim Kolar |
| [CASSANDRA-3237](https://issues.apache.org/jira/browse/CASSANDRA-3237) | refactor super column implmentation to use composite column names instead |  Minor | . | Matthew F. Dennis | Sylvain Lebresne |
| [CASSANDRA-4705](https://issues.apache.org/jira/browse/CASSANDRA-4705) | Speculative execution for reads / eager retries |  Major | . | Vijay | Vijay |
| [CASSANDRA-5243](https://issues.apache.org/jira/browse/CASSANDRA-5243) | cassandra-stress aggregate statistics |  Minor | Tools | Ryan McGuire | Ryan McGuire |
| [CASSANDRA-3983](https://issues.apache.org/jira/browse/CASSANDRA-3983) | Change order of directory searching for c\*.in.sh |  Minor | Packaging | Nick Bailey | Benjamin Coverston |
| [CASSANDRA-5271](https://issues.apache.org/jira/browse/CASSANDRA-5271) | Create tool to drop sstables to level 0 |  Trivial | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-5307](https://issues.apache.org/jira/browse/CASSANDRA-5307) | Update Message IDs to be ints instead of strings |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3534](https://issues.apache.org/jira/browse/CASSANDRA-3534) | Remove memory emergency pressure valve |  Minor | . | Vijay | Aleksey Yeschenko |
| [CASSANDRA-4872](https://issues.apache.org/jira/browse/CASSANDRA-4872) | Move manifest into sstable metadata |  Minor | . | Jonathan Ellis | Marcus Eriksson |
| [CASSANDRA-5250](https://issues.apache.org/jira/browse/CASSANDRA-5250) | Improve LeveledScanner work estimation |  Major | . | Jonathan Ellis | Marcus Eriksson |
| [CASSANDRA-4937](https://issues.apache.org/jira/browse/CASSANDRA-4937) | CRAR improvements (object cache + CompressionMetadata chunk offset storage moved off-heap). |  Major | . | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-3430](https://issues.apache.org/jira/browse/CASSANDRA-3430) | Break Big Compaction Lock apart |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5317](https://issues.apache.org/jira/browse/CASSANDRA-5317) | upgrade to recent snakeyaml |  Minor | . | Chris Burroughs | Jonathan Ellis |
| [CASSANDRA-4917](https://issues.apache.org/jira/browse/CASSANDRA-4917) | Optimize tombstone creation for ExpiringColumns |  Major | . | Christian Spriegel | Christian Spriegel |
| [CASSANDRA-5015](https://issues.apache.org/jira/browse/CASSANDRA-5015) | move bloom\_filter\_fp\_chance to compaction options |  Minor | . | Jonathan Ellis | Aleksey Yeschenko |
| [CASSANDRA-5389](https://issues.apache.org/jira/browse/CASSANDRA-5389) | Deserialize to arrays instead of trees |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-4885](https://issues.apache.org/jira/browse/CASSANDRA-4885) | Remove or rework per-row bloom filters |  Major | . | Jonathan Ellis | Jason Brown |
| [CASSANDRA-4932](https://issues.apache.org/jira/browse/CASSANDRA-4932) | Agree on a gcbefore/expirebefore value for all replica during validation compaction |  Minor | . | Sylvain Lebresne | Marcus Eriksson |
| [CASSANDRA-3533](https://issues.apache.org/jira/browse/CASSANDRA-3533) | TimeoutException when there is a firewall issue. |  Minor | . | Vijay | Vijay |
| [CASSANDRA-5459](https://issues.apache.org/jira/browse/CASSANDRA-5459) | Remove node from seeds list when it permanently leaves the cluster |  Minor | . | Jason Brown | Jason Brown |
| [CASSANDRA-5495](https://issues.apache.org/jira/browse/CASSANDRA-5495) | Do not hard code current version into BootstrapTest |  Trivial | Testing | Jason Brown | Jason Brown |
| [CASSANDRA-5306](https://issues.apache.org/jira/browse/CASSANDRA-5306) | Improve Dsnitch Severity |  Minor | . | Vijay | Vijay |
| [CASSANDRA-4180](https://issues.apache.org/jira/browse/CASSANDRA-4180) | Single-pass compaction for LCR |  Major | . | Sylvain Lebresne | Jonathan Ellis |
| [CASSANDRA-5075](https://issues.apache.org/jira/browse/CASSANDRA-5075) | CQL Add support for AS keyword in select |  Minor | CQL | Alexandr Kulik | Aleksey Yeschenko |
| [CASSANDRA-5557](https://issues.apache.org/jira/browse/CASSANDRA-5557) | Correctly handle interruptions of Thread.sleep() |  Minor | . | Mikhail Mazurskiy | Mikhail Mazurskiy |
| [CASSANDRA-5541](https://issues.apache.org/jira/browse/CASSANDRA-5541) | Reduce Empty Map allocations in RowMutation (modifications) |  Trivial | . | Dave Brosius | Jonathan Ellis |
| [CASSANDRA-5579](https://issues.apache.org/jira/browse/CASSANDRA-5579) | Use range tombstones when dropping cfs/columns from schema |  Trivial | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5581](https://issues.apache.org/jira/browse/CASSANDRA-5581) | Use System.nanoTime() to measure intervals |  Minor | . | Mikhail Mazurskiy | Mikhail Mazurskiy |
| [CASSANDRA-4693](https://issues.apache.org/jira/browse/CASSANDRA-4693) | CQL Protocol should allow multiple PreparedStatements to be atomically executed |  Major | . | Michaël Figuière | Sylvain Lebresne |
| [CASSANDRA-4450](https://issues.apache.org/jira/browse/CASSANDRA-4450) | CQL3: Allow preparing the consistency level, timestamp and ttl |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5595](https://issues.apache.org/jira/browse/CASSANDRA-5595) | Add counters to TRACE for how much CASSANDRA-5514 helped a query |  Minor | . | Jeremiah Jordan | Marcus Eriksson |
| [CASSANDRA-3734](https://issues.apache.org/jira/browse/CASSANDRA-3734) | Support native link w/o JNA in Java7 |  Minor | . | Jonathan Ellis | Jason Brown |
| [CASSANDRA-5004](https://issues.apache.org/jira/browse/CASSANDRA-5004) | Add a rate limit option to stress |  Minor | Tools | Brandon Williams | Jason Brown |
| [CASSANDRA-5538](https://issues.apache.org/jira/browse/CASSANDRA-5538) | Reduce Empty Map allocations in StorageProxy.sendToHintedEndpoints |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-5545](https://issues.apache.org/jira/browse/CASSANDRA-5545) | Add SASL authentication to CQL native protocol |  Major | . | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-4097](https://issues.apache.org/jira/browse/CASSANDRA-4097) | Classes in org.apache.cassandra.deps:avro:1.4.0-cassandra-1 clash with core Avro classes |  Minor | . | Andrew Swan | Jonathan Ellis |
| [CASSANDRA-5606](https://issues.apache.org/jira/browse/CASSANDRA-5606) | remove some deprecations in use of SnakeYaml |  Trivial | Configuration | Dave Brosius | Dave Brosius |
| [CASSANDRA-5582](https://issues.apache.org/jira/browse/CASSANDRA-5582) | Replace CustomHsHaServer with better optimized solution based on LMAX Disruptor |  Major | CQL | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-5650](https://issues.apache.org/jira/browse/CASSANDRA-5650) | Make index\_interval visible to CQL3 |  Minor | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-5678](https://issues.apache.org/jira/browse/CASSANDRA-5678) | Avoid over reconnecting in EC2MRS |  Minor | . | Jason Brown | Jason Brown |
| [CASSANDRA-5286](https://issues.apache.org/jira/browse/CASSANDRA-5286) | Streaming 2.0 |  Major | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-5625](https://issues.apache.org/jira/browse/CASSANDRA-5625) | Cleanup Tracing class |  Trivial | . | Mikhail Mazurskiy | Mikhail Mazurskiy |
| [CASSANDRA-5426](https://issues.apache.org/jira/browse/CASSANDRA-5426) | Redesign repair messages |  Minor | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-5613](https://issues.apache.org/jira/browse/CASSANDRA-5613) | Rename 'Table' class to 'Keyspace' in 2.0 |  Minor | . | Aleksey Yeschenko | Jeremiah Jordan |
| [CASSANDRA-5014](https://issues.apache.org/jira/browse/CASSANDRA-5014) | Convert timeout from milli second to micro second. |  Trivial | . | Vijay | Vijay |
| [CASSANDRA-5681](https://issues.apache.org/jira/browse/CASSANDRA-5681) | Refactor IESCS in Snitches |  Minor | . | Jason Brown | Jason Brown |
| [CASSANDRA-5649](https://issues.apache.org/jira/browse/CASSANDRA-5649) | Move resultset type information into prepare, not execute |  Major | . | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-5714](https://issues.apache.org/jira/browse/CASSANDRA-5714) | Allow coordinator failover for cursors |  Minor | . | Michaël Figuière | Sylvain Lebresne |
| [CASSANDRA-5667](https://issues.apache.org/jira/browse/CASSANDRA-5667) | Change timestamps used in CAS ballot proposals to be more resilient to clock skew |  Minor | . | Nick Puz | Jonathan Ellis |
| [CASSANDRA-5619](https://issues.apache.org/jira/browse/CASSANDRA-5619) | CAS UPDATE for a lost race: save round trip by returning column values |  Major | . | Blair Zajac | Sylvain Lebresne |
| [CASSANDRA-5719](https://issues.apache.org/jira/browse/CASSANDRA-5719) | Expire entries out of ThriftSessionManager |  Minor | . | Jason Brown | Jason Brown |
| [CASSANDRA-4495](https://issues.apache.org/jira/browse/CASSANDRA-4495) | Don't tie client side use of AbstractType to JDBC |  Minor | . | Sylvain Lebresne | Carl Yeksigian |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4995](https://issues.apache.org/jira/browse/CASSANDRA-4995) | CompactionSerializerTest fails to find jemalloc |  Minor | CQL | Brandon Williams | Vijay |
| [CASSANDRA-3818](https://issues.apache.org/jira/browse/CASSANDRA-3818) | disabling m-a-t for fun and profit (and other ant stuff) |  Minor | Packaging | Eric Evans | Dave Brosius |
| [CASSANDRA-5162](https://issues.apache.org/jira/browse/CASSANDRA-5162) | Remove SimpleTransportFactory |  Minor | . | Jason Brown | Jonathan Ellis |
| [CASSANDRA-5123](https://issues.apache.org/jira/browse/CASSANDRA-5123) | Multiget Supercolumns Sometimes Missing Results |  Minor | . | Tyler Hobbs | Sylvain Lebresne |
| [CASSANDRA-5199](https://issues.apache.org/jira/browse/CASSANDRA-5199) | Avoid serializing to byte[] on commitlog append |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5139](https://issues.apache.org/jira/browse/CASSANDRA-5139) | Drop keyspace argument from forceUserDefinedCompactions |  Minor | . | Jonathan Ellis | Yuki Morishita |
| [CASSANDRA-5228](https://issues.apache.org/jira/browse/CASSANDRA-5228) | Track maximum ttl and use to expire entire sstables |  Minor | . | Jonathan Ellis | Marcus Eriksson |
| [CASSANDRA-5403](https://issues.apache.org/jira/browse/CASSANDRA-5403) | Clean up ColumnFamily, ISortedColumns heirarchy |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5385](https://issues.apache.org/jira/browse/CASSANDRA-5385) | IndexHelper.skipBloomFilters won't skip non-SHA filters |  Major | . | Carl Yeksigian | Carl Yeksigian |
| [CASSANDRA-5125](https://issues.apache.org/jira/browse/CASSANDRA-5125) | Support indexes on composite column components (clustered columns) |  Major | . | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-5371](https://issues.apache.org/jira/browse/CASSANDRA-5371) | Perform size-tiered compactions in L0 ("hybrid compaction") |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5074](https://issues.apache.org/jira/browse/CASSANDRA-5074) | Add an official way to disable compaction |  Minor | . | Jonathan Ellis | Marcus Eriksson |
| [CASSANDRA-5440](https://issues.apache.org/jira/browse/CASSANDRA-5440) | ISE during short reads |  Major | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-5455](https://issues.apache.org/jira/browse/CASSANDRA-5455) | Remove PBSPredictor |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5458](https://issues.apache.org/jira/browse/CASSANDRA-5458) | Avoid serializing keyspace redundantly in RowMutation |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5465](https://issues.apache.org/jira/browse/CASSANDRA-5465) | Shuffle fails to write to system.range\_xfers |  Minor | . | Dave Brosius |  |
| [CASSANDRA-5393](https://issues.apache.org/jira/browse/CASSANDRA-5393) | Add retry mechanism to OTC for non-droppable\_verbs |  Major | . | Jeremiah Jordan | Jason Brown |
| [CASSANDRA-5439](https://issues.apache.org/jira/browse/CASSANDRA-5439) | allow STCS options to apply to the L0 compaction performed by LCS |  Minor | . | Jonathan Ellis | Carl Yeksigian |
| [CASSANDRA-5511](https://issues.apache.org/jira/browse/CASSANDRA-5511) | Clean up backwards compatibility complexity for 2.0 |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5532](https://issues.apache.org/jira/browse/CASSANDRA-5532) | Maven package installation broken by recent build changes |  Minor | Tools | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-5521](https://issues.apache.org/jira/browse/CASSANDRA-5521) | move IndexSummary off heap |  Major | . | Jonathan Ellis | Vijay |
| [CASSANDRA-5150](https://issues.apache.org/jira/browse/CASSANDRA-5150) | sstable2json doesn't check SIGPIPE |  Minor | Tools | Will Oberman | Pawel Mirski |
| [CASSANDRA-5577](https://issues.apache.org/jira/browse/CASSANDRA-5577) | Avoid unnecessary second pass on name-based queries |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5580](https://issues.apache.org/jira/browse/CASSANDRA-5580) | InputStreams not closed |  Minor | Configuration | Mikhail Mazurskiy | Mikhail Mazurskiy |
| [CASSANDRA-5603](https://issues.apache.org/jira/browse/CASSANDRA-5603) | YamlFileNetworkTopologySnitch doesn't call reconnectViaPreferredAddress in onChange |  Minor | . | Jeremiah Jordan | Brandon Williams |
| [CASSANDRA-5627](https://issues.apache.org/jira/browse/CASSANDRA-5627) | BlacklistingCompactionTest missing Apache license |  Trivial | . | Carl Yeksigian | Carl Yeksigian |
| [CASSANDRA-5576](https://issues.apache.org/jira/browse/CASSANDRA-5576) | CREATE/DROP TRIGGER in CQL |  Major | CQL | Jonathan Ellis | Vijay |
| [CASSANDRA-5149](https://issues.apache.org/jira/browse/CASSANDRA-5149) | Respect slice count even if column expire mid-request |  Major | . | Sylvain Lebresne | Aleksey Yeschenko |
| [CASSANDRA-5660](https://issues.apache.org/jira/browse/CASSANDRA-5660) | Gossiper incorrectly drops AppState for an upgrading node |  Minor | . | Jason Brown | Jason Brown |
| [CASSANDRA-5669](https://issues.apache.org/jira/browse/CASSANDRA-5669) | Connection thrashing in multi-region ec2 during upgrade, due to messaging version |  Minor | . | Jason Brown | Jason Brown |
| [CASSANDRA-5688](https://issues.apache.org/jira/browse/CASSANDRA-5688) | Update debian packaging for 2.0 |  Major | Packaging | Blair Zajac | Blair Zajac |
| [CASSANDRA-5705](https://issues.apache.org/jira/browse/CASSANDRA-5705) | Disallow renaming an indexed column |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5675](https://issues.apache.org/jira/browse/CASSANDRA-5675) | cqlsh shouldn't display "null" for empty values |  Minor | . | Sylvain Lebresne | Aleksey Yeschenko |
| [CASSANDRA-5707](https://issues.apache.org/jira/browse/CASSANDRA-5707) | Compression chunk\_length shouldn't be mandatory |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5692](https://issues.apache.org/jira/browse/CASSANDRA-5692) | Race condition in detecting version on a mixed 1.1/1.2 cluster |  Minor | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-5454](https://issues.apache.org/jira/browse/CASSANDRA-5454) | Changing column\_index\_size\_in\_kb on different nodes might corrupt files |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5151](https://issues.apache.org/jira/browse/CASSANDRA-5151) | Implement better way of eliminating compaction left overs. |  Major | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-5699](https://issues.apache.org/jira/browse/CASSANDRA-5699) | Streaming (2.0) can deadlock |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5702](https://issues.apache.org/jira/browse/CASSANDRA-5702) | ALTER RENAME is broken in trunk |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5723](https://issues.apache.org/jira/browse/CASSANDRA-5723) | DateType (timestamp type in CQL3) does not sort pre-'unix epoch' dates correctly |  Major | . | zhouhero | Sylvain Lebresne |
| [CASSANDRA-5429](https://issues.apache.org/jira/browse/CASSANDRA-5429) | Update scrub and scrubtest for single-pass compaction format |  Major | . | Jonathan Ellis | Jason Brown |
| [CASSANDRA-5731](https://issues.apache.org/jira/browse/CASSANDRA-5731) | Should use index\_interval from already loaded index summary |  Trivial | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-5697](https://issues.apache.org/jira/browse/CASSANDRA-5697) | cqlsh doesn't allow semicolons in BATCH statements |  Minor | Tools | Russell Spitzer | Aleksey Yeschenko |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3919](https://issues.apache.org/jira/browse/CASSANDRA-3919) | Dropping a column should do more than just remove the definition |  Major | . | Jonathan Ellis | Aleksey Yeschenko |
| [CASSANDRA-5441](https://issues.apache.org/jira/browse/CASSANDRA-5441) | Add support for read at CL.SERIAL |  Major | CQL | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5443](https://issues.apache.org/jira/browse/CASSANDRA-5443) | Add CAS CQL support |  Major | CQL | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-5442](https://issues.apache.org/jira/browse/CASSANDRA-5442) | Add support for specifying CAS commit CL |  Major | CQL | Jonathan Ellis | Jonathan Ellis |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3719](https://issues.apache.org/jira/browse/CASSANDRA-3719) | Upgrade thrift to latest release version (0.9.x) |  Minor | . | Jake Farrell | Jake Farrell |
| [CASSANDRA-5331](https://issues.apache.org/jira/browse/CASSANDRA-5331) | Remove ASSUME from cqlsh |  Trivial | Tools | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5277](https://issues.apache.org/jira/browse/CASSANDRA-5277) | rename AntiEntropyService |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5518](https://issues.apache.org/jira/browse/CASSANDRA-5518) | Clean out token range bisection on bootstrap |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5293](https://issues.apache.org/jira/browse/CASSANDRA-5293) | formalize that timestamps are epoch-in-micros in 2.0 |  Major | . | Jonathan Ellis |  |
| [CASSANDRA-5349](https://issues.apache.org/jira/browse/CASSANDRA-5349) | Add binary protocol support for bind variables to non-prepared statements |  Minor | CQL | Jonathan Ellis | Marcus Eriksson |
| [CASSANDRA-5585](https://issues.apache.org/jira/browse/CASSANDRA-5585) | Drop CQL2/CQL3-beta support from cqlsh |  Minor | Tools | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5348](https://issues.apache.org/jira/browse/CASSANDRA-5348) | Remove on-heap row cache |  Major | . | Jonathan Ellis | Jonathan Ellis |


