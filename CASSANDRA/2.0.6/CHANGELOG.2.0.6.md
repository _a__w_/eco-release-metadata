
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.0.6 - 2014-03-10



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-4445](https://issues.apache.org/jira/browse/CASSANDRA-4445) | balance utility for vnodes |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-6440](https://issues.apache.org/jira/browse/CASSANDRA-6440) | Repair should allow repairing particular endpoints to reduce WAN usage. |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-6561](https://issues.apache.org/jira/browse/CASSANDRA-6561) | Static columns in CQL3 |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6703](https://issues.apache.org/jira/browse/CASSANDRA-6703) | cqlsh support for "static" CQL3 columns |  Major | . | Mikhail Stepura | Mikhail Stepura |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6655](https://issues.apache.org/jira/browse/CASSANDRA-6655) | Writing mostly deletes to a Memtable results in undercounting the table's occupancy so it may not flush |  Minor | . | Benedict | Benedict |
| [CASSANDRA-4757](https://issues.apache.org/jira/browse/CASSANDRA-4757) | Expose bulk loading progress/status over jmx |  Minor | . | Nick Bailey | Tyler Hobbs |
| [CASSANDRA-6360](https://issues.apache.org/jira/browse/CASSANDRA-6360) | Make nodetool cfhistograms output easily understandable |  Trivial | Tools | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-6652](https://issues.apache.org/jira/browse/CASSANDRA-6652) | Stop CommitLogSegment.close() from unnecessarily calling sync() prior to cleaning the buffer |  Minor | . | Benedict | Benedict |
| [CASSANDRA-6364](https://issues.apache.org/jira/browse/CASSANDRA-6364) | There should be different disk\_failure\_policies for data and commit volumes or commit volume failure should always cause node exit |  Major | . | J. Ryan Earl | Benedict |
| [CASSANDRA-4851](https://issues.apache.org/jira/browse/CASSANDRA-4851) | CQL3: improve support for paginating over composites |  Minor | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6157](https://issues.apache.org/jira/browse/CASSANDRA-6157) | Selectively Disable hinted handoff for a data center |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-6737](https://issues.apache.org/jira/browse/CASSANDRA-6737) | A batch statements on a single partition should not create a new CF object for each update |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6775](https://issues.apache.org/jira/browse/CASSANDRA-6775) | Tools installed from the Debian package emit errors about missing javaagent. |  Minor | Packaging, Tools | Federico Piccinini | Federico Piccinini |
| [CASSANDRA-6665](https://issues.apache.org/jira/browse/CASSANDRA-6665) | Batching in CqlRecordWriter |  Minor | . | Christian Rolf | Christian Rolf |
| [CASSANDRA-6566](https://issues.apache.org/jira/browse/CASSANDRA-6566) | Differencer should not run in AntiEntropy Stage |  Minor | . | sankalp kohli | Yuki Morishita |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6623](https://issues.apache.org/jira/browse/CASSANDRA-6623) | Null in a cell caused by expired TTL does not work with IF clause (in CQL3) |  Minor | Testing | Csaba Seres | Sylvain Lebresne |
| [CASSANDRA-6522](https://issues.apache.org/jira/browse/CASSANDRA-6522) | DroppableTombstoneRatio JMX value is 0.0 for all CFs |  Minor | . | Daniel Kador | Marcus Eriksson |
| [CASSANDRA-6645](https://issues.apache.org/jira/browse/CASSANDRA-6645) | upgradesstables causes NPE for secondary indexes without an underlying column family |  Major | Secondary Indexes | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-6658](https://issues.apache.org/jira/browse/CASSANDRA-6658) | Nodes flap once at startup |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-6678](https://issues.apache.org/jira/browse/CASSANDRA-6678) | Unwanted schema pull while upgrading nodes from 1.2 to 2.0 |  Major | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-6667](https://issues.apache.org/jira/browse/CASSANDRA-6667) | Mean cells per sstable is calculated incorrectly |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-6682](https://issues.apache.org/jira/browse/CASSANDRA-6682) | Loss of precision under concurrent access on o.a.c.utils.EstimatedHistogram methods |  Minor | . | Paulo Gaspar | Paulo Gaspar |
| [CASSANDRA-6687](https://issues.apache.org/jira/browse/CASSANDRA-6687) | CQL: "drop table if exists" throws exception when table does not exist |  Minor | . | Brent Haines | Mikhail Stepura |
| [CASSANDRA-6649](https://issues.apache.org/jira/browse/CASSANDRA-6649) | CQL: disallow counter update with "USING TIMESTAMP" and "USING TTL" |  Trivial | . | Sylvain Lebresne | Aleksey Yeschenko |
| [CASSANDRA-6622](https://issues.apache.org/jira/browse/CASSANDRA-6622) | Streaming session failures during node replace of same address |  Major | . | Ravi Prasad | Brandon Williams |
| [CASSANDRA-6695](https://issues.apache.org/jira/browse/CASSANDRA-6695) | Don't exchange schema between nodes with different versions (no pull, no push) |  Major | . | Piotr Kołaczkowski | Aleksey Yeschenko |
| [CASSANDRA-6688](https://issues.apache.org/jira/browse/CASSANDRA-6688) | Avoid possible sstable overlaps with leveled compaction |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-6700](https://issues.apache.org/jira/browse/CASSANDRA-6700) | Use real node messaging versions for schema exchange decisions |  Major | . | Piotr Kołaczkowski | Aleksey Yeschenko |
| [CASSANDRA-6701](https://issues.apache.org/jira/browse/CASSANDRA-6701) | IN on the last clustering columns + ORDER BY DESC yield no results |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6711](https://issues.apache.org/jira/browse/CASSANDRA-6711) | SecondaryIndexManager#deleteFromIndexes() doesn't correctly retrieve column indexes |  Major | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-6707](https://issues.apache.org/jira/browse/CASSANDRA-6707) | AIOOBE when doing select count(\*) from on a mixed cluster. |  Critical | . | Piotr Kołaczkowski | Tyler Hobbs |
| [CASSANDRA-6713](https://issues.apache.org/jira/browse/CASSANDRA-6713) | Snapshot based repair does not send snapshot command to itself |  Major | . | sankalp kohli | Yuki Morishita |
| [CASSANDRA-6714](https://issues.apache.org/jira/browse/CASSANDRA-6714) | Fix replaying old (1.2) commitlog in Cassandra 2.0 |  Major | . | Piotr Kołaczkowski | Aleksey Yeschenko |
| [CASSANDRA-6734](https://issues.apache.org/jira/browse/CASSANDRA-6734) | Use correct partitioner in AbstractViewSSTableFinder |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5631](https://issues.apache.org/jira/browse/CASSANDRA-5631) | NPE when creating column family shortly after multinode startup |  Major | . | Martin Serrano | Aleksey Yeschenko |
| [CASSANDRA-6735](https://issues.apache.org/jira/browse/CASSANDRA-6735) | Exceptions during memtable flushes on shutdown hook prevent process shutdown |  Minor | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-6743](https://issues.apache.org/jira/browse/CASSANDRA-6743) | Dead code: net/HeaderTypes.java |  Trivial | . | Daniel Shelepov | Daniel Shelepov |
| [CASSANDRA-6722](https://issues.apache.org/jira/browse/CASSANDRA-6722) | cross-partition ordering should have warning or be disallowed when paging |  Minor | . | Russ Hatch | Sylvain Lebresne |
| [CASSANDRA-6577](https://issues.apache.org/jira/browse/CASSANDRA-6577) | ConcurrentModificationException during nodetool netstats |  Minor | Tools | Shao-Chuan Wang | Joshua McKenzie |
| [CASSANDRA-6748](https://issues.apache.org/jira/browse/CASSANDRA-6748) | If null is explicitly set to a column, paging\_state will not work |  Major | . | Katsutoshi Nagaoka | Sylvain Lebresne |
| [CASSANDRA-6683](https://issues.apache.org/jira/browse/CASSANDRA-6683) | BADNESS\_THRESHOLD does not working correctly with DynamicEndpointSnitch |  Major | . | Kirill Bogdanov | Tyler Hobbs |
| [CASSANDRA-6784](https://issues.apache.org/jira/browse/CASSANDRA-6784) | Compaction timestamp captured incorrectly in Compaction History table |  Trivial | . | Chander Pechetty | Chander Pechetty |
| [CASSANDRA-6769](https://issues.apache.org/jira/browse/CASSANDRA-6769) | Static columns break IN clauses |  Major | . | Tupshin Harper | Sylvain Lebresne |
| [CASSANDRA-6636](https://issues.apache.org/jira/browse/CASSANDRA-6636) | sstableloader fails when attempting to load data from a single node into a multi-node cluster |  Minor | . | Russ Hatch | Yuki Morishita |
| [CASSANDRA-6791](https://issues.apache.org/jira/browse/CASSANDRA-6791) | CompressedSequentialWriter can write zero-length segments during scrub |  Minor | . | Jonathan Ellis | Viktor Kuzmin |
| [CASSANDRA-6782](https://issues.apache.org/jira/browse/CASSANDRA-6782) | setting TTL on some columns seems to expire whole row |  Major | . | Russ Hatch | Sylvain Lebresne |
| [CASSANDRA-6733](https://issues.apache.org/jira/browse/CASSANDRA-6733) | Upgrade of 1.2.11 to 2.0.5 make IllegalArgumentException in Buffer.limit on read of a super column family |  Major | . | Nicolas Lalevée | Sylvain Lebresne |
| [CASSANDRA-6797](https://issues.apache.org/jira/browse/CASSANDRA-6797) | compaction and scrub data directories race on startup |  Minor | Compaction, Lifecycle | Matt Byrd | Joshua McKenzie |
| [CASSANDRA-5201](https://issues.apache.org/jira/browse/CASSANDRA-5201) | Cassandra/Hadoop does not support current Hadoop releases |  Major | . | Brian Jeltema | Benjamin Coverston |
| [CASSANDRA-6778](https://issues.apache.org/jira/browse/CASSANDRA-6778) | FBUtilities.singleton() should use the CF comparator |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6526](https://issues.apache.org/jira/browse/CASSANDRA-6526) | CQLSSTableWriter addRow(Map\<String, Object\> values) does not work as documented. |  Major | . | Yariv Amar | Sylvain Lebresne |
| [CASSANDRA-6813](https://issues.apache.org/jira/browse/CASSANDRA-6813) | server side ClassCastException using compact storage |  Major | . | Paul Kendall | Sylvain Lebresne |
| [CASSANDRA-6541](https://issues.apache.org/jira/browse/CASSANDRA-6541) | New versions of Hotspot create new Class objects on every JMX connection causing the heap to fill up with them if CMSClassUnloadingEnabled isn't set. |  Minor | Configuration | jonathan lacefield | Brandon Williams |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6657](https://issues.apache.org/jira/browse/CASSANDRA-6657) | Log the newsize value alongside the heap size at startup |  Trivial | . | Jeremy Hanna | Brandon Williams |


