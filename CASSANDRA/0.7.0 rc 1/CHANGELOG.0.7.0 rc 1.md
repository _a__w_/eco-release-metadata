
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.7.0 rc 1 - 2010-11-24



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1696](https://issues.apache.org/jira/browse/CASSANDRA-1696) | add --start-key to stress.py |  Major | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1687](https://issues.apache.org/jira/browse/CASSANDRA-1687) | Add UUID functions to cli |  Minor | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1698](https://issues.apache.org/jira/browse/CASSANDRA-1698) | nodetool cfhistograms |  Major | Tools | Jonathan Ellis | Pavel Yaskevich |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1677](https://issues.apache.org/jira/browse/CASSANDRA-1677) | Log type of dropped messages |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1660](https://issues.apache.org/jira/browse/CASSANDRA-1660) | Log GC/tpstats info when messages are dropped. |  Minor | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-1685](https://issues.apache.org/jira/browse/CASSANDRA-1685) | disambiguate READ\_RESPONSE |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1668](https://issues.apache.org/jira/browse/CASSANDRA-1668) | Cli grammar refactoring and better error messages. |  Minor | . | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-1690](https://issues.apache.org/jira/browse/CASSANDRA-1690) | add jna dependency to debian, red hat packages |  Minor | Packaging | Jonathan Ellis | Nick Bailey |
| [CASSANDRA-1702](https://issues.apache.org/jira/browse/CASSANDRA-1702) | handle skipping bad rows in LazilyCompacted path |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1693](https://issues.apache.org/jira/browse/CASSANDRA-1693) | add cli support for assuming CF comparator, validator |  Minor | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1719](https://issues.apache.org/jira/browse/CASSANDRA-1719) | improve read repair |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1758](https://issues.apache.org/jira/browse/CASSANDRA-1758) | clean up token/ring code on first startup |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1742](https://issues.apache.org/jira/browse/CASSANDRA-1742) | cli usability |  Minor | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1553](https://issues.apache.org/jira/browse/CASSANDRA-1553) | warn of imbalanced ranges in nodetool ring |  Minor | Tools | Jonathan Ellis | Jon Hermes |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1645](https://issues.apache.org/jira/browse/CASSANDRA-1645) | Bootstrapping new node causes RowMutationVerbHandler Couldn't find cfId |  Major | . | JCF | Gary Dusbabek |
| [CASSANDRA-1679](https://issues.apache.org/jira/browse/CASSANDRA-1679) | ByteBuffer bug in ExpiringColumn.updateDigest() |  Major | . | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-1681](https://issues.apache.org/jira/browse/CASSANDRA-1681) | IntegerType.toString() handles ByteBuffer incorrectly |  Major | . | Jim Ancona | Jim Ancona |
| [CASSANDRA-628](https://issues.apache.org/jira/browse/CASSANDRA-628) | java.net.SocketException: Invalid argument / java.net.NoRouteToHostException: Network is unreachable |  Major | . | Eric Evans | Eric Evans |
| [CASSANDRA-1686](https://issues.apache.org/jira/browse/CASSANDRA-1686) | o.a.c.dht.AbstractBounds missing serialVersionUID |  Minor | Tools | Eric Evans | Eric Evans |
| [CASSANDRA-1676](https://issues.apache.org/jira/browse/CASSANDRA-1676) | Avoid dropping messages off the client request path |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1691](https://issues.apache.org/jira/browse/CASSANDRA-1691) | Repair blocking forever when RF=1 |  Major | Tools | Mike Bulman | Stu Hood |
| [CASSANDRA-1692](https://issues.apache.org/jira/browse/CASSANDRA-1692) | gen-thrift-py can fail but claims success |  Minor | Tools | Matthew F. Dennis | Matthew F. Dennis |
| [CASSANDRA-1678](https://issues.apache.org/jira/browse/CASSANDRA-1678) | describe\_schema\_versions does not list downed hosts |  Minor | CQL | Edward Capriolo | Gary Dusbabek |
| [CASSANDRA-1697](https://issues.apache.org/jira/browse/CASSANDRA-1697) | Startup can fail if DNS lookup fails for seed node |  Trivial | . | Edward Capriolo | Jonathan Ellis |
| [CASSANDRA-1694](https://issues.apache.org/jira/browse/CASSANDRA-1694) | fix jna errno reporting |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1670](https://issues.apache.org/jira/browse/CASSANDRA-1670) | cannot move a node |  Major | . | Matthew F. Dennis | Gary Dusbabek |
| [CASSANDRA-1712](https://issues.apache.org/jira/browse/CASSANDRA-1712) | Creating a SuperColumnFamily other than BytesType results in incorrect comparator types |  Trivial | Tools | Dhaivat Pandit | Jonathan Ellis |
| [CASSANDRA-1713](https://issues.apache.org/jira/browse/CASSANDRA-1713) | Windows batch files use incorrect paths |  Minor | Packaging | Vladimir Loncar | Vladimir Loncar |
| [CASSANDRA-1722](https://issues.apache.org/jira/browse/CASSANDRA-1722) | Unbounded key range only ever scans first node in ring |  Major | . | Jonathan Ellis | Stu Hood |
| [CASSANDRA-1725](https://issues.apache.org/jira/browse/CASSANDRA-1725) | word\_count/pig loadfunc don't match the ColumnFamilyInputFormat ByteBuffer signature |  Major | . | Jeremy Hanna | T Jake Luciani |
| [CASSANDRA-1727](https://issues.apache.org/jira/browse/CASSANDRA-1727) | Read repair IndexOutOfBoundsException |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1720](https://issues.apache.org/jira/browse/CASSANDRA-1720) | DecoratedKey equals() only tests Token |  Minor | . | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-1731](https://issues.apache.org/jira/browse/CASSANDRA-1731) | cli "list" gives unhelpful error when not authenticated |  Minor | Tools | Jonathan Ellis | Pavel Yaskevich |
| [CASSANDRA-1733](https://issues.apache.org/jira/browse/CASSANDRA-1733) | get\_range\_slices doesn't return key from start\_key in KeyRange any more |  Major | . | Arya Goudarzi | T Jake Luciani |
| [CASSANDRA-1734](https://issues.apache.org/jira/browse/CASSANDRA-1734) | New subcolumn resurrect deleted subcolumns |  Minor | . | Wenjun | Jonathan Ellis |
| [CASSANDRA-1741](https://issues.apache.org/jira/browse/CASSANDRA-1741) | clear endpoint cache after updating keyspace metadata |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1700](https://issues.apache.org/jira/browse/CASSANDRA-1700) | mapreduce support is broken |  Major | . | Jeremy Hanna | Stu Hood |
| [CASSANDRA-1745](https://issues.apache.org/jira/browse/CASSANDRA-1745) | index scan should treat not-present columns as not matching index expressions |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1747](https://issues.apache.org/jira/browse/CASSANDRA-1747) | truncate is not secondary index-aware |  Major | Secondary Indexes | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1674](https://issues.apache.org/jira/browse/CASSANDRA-1674) | Repair using abnormally large amounts of disk space |  Major | . | Jonathan Ellis | Stu Hood |
| [CASSANDRA-1749](https://issues.apache.org/jira/browse/CASSANDRA-1749) | Streaming should hold a reference to the source SSTR to prevent GC races |  Critical | . | Stu Hood | Stu Hood |
| [CASSANDRA-1753](https://issues.apache.org/jira/browse/CASSANDRA-1753) | SSTableImport adds columns marked for delete incorrectly in methods addToStandardCF & addToSuperCF |  Major | Tools | Pushpinder Heer | Bryan Tower |
| [CASSANDRA-1732](https://issues.apache.org/jira/browse/CASSANDRA-1732) | nodetool move throws Assertion Error |  Major | . | Arya Goudarzi | T Jake Luciani |
| [CASSANDRA-1755](https://issues.apache.org/jira/browse/CASSANDRA-1755) | handle replica unavailability in index scan |  Minor | . | Jonathan Ellis | T Jake Luciani |
| [CASSANDRA-1756](https://issues.apache.org/jira/browse/CASSANDRA-1756) | DatabaseDescriptor static initialization circular reference when initialized through call to StorageService.instance.initClient |  Minor | . | Erik Onnen | Gary Dusbabek |
| [CASSANDRA-1715](https://issues.apache.org/jira/browse/CASSANDRA-1715) | More schema migration race conditions |  Critical | . | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-1760](https://issues.apache.org/jira/browse/CASSANDRA-1760) | JNA Native check can throw a NoSuchMethodError |  Minor | . | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-1591](https://issues.apache.org/jira/browse/CASSANDRA-1591) | get\_range\_slices always returns super columns that's been removed/restored, regardless of count value in slicerange |  Minor | . | Jianing hu | Jonathan Ellis |
| [CASSANDRA-1736](https://issues.apache.org/jira/browse/CASSANDRA-1736) | ConcurrentModificationException when updating column family metadata |  Major | . | JCF | Jonathan Ellis |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-1721](https://issues.apache.org/jira/browse/CASSANDRA-1721) | Expose SSTableTracker.estimatedKeys() for quickly estimating CF size. |  Minor | . | Jon Hermes | Jon Hermes |
| [CASSANDRA-975](https://issues.apache.org/jira/browse/CASSANDRA-975) | explore upgrading CLHM |  Minor | . | Jonathan Ellis | Matthew F. Dennis |


