
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.2.12 - 2018-02-16



### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-13873](https://issues.apache.org/jira/browse/CASSANDRA-13873) | Ref bug in Scrub |  Major | Tools | T Jake Luciani | Marcus Eriksson |
| [CASSANDRA-13006](https://issues.apache.org/jira/browse/CASSANDRA-13006) | Disable automatic heap dumps on OOM error |  Minor | Configuration | anmols | Benjamin Lerer |
| [CASSANDRA-13801](https://issues.apache.org/jira/browse/CASSANDRA-13801) | CompactionManager sometimes wrongly determines that a background compaction is running for a particular table |  Minor | Compaction | Dimitar Dimitrov | Dimitar Dimitrov |
| [CASSANDRA-14112](https://issues.apache.org/jira/browse/CASSANDRA-14112) | The inspectJvmOptions startup check can trigger some Exception on some JRE versions |  Major | Core | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-14021](https://issues.apache.org/jira/browse/CASSANDRA-14021) | test\_pycodestyle\_compliance - cqlsh\_tests.cqlsh\_tests.TestCqlsh code style errors |  Major | . | Michael Kjellman | Michael Kjellman |
| [CASSANDRA-14181](https://issues.apache.org/jira/browse/CASSANDRA-14181) | RPM package has too many executable files |  Minor | Packaging | Troels Arvin | Troels Arvin |
| [CASSANDRA-14092](https://issues.apache.org/jira/browse/CASSANDRA-14092) | Max ttl of 20 years will overflow localDeletionTime |  Blocker | Core | Paulo Motta | Paulo Motta |


