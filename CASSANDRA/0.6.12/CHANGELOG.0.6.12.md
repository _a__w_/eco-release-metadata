
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.6.12 - 2011-02-21



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2161](https://issues.apache.org/jira/browse/CASSANDRA-2161) | throttle hinted handoff delivery |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2175](https://issues.apache.org/jira/browse/CASSANDRA-2175) | make key cache preheating use less memory |  Minor | . | Jonathan Ellis | Jonathan Ellis |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2085](https://issues.apache.org/jira/browse/CASSANDRA-2085) | digest latencies are not included in snitch calculations |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2081](https://issues.apache.org/jira/browse/CASSANDRA-2081) | Consistency QUORUM does not work anymore (hector:Could not fullfill request on this host) |  Critical | . | Thibaut | amorton |
| [CASSANDRA-2128](https://issues.apache.org/jira/browse/CASSANDRA-2128) | Corrupted Commit logs |  Major | . | Paul Querna | Jonathan Ellis |


