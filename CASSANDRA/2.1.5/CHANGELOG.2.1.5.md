
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1.5 - 2015-04-29



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7688](https://issues.apache.org/jira/browse/CASSANDRA-7688) | Add data sizing to a system table |  Major | . | Jeremiah Jordan | Aleksey Yeschenko |
| [CASSANDRA-8734](https://issues.apache.org/jira/browse/CASSANDRA-8734) | Expose commit log archive status |  Minor | Configuration | Philip S Doctor | Chris Lohfink |
| [CASSANDRA-8225](https://issues.apache.org/jira/browse/CASSANDRA-8225) | Production-capable COPY FROM |  Major | Tools | Jonathan Ellis | Tyler Hobbs |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-8749](https://issues.apache.org/jira/browse/CASSANDRA-8749) | Cleanup SegmentedFile |  Trivial | . | Benedict | Benedict |
| [CASSANDRA-8538](https://issues.apache.org/jira/browse/CASSANDRA-8538) | Clarify default write time timestamp for CQL insert and update |  Minor | Documentation and Website | Jack Krupansky | Tyler Hobbs |
| [CASSANDRA-8829](https://issues.apache.org/jira/browse/CASSANDRA-8829) | Add extra checks to catch SSTable ref counting bugs |  Major | . | Richard Low | Richard Low |
| [CASSANDRA-8842](https://issues.apache.org/jira/browse/CASSANDRA-8842) | Upgrade java-driver used for stress |  Minor | Configuration | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-8847](https://issues.apache.org/jira/browse/CASSANDRA-8847) | Improve MD5Digest.hashCode() |  Trivial | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-8886](https://issues.apache.org/jira/browse/CASSANDRA-8886) | nodetool netstats shows the progress of every streaming session |  Minor | Tools | Phil Yang | Phil Yang |
| [CASSANDRA-8750](https://issues.apache.org/jira/browse/CASSANDRA-8750) | Ensure SSTableReader.last corresponds exactly with the file end |  Minor | . | Benedict | Benedict |
| [CASSANDRA-8908](https://issues.apache.org/jira/browse/CASSANDRA-8908) | Silly update to docs for UPDATE in Cql.g |  Trivial | . | Brian ONeill | Brian ONeill |
| [CASSANDRA-8086](https://issues.apache.org/jira/browse/CASSANDRA-8086) | Cassandra should have ability to limit the number of native connections |  Major | . | Vishy Kasar | Norman Maurer |
| [CASSANDRA-8912](https://issues.apache.org/jira/browse/CASSANDRA-8912) | nodetool command to get the status of things that can be enable/disable'd for backup and handoff |  Minor | Tools | Prajakta Bhosale | Prajakta Bhosale |
| [CASSANDRA-8909](https://issues.apache.org/jira/browse/CASSANDRA-8909) | Replication Strategy creation errors are lost in try/catch |  Trivial | . | Alan Boudreault | Alan Boudreault |
| [CASSANDRA-8769](https://issues.apache.org/jira/browse/CASSANDRA-8769) | Extend cassandra-stress to be slightly more configurable |  Minor | Tools | Anthony Cozzie | Anthony Cozzie |
| [CASSANDRA-8692](https://issues.apache.org/jira/browse/CASSANDRA-8692) | Coalesce intra-cluster network messages |  Major | Streaming and Messaging | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-8946](https://issues.apache.org/jira/browse/CASSANDRA-8946) | Make SSTableScanner always respect its bound |  Major | . | Sylvain Lebresne | Benedict |
| [CASSANDRA-8914](https://issues.apache.org/jira/browse/CASSANDRA-8914) | Don't lookup maxPurgeableTimestamp unless we need to |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-8085](https://issues.apache.org/jira/browse/CASSANDRA-8085) | Make PasswordAuthenticator number of hashing rounds configurable |  Major | Configuration | Tyler Hobbs | Sam Tunnicliffe |
| [CASSANDRA-7533](https://issues.apache.org/jira/browse/CASSANDRA-7533) | Let MAX\_OUTSTANDING\_REPLAY\_COUNT be configurable |  Minor | . | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-9032](https://issues.apache.org/jira/browse/CASSANDRA-9032) | Reduce logging level for MigrationTask abort due to down node from ERROR to INFO |  Minor | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-9066](https://issues.apache.org/jira/browse/CASSANDRA-9066) | BloomFilter serialization is inefficient |  Major | . | Benedict | Gustav Munkby |
| [CASSANDRA-8056](https://issues.apache.org/jira/browse/CASSANDRA-8056) | nodetool snapshot \<keyspace\> -cf \<table\> -t \<sametagname\> does not work on multiple tabes of the same keyspace |  Trivial | . | Esha Pathak |  |
| [CASSANDRA-9076](https://issues.apache.org/jira/browse/CASSANDRA-9076) | Consistency: make default max\_hint\_window\_in\_ms actually 3 hours |  Trivial | . | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-8360](https://issues.apache.org/jira/browse/CASSANDRA-8360) | In DTCS, always compact SSTables in the same time window, even if they are fewer than min\_threshold |  Minor | . | Björn Hegerfors | Björn Hegerfors |
| [CASSANDRA-8359](https://issues.apache.org/jira/browse/CASSANDRA-8359) | Make DTCS consider removing SSTables much more frequently |  Minor | . | Björn Hegerfors | Björn Hegerfors |
| [CASSANDRA-9128](https://issues.apache.org/jira/browse/CASSANDRA-9128) | Flush system.IndexInfo after index state changed |  Minor | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-8348](https://issues.apache.org/jira/browse/CASSANDRA-8348) | allow takeColumnFamilySnapshot to take a list of ColumnFamilies |  Minor | . | Peter Halliday | Sachin Janani |
| [CASSANDRA-9187](https://issues.apache.org/jira/browse/CASSANDRA-9187) | When altering a UDT only the UDT keyspace should be searched |  Minor | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-9196](https://issues.apache.org/jira/browse/CASSANDRA-9196) | Do not rebuild indexes if no columns are actually indexed |  Major | . | Sergio Bossa | Sergio Bossa |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-8154](https://issues.apache.org/jira/browse/CASSANDRA-8154) | desc table output shows key-only index ambiguously |  Minor | . | Russ Hatch | Tyler Hobbs |
| [CASSANDRA-8275](https://issues.apache.org/jira/browse/CASSANDRA-8275) | Some queries with multicolumn relation do not behave properly when secondary index is used |  Major | CQL, Secondary Indexes | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-8707](https://issues.apache.org/jira/browse/CASSANDRA-8707) | Move SegmentedFile, IndexSummary and BloomFilter to utilising RefCounted |  Critical | . | Benedict | Benedict |
| [CASSANDRA-8683](https://issues.apache.org/jira/browse/CASSANDRA-8683) | Ensure early reopening has no overlap with replaced files |  Critical | . | Marcus Eriksson | Benedict |
| [CASSANDRA-8744](https://issues.apache.org/jira/browse/CASSANDRA-8744) | Ensure SSTableReader.first/last are honoured universally |  Critical | . | Benedict | Benedict |
| [CASSANDRA-8747](https://issues.apache.org/jira/browse/CASSANDRA-8747) | Make SSTableWriter.openEarly behaviour more robust |  Major | . | Benedict | Benedict |
| [CASSANDRA-8796](https://issues.apache.org/jira/browse/CASSANDRA-8796) | 'nodetool info' prints exception against older node |  Minor | Tools | Robert Stupp | Robert Stupp |
| [CASSANDRA-8758](https://issues.apache.org/jira/browse/CASSANDRA-8758) | CompressionMetadata.Writer should use a safer version of RefCountedMemory |  Major | . | Benedict | Benedict |
| [CASSANDRA-8776](https://issues.apache.org/jira/browse/CASSANDRA-8776) | nodetool status reports success for missing keyspace |  Minor | . | Stuart Bishop | Sachin Janani |
| [CASSANDRA-8802](https://issues.apache.org/jira/browse/CASSANDRA-8802) | Leaked reference on windows |  Major | . | Philip Thompson | Benedict |
| [CASSANDRA-8406](https://issues.apache.org/jira/browse/CASSANDRA-8406) | Add option to set max\_sstable\_age in fractional days in DTCS |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-8807](https://issues.apache.org/jira/browse/CASSANDRA-8807) | SSTableSimpleUnsortedWriter may block indefinitely on close() if disk writer has crashed |  Minor | . | Benedict | Benedict |
| [CASSANDRA-8792](https://issues.apache.org/jira/browse/CASSANDRA-8792) | Improve Memory assertions |  Trivial | . | Benedict | Benedict |
| [CASSANDRA-8797](https://issues.apache.org/jira/browse/CASSANDRA-8797) | Tuple relation in select query is broken |  Major | . | Ajay | Tyler Hobbs |
| [CASSANDRA-8843](https://issues.apache.org/jira/browse/CASSANDRA-8843) | STCS getMaximalTask checking wrong set is empty. |  Minor | . | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-8856](https://issues.apache.org/jira/browse/CASSANDRA-8856) | Adjustments to range query and 2ary index query parallelism are incorrect |  Major | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-8862](https://issues.apache.org/jira/browse/CASSANDRA-8862) | Commit log pending tasks incremented during getPendingTasks |  Minor | . | Michael Swiecicki | Jonathan Ellis |
| [CASSANDRA-8768](https://issues.apache.org/jira/browse/CASSANDRA-8768) | Using a Cassandra 2.0 seed doesn't allow a new Cassandra 2.1 node to bootstrap |  Minor | . | Ron Kuris | Brandon Williams |
| [CASSANDRA-8848](https://issues.apache.org/jira/browse/CASSANDRA-8848) | Protocol exceptions always use streamID 0 |  Minor | . | Chris Bannister | Chris Bannister |
| [CASSANDRA-8366](https://issues.apache.org/jira/browse/CASSANDRA-8366) | Repair grows data on nodes, causes load to become unbalanced |  Major | Streaming and Messaging | Jan Karlsson | Marcus Eriksson |
| [CASSANDRA-8067](https://issues.apache.org/jira/browse/CASSANDRA-8067) | NullPointerException in KeyCacheSerializer |  Major | . | Eric Leleu | Aleksey Yeschenko |
| [CASSANDRA-8290](https://issues.apache.org/jira/browse/CASSANDRA-8290) | archiving commitlogs after restart fails |  Minor | Lifecycle, Local Write-Read Paths | Manuel Lausch | Sam Tunnicliffe |
| [CASSANDRA-8834](https://issues.apache.org/jira/browse/CASSANDRA-8834) | Top partitions reporting wrong cardinality |  Major | . | Chris Lohfink | Chris Lohfink |
| [CASSANDRA-8883](https://issues.apache.org/jira/browse/CASSANDRA-8883) | Percentile computation should use ceil not floor in EstimatedHistogram |  Minor | Observability | Chris Lohfink | Carl Yeksigian |
| [CASSANDRA-8860](https://issues.apache.org/jira/browse/CASSANDRA-8860) | Remove cold\_reads\_to\_omit from STCS |  Major | Compaction | Phil Yang | Marcus Eriksson |
| [CASSANDRA-8832](https://issues.apache.org/jira/browse/CASSANDRA-8832) | SSTableRewriter.abort() should be more robust to failure |  Major | . | Benedict | Benedict |
| [CASSANDRA-8689](https://issues.apache.org/jira/browse/CASSANDRA-8689) | Assertion error in 2.1.2: ERROR [IndexSummaryManager:1] |  Major | . | Jeff Liu | Benedict |
| [CASSANDRA-8882](https://issues.apache.org/jira/browse/CASSANDRA-8882) | Wrong type mapping for varint -- Cassandra Stress 2.1 |  Major | Tools | Sebastian Estevez |  |
| [CASSANDRA-8808](https://issues.apache.org/jira/browse/CASSANDRA-8808) | CQLSSTableWriter: close does not work + more than one table throws ex |  Major | CQL | Sebastian YEPES FERNANDEZ | Benjamin Lerer |
| [CASSANDRA-8613](https://issues.apache.org/jira/browse/CASSANDRA-8613) | Regression in mixed single and multi-column relation support |  Major | CQL | Tyler Hobbs | Benjamin Lerer |
| [CASSANDRA-8922](https://issues.apache.org/jira/browse/CASSANDRA-8922) | CQL doc: Correct link text in ALTER KEYSPACE section |  Trivial | Documentation and Website | Jon Åslund | Jon Åslund |
| [CASSANDRA-8913](https://issues.apache.org/jira/browse/CASSANDRA-8913) | Use long for key count estimate in cfstats |  Major | Tools | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-8544](https://issues.apache.org/jira/browse/CASSANDRA-8544) | Cassandra could not start with NPE in ColumnFamilyStore.removeUnfinishedCompactionLeftovers |  Minor | Lifecycle | Leonid Shalupov | Joshua McKenzie |
| [CASSANDRA-8757](https://issues.apache.org/jira/browse/CASSANDRA-8757) | IndexSummaryBuilder should construct itself offheap, and share memory between the result of each build() invocation |  Major | . | Benedict | Benedict |
| [CASSANDRA-8786](https://issues.apache.org/jira/browse/CASSANDRA-8786) | NullPointerException in ColumnDefinition.hasIndexOption |  Major | . | Mathijs Vogelzang | Aleksey Yeschenko |
| [CASSANDRA-8948](https://issues.apache.org/jira/browse/CASSANDRA-8948) | cassandra-stress does not honour consistency level (cl) parameter when used in combination with user command |  Major | Tools | Andreas Flinck | T Jake Luciani |
| [CASSANDRA-8722](https://issues.apache.org/jira/browse/CASSANDRA-8722) | Auth MBean needs to be registered |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-8535](https://issues.apache.org/jira/browse/CASSANDRA-8535) | java.lang.RuntimeException: Failed to rename XXX to YYY |  Major | Local Write-Read Paths | Leonid Shalupov | Joshua McKenzie |
| [CASSANDRA-8238](https://issues.apache.org/jira/browse/CASSANDRA-8238) | NPE in SizeTieredCompactionStrategy.filterColdSSTables |  Major | Compaction | Tyler Hobbs | Marcus Eriksson |
| [CASSANDRA-8841](https://issues.apache.org/jira/browse/CASSANDRA-8841) | single\_file\_split\_test fails on 2.1 |  Minor | Compaction | Alan Boudreault | Marcus Eriksson |
| [CASSANDRA-8839](https://issues.apache.org/jira/browse/CASSANDRA-8839) | DatabaseDescriptor throws NPE when rpc\_interface is used |  Major | Configuration | Jan Kesten | Ariel Weisberg |
| [CASSANDRA-8982](https://issues.apache.org/jira/browse/CASSANDRA-8982) | sstableloader outputs progress information and summary stats to stderr instead of stdout |  Major | Tools | Tom Alexander | Yuki Morishita |
| [CASSANDRA-8947](https://issues.apache.org/jira/browse/CASSANDRA-8947) | Cleanup Cell equality |  Minor | . | Benedict | Benedict |
| [CASSANDRA-8746](https://issues.apache.org/jira/browse/CASSANDRA-8746) | SSTableReader.cloneWithNewStart can drop too much page cache for compressed files |  Trivial | . | Benedict | Benedict |
| [CASSANDRA-8981](https://issues.apache.org/jira/browse/CASSANDRA-8981) | IndexSummaryManagerTest.testCompactionsRace intermittently timing out on trunk |  Minor | . | Joshua McKenzie | Benedict |
| [CASSANDRA-7816](https://issues.apache.org/jira/browse/CASSANDRA-7816) | Duplicate DOWN/UP Events Pushed with Native Protocol |  Minor | CQL | Michael Penick | Stefania |
| [CASSANDRA-8851](https://issues.apache.org/jira/browse/CASSANDRA-8851) | Uncaught exception on thread Thread[SharedPool-Worker-16,5,main] after upgrade to 2.1.3 |  Critical | . | Tobias Schlottke | Benedict |
| [CASSANDRA-8991](https://issues.apache.org/jira/browse/CASSANDRA-8991) | CQL3 DropIndexStatement should expose getColumnFamily like the CQL2 version does. |  Minor | . | Ulises Cervino Beresi | Ulises Cervino Beresi |
| [CASSANDRA-8950](https://issues.apache.org/jira/browse/CASSANDRA-8950) | NullPointerException in nodetool getendpoints with non-existent keyspace or table |  Minor | Tools | Tyler Hobbs | Stefania |
| [CASSANDRA-8559](https://issues.apache.org/jira/browse/CASSANDRA-8559) | OOM caused by large tombstone warning. |  Major | . | Dominic Letz | Aleksey Yeschenko |
| [CASSANDRA-8739](https://issues.apache.org/jira/browse/CASSANDRA-8739) | Don't check for overlap with sstables that have had their start positions moved in LCS |  Major | Compaction | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-8900](https://issues.apache.org/jira/browse/CASSANDRA-8900) | AssertionError when binding nested collection in a DELETE |  Minor | CQL | Olivier Michallat | Stefania |
| [CASSANDRA-9022](https://issues.apache.org/jira/browse/CASSANDRA-9022) | Node Cleanup deletes all its data after a new node joined the cluster |  Critical | . | Alan Boudreault | Benedict |
| [CASSANDRA-8411](https://issues.apache.org/jira/browse/CASSANDRA-8411) | Cassandra stress tool fails with NotStrictlyPositiveException on example profiles |  Trivial | Tools | Igor Meltser | Benedict |
| [CASSANDRA-8934](https://issues.apache.org/jira/browse/CASSANDRA-8934) | COPY command has inherent 128KB field size limit |  Major | Tools |  Brian Hess | Philip Thompson |
| [CASSANDRA-8949](https://issues.apache.org/jira/browse/CASSANDRA-8949) | CompressedSequentialWriter.resetAndTruncate can lose data |  Critical | Local Write-Read Paths | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-9021](https://issues.apache.org/jira/browse/CASSANDRA-9021) | AssertionError and Leak detected during sstable compaction |  Major | . | Rocco Varela | Benedict |
| [CASSANDRA-9027](https://issues.apache.org/jira/browse/CASSANDRA-9027) | Error processing org.apache.cassandra.metrics:type=HintedHandOffManager,name=Hints\_created-\<IPv6 address\> |  Major | . | Erik Forsberg | Erik Forsberg |
| [CASSANDRA-8516](https://issues.apache.org/jira/browse/CASSANDRA-8516) | NEW\_NODE topology event emitted instead of MOVED\_NODE by moving node |  Minor | CQL | Tyler Hobbs | Stefania |
| [CASSANDRA-9034](https://issues.apache.org/jira/browse/CASSANDRA-9034) | AssertionError in SizeEstimatesRecorder |  Minor | . | Stefania | Carl Yeksigian |
| [CASSANDRA-8993](https://issues.apache.org/jira/browse/CASSANDRA-8993) | EffectiveIndexInterval calculation is incorrect |  Blocker | . | Benedict | Benedict |
| [CASSANDRA-9047](https://issues.apache.org/jira/browse/CASSANDRA-9047) | The FROZEN and TUPLE keywords should not be reserved in CQL |  Trivial | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-8845](https://issues.apache.org/jira/browse/CASSANDRA-8845) | sorted CQLSSTableWriter accept unsorted clustering keys |  Major | . | Pierre N. | Carl Yeksigian |
| [CASSANDRA-9036](https://issues.apache.org/jira/browse/CASSANDRA-9036) | "disk full" when running cleanup (on a far from full disk) |  Major | . | Erik Forsberg | Robert Stupp |
| [CASSANDRA-8740](https://issues.apache.org/jira/browse/CASSANDRA-8740) | java.lang.AssertionError when reading saved cache |  Major | . | Nikolai Grigoriev | Dave Brosius |
| [CASSANDRA-8351](https://issues.apache.org/jira/browse/CASSANDRA-8351) | Running COPY FROM in cqlsh aborts with errors or segmentation fault |  Minor | . | Joseph Chu | Tyler Hobbs |
| [CASSANDRA-9077](https://issues.apache.org/jira/browse/CASSANDRA-9077) | Deleting an element from a List which is null throws a NPE |  Minor | . | dan jatnieks | Jeff Jirsa |
| [CASSANDRA-9081](https://issues.apache.org/jira/browse/CASSANDRA-9081) | Fix display of triggers in cqlsh |  Major | Tools | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-8919](https://issues.apache.org/jira/browse/CASSANDRA-8919) | cqlsh return error in querying of CompositeType data |  Minor | Tools | Mark | Tyler Hobbs |
| [CASSANDRA-8672](https://issues.apache.org/jira/browse/CASSANDRA-8672) | Ambiguous WriteTimeoutException while completing pending CAS commits |  Minor | . | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-9082](https://issues.apache.org/jira/browse/CASSANDRA-9082) | sstableloader error on trunk due to loading read meter |  Major | Tools | Tyler Hobbs | Benedict |
| [CASSANDRA-8979](https://issues.apache.org/jira/browse/CASSANDRA-8979) | MerkleTree mismatch for deleted and non-existing rows |  Major | . | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-9114](https://issues.apache.org/jira/browse/CASSANDRA-9114) | cqlsh: Formatting of map contents broken |  Major | Tools | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-9122](https://issues.apache.org/jira/browse/CASSANDRA-9122) | CassandraDaemon.java:167 NullPointerException |  Major | . | Will Zhang |  |
| [CASSANDRA-9080](https://issues.apache.org/jira/browse/CASSANDRA-9080) | cqlsh: COPY FROM doesn't quote column names |  Major | Tools | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-8669](https://issues.apache.org/jira/browse/CASSANDRA-8669) | simple\_repair test failing on 2.1 |  Major | . | Philip Thompson | Benedict |
| [CASSANDRA-9116](https://issues.apache.org/jira/browse/CASSANDRA-9116) | Indexes lost on upgrading to 2.1.4 |  Blocker | Configuration | Mark Dewey | Sam Tunnicliffe |
| [CASSANDRA-9180](https://issues.apache.org/jira/browse/CASSANDRA-9180) | Failed bootstrap/replace attempts persist entries in system.peers |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-9137](https://issues.apache.org/jira/browse/CASSANDRA-9137) | nodetool doesn't work when Cassandra run with the property java.net.preferIPv6Addresses=true |  Major | . | Andrey Trubachev | Andrey Trubachev |
| [CASSANDRA-8336](https://issues.apache.org/jira/browse/CASSANDRA-8336) | Add shutdown gossip state to prevent timeouts during rolling restarts |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-8978](https://issues.apache.org/jira/browse/CASSANDRA-8978) | CQLSSTableWriter causes ArrayIndexOutOfBoundsException |  Major | . | Thomas Borg Salling | Carl Yeksigian |
| [CASSANDRA-6458](https://issues.apache.org/jira/browse/CASSANDRA-6458) | nodetool getendpoints doesn't validate key arity |  Trivial | Tools | Daneel Yaitskov | Philip Thompson |
| [CASSANDRA-9135](https://issues.apache.org/jira/browse/CASSANDRA-9135) | testScrubCorruptedCounterRow is failing under test-compression target |  Major | Testing | Philip Thompson | Tyler Hobbs |
| [CASSANDRA-9098](https://issues.apache.org/jira/browse/CASSANDRA-9098) | Anticompactions not visible in nodetool compactionstats |  Minor | Compaction | Gustav Munkby | Marcus Eriksson |
| [CASSANDRA-9203](https://issues.apache.org/jira/browse/CASSANDRA-9203) | Removing cold\_reads\_to\_omit is not backwards compatible |  Major | . | Tommy Stendahl | Tommy Stendahl |
| [CASSANDRA-7292](https://issues.apache.org/jira/browse/CASSANDRA-7292) | Can't seed new node into ring with (public) ip of an old node |  Major | . | Juho Mäkinen | Brandon Williams |
| [CASSANDRA-9217](https://issues.apache.org/jira/browse/CASSANDRA-9217) | Error in cqlsh COPY TO |  Major | . | Brian Cantoni | Tyler Hobbs |
| [CASSANDRA-9138](https://issues.apache.org/jira/browse/CASSANDRA-9138) | BlacklistingCompactionsTest failing on test-compression target |  Major | Testing | Philip Thompson | Marcus Eriksson |
| [CASSANDRA-9235](https://issues.apache.org/jira/browse/CASSANDRA-9235) | Max sstable size in leveled manifest is an int, creating large sstables overflows this and breaks LCS |  Major | Compaction | Sergey Maznichenko | Marcus Eriksson |
| [CASSANDRA-9060](https://issues.apache.org/jira/browse/CASSANDRA-9060) | Anticompaction hangs on bloom filter bitset serialization |  Minor | . | Gustav Munkby | Gustav Munkby |
| [CASSANDRA-8812](https://issues.apache.org/jira/browse/CASSANDRA-8812) | JVM Crashes on Windows x86 |  Major | . | Amichai Rothman | Benedict |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-8657](https://issues.apache.org/jira/browse/CASSANDRA-8657) | long-test LongCompactionsTest fails |  Minor | Testing | Michael Shuler | Carl Yeksigian |
| [CASSANDRA-7712](https://issues.apache.org/jira/browse/CASSANDRA-7712) | temporary files need to be cleaned by unit tests |  Minor | Testing | Michael Shuler | Michael Shuler |
| [CASSANDRA-9125](https://issues.apache.org/jira/browse/CASSANDRA-9125) | cqlsh: add tests for INSERT and UPDATE completion |  Minor | Tools | Jim Witschey | Jim Witschey |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-8495](https://issues.apache.org/jira/browse/CASSANDRA-8495) | Add data type serialization formats to native protocol specs |  Minor | Documentation and Website | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-9158](https://issues.apache.org/jira/browse/CASSANDRA-9158) | Change org.antlr:stringtemplate dependency to org.antlr:ST4 |  Minor | Configuration | Tomoki Odaka | Dave Brosius |


