
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.1 beta1 - 2014-02-20



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5887](https://issues.apache.org/jira/browse/CASSANDRA-5887) | add lz4 compression for internode compressio |  Minor | . | Andy Cobley | Marcus Eriksson |
| [CASSANDRA-5590](https://issues.apache.org/jira/browse/CASSANDRA-5590) | User defined types for CQL3 |  Major | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5742](https://issues.apache.org/jira/browse/CASSANDRA-5742) | Add command "list snapshots" to nodetool |  Minor | Tools | Geert Schuring | sankalp kohli |
| [CASSANDRA-5357](https://issues.apache.org/jira/browse/CASSANDRA-5357) | Query cache / partition head cache |  Major | . | Jonathan Ellis | Marcus Eriksson |
| [CASSANDRA-6379](https://issues.apache.org/jira/browse/CASSANDRA-6379) | Replace index\_interval with min/max\_index\_interval |  Major | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-6305](https://issues.apache.org/jira/browse/CASSANDRA-6305) | cqlsh support for User Types |  Major | . | Aleksey Yeschenko | Mikhail Stepura |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5971](https://issues.apache.org/jira/browse/CASSANDRA-5971) | Get rid of thrift-generated Index\* classes usage in C\* internals |  Trivial | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5960](https://issues.apache.org/jira/browse/CASSANDRA-5960) | Remove 1.2 network compatibility code from 2.1 |  Trivial | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5996](https://issues.apache.org/jira/browse/CASSANDRA-5996) | Remove leveled manifest json migration code |  Minor | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-6138](https://issues.apache.org/jira/browse/CASSANDRA-6138) | simplify/readability of SizeTieredCompactionStrategy.mostInterestingBucket and eliminate some allocations |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-1337](https://issues.apache.org/jira/browse/CASSANDRA-1337) | parallelize fetching rows for low-cardinality indexes and range scans of small tables |  Minor | . | Jonathan Ellis | Tyler Hobbs |
| [CASSANDRA-6278](https://issues.apache.org/jira/browse/CASSANDRA-6278) | Use AtomicIntegerFieldUpdater to save memory in row cache |  Trivial | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-6253](https://issues.apache.org/jira/browse/CASSANDRA-6253) | Refactor: remove CFDefinition |  Major | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-1632](https://issues.apache.org/jira/browse/CASSANDRA-1632) | Thread workflow and cpu affinity |  Major | . | Chris Goffinet | Jason Brown |
| [CASSANDRA-6417](https://issues.apache.org/jira/browse/CASSANDRA-6417) | Make flush single-pass when partition tombstones are present |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-3578](https://issues.apache.org/jira/browse/CASSANDRA-3578) | Multithreaded commitlog |  Minor | . | Jonathan Ellis | Benedict |
| [CASSANDRA-5883](https://issues.apache.org/jira/browse/CASSANDRA-5883) | Switch to Logback |  Minor | Tools | Jonathan Ellis | Dave Brosius |
| [CASSANDRA-6356](https://issues.apache.org/jira/browse/CASSANDRA-6356) | Proposal: Statistics.db (SSTableMetadata) format change |  Minor | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-5417](https://issues.apache.org/jira/browse/CASSANDRA-5417) | Push composites support in the storage engine |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-5906](https://issues.apache.org/jira/browse/CASSANDRA-5906) | Avoid allocating over-large bloom filters |  Major | . | Jonathan Ellis | Yuki Morishita |
| [CASSANDRA-6199](https://issues.apache.org/jira/browse/CASSANDRA-6199) | Improve Stress Tool |  Minor | Tools | Benedict | Benedict |
| [CASSANDRA-6281](https://issues.apache.org/jira/browse/CASSANDRA-6281) | Use Atomic\*FieldUpdater to save memory |  Minor | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-6383](https://issues.apache.org/jira/browse/CASSANDRA-6383) | Secondary indexing of map keys |  Major | CQL, Secondary Indexes | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6438](https://issues.apache.org/jira/browse/CASSANDRA-6438) | Make user types keyspace scoped |  Major | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6381](https://issues.apache.org/jira/browse/CASSANDRA-6381) | Refactor nodetool |  Minor | . | Yuki Morishita | Clément Lardeur |
| [CASSANDRA-4511](https://issues.apache.org/jira/browse/CASSANDRA-4511) | Secondary index support for CQL3 collections |  Major | CQL, Secondary Indexes | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6271](https://issues.apache.org/jira/browse/CASSANDRA-6271) | Replace SnapTree in AtomicSortedColumns |  Major | . | Benedict | Benedict |
| [CASSANDRA-5131](https://issues.apache.org/jira/browse/CASSANDRA-5131) | Make the TOKENS app state the canonical place for tokens |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-6497](https://issues.apache.org/jira/browse/CASSANDRA-6497) | Iterable CqlPagingRecordReader |  Major | . | Luca Rosellini | Luca Rosellini |
| [CASSANDRA-6504](https://issues.apache.org/jira/browse/CASSANDRA-6504) | counters++ |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6640](https://issues.apache.org/jira/browse/CASSANDRA-6640) | Improve custom 2i performance and abstraction |  Major | . | Miguel Angel Fernandez Diaz | Miguel Angel Fernandez Diaz |
| [CASSANDRA-5549](https://issues.apache.org/jira/browse/CASSANDRA-5549) | Remove Table.switchLock |  Major | . | Jonathan Ellis | Benedict |
| [CASSANDRA-6594](https://issues.apache.org/jira/browse/CASSANDRA-6594) | CqlRecordWriter marked final |  Major | . | Luca Rosellini | Luca Rosellini |
| [CASSANDRA-6630](https://issues.apache.org/jira/browse/CASSANDRA-6630) | Replace UnsortedColumns with ArrayBackedSortedColumns |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6639](https://issues.apache.org/jira/browse/CASSANDRA-6639) | Update Guava to version 16 |  Trivial | . | Mikhail Mazurskiy | Mikhail Stepura |
| [CASSANDRA-6656](https://issues.apache.org/jira/browse/CASSANDRA-6656) | Exception logging |  Trivial | Tools | Ding Yuan | Ding Yuan |
| [CASSANDRA-6662](https://issues.apache.org/jira/browse/CASSANDRA-6662) | Sort/reconcile cells in ArrayBackedSortedColumns only when an accessor is called |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-6690](https://issues.apache.org/jira/browse/CASSANDRA-6690) | Replace EmptyColumns with updated ArrayBackedSortedColumns |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-4911](https://issues.apache.org/jira/browse/CASSANDRA-4911) | Lift limitation that order by columns must be selected for IN queries |  Minor | CQL | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-6660](https://issues.apache.org/jira/browse/CASSANDRA-6660) | Make node tool command take a password file |  Trivial | . | Vishy Kasar | Clément Lardeur |
| [CASSANDRA-6575](https://issues.apache.org/jira/browse/CASSANDRA-6575) | By default, Cassandra should refuse to start if JNA can't be initialized properly |  Minor | . | Tupshin Harper | Clément Lardeur |
| [CASSANDRA-6596](https://issues.apache.org/jira/browse/CASSANDRA-6596) | Split out outgoing stream throughput within a DC and inter-DC |  Minor | . | Jeremy Hanna | Vijay |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5896](https://issues.apache.org/jira/browse/CASSANDRA-5896) | Stress reports invalid latencies |  Major | Tools | Ryan McGuire | Dave Brosius |
| [CASSANDRA-5961](https://issues.apache.org/jira/browse/CASSANDRA-5961) | ant test should only show WARN+ on stdout |  Minor | Testing | Jonathan Ellis | Dave Brosius |
| [CASSANDRA-6153](https://issues.apache.org/jira/browse/CASSANDRA-6153) | Stress stopped calculating latency stats |  Major | Tools | Ryan McGuire | Mikhail Stepura |
| [CASSANDRA-6142](https://issues.apache.org/jira/browse/CASSANDRA-6142) | Remove multithreaded compaction (and precompactedrow) |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5519](https://issues.apache.org/jira/browse/CASSANDRA-5519) | Reduce index summary memory use for cold sstables |  Minor | . | Jonathan Ellis | Tyler Hobbs |
| [CASSANDRA-6231](https://issues.apache.org/jira/browse/CASSANDRA-6231) | Add snapshot disk space to cfstats |  Minor | . | Jonathan Ellis | Mikhail Stepura |
| [CASSANDRA-6515](https://issues.apache.org/jira/browse/CASSANDRA-6515) | Fix validator lookup when converting cells to objects (pig) |  Minor | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-6520](https://issues.apache.org/jira/browse/CASSANDRA-6520) | Fix dropping columns |  Major | CQL | Russ Hatch | Mikhail Stepura |
| [CASSANDRA-6530](https://issues.apache.org/jira/browse/CASSANDRA-6530) | Fix logback configuration in scripts and debian packaging for trunk/2.1 |  Minor | Tools | Michael Shuler | Michael Shuler |
| [CASSANDRA-6578](https://issues.apache.org/jira/browse/CASSANDRA-6578) | Slower shutdown on trunk than previous versions |  Minor | . | Brandon Williams | Benedict |
| [CASSANDRA-5202](https://issues.apache.org/jira/browse/CASSANDRA-5202) | CFs should have globally and temporally unique CF IDs to prevent "reusing" data from earlier incarnation of same CF name |  Major | . | Marat Bedretdinov | Yuki Morishita |
| [CASSANDRA-6498](https://issues.apache.org/jira/browse/CASSANDRA-6498) | Null pointer exception in custom secondary indexes |  Minor | Secondary Indexes | Andrés de la Peña | Miguel Angel Fernandez Diaz |
| [CASSANDRA-6583](https://issues.apache.org/jira/browse/CASSANDRA-6583) | DROP TYPE complains no keyspace is active (when keyspace is active) |  Minor | CQL | Russ Hatch | Sylvain Lebresne |
| [CASSANDRA-6414](https://issues.apache.org/jira/browse/CASSANDRA-6414) | BlacklistingCompactionsTest fails in trunk |  Major | Testing | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-6617](https://issues.apache.org/jira/browse/CASSANDRA-6617) | nodetool help removenode man page missing status, force, ID options description |  Trivial | Documentation and Website | K. B. Hahn | Brandon Williams |
| [CASSANDRA-6564](https://issues.apache.org/jira/browse/CASSANDRA-6564) | Gossiper failed with ArrayIndexOutOfBoundsException |  Minor | . | Shao-Chuan Wang | Tyler Hobbs |
| [CASSANDRA-6634](https://issues.apache.org/jira/browse/CASSANDRA-6634) | UTMetaData is missing equals() definition on trunk, breaking KSMetaData and CFMetaData equality |  Trivial | . | Benedict | Benedict |
| [CASSANDRA-6472](https://issues.apache.org/jira/browse/CASSANDRA-6472) | Node hangs when Drop Keyspace / Table is executed |  Minor | . | amorton |  |
| [CASSANDRA-6671](https://issues.apache.org/jira/browse/CASSANDRA-6671) | NoSuchElementException in trunk |  Major | . | Tyler Hobbs | Benedict |
| [CASSANDRA-6649](https://issues.apache.org/jira/browse/CASSANDRA-6649) | CQL: disallow counter update with "USING TIMESTAMP" and "USING TTL" |  Trivial | . | Sylvain Lebresne | Aleksey Yeschenko |
| [CASSANDRA-6695](https://issues.apache.org/jira/browse/CASSANDRA-6695) | Don't exchange schema between nodes with different versions (no pull, no push) |  Major | . | Piotr Kołaczkowski | Aleksey Yeschenko |
| [CASSANDRA-6631](https://issues.apache.org/jira/browse/CASSANDRA-6631) | cassandra-stress failing in trunk |  Major | Tools | Michael Shuler |  |
| [CASSANDRA-6691](https://issues.apache.org/jira/browse/CASSANDRA-6691) | Improvements and FIxes to Stress |  Minor | Tools | Benedict | Benedict |
| [CASSANDRA-6700](https://issues.apache.org/jira/browse/CASSANDRA-6700) | Use real node messaging versions for schema exchange decisions |  Major | . | Piotr Kołaczkowski | Aleksey Yeschenko |
| [CASSANDRA-6711](https://issues.apache.org/jira/browse/CASSANDRA-6711) | SecondaryIndexManager#deleteFromIndexes() doesn't correctly retrieve column indexes |  Major | . | Sergio Bossa | Sergio Bossa |
| [CASSANDRA-6712](https://issues.apache.org/jira/browse/CASSANDRA-6712) | Equals without hashcode in SpeculativeRetry |  Trivial | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-6713](https://issues.apache.org/jira/browse/CASSANDRA-6713) | Snapshot based repair does not send snapshot command to itself |  Major | . | sankalp kohli | Yuki Morishita |
| [CASSANDRA-6714](https://issues.apache.org/jira/browse/CASSANDRA-6714) | Fix replaying old (1.2) commitlog in Cassandra 2.0 |  Major | . | Piotr Kołaczkowski | Aleksey Yeschenko |
| [CASSANDRA-5631](https://issues.apache.org/jira/browse/CASSANDRA-5631) | NPE when creating column family shortly after multinode startup |  Major | . | Martin Serrano | Aleksey Yeschenko |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7114](https://issues.apache.org/jira/browse/CASSANDRA-7114) | Paging dtests |  Major | . | Ryan McGuire | Russ Hatch |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6456](https://issues.apache.org/jira/browse/CASSANDRA-6456) | log cassandra.yaml at startup |  Trivial | . | Jeremy Hanna | Sean Bridges |
| [CASSANDRA-5871](https://issues.apache.org/jira/browse/CASSANDRA-5871) | Update nodetool to use o.a.c.metrics |  Minor | . | Yuki Morishita | Lyuben Todorov |
| [CASSANDRA-6355](https://issues.apache.org/jira/browse/CASSANDRA-6355) | SSTable/SSTableReader cleanup |  Trivial | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-5351](https://issues.apache.org/jira/browse/CASSANDRA-5351) | Avoid repairing already-repaired data by default |  Major | . | Jonathan Ellis | Lyuben Todorov |
| [CASSANDRA-5872](https://issues.apache.org/jira/browse/CASSANDRA-5872) | Bundle JNA |  Minor | . | Jonathan Ellis | Lyuben Todorov |


