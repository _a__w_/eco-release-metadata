
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.2.x - Unreleased (as of 2018-10-20)



### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9952](https://issues.apache.org/jira/browse/CASSANDRA-9952) | UDF with no parameters prevents cqlsh DESCRIBE from working |  Minor | Tools | Jim Meyer |  |
| [CASSANDRA-9619](https://issues.apache.org/jira/browse/CASSANDRA-9619) | Read performance regression in tables with many columns on trunk and 2.2 vs. 2.1 |  Major | . | Jim Witschey | Benedict |
| [CASSANDRA-10487](https://issues.apache.org/jira/browse/CASSANDRA-10487) | Regression in select count with limit |  Major | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-10372](https://issues.apache.org/jira/browse/CASSANDRA-10372) | Adds smallint and tinyint to the CQL documentation |  Minor | Documentation and Website | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-10814](https://issues.apache.org/jira/browse/CASSANDRA-10814) | [patch] make json date formatter thread safe |  Trivial | Local Write-Read Paths | Dave Brosius | Dave Brosius |
| [CASSANDRA-11047](https://issues.apache.org/jira/browse/CASSANDRA-11047) | native protocol will not bind ipv6 |  Major | CQL | Brandon Williams | Norman Maurer |
| [CASSANDRA-10607](https://issues.apache.org/jira/browse/CASSANDRA-10607) | Using reserved keyword for Type field crashes cqlsh |  Minor | Tools | Mike Prince |  |
| [CASSANDRA-11339](https://issues.apache.org/jira/browse/CASSANDRA-11339) | WHERE clause in SELECT DISTINCT can be ignored |  Major | CQL | Philip Thompson | Alex Petrov |
| [CASSANDRA-10583](https://issues.apache.org/jira/browse/CASSANDRA-10583) | After bulk loading CQL query on timestamp column returns wrong result |  Major | . | Kai Wang |  |
| [CASSANDRA-10988](https://issues.apache.org/jira/browse/CASSANDRA-10988) | isInclusive and boundsAsComposites in Restriction take bounds in different order |  Major | CQL | Vassil Hristov | Alex Petrov |
| [CASSANDRA-13130](https://issues.apache.org/jira/browse/CASSANDRA-13130) | Strange result of several list updates in a single request |  Trivial | . | Mikhail Krupitskiy | Benjamin Lerer |
| [CASSANDRA-13847](https://issues.apache.org/jira/browse/CASSANDRA-13847) | test failure in cqlsh\_tests.cqlsh\_tests.CqlLoginTest.test\_list\_roles\_after\_login |  Major | Testing, Tools | Joel Knighton | Andrés de la Peña |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9464](https://issues.apache.org/jira/browse/CASSANDRA-9464) | test-clientutil-jar broken |  Major | . | Michael Shuler |  |
| [CASSANDRA-12377](https://issues.apache.org/jira/browse/CASSANDRA-12377) | Add byteman support for 2.2 |  Trivial | Testing | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-12890](https://issues.apache.org/jira/browse/CASSANDRA-12890) | testall failure in org.apache.cassandra.db.ColumnFamilyStoreTest.testSliceByNamesCommandOldMetadata |  Major | . | Michael Shuler | Sylvain Lebresne |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-10146](https://issues.apache.org/jira/browse/CASSANDRA-10146) | Deprecate v1 and v2 protocol in 2.2, drop support in 3.0 |  Major | CQL | Tyler Hobbs | Benjamin Lerer |


