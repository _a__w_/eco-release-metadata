
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.8.7 - 2011-10-10



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3201](https://issues.apache.org/jira/browse/CASSANDRA-3201) | When a mmap fails, Cassandra should exit. |  Minor | . | Eldon Stegall | Jonathan Ellis |
| [CASSANDRA-2777](https://issues.apache.org/jira/browse/CASSANDRA-2777) | Pig storage handler should implement LoadMetadata |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3137](https://issues.apache.org/jira/browse/CASSANDRA-3137) | Implement wrapping intersections for ConfigHelper's InputKeyRange |  Minor | . | mck | mck |
| [CASSANDRA-3211](https://issues.apache.org/jira/browse/CASSANDRA-3211) | Enhanced IP resolution for machines with multiple network interfaces |  Minor | . | Brian ONeill | Brian ONeill |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-3208](https://issues.apache.org/jira/browse/CASSANDRA-3208) | USE \<keyspace\> doesn't work for numeric keyspaces |  Minor | . | Ophir Radnitz | Pavel Yaskevich |
| [CASSANDRA-3222](https://issues.apache.org/jira/browse/CASSANDRA-3222) | cfhistograms is transposed/wrong again |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3207](https://issues.apache.org/jira/browse/CASSANDRA-3207) | Log message at INFO when a global or keyspace level repair operation completes |  Minor | . | Benjamin Coverston | Sylvain Lebresne |
| [CASSANDRA-3218](https://issues.apache.org/jira/browse/CASSANDRA-3218) | Failure reading a erroneous/spurious AutoSavingCache file can result in a failed application of a migration, which can prevent a node from reaching schema agreement. |  Minor | . | Eldon Stegall | Eldon Stegall |
| [CASSANDRA-3247](https://issues.apache.org/jira/browse/CASSANDRA-3247) | sstableloader ignores option doesn't work correctly |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3206](https://issues.apache.org/jira/browse/CASSANDRA-3206) | increase file descriptor limit in deb, rpm packages |  Minor | Packaging | Jonathan Ellis | paul cannon |
| [CASSANDRA-3256](https://issues.apache.org/jira/browse/CASSANDRA-3256) | AssertionError when repairing a node |  Minor | . | Jason Harvey | Sylvain Lebresne |
| [CASSANDRA-3257](https://issues.apache.org/jira/browse/CASSANDRA-3257) | Enabling SSL on a fairly light cluster leaks Open files. |  Minor | . | Vijay | Vijay |
| [CASSANDRA-3231](https://issues.apache.org/jira/browse/CASSANDRA-3231) | CQL does not throw an error when invalid hex is supplied |  Minor | . | Brandon Williams | Pavel Yaskevich |
| [CASSANDRA-3239](https://issues.apache.org/jira/browse/CASSANDRA-3239) | can't use RackInferringSnitch and CQL JDBC's "CREATE KEYSPACE" with NetworkTopologyStrategy |  Minor | CQL | M Chen | Pavel Yaskevich |
| [CASSANDRA-3262](https://issues.apache.org/jira/browse/CASSANDRA-3262) | SimpleSnitch.compareEndpoints doesn't respect the intent of the snitch |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-3255](https://issues.apache.org/jira/browse/CASSANDRA-3255) | Sstable scrub status persists in compactionstats after scrub is complete |  Trivial | . | Jason Harvey | Pavel Yaskevich |
| [CASSANDRA-2810](https://issues.apache.org/jira/browse/CASSANDRA-2810) | RuntimeException in Pig when using "dump" command on column name |  Major | . | Silvère Lestang | Brandon Williams |
| [CASSANDRA-3258](https://issues.apache.org/jira/browse/CASSANDRA-3258) | \*.bat files fails when CASSANDRA\_HOME contains a white space. |  Major | Packaging | Tim Almdal | Tim Almdal |
| [CASSANDRA-3243](https://issues.apache.org/jira/browse/CASSANDRA-3243) | Node which was decommissioned and shut-down reappears on a single node |  Minor | . | Jason Harvey | Brandon Williams |
| [CASSANDRA-3259](https://issues.apache.org/jira/browse/CASSANDRA-3259) | Replace token leaves the old node state in tact causing problems in cli |  Minor | . | Vijay | Vijay |
| [CASSANDRA-3293](https://issues.apache.org/jira/browse/CASSANDRA-3293) | Metered Flusher log message confusing |  Trivial | . | Radim Kolar |  |
| [CASSANDRA-3296](https://issues.apache.org/jira/browse/CASSANDRA-3296) | CsDef instead of CfDef in system\_add\_keyspace() function |  Trivial | . | Alexis Wilke |  |
| [CASSANDRA-3273](https://issues.apache.org/jira/browse/CASSANDRA-3273) | FailureDetector can take a very long time to mark a host down |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-3198](https://issues.apache.org/jira/browse/CASSANDRA-3198) | debian packaging installation problem when installing for the first time |  Major | Packaging | Jérémy Sevellec | Jérémy Sevellec |
| [CASSANDRA-3301](https://issues.apache.org/jira/browse/CASSANDRA-3301) | Java Stress Tool:  COUNTER\_GET reads from CounterSuper1 instead of SuperCounter1 |  Major | . | Cathy Daw | Sylvain Lebresne |
| [CASSANDRA-3309](https://issues.apache.org/jira/browse/CASSANDRA-3309) | Nodetool Doesnt close the open JMX connection causing it to leak Threads |  Major | . | Vijay | Vijay |
| [CASSANDRA-3312](https://issues.apache.org/jira/browse/CASSANDRA-3312) | need initClause when catch Exception and throw new Exception in cli |  Minor | . | Jackson Chung | satish babu krishnamoorthy |


