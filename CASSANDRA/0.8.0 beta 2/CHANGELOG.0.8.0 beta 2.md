
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.8.0 beta 2 - 2011-05-05



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2404](https://issues.apache.org/jira/browse/CASSANDRA-2404) | if out of disk space reclaim compacted SSTables during memtable flush |  Minor | . | amorton | Jonathan Ellis |
| [CASSANDRA-2551](https://issues.apache.org/jira/browse/CASSANDRA-2551) | nodetool: Automatically calculate compaction progress |  Minor | . | Chris Burroughs | Chris Burroughs |
| [CASSANDRA-2560](https://issues.apache.org/jira/browse/CASSANDRA-2560) | Fix svn:ignore on driver's sub-folders |  Trivial | . | Stephen Connolly | Stephen Connolly |
| [CASSANDRA-2558](https://issues.apache.org/jira/browse/CASSANDRA-2558) | Add "concurrent\_compactions" configuration |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2561](https://issues.apache.org/jira/browse/CASSANDRA-2561) | CASSANDRA-1851 for v0.8.0 branch |  Major | Packaging | Stephen Connolly | Stephen Connolly |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2517](https://issues.apache.org/jira/browse/CASSANDRA-2517) | Update distributed tests for optional Column fields |  Major | Testing | Stu Hood | Stu Hood |
| [CASSANDRA-2457](https://issues.apache.org/jira/browse/CASSANDRA-2457) | Batch\_mutate is broken for counters |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-2497](https://issues.apache.org/jira/browse/CASSANDRA-2497) | CLI: issue with keys being interpreted as hex and causing SET statement to fail |  Minor | . | Cathy Daw | Pavel Yaskevich |
| [CASSANDRA-2499](https://issues.apache.org/jira/browse/CASSANDRA-2499) | cassandra-env.sh pattern matching for OpenJDK broken in some cases |  Minor | Packaging | Tyler Hobbs | Jackson Chung |
| [CASSANDRA-2528](https://issues.apache.org/jira/browse/CASSANDRA-2528) | NPE from PrecompactedRow |  Major | . | Jackson Chung | Jonathan Ellis |
| [CASSANDRA-2525](https://issues.apache.org/jira/browse/CASSANDRA-2525) | CQL: create keyspace does not the replication factor argument and allows invalid sql to pass thru |  Critical | . | Cathy Daw | Jonathan Ellis |
| [CASSANDRA-2444](https://issues.apache.org/jira/browse/CASSANDRA-2444) | Remove checkAllColumnFamilies on startup |  Minor | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-2539](https://issues.apache.org/jira/browse/CASSANDRA-2539) | CQL: cqlsh does shows Exception, but not error message when running truncate while a node is down. |  Trivial | . | Cathy Daw | Eric Evans |
| [CASSANDRA-2550](https://issues.apache.org/jira/browse/CASSANDRA-2550) | nodetool setcompactionthroughput requiring wrong number of arguments? |  Minor | Tools | Terje Marthinussen | Terje Marthinussen |
| [CASSANDRA-2532](https://issues.apache.org/jira/browse/CASSANDRA-2532) | Log read timeouts at the StorageProxy level |  Major | . | Stu Hood | Stu Hood |
| [CASSANDRA-2508](https://issues.apache.org/jira/browse/CASSANDRA-2508) | missing imports in CQL Python driver |  Major | Tools | Eric Evans | Jonathan Ellis |
| [CASSANDRA-2557](https://issues.apache.org/jira/browse/CASSANDRA-2557) | StorageProxy sends same message multiple times |  Major | . | Shotaro Kamio |  |
| [CASSANDRA-2567](https://issues.apache.org/jira/browse/CASSANDRA-2567) | CQL: DELETE documentation uses UPDATE examples |  Trivial | . | Cathy Daw |  |
| [CASSANDRA-2565](https://issues.apache.org/jira/browse/CASSANDRA-2565) | Nodes are not reported as alive after being marked down |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-2538](https://issues.apache.org/jira/browse/CASSANDRA-2538) | CQL: NPE running SELECT with an IN clause |  Minor | . | Cathy Daw | Eric Evans |
| [CASSANDRA-2533](https://issues.apache.org/jira/browse/CASSANDRA-2533) | CQL documentation missing INSERT syntax |  Trivial | Documentation and Website | Cathy Daw | Pavel Yaskevich |
| [CASSANDRA-2570](https://issues.apache.org/jira/browse/CASSANDRA-2570) | CQL: incorrect error message running truncate on CF that does not exist |  Trivial | CQL | Cathy Daw | Pavel Yaskevich |
| [CASSANDRA-2572](https://issues.apache.org/jira/browse/CASSANDRA-2572) | cassandra\_cli: CREATE CF HELP should list option as key\_cache\_save\_period instead of keys\_cached\_save\_period |  Trivial | Tools | Cathy Daw | Pavel Yaskevich |
| [CASSANDRA-2563](https://issues.apache.org/jira/browse/CASSANDRA-2563) | Error starting up a cassandra cluster after creating a table in the system keyspace: Attempt to assign id to existing column family. |  Critical | . | Cathy Daw | Jonathan Ellis |
| [CASSANDRA-2552](https://issues.apache.org/jira/browse/CASSANDRA-2552) | ReadResponseResolver Race |  Major | . | Stu Hood | Jonathan Ellis |
| [CASSANDRA-2556](https://issues.apache.org/jira/browse/CASSANDRA-2556) | DatacenterReadResolver not triggering repair |  Minor | . | Stu Hood | Jonathan Ellis |
| [CASSANDRA-2575](https://issues.apache.org/jira/browse/CASSANDRA-2575) | make forceUserDefinedCompaction actually do what it says |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-2536](https://issues.apache.org/jira/browse/CASSANDRA-2536) | Schema disagreements when using connections to multiple hosts |  Minor | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-2571](https://issues.apache.org/jira/browse/CASSANDRA-2571) | Check for null super column for SC CF in ThriftValidation (and always validate the sc key) |  Major | . | Mike Bulman | Sylvain Lebresne |
| [CASSANDRA-2580](https://issues.apache.org/jira/browse/CASSANDRA-2580) | stress will not use existing keyspaces |  Major | Tools | Brandon Williams | Pavel Yaskevich |
| [CASSANDRA-2549](https://issues.apache.org/jira/browse/CASSANDRA-2549) | Start up of 0.8-beta1 on Ubuntu |  Major | Packaging | Drew Broadley | Timu Eren |
| [CASSANDRA-2579](https://issues.apache.org/jira/browse/CASSANDRA-2579) | apache-cassandra-cql-\*.jar as a separate artifact |  Minor | Packaging, Tools | Eric Evans | Eric Evans |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2586](https://issues.apache.org/jira/browse/CASSANDRA-2586) | Remove unframed Thrift option |  Major | CQL | Jonathan Ellis | Jonathan Ellis |


