
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.0.0 - 2013-09-03



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5337](https://issues.apache.org/jira/browse/CASSANDRA-5337) | vnode-aware replacenode command |  Major | . | Jonathan Ellis | Brandon Williams |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5693](https://issues.apache.org/jira/browse/CASSANDRA-5693) | More pre-table creation property validation |  Trivial | . | Dave Brosius | Dave Brosius |
| [CASSANDRA-5880](https://issues.apache.org/jira/browse/CASSANDRA-5880) | DESCRIBE SCHEMA should omit system keyspaces by default |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5534](https://issues.apache.org/jira/browse/CASSANDRA-5534) | Writing wide row causes high CPU usage after compaction |  Major | . | Ryan McGuire | Sylvain Lebresne |
| [CASSANDRA-5729](https://issues.apache.org/jira/browse/CASSANDRA-5729) | Add new TimestampType to cqlsh |  Minor | . | Sylvain Lebresne | Aleksey Yeschenko |
| [CASSANDRA-5690](https://issues.apache.org/jira/browse/CASSANDRA-5690) | Fix AsyncOneResponse |  Minor | . | Mikhail Mazurskiy | Mikhail Mazurskiy |
| [CASSANDRA-5774](https://issues.apache.org/jira/browse/CASSANDRA-5774) | Fix system.schema\_triggers-related trigger/schema issues |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5138](https://issues.apache.org/jira/browse/CASSANDRA-5138) | Provide a better CQL error when table data does not conform to CQL metadata. |  Minor | . | Brian ONeill | Sylvain Lebresne |
| [CASSANDRA-5830](https://issues.apache.org/jira/browse/CASSANDRA-5830) | Paxos loops endlessly due to faulty condition check |  Major | . | Soumava Ghosh | Soumava Ghosh |
| [CASSANDRA-4573](https://issues.apache.org/jira/browse/CASSANDRA-4573) | HSHA doesn't handle large messages gracefully |  Major | . | Tyler Hobbs | Pavel Yaskevich |
| [CASSANDRA-5855](https://issues.apache.org/jira/browse/CASSANDRA-5855) | Scrub does not understand compound primary key created in CQL 3 beta |  Major | Tools | J.B. Langston | Tyler Hobbs |
| [CASSANDRA-5851](https://issues.apache.org/jira/browse/CASSANDRA-5851) | Fix 2i on composite components omissions |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5859](https://issues.apache.org/jira/browse/CASSANDRA-5859) | Don't return internal StreamState objects from streaming mbeans |  Major | . | Nick Bailey | Yuki Morishita |
| [CASSANDRA-5908](https://issues.apache.org/jira/browse/CASSANDRA-5908) | Add existing sstables to leveled manifest on startup |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-5917](https://issues.apache.org/jira/browse/CASSANDRA-5917) | NoSuchMethodError when calling YamlConfigurationLoader.loadConfig() |  Minor | CQL | Jinder Aujla | Dave Brosius |
| [CASSANDRA-5931](https://issues.apache.org/jira/browse/CASSANDRA-5931) | Fix periodic flushing when encountering an empty memtable |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5928](https://issues.apache.org/jira/browse/CASSANDRA-5928) | dateOf() in 2.0 won't work with timestamp columns created in 1.2- |  Major | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-5938](https://issues.apache.org/jira/browse/CASSANDRA-5938) | Bloom filter will be loaded when SSTable is opened for batch |  Trivial | . | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-5925](https://issues.apache.org/jira/browse/CASSANDRA-5925) | Race condition in update lightweight transaction |  Major | . | Phil Persad | Sylvain Lebresne |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5869](https://issues.apache.org/jira/browse/CASSANDRA-5869) | Make vnodes default in 2.0 |  Major | . | Jonathan Ellis | Jonathan Ellis |


