
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 2.0.10 - 2014-08-25



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-6927](https://issues.apache.org/jira/browse/CASSANDRA-6927) | Create a CQL3 based bulk OutputFormat |  Minor | . | Paul Pak | Paul Pak |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7411](https://issues.apache.org/jira/browse/CASSANDRA-7411) | Node enables vnodes when bounced |  Minor | . | Philip Thompson | Philip Thompson |
| [CASSANDRA-7345](https://issues.apache.org/jira/browse/CASSANDRA-7345) | Unfinished or inflight CAS are always done at QUORUM |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-6621](https://issues.apache.org/jira/browse/CASSANDRA-6621) | Add flag to disable STCS in L0 |  Minor | . | Bartłomiej Romański | Marcus Eriksson |
| [CASSANDRA-7528](https://issues.apache.org/jira/browse/CASSANDRA-7528) | certificate not validated for internode SSL encryption. |  Major | . | Joseph Clark | Brandon Williams |
| [CASSANDRA-6596](https://issues.apache.org/jira/browse/CASSANDRA-6596) | Split out outgoing stream throughput within a DC and inter-DC |  Minor | . | Jeremy Hanna | Vijay |
| [CASSANDRA-7621](https://issues.apache.org/jira/browse/CASSANDRA-7621) | Use jvm server machine code compiler |  Minor | Configuration | Blake Eggleston |  |
| [CASSANDRA-6905](https://issues.apache.org/jira/browse/CASSANDRA-6905) | commitlog archive replay should attempt to replay all mutations |  Minor | . | Brandon Williams | Ala' Alkhaldi |
| [CASSANDRA-7618](https://issues.apache.org/jira/browse/CASSANDRA-7618) | Upgrade Cassandra Java driver to 2.0.3 release |  Minor | . | Alex Liu | Alex Liu |
| [CASSANDRA-7432](https://issues.apache.org/jira/browse/CASSANDRA-7432) | Add new CMS GC flags to cassandra\_env.sh for JVM later than 1.7.0\_60 |  Major | Packaging | graham sanderson | Brandon Williams |
| [CASSANDRA-7595](https://issues.apache.org/jira/browse/CASSANDRA-7595) | EmbeddedCassandraService class should provide a stop method |  Minor | . | Mirko Tschäni | Lyuben Todorov |
| [CASSANDRA-7635](https://issues.apache.org/jira/browse/CASSANDRA-7635) | Make hinted\_handoff\_throttle\_in\_kb configurable via nodetool |  Minor | Tools | Matt Stump | Lyuben Todorov |
| [CASSANDRA-7726](https://issues.apache.org/jira/browse/CASSANDRA-7726) | Give CRR a default input\_cql Statement |  Major | . | Russell Spitzer | Mike Adamson |
| [CASSANDRA-6126](https://issues.apache.org/jira/browse/CASSANDRA-6126) | Set MALLOC\_ARENA\_MAX in cassandra-env.sh for new glibc per-thread allocator |  Minor | Packaging | J. Ryan Earl | Brandon Williams |
| [CASSANDRA-7641](https://issues.apache.org/jira/browse/CASSANDRA-7641) | cqlsh should automatically disable tracing when selecting from system\_traces |  Minor | Tools | Brandon Williams | Philip Thompson |
| [CASSANDRA-7788](https://issues.apache.org/jira/browse/CASSANDRA-7788) | Improve PasswordAuthenticator default super user setup |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-7753](https://issues.apache.org/jira/browse/CASSANDRA-7753) | Level compaction for Paxos table |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-7650](https://issues.apache.org/jira/browse/CASSANDRA-7650) | Expose auto\_bootstrap as a system property override |  Trivial | Configuration | Joe Hohertz | Joe Hohertz |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7278](https://issues.apache.org/jira/browse/CASSANDRA-7278) | NPE in StorageProxy.java:1920 |  Minor | . | Duncan Sands | sankalp kohli |
| [CASSANDRA-7440](https://issues.apache.org/jira/browse/CASSANDRA-7440) | Unrecognized opcode or flag causes ArrayIndexOutOfBoundsException |  Minor | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-7459](https://issues.apache.org/jira/browse/CASSANDRA-7459) | CqlRecordWriter doesn't close socket connections |  Major | . | Peter Williams | Peter Williams |
| [CASSANDRA-7414](https://issues.apache.org/jira/browse/CASSANDRA-7414) | After cleanup we can end up with non-compacting high level sstables |  Minor | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-7455](https://issues.apache.org/jira/browse/CASSANDRA-7455) | AssertionError with static columns |  Minor | . | graham sanderson | Sylvain Lebresne |
| [CASSANDRA-7452](https://issues.apache.org/jira/browse/CASSANDRA-7452) | Fix typo in CFDefinition (regarding static columns) |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-7254](https://issues.apache.org/jira/browse/CASSANDRA-7254) | NPE on startup if another Cassandra instance is already running |  Minor | . | Tyler Hobbs | Brandon Williams |
| [CASSANDRA-7490](https://issues.apache.org/jira/browse/CASSANDRA-7490) | Static columns mess up selects with ordering and clustering column ranges |  Major | CQL | graham sanderson | graham sanderson |
| [CASSANDRA-7470](https://issues.apache.org/jira/browse/CASSANDRA-7470) | java.lang.AssertionError are causing cql responses to always go to streamId = 0 instead of the request sending streamId |  Trivial | CQL | Dominic Letz | Tyler Hobbs |
| [CASSANDRA-7535](https://issues.apache.org/jira/browse/CASSANDRA-7535) | Coverage analysis for range queries |  Major | . | David Semeria | Tyler Hobbs |
| [CASSANDRA-7541](https://issues.apache.org/jira/browse/CASSANDRA-7541) | Windows: IOException when repairing a range of tokens |  Minor | Streaming and Messaging | Joshua McKenzie | Joshua McKenzie |
| [CASSANDRA-7508](https://issues.apache.org/jira/browse/CASSANDRA-7508) | nodetool prints error if dirs don't exist. |  Minor | Tools | Jeremiah Jordan | Tyler Hobbs |
| [CASSANDRA-7200](https://issues.apache.org/jira/browse/CASSANDRA-7200) | word count broken |  Major | . | Brandon Williams | Ala' Alkhaldi |
| [CASSANDRA-5345](https://issues.apache.org/jira/browse/CASSANDRA-5345) | Potential problem with GarbageCollectorMXBean |  Minor | Observability | Matt Byrd | Joshua McKenzie |
| [CASSANDRA-7572](https://issues.apache.org/jira/browse/CASSANDRA-7572) | cassandra-env.sh fails to find Java version if JAVA\_TOOL\_OPTIONS in environment |  Major | Configuration | Gregory Ramsperger | Gregory Ramsperger |
| [CASSANDRA-7576](https://issues.apache.org/jira/browse/CASSANDRA-7576) | DateType columns not properly converted to TimestampType when in ReversedType columns. |  Major | . | Karl Rieb | Karl Rieb |
| [CASSANDRA-6454](https://issues.apache.org/jira/browse/CASSANDRA-6454) | Pig support for hadoop CqlInputFormat |  Major | . | Alex Liu | Alex Liu |
| [CASSANDRA-6930](https://issues.apache.org/jira/browse/CASSANDRA-6930) | Dynamic Snitch isWorthMergingForRangeQuery Doesn't Handle Some Cases Optimally |  Minor | . | Tyler Hobbs | Ala' Alkhaldi |
| [CASSANDRA-7611](https://issues.apache.org/jira/browse/CASSANDRA-7611) | incomplete CREATE/DROP USER help and tab completion |  Trivial | Documentation and Website | K. B. Hahn | Michał Michalski |
| [CASSANDRA-7467](https://issues.apache.org/jira/browse/CASSANDRA-7467) | flood of "setting live ratio to maximum of 64" from repair |  Major | . | Jackson Chung | Aleksey Yeschenko |
| [CASSANDRA-5481](https://issues.apache.org/jira/browse/CASSANDRA-5481) | CQLSH exception handling could leave a session in a bad state |  Minor | . | Jordan Pittier | Jordan Pittier |
| [CASSANDRA-7649](https://issues.apache.org/jira/browse/CASSANDRA-7649) | remove ability to change num\_tokens |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-7647](https://issues.apache.org/jira/browse/CASSANDRA-7647) | Track min/max timestamps of range tombstones |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-7632](https://issues.apache.org/jira/browse/CASSANDRA-7632) | NPE in AutoSavingCache$Writer.deleteOldCacheFiles |  Minor | . | Vishy Kasar | Marcus Eriksson |
| [CASSANDRA-7511](https://issues.apache.org/jira/browse/CASSANDRA-7511) | Always flush on TRUNCATE |  Minor | . | Viktor Jevdokimov | Jeremiah Jordan |
| [CASSANDRA-7668](https://issues.apache.org/jira/browse/CASSANDRA-7668) | Make gc\_grace\_seconds 7 days for system tables |  Minor | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-7669](https://issues.apache.org/jira/browse/CASSANDRA-7669) | nodetool fails to connect when ipv6 host is specified |  Trivial | Tools | Adam Holmberg | Adam Holmberg |
| [CASSANDRA-7663](https://issues.apache.org/jira/browse/CASSANDRA-7663) | Removing a seed causes previously removed seeds to reappear |  Major | . | Richard Low | Brandon Williams |
| [CASSANDRA-7229](https://issues.apache.org/jira/browse/CASSANDRA-7229) | Hadoop2 jobs throw java.lang.IncompatibleClassChangeError |  Major | . | Mariusz Kryński | Mariusz Kryński |
| [CASSANDRA-7691](https://issues.apache.org/jira/browse/CASSANDRA-7691) | Match error message with "tombstone\_failure\_threshold" config parameter |  Trivial | . | Nikita Vetoshkin | Nikita Vetoshkin |
| [CASSANDRA-7709](https://issues.apache.org/jira/browse/CASSANDRA-7709) | sstable2json has resource leaks |  Minor | Tools | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-7694](https://issues.apache.org/jira/browse/CASSANDRA-7694) | Expected Compaction Interruption is logged as ERROR |  Minor | Compaction | Philip Thompson | Marcus Eriksson |
| [CASSANDRA-6612](https://issues.apache.org/jira/browse/CASSANDRA-6612) | Query failing due to AssertionError |  Major | . | A Verma | Sylvain Lebresne |
| [CASSANDRA-7570](https://issues.apache.org/jira/browse/CASSANDRA-7570) | CqlPagingRecordReader is broken |  Major | . | Brandon Williams | Alex Liu |
| [CASSANDRA-7707](https://issues.apache.org/jira/browse/CASSANDRA-7707) | blobAs() function results not validated |  Critical | CQL | Tyler Hobbs | Sylvain Lebresne |
| [CASSANDRA-7700](https://issues.apache.org/jira/browse/CASSANDRA-7700) | Unexpected exception in RangeTombstoneList.insertFrom during Python Driver Duration Test |  Major | . | Rick Smith | Sylvain Lebresne |
| [CASSANDRA-7733](https://issues.apache.org/jira/browse/CASSANDRA-7733) | Fix supercolumn range deletion from Thrift (+ a few tests) |  Trivial | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-7730](https://issues.apache.org/jira/browse/CASSANDRA-7730) | altering a table to add a static column bypasses clustering column requirement check |  Major | . | Tupshin Harper | Sylvain Lebresne |
| [CASSANDRA-6276](https://issues.apache.org/jira/browse/CASSANDRA-6276) | CQL: Map can not be created with the same name as a previously dropped list |  Minor | . | Oli Schacher | Benjamin Lerer |
| [CASSANDRA-7750](https://issues.apache.org/jira/browse/CASSANDRA-7750) | Do not flush on truncate if "durable\_writes" is false. |  Minor | . | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-7752](https://issues.apache.org/jira/browse/CASSANDRA-7752) | Fix expiring map time for CAS messages |  Minor | . | sankalp kohli | sankalp kohli |
| [CASSANDRA-7478](https://issues.apache.org/jira/browse/CASSANDRA-7478) | StorageService.getJoiningNodes returns duplicate ips |  Major | . | Nick Bailey | Jonathan Ellis |
| [CASSANDRA-7745](https://issues.apache.org/jira/browse/CASSANDRA-7745) | Background LCS compactions stall with pending compactions remaining |  Minor | Compaction | J.B. Langston | Yuki Morishita |
| [CASSANDRA-7756](https://issues.apache.org/jira/browse/CASSANDRA-7756) | NullPointerException in getTotalBufferSize |  Major | Local Write-Read Paths | Leonid Shalupov | T Jake Luciani |
| [CASSANDRA-7585](https://issues.apache.org/jira/browse/CASSANDRA-7585) | cassandra sstableloader connection refused with inter\_node\_encryption |  Major | Tools | Samphel Norden | Yuki Morishita |
| [CASSANDRA-7758](https://issues.apache.org/jira/browse/CASSANDRA-7758) | Some gossip messages are very slow to process on vnode clusters |  Major | . | Rick Branson | Rick Branson |
| [CASSANDRA-7222](https://issues.apache.org/jira/browse/CASSANDRA-7222) | cqlsh does not wait for tracing to complete before printing |  Major | Tools | Tyler Hobbs | Mikhail Stepura |
| [CASSANDRA-7252](https://issues.apache.org/jira/browse/CASSANDRA-7252) | RingCache cannot be configured to use local DC only |  Major | . | Robbie Strickland | Robbie Strickland |
| [CASSANDRA-7499](https://issues.apache.org/jira/browse/CASSANDRA-7499) | Unable to update list element by index using CAS condition |  Major | CQL | DOAN DuyHai | Sylvain Lebresne |
| [CASSANDRA-7744](https://issues.apache.org/jira/browse/CASSANDRA-7744) | Dropping the last collection column turns CompoundSparseCellNameType$WithCollection into CompoundDenseCellNameType |  Major | . | Aleksey Yeschenko | Sylvain Lebresne |
| [CASSANDRA-7560](https://issues.apache.org/jira/browse/CASSANDRA-7560) | 'nodetool repair -pr' leads to indefinitely hanging AntiEntropySession |  Major | . | Vladimir Avram | Yuki Morishita |
| [CASSANDRA-7787](https://issues.apache.org/jira/browse/CASSANDRA-7787) | 2i index indexing the cql3 row marker throws NPE |  Minor | . | Berenguer Blasi | Berenguer Blasi |
| [CASSANDRA-7506](https://issues.apache.org/jira/browse/CASSANDRA-7506) | querying secondary index using complete collection should warn/error |  Major | Secondary Indexes | Russ Hatch | Tyler Hobbs |
| [CASSANDRA-7798](https://issues.apache.org/jira/browse/CASSANDRA-7798) | Empty clustering column not caught for CQL3 update to compact storage counter table |  Major | . | Jeremiah Jordan | Aleksey Yeschenko |
| [CASSANDRA-7664](https://issues.apache.org/jira/browse/CASSANDRA-7664) | IndexOutOfBoundsException thrown during repair |  Major | . | ZhongYu | Marcus Eriksson |
| [CASSANDRA-7796](https://issues.apache.org/jira/browse/CASSANDRA-7796) | Stop inheriting liveRatio/computedAt from previous memtables |  Minor | . | Aleksey Yeschenko | Aleksey Yeschenko |
| [CASSANDRA-7795](https://issues.apache.org/jira/browse/CASSANDRA-7795) | Make StreamReceiveTask thread safe and GC friendly |  Minor | Streaming and Messaging | Yuki Morishita | Yuki Morishita |
| [CASSANDRA-7797](https://issues.apache.org/jira/browse/CASSANDRA-7797) | Cannot alter ReversedType(DateType) clustering column to ReversedType(TimestampType) |  Major | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-7774](https://issues.apache.org/jira/browse/CASSANDRA-7774) | CqlRecordReader creates wrong cluster if the first replica of a split is down |  Major | . | Jacek Lewandowski | Jacek Lewandowski |
| [CASSANDRA-7600](https://issues.apache.org/jira/browse/CASSANDRA-7600) | Schema change notifications sent for no-op DDL statements |  Minor | . | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-7332](https://issues.apache.org/jira/browse/CASSANDRA-7332) | NPE in SecondaryIndexManager.deleteFromIndexes |  Minor | . | Duncan Sands | Ryan McGuire |
| [CASSANDRA-7601](https://issues.apache.org/jira/browse/CASSANDRA-7601) | Data loss after nodetool taketoken |  Minor | Testing | Philip Thompson | Brandon Williams |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7011](https://issues.apache.org/jira/browse/CASSANDRA-7011) | auth\_test system\_auth\_ks\_is\_alterable\_test dtest hangs in 2.1 and 2.0 |  Major | Testing | Michael Shuler | Philip Thompson |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-7703](https://issues.apache.org/jira/browse/CASSANDRA-7703) | Fix failing cqlsh tests |  Major | Testing | Tyler Hobbs | Tyler Hobbs |


