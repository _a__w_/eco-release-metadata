
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.5 - 2010-01-24



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-363](https://issues.apache.org/jira/browse/CASSANDRA-363) | Reenable/Make TokenUpdateVerbHandler work so that InitialToken can be set |  Major | . | Sammy Yu | Sammy Yu |
| [CASSANDRA-422](https://issues.apache.org/jira/browse/CASSANDRA-422) | Add example of EndPointSnitch to contrib |  Major | . | Sammy Yu | Sammy Yu |
| [CASSANDRA-442](https://issues.apache.org/jira/browse/CASSANDRA-442) | add reads to stress.py |  Minor | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-447](https://issues.apache.org/jira/browse/CASSANDRA-447) | Ability to temporary set minimum and maximum compaction threshold |  Major | . | Sammy Yu | Sammy Yu |
| [CASSANDRA-412](https://issues.apache.org/jira/browse/CASSANDRA-412) | Add version number validation between Cassandra and Thrift |  Minor | . | Lance Weber | Jonathan Ellis |
| [CASSANDRA-426](https://issues.apache.org/jira/browse/CASSANDRA-426) | option (default to false) to snapshot sstables before each compaction |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-482](https://issues.apache.org/jira/browse/CASSANDRA-482) | Expose out additional metrics for the thread pools: active threads, pool size, and completed tasks |  Major | . | Sammy Yu | Sammy Yu |
| [CASSANDRA-489](https://issues.apache.org/jira/browse/CASSANDRA-489) | SSTable export tool |  Minor | Tools | Eric Evans | Eric Evans |
| [CASSANDRA-423](https://issues.apache.org/jira/browse/CASSANDRA-423) | add new LRU cache |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-505](https://issues.apache.org/jira/browse/CASSANDRA-505) | turn nodeprobe flush\_binary into nodeprobe flush |  Major | Tools | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-492](https://issues.apache.org/jira/browse/CASSANDRA-492) | Data Center Quorum |  Minor | . | Vijay | Vijay |
| [CASSANDRA-517](https://issues.apache.org/jira/browse/CASSANDRA-517) | have bootstrap token-selector consider predicted load |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-435](https://issues.apache.org/jira/browse/CASSANDRA-435) | unbootstrap |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-541](https://issues.apache.org/jira/browse/CASSANDRA-541) | allow moving a live node with data on it |  Major | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-549](https://issues.apache.org/jira/browse/CASSANDRA-549) | Windows start script for cli client |  Minor | Tools | Dirk Weber | Dirk Weber |
| [CASSANDRA-192](https://issues.apache.org/jira/browse/CASSANDRA-192) | Load balancing |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-544](https://issues.apache.org/jira/browse/CASSANDRA-544) | add "clear pending ranges" to jmx |  Minor | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-344](https://issues.apache.org/jira/browse/CASSANDRA-344) | allow slicing during key range query |  Major | . | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-193](https://issues.apache.org/jira/browse/CASSANDRA-193) | Proactive repair |  Major | . | Jonathan Ellis | Stu Hood |
| [CASSANDRA-520](https://issues.apache.org/jira/browse/CASSANDRA-520) | Implement Range Repairs |  Major | . | Stu Hood | Stu Hood |
| [CASSANDRA-567](https://issues.apache.org/jira/browse/CASSANDRA-567) | add querying of supercolumns to commandline, or replace it |  Minor | Tools | Jonathan Ellis | Eric Evans |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-375](https://issues.apache.org/jira/browse/CASSANDRA-375) | Echo writes to bootstrapping nodes |  Major | . | Sandeep Tata | Sandeep Tata |
| [CASSANDRA-416](https://issues.apache.org/jira/browse/CASSANDRA-416) | modify cassandra.in.sh to only set $CASSANDRA\_CONF if it is not already set |  Trivial | . | Lance Weber | Eric Evans |
| [CASSANDRA-424](https://issues.apache.org/jira/browse/CASSANDRA-424) | remove deprecated SSTR.get calls |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-414](https://issues.apache.org/jira/browse/CASSANDRA-414) | remove sstableLock |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-420](https://issues.apache.org/jira/browse/CASSANDRA-420) | Improve performance of BinaryMemtable sort phase |  Minor | . | Johan Oskarsson | Johan Oskarsson |
| [CASSANDRA-444](https://issues.apache.org/jira/browse/CASSANDRA-444) | improve throughput for normal inserts on many-core systems |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-433](https://issues.apache.org/jira/browse/CASSANDRA-433) | Remove item flush limit in BinaryMemtable |  Major | . | Johan Oskarsson | Chris Goffinet |
| [CASSANDRA-401](https://issues.apache.org/jira/browse/CASSANDRA-401) | Less crappy failure mode when swamped with inserts than "run out of memory and gc-storm to death" |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-456](https://issues.apache.org/jira/browse/CASSANDRA-456) | CommitLog should log RowMutations, not Rows |  Minor | . | Sandeep Tata | Sandeep Tata |
| [CASSANDRA-437](https://issues.apache.org/jira/browse/CASSANDRA-437) | record token/endpoint (ip) pairs in system table |  Major | . | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-464](https://issues.apache.org/jira/browse/CASSANDRA-464) | Cassandra-cli framed server transport option |  Trivial | Tools | Dan Di Spaltro | Dan Di Spaltro |
| [CASSANDRA-446](https://issues.apache.org/jira/browse/CASSANDRA-446) | Use DecoratedKey objects in Memtable, SSTableReader/Writer objects |  Major | . | Jonathan Ellis | Johan Oskarsson |
| [CASSANDRA-457](https://issues.apache.org/jira/browse/CASSANDRA-457) | web-based Cassandra browser |  Major | . | Jun Rao | Hernan Badenes |
| [CASSANDRA-438](https://issues.apache.org/jira/browse/CASSANDRA-438) | autoboostrap |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-374](https://issues.apache.org/jira/browse/CASSANDRA-374) | Startup script should pass extra command line parameters to the JVM |  Minor | Tools | Sandeep Tata | Eric Evans |
| [CASSANDRA-498](https://issues.apache.org/jira/browse/CASSANDRA-498) | Replace EndPoint class with InetAddress |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-504](https://issues.apache.org/jira/browse/CASSANDRA-504) | Reduce GC overhead |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-252](https://issues.apache.org/jira/browse/CASSANDRA-252) | Auto-ring-verification for nodeprobe |  Major | Tools | Jonathan Ellis | Eric Evans |
| [CASSANDRA-483](https://issues.apache.org/jira/browse/CASSANDRA-483) | clean up bootstrap code, 2 |  Major | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-514](https://issues.apache.org/jira/browse/CASSANDRA-514) | stress.py fixes and improvements |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-513](https://issues.apache.org/jira/browse/CASSANDRA-513) | bootstrapper should consult the replicationstrategy for determining which replicas to request data from |  Major | . | Jonathan Ellis | Vijay |
| [CASSANDRA-350](https://issues.apache.org/jira/browse/CASSANDRA-350) | optimize away unnecessary range scans |  Major | . | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-510](https://issues.apache.org/jira/browse/CASSANDRA-510) | reading from large supercolumns is excessively slow |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-527](https://issues.apache.org/jira/browse/CASSANDRA-527) | clean up gossip notifications to the rest of the system |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-488](https://issues.apache.org/jira/browse/CASSANDRA-488) | TcpConnectionManager only ever has one connection |  Minor | . | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-539](https://issues.apache.org/jira/browse/CASSANDRA-539) | thread flushes from recoverymanager |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-542](https://issues.apache.org/jira/browse/CASSANDRA-542) | stress.py enhancements |  Minor | . | Scott White | Scott White |
| [CASSANDRA-540](https://issues.apache.org/jira/browse/CASSANDRA-540) | Commit log replays should be multithreaded |  Minor | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-558](https://issues.apache.org/jira/browse/CASSANDRA-558) | optimize local writes |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-563](https://issues.apache.org/jira/browse/CASSANDRA-563) | expose TimeoutException to Thrift |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-248](https://issues.apache.org/jira/browse/CASSANDRA-248) | Replace factory method based on explicit locks for a more succint syntax |  Major | . | Edward Ribeiro | Gary Dusbabek |
| [CASSANDRA-574](https://issues.apache.org/jira/browse/CASSANDRA-574) | loadbalance should first leave and then determine new token |  Minor | . | Jaakko Laine | Jaakko Laine |
| [CASSANDRA-589](https://issues.apache.org/jira/browse/CASSANDRA-589) | cassandra/bin scripts - do not assume JAVA\_HOME would be set in production environments |  Minor | . | Karthik K | Karthik K |
| [CASSANDRA-590](https://issues.apache.org/jira/browse/CASSANDRA-590) | cassandra/bin scripts - JAVA\_HOME injection - json2sstable / sstable2json / nodeprobe |  Minor | . | Karthik K | Karthik K |
| [CASSANDRA-594](https://issues.apache.org/jira/browse/CASSANDRA-594) | Add "count" and "del" commands to CLI |  Major | Tools | gabriele renzi | gabriele renzi |
| [CASSANDRA-597](https://issues.apache.org/jira/browse/CASSANDRA-597) | remove "heartbeat" from HeartBeatState |  Minor | . | Jaakko Laine | Jaakko Laine |
| [CASSANDRA-568](https://issues.apache.org/jira/browse/CASSANDRA-568) | make consistencylevel in get\_range\_slice meaningful |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-324](https://issues.apache.org/jira/browse/CASSANDRA-324) | Add documentation to Thrift interface |  Minor | Documentation and Website | Michael Greene | Gary Dusbabek |
| [CASSANDRA-584](https://issues.apache.org/jira/browse/CASSANDRA-584) | add read support for ConsistencyLevel.ALL |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-613](https://issues.apache.org/jira/browse/CASSANDRA-613) | add super column support for delete and count to cli |  Minor | Tools | Eric Evans | Eric Evans |
| [CASSANDRA-564](https://issues.apache.org/jira/browse/CASSANDRA-564) | Provide recoverability when a node dies and it is impossible to get the same IP. |  Minor | . | Anthony Molinaro | Jaakko Laine |
| [CASSANDRA-621](https://issues.apache.org/jira/browse/CASSANDRA-621) | GC compacted sstables as part of cleanup, compaction |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-619](https://issues.apache.org/jira/browse/CASSANDRA-619) | sstable2json makes assumption for keyspace |  Trivial | Tools | Chris Goffinet | Eric Evans |
| [CASSANDRA-652](https://issues.apache.org/jira/browse/CASSANDRA-652) | RowMutations are being serialized for the commit log during commit log recovery.  This is wasteul. |  Minor | . | Gary Dusbabek | Gary Dusbabek |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-410](https://issues.apache.org/jira/browse/CASSANDRA-410) | flexjson.jar -\> flexjson-1.7.jar |  Minor | . | Eric Evans | Eric Evans |
| [CASSANDRA-411](https://issues.apache.org/jira/browse/CASSANDRA-411) | NOTICE.txt out of data for third-party libs |  Major | . | Eric Evans | Eric Evans |
| [CASSANDRA-413](https://issues.apache.org/jira/browse/CASSANDRA-413) | clean up SSTR.open/get and r/m unnecessary synchronization |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-421](https://issues.apache.org/jira/browse/CASSANDRA-421) | nodeprobe outputs incorrectly ordered ring |  Minor | Tools | Eric Evans | Eric Evans |
| [CASSANDRA-425](https://issues.apache.org/jira/browse/CASSANDRA-425) | AssertionError during initial compaction |  Critical | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-429](https://issues.apache.org/jira/browse/CASSANDRA-429) | more missing svn properties |  Minor | . | Eric Evans | Eric Evans |
| [CASSANDRA-415](https://issues.apache.org/jira/browse/CASSANDRA-415) | NOTICE should not reference pom.xml for dev list |  Minor | Documentation and Website | Eric Evans | Eric Evans |
| [CASSANDRA-428](https://issues.apache.org/jira/browse/CASSANDRA-428) | source for (unreleased )commons javaflow jar |  Minor | . | Eric Evans | Eric Evans |
| [CASSANDRA-371](https://issues.apache.org/jira/browse/CASSANDRA-371) | LICENSE.txt should mention lib/licenses |  Major | . | Eric Evans | Eric Evans |
| [CASSANDRA-427](https://issues.apache.org/jira/browse/CASSANDRA-427) | ReadResponseResolver logs resolve data to info, should be debug |  Trivial | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-431](https://issues.apache.org/jira/browse/CASSANDRA-431) | anticompaction return value is broken |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-440](https://issues.apache.org/jira/browse/CASSANDRA-440) | get\_key\_range problems when a node is down |  Major | . | Simon Smith | Jonathan Ellis |
| [CASSANDRA-448](https://issues.apache.org/jira/browse/CASSANDRA-448) | README.txt inaccuracies |  Minor | Documentation and Website | Eric Evans | Paul Querna |
| [CASSANDRA-436](https://issues.apache.org/jira/browse/CASSANDRA-436) | OOM during major compaction on many (hundreds) of sstables |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-445](https://issues.apache.org/jira/browse/CASSANDRA-445) | commitlog may consider writes flushed, that are not yet |  Critical | . | Jonathan Ellis | Jun Rao |
| [CASSANDRA-449](https://issues.apache.org/jira/browse/CASSANDRA-449) | Update Binary Memtable Example work with getReadStorageEndPoints |  Minor | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-454](https://issues.apache.org/jira/browse/CASSANDRA-454) | assertion fails in CFS.forceAntiCompaction |  Major | . | Jun Rao | Jonathan Ellis |
| [CASSANDRA-458](https://issues.apache.org/jira/browse/CASSANDRA-458) | Null pointer exception in doIndexing(ColumnIndexer.java:142) |  Major | . | Teodor Sigaev | Jonathan Ellis |
| [CASSANDRA-460](https://issues.apache.org/jira/browse/CASSANDRA-460) | java.lang.NegativeArraySizeException being thrown for large column names |  Major | . | T Jake Luciani | Jonathan Ellis |
| [CASSANDRA-455](https://issues.apache.org/jira/browse/CASSANDRA-455) | DebuggableScheduledThreadPoolExecutor only schedules a task once |  Major | . | Jun Rao | Jonathan Ellis |
| [CASSANDRA-459](https://issues.apache.org/jira/browse/CASSANDRA-459) | Commitlog segments don't get deleted |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-461](https://issues.apache.org/jira/browse/CASSANDRA-461) | Outdated comment in default storage-conf.xml |  Trivial | Documentation and Website | Lars Francke | Jonathan Ellis |
| [CASSANDRA-467](https://issues.apache.org/jira/browse/CASSANDRA-467) | nodeprobe flush\_binary using Order Preserving Partitioner |  Minor | . | Chris Goffinet | Jonathan Ellis |
| [CASSANDRA-471](https://issues.apache.org/jira/browse/CASSANDRA-471) | Submit Flush is Failing with a RejectedExecutionException |  Major | Tools | Dan Di Spaltro | Jonathan Ellis |
| [CASSANDRA-385](https://issues.apache.org/jira/browse/CASSANDRA-385) | intellibootstrap |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-465](https://issues.apache.org/jira/browse/CASSANDRA-465) | nodeprobe setcompactionthreshold \<Max\> does not allow zero |  Minor | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-479](https://issues.apache.org/jira/browse/CASSANDRA-479) | Fail startup if \<seeds\> is empty |  Major | . | Jonathan Ellis | Sammy Yu |
| [CASSANDRA-481](https://issues.apache.org/jira/browse/CASSANDRA-481) | describe\_keyspace fails on the system table |  Minor | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-480](https://issues.apache.org/jira/browse/CASSANDRA-480) | Remove "cluster" command in nodeprobe usage |  Minor | Tools | Sammy Yu | Sammy Yu |
| [CASSANDRA-430](https://issues.apache.org/jira/browse/CASSANDRA-430) | pom.xml out of date |  Minor | Tools | Eric Evans | Niall Pemberton |
| [CASSANDRA-490](https://issues.apache.org/jira/browse/CASSANDRA-490) | FBUtilities.bytesToHex and FBUtilities.hexToBytes are not inverses |  Major | . | Sandeep Tata | Sandeep Tata |
| [CASSANDRA-495](https://issues.apache.org/jira/browse/CASSANDRA-495) | Token Ranges Do Not Wrap Correctly |  Major | . | Stu Hood | Stu Hood |
| [CASSANDRA-491](https://issues.apache.org/jira/browse/CASSANDRA-491) | Hinted HandOff doesn't work for SuperColumnFamilies |  Major | . | Sammy Yu | Jonathan Ellis |
| [CASSANDRA-496](https://issues.apache.org/jira/browse/CASSANDRA-496) | hinted handoff will give up if there are not unique nodes to store each hint |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-500](https://issues.apache.org/jira/browse/CASSANDRA-500) | Compaction regression doesn't delete tombstones in trunk |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-497](https://issues.apache.org/jira/browse/CASSANDRA-497) | Include bootstrap targets in consistencylevel computations |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-503](https://issues.apache.org/jira/browse/CASSANDRA-503) | RecoveryManager should ignore hidden files (like .DS\_Store) created by some operating systems. |  Trivial | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-507](https://issues.apache.org/jira/browse/CASSANDRA-507) | Tombstone records in Cassandra are not being deleted |  Major | . | Ramzi Rabah | Jonathan Ellis |
| [CASSANDRA-512](https://issues.apache.org/jira/browse/CASSANDRA-512) | regression prevents recognizing local reads |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-515](https://issues.apache.org/jira/browse/CASSANDRA-515) | Gossiper misses first updates when restarting a node |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-524](https://issues.apache.org/jira/browse/CASSANDRA-524) | can't write with consistency level of one after some nodes fail |  Major | . | Edmond Lau | Jonathan Ellis |
| [CASSANDRA-523](https://issues.apache.org/jira/browse/CASSANDRA-523) | Add updateForeignTokenUnsafe for BMT imports to work properly |  Major | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-432](https://issues.apache.org/jira/browse/CASSANDRA-432) | possible to have orphaned data files that never get deleted |  Minor | . | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-522](https://issues.apache.org/jira/browse/CASSANDRA-522) | NPE heisenbug when starting entire cluster (for first time) with autobootstrap=true |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-528](https://issues.apache.org/jira/browse/CASSANDRA-528) | clhm licensing and attribution notices should match current practice |  Minor | . | Eric Evans | Eric Evans |
| [CASSANDRA-533](https://issues.apache.org/jira/browse/CASSANDRA-533) | Need to close files in loadBloomFilter and loadIndexFile |  Major | . | Tim Freeman | Tim Freeman |
| [CASSANDRA-529](https://issues.apache.org/jira/browse/CASSANDRA-529) | Read Repair throws UnknownHostException |  Major | . | Sammy Yu | Sammy Yu |
| [CASSANDRA-316](https://issues.apache.org/jira/browse/CASSANDRA-316) | antrl generated files should not be included in the source release |  Major | Tools | Eric Evans | Eric Evans |
| [CASSANDRA-452](https://issues.apache.org/jira/browse/CASSANDRA-452) | Corrupt SSTable |  Major | . | Sammy Yu | Jonathan Ellis |
| [CASSANDRA-532](https://issues.apache.org/jira/browse/CASSANDRA-532) | Flush creates empty SSTables if nothing exists in that CF |  Major | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-536](https://issues.apache.org/jira/browse/CASSANDRA-536) | New node is attempting to bootstrap to itself |  Major | . | Ray Slakinski | Jonathan Ellis |
| [CASSANDRA-538](https://issues.apache.org/jira/browse/CASSANDRA-538) | when streaming sstables to other nodes, make sure they don't get compacted before they are streamed |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-546](https://issues.apache.org/jira/browse/CASSANDRA-546) | Streaming error on bootstrap |  Major | . | Ray Slakinski |  |
| [CASSANDRA-550](https://issues.apache.org/jira/browse/CASSANDRA-550) | clean up better after streaming |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-551](https://issues.apache.org/jira/browse/CASSANDRA-551) | When you omit keyspace in the "show keyspace" command in the CLI your connection gets terminated |  Minor | . | Hafsteinn Baldvinsson | Hafsteinn Baldvinsson |
| [CASSANDRA-552](https://issues.apache.org/jira/browse/CASSANDRA-552) | file descriptor leak in getKeyRange |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-555](https://issues.apache.org/jira/browse/CASSANDRA-555) | Multiget looks up keys locally serially |  Major | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-557](https://issues.apache.org/jira/browse/CASSANDRA-557) | typo in conf/storage-conf.xml: BinaryMemtableSizeInMB is defined twice |  Minor | . | TuxRacer | Eric Evans |
| [CASSANDRA-548](https://issues.apache.org/jira/browse/CASSANDRA-548) | Make sure application states are delivered in correct order |  Major | . | Jaakko Laine | Jaakko Laine |
| [CASSANDRA-560](https://issues.apache.org/jira/browse/CASSANDRA-560) | remove deleted endpoints from hinted CF |  Major | . | Jaakko Laine | Jaakko Laine |
| [CASSANDRA-565](https://issues.apache.org/jira/browse/CASSANDRA-565) | BufferedRandomAccessFile.read doesn't always do full reads |  Major | . | Jun Rao | Jun Rao |
| [CASSANDRA-566](https://issues.apache.org/jira/browse/CASSANDRA-566) | RingCache code breaks |  Major | . | Jun Rao | Jun Rao |
| [CASSANDRA-575](https://issues.apache.org/jira/browse/CASSANDRA-575) | wait for bootstrap gossip to propagate |  Major | . | Jaakko Laine | Jaakko Laine |
| [CASSANDRA-573](https://issues.apache.org/jira/browse/CASSANDRA-573) | handle empty unbootstrap ranges |  Major | . | Jaakko Laine | Jaakko Laine |
| [CASSANDRA-578](https://issues.apache.org/jira/browse/CASSANDRA-578) | get\_range\_slice NPE |  Major | . | Dan Di Spaltro | Jonathan Ellis |
| [CASSANDRA-150](https://issues.apache.org/jira/browse/CASSANDRA-150) | multiple seeds (only when seed count = node count?) can cause cluster partition |  Minor | . | Jonathan Ellis | Jaakko Laine |
| [CASSANDRA-576](https://issues.apache.org/jira/browse/CASSANDRA-576) | Expected both token and generation columns |  Major | . | Dan Di Spaltro | Jonathan Ellis |
| [CASSANDRA-588](https://issues.apache.org/jira/browse/CASSANDRA-588) | Maven build broken because it does not include all local dependencies |  Major | . | Adam Fisk | Adam Fisk |
| [CASSANDRA-591](https://issues.apache.org/jira/browse/CASSANDRA-591) | remove unused setters from DatabaseDescriptor |  Minor | . | Eric Evans | Eric Evans |
| [CASSANDRA-554](https://issues.apache.org/jira/browse/CASSANDRA-554) | don't count temporary to-stream files towards system load |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-499](https://issues.apache.org/jira/browse/CASSANDRA-499) | SSTable import tool |  Minor | Tools | Eric Evans | Eric Evans |
| [CASSANDRA-593](https://issues.apache.org/jira/browse/CASSANDRA-593) | Assertion error on quorum write |  Minor | . | Dan Di Spaltro | Dan Di Spaltro |
| [CASSANDRA-583](https://issues.apache.org/jira/browse/CASSANDRA-583) | when a supercolumn is marked deleted, need to check for newer data in subcolumns |  Critical | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-602](https://issues.apache.org/jira/browse/CASSANDRA-602) | bytesToHex broken for small byte values |  Blocker | . | Stu Hood | Stu Hood |
| [CASSANDRA-581](https://issues.apache.org/jira/browse/CASSANDRA-581) | RandomPartitioner convertFromDiskFormat is slow |  Minor | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-606](https://issues.apache.org/jira/browse/CASSANDRA-606) | Compaction can't find files |  Major | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-519](https://issues.apache.org/jira/browse/CASSANDRA-519) | bug in CollatedOrderPreservingPartitioner.midpoint |  Minor | . | Jonathan Ellis | Stu Hood |
| [CASSANDRA-604](https://issues.apache.org/jira/browse/CASSANDRA-604) | Compactions might remove tombstones without removing the actual data |  Major | . | Ramzi Rabah | Jonathan Ellis |
| [CASSANDRA-610](https://issues.apache.org/jira/browse/CASSANDRA-610) | Gossip conviction threshold too low |  Major | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-609](https://issues.apache.org/jira/browse/CASSANDRA-609) | Easy to OOM on log replay since memtable limits are ignored |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-616](https://issues.apache.org/jira/browse/CASSANDRA-616) | Anti-entropy might attempt to repair the system table |  Blocker | . | Stu Hood |  |
| [CASSANDRA-623](https://issues.apache.org/jira/browse/CASSANDRA-623) | NullPointer during a get\_range\_slice |  Major | . | Dan Di Spaltro | Jonathan Ellis |
| [CASSANDRA-605](https://issues.apache.org/jira/browse/CASSANDRA-605) | Corruption in CommitLog |  Major | . | Chris Goffinet | Jonathan Ellis |
| [CASSANDRA-630](https://issues.apache.org/jira/browse/CASSANDRA-630) | streaming fails on windows because file is not closed before it is renamed |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-631](https://issues.apache.org/jira/browse/CASSANDRA-631) | possible NPE in StorageProxy? |  Major | . | gabriele renzi | gabriele renzi |
| [CASSANDRA-618](https://issues.apache.org/jira/browse/CASSANDRA-618) | json2sstable/sstable2json don't export/import correct column names when the column family is of BytesType ordering |  Minor | Tools | Ramzi Rabah | Eric Evans |
| [CASSANDRA-629](https://issues.apache.org/jira/browse/CASSANDRA-629) | antientropyservice patchset nearly doubles compaction time |  Major | . | Jonathan Ellis | Stu Hood |
| [CASSANDRA-603](https://issues.apache.org/jira/browse/CASSANDRA-603) | pending range collision between nodes |  Major | . | Chris Goffinet | Jaakko Laine |
| [CASSANDRA-639](https://issues.apache.org/jira/browse/CASSANDRA-639) | Assertion failure in MerkleTree |  Minor | . | Gary Dusbabek | Stu Hood |
| [CASSANDRA-636](https://issues.apache.org/jira/browse/CASSANDRA-636) | SSTableImportTest is looking for test resources that are not available via the pom |  Major | . | Nate McCall | Nate McCall |
| [CASSANDRA-640](https://issues.apache.org/jira/browse/CASSANDRA-640) | Repair should never reuse a tree |  Major | . | Stu Hood | Stu Hood |
| [CASSANDRA-572](https://issues.apache.org/jira/browse/CASSANDRA-572) | handle old gossip properly |  Major | . | Jaakko Laine | Jaakko Laine |
| [CASSANDRA-643](https://issues.apache.org/jira/browse/CASSANDRA-643) | NullPointerException when calling get\_range\_slice() |  Major | . | Jonathan Hseu | Jonathan Ellis |
| [CASSANDRA-648](https://issues.apache.org/jira/browse/CASSANDRA-648) | findSuitableEndPoint not returning closest endPoint |  Major | . | Chris Goffinet | Jonathan Ellis |
| [CASSANDRA-650](https://issues.apache.org/jira/browse/CASSANDRA-650) | InstanceAlreadyExistsException |  Major | . | Chris Goffinet | Jonathan Ellis |
| [CASSANDRA-634](https://issues.apache.org/jira/browse/CASSANDRA-634) | Hinted Handoff Exception |  Major | . | Chris Goffinet | Jaakko Laine |
| [CASSANDRA-647](https://issues.apache.org/jira/browse/CASSANDRA-647) | get\_range\_slice() returns removed columns |  Minor | . | Jonathan Hseu | Jonathan Ellis |
| [CASSANDRA-649](https://issues.apache.org/jira/browse/CASSANDRA-649) | get\_range\_slice() behavior is inconsistent with get\_slice() and multiget\_slice() when super\_column is set in ColumnParent |  Major | . | Jonathan Hseu | Jonathan Ellis |
| [CASSANDRA-655](https://issues.apache.org/jira/browse/CASSANDRA-655) | need serialVersionUID for Token |  Minor | . | Eric Evans | Eric Evans |
| [CASSANDRA-624](https://issues.apache.org/jira/browse/CASSANDRA-624) | "maven install" does not find libthrift-r808609.jar |  Minor | Tools | Christophe Pierret |  |
| [CASSANDRA-651](https://issues.apache.org/jira/browse/CASSANDRA-651) | cassandra 0.5 version throttles and sometimes kills traffic to a node if you restart it. |  Major | . | Ramzi Rabah | Gary Dusbabek |
| [CASSANDRA-662](https://issues.apache.org/jira/browse/CASSANDRA-662) | RowMutation applied twice in RowMutationVerbHandler |  Major | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-637](https://issues.apache.org/jira/browse/CASSANDRA-637) | CliClient contains an unused, unavailable import |  Major | . | Nate McCall | Nate McCall |
| [CASSANDRA-663](https://issues.apache.org/jira/browse/CASSANDRA-663) | Abort bootstrap if our IP is already in the token ring |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-668](https://issues.apache.org/jira/browse/CASSANDRA-668) | long commitlog syncs can cause write pauses |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-681](https://issues.apache.org/jira/browse/CASSANDRA-681) | Error deleting files during bootstrap |  Minor | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-646](https://issues.apache.org/jira/browse/CASSANDRA-646) | Fix few minor problems in nodeprobe cfstats |  Minor | Tools | Ramzi Rabah | Jonathan Ellis |
| [CASSANDRA-696](https://issues.apache.org/jira/browse/CASSANDRA-696) | Bootstrapping doesn't work on new clusters |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-694](https://issues.apache.org/jira/browse/CASSANDRA-694) | Failure to flush commit log |  Major | . | Ryan Daum | Jonathan Ellis |
| [CASSANDRA-673](https://issues.apache.org/jira/browse/CASSANDRA-673) | bootstrapping does not work properly using multiple key space |  Minor | . | Michael Lee | Jaakko Laine |
| [CASSANDRA-660](https://issues.apache.org/jira/browse/CASSANDRA-660) | Update pom.xml for Maven usage |  Minor | Tools | Ryan Daum | Ryan Daum |
| [CASSANDRA-716](https://issues.apache.org/jira/browse/CASSANDRA-716) | bootstrapping does not work properly using multiple DataFileDirectory |  Major | . | david.pan | Gary Dusbabek |
| [CASSANDRA-722](https://issues.apache.org/jira/browse/CASSANDRA-722) | batch insert failing with TokenMetadata AssertionError |  Major | . | Dan Di Spaltro | Jaakko Laine |
| [CASSANDRA-734](https://issues.apache.org/jira/browse/CASSANDRA-734) | Table.open has a broken lock in it |  Minor | . | Jeff Hodges | Jonathan Ellis |
| [CASSANDRA-750](https://issues.apache.org/jira/browse/CASSANDRA-750) | avoid setting up completion handler for no-op stream in non-bootstrap mode |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-703](https://issues.apache.org/jira/browse/CASSANDRA-703) | Insert, Delete and Insert into a column family doesnt work... |  Minor | . | Vijay | Jonathan Ellis |
| [CASSANDRA-729](https://issues.apache.org/jira/browse/CASSANDRA-729) | Bug in count columns. |  Minor | . | Gasol Wu |  |
| [CASSANDRA-762](https://issues.apache.org/jira/browse/CASSANDRA-762) | Load balancing does not account for the load of the moving node |  Minor | . | Stu Hood | Stu Hood |
| [CASSANDRA-731](https://issues.apache.org/jira/browse/CASSANDRA-731) | a few insert operations failed while bootstrapping |  Major | . | david.pan | Jaakko Laine |
| [CASSANDRA-790](https://issues.apache.org/jira/browse/CASSANDRA-790) | SSTables limited to (2^31)/15 keys |  Blocker | . | Stu Hood | Stu Hood |
| [CASSANDRA-795](https://issues.apache.org/jira/browse/CASSANDRA-795) | Streaming broken on windows (FileStreamTask.CHUNK\_SIZE is too big). |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-800](https://issues.apache.org/jira/browse/CASSANDRA-800) | Spurious Gossip Up/Down and IO Errors |  Major | . | Ryan King |  |
| [CASSANDRA-781](https://issues.apache.org/jira/browse/CASSANDRA-781) | in a cluster, get\_range\_slice() does not return all the keys it should |  Major | . | bjc | Jonathan Ellis |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-485](https://issues.apache.org/jira/browse/CASSANDRA-485) | Add ability to bootstrap a node from 0.4 |  Major | . | Chris Goffinet | Jonathan Ellis |
| [CASSANDRA-466](https://issues.apache.org/jira/browse/CASSANDRA-466) | skip corrupt rows during compaction instead of erroring out and aborting |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-525](https://issues.apache.org/jira/browse/CASSANDRA-525) | use "pending ranges" abstraction in TokenMetadata |  Major | . | Jonathan Ellis | Jonathan Ellis |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-463](https://issues.apache.org/jira/browse/CASSANDRA-463) | add MemtableFlushAfterMinutes, a global replacement for FlushPeriodInMinutes |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-477](https://issues.apache.org/jira/browse/CASSANDRA-477) | clean up bootstrap code |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-441](https://issues.apache.org/jira/browse/CASSANDRA-441) | Verify that quorum reads throw UnavailableException if insufficient live nodes are present |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-516](https://issues.apache.org/jira/browse/CASSANDRA-516) | move BasicUtilities.\* into FBUtilities and remove redundant methods |  Minor | . | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-298](https://issues.apache.org/jira/browse/CASSANDRA-298) | check for common PEBCAKs in startup |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-596](https://issues.apache.org/jira/browse/CASSANDRA-596) | add contrib example of using "fat client" api |  Minor | . | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-577](https://issues.apache.org/jira/browse/CASSANDRA-577) | document upgrade process from 0.4 to 0.5 |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-608](https://issues.apache.org/jira/browse/CASSANDRA-608) | r/m dead code from MerkleTree, AntiEntropyService |  Major | . | Jonathan Ellis | Stu Hood |
| [CASSANDRA-607](https://issues.apache.org/jira/browse/CASSANDRA-607) | implement repair-via-rangecommand |  Major | . | Jonathan Ellis | Stu Hood |


