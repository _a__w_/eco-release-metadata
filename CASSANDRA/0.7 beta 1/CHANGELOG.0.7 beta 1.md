
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.7 beta 1 - 2010-08-13



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-764](https://issues.apache.org/jira/browse/CASSANDRA-764) | bitmasks applied to SliceRange |  Minor | . | Ted Zlatanov | Ted Zlatanov |
| [CASSANDRA-900](https://issues.apache.org/jira/browse/CASSANDRA-900) | access levels for Thrift authorization |  Major | . | Ted Zlatanov | Ted Zlatanov |
| [CASSANDRA-970](https://issues.apache.org/jira/browse/CASSANDRA-970) | Throw NotFoundException when attempting to authenticate to a nonexistent keyspace |  Major | . | Todd Blose | Todd Blose |
| [CASSANDRA-699](https://issues.apache.org/jira/browse/CASSANDRA-699) | Add (optional) expiration time for column |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-968](https://issues.apache.org/jira/browse/CASSANDRA-968) | Add login requirement to hadoop |  Major | . | Todd Blose | Todd Blose |
| [CASSANDRA-1060](https://issues.apache.org/jira/browse/CASSANDRA-1060) | make concurrent\_reads, concurrent\_writes configurable at runtime via JMX |  Trivial | . | Jonathan Ellis | Roger Schildmeijer |
| [CASSANDRA-983](https://issues.apache.org/jira/browse/CASSANDRA-983) | flush and snapshot keyspace/CF before dropping it |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-531](https://issues.apache.org/jira/browse/CASSANDRA-531) | truncate support |  Minor | . | Jonathan Ellis | Ran Tavory |
| [CASSANDRA-1068](https://issues.apache.org/jira/browse/CASSANDRA-1068) | Create a mini admin (JMX) interface per each cassandra node |  Minor | . | Ran Tavory | Ran Tavory |
| [CASSANDRA-1075](https://issues.apache.org/jira/browse/CASSANDRA-1075) | Thrift interface call to query whether all live nodes agree on the schema id |  Minor | . | Benjamin Black | Gary Dusbabek |
| [CASSANDRA-1090](https://issues.apache.org/jira/browse/CASSANDRA-1090) | make it possible to tell when repair has finished |  Major | . | Jonathan Ellis | Stu Hood |
| [CASSANDRA-1099](https://issues.apache.org/jira/browse/CASSANDRA-1099) | make CFS.flushWriter\_ size configurable |  Minor | . | Jonathan Ellis | Brandon Williams |
| [CASSANDRA-1101](https://issues.apache.org/jira/browse/CASSANDRA-1101) | A Hadoop Output Format That Targets Cassandra |  Major | . | Karthick Sankarachary | Karthick Sankarachary |
| [CASSANDRA-1201](https://issues.apache.org/jira/browse/CASSANDRA-1201) | Red Hat files needed for packaging |  Minor | . | Peter Halliday | Peter Halliday |
| [CASSANDRA-1031](https://issues.apache.org/jira/browse/CASSANDRA-1031) | get cassandra version via JMX (previously thrift) |  Minor | . | Lee Li | Matthew F. Dennis |
| [CASSANDRA-1133](https://issues.apache.org/jira/browse/CASSANDRA-1133) | utilities for schema import-from/export-to yaml. |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-1282](https://issues.apache.org/jira/browse/CASSANDRA-1282) | NumericType: comparator for integers of fixed and arbitrary length |  Major | . | Folke Behrens | Folke Behrens |
| [CASSANDRA-749](https://issues.apache.org/jira/browse/CASSANDRA-749) | Secondary indices for column families |  Minor | . | Gary Dusbabek | Jonathan Ellis |
| [CASSANDRA-1004](https://issues.apache.org/jira/browse/CASSANDRA-1004) | Debian packaging should allow a specific user name and optionally create it as "cassandra" on install |  Minor | Packaging | Ted Zlatanov | Eric Evans |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-810](https://issues.apache.org/jira/browse/CASSANDRA-810) | Do away with the streaming directory |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-389](https://issues.apache.org/jira/browse/CASSANDRA-389) | SSTable Versioning |  Minor | . | Chris Goffinet | Stu Hood |
| [CASSANDRA-820](https://issues.apache.org/jira/browse/CASSANDRA-820) | add key counts to stress.py |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-801](https://issues.apache.org/jira/browse/CASSANDRA-801) | The Key Cache should be configurable by absolute numbers |  Major | . | Ryan King | Stu Hood |
| [CASSANDRA-848](https://issues.apache.org/jira/browse/CASSANDRA-848) | get rid of ThriftGlue |  Minor | . | Ted Zlatanov | Ted Zlatanov |
| [CASSANDRA-777](https://issues.apache.org/jira/browse/CASSANDRA-777) | Extract SSTable interfaces to allow for alternative implementations |  Blocker | . | Stu Hood | Stu Hood |
| [CASSANDRA-851](https://issues.apache.org/jira/browse/CASSANDRA-851) | Hashlib instead of md5-lib in stress.py |  Trivial | . | Staffan Ericsson | Staffan Ericsson |
| [CASSANDRA-862](https://issues.apache.org/jira/browse/CASSANDRA-862) | Remove FBUtlities.equals in favour of ObjectUtils.equals |  Trivial | . | gabriele renzi |  |
| [CASSANDRA-625](https://issues.apache.org/jira/browse/CASSANDRA-625) | Migrate to slf4j from log4j in cassandra code |  Minor | . | gabriele renzi |  |
| [CASSANDRA-756](https://issues.apache.org/jira/browse/CASSANDRA-756) | Table.open should not propagate IOException |  Trivial | . | Jonathan Ellis | Rodrigo Peinado |
| [CASSANDRA-905](https://issues.apache.org/jira/browse/CASSANDRA-905) | improve py\_stress termination condition |  Minor | . | Michael Spiegel | Michael Spiegel |
| [CASSANDRA-917](https://issues.apache.org/jira/browse/CASSANDRA-917) | Refactor QueryFilter so callers don't have to care about special-casing supercolumn paths |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-950](https://issues.apache.org/jira/browse/CASSANDRA-950) | Cache BufferedRandomAccessFile.length |  Major | . | Johan Oskarsson | Johan Oskarsson |
| [CASSANDRA-44](https://issues.apache.org/jira/browse/CASSANDRA-44) | It is difficult to modify the set of ColumnFamliies in an existing cluster |  Major | . | Eric Evans | Gary Dusbabek |
| [CASSANDRA-962](https://issues.apache.org/jira/browse/CASSANDRA-962) | Schema definition propagation should happen before obtaining a bootstrap token. |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-821](https://issues.apache.org/jira/browse/CASSANDRA-821) | get\_range\_slice performance |  Minor | . | Brandon Williams | Johan Oskarsson |
| [CASSANDRA-829](https://issues.apache.org/jira/browse/CASSANDRA-829) | inner classes may be static |  Trivial | . | gabriele renzi | gabriele renzi |
| [CASSANDRA-767](https://issues.apache.org/jira/browse/CASSANDRA-767) | Row keys should be byte[]s, not Strings |  Critical | . | Stu Hood | Stu Hood |
| [CASSANDRA-971](https://issues.apache.org/jira/browse/CASSANDRA-971) | simplify configuration file loading |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1003](https://issues.apache.org/jira/browse/CASSANDRA-1003) | avoid treating ttl \<= 0 the same as ttl not specified |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-986](https://issues.apache.org/jira/browse/CASSANDRA-986) | Make endpointsnitch per-cluster instead of per-keyspace |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-946](https://issues.apache.org/jira/browse/CASSANDRA-946) | Add a configuration and implementation to populate the data into memory |  Minor | . | Vijay | Vijay |
| [CASSANDRA-714](https://issues.apache.org/jira/browse/CASSANDRA-714) | remove keyspace argument from thrift methods |  Major | . | Eric Evans | Todd Blose |
| [CASSANDRA-1023](https://issues.apache.org/jira/browse/CASSANDRA-1023) | improve debug logging for local inserts |  Minor | . | Ryan King | Ryan King |
| [CASSANDRA-999](https://issues.apache.org/jira/browse/CASSANDRA-999) | add crc for commitlogheader |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1032](https://issues.apache.org/jira/browse/CASSANDRA-1032) | write mutation length in commitlog as int, not long |  Trivial | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1043](https://issues.apache.org/jira/browse/CASSANDRA-1043) | Faster UUID comparisons |  Minor | . | James Golick | James Golick |
| [CASSANDRA-1058](https://issues.apache.org/jira/browse/CASSANDRA-1058) | Add ColumnType as enum |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-1017](https://issues.apache.org/jira/browse/CASSANDRA-1017) | keep Thrift connection open between get\_range\_slices calls in ColumnFamilyRecordReader |  Minor | . | Jonathan Ellis | Johan Oskarsson |
| [CASSANDRA-1055](https://issues.apache.org/jira/browse/CASSANDRA-1055) | Update javadocs for various snitch/rack aware related files |  Minor | . | Jeremy Hanna | Jeremy Hanna |
| [CASSANDRA-1077](https://issues.apache.org/jira/browse/CASSANDRA-1077) | clean up ExpiringMap |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1069](https://issues.apache.org/jira/browse/CASSANDRA-1069) | cfid map is no longer needed in CommitLogHeader |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-872](https://issues.apache.org/jira/browse/CASSANDRA-872) | Changing the ip address associated w/ a token should generate a warning |  Major | . | Stu Hood | Stu Hood |
| [CASSANDRA-932](https://issues.apache.org/jira/browse/CASSANDRA-932) | Use cfids instead of cfnames for major operations. |  Major | . | Gary Dusbabek | Stu Hood |
| [CASSANDRA-1087](https://issues.apache.org/jira/browse/CASSANDRA-1087) | Give a better error message when no schema is present. |  Major | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-1121](https://issues.apache.org/jira/browse/CASSANDRA-1121) | CassandraBulkLoader isn't compiling |  Major | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-987](https://issues.apache.org/jira/browse/CASSANDRA-987) | We need a way to recover from a crash during migration if the crash happens before 'definitions' are flushed. |  Major | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-1046](https://issues.apache.org/jira/browse/CASSANDRA-1046) | optimize Memtable.getSliceIterator |  Major | . | Jonathan Ellis | Matthew F. Dennis |
| [CASSANDRA-1117](https://issues.apache.org/jira/browse/CASSANDRA-1117) | Clean up MMAP support |  Major | . | Stu Hood | Stu Hood |
| [CASSANDRA-956](https://issues.apache.org/jira/browse/CASSANDRA-956) | include stream request in Streaming status on destination node |  Minor | . | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-1110](https://issues.apache.org/jira/browse/CASSANDRA-1110) | sstable2json.bat and json2sstable.bat don't work in trunk |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-1131](https://issues.apache.org/jira/browse/CASSANDRA-1131) | Towards A Formalization Of The Cassandra Daemon |  Major | . | Karthick Sankarachary |  |
| [CASSANDRA-1194](https://issues.apache.org/jira/browse/CASSANDRA-1194) | cleanup todos in AbstractReplicationStrategy |  Minor | . | Matthew F. Dennis | Matthew F. Dennis |
| [CASSANDRA-16](https://issues.apache.org/jira/browse/CASSANDRA-16) | Memory efficient compactions |  Major | . | Sandeep Tata | Jonathan Ellis |
| [CASSANDRA-579](https://issues.apache.org/jira/browse/CASSANDRA-579) | Stream SSTables without Anti-compaction |  Critical | . | Stu Hood | Stu Hood |
| [CASSANDRA-1208](https://issues.apache.org/jira/browse/CASSANDRA-1208) | remove PendingFile.ptr |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1116](https://issues.apache.org/jira/browse/CASSANDRA-1116) | CompactionManager improvements |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1200](https://issues.apache.org/jira/browse/CASSANDRA-1200) | additional BRAF tests |  Minor | . | Matthew F. Dennis | Matthew F. Dennis |
| [CASSANDRA-1115](https://issues.apache.org/jira/browse/CASSANDRA-1115) | need more state such as when a node is leaving and bootstraping rangs |  Major | Tools | jingfengtan | Matthew F. Dennis |
| [CASSANDRA-1142](https://issues.apache.org/jira/browse/CASSANDRA-1142) | redesign hinted handoff schema |  Major | . | Jonathan Ellis | Brandon Williams |
| [CASSANDRA-1196](https://issues.apache.org/jira/browse/CASSANDRA-1196) | Invalid UTF-8 keys [for legacy OPP] should cause exceptions |  Minor | . | Stu Hood | Nick Bailey |
| [CASSANDRA-1191](https://issues.apache.org/jira/browse/CASSANDRA-1191) | Make Strategy classes fail if asked to fulfil an impossible replication request |  Minor | . | Jonathan Ellis | Matthew F. Dennis |
| [CASSANDRA-1227](https://issues.apache.org/jira/browse/CASSANDRA-1227) | Input and Output column families should be configured independently |  Major | . | Bryan Tower | Bryan Tower |
| [CASSANDRA-1247](https://issues.apache.org/jira/browse/CASSANDRA-1247) | Convert type of ColumnFamily.id and CFMetaData.cfId to Integer |  Minor | . | Folke Behrens | Folke Behrens |
| [CASSANDRA-1251](https://issues.apache.org/jira/browse/CASSANDRA-1251) | [anti]compaction log messages need commas |  Minor | . | Matthew F. Dennis | Jon Hermes |
| [CASSANDRA-972](https://issues.apache.org/jira/browse/CASSANDRA-972) | log output should state the current version, the Thrift VERSION, and, if applicable, SVN rev |  Major | . | Ted Zlatanov | Matthew F. Dennis |
| [CASSANDRA-1261](https://issues.apache.org/jira/browse/CASSANDRA-1261) | Allow local range slices when consistency level is one |  Major | . | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-1035](https://issues.apache.org/jira/browse/CASSANDRA-1035) | Implement User/Keyspace throughput Scheduler |  Major | . | Stu Hood | Nirmal Ranganathan |
| [CASSANDRA-1223](https://issues.apache.org/jira/browse/CASSANDRA-1223) | HH improvements |  Minor | . | Jonathan Ellis | Brandon Williams |
| [CASSANDRA-270](https://issues.apache.org/jira/browse/CASSANDRA-270) | Reduce copies in data write path |  Major | . | Todd Lipcon | Jonathan Ellis |
| [CASSANDRA-1266](https://issues.apache.org/jira/browse/CASSANDRA-1266) | Upgrade thrift jar to support more robust transport and protocol |  Major | . | Nate McCall | Nate McCall |
| [CASSANDRA-1164](https://issues.apache.org/jira/browse/CASSANDRA-1164) | py\_stress needs a way to adjust the RF for the keyspace it creates |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-1249](https://issues.apache.org/jira/browse/CASSANDRA-1249) | Deprecate IPartitioner.(to\|from)DiskFormat in favor of raw bytes |  Minor | . | Jonathan Ellis | Stu Hood |
| [CASSANDRA-1267](https://issues.apache.org/jira/browse/CASSANDRA-1267) | Improve performance of cached row slices |  Minor | . | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-1296](https://issues.apache.org/jira/browse/CASSANDRA-1296) | DES improvements |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-1186](https://issues.apache.org/jira/browse/CASSANDRA-1186) | futureproof [KS\|CF]MetaData |  Critical | . | Jonathan Ellis | Stu Hood |
| [CASSANDRA-1302](https://issues.apache.org/jira/browse/CASSANDRA-1302) | Allow Row Iterator to use the RowCache |  Major | . | T Jake Luciani | T Jake Luciani |
| [CASSANDRA-1321](https://issues.apache.org/jira/browse/CASSANDRA-1321) | loadSchemaFromYaml should push migrations to cluster. |  Major | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-1272](https://issues.apache.org/jira/browse/CASSANDRA-1272) | Don't stop server from starting if version.properties is missing |  Major | . | Johan Oskarsson | Matthew F. Dennis |
| [CASSANDRA-1204](https://issues.apache.org/jira/browse/CASSANDRA-1204) | CliClient Support for add, drop, and rename should be added for keyspace and column family |  Minor | Tools | Mason Bryant | Mason Bryant |
| [CASSANDRA-1327](https://issues.apache.org/jira/browse/CASSANDRA-1327) | page within a single row during hinted handoff |  Major | . | Jonathan Ellis | Brandon Williams |
| [CASSANDRA-1061](https://issues.apache.org/jira/browse/CASSANDRA-1061) | GCInspector uses com.sun.management - Exception under IBM JDK |  Major | . | Davanum Srinivas | Gary Dusbabek |
| [CASSANDRA-1336](https://issues.apache.org/jira/browse/CASSANDRA-1336) | Update to new Log4j version |  Minor | Packaging | Folke Behrens | Jonathan Ellis |
| [CASSANDRA-1066](https://issues.apache.org/jira/browse/CASSANDRA-1066) | DatacenterShardStrategy needs enforceable and keyspace based RF |  Minor | . | Jeremy Hanna | Jeremy Hanna |
| [CASSANDRA-1338](https://issues.apache.org/jira/browse/CASSANDRA-1338) | optimize forward slices starting with '' by skipping the column index |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1276](https://issues.apache.org/jira/browse/CASSANDRA-1276) | GCGraceSeconds per ColumnFamily |  Minor | . | B. Todd Burruss | Jon Hermes |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-811](https://issues.apache.org/jira/browse/CASSANDRA-811) | support starting an avro enabled node (experimental) |  Major | . | Eric Evans | Eric Evans |
| [CASSANDRA-812](https://issues.apache.org/jira/browse/CASSANDRA-812) | refactor system tests to accommodate avro |  Major | . | Eric Evans | Eric Evans |
| [CASSANDRA-906](https://issues.apache.org/jira/browse/CASSANDRA-906) | Mixed use of Stage's name. Must use public static field. |  Trivial | . | Rodrigo Peinado | Rodrigo Peinado |
| [CASSANDRA-935](https://issues.apache.org/jira/browse/CASSANDRA-935) | login() request via Thrift/PHP fails with "Unexpected authentication problem" in cassandra log / "Internal error processing login" in Thrift |  Minor | . | Vitaly Pecharsky | Roger Schildmeijer |
| [CASSANDRA-944](https://issues.apache.org/jira/browse/CASSANDRA-944) | system.test\_thrift\_server.TestMutations.test\_batch\_mutate\_standard\_columns appears to be non deterministic |  Major | Tools | Matthew F. Dennis | Brandon Williams |
| [CASSANDRA-945](https://issues.apache.org/jira/browse/CASSANDRA-945) | org.apache.cassandra.config.CFMetaData defines equals but does not define hashCode |  Major | . | Matthew F. Dennis | Matthew F. Dennis |
| [CASSANDRA-963](https://issues.apache.org/jira/browse/CASSANDRA-963) | loadSchemaFromXml records no migrations |  Major | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-965](https://issues.apache.org/jira/browse/CASSANDRA-965) | remove deprecated get\_string\*\_property() thrift methods |  Minor | . | Eric Evans | Eric Evans |
| [CASSANDRA-966](https://issues.apache.org/jira/browse/CASSANDRA-966) | StorageService.getPartitioner() and QueryFilter.getColumnComparator() should be statically accessed |  Trivial | . | Tupshin Harper | Tupshin Harper |
| [CASSANDRA-915](https://issues.apache.org/jira/browse/CASSANDRA-915) | disallow column family names containing hyphens |  Minor | . | Eric Evans |  |
| [CASSANDRA-744](https://issues.apache.org/jira/browse/CASSANDRA-744) | [multi\_]get\_count should take a SlicePredicate |  Minor | . | Jonathan Ellis | Sylvain Lebresne |
| [CASSANDRA-943](https://issues.apache.org/jira/browse/CASSANDRA-943) | Spelling correction: rename DatacenterShardStategy to DatacenterShardStrategy |  Minor | . | Rodrigo Peinado | Eric Sigler |
| [CASSANDRA-1006](https://issues.apache.org/jira/browse/CASSANDRA-1006) | Exception starting up cluster with ByteOrderedPartitioner without schema set |  Major | . | Erick Tryzelaar | Stu Hood |
| [CASSANDRA-995](https://issues.apache.org/jira/browse/CASSANDRA-995) | restarting node crashes with NPE when, while replaying the commitlog, the cfMetaData is requested |  Critical | . | Ted Zlatanov | Gary Dusbabek |
| [CASSANDRA-1009](https://issues.apache.org/jira/browse/CASSANDRA-1009) | Bad EndpointSnitch config in trunk |  Major | . | Stu Hood | Stu Hood |
| [CASSANDRA-805](https://issues.apache.org/jira/browse/CASSANDRA-805) | using Integer.MAX\_VALUE for executor keepalive time defeats the purpose of the SEDA-like stage divisions |  Trivial | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1002](https://issues.apache.org/jira/browse/CASSANDRA-1002) | Fat client example cannot find schema |  Major | . | Johan Oskarsson | Gary Dusbabek |
| [CASSANDRA-1020](https://issues.apache.org/jira/browse/CASSANDRA-1020) | keys are now byte[] but hashmap cannot have byte[] as keys so they need to be fixed |  Critical | . | Vijay | Vijay |
| [CASSANDRA-757](https://issues.apache.org/jira/browse/CASSANDRA-757) | FatClient removal causes ConcurrentModificationException |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-739](https://issues.apache.org/jira/browse/CASSANDRA-739) | multiget returns empty ColumnOrSuperColumn instead of null |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1033](https://issues.apache.org/jira/browse/CASSANDRA-1033) | stress.py broken in trunk |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-1022](https://issues.apache.org/jira/browse/CASSANDRA-1022) | separate assignment of current keyspace from login() |  Minor | . | Eric Evans | Todd Blose |
| [CASSANDRA-1038](https://issues.apache.org/jira/browse/CASSANDRA-1038) | Not all column families are created |  Major | . | Brandon Williams | Gary Dusbabek |
| [CASSANDRA-1054](https://issues.apache.org/jira/browse/CASSANDRA-1054) | isSuper flag in cfstore is wrongly set in 0.7 |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-948](https://issues.apache.org/jira/browse/CASSANDRA-948) | Cannot Start Cassandra Under Windows |  Major | . | Nick Berardi | Gary Dusbabek |
| [CASSANDRA-1030](https://issues.apache.org/jira/browse/CASSANDRA-1030) | fix contrib/word\_count build in 0.7 |  Major | . | Jonathan Ellis | Jeremy Hanna |
| [CASSANDRA-1036](https://issues.apache.org/jira/browse/CASSANDRA-1036) | Attempting to mutate a non-existant CF does not propagate an error to the client |  Minor | . | Brandon Williams | Sylvain Lebresne |
| [CASSANDRA-1040](https://issues.apache.org/jira/browse/CASSANDRA-1040) | read failure during flush |  Critical | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1067](https://issues.apache.org/jira/browse/CASSANDRA-1067) | Use batch\_mutate in stress.py |  Major | . | Stu Hood | Stu Hood |
| [CASSANDRA-1080](https://issues.apache.org/jira/browse/CASSANDRA-1080) | NPE when no keyspaces section is found in yaml file |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-1076](https://issues.apache.org/jira/browse/CASSANDRA-1076) | StreamingService.StreamDestinations never empties |  Minor | . | Gary Dusbabek | Stu Hood |
| [CASSANDRA-1094](https://issues.apache.org/jira/browse/CASSANDRA-1094) | Use get\_range\_slices in stress.py |  Minor | Tools | Stu Hood | Stu Hood |
| [CASSANDRA-952](https://issues.apache.org/jira/browse/CASSANDRA-952) | DC Quorum broken @ trunk |  Minor | . | Vijay | Vijay |
| [CASSANDRA-1103](https://issues.apache.org/jira/browse/CASSANDRA-1103) | DSS rack-awareness doesn't really work |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1062](https://issues.apache.org/jira/browse/CASSANDRA-1062) | update contrib/bmt\_example to work post-CASSANDRA-44 |  Minor | . | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-1109](https://issues.apache.org/jira/browse/CASSANDRA-1109) | Cassandra does not expire data after setting timeToLive argument for each value |  Major | . | Jignesh Dhruv | Jignesh Dhruv |
| [CASSANDRA-1008](https://issues.apache.org/jira/browse/CASSANDRA-1008) | Assertion failure loadbalance-ing a ByteOrderedPartitioner cluster |  Minor | . | Erick Tryzelaar | Erick Tryzelaar |
| [CASSANDRA-1050](https://issues.apache.org/jira/browse/CASSANDRA-1050) | Too many splits for ColumnFamily with only a few rows |  Major | . | Joost Ouwerkerk | Johan Oskarsson |
| [CASSANDRA-1019](https://issues.apache.org/jira/browse/CASSANDRA-1019) | "java.net.ConnectException: Connection timed out" in MESSAGE-STREAMING-POOL:1 |  Major | . | B. Todd Burruss | Stu Hood |
| [CASSANDRA-1122](https://issues.apache.org/jira/browse/CASSANDRA-1122) | Lack of subcomparator\_type will corrupt the keyspace in Thrift system\_add\_keyspace() |  Major | . | Arya Goudarzi | Gary Dusbabek |
| [CASSANDRA-1128](https://issues.apache.org/jira/browse/CASSANDRA-1128) | sstable2json spews because it uses DatabaseDescriptor before loadSchemas() is called |  Minor | Tools | Matthew F. Dennis | Matthew F. Dennis |
| [CASSANDRA-1111](https://issues.apache.org/jira/browse/CASSANDRA-1111) | describe\_ring() throws on single node clusters and/or probably clusters without a replication factor |  Major | . | Dominic Williams | Gary Dusbabek |
| [CASSANDRA-1148](https://issues.apache.org/jira/browse/CASSANDRA-1148) | Update stress.py to thrift api changes |  Major | . | Johan Oskarsson | Johan Oskarsson |
| [CASSANDRA-1152](https://issues.apache.org/jira/browse/CASSANDRA-1152) | Read operation with ConsistencyLevel.ALL throws exception |  Major | . | Yuki Morishita | Gary Dusbabek |
| [CASSANDRA-1146](https://issues.apache.org/jira/browse/CASSANDRA-1146) | "Expected both token and generation columns" |  Major | . | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-1118](https://issues.apache.org/jira/browse/CASSANDRA-1118) | allow overriding existing token owner with a new IP |  Minor | . | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-1168](https://issues.apache.org/jira/browse/CASSANDRA-1168) | system\_drop\_column\_family() and system\_rename\_column\_family() should not take keyspace args |  Minor | . | Benjamin Black | Matthew F. Dennis |
| [CASSANDRA-1160](https://issues.apache.org/jira/browse/CASSANDRA-1160) | race with insufficiently constructed Gossiper |  Minor | . | Jonathan Ellis | Matthew F. Dennis |
| [CASSANDRA-1119](https://issues.apache.org/jira/browse/CASSANDRA-1119) | detect incomplete commitlogheader |  Minor | . | Jonathan Ellis | Matthew F. Dennis |
| [CASSANDRA-1178](https://issues.apache.org/jira/browse/CASSANDRA-1178) | get\_slice calls do not close files when finished resulting in "too many open files" exceptions and rendering C unusable |  Major | . | Matthew F. Dennis | Matthew F. Dennis |
| [CASSANDRA-1188](https://issues.apache.org/jira/browse/CASSANDRA-1188) | multiget\_slice calls do not close files resulting in file descriptor leak |  Critical | . | Matt Conway | Matthew F. Dennis |
| [CASSANDRA-1130](https://issues.apache.org/jira/browse/CASSANDRA-1130) | Row iteration can stomp start-of-row mark |  Major | . | Jignesh Dhruv | Sylvain Lebresne |
| [CASSANDRA-1169](https://issues.apache.org/jira/browse/CASSANDRA-1169) | AES makes Streaming unhappy |  Critical | . | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-1147](https://issues.apache.org/jira/browse/CASSANDRA-1147) | cassandra cannot bootstrap when using DatacenterShardStrategy |  Major | . | Matthew F. Dennis | Matthew F. Dennis |
| [CASSANDRA-1180](https://issues.apache.org/jira/browse/CASSANDRA-1180) | read\_repair\_chance is missing from CfDef |  Minor | . | Arya Goudarzi | Gary Dusbabek |
| [CASSANDRA-1179](https://issues.apache.org/jira/browse/CASSANDRA-1179) | split commitlog into header + mutations files |  Major | . | Jonathan Ellis | Matthew F. Dennis |
| [CASSANDRA-1219](https://issues.apache.org/jira/browse/CASSANDRA-1219) | avoid creating a new byte[] for each mutation replayed |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1203](https://issues.apache.org/jira/browse/CASSANDRA-1203) | system\_drop\_keyspace can cause a node to be unstartable |  Major | . | Mason Bryant | Gary Dusbabek |
| [CASSANDRA-1242](https://issues.apache.org/jira/browse/CASSANDRA-1242) | When loading an AbstractType that does not include an instance field, an unhelpful exception is raised making diagnosis difficult |  Major | . | Erik Onnen | Erik Onnen |
| [CASSANDRA-1246](https://issues.apache.org/jira/browse/CASSANDRA-1246) | Hadoop output SlicePredicate is slow and doesn't work as intended |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1273](https://issues.apache.org/jira/browse/CASSANDRA-1273) | describe\_keyspace not returning reconciler info |  Minor | . | Jeremy Hanna | Jeremy Hanna |
| [CASSANDRA-1011](https://issues.apache.org/jira/browse/CASSANDRA-1011) | Exception auto-bootstrapping two nodes nodes at the same time |  Minor | . | Erick Tryzelaar | Gary Dusbabek |
| [CASSANDRA-1290](https://issues.apache.org/jira/browse/CASSANDRA-1290) | Cassandra-cli doesn't use framed transport by default |  Minor | . | Jon Hermes | Jon Hermes |
| [CASSANDRA-1294](https://issues.apache.org/jira/browse/CASSANDRA-1294) | MIsc license inclusion booboos with recent index commits |  Minor | . | Nate McCall | Nate McCall |
| [CASSANDRA-1299](https://issues.apache.org/jira/browse/CASSANDRA-1299) | EOFException in LazilyCompactedRow |  Critical | . | Stu Hood | Jonathan Ellis |
| [CASSANDRA-1298](https://issues.apache.org/jira/browse/CASSANDRA-1298) | avoid replaying fully-flushed commitlog segments |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-475](https://issues.apache.org/jira/browse/CASSANDRA-475) | sending random data crashes thrift service |  Major | . | Eric Evans | Nate McCall |
| [CASSANDRA-1028](https://issues.apache.org/jira/browse/CASSANDRA-1028) | NPE in AntiEntropyService.getNeighbors |  Major | . | Jonathan Ellis | Stu Hood |
| [CASSANDRA-1287](https://issues.apache.org/jira/browse/CASSANDRA-1287) | Rename 'table' -\> 'keyspace' in public APIs |  Major | . | Stu Hood | Stu Hood |
| [CASSANDRA-1318](https://issues.apache.org/jira/browse/CASSANDRA-1318) | CommitLogTest and RecoveryManager2Test is failing in trunk |  Minor | . | Matthew F. Dennis | Matthew F. Dennis |
| [CASSANDRA-1309](https://issues.apache.org/jira/browse/CASSANDRA-1309) | LegacySSTableTest breaks when run from a svn checkout |  Minor | . | Jonathan Ellis | Stu Hood |
| [CASSANDRA-1326](https://issues.apache.org/jira/browse/CASSANDRA-1326) | contrib/pig's cassandra.yaml is out of date |  Major | . | Jeremy Hanna | Jeremy Hanna |
| [CASSANDRA-1292](https://issues.apache.org/jira/browse/CASSANDRA-1292) | Multiple migrations might run at once |  Critical | . | Stu Hood | Gary Dusbabek |
| [CASSANDRA-1333](https://issues.apache.org/jira/browse/CASSANDRA-1333) | cassandra-cli cannot connect |  Major | . | Arya Goudarzi | Jonathan Ellis |
| [CASSANDRA-1310](https://issues.apache.org/jira/browse/CASSANDRA-1310) | Disallow KS definition with RF \> # of nodes |  Major | . | Arya Goudarzi | Nate McCall |
| [CASSANDRA-1340](https://issues.apache.org/jira/browse/CASSANDRA-1340) | making cassandra-cli friendlier to scripts |  Minor | Tools | Eric Evans | Eric Evans |
| [CASSANDRA-1286](https://issues.apache.org/jira/browse/CASSANDRA-1286) | Nodetool ring crashes when no schema is loaded |  Minor | Tools | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-1349](https://issues.apache.org/jira/browse/CASSANDRA-1349) | DynamicEndpointSnitch should implement AbstractRackAwareSnitch, not AbstractEndpointSnitch |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-1351](https://issues.apache.org/jira/browse/CASSANDRA-1351) | Avro Schema Swap |  Critical | . | Stu Hood | Stu Hood |
| [CASSANDRA-1279](https://issues.apache.org/jira/browse/CASSANDRA-1279) | heisenbug in RoundRobinSchedulerTest |  Major | . | Jonathan Ellis | Nirmal Ranganathan |
| [CASSANDRA-1343](https://issues.apache.org/jira/browse/CASSANDRA-1343) | gossip throws IllegalStateException |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-1334](https://issues.apache.org/jira/browse/CASSANDRA-1334) | cfstats shows deleted cfs |  Major | . | Arya Goudarzi | Gary Dusbabek |
| [CASSANDRA-1352](https://issues.apache.org/jira/browse/CASSANDRA-1352) | Update py\_stress to reflect udpated KsDef params |  Trivial | . | Nirmal Ranganathan | Nirmal Ranganathan |
| [CASSANDRA-1354](https://issues.apache.org/jira/browse/CASSANDRA-1354) | Static CFMetaData objects are using the wrong constructor. |  Major | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-1353](https://issues.apache.org/jira/browse/CASSANDRA-1353) | commit log recovery is broken when the CL contains mutations to CFs that have been dropped |  Major | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-1362](https://issues.apache.org/jira/browse/CASSANDRA-1362) | AsynchResult does not respect TimeUnit in get() |  Trivial | . | Martin Hentschel | Martin Hentschel |
| [CASSANDRA-1356](https://issues.apache.org/jira/browse/CASSANDRA-1356) | "ant" always compiles at least 36 files, even if no changes were made |  Major | Packaging | Jonathan Ellis | Folke Behrens |
| [CASSANDRA-1357](https://issues.apache.org/jira/browse/CASSANDRA-1357) | get rid of internode directory |  Major | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-1360](https://issues.apache.org/jira/browse/CASSANDRA-1360) | nosetests are busted because "Not enough live nodes to support this keyspace" |  Major | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-1332](https://issues.apache.org/jira/browse/CASSANDRA-1332) | Scan results out of order |  Major | . | Dave Revell | Jonathan Ellis |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-951](https://issues.apache.org/jira/browse/CASSANDRA-951) | Merge similar tests |  Minor | . | Stu Hood | Stu Hood |
| [CASSANDRA-1082](https://issues.apache.org/jira/browse/CASSANDRA-1082) | Additional test to check if a CF can be added to an empty KS after creation |  Minor | . | Erik Onnen | Gary Dusbabek |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-819](https://issues.apache.org/jira/browse/CASSANDRA-819) | refactor database descriptor so that KS and CF descriptions are contained within serializable classes. |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-825](https://issues.apache.org/jira/browse/CASSANDRA-825) | Create an additional system keyspace to store KS definitions and migration data. |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-840](https://issues.apache.org/jira/browse/CASSANDRA-840) | Add the ability to add/remove/rename column families on a single node. |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-861](https://issues.apache.org/jira/browse/CASSANDRA-861) | Add the ability to add/remove/rename keyspaces on a single node. |  Major | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-827](https://issues.apache.org/jira/browse/CASSANDRA-827) | Add hooks that recognize when system KS is updated. |  Major | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-953](https://issues.apache.org/jira/browse/CASSANDRA-953) | Expose byte[] row keys in client APIs |  Major | . | Stu Hood | Stu Hood |
| [CASSANDRA-1070](https://issues.apache.org/jira/browse/CASSANDRA-1070) | Update interface to use a wrapper for various clock types |  Major | . | Johan Oskarsson | Sylvain Lebresne |
| [CASSANDRA-1144](https://issues.apache.org/jira/browse/CASSANDRA-1144) | Add support for a reconciler class |  Major | . | Johan Oskarsson | Johan Oskarsson |
| [CASSANDRA-1127](https://issues.apache.org/jira/browse/CASSANDRA-1127) | merge RowIndexed\* back into SSTable\* |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-958](https://issues.apache.org/jira/browse/CASSANDRA-958) | Expose byte[] keys to Avro API |  Major | . | Stu Hood | Eric Evans |
| [CASSANDRA-1153](https://issues.apache.org/jira/browse/CASSANDRA-1153) | Add index metadata to CFMetadata |  Major | . | Jonathan Ellis | Matthew F. Dennis |
| [CASSANDRA-1154](https://issues.apache.org/jira/browse/CASSANDRA-1154) | basic, single node indexing against one indexexpression |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1265](https://issues.apache.org/jira/browse/CASSANDRA-1265) | make getBuckets deterministic |  Minor | . | Jonathan Ellis | Tyler Hobbs |
| [CASSANDRA-1155](https://issues.apache.org/jira/browse/CASSANDRA-1155) | keep persistent row statistics |  Major | . | Jonathan Ellis | Brandon Williams |
| [CASSANDRA-1157](https://issues.apache.org/jira/browse/CASSANDRA-1157) | add support for multiple indexexpressions |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1258](https://issues.apache.org/jira/browse/CASSANDRA-1258) | rebuild indexes after streaming |  Major | . | Jonathan Ellis | Nate McCall |
| [CASSANDRA-1156](https://issues.apache.org/jira/browse/CASSANDRA-1156) | support querying multiple nodes for index scan |  Major | . | Jonathan Ellis | Jonathan Ellis |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-930](https://issues.apache.org/jira/browse/CASSANDRA-930) | make RR chance configurable |  Minor | . | Jonathan Ellis | Matthew F. Dennis |
| [CASSANDRA-989](https://issues.apache.org/jira/browse/CASSANDRA-989) | Document the change to byte[] keys |  Critical | . | Stu Hood | Stu Hood |
| [CASSANDRA-985](https://issues.apache.org/jira/browse/CASSANDRA-985) | CompactionSpeedTest is 25% of the total test suite run time |  Minor | Tools | Jonathan Ellis | Stu Hood |
| [CASSANDRA-993](https://issues.apache.org/jira/browse/CASSANDRA-993) | r/m obsolete contrib/cassandra\_browser |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-859](https://issues.apache.org/jira/browse/CASSANDRA-859) | update cassandra-cli for mandatory login() |  Minor | Tools | Eric Evans | Todd Blose |
| [CASSANDRA-994](https://issues.apache.org/jira/browse/CASSANDRA-994) | re-organize endpointsnitch implementations |  Major | . | Jonathan Ellis | Erick Tryzelaar |
| [CASSANDRA-1065](https://issues.apache.org/jira/browse/CASSANDRA-1065) | r/m deprecated batch\_insert, get\_range\_slice methods |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1052](https://issues.apache.org/jira/browse/CASSANDRA-1052) | Move ks/cf definitions into system keyspace |  Minor | . | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-1105](https://issues.apache.org/jira/browse/CASSANDRA-1105) | Create timers to reload cassandra-rack.properties and datacenters.properties |  Minor | . | Jonathan Ellis | Jon Hermes |
| [CASSANDRA-1165](https://issues.apache.org/jira/browse/CASSANDRA-1165) | Remove hadoop from lib and put it as a dep in ivy.xml |  Minor | . | Jeremy Hanna | Jeremy Hanna |
| [CASSANDRA-1312](https://issues.apache.org/jira/browse/CASSANDRA-1312) | remove bitmask predicates |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-1308](https://issues.apache.org/jira/browse/CASSANDRA-1308) | Switch Migrations Serialization to Avro |  Major | . | Stu Hood | Stu Hood |
| [CASSANDRA-1256](https://issues.apache.org/jira/browse/CASSANDRA-1256) | Reduce thread stack size |  Minor | . | Jonathan Ellis | Brandon Williams |


