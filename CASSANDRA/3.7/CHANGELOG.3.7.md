
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.7 - 2016-06-13



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11831](https://issues.apache.org/jira/browse/CASSANDRA-11831) | Ability to disable purgeable tombstone check via startup flag |  Major | . | Ryan Svihla | Marcus Eriksson |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11552](https://issues.apache.org/jira/browse/CASSANDRA-11552) | Reduce amount of logging calls from ColumnFamilyStore.selectAndReference |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-11518](https://issues.apache.org/jira/browse/CASSANDRA-11518) | o.a.c.utils.UUIDGen clock generation is not very high in entropy |  Trivial | Core | Ariel Weisberg | Ariel Weisberg |
| [CASSANDRA-11541](https://issues.apache.org/jira/browse/CASSANDRA-11541) | correct the java documentation for SlabAllocator and NativeAllocator |  Trivial | . | ZhaoYang | ZhaoYang |
| [CASSANDRA-11754](https://issues.apache.org/jira/browse/CASSANDRA-11754) | Support disabling early open on individual tables |  Minor | . | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-10828](https://issues.apache.org/jira/browse/CASSANDRA-10828) | Allow usage of multiplier in the start value of cassandra-stress population sequence |  Trivial | Tools | Eduard Tudenhoefner | Eduard Tudenhoefner |
| [CASSANDRA-11647](https://issues.apache.org/jira/browse/CASSANDRA-11647) | Don't use static dataDirectories field in Directories instances |  Major | . | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-11032](https://issues.apache.org/jira/browse/CASSANDRA-11032) | Full trace returned on ReadFailure by cqlsh |  Minor | Tools | Chris Splinter | Zhongxiang Zheng |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11661](https://issues.apache.org/jira/browse/CASSANDRA-11661) | Cassandra 2.0 and later require Java 7u25 or later - jdk 101 |  Critical | Testing | William Boutin | Eduard Tudenhoefner |
| [CASSANDRA-9861](https://issues.apache.org/jira/browse/CASSANDRA-9861) | When forcibly exiting due to OOM, we should produce a heap dump |  Minor | Lifecycle | Benedict | Benjamin Lerer |
| [CASSANDRA-11626](https://issues.apache.org/jira/browse/CASSANDRA-11626) | cqlsh fails and exits on non-ascii chars |  Minor | Tools | Robert Stupp | Tyler Hobbs |
| [CASSANDRA-11540](https://issues.apache.org/jira/browse/CASSANDRA-11540) | The JVM should exit if jmx fails to bind |  Major | Core | Brandon Williams | Alex Petrov |
| [CASSANDRA-11475](https://issues.apache.org/jira/browse/CASSANDRA-11475) | MV code refactor |  Major | Coordination, Materialized Views | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-11615](https://issues.apache.org/jira/browse/CASSANDRA-11615) | cassandra-stress blocks when connecting to a big cluster |  Major | Tools | Eduard Tudenhoefner | Andy Tolbert |
| [CASSANDRA-9395](https://issues.apache.org/jira/browse/CASSANDRA-9395) | Prohibit Counter type as part of the PK |  Major | CQL | Sebastian Estevez | Brett Snyder |
| [CASSANDRA-11736](https://issues.apache.org/jira/browse/CASSANDRA-11736) | LegacySSTableTest::testStreamLegacyCqlTables fails |  Minor | Testing | Alex Petrov | Alex Petrov |
| [CASSANDRA-11737](https://issues.apache.org/jira/browse/CASSANDRA-11737) | Add a way to disable severity in DynamicEndpointSnitch |  Major | . | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-11309](https://issues.apache.org/jira/browse/CASSANDRA-11309) | Generic Java UDF types broken for RETURNS NULL ON NULL INPUT |  Minor | CQL | Robert Stupp | Robert Stupp |
| [CASSANDRA-11657](https://issues.apache.org/jira/browse/CASSANDRA-11657) | LongLeveledCompactionStrategyTest broken since CASSANDRA-10099 |  Major | Testing | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-11644](https://issues.apache.org/jira/browse/CASSANDRA-11644) | gc\_warn\_threshold\_in\_ms is not applied when it's greater than MIN\_LOG\_DURATION(200ms) |  Minor | . | ZhaoYang | ZhaoYang |
| [CASSANDRA-11753](https://issues.apache.org/jira/browse/CASSANDRA-11753) | cqlsh show sessions truncates time\_elapsed values \> 999999 |  Major | CQL, Observability, Testing, Tools | Jonathan Shook | Stefania |
| [CASSANDRA-11705](https://issues.apache.org/jira/browse/CASSANDRA-11705) | clearSnapshots using Directories.dataDirectories instead of CFS.initialDirectories |  Minor | . | Blake Eggleston | Blake Eggleston |
| [CASSANDRA-11855](https://issues.apache.org/jira/browse/CASSANDRA-11855) | MessagingService#getCommandDroppedTasks should be displayed in netstats |  Major | Tools | Jeremiah Jordan | Jeremiah Jordan |
| [CASSANDRA-11272](https://issues.apache.org/jira/browse/CASSANDRA-11272) | NullPointerException (NPE) during bootstrap startup in StorageService.java |  Major | Lifecycle | Jason Kania | Alex Petrov |
| [CASSANDRA-11739](https://issues.apache.org/jira/browse/CASSANDRA-11739) | Cache key references might cause OOM on incremental repair |  Major | Streaming and Messaging | Paulo Motta | Paulo Motta |
| [CASSANDRA-11867](https://issues.apache.org/jira/browse/CASSANDRA-11867) | Possible memory leak in NIODataInputStream |  Minor | Local Write-Read Paths | Robert Stupp | Robert Stupp |
| [CASSANDRA-11708](https://issues.apache.org/jira/browse/CASSANDRA-11708) | sstableloader not work with ssl options |  Minor | Tools | Bin Chen | Yuki Morishita |
| [CASSANDRA-11848](https://issues.apache.org/jira/browse/CASSANDRA-11848) | replace address can "succeed" without actually streaming anything |  Major | Streaming and Messaging | Jeremiah Jordan | Paulo Motta |
| [CASSANDRA-9530](https://issues.apache.org/jira/browse/CASSANDRA-9530) | SSTable corruption can trigger OOM |  Major | Local Write-Read Paths | Sylvain Lebresne | Alex Petrov |
| [CASSANDRA-11824](https://issues.apache.org/jira/browse/CASSANDRA-11824) | If repair fails no way to run repair again |  Major | . | T Jake Luciani | Marcus Eriksson |
| [CASSANDRA-11891](https://issues.apache.org/jira/browse/CASSANDRA-11891) | WriteTimeout during commit log replay due to MV lock |  Critical | Lifecycle, Local Write-Read Paths, Materialized Views | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-11743](https://issues.apache.org/jira/browse/CASSANDRA-11743) | Race condition in CommitLog.recover can prevent startup |  Major | Lifecycle | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-11127](https://issues.apache.org/jira/browse/CASSANDRA-11127) | index\_summary\_upgrade\_test.py is failing |  Major | Testing | Philip Thompson | Tyler Hobbs |
| [CASSANDRA-9669](https://issues.apache.org/jira/browse/CASSANDRA-9669) | If sstable flushes complete out of order, on restart we can fail to replay necessary commit log records |  Critical | Local Write-Read Paths | Benedict | Branimir Lambov |
| [CASSANDRA-11912](https://issues.apache.org/jira/browse/CASSANDRA-11912) | Remove DatabaseDescriptor import from AbstractType |  Major | Configuration, Core | Jeremiah Jordan | Alex Petrov |
| [CASSANDRA-11711](https://issues.apache.org/jira/browse/CASSANDRA-11711) | testJsonThreadSafety is failing / flapping |  Minor | Testing | Alex Petrov | Tyler Hobbs |
| [CASSANDRA-11922](https://issues.apache.org/jira/browse/CASSANDRA-11922) | BatchlogManagerTest is failing intermittently |  Major | Compaction | Alex Petrov | Alex Petrov |
| [CASSANDRA-11587](https://issues.apache.org/jira/browse/CASSANDRA-11587) | Cfstats estimate number of keys should return 0 for empty table |  Trivial | Tools | Jane Deng | Mahdi Mohammadinasab |
| [CASSANDRA-11152](https://issues.apache.org/jira/browse/CASSANDRA-11152) | SOURCE command in CQLSH 3.2 requires that "use keyspace" is in the cql file that you are sourcing |  Major | Tools | Francesco Animali | Stefania |
| [CASSANDRA-11664](https://issues.apache.org/jira/browse/CASSANDRA-11664) | Tab completion in cqlsh doesn't work for capitalized letters |  Minor | Tools | J.B. Langston | Mahdi Mohammadinasab |
| [CASSANDRA-11849](https://issues.apache.org/jira/browse/CASSANDRA-11849) | Potential data directory problems due to CFS getDirectories logic |  Major | . | T Jake Luciani | Blake Eggleston |
| [CASSANDRA-11742](https://issues.apache.org/jira/browse/CASSANDRA-11742) | Failed bootstrap results in exception when node is restarted |  Minor | Lifecycle | Tommy Stendahl | Joel Knighton |
| [CASSANDRA-11765](https://issues.apache.org/jira/browse/CASSANDRA-11765) | dtest failure in upgrade\_tests.upgrade\_through\_versions\_test.ProtoV3Upgrade\_AllVersions\_Skips\_3\_0\_x\_EndsAt\_Trunk\_HEAD.rolling\_upgrade\_test |  Major | Compaction | Philip Thompson | Stefania |
| [CASSANDRA-11915](https://issues.apache.org/jira/browse/CASSANDRA-11915) | [PATCH] doc: correct section number of opcode in protocol spec |  Trivial | Documentation and Website | Amos Jianjun Kong | Amos Jianjun Kong |
| [CASSANDRA-11930](https://issues.apache.org/jira/browse/CASSANDRA-11930) | Range tombstone serialisation between 2.1 and 3.0 nodes (in 3.0 -\> 2.1 direction) is broken for some Thrift deletions |  Major | Coordination | Aleksey Yeschenko | Tyler Hobbs |
| [CASSANDRA-11884](https://issues.apache.org/jira/browse/CASSANDRA-11884) | dtest failure in secondary\_indexes\_test.TestSecondaryIndexesOnCollections.test\_tuple\_indexes |  Major | Secondary Indexes | Sean McCarthy | Branimir Lambov |
| [CASSANDRA-13739](https://issues.apache.org/jira/browse/CASSANDRA-13739) | Cassandra can't start because of unknown type \<usertype\> exception |  Major | Core, CQL | Andrey Khashchin |  |
| [CASSANDRA-10391](https://issues.apache.org/jira/browse/CASSANDRA-10391) | sstableloader fails with client SSL enabled with non-standard keystore/truststore location |  Major | Tools | Jon Moses | Andrew Hust |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-9039](https://issues.apache.org/jira/browse/CASSANDRA-9039) | CommitLog compressed configuration not run in several unit tests |  Major | Local Write-Read Paths | Ariel Weisberg | Benjamin Lerer |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-11840](https://issues.apache.org/jira/browse/CASSANDRA-11840) | Set a more conservative default to streaming\_socket\_timeout\_in\_ms |  Major | Streaming and Messaging | Paulo Motta | Paulo Motta |


