
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 1.1.12 - 2013-05-27



### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-5393](https://issues.apache.org/jira/browse/CASSANDRA-5393) | Add retry mechanism to OTC for non-droppable\_verbs |  Major | . | Jeremiah Jordan | Jason Brown |
| [CASSANDRA-5497](https://issues.apache.org/jira/browse/CASSANDRA-5497) | Use allocator information to improve memtable memory usage estimate |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-5535](https://issues.apache.org/jira/browse/CASSANDRA-5535) | Manifest file not fsynced |  Minor | . | Terry Cumaranatunge | Marcus Eriksson |
| [CASSANDRA-5551](https://issues.apache.org/jira/browse/CASSANDRA-5551) | intersects the bounds not right |  Minor | . | yangwei | yangwei |
| [CASSANDRA-5529](https://issues.apache.org/jira/browse/CASSANDRA-5529) | thrift\_max\_message\_length\_in\_mb makes long-lived connections error out |  Major | CQL | Rob Timpe | Jonathan Ellis |
| [CASSANDRA-5488](https://issues.apache.org/jira/browse/CASSANDRA-5488) | CassandraStorage throws NullPointerException (NPE) when widerows is set to 'true' |  Minor | . | Sheetal Gosrani | Sheetal Gosrani |


