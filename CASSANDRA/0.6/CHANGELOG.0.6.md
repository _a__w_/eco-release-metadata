
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.6 - 2010-04-13



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-632](https://issues.apache.org/jira/browse/CASSANDRA-632) | add benchmarking for supercolumns |  Major | . | Jonathan Ellis | Brandon Williams |
| [CASSANDRA-408](https://issues.apache.org/jira/browse/CASSANDRA-408) | Pool BufferedRandomAccessFile objects used by sstable reads |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-661](https://issues.apache.org/jira/browse/CASSANDRA-661) | Pretty-print column names in CLI 'get' command |  Trivial | Tools | Ryan Daum | Ryan Daum |
| [CASSANDRA-678](https://issues.apache.org/jira/browse/CASSANDRA-678) | row-level cache |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-547](https://issues.apache.org/jira/browse/CASSANDRA-547) | authentication and authorization functions (stage 1) |  Major | . | Ted Zlatanov | Ted Zlatanov |
| [CASSANDRA-687](https://issues.apache.org/jira/browse/CASSANDRA-687) | Add ConsistencyLevel.ANY |  Minor | . | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-698](https://issues.apache.org/jira/browse/CASSANDRA-698) | ClusterProbe |  Minor | Tools | Chris Goffinet | Eric Evans |
| [CASSANDRA-732](https://issues.apache.org/jira/browse/CASSANDRA-732) | expose forceCompaction in CFS mbean |  Trivial | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-708](https://issues.apache.org/jira/browse/CASSANDRA-708) | Set Cache Capacity via nodeprobe |  Minor | Tools | Chris Goffinet | Jonathan Ellis |
| [CASSANDRA-723](https://issues.apache.org/jira/browse/CASSANDRA-723) | Add global snapshot to nodeprobe |  Minor | . | Jaakko Laine | Jaakko Laine |
| [CASSANDRA-745](https://issues.apache.org/jira/browse/CASSANDRA-745) | Support Range Queries all the partitioners currently supported. |  Major | . | Vijay | Jonathan Ellis |
| [CASSANDRA-758](https://issues.apache.org/jira/browse/CASSANDRA-758) | support wrapped range queries |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-759](https://issues.apache.org/jira/browse/CASSANDRA-759) | add start-exclusive mode to range queries |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-620](https://issues.apache.org/jira/browse/CASSANDRA-620) | Add per-keyspace replication factor (possibly even replication strategy) |  Major | . | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-736](https://issues.apache.org/jira/browse/CASSANDRA-736) | time to add first-class thrift methods for the string meta queries? |  Minor | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-740](https://issues.apache.org/jira/browse/CASSANDRA-740) | Create InProcessCassandraServer for unit tests |  Minor | . | Ran Tavory | Ran Tavory |
| [CASSANDRA-709](https://issues.apache.org/jira/browse/CASSANDRA-709) | Add jmx progress indicators for streaming & bootstrap operations |  Minor | Tools | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-782](https://issues.apache.org/jira/browse/CASSANDRA-782) | Create a cleanup utility that cleans all data directories |  Minor | . | Ran Tavory | Ran Tavory |
| [CASSANDRA-775](https://issues.apache.org/jira/browse/CASSANDRA-775) | Allow token range queries over Thrift |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-342](https://issues.apache.org/jira/browse/CASSANDRA-342) | hadoop integration |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-852](https://issues.apache.org/jira/browse/CASSANDRA-852) | add login to cli |  Minor | Tools | Jonathan Ellis | Roger Schildmeijer |
| [CASSANDRA-870](https://issues.apache.org/jira/browse/CASSANDRA-870) | Track row stats per CF at compaction time |  Minor | Tools | Jonathan Ellis | Brandon Williams |
| [CASSANDRA-769](https://issues.apache.org/jira/browse/CASSANDRA-769) | refuse to start node if ClusterName in storage-conf.xml is different from ClusterName in system |  Minor | . | Robert Coli | Brandon Williams |
| [CASSANDRA-910](https://issues.apache.org/jira/browse/CASSANDRA-910) | Pig LoadFunc for Cassandra |  Major | . | Stu Hood | Stu Hood |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-535](https://issues.apache.org/jira/browse/CASSANDRA-535) | Support remote clients via Java API |  Major | . | T Jake Luciani | Gary Dusbabek |
| [CASSANDRA-627](https://issues.apache.org/jira/browse/CASSANDRA-627) | use FastDateFormat instead of SimpleDateFormat |  Minor | . | gabriele renzi | gabriele renzi |
| [CASSANDRA-635](https://issues.apache.org/jira/browse/CASSANDRA-635) | stress.py should be standalone |  Minor | Tools | Brandon Williams | Brandon Williams |
| [CASSANDRA-336](https://issues.apache.org/jira/browse/CASSANDRA-336) | Merge batchmutation types and support batched deletes |  Major | . | Evan Weaver | Jeff Hodges |
| [CASSANDRA-599](https://issues.apache.org/jira/browse/CASSANDRA-599) | The pending tasks listed for the compaction pool is confusing |  Trivial | Tools | Tim Freeman | Jonathan Ellis |
| [CASSANDRA-654](https://issues.apache.org/jira/browse/CASSANDRA-654) | Type parameter hides surrounding type parameter in CacheTable\<k,v\>.CacheableObject\<v\> |  Trivial | . | gabriele renzi | gabriele renzi |
| [CASSANDRA-587](https://issues.apache.org/jira/browse/CASSANDRA-587) | consolidate json libraries |  Minor | . | Eric Evans | Eric Evans |
| [CASSANDRA-664](https://issues.apache.org/jira/browse/CASSANDRA-664) | add Perl autogen target to build.xml |  Minor | . | Ted Zlatanov | Ted Zlatanov |
| [CASSANDRA-659](https://issues.apache.org/jira/browse/CASSANDRA-659) | clean up MessagingService |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-672](https://issues.apache.org/jira/browse/CASSANDRA-672) | AbstractReplicationStrategy contains an unused IPartitioner |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-676](https://issues.apache.org/jira/browse/CASSANDRA-676) | stress.py should raise when keys are not found |  Minor | Tools | Brandon Williams | Brandon Williams |
| [CASSANDRA-675](https://issues.apache.org/jira/browse/CASSANDRA-675) | TcpReader is slow because of using Exception handling in the normal path |  Major | . | Jun Rao | Jun Rao |
| [CASSANDRA-686](https://issues.apache.org/jira/browse/CASSANDRA-686) | update thrift interface definition for new API versioning |  Minor | . | Eric Evans | Eric Evans |
| [CASSANDRA-684](https://issues.apache.org/jira/browse/CASSANDRA-684) | clean up "stage" code |  Trivial | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-701](https://issues.apache.org/jira/browse/CASSANDRA-701) | clean up message deserialization confusion |  Trivial | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-679](https://issues.apache.org/jira/browse/CASSANDRA-679) | SSTableExport should be able to enumerate keys |  Trivial | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-700](https://issues.apache.org/jira/browse/CASSANDRA-700) | Replacing the many forms of singleton instance methods |  Major | . | Jeff Hodges | Jeff Hodges |
| [CASSANDRA-706](https://issues.apache.org/jira/browse/CASSANDRA-706) | final classes are so lame |  Major | . | Jeff Hodges |  |
| [CASSANDRA-688](https://issues.apache.org/jira/browse/CASSANDRA-688) | cache refinements |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-728](https://issues.apache.org/jira/browse/CASSANDRA-728) | refactor blocking io to use thread (and queue) per conn pool, not thread per socket |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-667](https://issues.apache.org/jira/browse/CASSANDRA-667) | Avoid causing IO contention when cleaning up compacted SSTables |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-702](https://issues.apache.org/jira/browse/CASSANDRA-702) | stop smoothing out JMX stats |  Minor | Tools | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-748](https://issues.apache.org/jira/browse/CASSANDRA-748) | org.apache.cassandra.concurrent:type=COMMITLOG should actually be org.apache.cassandra.db:type=COMMITLOG |  Trivial | Tools | Robert Coli | Robert Coli |
| [CASSANDRA-733](https://issues.apache.org/jira/browse/CASSANDRA-733) | track latency in nanoseconds, not milliseconds |  Minor | . | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-746](https://issues.apache.org/jira/browse/CASSANDRA-746) | seeds silently don't bootstrap |  Trivial | . | Brandon Williams | Gary Dusbabek |
| [CASSANDRA-771](https://issues.apache.org/jira/browse/CASSANDRA-771) | Allow range queries against non-primary nodes for a given range |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-773](https://issues.apache.org/jira/browse/CASSANDRA-773) | Use Map.entrySet and Map.values instead of multiple lookups from Map.keySet |  Trivial | . | gabriele renzi | gabriele renzi |
| [CASSANDRA-780](https://issues.apache.org/jira/browse/CASSANDRA-780) | Refactor Bootstrapper |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-765](https://issues.apache.org/jira/browse/CASSANDRA-765) | stress.py should support get\_range\_slice |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-802](https://issues.apache.org/jira/browse/CASSANDRA-802) | use Ivy for lib/ jars where possible |  Minor | . | Eric Evans | Eric Evans |
| [CASSANDRA-799](https://issues.apache.org/jira/browse/CASSANDRA-799) | memtable sort is the bottleneck for range query performance |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-807](https://issues.apache.org/jira/browse/CASSANDRA-807) | Improve cassandra-cli error display and thrift connection checking |  Major | Tools | Mark Wolfe | Mark Wolfe |
| [CASSANDRA-831](https://issues.apache.org/jira/browse/CASSANDRA-831) | automate license audits using ant |  Major | Tools | Eric Evans | Eric Evans |
| [CASSANDRA-801](https://issues.apache.org/jira/browse/CASSANDRA-801) | The Key Cache should be configurable by absolute numbers |  Major | . | Ryan King | Stu Hood |
| [CASSANDRA-822](https://issues.apache.org/jira/browse/CASSANDRA-822) | A natural endpoint should be responsible for hints |  Major | . | Ryan King | Jonathan Ellis |
| [CASSANDRA-843](https://issues.apache.org/jira/browse/CASSANDRA-843) | Issue warnings for large rows |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-849](https://issues.apache.org/jira/browse/CASSANDRA-849) | Clarify where cassandra listens for clients in log message |  Trivial | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-878](https://issues.apache.org/jira/browse/CASSANDRA-878) | CalloutLocation and StagingFileDirectory are no longer needed. |  Trivial | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-886](https://issues.apache.org/jira/browse/CASSANDRA-886) | Obsolete configuration in DatabaseDescriptor |  Trivial | . | Roger Schildmeijer | Roger Schildmeijer |
| [CASSANDRA-898](https://issues.apache.org/jira/browse/CASSANDRA-898) | Add LoadBytes JMX Attribute (similar to LoadString) to help tracking node load |  Trivial | . | Juho Mäkinen | Juho Mäkinen |
| [CASSANDRA-888](https://issues.apache.org/jira/browse/CASSANDRA-888) | CommitLogDirectory matching a DataFileDirectory should be disallowed |  Trivial | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-863](https://issues.apache.org/jira/browse/CASSANDRA-863) | Default cache levels should be explicit, not percentage |  Trivial | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-901](https://issues.apache.org/jira/browse/CASSANDRA-901) | add row compaction stats to nodetool |  Trivial | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-789](https://issues.apache.org/jira/browse/CASSANDRA-789) | Add configurable range sizes, paging to hadoop range queries |  Minor | . | Jonathan Ellis | Johan Oskarsson |
| [CASSANDRA-890](https://issues.apache.org/jira/browse/CASSANDRA-890) | Get Hadoop input format sub splits in parallel |  Major | . | Johan Oskarsson | Johan Oskarsson |
| [CASSANDRA-908](https://issues.apache.org/jira/browse/CASSANDRA-908) | stress.py fixes and improvements |  Minor | . | Brandon Williams | Brandon Williams |
| [CASSANDRA-916](https://issues.apache.org/jira/browse/CASSANDRA-916) | StreamingServiceMBean doesn't update status when a node is receiving stream data |  Trivial | . | Gary Dusbabek | Gary Dusbabek |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-601](https://issues.apache.org/jira/browse/CASSANDRA-601) | Remove -Dcassandra from the command line that bin/cassandra generates |  Trivial | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-338](https://issues.apache.org/jira/browse/CASSANDRA-338) | Fix low-hanging scoping and other issues picked up by findbugs |  Trivial | . | Michael Greene | Michael Greene |
| [CASSANDRA-631](https://issues.apache.org/jira/browse/CASSANDRA-631) | possible NPE in StorageProxy? |  Major | . | gabriele renzi | gabriele renzi |
| [CASSANDRA-614](https://issues.apache.org/jira/browse/CASSANDRA-614) | Fix Findbugs: Static initializers |  Major | . | Stu Hood | Jonathan Ellis |
| [CASSANDRA-655](https://issues.apache.org/jira/browse/CASSANDRA-655) | need serialVersionUID for Token |  Minor | . | Eric Evans | Eric Evans |
| [CASSANDRA-624](https://issues.apache.org/jira/browse/CASSANDRA-624) | "maven install" does not find libthrift-r808609.jar |  Minor | Tools | Christophe Pierret |  |
| [CASSANDRA-669](https://issues.apache.org/jira/browse/CASSANDRA-669) | Handle mmapping index files greater than 2GB |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-658](https://issues.apache.org/jira/browse/CASSANDRA-658) | Hinted Handoff CF contention |  Major | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-680](https://issues.apache.org/jira/browse/CASSANDRA-680) | hinted handoff reads all hints for a single keyspace into memory |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-689](https://issues.apache.org/jira/browse/CASSANDRA-689) | Multi-get slice failing Nullpointer Exception |  Major | . | Chris Goffinet | Jonathan Ellis |
| [CASSANDRA-715](https://issues.apache.org/jira/browse/CASSANDRA-715) | HHOM goes into infinite loop, wasting cpu |  Major | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-717](https://issues.apache.org/jira/browse/CASSANDRA-717) | register AES verbs at SS start |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-695](https://issues.apache.org/jira/browse/CASSANDRA-695) | Gossiper convicts same node over and over. |  Trivial | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-719](https://issues.apache.org/jira/browse/CASSANDRA-719) | fix failing CompactionsPurgeTest |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-657](https://issues.apache.org/jira/browse/CASSANDRA-657) | AntiEntropyService causes lots of errors to be logged during unit tests |  Trivial | . | Jonathan Ellis | Stu Hood |
| [CASSANDRA-737](https://issues.apache.org/jira/browse/CASSANDRA-737) | Streaming code relies on sockets being bound to the correct address (InetAddress.anyLocalAddress() is bad) |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-644](https://issues.apache.org/jira/browse/CASSANDRA-644) | Provide way to remove nodes from gossip entirely |  Minor | Tools | Jonathan Ellis | Jaakko Laine |
| [CASSANDRA-724](https://issues.apache.org/jira/browse/CASSANDRA-724) | Insert/Get Contention |  Major | . | Chris Goffinet | Jonathan Ellis |
| [CASSANDRA-770](https://issues.apache.org/jira/browse/CASSANDRA-770) | JMX RowCache requests also include writes |  Minor | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-763](https://issues.apache.org/jira/browse/CASSANDRA-763) | getRangeSlice returns keys from outside the desired range |  Major | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-772](https://issues.apache.org/jira/browse/CASSANDRA-772) | DeletingReferences not created for existing sstables on startup |  Major | . | Stu Hood | Stu Hood |
| [CASSANDRA-778](https://issues.apache.org/jira/browse/CASSANDRA-778) | Gossiper thread deadlock |  Major | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-779](https://issues.apache.org/jira/browse/CASSANDRA-779) | Bootstrapping is not threadsafe |  Major | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-766](https://issues.apache.org/jira/browse/CASSANDRA-766) | SSTableExport can not accept -f , -k options |  Minor | Tools | Santal Li | Jonathan Ellis |
| [CASSANDRA-796](https://issues.apache.org/jira/browse/CASSANDRA-796) | binary artifacts need ivy for dependencies |  Minor | Tools | Eric Evans | Eric Evans |
| [CASSANDRA-797](https://issues.apache.org/jira/browse/CASSANDRA-797) | cassandra-cli.bat batch file not passing arguments onto the main class |  Major | Tools | Mark Wolfe | Mark Wolfe |
| [CASSANDRA-804](https://issues.apache.org/jira/browse/CASSANDRA-804) | Warn operator when there is not enough disk space for compaction |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-806](https://issues.apache.org/jira/browse/CASSANDRA-806) | Unit tests log RTE to stderr even though tests still succeed. |  Minor | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-787](https://issues.apache.org/jira/browse/CASSANDRA-787) | NPE in DatacenterShardStatergy |  Minor | . | Vijay | Vijay |
| [CASSANDRA-817](https://issues.apache.org/jira/browse/CASSANDRA-817) | Wordcount contrib does not work in Hadoop distributed mode |  Minor | . | Johan Oskarsson | Johan Oskarsson |
| [CASSANDRA-816](https://issues.apache.org/jira/browse/CASSANDRA-816) | Wordcount contrib is not including all required jars |  Minor | . | Johan Oskarsson | Johan Oskarsson |
| [CASSANDRA-814](https://issues.apache.org/jira/browse/CASSANDRA-814) | Compaction bucketizing is computing average size incorrectly |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-824](https://issues.apache.org/jira/browse/CASSANDRA-824) | Unable to start Cassandra in windows if P drive exsists |  Trivial | . | Kaz Walker | Gary Dusbabek |
| [CASSANDRA-738](https://issues.apache.org/jira/browse/CASSANDRA-738) | cassandra-cli parsing error |  Minor | Tools | Johan Hilding | Eric Evans |
| [CASSANDRA-828](https://issues.apache.org/jira/browse/CASSANDRA-828) | possible NPE in StorageService |  Minor | . | gabriele renzi | gabriele renzi |
| [CASSANDRA-711](https://issues.apache.org/jira/browse/CASSANDRA-711) | Some Thrift Exceptions not passed down to Client |  Minor | . | Chris Goffinet | Jonathan Ellis |
| [CASSANDRA-853](https://issues.apache.org/jira/browse/CASSANDRA-853) | ConcurrentModificationException |  Major | . | B. Todd Burruss | gabriele renzi |
| [CASSANDRA-837](https://issues.apache.org/jira/browse/CASSANDRA-837) | hadoop recordreader hardcodes row count |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-858](https://issues.apache.org/jira/browse/CASSANDRA-858) | cassandra-cli.bat fails to start cli client |  Trivial | Tools | Morten Wegelbye Holm | Morten Wegelbye Holm |
| [CASSANDRA-867](https://issues.apache.org/jira/browse/CASSANDRA-867) | Invalid host/port parameters to cassandra-cli leaves system in unrecoverable state |  Minor | Tools | Morten Wegelbye Holm | Morten Wegelbye Holm |
| [CASSANDRA-864](https://issues.apache.org/jira/browse/CASSANDRA-864) | ConcurrentModificationException during QuorumResponseHandler |  Minor | . | B. Todd Burruss | Jonathan Ellis |
| [CASSANDRA-761](https://issues.apache.org/jira/browse/CASSANDRA-761) | Binary Memtable does not invalidate cache |  Major | . | Chris Goffinet | Jonathan Ellis |
| [CASSANDRA-882](https://issues.apache.org/jira/browse/CASSANDRA-882) | RowWarningThreshold doesn't allow values more than about 2GB |  Trivial | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-899](https://issues.apache.org/jira/browse/CASSANDRA-899) | skipBytes in SSTable\*Iterator is in an assert |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-902](https://issues.apache.org/jira/browse/CASSANDRA-902) | Bootstrapping might skip needed ranges. |  Major | . | Gary Dusbabek | Gary Dusbabek |
| [CASSANDRA-907](https://issues.apache.org/jira/browse/CASSANDRA-907) | TimeUUID comparator identify different UUID as long as they have same timestamp |  Minor | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-884](https://issues.apache.org/jira/browse/CASSANDRA-884) | get\_range\_slice returns multiple copies of each row for ConsistencyLevel \> ONE |  Major | . | Omer van der Horst Jansen | Omer van der Horst Jansen |
| [CASSANDRA-885](https://issues.apache.org/jira/browse/CASSANDRA-885) | OOM on Commit log Replay |  Major | . | Paul Querna | Jonathan Ellis |
| [CASSANDRA-857](https://issues.apache.org/jira/browse/CASSANDRA-857) | AssertionError in MappedFileDataInput.skipBytes when slicing a large number of keys |  Major | . | Robert Edmonds | Jonathan Ellis |
| [CASSANDRA-834](https://issues.apache.org/jira/browse/CASSANDRA-834) | Exception during batch\_mutate |  Minor | . | Brandon Williams | Jonathan Ellis |
| [CASSANDRA-677](https://issues.apache.org/jira/browse/CASSANDRA-677) | cassandra-cli doesn't allow hyphens in hostnames |  Minor | Tools | Eric Evans | Roger Schildmeijer |
| [CASSANDRA-911](https://issues.apache.org/jira/browse/CASSANDRA-911) | QuorumResponseHandler sets timeout incorrectly |  Minor | . | Chris Goffinet | Chris Goffinet |
| [CASSANDRA-850](https://issues.apache.org/jira/browse/CASSANDRA-850) | Ant release target doesn't include all jars in binary tarball |  Major | . | Johan Oskarsson | Eric Evans |
| [CASSANDRA-923](https://issues.apache.org/jira/browse/CASSANDRA-923) | Enable DEBUG file logging during unit tests |  Major | . | Stu Hood | Stu Hood |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-751](https://issues.apache.org/jira/browse/CASSANDRA-751) | refactor streaming |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-823](https://issues.apache.org/jira/browse/CASSANDRA-823) | Add method to set Hadoop InputSlit sizes |  Minor | . | Johan Oskarsson | Johan Oskarsson |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-854](https://issues.apache.org/jira/browse/CASSANDRA-854) | Can't run cassanfra-cli as a role user - getting permission denied |  Trivial | Tools | Michael | Eric Evans |
| [CASSANDRA-845](https://issues.apache.org/jira/browse/CASSANDRA-845) | nothing appearing to happen during bootstrap while waiting for anticompaction is still confusing |  Minor | . | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-600](https://issues.apache.org/jira/browse/CASSANDRA-600) | storageproxy methods should throw TimeoutException instead of [thrift] TimedOut |  Minor | . | Jonathan Ellis | Todd Blose |
| [CASSANDRA-656](https://issues.apache.org/jira/browse/CASSANDRA-656) | remove DataInputBuffer |  Minor | . | Jonathan Ellis | Todd Blose |
| [CASSANDRA-710](https://issues.apache.org/jira/browse/CASSANDRA-710) | remove deprecated get\_key\_range api |  Minor | . | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-705](https://issues.apache.org/jira/browse/CASSANDRA-705) | explore replacing socket nio with blocking io |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-783](https://issues.apache.org/jira/browse/CASSANDRA-783) | refactor CommitLog |  Minor | . | Jonathan Ellis | Jonathan Ellis |
| [CASSANDRA-813](https://issues.apache.org/jira/browse/CASSANDRA-813) | add some kind of logging of GC activity |  Minor | . | Jonathan Ellis | Brandon Williams |
| [CASSANDRA-880](https://issues.apache.org/jira/browse/CASSANDRA-880) | add "drain" command |  Major | Tools | Jonathan Ellis | Gary Dusbabek |
| [CASSANDRA-903](https://issues.apache.org/jira/browse/CASSANDRA-903) | create py\_stress README |  Minor | . | Jonathan Ellis | Brandon Williams |


