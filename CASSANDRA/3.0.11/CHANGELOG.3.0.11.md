
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 3.0.11 - 2017-02-21



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12967](https://issues.apache.org/jira/browse/CASSANDRA-12967) | Spec for Cassandra RPM Build |  Major | Packaging | Bhuvan Rawal | Michael Shuler |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12186](https://issues.apache.org/jira/browse/CASSANDRA-12186) | anticompaction log message doesn't include the parent repair session id |  Minor | Observability | Wei Deng | Tommy Stendahl |
| [CASSANDRA-12883](https://issues.apache.org/jira/browse/CASSANDRA-12883) | Remove support for non-JavaScript UDFs |  Minor | CQL | Robert Stupp | Robert Stupp |
| [CASSANDRA-12981](https://issues.apache.org/jira/browse/CASSANDRA-12981) | Refactor ColumnCondition |  Major | CQL | Benjamin Lerer | Benjamin Lerer |
| [CASSANDRA-13180](https://issues.apache.org/jira/browse/CASSANDRA-13180) | Better handling of missing entries in system\_schema.columns during startup |  Major | Distributed Metadata | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-13205](https://issues.apache.org/jira/browse/CASSANDRA-13205) | Hint related logging should include the IP address of the destination in addition to host ID |  Trivial | . | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-13008](https://issues.apache.org/jira/browse/CASSANDRA-13008) | Add vm.max\_map\_count StartupCheck |  Minor | Configuration | Jay Zhuang | Jay Zhuang |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12535](https://issues.apache.org/jira/browse/CASSANDRA-12535) | Prevent reloading of logback.xml from UDF sandbox |  Minor | . | Pat Patterson | Robert Stupp |
| [CASSANDRA-12911](https://issues.apache.org/jira/browse/CASSANDRA-12911) | cassandra.yaml mentions wrong table names for PasswordAuthenticator/CassandraAuthorizer |  Trivial | Core | Eduard Tudenhoefner | Eduard Tudenhoefner |
| [CASSANDRA-12901](https://issues.apache.org/jira/browse/CASSANDRA-12901) | Repair can hang if node dies during sync or anticompaction |  Major | Streaming and Messaging | Paulo Motta | Paulo Motta |
| [CASSANDRA-12281](https://issues.apache.org/jira/browse/CASSANDRA-12281) | Gossip blocks on startup when there are pending range movements |  Major | Core | Eric Evans | Stefan Podkowinski |
| [CASSANDRA-12934](https://issues.apache.org/jira/browse/CASSANDRA-12934) | AnticompactionRequestSerializer serializedSize is incorrect |  Major | Core | Jason Brown | Jason Brown |
| [CASSANDRA-12899](https://issues.apache.org/jira/browse/CASSANDRA-12899) | stand alone sstableupgrade prints LEAK DETECTED warnings some times |  Major | . | Jeremiah Jordan | Yuki Morishita |
| [CASSANDRA-12651](https://issues.apache.org/jira/browse/CASSANDRA-12651) | Failure in SecondaryIndexTest.testAllowFilteringOnPartitionKeyWithSecondaryIndex |  Critical | Testing | Joel Knighton | Sam Tunnicliffe |
| [CASSANDRA-12914](https://issues.apache.org/jira/browse/CASSANDRA-12914) | cqlsh describe fails with "list[i] not a string for i in..." |  Minor | Tools | Adam Holmberg | Adam Holmberg |
| [CASSANDRA-12900](https://issues.apache.org/jira/browse/CASSANDRA-12900) | Resurrect or remove HeapPool (unslabbed\_heap\_buffers) |  Major | . | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-12739](https://issues.apache.org/jira/browse/CASSANDRA-12739) | Nodetool uses cassandra-env.sh MAX\_HEAP\_SIZE if set |  Major | Tools | Brad Vernon |  |
| [CASSANDRA-12868](https://issues.apache.org/jira/browse/CASSANDRA-12868) | Reject default\_time\_to\_live option when creating or altering MVs |  Minor | . | Srinivasarao Daruna | Sundar Srinivasan |
| [CASSANDRA-12935](https://issues.apache.org/jira/browse/CASSANDRA-12935) | Use saved tokens when setting local tokens on StorageService.joinRing() |  Minor | Coordination | Paulo Motta | Paulo Motta |
| [CASSANDRA-12817](https://issues.apache.org/jira/browse/CASSANDRA-12817) | testall failure in org.apache.cassandra.cql3.validation.entities.UFTest.testAllNativeTypes |  Major | . | Sean McCarthy | Robert Stupp |
| [CASSANDRA-12768](https://issues.apache.org/jira/browse/CASSANDRA-12768) | CQL often queries static columns unnecessarily |  Major | . | Sylvain Lebresne | Sylvain Lebresne |
| [CASSANDRA-12694](https://issues.apache.org/jira/browse/CASSANDRA-12694) | PAXOS Update Corrupted empty row exception |  Major | Local Write-Read Paths | Cameron Zemek | Alex Petrov |
| [CASSANDRA-12673](https://issues.apache.org/jira/browse/CASSANDRA-12673) | Nodes cannot see each other in multi-DC, non-EC2 environment with two-interface nodes due to outbound node-to-node connection binding to private interface |  Minor | Core | Milan Majercik | Milan Majercik |
| [CASSANDRA-12781](https://issues.apache.org/jira/browse/CASSANDRA-12781) | Disable RPC\_READY gossip flag when shutting down client servers |  Major | Distributed Metadata | Sean McCarthy | Stefania |
| [CASSANDRA-13033](https://issues.apache.org/jira/browse/CASSANDRA-13033) | Thread local pools never cleaned up |  Major | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-12796](https://issues.apache.org/jira/browse/CASSANDRA-12796) | Heap exhaustion when rebuilding secondary index over a table with wide partitions |  Critical | Core, Secondary Indexes | Milan Majercik | Sam Tunnicliffe |
| [CASSANDRA-13021](https://issues.apache.org/jira/browse/CASSANDRA-13021) | Nodetool compactionstats fails with NullPointerException |  Major | . | Jay Zhuang | Jay Zhuang |
| [CASSANDRA-13040](https://issues.apache.org/jira/browse/CASSANDRA-13040) | Estimated TS drop-time histogram updated with Cell.NO\_DELETION\_TIME |  Major | . | Stefan Podkowinski | Stefan Podkowinski |
| [CASSANDRA-12984](https://issues.apache.org/jira/browse/CASSANDRA-12984) | MVs are built unnecessarily again after bootstrap |  Major | Core | Benjamin Roth | Benjamin Roth |
| [CASSANDRA-8616](https://issues.apache.org/jira/browse/CASSANDRA-8616) | sstable tools may result in commit log segments be written |  Major | Tools | Tyler Hobbs | Yuki Morishita |
| [CASSANDRA-12905](https://issues.apache.org/jira/browse/CASSANDRA-12905) | Retry acquire MV lock on failure instead of throwing WTE on streaming |  Critical | Materialized Views, Streaming and Messaging | Nir Zilka | Benjamin Roth |
| [CASSANDRA-12620](https://issues.apache.org/jira/browse/CASSANDRA-12620) | Resurrected rows with expired TTL on update to 3.x |  Major | Local Write-Read Paths | Collin Sauve | Benjamin Lerer |
| [CASSANDRA-12959](https://issues.apache.org/jira/browse/CASSANDRA-12959) | copy from csv import wrong values with udt having set when fields are not specified in correct order in csv |  Major | Tools | Quentin Ambard | Stefania |
| [CASSANDRA-12909](https://issues.apache.org/jira/browse/CASSANDRA-12909) | cqlsh copy cannot parse strings when counters are present |  Major | Tools | Stefania | Stefania |
| [CASSANDRA-13074](https://issues.apache.org/jira/browse/CASSANDRA-13074) | DynamicEndpointSnitch frequently no-ops through early exit in multi-datacenter situations |  Major | Coordination | Joel Knighton | Joel Knighton |
| [CASSANDRA-12348](https://issues.apache.org/jira/browse/CASSANDRA-12348) | Flaky failures in SSTableRewriterTest.basicTest2/getPositionsTest |  Major | Testing | Joel Knighton | Stefania |
| [CASSANDRA-12453](https://issues.apache.org/jira/browse/CASSANDRA-12453) | AutoSavingCache does not store required keys making RowCacheTests Flaky |  Minor | Core | sankalp kohli | Jay Zhuang |
| [CASSANDRA-13046](https://issues.apache.org/jira/browse/CASSANDRA-13046) | Missing PID file directory when installing from RPM |  Minor | Packaging | Marcel Dopita | Michael Shuler |
| [CASSANDRA-12794](https://issues.apache.org/jira/browse/CASSANDRA-12794) | COPY FROM with NULL='' fails when inserting empty row in primary key |  Major | Tools | Sucwinder Bassi | Stefania |
| [CASSANDRA-12979](https://issues.apache.org/jira/browse/CASSANDRA-12979) | checkAvailableDiskSpace doesn't update expectedWriteSize when reducing thread scope |  Major | . | Jon Haddad | Jon Haddad |
| [CASSANDRA-12856](https://issues.apache.org/jira/browse/CASSANDRA-12856) | dtest failure in replication\_test.SnitchConfigurationUpdateTest.test\_cannot\_restart\_with\_different\_rack |  Major | . | Sean McCarthy | Stefania |
| [CASSANDRA-11431](https://issues.apache.org/jira/browse/CASSANDRA-11431) | java: error: unmappable character for encoding ASCII |  Major | Packaging | Lorena Pesantez | Jay Zhuang |
| [CASSANDRA-12997](https://issues.apache.org/jira/browse/CASSANDRA-12997) | Use timestamp from ClientState by default in AlterTableStatement |  Minor | . | Sean McCarthy | Sylvain Lebresne |
| [CASSANDRA-12203](https://issues.apache.org/jira/browse/CASSANDRA-12203) | AssertionError on compaction after upgrade (2.1.9 -\> 3.7) |  Critical | Core | Roman S. Borschel | Yuki Morishita |
| [CASSANDRA-12563](https://issues.apache.org/jira/browse/CASSANDRA-12563) | Stress daemon help is incorrect |  Trivial | Tools | Christopher Batey | Christopher Batey |
| [CASSANDRA-13115](https://issues.apache.org/jira/browse/CASSANDRA-13115) | Read repair is not blocking repair to finish in foreground repair |  Major | . | Xiaolong Jiang | Sylvain Lebresne |
| [CASSANDRA-13075](https://issues.apache.org/jira/browse/CASSANDRA-13075) | Indexer is not correctly invoked when building indexes over sstables |  Critical | . | Sergio Bossa | Alex Petrov |
| [CASSANDRA-12443](https://issues.apache.org/jira/browse/CASSANDRA-12443) | Remove alter type support |  Major | . | Carl Yeksigian | Benjamin Lerer |
| [CASSANDRA-12925](https://issues.apache.org/jira/browse/CASSANDRA-12925) | AssertionError executing authz stmt on UDF without keyspace |  Minor | CQL | Sam Tunnicliffe | Sam Tunnicliffe |
| [CASSANDRA-13025](https://issues.apache.org/jira/browse/CASSANDRA-13025) | dtest failure in upgrade\_tests.cql\_tests.TestCQLNodes3RF3\_Upgrade\_current\_3\_x\_To\_indev\_3\_x.static\_columns\_with\_distinct\_test |  Critical | Testing | Sean McCarthy | Sylvain Lebresne |
| [CASSANDRA-12829](https://issues.apache.org/jira/browse/CASSANDRA-12829) | DELETE query with an empty IN clause can delete more than expected |  Major | CQL | Jason T. Bradshaw | Alex Petrov |
| [CASSANDRA-13117](https://issues.apache.org/jira/browse/CASSANDRA-13117) | Dump threads when unit test times out |  Minor | Testing | Marcus Eriksson | Marcus Eriksson |
| [CASSANDRA-13177](https://issues.apache.org/jira/browse/CASSANDRA-13177) | sstabledump doesn't handle non-empty partitions with a partition-level deletion correctly |  Major | Tools | Tyler Hobbs | Tyler Hobbs |
| [CASSANDRA-13009](https://issues.apache.org/jira/browse/CASSANDRA-13009) | Speculative retry bugs |  Major | . | Simon Zhou | Simon Zhou |
| [CASSANDRA-13124](https://issues.apache.org/jira/browse/CASSANDRA-13124) | Abort or retry on failed hints delivery |  Minor | . | Paulo Motta | Stefan Podkowinski |
| [CASSANDRA-13152](https://issues.apache.org/jira/browse/CASSANDRA-13152) | UPDATE on counter columns with empty list as argument in IN disables cluster |  Major | . | jorge collinet | Benjamin Lerer |
| [CASSANDRA-13125](https://issues.apache.org/jira/browse/CASSANDRA-13125) | Duplicate rows after upgrading from 2.1.16 to 3.0.10/3.9 |  Critical | . | Zhongxiang Zheng | Sylvain Lebresne |
| [CASSANDRA-13109](https://issues.apache.org/jira/browse/CASSANDRA-13109) | Lightweight transactions temporarily fail after upgrade from 2.1 to 3.0 |  Major | . | Samuel Klock | Samuel Klock |
| [CASSANDRA-13114](https://issues.apache.org/jira/browse/CASSANDRA-13114) | Upgrade netty to 4.0.44 to fix memory leak with client encryption |  Blocker | . | Tom van der Woerdt | Stefan Podkowinski |
| [CASSANDRA-13173](https://issues.apache.org/jira/browse/CASSANDRA-13173) | Reloading logback.xml does not work |  Minor | . | Robert Stupp | Robert Stupp |
| [CASSANDRA-12539](https://issues.apache.org/jira/browse/CASSANDRA-12539) | Empty CommitLog prevents restart |  Major | . | Stefano Ortolani | Benjamin Lerer |
| [CASSANDRA-13159](https://issues.apache.org/jira/browse/CASSANDRA-13159) | Coalescing strategy can enter infinite loop |  Major | Streaming and Messaging | Corentin Chary | Corentin Chary |
| [CASSANDRA-13204](https://issues.apache.org/jira/browse/CASSANDRA-13204) | Thread Leak in OutboundTcpConnection |  Major | . | sankalp kohli | Jason Brown |
| [CASSANDRA-12876](https://issues.apache.org/jira/browse/CASSANDRA-12876) | Negative mean write latency |  Major | Observability | Kévin LOVATO | Per Otterström |
| [CASSANDRA-13219](https://issues.apache.org/jira/browse/CASSANDRA-13219) | Cassandra.yaml now unicode instead of ascii after 13090 |  Minor | Configuration | Philip Thompson | Ariel Weisberg |
| [CASSANDRA-9639](https://issues.apache.org/jira/browse/CASSANDRA-9639) | size\_estimates is inacurate in multi-dc clusters |  Minor | . | Sebastian Estevez | Chris Lohfink |
| [CASSANDRA-13211](https://issues.apache.org/jira/browse/CASSANDRA-13211) | Use portable stderr for java error in startup |  Major | . | Max Bowsher | Michael Shuler |
| [CASSANDRA-13018](https://issues.apache.org/jira/browse/CASSANDRA-13018) | Exceptions encountered calling getSeeds() breaks OTC thread |  Major | Streaming and Messaging | Jeff Jirsa | Jeff Jirsa |
| [CASSANDRA-12368](https://issues.apache.org/jira/browse/CASSANDRA-12368) | Background read repair not fixing replicas |  Major | Coordination, Local Write-Read Paths | Paulo Motta | Alex Petrov |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-12894](https://issues.apache.org/jira/browse/CASSANDRA-12894) | testall failure in org.apache.cassandra.hints.HintsBufferPoolTest.testBackpressure-compression |  Major | . | Michael Shuler | Ariel Weisberg |


