
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Cassandra Changelog

## Release 0.7.2 - 2011-02-16



### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [CASSANDRA-2128](https://issues.apache.org/jira/browse/CASSANDRA-2128) | Corrupted Commit logs |  Major | . | Paul Querna | Jonathan Ellis |
| [CASSANDRA-2136](https://issues.apache.org/jira/browse/CASSANDRA-2136) | CLI does not use sub-comparator on Super CF \`get\`. |  Minor | Tools | Pavel Yaskevich | Pavel Yaskevich |
| [CASSANDRA-2146](https://issues.apache.org/jira/browse/CASSANDRA-2146) | cli read\_repair\_chance input not validated |  Minor | . | Chris Burroughs | Pavel Yaskevich |
| [CASSANDRA-2165](https://issues.apache.org/jira/browse/CASSANDRA-2165) | EOFException during name query |  Major | . | Sylvain Lebresne | Jonathan Ellis |


