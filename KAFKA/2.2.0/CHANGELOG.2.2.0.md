
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Kafka Changelog

## Release 2.2.0 - Unreleased (as of 2018-10-20)



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [KAFKA-7096](https://issues.apache.org/jira/browse/KAFKA-7096) | Consumer should drop the data for unassigned topic partitions |  Major | . | Mayuresh Gharat | Mayuresh Gharat |
| [KAFKA-7266](https://issues.apache.org/jira/browse/KAFKA-7266) | Fix MetricsTest test flakiness |  Minor | . | Stanislav Kozlovski | Stanislav Kozlovski |
| [KAFKA-7478](https://issues.apache.org/jira/browse/KAFKA-7478) | Reduce OAuthBearerLoginModule verbosity |  Minor | . | Stanislav Kozlovski | Stanislav Kozlovski |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [KAFKA-5950](https://issues.apache.org/jira/browse/KAFKA-5950) | AdminClient should retry based on returned error codes |  Major | . | Ismael Juma | Andrey Dyachkov |
| [KAFKA-7476](https://issues.apache.org/jira/browse/KAFKA-7476) | SchemaProjector is not properly handling Date-based logical types |  Major | KafkaConnect | Robert Yokota | Robert Yokota |


