
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Hadoop: Ozone Changelog

## Release 0.4.0 - Unreleased (as of 2018-10-20)



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [HDDS-645](https://issues.apache.org/jira/browse/HDDS-645) | Enable OzoneFS contract tests by default |  Blocker | test | Arpit Agarwal | Arpit Agarwal |
| [HDDS-654](https://issues.apache.org/jira/browse/HDDS-654) | Add ServiceLoader resource for OzoneFileSystem |  Major | Ozone Filesystem | Jitendra Nath Pandey | Jitendra Nath Pandey |
| [HDDS-439](https://issues.apache.org/jira/browse/HDDS-439) | 'ozone oz volume create' should default to current logged in user |  Blocker | Tools | Arpit Agarwal | Dinesh Chitlangia |
| [HDDS-378](https://issues.apache.org/jira/browse/HDDS-378) | Remove dependencies between hdds/ozone and hdfs proto files |  Major | . | Elek, Marton | Elek, Marton |
| [HDDS-651](https://issues.apache.org/jira/browse/HDDS-651) | Rename o3 to o3fs for Filesystem |  Blocker | . | Namit Maheshwari | Jitendra Nath Pandey |
| [HDDS-621](https://issues.apache.org/jira/browse/HDDS-621) | ozone genconf improvements |  Major | . | Arpit Agarwal | Dinesh Chitlangia |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [HDDS-583](https://issues.apache.org/jira/browse/HDDS-583) | SCM returns zero as the return code, even when invalid options are passed |  Major | . | Namit Maheshwari | Namit Maheshwari |
| [HDDS-450](https://issues.apache.org/jira/browse/HDDS-450) | Generate BlockCommitSequenceId in ContainerStateMachine for every commit operation in Ratis |  Major | Ozone Datanode | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-590](https://issues.apache.org/jira/browse/HDDS-590) | Add unit test for HDDS-583 |  Major | . | Namit Maheshwari | Namit Maheshwari |
| [HDDS-608](https://issues.apache.org/jira/browse/HDDS-608) | Access denied exception for user hdfs while making getContainerWithPipeline call. |  Major | SCM | Namit Maheshwari | Mukul Kumar Singh |
| [HDDS-601](https://issues.apache.org/jira/browse/HDDS-601) | On restart, SCM throws 'No such datanode' exception |  Major | SCM | Soumitra Sulav | Hanisha Koneru |
| [HDDS-627](https://issues.apache.org/jira/browse/HDDS-627) | OzoneFS read from an MR Job throws java.lang.ArrayIndexOutOfBoundsException |  Major | Ozone Filesystem | Soumitra Sulav | Mukul Kumar Singh |
| [HDDS-636](https://issues.apache.org/jira/browse/HDDS-636) | Turn off ozone.metastore.rocksdb.statistics for now |  Major | . | Arpit Agarwal | Arpit Agarwal |
| [HDDS-634](https://issues.apache.org/jira/browse/HDDS-634) | OzoneFS: support basic user group and permission for file/dir |  Major | . | Xiaoyu Yao | Xiaoyu Yao |
| [HDDS-609](https://issues.apache.org/jira/browse/HDDS-609) | On restart, SCM does not exit chill mode as it expects DNs to report containers in ALLOCATED state |  Major | . | Namit Maheshwari | Hanisha Koneru |
| [HDDS-628](https://issues.apache.org/jira/browse/HDDS-628) | Fix outdated names used in HDDS documentations |  Minor | document | Yiqun Lin | Yiqun Lin |
| [HDDS-641](https://issues.apache.org/jira/browse/HDDS-641) | Fix ozone filesystem robot test |  Major | Ozone Filesystem, test | Mukul Kumar Singh | Mukul Kumar Singh |
| [HDDS-639](https://issues.apache.org/jira/browse/HDDS-639) | ChunkGroupInputStream gets into infinite loop after reading a block |  Blocker | Ozone Client | Nanda kumar | Mukul Kumar Singh |
| [HDDS-624](https://issues.apache.org/jira/browse/HDDS-624) | PutBlock fails with Unexpected Storage Container Exception |  Major | . | Namit Maheshwari | Arpit Agarwal |
| [HDDS-644](https://issues.apache.org/jira/browse/HDDS-644) | Rename dfs.container.ratis.num.container.op.threads |  Minor | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-587](https://issues.apache.org/jira/browse/HDDS-587) | Add new classes for pipeline management |  Major | SCM | Lokesh Jain | Lokesh Jain |
| [HDDS-646](https://issues.apache.org/jira/browse/HDDS-646) | TestChunkStreams.testErrorReadGroupInputStream fails |  Major | test | Nanda kumar | Nanda kumar |
| [HDDS-653](https://issues.apache.org/jira/browse/HDDS-653) | TestMetadataStore#testIterator fails on Windows |  Major | test | Yiqun Lin | Yiqun Lin |
| [HDDS-603](https://issues.apache.org/jira/browse/HDDS-603) | Add BlockCommitSequenceId field per Container and expose it in Container Reports |  Major | . | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-579](https://issues.apache.org/jira/browse/HDDS-579) | ContainerStateMachine should fail subsequent transactions per container in case one fails |  Major | . | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-629](https://issues.apache.org/jira/browse/HDDS-629) | Make ApplyTransaction calls in ContainerStateMachine idempotent |  Major | . | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-490](https://issues.apache.org/jira/browse/HDDS-490) | Improve om and scm start up options |  Major | . | Namit Maheshwari | Namit Maheshwari |
| [HDDS-667](https://issues.apache.org/jira/browse/HDDS-667) | Fix TestOzoneFileInterfaces |  Major | Ozone Filesystem | Mukul Kumar Singh | Mukul Kumar Singh |
| [HDDS-656](https://issues.apache.org/jira/browse/HDDS-656) | Add logic for pipeline report and action processing in new pipeline code |  Major | SCM | Lokesh Jain | Lokesh Jain |
| [HDDS-662](https://issues.apache.org/jira/browse/HDDS-662) | Introduce ContainerReplicaState in StorageContainerManager |  Major | SCM | Nanda kumar | Nanda kumar |
| [HDDS-661](https://issues.apache.org/jira/browse/HDDS-661) | When a volume fails in datanode, VersionEndpointTask#call ends up in dead lock. |  Major | Ozone Datanode | Nanda kumar | Hanisha Koneru |
| [HDDS-673](https://issues.apache.org/jira/browse/HDDS-673) | Suppress "Key not found" exception log with stack trace in OM |  Major | . | Xiaoyu Yao | Arpit Agarwal |
| [HDDS-690](https://issues.apache.org/jira/browse/HDDS-690) | Javadoc build fails in hadoop-ozone |  Major | . | Takanobu Asanuma | Takanobu Asanuma |
| [HDDS-612](https://issues.apache.org/jira/browse/HDDS-612) | Even after setting hdds.scm.chillmode.enabled to false, SCM allocateblock fails with ChillModePrecheck exception |  Major | . | Namit Maheshwari | Hanisha Koneru |
| [HDDS-672](https://issues.apache.org/jira/browse/HDDS-672) | Spark shell throws OzoneFileSystem not found |  Major | . | Namit Maheshwari | Anu Engineer |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [HDDS-10](https://issues.apache.org/jira/browse/HDDS-10) | Add kdc docker image for secure ozone cluster |  Major | Security | Ajay Kumar | Ajay Kumar |
| [HDDS-588](https://issues.apache.org/jira/browse/HDDS-588) | SelfSignedCertificate#generateCertificate should sign the certificate the configured security provider |  Minor | Security | Xiaoyu Yao | Xiaoyu Yao |
| [HDDS-585](https://issues.apache.org/jira/browse/HDDS-585) | Handle common request identifiers in a transparent way |  Major | . | Elek, Marton | Elek, Marton |
| [HDDS-577](https://issues.apache.org/jira/browse/HDDS-577) | Support S3 buckets as first class objects in Ozone Manager - 2 |  Major | S3 | Anu Engineer | Bharat Viswanadham |
| [HDDS-591](https://issues.apache.org/jira/browse/HDDS-591) | Adding ASF license header to kadm5.acl |  Major | . | Xiaoyu Yao | Ajay Kumar |
| [HDDS-522](https://issues.apache.org/jira/browse/HDDS-522) | Implement PutBucket REST endpoint |  Major | . | Elek, Marton | Bharat Viswanadham |
| [HDDS-606](https://issues.apache.org/jira/browse/HDDS-606) | Create delete s3Bucket |  Major | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-445](https://issues.apache.org/jira/browse/HDDS-445) | Create a logger to print out all of the incoming requests |  Major | . | Elek, Marton | Bharat Viswanadham |
| [HDDS-613](https://issues.apache.org/jira/browse/HDDS-613) | Update  HeadBucket, DeleteBucket to not to have volume in path |  Major | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-657](https://issues.apache.org/jira/browse/HDDS-657) | Remove {volume} path segments from all the remaining rest endpoints |  Major | . | Elek, Marton | Elek, Marton |
| [HDDS-683](https://issues.apache.org/jira/browse/HDDS-683) | Add a shell command to provide ozone mapping for a S3Bucket |  Major | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-678](https://issues.apache.org/jira/browse/HDDS-678) | Format of Last-Modified header is invalid in HEAD Object call |  Major | . | Elek, Marton | Elek, Marton |
| [HDDS-680](https://issues.apache.org/jira/browse/HDDS-680) | Provide web based bucket browser. |  Major | S3 | Elek, Marton | Elek, Marton |
| [HDDS-701](https://issues.apache.org/jira/browse/HDDS-701) | Support key multi-delete |  Major | S3 | Elek, Marton | Elek, Marton |
| [HDDS-679](https://issues.apache.org/jira/browse/HDDS-679) | Add query parameter to the constructed query in VirtualHostStyleFilter |  Major | . | Elek, Marton | Bharat Viswanadham |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [HDDS-599](https://issues.apache.org/jira/browse/HDDS-599) | Fix TestOzoneConfiguration TestOzoneConfigurationFields |  Major | . | Bharat Viswanadham | Sandeep Nemuri |
| [HDDS-665](https://issues.apache.org/jira/browse/HDDS-665) | Add hdds.datanode.dir to docker-config |  Major | . | Bharat Viswanadham | Bharat Viswanadham |


