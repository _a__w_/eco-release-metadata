
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Hadoop: Ozone Changelog

## Release 0.2.1 - 2018-10-01



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [HDDS-96](https://issues.apache.org/jira/browse/HDDS-96) | Add an option in ozone script to generate a site file with minimally required ozone configs |  Major | . | Dinesh Chitlangia | Dinesh Chitlangia |
| [HDDS-192](https://issues.apache.org/jira/browse/HDDS-192) | Create new SCMCommand to request a replication of a container |  Major | SCM | Elek, Marton | Elek, Marton |
| [HDDS-198](https://issues.apache.org/jira/browse/HDDS-198) | Create AuditLogger mechanism to be used by OM, SCM and Datanode |  Major | . | Dinesh Chitlangia | Dinesh Chitlangia |
| [HDDS-203](https://issues.apache.org/jira/browse/HDDS-203) | Add getCommittedBlockLength API in datanode |  Major | Ozone Client, Ozone Datanode | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-364](https://issues.apache.org/jira/browse/HDDS-364) | Update open container replica information in SCM during DN register |  Major | . | Ajay Kumar | Ajay Kumar |
| [HDDS-375](https://issues.apache.org/jira/browse/HDDS-375) | ContainerReportHandler should not send replication events for open containers |  Major | . | Ajay Kumar | Ajay Kumar |
| [HDDS-214](https://issues.apache.org/jira/browse/HDDS-214) | HDDS/Ozone First Release |  Major | . | Anu Engineer | Elek, Marton |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [HDDS-13](https://issues.apache.org/jira/browse/HDDS-13) | Refactor StorageContainerManager into seperate RPC endpoints |  Major | SCM | Anu Engineer | Anu Engineer |
| [HDDS-15](https://issues.apache.org/jira/browse/HDDS-15) | Add memory profiler support to Genesis |  Major | Tools | Anu Engineer | Anu Engineer |
| [HDDS-1](https://issues.apache.org/jira/browse/HDDS-1) | Remove SCM Block DB |  Major | SCM | Xiaoyu Yao | Xiaoyu Yao |
| [HDDS-12](https://issues.apache.org/jira/browse/HDDS-12) | Modify containerProtocol Calls for read, write and delete chunk to datanode to use a "long" blockId key |  Major | SCM Client | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-20](https://issues.apache.org/jira/browse/HDDS-20) | Ozone: Add support for rename key within a bucket for rpc client |  Major | . | Lokesh Jain | Lokesh Jain |
| [HDDS-18](https://issues.apache.org/jira/browse/HDDS-18) | Ozone Shell should use RestClient and RpcClient |  Major | . | Lokesh Jain | Lokesh Jain |
| [HDDS-42](https://issues.apache.org/jira/browse/HDDS-42) | Inconsistent module names and descriptions |  Major | . | Tsz Wo Nicholas Sze | Tsz Wo Nicholas Sze |
| [HDDS-16](https://issues.apache.org/jira/browse/HDDS-16) | Remove Pipeline from Datanode Container Protocol protobuf definition. |  Major | Native, Ozone Datanode | Mukul Kumar Singh | Mukul Kumar Singh |
| [HDDS-39](https://issues.apache.org/jira/browse/HDDS-39) | Ozone: Compile Ozone/HDFS/Cblock protobuf files with proto3 compiler using maven protoc plugin |  Major | Native | Mukul Kumar Singh | Mukul Kumar Singh |
| [HDDS-25](https://issues.apache.org/jira/browse/HDDS-25) | Simple async event processing for SCM |  Major | SCM | Elek, Marton | Elek, Marton |
| [HDDS-40](https://issues.apache.org/jira/browse/HDDS-40) | Separating packaging of Ozone/HDDS from the main Hadoop |  Major | . | Elek, Marton | Elek, Marton |
| [HDDS-21](https://issues.apache.org/jira/browse/HDDS-21) | Add support for rename key within a bucket for rest client |  Major | . | Lokesh Jain | Lokesh Jain |
| [HDDS-17](https://issues.apache.org/jira/browse/HDDS-17) | Add node to container map class to simplify state in SCM |  Major | SCM | Anu Engineer | Anu Engineer |
| [HDDS-143](https://issues.apache.org/jira/browse/HDDS-143) | Provide docker compose files to measure performance in a pseudo cluster |  Major | Tools | Elek, Marton | Elek, Marton |
| [HDDS-147](https://issues.apache.org/jira/browse/HDDS-147) | Update Ozone site docs |  Major | document | Arpit Agarwal | Arpit Agarwal |
| [HDDS-157](https://issues.apache.org/jira/browse/HDDS-157) | Upgrade common-langs version to 3.7 in HDDS and Ozone |  Major | . | Takanobu Asanuma | Takanobu Asanuma |
| [HDDS-146](https://issues.apache.org/jira/browse/HDDS-146) | Refactor the structure of the acceptance tests |  Major | . | Elek, Marton | Elek, Marton |
| [HDDS-172](https://issues.apache.org/jira/browse/HDDS-172) | The numbers of operation should be integer in KSM UI |  Minor | . | Takanobu Asanuma | Takanobu Asanuma |
| [HDDS-184](https://issues.apache.org/jira/browse/HDDS-184) | Upgrade common-langs version to 3.7 in hadoop-tools/hadoop-ozone |  Major | . | Takanobu Asanuma | Takanobu Asanuma |
| [HDDS-177](https://issues.apache.org/jira/browse/HDDS-177) | Create a releasable ozonefs artifact |  Major | Ozone Filesystem | Elek, Marton | Elek, Marton |
| [HDDS-189](https://issues.apache.org/jira/browse/HDDS-189) | Update HDDS to start OzoneManager |  Major | . | Arpit Agarwal | Arpit Agarwal |
| [HDDS-191](https://issues.apache.org/jira/browse/HDDS-191) | Queue SCMCommands via EventQueue in SCM |  Major | . | Elek, Marton | Elek, Marton |
| [HDDS-194](https://issues.apache.org/jira/browse/HDDS-194) | Remove NodePoolManager and node pool handling from SCM |  Major | SCM | Elek, Marton | Elek, Marton |
| [HDDS-193](https://issues.apache.org/jira/browse/HDDS-193) | Make Datanode heartbeat dispatcher in SCM event based |  Major | SCM | Elek, Marton | Elek, Marton |
| [HDDS-195](https://issues.apache.org/jira/browse/HDDS-195) | Create generic CommandWatcher utility |  Major | . | Elek, Marton | Elek, Marton |
| [HDDS-212](https://issues.apache.org/jira/browse/HDDS-212) | Introduce NodeStateManager to manage the state of Datanodes in SCM |  Major | SCM | Nanda kumar | Nanda kumar |
| [HDDS-217](https://issues.apache.org/jira/browse/HDDS-217) | Move all SCMEvents to a package |  Major | SCM | Anu Engineer | Anu Engineer |
| [HDDS-224](https://issues.apache.org/jira/browse/HDDS-224) | Create metrics for Event Watcher |  Major | SCM | Elek, Marton | Elek, Marton |
| [HDDS-240](https://issues.apache.org/jira/browse/HDDS-240) | Implement metrics for EventQueue |  Major | SCM | Elek, Marton | Elek, Marton |
| [HDDS-48](https://issues.apache.org/jira/browse/HDDS-48) | ContainerIO - Storage Management |  Major | . | Hanisha Koneru | Hanisha Koneru |
| [HDDS-242](https://issues.apache.org/jira/browse/HDDS-242) | Introduce NEW\_NODE, STALE\_NODE and DEAD\_NODE event and corresponding event handlers in SCM |  Major | SCM | Nanda kumar | Nanda kumar |
| [HDDS-228](https://issues.apache.org/jira/browse/HDDS-228) | Add the ReplicaMaps to ContainerStateManager |  Major | SCM | Anu Engineer | Ajay Kumar |
| [HDDS-238](https://issues.apache.org/jira/browse/HDDS-238) | Add Node2Pipeline Map in SCM to track ratis/standalone pipelines. |  Major | SCM | Mukul Kumar Singh | Mukul Kumar Singh |
| [HDDS-232](https://issues.apache.org/jira/browse/HDDS-232) | Parallel unit test execution for HDDS/Ozone |  Major | . | Arpit Agarwal | Arpit Agarwal |
| [HDDS-269](https://issues.apache.org/jira/browse/HDDS-269) | Refactor IdentifiableEventPayload to use a long ID |  Major | . | Xiaoyu Yao | Ajay Kumar |
| [HDDS-259](https://issues.apache.org/jira/browse/HDDS-259) | Implement ContainerReportPublisher and NodeReportPublisher |  Major | Ozone Datanode | Nanda kumar | Nanda kumar |
| [HDDS-250](https://issues.apache.org/jira/browse/HDDS-250) | Cleanup ContainerData |  Major | . | Hanisha Koneru | Hanisha Koneru |
| [HDDS-249](https://issues.apache.org/jira/browse/HDDS-249) | Fail if multiple SCM IDs on the DataNode and add SCM ID check after version request |  Major | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-199](https://issues.apache.org/jira/browse/HDDS-199) | Implement ReplicationManager to handle underreplication of closed containers |  Major | SCM | Elek, Marton | Elek, Marton |
| [HDDS-258](https://issues.apache.org/jira/browse/HDDS-258) | Helper methods to generate NodeReport and ContainerReport for testing |  Major | SCM | Nanda kumar | Nanda kumar |
| [HDDS-285](https://issues.apache.org/jira/browse/HDDS-285) | Create a generic Metadata Iterator |  Major | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-201](https://issues.apache.org/jira/browse/HDDS-201) | Add name for LeaseManager |  Minor | SCM | Elek, Marton | Sandeep Nemuri |
| [HDDS-270](https://issues.apache.org/jira/browse/HDDS-270) | Move generic container util functions to ContianerUtils |  Major | . | Hanisha Koneru | Hanisha Koneru |
| [HDDS-248](https://issues.apache.org/jira/browse/HDDS-248) | Refactor DatanodeContainerProtocol.proto |  Major | . | Hanisha Koneru | Hanisha Koneru |
| [HDDS-287](https://issues.apache.org/jira/browse/HDDS-287) | Add Close ContainerAction to Datanode#StateContext when the container gets full |  Major | Ozone Datanode | Nanda kumar | Nanda kumar |
| [HDDS-293](https://issues.apache.org/jira/browse/HDDS-293) | Reduce memory usage and object creation in KeyData |  Major | . | Tsz Wo Nicholas Sze | Tsz Wo Nicholas Sze |
| [HDDS-305](https://issues.apache.org/jira/browse/HDDS-305) | Datanode StateContext#addContainerActionIfAbsent will add container action even if there already is a ContainerAction |  Major | Ozone Datanode | Nanda kumar | Nanda kumar |
| [HDDS-271](https://issues.apache.org/jira/browse/HDDS-271) | Create a block iterator to iterate blocks in a container |  Major | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-304](https://issues.apache.org/jira/browse/HDDS-304) | Process ContainerAction from datanode heartbeat in SCM |  Major | SCM | Nanda kumar | Nanda kumar |
| [HDDS-312](https://issues.apache.org/jira/browse/HDDS-312) | Add blockIterator to Container |  Minor | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-300](https://issues.apache.org/jira/browse/HDDS-300) | Create a config for volume choosing policy |  Major | Ozone Datanode | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-124](https://issues.apache.org/jira/browse/HDDS-124) | Validate all required configs needed for ozone-site.xml and reflect the changes in ozone-default.xml |  Major | . | Dinesh Chitlangia | Dinesh Chitlangia |
| [HDDS-245](https://issues.apache.org/jira/browse/HDDS-245) | Handle ContainerReports in the SCM |  Major | SCM | Elek, Marton | Elek, Marton |
| [HDDS-345](https://issues.apache.org/jira/browse/HDDS-345) | Upgrade RocksDB version from 5.8.0 to 5.14.2 |  Major | Ozone Datanode, Ozone Manager, SCM | Nanda kumar | Nanda kumar |
| [HDDS-298](https://issues.apache.org/jira/browse/HDDS-298) | Implement SCMClientProtocolServer.getContainerWithPipeline for closed containers |  Critical | SCM | Elek, Marton | Ajay Kumar |
| [HDDS-342](https://issues.apache.org/jira/browse/HDDS-342) | Add example byteman script to print out hadoop rpc traffic |  Minor | . | Elek, Marton | Elek, Marton |
| [HDDS-328](https://issues.apache.org/jira/browse/HDDS-328) | Support export and import of the KeyValueContainer |  Critical | Ozone Datanode | Elek, Marton | Elek, Marton |
| [HDDS-317](https://issues.apache.org/jira/browse/HDDS-317) | Use new StorageSize API for reading ozone.scm.container.size.gb |  Major | SCM | Nanda kumar | Junjie Chen |
| [HDDS-363](https://issues.apache.org/jira/browse/HDDS-363) | Faster datanode registration during the first startup |  Minor | Ozone Datanode | Elek, Marton | Elek, Marton |
| [HDDS-374](https://issues.apache.org/jira/browse/HDDS-374) | Support to configure container size in units lesser than GB |  Major | Ozone Datanode | Nanda kumar | Nanda kumar |
| [HDDS-377](https://issues.apache.org/jira/browse/HDDS-377) | Make the ScmClient closable and stop the started threads |  Major | SCM Client | Elek, Marton | Elek, Marton |
| [HDDS-376](https://issues.apache.org/jira/browse/HDDS-376) | Create custom message structure for use in AuditLogging |  Major | . | Dinesh Chitlangia | Dinesh Chitlangia |
| [HDDS-379](https://issues.apache.org/jira/browse/HDDS-379) | Simplify and improve the cli arg parsing of ozone scmcli |  Major | . | Elek, Marton | Elek, Marton |
| [HDDS-392](https://issues.apache.org/jira/browse/HDDS-392) | Incomplete description about auditMap#key in AuditLogging Framework |  Trivial | . | Dinesh Chitlangia | Dinesh Chitlangia |
| [HDDS-336](https://issues.apache.org/jira/browse/HDDS-336) | Print out container location information for a specific ozone key |  Major | SCM | Elek, Marton | LiXin Ge |
| [HDDS-75](https://issues.apache.org/jira/browse/HDDS-75) | Support for CopyContainer |  Blocker | Ozone Datanode | Anu Engineer | Elek, Marton |
| [HDDS-369](https://issues.apache.org/jira/browse/HDDS-369) | Remove the containers of a dead node from the container state map |  Major | SCM | Elek, Marton | Elek, Marton |
| [HDDS-333](https://issues.apache.org/jira/browse/HDDS-333) | Create an Ozone Logo |  Major | Ozone Manager | Anu Engineer | Priyanka Nagwekar |
| [HDDS-303](https://issues.apache.org/jira/browse/HDDS-303) | Removing logic to identify containers to be closed from SCM |  Major | SCM | Nanda kumar | Nanda kumar |
| [HDDS-404](https://issues.apache.org/jira/browse/HDDS-404) | Implement toString() in OmKeyLocationInfo |  Major | Ozone Manager | Dinesh Chitlangia | Dinesh Chitlangia |
| [HDDS-190](https://issues.apache.org/jira/browse/HDDS-190) | Improve shell error message for unrecognized option |  Blocker | . | Arpit Agarwal | Elek, Marton |
| [HDDS-417](https://issues.apache.org/jira/browse/HDDS-417) | Ambiguous error message when using genconf tool |  Major | Tools | Dinesh Chitlangia | Dinesh Chitlangia |
| [HDDS-425](https://issues.apache.org/jira/browse/HDDS-425) | Move unit test of the genconf tool to hadoop-ozone/tools module |  Minor | Tools | Dinesh Chitlangia | Dinesh Chitlangia |
| [HDDS-428](https://issues.apache.org/jira/browse/HDDS-428) | OzoneManager lock optimization |  Major | OM | Nanda kumar | Nanda kumar |
| [HDDS-436](https://issues.apache.org/jira/browse/HDDS-436) | Allow SCM chill mode to be disabled by configuration. |  Major | . | Ajay Kumar | Ajay Kumar |
| [HDDS-438](https://issues.apache.org/jira/browse/HDDS-438) | 'ozone oz' should print usage when command or sub-command is missing |  Major | Tools | Arpit Agarwal | Dinesh Chitlangia |
| [HDDS-429](https://issues.apache.org/jira/browse/HDDS-429) | StorageContainerManager lock optimization |  Blocker | SCM | Nanda kumar | Nanda kumar |
| [HDDS-454](https://issues.apache.org/jira/browse/HDDS-454) | TestChunkStreams#testErrorReadGroupInputStream & TestChunkStreams#testReadGroupInputStream are failing |  Major | Ozone Client | Nanda kumar | chencan |
| [HDDS-446](https://issues.apache.org/jira/browse/HDDS-446) | Provide shaded artifact to start ozone service as a datanode plugin |  Major | . | Elek, Marton | Elek, Marton |
| [HDDS-449](https://issues.apache.org/jira/browse/HDDS-449) | Add a NULL check to protect DeadNodeHandler#onMessage |  Minor | . | LiXin Ge | LiXin Ge |
| [HDDS-462](https://issues.apache.org/jira/browse/HDDS-462) | Optimize ContainerStateMap#getMatchingContainerIDs in SCM |  Major | SCM | Nanda kumar | Nanda kumar |
| [HDDS-469](https://issues.apache.org/jira/browse/HDDS-469) | Rename 'ozone oz' to 'ozone sh' |  Blocker | . | Arpit Agarwal | Arpit Agarwal |
| [HDDS-435](https://issues.apache.org/jira/browse/HDDS-435) | Enhance the existing ozone documentation |  Blocker | document | Elek, Marton | Elek, Marton |
| [HDDS-352](https://issues.apache.org/jira/browse/HDDS-352) | Separate install and testing phases in acceptance tests. |  Major | . | Elek, Marton | Elek, Marton |
| [HDDS-468](https://issues.apache.org/jira/browse/HDDS-468) | Add version number to datanode plugin and ozone file system jar |  Major | . | Bharat Viswanadham | Bharat Viswanadham |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [HDDS-11](https://issues.apache.org/jira/browse/HDDS-11) | Fix findbugs exclude rules for ozone and hdds projects |  Major | . | Elek, Marton | Elek, Marton |
| [HDDS-28](https://issues.apache.org/jira/browse/HDDS-28) | Ozone:Duplicate declaration in hadoop-tools/hadoop-ozone/pom.xml |  Major | Ozone Filesystem | Tsz Wo Nicholas Sze | Tsz Wo Nicholas Sze |
| [HDDS-37](https://issues.apache.org/jira/browse/HDDS-37) | Remove dependency of hadoop-hdds-common and hadoop-hdds-server-scm from hadoop-ozone/tools/pom.xml |  Major | . | Nanda kumar | Sandeep Nemuri |
| [HDDS-43](https://issues.apache.org/jira/browse/HDDS-43) | Rename hdsl to hdds in hadoop-ozone/acceptance-test/README.md |  Trivial | . | Sandeep Nemuri | Sandeep Nemuri |
| [HDDS-19](https://issues.apache.org/jira/browse/HDDS-19) | Update ozone to latest ratis snapshot build (0.1.1-alpha-d7d7061-SNAPSHOT) |  Blocker | Ozone Datanode | Lokesh Jain | Lokesh Jain |
| [HDDS-77](https://issues.apache.org/jira/browse/HDDS-77) | Key replication factor and type should be stored per key by Ozone Manager |  Major | Ozone Manager | Mukul Kumar Singh | Mukul Kumar Singh |
| [HDDS-38](https://issues.apache.org/jira/browse/HDDS-38) | Add SCMNodeStorage map in SCM class to store storage statistics per Datanode |  Major | SCM | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-76](https://issues.apache.org/jira/browse/HDDS-76) | Modify SCMStorageReportProto to include the data dir paths as well as the StorageType info |  Major | SCM | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-89](https://issues.apache.org/jira/browse/HDDS-89) | Create ozone specific inline documentation as part of the build |  Major | Ozone Manager, SCM | Elek, Marton | Elek, Marton |
| [HDDS-74](https://issues.apache.org/jira/browse/HDDS-74) | Rename name of properties related to configuration tags |  Major | . | Nanda kumar | Sandeep Nemuri |
| [HDDS-49](https://issues.apache.org/jira/browse/HDDS-49) | Standalone protocol should use grpc in place of netty. |  Major | Ozone Datanode | Mukul Kumar Singh | Mukul Kumar Singh |
| [HDDS-85](https://issues.apache.org/jira/browse/HDDS-85) | Send Container State Info while sending the container report from Datanode to SCM |  Major | SCM | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-84](https://issues.apache.org/jira/browse/HDDS-84) | The root directory of ozone.tar.gz should contain the version string |  Major | . | Elek, Marton | Elek, Marton |
| [HDDS-44](https://issues.apache.org/jira/browse/HDDS-44) | Ozone: start-ozone.sh fail to start datanode because of incomplete classpaths |  Major | Tools | Mukul Kumar Singh | Mukul Kumar Singh |
| [HDDS-110](https://issues.apache.org/jira/browse/HDDS-110) | Checkstyle is not working in the HDDS precommit hook |  Major | . | Elek, Marton | Elek, Marton |
| [HDDS-45](https://issues.apache.org/jira/browse/HDDS-45) | Removal of old OzoneRestClient |  Major | Ozone Client | Nanda kumar | Lokesh Jain |
| [HDDS-78](https://issues.apache.org/jira/browse/HDDS-78) | Add per volume level storage stats in SCM. |  Major | . | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-114](https://issues.apache.org/jira/browse/HDDS-114) | Ozone Datanode mbean registration fails for StorageLocation |  Blocker | Ozone Datanode | Mukul Kumar Singh | Elek, Marton |
| [HDDS-88](https://issues.apache.org/jira/browse/HDDS-88) | Create separate message structure to represent ports in DatanodeDetails |  Major | Ozone Datanode, SCM | Nanda kumar | Nanda kumar |
| [HDDS-142](https://issues.apache.org/jira/browse/HDDS-142) | TestMetadataStore fails on Windows |  Major | . | Íñigo Goiri | Inigo |
| [HDDS-126](https://issues.apache.org/jira/browse/HDDS-126) | Fix findbugs warning in MetadataKeyFilters.java |  Major | . | Akira Ajisaka | Akira Ajisaka |
| [HDDS-127](https://issues.apache.org/jira/browse/HDDS-127) | Add CloseContainerEventHandler in SCM |  Major | SCM | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-72](https://issues.apache.org/jira/browse/HDDS-72) | Add deleteTransactionId field in ContainerInfo |  Major | Ozone Datanode, SCM | Lokesh Jain | Lokesh Jain |
| [HDDS-158](https://issues.apache.org/jira/browse/HDDS-158) | DatanodeStateMachine endPoint task throws NullPointerException |  Major | Ozone Datanode | Mukul Kumar Singh | Nanda kumar |
| [HDDS-130](https://issues.apache.org/jira/browse/HDDS-130) | TestGenerateOzoneRequiredConfigurations should use GenericTestUtils#getTempPath to set output directory |  Minor | Tools | Nanda kumar | Sandeep Nemuri |
| [HDDS-109](https://issues.apache.org/jira/browse/HDDS-109) | Add reconnect logic for XceiverClientGrpc |  Major | Ozone Client | Lokesh Jain | Lokesh Jain |
| [HDDS-161](https://issues.apache.org/jira/browse/HDDS-161) | Add functionality to queue ContainerClose command from SCM Heartbeat Response to Ratis |  Major | Ozone Datanode, SCM | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-141](https://issues.apache.org/jira/browse/HDDS-141) | Remove PipeLine Class from SCM and move the data field in the Pipeline to ContainerInfo |  Major | SCM | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-180](https://issues.apache.org/jira/browse/HDDS-180) | CloseContainer should commit all pending open keys for a container |  Major | Ozone Datanode | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-186](https://issues.apache.org/jira/browse/HDDS-186) | Create under replicated queue |  Major | SCM | Ajay Kumar | Ajay Kumar |
| [HDDS-94](https://issues.apache.org/jira/browse/HDDS-94) | Change ozone datanode command to start the standalone datanode plugin |  Major | Ozone Datanode | Elek, Marton | Sandeep Nemuri |
| [HDDS-206](https://issues.apache.org/jira/browse/HDDS-206) | Ozone shell command doesn't respect KSM port set in ozone-site.xml |  Major | Ozone Manager | Nilotpal Nandi | Shashikant Banerjee |
| [HDDS-175](https://issues.apache.org/jira/browse/HDDS-175) | Refactor ContainerInfo to remove Pipeline object from it |  Major | SCM | Ajay Kumar | Ajay Kumar |
| [HDDS-208](https://issues.apache.org/jira/browse/HDDS-208) | ozone createVolume command ignores the first character of the volume name argument |  Major | . | Nilotpal Nandi | Lokesh Jain |
| [HDDS-234](https://issues.apache.org/jira/browse/HDDS-234) | Add SCM node report handler |  Major | SCM | Ajay Kumar | Ajay Kumar |
| [HDDS-187](https://issues.apache.org/jira/browse/HDDS-187) | Command status publisher for datanode |  Major | SCM | Ajay Kumar | Ajay Kumar |
| [HDDS-253](https://issues.apache.org/jira/browse/HDDS-253) | SCMBlockDeletingService should publish events for delete blocks to EventQueue |  Major | SCM | Lokesh Jain | Lokesh Jain |
| [HDDS-210](https://issues.apache.org/jira/browse/HDDS-210) | Make "-file" argument optional for ozone getKey command |  Major | Ozone Client | Nilotpal Nandi | Lokesh Jain |
| [HDDS-251](https://issues.apache.org/jira/browse/HDDS-251) | Integrate BlockDeletingService in KeyValueHandler |  Major | Ozone Datanode | Lokesh Jain | Lokesh Jain |
| [HDDS-254](https://issues.apache.org/jira/browse/HDDS-254) | Fix TestStorageContainerManager#testBlockDeletingThrottling |  Major | . | Lokesh Jain | Lokesh Jain |
| [HDDS-241](https://issues.apache.org/jira/browse/HDDS-241) | Handle Volume in inconsistent state |  Major | . | Hanisha Koneru | Hanisha Koneru |
| [HDDS-207](https://issues.apache.org/jira/browse/HDDS-207) | ozone listVolume command accepts random values as argument |  Major | Ozone Client | Nilotpal Nandi | Lokesh Jain |
| [HDDS-264](https://issues.apache.org/jira/browse/HDDS-264) | 'oz' subcommand reference is not present in 'ozone' command help |  Minor | Ozone Client | Nilotpal Nandi | Sandeep Nemuri |
| [HDDS-256](https://issues.apache.org/jira/browse/HDDS-256) |  Adding CommandStatusReport Handler |  Major | SCM | Ajay Kumar | Ajay Kumar |
| [HDDS-257](https://issues.apache.org/jira/browse/HDDS-257) | Hook up VolumeSet#shutdown from HddsDispatcher#shutdown |  Major | . | Hanisha Koneru | Hanisha Koneru |
| [HDDS-239](https://issues.apache.org/jira/browse/HDDS-239) | Add PipelineStateManager to track pipeline state transition |  Major | SCM | Mukul Kumar Singh | Mukul Kumar Singh |
| [HDDS-275](https://issues.apache.org/jira/browse/HDDS-275) | Add message output for succeeded -deleteVolume CLI |  Minor | Ozone Client | Nilotpal Nandi | Nilotpal Nandi |
| [HDDS-181](https://issues.apache.org/jira/browse/HDDS-181) | CloseContainer should commit all pending open Keys on a datanode |  Major | Ozone Datanode | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-262](https://issues.apache.org/jira/browse/HDDS-262) | Send SCM healthy and failed volumes in the heartbeat |  Major | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-272](https://issues.apache.org/jira/browse/HDDS-272) | TestBlockDeletingService is failing with DiskOutOfSpaceException |  Major | Ozone Datanode | Mukul Kumar Singh | Lokesh Jain |
| [HDDS-282](https://issues.apache.org/jira/browse/HDDS-282) | Consolidate logging in scm/container-service |  Major | . | Elek, Marton | Elek, Marton |
| [HDDS-266](https://issues.apache.org/jira/browse/HDDS-266) | Integrate checksum into .container file |  Major | . | Hanisha Koneru | Hanisha Koneru |
| [HDDS-288](https://issues.apache.org/jira/browse/HDDS-288) | Fix bugs in OpenContainerBlockMap |  Major | . | Tsz Wo Nicholas Sze | Tsz Wo Nicholas Sze |
| [HDDS-292](https://issues.apache.org/jira/browse/HDDS-292) | Fix ContainerMapping#getMatchingContainerWithPipeline |  Major | . | Xiaoyu Yao | Xiaoyu Yao |
| [HDDS-277](https://issues.apache.org/jira/browse/HDDS-277) | PipelineStateMachine should handle closure of pipelines in SCM |  Blocker | SCM | Mukul Kumar Singh | Mukul Kumar Singh |
| [HDDS-291](https://issues.apache.org/jira/browse/HDDS-291) | Initialize hadoop metrics system in standalone hdds datanodes |  Minor | Ozone Datanode | Elek, Marton | Elek, Marton |
| [HDDS-246](https://issues.apache.org/jira/browse/HDDS-246) | Datanode should throw BlockNotCommittedException for uncommitted blocks to Ozone Client |  Blocker | Ozone Datanode | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-273](https://issues.apache.org/jira/browse/HDDS-273) | DeleteLog entries should be purged only after corresponding DNs commit the transaction |  Critical | SCM | Lokesh Jain | Lokesh Jain |
| [HDDS-302](https://issues.apache.org/jira/browse/HDDS-302) | Fix javadoc and add implementation details in ContainerStateMachine |  Major | Ozone Datanode | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-279](https://issues.apache.org/jira/browse/HDDS-279) | DeleteBlocks command should not be sent for open containers |  Major | SCM | Lokesh Jain | Lokesh Jain |
| [HDDS-226](https://issues.apache.org/jira/browse/HDDS-226) | Client should update block length in OM while committing the key |  Major | Ozone Manager | Mukul Kumar Singh | Shashikant Banerjee |
| [HDDS-310](https://issues.apache.org/jira/browse/HDDS-310) | VolumeSet shutdown hook fails on datanode restart |  Major | Ozone Datanode | Mukul Kumar Singh | Bharat Viswanadham |
| [HDDS-290](https://issues.apache.org/jira/browse/HDDS-290) | putKey is failing with KEY\_ALLOCATION\_ERROR |  Major | . | Namit Maheshwari | Xiaoyu Yao |
| [HDDS-307](https://issues.apache.org/jira/browse/HDDS-307) | docs link on ozone website is broken |  Blocker | document | Arpit Agarwal | Xiaoyu Yao |
| [HDDS-230](https://issues.apache.org/jira/browse/HDDS-230) | ContainerStateMachine should implement readStateMachineData api to read data from Containers if required during replication |  Critical | Ozone Datanode | Mukul Kumar Singh | Mukul Kumar Singh |
| [HDDS-301](https://issues.apache.org/jira/browse/HDDS-301) | ozone command shell does not contain subcommand to run ozoneFS commands |  Major | Ozone Filesystem | Nilotpal Nandi | Nilotpal Nandi |
| [HDDS-197](https://issues.apache.org/jira/browse/HDDS-197) | DataNode should return ContainerClosingException/ContainerClosedException (CCE) to client if the container is in Closing/Closed State |  Blocker | Ozone Client, Ozone Datanode | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-267](https://issues.apache.org/jira/browse/HDDS-267) | Handle consistency issues during container update/close |  Major | . | Hanisha Koneru | Hanisha Koneru |
| [HDDS-344](https://issues.apache.org/jira/browse/HDDS-344) | Remove multibyte characters from OzoneAcl |  Trivial | . | Takanobu Asanuma | Takanobu Asanuma |
| [HDDS-339](https://issues.apache.org/jira/browse/HDDS-339) | Add block length and blockId in PutKeyResponse |  Major | Ozone Client, Ozone Datanode | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-327](https://issues.apache.org/jira/browse/HDDS-327) | CloseContainer command handler in HDDS Dispatcher should not throw exception if the container is already closed |  Major | . | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-308](https://issues.apache.org/jira/browse/HDDS-308) | SCM should identify a container with pending deletes using container reports |  Major | SCM | Lokesh Jain | Lokesh Jain |
| [HDDS-346](https://issues.apache.org/jira/browse/HDDS-346) | ozoneShell show the new volume info after updateVolume command like updateBucket command. |  Minor | . | chencan | chencan |
| [HDDS-324](https://issues.apache.org/jira/browse/HDDS-324) | Use pipeline name as Ratis groupID to allow datanode to report pipeline info |  Major | Ozone Datanode | Mukul Kumar Singh | Mukul Kumar Singh |
| [HDDS-347](https://issues.apache.org/jira/browse/HDDS-347) | TestCloseContainerByPipeline#testCloseContainerViaStandaAlone sometimes fails |  Major | . | LiXin Ge | LiXin Ge |
| [HDDS-179](https://issues.apache.org/jira/browse/HDDS-179) | CloseContainer/PutKey command should be syncronized with write operations. |  Major | Ozone Client, Ozone Datanode | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-119](https://issues.apache.org/jira/browse/HDDS-119) | Skip Apache license header check for some ozone doc scripts |  Major | document | Xiaoyu Yao | Ajay Kumar |
| [HDDS-244](https://issues.apache.org/jira/browse/HDDS-244) | Synchronize PutKey and WriteChunk requests in Ratis Server |  Blocker | Ozone Datanode | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-355](https://issues.apache.org/jira/browse/HDDS-355) | Disable OpenKeyDeleteService and DeleteKeysService. |  Major | OM | Xiaoyu Yao | Anu Engineer |
| [HDDS-265](https://issues.apache.org/jira/browse/HDDS-265) | Move numPendingDeletionBlocks and deleteTransactionId from ContainerData to KeyValueContainerData |  Major | . | Hanisha Koneru | LiXin Ge |
| [HDDS-350](https://issues.apache.org/jira/browse/HDDS-350) | ContainerMapping#flushContainerInfo doesn't set containerId |  Major | . | Ajay Kumar | Ajay Kumar |
| [HDDS-356](https://issues.apache.org/jira/browse/HDDS-356) | Support ColumnFamily based RockDBStore and TableStore |  Major | . | Xiaoyu Yao | Anu Engineer |
| [HDDS-334](https://issues.apache.org/jira/browse/HDDS-334) | Update GettingStarted page to mention details about Ozone GenConf tool |  Minor | document | Dinesh Chitlangia | Dinesh Chitlangia |
| [HDDS-313](https://issues.apache.org/jira/browse/HDDS-313) | Add metrics to containerState Machine |  Major | Ozone Datanode | Mukul Kumar Singh | chencan |
| [HDDS-227](https://issues.apache.org/jira/browse/HDDS-227) | Use Grpc as the default transport protocol for Standalone pipeline |  Major | . | Mukul Kumar Singh | chencan |
| [HDDS-247](https://issues.apache.org/jira/browse/HDDS-247) | Handle CLOSED\_CONTAINER\_IO exception in ozoneClient |  Blocker | Ozone Client | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-381](https://issues.apache.org/jira/browse/HDDS-381) | Fix TestKeys#testPutAndGetKeyWithDnRestart |  Major | Ozone Datanode | Mukul Kumar Singh | Mukul Kumar Singh |
| [HDDS-332](https://issues.apache.org/jira/browse/HDDS-332) | Remove the ability to configure ozone.handler.type |  Major | . | Dinesh Chitlangia | Anu Engineer |
| [HDDS-359](https://issues.apache.org/jira/browse/HDDS-359) | RocksDB Profiles support |  Major | . | Xiaoyu Yao | Anu Engineer |
| [HDDS-382](https://issues.apache.org/jira/browse/HDDS-382) | Remove RatisTestHelper#RatisTestSuite constructor argument and fix checkstyle in ContainerTestHelper, GenericTestUtils |  Major | . | Nanda kumar | Nanda kumar |
| [HDDS-365](https://issues.apache.org/jira/browse/HDDS-365) | Implement flushStateMachineData for containerStateMachine |  Major | Ozone Datanode | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-380](https://issues.apache.org/jira/browse/HDDS-380) | Remove synchronization from ChunkGroupOutputStream and ChunkOutputStream |  Major | Ozone Client | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-280](https://issues.apache.org/jira/browse/HDDS-280) | Support ozone dist-start-stitching on openbsd/osx |  Major | . | Elek, Marton | Elek, Marton |
| [HDDS-388](https://issues.apache.org/jira/browse/HDDS-388) | Fix the name of the db profile configuration key |  Trivial | . | Elek, Marton | Elek, Marton |
| [HDDS-357](https://issues.apache.org/jira/browse/HDDS-357) | Use DBStore and TableStore for OzoneManager non-background service |  Major | . | Xiaoyu Yao | Nanda kumar |
| [HDDS-263](https://issues.apache.org/jira/browse/HDDS-263) | Add retries in Ozone Client to handle BlockNotCommitted Exception |  Blocker | Ozone Client | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-343](https://issues.apache.org/jira/browse/HDDS-343) | Containers are stuck in closing state in scm |  Blocker | SCM | Elek, Marton | Elek, Marton |
| [HDDS-396](https://issues.apache.org/jira/browse/HDDS-396) | Remove openContainers.db from SCM |  Major | SCM | Mukul Kumar Singh | Dinesh Chitlangia |
| [HDDS-268](https://issues.apache.org/jira/browse/HDDS-268) | Add SCM close container watcher |  Blocker | . | Xiaoyu Yao | Ajay Kumar |
| [HDDS-315](https://issues.apache.org/jira/browse/HDDS-315) | ozoneShell infoKey does not work for directories created as key and throws 'KEY\_NOT\_FOUND' error |  Major | . | Nilotpal Nandi | Dinesh Chitlangia |
| [HDDS-358](https://issues.apache.org/jira/browse/HDDS-358) | Use DBStore and TableStore for DeleteKeyService |  Major | . | Xiaoyu Yao | Anu Engineer |
| [HDDS-405](https://issues.apache.org/jira/browse/HDDS-405) | User/volume mapping is not cleaned up during the deletion of the last volume |  Critical | Ozone Manager | Elek, Marton | Elek, Marton |
| [HDDS-297](https://issues.apache.org/jira/browse/HDDS-297) | Add pipeline actions in Ozone |  Major | SCM | Mukul Kumar Singh | Mukul Kumar Singh |
| [HDDS-406](https://issues.apache.org/jira/browse/HDDS-406) | Enable acceptace test of the putKey for rpc protocol |  Major | test | Elek, Marton | Elek, Marton |
| [HDDS-397](https://issues.apache.org/jira/browse/HDDS-397) | Handle deletion for keys with no blocks |  Major | Ozone Manager | Lokesh Jain | Lokesh Jain |
| [HDDS-321](https://issues.apache.org/jira/browse/HDDS-321) | ozoneFS put/copyFromLocal command does not work for a directory when the directory contains file(s) as well as subdirectories |  Blocker | . | Nilotpal Nandi | Nilotpal Nandi |
| [HDDS-408](https://issues.apache.org/jira/browse/HDDS-408) | Read (getKey) operation is very slow |  Blocker | Ozone Datanode, Ozone Manager | Nilotpal Nandi | Nanda kumar |
| [HDDS-351](https://issues.apache.org/jira/browse/HDDS-351) | Add chill mode state to SCM |  Major | . | Ajay Kumar | Ajay Kumar |
| [HDDS-400](https://issues.apache.org/jira/browse/HDDS-400) | Check global replication state for containers of dead node |  Blocker | SCM | Elek, Marton | Elek, Marton |
| [HDDS-410](https://issues.apache.org/jira/browse/HDDS-410) | ozone scmcli list is not working properly |  Minor | SCM | Nilotpal Nandi | Hanisha Koneru |
| [HDDS-403](https://issues.apache.org/jira/browse/HDDS-403) | Fix createdOn and modifiedOn timestamp for volume, bucket, key |  Blocker | Ozone Manager | Nilotpal Nandi | Dinesh Chitlangia |
| [HDDS-421](https://issues.apache.org/jira/browse/HDDS-421) | Resilient DNS resolution in datanode-service |  Major | Ozone Datanode | Elek, Marton | Elek, Marton |
| [HDDS-431](https://issues.apache.org/jira/browse/HDDS-431) | LeaseManager of CommandWatcher is not started |  Major | SCM | Elek, Marton | Elek, Marton |
| [HDDS-416](https://issues.apache.org/jira/browse/HDDS-416) | Remove currentPosition from ChunkInputStreamEntry |  Major | Ozone Client | Lokesh Jain | Lokesh Jain |
| [HDDS-424](https://issues.apache.org/jira/browse/HDDS-424) | Consolidate ozone oz parameters to use GNU convention |  Major | Ozone CLI | Elek, Marton | Elek, Marton |
| [HDDS-432](https://issues.apache.org/jira/browse/HDDS-432) | Replication of closed containers is not working |  Critical | SCM | Elek, Marton | Elek, Marton |
| [HDDS-433](https://issues.apache.org/jira/browse/HDDS-433) | ContainerStateMachine#readStateMachineData should properly build LogEntryProto |  Blocker | Ozone Datanode | Lokesh Jain | Lokesh Jain |
| [HDDS-430](https://issues.apache.org/jira/browse/HDDS-430) | Close piplineActions are duplicated in the HB response |  Major | SCM | Elek, Marton | Shashikant Banerjee |
| [HDDS-395](https://issues.apache.org/jira/browse/HDDS-395) | TestOzoneRestWithMiniCluster fails with "Unable to read ROCKDB config" |  Major | Ozone Manager | Mukul Kumar Singh | Dinesh Chitlangia |
| [HDDS-420](https://issues.apache.org/jira/browse/HDDS-420) | putKey failing with KEY\_ALLOCATION\_ERROR |  Blocker | Ozone Manager | Nilotpal Nandi | Shashikant Banerjee |
| [HDDS-415](https://issues.apache.org/jira/browse/HDDS-415) | 'ozone om' with incorrect argument first logs all the STARTUP\_MSG |  Blocker | . | Namit Maheshwari | Namit Maheshwari |
| [HDDS-233](https://issues.apache.org/jira/browse/HDDS-233) | Update ozone to latest ratis snapshot build(0.3.0-50588bd-SNAPSHOT) |  Major | Ozone Datanode | Mukul Kumar Singh | Shashikant Banerjee |
| [HDDS-456](https://issues.apache.org/jira/browse/HDDS-456) | TestOzoneShell#init is breaking due to Null Pointer Exception |  Major | . | Dinesh Chitlangia | Dinesh Chitlangia |
| [HDDS-414](https://issues.apache.org/jira/browse/HDDS-414) | Fix sbin/stop-ozone.sh to stop Ozone daemons |  Major | . | Namit Maheshwari | Elek, Marton |
| [HDDS-423](https://issues.apache.org/jira/browse/HDDS-423) | Introduce an ozone specific log4j.properties |  Minor | . | Elek, Marton | Elek, Marton |
| [HDDS-389](https://issues.apache.org/jira/browse/HDDS-389) | Remove XceiverServer and XceiverClient and related classes |  Major | SCM | Mukul Kumar Singh | chencan |
| [HDDS-452](https://issues.apache.org/jira/browse/HDDS-452) | 'ozone scm' with incorrect argument first logs all the STARTUP\_MSG |  Blocker | . | Elek, Marton | Namit Maheshwari |
| [HDDS-419](https://issues.apache.org/jira/browse/HDDS-419) | ChunkInputStream bulk read api does not read from all the chunks |  Blocker | Ozone Client | Mukul Kumar Singh | Lokesh Jain |
| [HDDS-466](https://issues.apache.org/jira/browse/HDDS-466) | Handle null in argv of StorageContainerManager#createSCM |  Major | test | Nanda kumar | Nanda kumar |
| [HDDS-409](https://issues.apache.org/jira/browse/HDDS-409) | Ozone acceptance-test and integration-test packages have undefined hadoop component |  Major | . | Hanisha Koneru | Dinesh Chitlangia |
| [HDDS-465](https://issues.apache.org/jira/browse/HDDS-465) | Suppress group mapping lookup warnings for ozone |  Minor | . | Xiaoyu Yao | Xiaoyu Yao |
| [HDDS-470](https://issues.apache.org/jira/browse/HDDS-470) | Ozone acceptance tests are failing |  Blocker | test | Arpit Agarwal | Elek, Marton |
| [HDDS-399](https://issues.apache.org/jira/browse/HDDS-399) | Persist open pipeline information across SCM restart. |  Blocker | SCM | Mukul Kumar Singh | Mukul Kumar Singh |
| [HDDS-463](https://issues.apache.org/jira/browse/HDDS-463) | Fix the release packaging of the ozone distribution |  Blocker | . | Elek, Marton | Elek, Marton |
| [HDDS-487](https://issues.apache.org/jira/browse/HDDS-487) | Doc files are missing ASF license headers |  Blocker | documentation | Arpit Agarwal | Namit Maheshwari |
| [HDDS-491](https://issues.apache.org/jira/browse/HDDS-491) | Minor typos in README.md in smoketest |  Trivial | . | Bharat Viswanadham | chencan |
| [HDDS-481](https://issues.apache.org/jira/browse/HDDS-481) | Classes are missing from the shaded ozonefs jar |  Critical | Ozone Filesystem | Elek, Marton | Bharat Viswanadham |
| [HDDS-483](https://issues.apache.org/jira/browse/HDDS-483) | Update ozone Documentation to fix below issues |  Major | . | Xiaoyu Yao | Namit Maheshwari |
| [HDDS-495](https://issues.apache.org/jira/browse/HDDS-495) | Ozone docs and ozonefs packages have undefined hadoop component |  Major | . | Dinesh Chitlangia | Dinesh Chitlangia |
| [HDDS-440](https://issues.apache.org/jira/browse/HDDS-440) | Datanode loops forever if it cannot create directories |  Blocker | Ozone Datanode | Arpit Agarwal | Bharat Viswanadham |
| [HDDS-496](https://issues.apache.org/jira/browse/HDDS-496) | Ozone tools module is incorrectly classified as 'hdds' component |  Major | . | Dinesh Chitlangia | Dinesh Chitlangia |
| [HDDS-501](https://issues.apache.org/jira/browse/HDDS-501) | AllocateBlockResponse.keyLocation must be an optional field |  Blocker | . | Arpit Agarwal | Arpit Agarwal |
| [HDDS-500](https://issues.apache.org/jira/browse/HDDS-500) | TestErrorCode.java has wrong package name |  Major | Ozone Manager | Anu Engineer | Anu Engineer |
| [HDDS-503](https://issues.apache.org/jira/browse/HDDS-503) | Suppress ShellBasedUnixGroupsMapping exception in tests |  Blocker | test | Arpit Agarwal | Arpit Agarwal |
| [HDDS-497](https://issues.apache.org/jira/browse/HDDS-497) | Suppress license warnings for error log files |  Blocker | Tools | Arpit Agarwal | Arpit Agarwal |
| [HDDS-476](https://issues.apache.org/jira/browse/HDDS-476) | Add Pipeline reports to make pipeline active on SCM restart |  Blocker | SCM | Mukul Kumar Singh | Mukul Kumar Singh |
| [HDDS-502](https://issues.apache.org/jira/browse/HDDS-502) | Exception in OM startup when running unit tests |  Blocker | Ozone Manager | Arpit Agarwal | Arpit Agarwal |
| [HDDS-461](https://issues.apache.org/jira/browse/HDDS-461) | Container remains in CLOSING state in SCM forever |  Major | SCM | Nilotpal Nandi | Shashikant Banerjee |
| [HDDS-460](https://issues.apache.org/jira/browse/HDDS-460) | Replication manager failed to import container data |  Major | SCM | Nilotpal Nandi | Elek, Marton |
| [HDDS-507](https://issues.apache.org/jira/browse/HDDS-507) | EventQueue should be shutdown on SCM shutdown |  Major | . | Xiaoyu Yao | Xiaoyu Yao |
| [HDDS-509](https://issues.apache.org/jira/browse/HDDS-509) | TestStorageContainerManager is flaky |  Major | test | Nanda kumar | Xiaoyu Yao |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [HDDS-33](https://issues.apache.org/jira/browse/HDDS-33) | Ozone : Fix the test logic in TestKeySpaceManager#testDeleteKey |  Major | Ozone Manager | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-47](https://issues.apache.org/jira/browse/HDDS-47) | Add acceptance tests for Ozone Shell |  Major | . | Lokesh Jain | Lokesh Jain |
| [HDDS-73](https://issues.apache.org/jira/browse/HDDS-73) | Add acceptance tests for Ozone Shell |  Major | . | Lokesh Jain | Lokesh Jain |
| [HDDS-107](https://issues.apache.org/jira/browse/HDDS-107) | TestOzoneConfigurationFields is failing |  Major | SCM | Nanda kumar | LiXin Ge |
| [HDDS-235](https://issues.apache.org/jira/browse/HDDS-235) | Fix TestOzoneAuditLogger#verifyDefaultLogLevel |  Major | . | Xiaoyu Yao | Xiaoyu Yao |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [HDDS-27](https://issues.apache.org/jira/browse/HDDS-27) | Fix TestStorageContainerManager#testBlockDeletionTransactions |  Major | . | Xiaoyu Yao | Xiaoyu Yao |
| [HDDS-23](https://issues.apache.org/jira/browse/HDDS-23) | Remove SCMNodeAddressList from SCMRegisterRequestProto |  Major | Ozone Datanode, SCM | Nanda kumar | Nanda kumar |
| [HDDS-6](https://issues.apache.org/jira/browse/HDDS-6) | Enable SCM kerberos auth |  Major | SCM, Security | Ajay Kumar | Ajay Kumar |
| [HDDS-30](https://issues.apache.org/jira/browse/HDDS-30) | Fix TestContainerSQLCli |  Major | . | Xiaoyu Yao | Shashikant Banerjee |
| [HDDS-31](https://issues.apache.org/jira/browse/HDDS-31) | Fix TestSCMCli |  Major | . | Xiaoyu Yao | Lokesh Jain |
| [HDDS-34](https://issues.apache.org/jira/browse/HDDS-34) | Remove .meta file during creation of container |  Major | Ozone Datanode | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-32](https://issues.apache.org/jira/browse/HDDS-32) | Fix TestContainerDeletionChoosingPolicy#testTopNOrderedChoosingPolicy |  Major | . | Xiaoyu Yao | Mukul Kumar Singh |
| [HDDS-52](https://issues.apache.org/jira/browse/HDDS-52) | Fix TestSCMCli#testInfoContainer |  Major | . | Mukul Kumar Singh | Mukul Kumar Singh |
| [HDDS-53](https://issues.apache.org/jira/browse/HDDS-53) | Fix TestKey#testPutAndGetKeyWithDnRestart |  Major | . | Mukul Kumar Singh | Mukul Kumar Singh |
| [HDDS-51](https://issues.apache.org/jira/browse/HDDS-51) | Fix TestDeletedBlockLog#testDeletedBlockTransactions |  Major | . | Xiaoyu Yao | Mukul Kumar Singh |
| [HDDS-29](https://issues.apache.org/jira/browse/HDDS-29) | Fix TestStorageContainerManager#testRpcPermission |  Major | . | Xiaoyu Yao | Mukul Kumar Singh |
| [HDDS-57](https://issues.apache.org/jira/browse/HDDS-57) | TestContainerCloser#testRepeatedClose and TestContainerCloser#testCleanupThreadRuns fail consistently |  Minor | SCM | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-87](https://issues.apache.org/jira/browse/HDDS-87) | Fix test failures with uninitialized storageLocation field in storageReport |  Major | SCM | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-71](https://issues.apache.org/jira/browse/HDDS-71) | Send ContainerType to Datanode during container creation |  Major | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-82](https://issues.apache.org/jira/browse/HDDS-82) | Merge ContainerData and ContainerStatus classes |  Major | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-79](https://issues.apache.org/jira/browse/HDDS-79) | Remove ReportState from SCMHeartbeatRequestProto |  Major | Ozone Datanode, SCM | Nanda kumar | Nanda kumar |
| [HDDS-80](https://issues.apache.org/jira/browse/HDDS-80) | Remove SendContainerCommand from SCM |  Major | Ozone Datanode, SCM | Nanda kumar | Nanda kumar |
| [HDDS-92](https://issues.apache.org/jira/browse/HDDS-92) | Use DBType during parsing datanode .container files |  Major | Ozone Datanode | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-90](https://issues.apache.org/jira/browse/HDDS-90) | Create ContainerData, Container classes |  Major | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-125](https://issues.apache.org/jira/browse/HDDS-125) | Cleanup HDDS CheckStyle issues |  Major | . | Anu Engineer | Anu Engineer |
| [HDDS-81](https://issues.apache.org/jira/browse/HDDS-81) | Moving ContainerReport inside Datanode heartbeat |  Major | . | Nanda kumar | Nanda kumar |
| [HDDS-128](https://issues.apache.org/jira/browse/HDDS-128) | Support for DN to SCM signaling |  Major | Ozone Datanode, SCM | Nanda kumar | Nanda kumar |
| [HDDS-116](https://issues.apache.org/jira/browse/HDDS-116) | Implement VolumeSet to manage disk volumes |  Major | . | Hanisha Koneru | Hanisha Koneru |
| [HDDS-137](https://issues.apache.org/jira/browse/HDDS-137) | Cleanup Hdds-ozone CheckStyle Issues |  Major | . | Anu Engineer | Anu Engineer |
| [HDDS-144](https://issues.apache.org/jira/browse/HDDS-144) | Fix TestEndPoint#testHeartbeat and TestEndPoint#testRegister |  Major | Ozone Datanode, SCM | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-145](https://issues.apache.org/jira/browse/HDDS-145) | Freon times out because of because of wrong ratis port number in datanode details |  Blocker | . | Mukul Kumar Singh | Mukul Kumar Singh |
| [HDDS-148](https://issues.apache.org/jira/browse/HDDS-148) | Remove ContainerReportManager and ContainerReportManagerImpl |  Major | Ozone Datanode | Nanda kumar | Nanda kumar |
| [HDDS-129](https://issues.apache.org/jira/browse/HDDS-129) | Support for ReportManager in Datanode |  Major | Ozone Datanode | Nanda kumar | Nanda kumar |
| [HDDS-123](https://issues.apache.org/jira/browse/HDDS-123) | ContainerSet class to manage ContainerMap |  Major | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-140](https://issues.apache.org/jira/browse/HDDS-140) | Add DU usage to VolumeInfo |  Major | . | Hanisha Koneru | Hanisha Koneru |
| [HDDS-133](https://issues.apache.org/jira/browse/HDDS-133) | Change format of .container files to Yaml |  Major | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-136](https://issues.apache.org/jira/browse/HDDS-136) | Rename dbPath,containerFilePath |  Major | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-163](https://issues.apache.org/jira/browse/HDDS-163) | Add Datanode heartbeat dispatcher in SCM |  Major | SCM | Nanda kumar | Nanda kumar |
| [HDDS-156](https://issues.apache.org/jira/browse/HDDS-156) | Implement HDDSVolume to manage volume state |  Major | . | Hanisha Koneru | Hanisha Koneru |
| [HDDS-155](https://issues.apache.org/jira/browse/HDDS-155) | Implement KeyValueContainer and adopt new disk layout for the containers |  Major | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-160](https://issues.apache.org/jira/browse/HDDS-160) | Refactor KeyManager, ChunkManager |  Major | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-170](https://issues.apache.org/jira/browse/HDDS-170) | Fix TestBlockDeletingService#testBlockDeletionTimeout |  Major | SCM | Mukul Kumar Singh | Lokesh Jain |
| [HDDS-173](https://issues.apache.org/jira/browse/HDDS-173) | Refactor Dispatcher and implement Handler for new ContainerIO design |  Major | . | Hanisha Koneru | Hanisha Koneru |
| [HDDS-183](https://issues.apache.org/jira/browse/HDDS-183) | Integrate Volumeset, ContainerSet and HddsDispatcher |  Major | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-185](https://issues.apache.org/jira/browse/HDDS-185) | TestCloseContainerByPipeline#testCloseContainerViaRatis fail intermittently |  Major | SCM | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-169](https://issues.apache.org/jira/browse/HDDS-169) | Add Volume IO Stats |  Major | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-205](https://issues.apache.org/jira/browse/HDDS-205) | Add metrics to HddsDispatcher |  Major | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-176](https://issues.apache.org/jira/browse/HDDS-176) | Add keyCount and container maximum size to ContainerData |  Major | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-229](https://issues.apache.org/jira/browse/HDDS-229) | Remove singleton for Handler |  Blocker | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-182](https://issues.apache.org/jira/browse/HDDS-182) | CleanUp Reimplemented classes |  Major | . | Hanisha Koneru | Hanisha Koneru |
| [HDDS-204](https://issues.apache.org/jira/browse/HDDS-204) | Modify Integration tests for new ContainerIO classes |  Major | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-211](https://issues.apache.org/jira/browse/HDDS-211) | Add a create container Lock |  Major | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-237](https://issues.apache.org/jira/browse/HDDS-237) | Add updateDeleteTransactionId |  Major | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-213](https://issues.apache.org/jira/browse/HDDS-213) | Single lock to synchronize KeyValueContainer#update |  Major | . | Hanisha Koneru | Hanisha Koneru |
| [HDDS-255](https://issues.apache.org/jira/browse/HDDS-255) | Fix TestOzoneConfigurationFields for missing hdds.command.status.report.interval in config classes |  Minor | test | Nanda kumar | Sandeep Nemuri |
| [HDDS-260](https://issues.apache.org/jira/browse/HDDS-260) | Support in Datanode for sending ContainerActions to SCM |  Major | Ozone Datanode | Nanda kumar | Nanda kumar |
| [HDDS-219](https://issues.apache.org/jira/browse/HDDS-219) | Genearate version-info.properties for hadoop and ozone |  Major | . | Elek, Marton | Sandeep Nemuri |
| [HDDS-218](https://issues.apache.org/jira/browse/HDDS-218) | add existing docker-compose files to the ozone release artifact |  Minor | . | Elek, Marton | Elek, Marton |
| [HDDS-353](https://issues.apache.org/jira/browse/HDDS-353) | Multiple delete Blocks tests are failing consistently |  Major | Ozone Manager, SCM | Shashikant Banerjee | Lokesh Jain |
| [HDDS-98](https://issues.apache.org/jira/browse/HDDS-98) | Adding Ozone Manager Audit Log |  Major | . | Xiaoyu Yao | Dinesh Chitlangia |
| [HDDS-222](https://issues.apache.org/jira/browse/HDDS-222) | Remove hdfs command line from ozone distribution. |  Major | . | Elek, Marton | Elek, Marton |
| [HDDS-464](https://issues.apache.org/jira/browse/HDDS-464) | Fix TestCloseContainerHandlingByClient |  Major | test | Lokesh Jain | Lokesh Jain |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [HDDS-3](https://issues.apache.org/jira/browse/HDDS-3) | Send NodeReport and ContainerReport when datanodes register |  Major | Ozone Datanode, SCM | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-113](https://issues.apache.org/jira/browse/HDDS-113) | Rest and Rpc Client should verify resource name using HddsClientUtils |  Major | Ozone Client | Lokesh Jain | Lokesh Jain |
| [HDDS-112](https://issues.apache.org/jira/browse/HDDS-112) | OzoneShell should support commands with url without scheme |  Major | Ozone Client | Lokesh Jain | Lokesh Jain |
| [HDDS-111](https://issues.apache.org/jira/browse/HDDS-111) | Include tests for Rest Client in TestVolume and TestBucket |  Major | Ozone Client | Lokesh Jain | Lokesh Jain |
| [HDDS-159](https://issues.apache.org/jira/browse/HDDS-159) | RestClient: Implement list operations for volume, bucket and keys |  Major | Ozone Client | Lokesh Jain | Lokesh Jain |
| [HDDS-166](https://issues.apache.org/jira/browse/HDDS-166) | Create a landing page for Ozone |  Major | document | Elek, Marton | Elek, Marton |
| [HDDS-178](https://issues.apache.org/jira/browse/HDDS-178) | DN should update transactionId on block delete |  Major | Ozone Datanode | Lokesh Jain | Lokesh Jain |
| [HDDS-167](https://issues.apache.org/jira/browse/HDDS-167) | Rename KeySpaceManager to OzoneManager |  Major | Ozone Manager | Arpit Agarwal | Arpit Agarwal |
| [HDDS-362](https://issues.apache.org/jira/browse/HDDS-362) | Modify functions impacted by SCM chill mode in ScmBlockLocationProtocol |  Major | . | Ajay Kumar | Ajay Kumar |
| [HDDS-366](https://issues.apache.org/jira/browse/HDDS-366) | Update functions impacted by SCM chill mode in StorageContainerLocationProtocol |  Major | . | Ajay Kumar | Ajay Kumar |
| [HDDS-488](https://issues.apache.org/jira/browse/HDDS-488) | Handle chill mode exception from SCM in OzoneManager |  Major | . | Ajay Kumar | Ajay Kumar |
| [HDDS-471](https://issues.apache.org/jira/browse/HDDS-471) | Fix failing unit tests |  Major | test | Arpit Agarwal |  |


