
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Hadoop: Ozone Changelog

## Release 0.3.0 - Unreleased (as of 2018-10-20)



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [HDDS-370](https://issues.apache.org/jira/browse/HDDS-370) | Add and implement following functions in SCMClientProtocolServer |  Major | . | Ajay Kumar | Ajay Kumar |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [HDDS-398](https://issues.apache.org/jira/browse/HDDS-398) | Support multiple tests in freon |  Minor | Tools | Elek, Marton | Elek, Marton |
| [HDDS-417](https://issues.apache.org/jira/browse/HDDS-417) | Ambiguous error message when using genconf tool |  Major | Tools | Dinesh Chitlangia | Dinesh Chitlangia |
| [HDDS-425](https://issues.apache.org/jira/browse/HDDS-425) | Move unit test of the genconf tool to hadoop-ozone/tools module |  Minor | Tools | Dinesh Chitlangia | Dinesh Chitlangia |
| [HDDS-438](https://issues.apache.org/jira/browse/HDDS-438) | 'ozone oz' should print usage when command or sub-command is missing |  Major | Tools | Arpit Agarwal | Dinesh Chitlangia |
| [HDDS-429](https://issues.apache.org/jira/browse/HDDS-429) | StorageContainerManager lock optimization |  Blocker | SCM | Nanda kumar | Nanda kumar |
| [HDDS-454](https://issues.apache.org/jira/browse/HDDS-454) | TestChunkStreams#testErrorReadGroupInputStream & TestChunkStreams#testReadGroupInputStream are failing |  Major | Ozone Client | Nanda kumar | chencan |
| [HDDS-449](https://issues.apache.org/jira/browse/HDDS-449) | Add a NULL check to protect DeadNodeHandler#onMessage |  Minor | . | LiXin Ge | LiXin Ge |
| [HDDS-469](https://issues.apache.org/jira/browse/HDDS-469) | Rename 'ozone oz' to 'ozone sh' |  Blocker | . | Arpit Agarwal | Arpit Agarwal |
| [HDDS-435](https://issues.apache.org/jira/browse/HDDS-435) | Enhance the existing ozone documentation |  Blocker | document | Elek, Marton | Elek, Marton |
| [HDDS-352](https://issues.apache.org/jira/browse/HDDS-352) | Separate install and testing phases in acceptance tests. |  Major | . | Elek, Marton | Elek, Marton |
| [HDDS-468](https://issues.apache.org/jira/browse/HDDS-468) | Add version number to datanode plugin and ozone file system jar |  Major | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-174](https://issues.apache.org/jira/browse/HDDS-174) | Shell error messages are often cryptic |  Blocker | . | Arpit Agarwal | Nanda kumar |
| [HDDS-447](https://issues.apache.org/jira/browse/HDDS-447) | Separate ozone-dist and hadoop-dist projects with real classpath separation |  Major | . | Elek, Marton | Elek, Marton |
| [HDDS-554](https://issues.apache.org/jira/browse/HDDS-554) | In XceiverClientSpi, implement sendCommand(..) using sendCommandAsync(..) |  Major | Ozone Client | Tsz Wo Nicholas Sze | Tsz Wo Nicholas Sze |
| [HDDS-455](https://issues.apache.org/jira/browse/HDDS-455) | Ozone genconf tool must use picocli |  Minor | . | Dinesh Chitlangia | Dinesh Chitlangia |
| [HDDS-391](https://issues.apache.org/jira/browse/HDDS-391) | Simplify Audit Framework to make audit logging easier to use |  Major | Ozone Manager | Dinesh Chitlangia | Dinesh Chitlangia |
| [HDDS-526](https://issues.apache.org/jira/browse/HDDS-526) | Clean previous chill mode code from NodeManager. |  Major | . | Ajay Kumar | Ajay Kumar |
| [HDDS-561](https://issues.apache.org/jira/browse/HDDS-561) | Move Node2ContainerMap and Node2PipelineMap to NodeManager |  Major | SCM | Lokesh Jain | Lokesh Jain |
| [HDDS-567](https://issues.apache.org/jira/browse/HDDS-567) | Rename Mapping to ContainerManager in SCM |  Major | SCM | Nanda kumar | Nanda kumar |
| [HDDS-508](https://issues.apache.org/jira/browse/HDDS-508) | Add robot framework to the apache/hadoop-runner baseimage |  Major | test | Elek, Marton | Dinesh Chitlangia |
| [HDDS-443](https://issues.apache.org/jira/browse/HDDS-443) | Create reusable ProgressBar utility for freon tests |  Major | test | Elek, Marton | Zsolt Horvath |
| [HDDS-619](https://issues.apache.org/jira/browse/HDDS-619) | hdds.db.profile should not be tagged as a required setting & should default to DISK |  Major | . | Arpit Agarwal | Dinesh Chitlangia |
| [HDDS-645](https://issues.apache.org/jira/browse/HDDS-645) | Enable OzoneFS contract tests by default |  Blocker | test | Arpit Agarwal | Arpit Agarwal |
| [HDDS-616](https://issues.apache.org/jira/browse/HDDS-616) | Collect all the robot test outputs and return with the right exit code |  Major | test | Elek, Marton | Elek, Marton |
| [HDDS-654](https://issues.apache.org/jira/browse/HDDS-654) | Add ServiceLoader resource for OzoneFileSystem |  Major | Ozone Filesystem | Jitendra Nath Pandey | Jitendra Nath Pandey |
| [HDDS-439](https://issues.apache.org/jira/browse/HDDS-439) | 'ozone oz volume create' should default to current logged in user |  Blocker | Tools | Arpit Agarwal | Dinesh Chitlangia |
| [HDDS-378](https://issues.apache.org/jira/browse/HDDS-378) | Remove dependencies between hdds/ozone and hdfs proto files |  Major | . | Elek, Marton | Elek, Marton |
| [HDDS-527](https://issues.apache.org/jira/browse/HDDS-527) | Show SCM chill mode status in SCM UI. |  Major | . | Ajay Kumar | Yiqun Lin |
| [HDDS-651](https://issues.apache.org/jira/browse/HDDS-651) | Rename o3 to o3fs for Filesystem |  Blocker | . | Namit Maheshwari | Jitendra Nath Pandey |
| [HDDS-621](https://issues.apache.org/jira/browse/HDDS-621) | ozone genconf improvements |  Major | . | Arpit Agarwal | Dinesh Chitlangia |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [HDDS-383](https://issues.apache.org/jira/browse/HDDS-383) | Ozone Client should discard preallocated blocks from closed containers |  Major | Ozone Client | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-351](https://issues.apache.org/jira/browse/HDDS-351) | Add chill mode state to SCM |  Major | . | Ajay Kumar | Ajay Kumar |
| [HDDS-410](https://issues.apache.org/jira/browse/HDDS-410) | ozone scmcli list is not working properly |  Minor | SCM | Nilotpal Nandi | Hanisha Koneru |
| [HDDS-421](https://issues.apache.org/jira/browse/HDDS-421) | Resilient DNS resolution in datanode-service |  Major | Ozone Datanode | Elek, Marton | Elek, Marton |
| [HDDS-431](https://issues.apache.org/jira/browse/HDDS-431) | LeaseManager of CommandWatcher is not started |  Major | SCM | Elek, Marton | Elek, Marton |
| [HDDS-424](https://issues.apache.org/jira/browse/HDDS-424) | Consolidate ozone oz parameters to use GNU convention |  Major | Ozone CLI | Elek, Marton | Elek, Marton |
| [HDDS-415](https://issues.apache.org/jira/browse/HDDS-415) | 'ozone om' with incorrect argument first logs all the STARTUP\_MSG |  Blocker | . | Namit Maheshwari | Namit Maheshwari |
| [HDDS-456](https://issues.apache.org/jira/browse/HDDS-456) | TestOzoneShell#init is breaking due to Null Pointer Exception |  Major | . | Dinesh Chitlangia | Dinesh Chitlangia |
| [HDDS-389](https://issues.apache.org/jira/browse/HDDS-389) | Remove XceiverServer and XceiverClient and related classes |  Major | SCM | Mukul Kumar Singh | chencan |
| [HDDS-466](https://issues.apache.org/jira/browse/HDDS-466) | Handle null in argv of StorageContainerManager#createSCM |  Major | test | Nanda kumar | Nanda kumar |
| [HDDS-409](https://issues.apache.org/jira/browse/HDDS-409) | Ozone acceptance-test and integration-test packages have undefined hadoop component |  Major | . | Hanisha Koneru | Dinesh Chitlangia |
| [HDDS-470](https://issues.apache.org/jira/browse/HDDS-470) | Ozone acceptance tests are failing |  Blocker | test | Arpit Agarwal | Elek, Marton |
| [HDDS-463](https://issues.apache.org/jira/browse/HDDS-463) | Fix the release packaging of the ozone distribution |  Blocker | . | Elek, Marton | Elek, Marton |
| [HDDS-487](https://issues.apache.org/jira/browse/HDDS-487) | Doc files are missing ASF license headers |  Blocker | documentation | Arpit Agarwal | Namit Maheshwari |
| [HDDS-481](https://issues.apache.org/jira/browse/HDDS-481) | Classes are missing from the shaded ozonefs jar |  Critical | Ozone Filesystem | Elek, Marton | Bharat Viswanadham |
| [HDDS-483](https://issues.apache.org/jira/browse/HDDS-483) | Update ozone Documentation to fix below issues |  Major | . | Xiaoyu Yao | Namit Maheshwari |
| [HDDS-495](https://issues.apache.org/jira/browse/HDDS-495) | Ozone docs and ozonefs packages have undefined hadoop component |  Major | . | Dinesh Chitlangia | Dinesh Chitlangia |
| [HDDS-440](https://issues.apache.org/jira/browse/HDDS-440) | Datanode loops forever if it cannot create directories |  Blocker | Ozone Datanode | Arpit Agarwal | Bharat Viswanadham |
| [HDDS-496](https://issues.apache.org/jira/browse/HDDS-496) | Ozone tools module is incorrectly classified as 'hdds' component |  Major | . | Dinesh Chitlangia | Dinesh Chitlangia |
| [HDDS-501](https://issues.apache.org/jira/browse/HDDS-501) | AllocateBlockResponse.keyLocation must be an optional field |  Blocker | . | Arpit Agarwal | Arpit Agarwal |
| [HDDS-500](https://issues.apache.org/jira/browse/HDDS-500) | TestErrorCode.java has wrong package name |  Major | Ozone Manager | Anu Engineer | Anu Engineer |
| [HDDS-503](https://issues.apache.org/jira/browse/HDDS-503) | Suppress ShellBasedUnixGroupsMapping exception in tests |  Blocker | test | Arpit Agarwal | Arpit Agarwal |
| [HDDS-497](https://issues.apache.org/jira/browse/HDDS-497) | Suppress license warnings for error log files |  Blocker | Tools | Arpit Agarwal | Arpit Agarwal |
| [HDDS-476](https://issues.apache.org/jira/browse/HDDS-476) | Add Pipeline reports to make pipeline active on SCM restart |  Blocker | SCM | Mukul Kumar Singh | Mukul Kumar Singh |
| [HDDS-502](https://issues.apache.org/jira/browse/HDDS-502) | Exception in OM startup when running unit tests |  Blocker | Ozone Manager | Arpit Agarwal | Arpit Agarwal |
| [HDDS-461](https://issues.apache.org/jira/browse/HDDS-461) | Container remains in CLOSING state in SCM forever |  Major | SCM | Nilotpal Nandi | Shashikant Banerjee |
| [HDDS-95](https://issues.apache.org/jira/browse/HDDS-95) | Shade the hadoop-ozone/objectstore-service project |  Major | Ozone Datanode | Elek, Marton |  |
| [HDDS-460](https://issues.apache.org/jira/browse/HDDS-460) | Replication manager failed to import container data |  Major | SCM | Nilotpal Nandi | Elek, Marton |
| [HDDS-59](https://issues.apache.org/jira/browse/HDDS-59) | Ozone client should update blocksize in OM for sub-block writes |  Blocker | Ozone Client | Mukul Kumar Singh | Mukul Kumar Singh |
| [HDDS-507](https://issues.apache.org/jira/browse/HDDS-507) | EventQueue should be shutdown on SCM shutdown |  Major | . | Xiaoyu Yao | Xiaoyu Yao |
| [HDDS-509](https://issues.apache.org/jira/browse/HDDS-509) | TestStorageContainerManager is flaky |  Major | test | Nanda kumar | Xiaoyu Yao |
| [HDDS-513](https://issues.apache.org/jira/browse/HDDS-513) | Check if the EventQueue is not closed before executing handlers |  Major | SCM | Nanda kumar | Nanda kumar |
| [HDDS-394](https://issues.apache.org/jira/browse/HDDS-394) | Rename \*Key Apis in DatanodeContainerProtocol to \*Block apis |  Major | Ozone Datanode | Mukul Kumar Singh | Dinesh Chitlangia |
| [HDDS-514](https://issues.apache.org/jira/browse/HDDS-514) | Clean Unregister JMX upon SCMConnectionManager#close |  Minor | . | Xiaoyu Yao | Xiaoyu Yao |
| [HDDS-551](https://issues.apache.org/jira/browse/HDDS-551) | Fix the close container status check in CloseContainerCommandHandler |  Major | Ozone Datanode | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-289](https://issues.apache.org/jira/browse/HDDS-289) | Volume and Bucket name should not contain a delimiter ('/') |  Major | Ozone Client | Namit Maheshwari | chencan |
| [HDDS-325](https://issues.apache.org/jira/browse/HDDS-325) | Add event watcher for delete blocks command |  Major | Ozone Datanode, SCM | Lokesh Jain | Lokesh Jain |
| [HDDS-565](https://issues.apache.org/jira/browse/HDDS-565) | TestContainerPersistence fails regularly in Jenkins |  Minor | test | Hanisha Koneru | Dinesh Chitlangia |
| [HDDS-354](https://issues.apache.org/jira/browse/HDDS-354) | VolumeInfo.getScmUsed throws NPE |  Major | Ozone Datanode | Ajay Kumar | Hanisha Koneru |
| [HDDS-578](https://issues.apache.org/jira/browse/HDDS-578) | om-audit-log4j2.properties must be packaged in ozone-dist |  Major | Ozone Manager | Dinesh Chitlangia | Dinesh Chitlangia |
| [HDDS-575](https://issues.apache.org/jira/browse/HDDS-575) | LoadExistingContainers should be moved to SCMContainerManager from ContainerStateManager. |  Major | SCM | Mukul Kumar Singh | Mukul Kumar Singh |
| [HDDS-583](https://issues.apache.org/jira/browse/HDDS-583) | SCM returns zero as the return code, even when invalid options are passed |  Major | . | Namit Maheshwari | Namit Maheshwari |
| [HDDS-450](https://issues.apache.org/jira/browse/HDDS-450) | Generate BlockCommitSequenceId in ContainerStateMachine for every commit operation in Ratis |  Major | Ozone Datanode | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-590](https://issues.apache.org/jira/browse/HDDS-590) | Add unit test for HDDS-583 |  Major | . | Namit Maheshwari | Namit Maheshwari |
| [HDDS-559](https://issues.apache.org/jira/browse/HDDS-559) | fs.default.name is deprecated |  Major | documentation | Arpit Agarwal | Dinesh Chitlangia |
| [HDDS-604](https://issues.apache.org/jira/browse/HDDS-604) | Correct Ozone getOzoneConf description |  Minor | . | Hanisha Koneru | Dinesh Chitlangia |
| [HDDS-608](https://issues.apache.org/jira/browse/HDDS-608) | Access denied exception for user hdfs while making getContainerWithPipeline call. |  Major | SCM | Namit Maheshwari | Mukul Kumar Singh |
| [HDDS-617](https://issues.apache.org/jira/browse/HDDS-617) | XceiverClientManager should cache pipelines on pipelineID |  Major | SCM | Mukul Kumar Singh | Mukul Kumar Singh |
| [HDDS-630](https://issues.apache.org/jira/browse/HDDS-630) | Rename KSM to OM in Hdds.md |  Minor | document | Takanobu Asanuma | Takanobu Asanuma |
| [HDDS-601](https://issues.apache.org/jira/browse/HDDS-601) | On restart, SCM throws 'No such datanode' exception |  Major | SCM | Soumitra Sulav | Hanisha Koneru |
| [HDDS-627](https://issues.apache.org/jira/browse/HDDS-627) | OzoneFS read from an MR Job throws java.lang.ArrayIndexOutOfBoundsException |  Major | Ozone Filesystem | Soumitra Sulav | Mukul Kumar Singh |
| [HDDS-550](https://issues.apache.org/jira/browse/HDDS-550) | Serialize ApplyTransaction calls per Container in ContainerStateMachine |  Major | . | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-636](https://issues.apache.org/jira/browse/HDDS-636) | Turn off ozone.metastore.rocksdb.statistics for now |  Major | . | Arpit Agarwal | Arpit Agarwal |
| [HDDS-634](https://issues.apache.org/jira/browse/HDDS-634) | OzoneFS: support basic user group and permission for file/dir |  Major | . | Xiaoyu Yao | Xiaoyu Yao |
| [HDDS-625](https://issues.apache.org/jira/browse/HDDS-625) | putKey hangs for a long time after completion, sometimes forever |  Blocker | Ozone Client | Arpit Agarwal | Arpit Agarwal |
| [HDDS-609](https://issues.apache.org/jira/browse/HDDS-609) | On restart, SCM does not exit chill mode as it expects DNs to report containers in ALLOCATED state |  Major | . | Namit Maheshwari | Hanisha Koneru |
| [HDDS-628](https://issues.apache.org/jira/browse/HDDS-628) | Fix outdated names used in HDDS documentations |  Minor | document | Yiqun Lin | Yiqun Lin |
| [HDDS-555](https://issues.apache.org/jira/browse/HDDS-555) | RandomKeyGenerator runs not closing the XceiverClient properly |  Major | . | Shashikant Banerjee | Mukul Kumar Singh |
| [HDDS-641](https://issues.apache.org/jira/browse/HDDS-641) | Fix ozone filesystem robot test |  Major | Ozone Filesystem, test | Mukul Kumar Singh | Mukul Kumar Singh |
| [HDDS-639](https://issues.apache.org/jira/browse/HDDS-639) | ChunkGroupInputStream gets into infinite loop after reading a block |  Blocker | Ozone Client | Nanda kumar | Mukul Kumar Singh |
| [HDDS-624](https://issues.apache.org/jira/browse/HDDS-624) | PutBlock fails with Unexpected Storage Container Exception |  Major | . | Namit Maheshwari | Arpit Agarwal |
| [HDDS-644](https://issues.apache.org/jira/browse/HDDS-644) | Rename dfs.container.ratis.num.container.op.threads |  Minor | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-587](https://issues.apache.org/jira/browse/HDDS-587) | Add new classes for pipeline management |  Major | SCM | Lokesh Jain | Lokesh Jain |
| [HDDS-646](https://issues.apache.org/jira/browse/HDDS-646) | TestChunkStreams.testErrorReadGroupInputStream fails |  Major | test | Nanda kumar | Nanda kumar |
| [HDDS-653](https://issues.apache.org/jira/browse/HDDS-653) | TestMetadataStore#testIterator fails on Windows |  Major | test | Yiqun Lin | Yiqun Lin |
| [HDDS-603](https://issues.apache.org/jira/browse/HDDS-603) | Add BlockCommitSequenceId field per Container and expose it in Container Reports |  Major | . | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-579](https://issues.apache.org/jira/browse/HDDS-579) | ContainerStateMachine should fail subsequent transactions per container in case one fails |  Major | . | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-629](https://issues.apache.org/jira/browse/HDDS-629) | Make ApplyTransaction calls in ContainerStateMachine idempotent |  Major | . | Shashikant Banerjee | Shashikant Banerjee |
| [HDDS-490](https://issues.apache.org/jira/browse/HDDS-490) | Improve om and scm start up options |  Major | . | Namit Maheshwari | Namit Maheshwari |
| [HDDS-667](https://issues.apache.org/jira/browse/HDDS-667) | Fix TestOzoneFileInterfaces |  Major | Ozone Filesystem | Mukul Kumar Singh | Mukul Kumar Singh |
| [HDDS-661](https://issues.apache.org/jira/browse/HDDS-661) | When a volume fails in datanode, VersionEndpointTask#call ends up in dead lock. |  Major | Ozone Datanode | Nanda kumar | Hanisha Koneru |
| [HDDS-687](https://issues.apache.org/jira/browse/HDDS-687) | Exception while loading containers during SCM startup |  Blocker | SCM | Namit Maheshwari | Lokesh Jain |
| [HDDS-673](https://issues.apache.org/jira/browse/HDDS-673) | Suppress "Key not found" exception log with stack trace in OM |  Major | . | Xiaoyu Yao | Arpit Agarwal |
| [HDDS-690](https://issues.apache.org/jira/browse/HDDS-690) | Javadoc build fails in hadoop-ozone |  Major | . | Takanobu Asanuma | Takanobu Asanuma |
| [HDDS-672](https://issues.apache.org/jira/browse/HDDS-672) | Spark shell throws OzoneFileSystem not found |  Major | . | Namit Maheshwari | Anu Engineer |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [HDDS-479](https://issues.apache.org/jira/browse/HDDS-479) | Add more ozone fs tests in the robot integration framework |  Minor | . | Nilotpal Nandi | Nilotpal Nandi |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [HDDS-6](https://issues.apache.org/jira/browse/HDDS-6) | Enable SCM kerberos auth |  Major | SCM, Security | Ajay Kumar | Ajay Kumar |
| [HDDS-5](https://issues.apache.org/jira/browse/HDDS-5) | Enable OzoneManager kerberos auth |  Major | Ozone Manager, Security | Ajay Kumar | Ajay Kumar |
| [HDDS-7](https://issues.apache.org/jira/browse/HDDS-7) | Enable kerberos auth for Ozone client in hadoop rpc |  Major | Ozone Client, SCM Client | Ajay Kumar | Ajay Kumar |
| [HDDS-70](https://issues.apache.org/jira/browse/HDDS-70) | Fix config names for secure ksm and scm |  Major | . | Ajay Kumar | Ajay Kumar |
| [HDDS-100](https://issues.apache.org/jira/browse/HDDS-100) | SCM CA: generate public/private key pair for SCM/OM/DNs |  Major | . | Xiaoyu Yao | Ajay Kumar |
| [HDDS-222](https://issues.apache.org/jira/browse/HDDS-222) | Remove hdfs command line from ozone distribution. |  Major | . | Elek, Marton | Elek, Marton |
| [HDDS-441](https://issues.apache.org/jira/browse/HDDS-441) | Create new s3gateway daemon |  Major | . | Elek, Marton | Elek, Marton |
| [HDDS-444](https://issues.apache.org/jira/browse/HDDS-444) | Add rest service to the s3gateway |  Major | . | Elek, Marton | Elek, Marton |
| [HDDS-525](https://issues.apache.org/jira/browse/HDDS-525) | Support virtual-hosted style URLs |  Major | . | Elek, Marton | Bharat Viswanadham |
| [HDDS-562](https://issues.apache.org/jira/browse/HDDS-562) | Create acceptance test to test aws cli with the s3 gateway |  Major | . | Elek, Marton | Elek, Marton |
| [HDDS-520](https://issues.apache.org/jira/browse/HDDS-520) | Implement HeadBucket REST endpoint |  Major | . | Elek, Marton | Bharat Viswanadham |
| [HDDS-512](https://issues.apache.org/jira/browse/HDDS-512) | update test.sh to remove robot framework & python-pip installation |  Major | . | Dinesh Chitlangia | Dinesh Chitlangia |
| [HDDS-521](https://issues.apache.org/jira/browse/HDDS-521) | Implement DeleteBucket REST endpoint |  Major | . | Elek, Marton | Bharat Viswanadham |
| [HDDS-585](https://issues.apache.org/jira/browse/HDDS-585) | Handle common request identifiers in a transparent way |  Major | . | Elek, Marton | Elek, Marton |
| [HDDS-577](https://issues.apache.org/jira/browse/HDDS-577) | Support S3 buckets as first class objects in Ozone Manager - 2 |  Major | S3 | Anu Engineer | Bharat Viswanadham |
| [HDDS-517](https://issues.apache.org/jira/browse/HDDS-517) | Implement HeadObject REST endpoint |  Major | . | Elek, Marton | LiXin Ge |
| [HDDS-522](https://issues.apache.org/jira/browse/HDDS-522) | Implement PutBucket REST endpoint |  Major | . | Elek, Marton | Bharat Viswanadham |
| [HDDS-606](https://issues.apache.org/jira/browse/HDDS-606) | Create delete s3Bucket |  Major | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-445](https://issues.apache.org/jira/browse/HDDS-445) | Create a logger to print out all of the incoming requests |  Major | . | Elek, Marton | Bharat Viswanadham |
| [HDDS-613](https://issues.apache.org/jira/browse/HDDS-613) | Update  HeadBucket, DeleteBucket to not to have volume in path |  Major | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-518](https://issues.apache.org/jira/browse/HDDS-518) | Implement PutObject Rest endpoint |  Major | . | Elek, Marton | chencan |
| [HDDS-519](https://issues.apache.org/jira/browse/HDDS-519) | Implement ListBucket REST endpoint |  Major | . | Elek, Marton | LiXin Ge |
| [HDDS-657](https://issues.apache.org/jira/browse/HDDS-657) | Remove {volume} path segments from all the remaining rest endpoints |  Major | . | Elek, Marton | Elek, Marton |
| [HDDS-683](https://issues.apache.org/jira/browse/HDDS-683) | Add a shell command to provide ozone mapping for a S3Bucket |  Major | . | Bharat Viswanadham | Bharat Viswanadham |
| [HDDS-678](https://issues.apache.org/jira/browse/HDDS-678) | Format of Last-Modified header is invalid in HEAD Object call |  Major | . | Elek, Marton | Elek, Marton |
| [HDDS-680](https://issues.apache.org/jira/browse/HDDS-680) | Provide web based bucket browser. |  Major | S3 | Elek, Marton | Elek, Marton |
| [HDDS-701](https://issues.apache.org/jira/browse/HDDS-701) | Support key multi-delete |  Major | S3 | Elek, Marton | Elek, Marton |
| [HDDS-679](https://issues.apache.org/jira/browse/HDDS-679) | Add query parameter to the constructed query in VirtualHostStyleFilter |  Major | . | Elek, Marton | Bharat Viswanadham |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [HDDS-599](https://issues.apache.org/jira/browse/HDDS-599) | Fix TestOzoneConfiguration TestOzoneConfigurationFields |  Major | . | Bharat Viswanadham | Sandeep Nemuri |
| [HDDS-665](https://issues.apache.org/jira/browse/HDDS-665) | Add hdds.datanode.dir to docker-config |  Major | . | Bharat Viswanadham | Bharat Viswanadham |


