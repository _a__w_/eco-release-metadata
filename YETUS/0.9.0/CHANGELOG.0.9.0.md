
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Yetus Changelog

## Release 0.9.0 - Unreleased (as of 2018-10-20)



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [YETUS-690](https://issues.apache.org/jira/browse/YETUS-690) | releasedocmaker has a hard-coded reference to ASF JIRA |  Minor | Release Doc Maker | Allen Wittenauer | Allen Wittenauer |
| [YETUS-707](https://issues.apache.org/jira/browse/YETUS-707) | Fix typo in documentation |  Major | website and documentation | Fokko Driesprong | Fokko Driesprong |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [YETUS-695](https://issues.apache.org/jira/browse/YETUS-695) | Fix pylint warnings |  Minor | Release Doc Maker, ShellDocs | Akira Ajisaka | Akira Ajisaka |


