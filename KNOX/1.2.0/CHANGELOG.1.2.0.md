
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Knox Changelog

## Release 1.2.0 - Unreleased (as of 2018-10-20)



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [KNOX-1401](https://issues.apache.org/jira/browse/KNOX-1401) | Add HAL+JSON To the List of JSON Content Types |  Major | . | William Watson |  |
| [KNOX-1112](https://issues.apache.org/jira/browse/KNOX-1112) | KnoxShell support for managing simple descriptors |  Major | KnoxShell | Phil Zampino | Phil Zampino |
| [KNOX-1411](https://issues.apache.org/jira/browse/KNOX-1411) | WEBHBASE service discovery references incorrect property for the port |  Major | Server | Phil Zampino | Phil Zampino |
| [KNOX-1444](https://issues.apache.org/jira/browse/KNOX-1444) | Upgrade to ASF POM version 21 |  Major | Build | Kevin Risden | Kevin Risden |
| [KNOX-1441](https://issues.apache.org/jira/browse/KNOX-1441) | Enable spotbugs for static build checking |  Major | Build | Kevin Risden | Kevin Risden |
| [KNOX-1442](https://issues.apache.org/jira/browse/KNOX-1442) | Enable forbiddenapis for static build checking |  Major | Build | Kevin Risden | Kevin Risden |
| [KNOX-1460](https://issues.apache.org/jira/browse/KNOX-1460) | Zookeeper tests should call close() instead of stop() |  Minor | . | Kevin Risden | Kevin Risden |
| [KNOX-1461](https://issues.apache.org/jira/browse/KNOX-1461) | Remove JRE/JDK 1.7 check introduced by KNOX-769 |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1443](https://issues.apache.org/jira/browse/KNOX-1443) | Add checkstyle to enforce coding style |  Major | Build | Kevin Risden | Kevin Risden |
| [KNOX-1440](https://issues.apache.org/jira/browse/KNOX-1440) | WEBHDFS: retry should not be attempted for datanode write failures |  Major | Server | Phil Zampino | Phil Zampino |
| [KNOX-1464](https://issues.apache.org/jira/browse/KNOX-1464) | Remove explicit guava dependency from gateway-server InstrUtils |  Minor | . | Kevin Risden | Kevin Risden |
| [KNOX-1465](https://issues.apache.org/jira/browse/KNOX-1465) | Remove explicit guava dependency from gateway-provider-ha |  Minor | . | Kevin Risden | Kevin Risden |
| [KNOX-1466](https://issues.apache.org/jira/browse/KNOX-1466) | Remove explicit guava dependency from gateway-provider-rewrite-func-inbound-query-param |  Minor | . | Kevin Risden | Kevin Risden |
| [KNOX-1467](https://issues.apache.org/jira/browse/KNOX-1467) | Remove explicit guava dependency from gateway-provider-security-preauth |  Minor | . | Kevin Risden | Kevin Risden |
| [KNOX-1468](https://issues.apache.org/jira/browse/KNOX-1468) | Remove explicit guava dependency from gateway-service-nifi |  Minor | . | Kevin Risden | Kevin Risden |
| [KNOX-1359](https://issues.apache.org/jira/browse/KNOX-1359) | Disable kerberos debugging by default |  Major | Server | Kevin Minder | Kevin Risden |
| [KNOX-1475](https://issues.apache.org/jira/browse/KNOX-1475) | Bump shrinkwrap dependency versions |  Minor | Build | Kevin Risden | Kevin Risden |
| [KNOX-1477](https://issues.apache.org/jira/browse/KNOX-1477) | Bump commons-\* dependency versions |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1478](https://issues.apache.org/jira/browse/KNOX-1478) | Support running checkstyle from submodules |  Minor | Build | Kevin Risden | Kevin Risden |
| [KNOX-1479](https://issues.apache.org/jira/browse/KNOX-1479) | Replace Mockito usage with EasyMock |  Major | Tests | Kevin Risden | Kevin Risden |
| [KNOX-1474](https://issues.apache.org/jira/browse/KNOX-1474) | Bump apacheds dependency version to 2.0.0-AM24 |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1448](https://issues.apache.org/jira/browse/KNOX-1448) | Maven pom dependency cleanup |  Major | Build | Kevin Risden | Kevin Risden |
| [KNOX-1291](https://issues.apache.org/jira/browse/KNOX-1291) | ssl.include\|exclude.ciphers & ssl.exclude.protocols support comma or pipe separated lists |  Minor | . | Khalid Diriye | Kevin Risden |
| [KNOX-1501](https://issues.apache.org/jira/browse/KNOX-1501) | Upgrade hadoop to 3.1.1 |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1511](https://issues.apache.org/jira/browse/KNOX-1511) | Enable forbiddenapis commons-io signatures |  Major | Build | Kevin Risden | Kevin Risden |
| [KNOX-1512](https://issues.apache.org/jira/browse/KNOX-1512) | Replace deprecated EasyMock new Capture() calls |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1484](https://issues.apache.org/jira/browse/KNOX-1484) | Bump version dependencies Oct 2018 |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1518](https://issues.apache.org/jira/browse/KNOX-1518) | Large HDFS file downloads are incomplete when content is gzipped |  Major | Server | Phil Zampino | Phil Zampino |
| [KNOX-1521](https://issues.apache.org/jira/browse/KNOX-1521) | Implement UrlRewriteRequestStream methods for performance |  Major | Server | Kevin Risden | Kevin Risden |
| [KNOX-1522](https://issues.apache.org/jira/browse/KNOX-1522) | Add HA support for HadoopAuthProvider |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1528](https://issues.apache.org/jira/browse/KNOX-1528) | Remove hadoop-common dependency from gateway-spi module |  Minor | . | Kevin Risden | Kevin Risden |
| [KNOX-1011](https://issues.apache.org/jira/browse/KNOX-1011) | Add ldap.port system property to configure LDAP demo server port |  Critical | KnoxSSO | Ayub Pathan | Attila Csoma |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [KNOX-1415](https://issues.apache.org/jira/browse/KNOX-1415) | Upgrade Jetty to 9.2.25 |  Major | Server | Larry McCay | Larry McCay |
| [KNOX-1412](https://issues.apache.org/jira/browse/KNOX-1412) | Failure while executing a \`curl\` command using Content-Type: text/xml" "Content-Encoding: gzip" |  Major | Server | Ernani Pereira de Mattos Junior | Sandeep More |
| [KNOX-1420](https://issues.apache.org/jira/browse/KNOX-1420) | Add app.css path for Zeppelin's style sheet |  Major | Server | Prabhjyot Singh | Prabhjyot Singh |
| [KNOX-1422](https://issues.apache.org/jira/browse/KNOX-1422) | Whitelisting not working as expected for knoxauth redirects |  Major | Server | Sandeep More | Sandeep More |
| [KNOX-1424](https://issues.apache.org/jira/browse/KNOX-1424) | knox renaming "app" to "/gateway/ui\_ns1/zeppelin/app" |  Major | Server | Sameer Shaikh |  |
| [KNOX-1417](https://issues.apache.org/jira/browse/KNOX-1417) | Upgrade Bouncy Castle Dependency from Pac4J |  Major | Server | Larry McCay | Larry McCay |
| [KNOX-1416](https://issues.apache.org/jira/browse/KNOX-1416) | Upgrade Spring Framework to 4.3.17 |  Major | Server | Larry McCay | Larry McCay |
| [KNOX-1434](https://issues.apache.org/jira/browse/KNOX-1434) | Visiting Knox Admin UI forces subsequent requests to other services redirect to HTTPS |  Critical | AdminUI | Vipin Rathor |  |
| [KNOX-1445](https://issues.apache.org/jira/browse/KNOX-1445) | Ensure that gateway-adapter module classes have @Deprecated annotation |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-720](https://issues.apache.org/jira/browse/KNOX-720) | Knox DSL - Ensure that HTTP client is closed on shutdown |  Major | ClientDSL | Marco Battaglia | Kevin Risden |
| [KNOX-1459](https://issues.apache.org/jira/browse/KNOX-1459) | Remove testng assertions |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1093](https://issues.apache.org/jira/browse/KNOX-1093) | KNOX Not Handling safemode state of one of the NameNode In HA state |  Major | Server | Rajesh Chandramohan | Matthew Sharp |
| [KNOX-1436](https://issues.apache.org/jira/browse/KNOX-1436) | AbstractHdfsHaDispatch failoverRequest - Improve Failover Logging |  Minor | . | Matthew Sharp | Matthew Sharp |
| [KNOX-1505](https://issues.apache.org/jira/browse/KNOX-1505) | Knox should close CuratorFramework clients when finished |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1430](https://issues.apache.org/jira/browse/KNOX-1430) | Potential output stream handle leak when copyLarge files in streamResponse |  Major | Server | Guang Yang | Guang Yang |
| [KNOX-1429](https://issues.apache.org/jira/browse/KNOX-1429) | HadoopAuthFilter avoid logging sensitive values |  Major | . | Larry McCay | Kevin Risden |
| [KNOX-1523](https://issues.apache.org/jira/browse/KNOX-1523) | XML parsing cleanup |  Major | Server | Sandeep More | Sandeep More |
| [KNOX-1529](https://issues.apache.org/jira/browse/KNOX-1529) | gateway-shell-release doesn't include shaded jar |  Blocker | . | Kevin Risden | Kevin Risden |
| [KNOX-1520](https://issues.apache.org/jira/browse/KNOX-1520) | markbook refers to wrong parent-pom version |  Minor | Site | Lars Francke | Lars Francke |
| [KNOX-1519](https://issues.apache.org/jira/browse/KNOX-1519) | Fix 404s in the documentation |  Trivial | Site | Lars Francke | Lars Francke |
| [KNOX-1152](https://issues.apache.org/jira/browse/KNOX-1152) | Guard Against Missing Subject in Identity Assertion |  Minor | Server | Rick Kellogg | Rick Kellogg |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [KNOX-1451](https://issues.apache.org/jira/browse/KNOX-1451) | Setup base checkstyle plugin |  Major | Build | Kevin Risden | Kevin Risden |
| [KNOX-1452](https://issues.apache.org/jira/browse/KNOX-1452) | Enable checkstyle no tabs in source file rule |  Major | Build | Kevin Risden | Kevin Risden |
| [KNOX-1453](https://issues.apache.org/jira/browse/KNOX-1453) | Enable checkstyle no unused imports rule |  Major | Build | Kevin Risden | Kevin Risden |
| [KNOX-1454](https://issues.apache.org/jira/browse/KNOX-1454) | Enable checkstyle no wildcard imports rule |  Major | Build | Kevin Risden | Kevin Risden |
| [KNOX-1449](https://issues.apache.org/jira/browse/KNOX-1449) | Ensure all pom.xml files have same header |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1450](https://issues.apache.org/jira/browse/KNOX-1450) | Remove redundant configurations from pom.xml |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1455](https://issues.apache.org/jira/browse/KNOX-1455) | Enable checkstyle ASF header check rule |  Major | Build | Kevin Risden | Kevin Risden |
| [KNOX-1457](https://issues.apache.org/jira/browse/KNOX-1457) | Add mailing lists to top level pom |  Major | Build | Kevin Risden | Kevin Risden |
| [KNOX-1470](https://issues.apache.org/jira/browse/KNOX-1470) | Remove surefire-version and failsafe-version properties |  Major | Build | Kevin Risden | Kevin Risden |
| [KNOX-1471](https://issues.apache.org/jira/browse/KNOX-1471) | Remove unused genson dependency |  Minor | Build | Kevin Risden | Kevin Risden |
| [KNOX-1472](https://issues.apache.org/jira/browse/KNOX-1472) | Remove explicit surefire plugin block in child poms |  Major | Build | Kevin Risden | Kevin Risden |
| [KNOX-1473](https://issues.apache.org/jira/browse/KNOX-1473) | Remove plugin versions that are from parent ASF pom |  Major | Build | Kevin Risden | Kevin Risden |
| [KNOX-1463](https://issues.apache.org/jira/browse/KNOX-1463) | Use maven-dependency-plugin to ensure all dependencies are declared |  Major | Build | Kevin Risden | Kevin Risden |
| [KNOX-1480](https://issues.apache.org/jira/browse/KNOX-1480) | Remove cobertura-maven-plugin |  Major | Build | Kevin Risden | Kevin Risden |
| [KNOX-1481](https://issues.apache.org/jira/browse/KNOX-1481) | Remove javancss-maven-plugin |  Major | Build | Kevin Risden | Kevin Risden |
| [KNOX-1482](https://issues.apache.org/jira/browse/KNOX-1482) | Migrate to maven-site-plugin reportSets |  Minor | Build | Kevin Risden | Kevin Risden |
| [KNOX-1456](https://issues.apache.org/jira/browse/KNOX-1456) | Move dependency versions to top level pom properties |  Major | Build | Kevin Risden | Kevin Risden |
| [KNOX-1490](https://issues.apache.org/jira/browse/KNOX-1490) | Upgrade buildnumber-maven-plugin to 1.4 |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1486](https://issues.apache.org/jira/browse/KNOX-1486) | Upgrade commons-lang3 to 3.8.1 |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1499](https://issues.apache.org/jira/browse/KNOX-1499) | Upgrade cors-filter to 2.6 |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1485](https://issues.apache.org/jira/browse/KNOX-1485) | Upgrade bcprov-jdk15on to 1.60 |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1495](https://issues.apache.org/jira/browse/KNOX-1495) | Upgrade httpclient to 4.5.6 and httpcore to 4.4.10 |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1494](https://issues.apache.org/jira/browse/KNOX-1494) | Upgrade groovy to 2.5.2 |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1492](https://issues.apache.org/jira/browse/KNOX-1492) | Upgrade jna to 4.5.2 |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1491](https://issues.apache.org/jira/browse/KNOX-1491) | Upgrade jline to 2.14.6 |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1497](https://issues.apache.org/jira/browse/KNOX-1497) | Upgrade jansi to 1.17.1 |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1500](https://issues.apache.org/jira/browse/KNOX-1500) | Upgrade rest-assured to 3.1.1 |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1498](https://issues.apache.org/jira/browse/KNOX-1498) | Upgrade maven-enforcer-plugin to 3.0.0-M2 |  Major | Build | Kevin Risden | Kevin Risden |
| [KNOX-1496](https://issues.apache.org/jira/browse/KNOX-1496) | Upgrade maven-bundle-plugin to 4.0.0 |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1493](https://issues.apache.org/jira/browse/KNOX-1493) | Upgrade joda-time to 2.10 |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1487](https://issues.apache.org/jira/browse/KNOX-1487) | Upgrade easymock to 3.6 |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1489](https://issues.apache.org/jira/browse/KNOX-1489) | Upgrade jericho-html to 3.4 |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1503](https://issues.apache.org/jira/browse/KNOX-1503) | Upgrade slf4j to 1.7.25 |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1488](https://issues.apache.org/jira/browse/KNOX-1488) | Upgrade curator to 4.0.1 |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1504](https://issues.apache.org/jira/browse/KNOX-1504) | Upgrade eclipselink to 2.7.3 |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1508](https://issues.apache.org/jira/browse/KNOX-1508) | Upgrade taglibs-standard-spec and taglibs-standard-impl to 1.2.5 |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1506](https://issues.apache.org/jira/browse/KNOX-1506) | Upgrade protobuf-java to 3.6.1 |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1507](https://issues.apache.org/jira/browse/KNOX-1507) | Upgrade okhttp to 2.7.5 |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1510](https://issues.apache.org/jira/browse/KNOX-1510) | Upgrade nimbus-jose-jwt to 6.0.2 |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1509](https://issues.apache.org/jira/browse/KNOX-1509) | Upgrade pac4j dependencies |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1513](https://issues.apache.org/jira/browse/KNOX-1513) | Upgrade spring-core to 5.0.9.RELEASE |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1514](https://issues.apache.org/jira/browse/KNOX-1514) | Upgrade jackson to 2.9.7 |  Major | . | Kevin Risden | Kevin Risden |
| [KNOX-1515](https://issues.apache.org/jira/browse/KNOX-1515) | Upgrade shiro to 1.4.0 and ehcache to 2.6.11 |  Major | . | Kevin Risden | Kevin Risden |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [KNOX-1402](https://issues.apache.org/jira/browse/KNOX-1402) | Upgrade to Apache parent pom version 20 |  Trivial | . | Colm O hEigeartaigh | Colm O hEigeartaigh |
| [KNOX-543](https://issues.apache.org/jira/browse/KNOX-543) | Add documentation and configuration examples to the user guide for replaybuffersize |  Major | Site | Kristopher Kane | Matthew Sharp |
| [KNOX-542](https://issues.apache.org/jira/browse/KNOX-542) | Add replayBufferSize defaults to the sandbox topology |  Major | . | Kristopher Kane | Matthew Sharp |


