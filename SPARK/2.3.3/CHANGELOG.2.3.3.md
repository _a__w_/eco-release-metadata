
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Spark Changelog

## Release 2.3.3 - Unreleased (as of 2018-10-20)



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [SPARK-25754](https://issues.apache.org/jira/browse/SPARK-25754) | Change CDN for MathJax |  Trivial | Documentation | Gengliang Wang | Gengliang Wang |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [SPARK-24677](https://issues.apache.org/jira/browse/SPARK-24677) | TaskSetManager not updating successfulTaskDurations for old stage attempts |  Critical | Spark Core | dzcxzl | dzcxzl |
| [SPARK-24755](https://issues.apache.org/jira/browse/SPARK-24755) | Executor loss can cause task to not be resubmitted |  Major | Spark Core | Mridul Muralidharan | Hieu Tri Huynh |
| [SPARK-25081](https://issues.apache.org/jira/browse/SPARK-25081) | Nested spill in ShuffleExternalSorter may access a released memory page |  Blocker | Spark Core | Shixiong Zhu | Shixiong Zhu |
| [SPARK-25425](https://issues.apache.org/jira/browse/SPARK-25425) | Extra options must overwrite sessions options |  Major | SQL | Maxim Gekk | Maxim Gekk |
| [SPARK-25471](https://issues.apache.org/jira/browse/SPARK-25471) | Fix tests for Python 3.6 with Pandas 0.23+ |  Major | PySpark, Tests | Bryan Cutler | Bryan Cutler |
| [SPARK-25450](https://issues.apache.org/jira/browse/SPARK-25450) | PushProjectThroughUnion rule uses the same exprId for project expressions in each Union child, causing mistakes in constant propagation |  Major | SQL | Maryann Xue | Maryann Xue |
| [SPARK-25502](https://issues.apache.org/jira/browse/SPARK-25502) | [Spark Job History] Empty Page when page number exceeds the reatinedTask size |  Minor | Web UI | ABHISHEK KUMAR GUPTA | shahid |
| [SPARK-25503](https://issues.apache.org/jira/browse/SPARK-25503) | [Spark Job History] Total task message in stage page is ambiguous |  Major | Web UI | ABHISHEK KUMAR GUPTA | shahid |
| [SPARK-25509](https://issues.apache.org/jira/browse/SPARK-25509) | SHS V2 cannot enabled in Windows, because POSIX permissions is not support. |  Major | Spark Core | Rong Tang | Rong Tang |
| [SPARK-25536](https://issues.apache.org/jira/browse/SPARK-25536) | executorSource.METRIC read wrong record in Executor.scala Line444 |  Major | Spark Core | ZhuoerXu | shahid |
| [SPARK-25533](https://issues.apache.org/jira/browse/SPARK-25533) | Inconsistent message for Completed Jobs in the  JobUI, when there are failed jobs, compared to spark2.2 |  Major | Web UI | shahid | shahid |
| [SPARK-25570](https://issues.apache.org/jira/browse/SPARK-25570) | Replace 2.3.1 with 2.3.2 in HiveExternalCatalogVersionsSuite |  Minor | SQL, Tests | Dongjoon Hyun | Dongjoon Hyun |
| [SPARK-25568](https://issues.apache.org/jira/browse/SPARK-25568) | Continue to update the remaining accumulators when failing to update one accumulator |  Major | Spark Core | Shixiong Zhu | Shixiong Zhu |
| [SPARK-25674](https://issues.apache.org/jira/browse/SPARK-25674) | If the records are incremented by more than 1 at a time,the number of bytes might rarely ever get updated |  Minor | SQL | liuxian | liuxian |
| [SPARK-25714](https://issues.apache.org/jira/browse/SPARK-25714) | Null Handling in the Optimizer rule BooleanSimplification |  Blocker | SQL | Xiao Li | Xiao Li |
| [SPARK-25726](https://issues.apache.org/jira/browse/SPARK-25726) | Flaky test: SaveIntoDataSourceCommandSuite.\`simpleString is redacted\` |  Minor | SQL, Tests | Dongjoon Hyun | Dongjoon Hyun |
| [SPARK-21402](https://issues.apache.org/jira/browse/SPARK-21402) | Fix java array of structs deserialization |  Major | SQL | Tom | Vladimir Kuriatkov |
| [SPARK-25768](https://issues.apache.org/jira/browse/SPARK-25768) | Constant argument expecting Hive UDAFs doesn't work |  Major | SQL | Peter Toth | Peter Toth |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [SPARK-25583](https://issues.apache.org/jira/browse/SPARK-25583) | Add newly added History server related configurations in the documentation |  Minor | Spark Core | shahid | shahid |


