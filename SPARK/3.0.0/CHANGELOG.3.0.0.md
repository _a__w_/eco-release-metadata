
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Spark Changelog

## Release 3.0.0 - Unreleased (as of 2018-10-20)



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [SPARK-25589](https://issues.apache.org/jira/browse/SPARK-25589) | Add BloomFilterBenchmark |  Major | SQL, Tests | Dongjoon Hyun | Dongjoon Hyun |
| [SPARK-25635](https://issues.apache.org/jira/browse/SPARK-25635) | Support selective direct encoding in native ORC write |  Major | SQL | Dongjoon Hyun | Dongjoon Hyun |
| [SPARK-25202](https://issues.apache.org/jira/browse/SPARK-25202) | SQL Function Split Should Respect Limit Argument |  Minor | Spark Core | Parker Hegstrom | Parker Hegstrom |
| [SPARK-25560](https://issues.apache.org/jira/browse/SPARK-25560) | Allow Function Injection in SparkSessionExtensions |  Major | Spark Core, SQL | Russell Spitzer | Russell Spitzer |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [SPARK-20636](https://issues.apache.org/jira/browse/SPARK-20636) | Eliminate unnecessary shuffle with adjacent Window expressions |  Major | Optimizer | Michael Styles | Michael Styles |
| [SPARK-25415](https://issues.apache.org/jira/browse/SPARK-25415) | Make plan change log in RuleExecutor configurable by SQLConf |  Major | SQL | Maryann Xue | Maryann Xue |
| [SPARK-25338](https://issues.apache.org/jira/browse/SPARK-25338) | Several tests miss calling super.afterAll() in their afterAll() method |  Minor | Tests | Kazuaki Ishizaki | Kazuaki Ishizaki |
| [SPARK-25426](https://issues.apache.org/jira/browse/SPARK-25426) | Remove the duplicate fallback logic in UnsafeProjection |  Minor | SQL | Takeshi Yamamuro | Takeshi Yamamuro |
| [SPARK-25436](https://issues.apache.org/jira/browse/SPARK-25436) | Bump master branch version to 2.5.0-SNAPSHOT |  Major | Build | Xiao Li | Xiao Li |
| [SPARK-25423](https://issues.apache.org/jira/browse/SPARK-25423) | Output "dataFilters" in DataSourceScanExec.metadata |  Trivial | SQL | Maryann Xue | Yuming Wang |
| [SPARK-16323](https://issues.apache.org/jira/browse/SPARK-16323) | Avoid unnecessary cast when doing integral divide |  Minor | SQL | Sean Zhong | Marco Gaido |
| [SPARK-25444](https://issues.apache.org/jira/browse/SPARK-25444) | Refactor GenArrayData.genCodeToCreateArrayData() method |  Major | SQL | Kazuaki Ishizaki | Kazuaki Ishizaki |
| [SPARK-25457](https://issues.apache.org/jira/browse/SPARK-25457) | IntegralDivide (div) should not always return long |  Major | SQL | Marco Gaido | Marco Gaido |
| [SPARK-25381](https://issues.apache.org/jira/browse/SPARK-25381) | Stratified sampling by Column argument |  Minor | SQL | Maxim Gekk | Maxim Gekk |
| [SPARK-25366](https://issues.apache.org/jira/browse/SPARK-25366) | Document Zstd and brotli CompressionCodec requirements for Parquet files |  Minor | Documentation, SQL | liuxian | liuxian |
| [SPARK-25494](https://issues.apache.org/jira/browse/SPARK-25494) | Upgrade Spark's use of Janino to 3.0.10 |  Major | SQL | Kris Mok | Kris Mok |
| [SPARK-24355](https://issues.apache.org/jira/browse/SPARK-24355) | Improve Spark shuffle server responsiveness to non-ChunkFetch requests |  Major | Shuffle | Min Shen | Sanket Chintapalli |
| [SPARK-25465](https://issues.apache.org/jira/browse/SPARK-25465) | Refactor Parquet test suites in project Hive |  Major | SQL | Gengliang Wang | Gengliang Wang |
| [SPARK-21291](https://issues.apache.org/jira/browse/SPARK-21291) | R bucketBy partitionBy API |  Major | SparkR | Felix Cheung | Huaxin Gao |
| [SPARK-25514](https://issues.apache.org/jira/browse/SPARK-25514) | Generating pretty JSON by to\_json |  Minor | SQL | Maxim Gekk | Maxim Gekk |
| [SPARK-21436](https://issues.apache.org/jira/browse/SPARK-21436) | Take advantage of known partioner for distinct on RDDs |  Minor | Spark Core | holdenk | holdenk |
| [SPARK-25458](https://issues.apache.org/jira/browse/SPARK-25458) | Support FOR ALL COLUMNS in ANALYZE TABLE |  Major | SQL | Xiao Li | Dilip Biswal |
| [SPARK-25429](https://issues.apache.org/jira/browse/SPARK-25429) | SparkListenerBus inefficient due to 'LiveStageMetrics#accumulatorIds:Array[Long]' data structure |  Major | Spark Core | DENG FEI | Yuming Wang |
| [SPARK-25449](https://issues.apache.org/jira/browse/SPARK-25449) | Don't send zero accumulators in heartbeats |  Major | Spark Core | Mukul Murthy | Mukul Murthy |
| [SPARK-25447](https://issues.apache.org/jira/browse/SPARK-25447) | Support JSON options by schema\_of\_json |  Minor | SQL | Maxim Gekk | Maxim Gekk |
| [SPARK-25048](https://issues.apache.org/jira/browse/SPARK-25048) | Pivoting by multiple columns in Scala/Java |  Minor | SQL | Maxim Gekk | Maxim Gekk |
| [SPARK-25565](https://issues.apache.org/jira/browse/SPARK-25565) | Add scala style checker to check add Locale.ROOT to .toLowerCase and .toUpperCase for internal calls |  Minor | Build | Yuming Wang | Hyukjin Kwon |
| [SPARK-18364](https://issues.apache.org/jira/browse/SPARK-18364) | Expose metrics for YarnShuffleService |  Major | YARN | Steven Rand | Marek Simunek |
| [SPARK-25575](https://issues.apache.org/jira/browse/SPARK-25575) | SQL tab in the spark UI doesn't have option of  hiding tables, eventhough other UI tabs has. |  Minor | Web UI | shahid | shahid |
| [SPARK-25592](https://issues.apache.org/jira/browse/SPARK-25592) | Bump master branch version to 3.0.0-SNAPSHOT |  Major | Build | Xiao Li | Xiao Li |
| [SPARK-25581](https://issues.apache.org/jira/browse/SPARK-25581) | Rename method \`benchmark\` in BenchmarkBase as benchmarkSuite |  Major | SQL | Gengliang Wang | Gengliang Wang |
| [SPARK-25595](https://issues.apache.org/jira/browse/SPARK-25595) | Ignore corrupt Avro file if flag IGNORE\_CORRUPT\_FILES enabled |  Major | SQL | Gengliang Wang | Gengliang Wang |
| [SPARK-17159](https://issues.apache.org/jira/browse/SPARK-17159) | Improve FileInputDStream.findNewFiles list performance |  Minor | DStreams | Steve Loughran | Steve Loughran |
| [SPARK-25408](https://issues.apache.org/jira/browse/SPARK-25408) | Move to idiomatic Java 8 |  Minor | Spark Core | Fokko Driesprong | Fokko Driesprong |
| [SPARK-25653](https://issues.apache.org/jira/browse/SPARK-25653) | Add tag ExtendedHiveTest for HiveSparkSubmitSuite |  Major | Tests | Gengliang Wang | Gengliang Wang |
| [SPARK-25062](https://issues.apache.org/jira/browse/SPARK-25062) | Clean up BlockLocations in FileStatus objects |  Major | Spark Core | andrzej.stankevich@gmail.com | Peter Toth |
| [SPARK-25539](https://issues.apache.org/jira/browse/SPARK-25539) | Update lz4-java to get speed improvement |  Minor | Build | Yuming Wang | Yuming Wang |
| [SPARK-25641](https://issues.apache.org/jira/browse/SPARK-25641) | Change the spark.shuffle.server.chunkFetchHandlerThreadsPercent default to 100 |  Minor | Spark Core | Sanket Reddy | Sanket Reddy |
| [SPARK-25497](https://issues.apache.org/jira/browse/SPARK-25497) | limit operation within whole stage codegen should not consume all the inputs |  Major | SQL | Wenchen Fan | Wenchen Fan |
| [SPARK-24851](https://issues.apache.org/jira/browse/SPARK-24851) | Map a Stage ID to it's Associated Job ID in UI |  Trivial | Spark Core | Parth Gandhi | Parth Gandhi |
| [SPARK-25699](https://issues.apache.org/jira/browse/SPARK-25699) | Partially push down conjunctive predicated in Orc |  Major | SQL | Gengliang Wang | Gengliang Wang |
| [SPARK-25016](https://issues.apache.org/jira/browse/SPARK-25016) | remove Support for hadoop 2.6 |  Major | Build | Thomas Graves | Sean Owen |
| [SPARK-24109](https://issues.apache.org/jira/browse/SPARK-24109) | Remove class SnappyOutputStreamWrapper |  Minor | Block Manager, Input/Output | wangjinhai | Takeshi Yamamuro |
| [SPARK-25684](https://issues.apache.org/jira/browse/SPARK-25684) | Organize header related codes in CSV datasource |  Major | SQL | Hyukjin Kwon | Hyukjin Kwon |
| [SPARK-25566](https://issues.apache.org/jira/browse/SPARK-25566) | [Spark Job History] SQL UI Page does not support Pagination |  Major | Web UI | ABHISHEK KUMAR GUPTA | shahid |
| [SPARK-25712](https://issues.apache.org/jira/browse/SPARK-25712) | Improve usage message of start-master.sh and start-slave.sh |  Minor | Spark Core | Gengliang Wang | Gengliang Wang |
| [SPARK-20327](https://issues.apache.org/jira/browse/SPARK-20327) | Add CLI support for YARN custom resources, like GPUs |  Major | Spark Shell, Spark Submit | Daniel Templeton | Szilard Nemeth |
| [SPARK-25711](https://issues.apache.org/jira/browse/SPARK-25711) | Allow history server to show usage and remove deprecated options |  Minor | Spark Core | Gengliang Wang | Gengliang Wang |
| [SPARK-25716](https://issues.apache.org/jira/browse/SPARK-25716) | Project and Aggregate generate valid constraints with unnecessary operation |  Minor | SQL | SongYadong | SongYadong |
| [SPARK-25394](https://issues.apache.org/jira/browse/SPARK-25394) | Expose App status metrics as Source |  Major | Spark Core | Stavros Kontopoulos | Stavros Kontopoulos |
| [SPARK-25393](https://issues.apache.org/jira/browse/SPARK-25393) | Parsing CSV strings in a column |  Minor | SQL | Maxim Gekk | Maxim Gekk |
| [SPARK-25734](https://issues.apache.org/jira/browse/SPARK-25734) | Literal should have a value corresponding to dataType |  Minor | SQL | Takeshi Yamamuro | Takeshi Yamamuro |
| [SPARK-25760](https://issues.apache.org/jira/browse/SPARK-25760) | Set AddJarCommand return empty |  Minor | SQL | Yuming Wang | Yuming Wang |
| [SPARK-25683](https://issues.apache.org/jira/browse/SPARK-25683) | Updated the log for the firstTime event Drop occurs. |  Trivial | Spark Core | Devaraj K | Shivu Sondur |
| [SPARK-25269](https://issues.apache.org/jira/browse/SPARK-25269) | SQL interface support specify StorageLevel when cache table |  Major | SQL | Yuming Wang | Yuming Wang |
| [SPARK-25785](https://issues.apache.org/jira/browse/SPARK-25785) | Add prettyNames for from\_json, to\_json, from\_csv, and schema\_of\_json |  Trivial | SQL | Hyukjin Kwon | Hyukjin Kwon |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [SPARK-25418](https://issues.apache.org/jira/browse/SPARK-25418) | The metadata of DataSource table should not include Hive-generated storage properties. |  Major | SQL | Takuya Ueshin | Takuya Ueshin |
| [SPARK-25472](https://issues.apache.org/jira/browse/SPARK-25472) | Structured Streaming query.stop() doesn't always stop gracefully |  Major | Structured Streaming | Burak Yavuz | Burak Yavuz |
| [SPARK-25473](https://issues.apache.org/jira/browse/SPARK-25473) | PySpark ForeachWriter test fails on Python 3.6 and macOS High Serria |  Major | PySpark, Structured Streaming | Hyukjin Kwon | Hyukjin Kwon |
| [SPARK-25534](https://issues.apache.org/jira/browse/SPARK-25534) | Make \`SQLHelper\` trait |  Major | SQL | Dongjoon Hyun | Dongjoon Hyun |
| [SPARK-25541](https://issues.apache.org/jira/browse/SPARK-25541) | CaseInsensitiveMap should be serializable after '-' operator |  Major | SQL | Gengliang Wang | Gengliang Wang |
| [SPARK-25372](https://issues.apache.org/jira/browse/SPARK-25372) | Deprecate Yarn-specific configs in regards to keytab login for SparkSubmit |  Major | Kubernetes, YARN | Ilan Filonenko | Ilan Filonenko |
| [SPARK-25540](https://issues.apache.org/jira/browse/SPARK-25540) | Make HiveContext in PySpark behave as the same as Scala. |  Major | PySpark, SQL | Takuya Ueshin | Takuya Ueshin |
| [SPARK-25525](https://issues.apache.org/jira/browse/SPARK-25525) | Do not update conf for existing SparkContext in SparkSession.getOrCreate. |  Major | PySpark, SQL | Takuya Ueshin | Takuya Ueshin |
| [SPARK-25586](https://issues.apache.org/jira/browse/SPARK-25586) | toString method of GeneralizedLinearRegressionTrainingSummary runs in infinite loop throwing StackOverflowError |  Minor | MLlib, Spark Core | Ankur Gupta | Ankur Gupta |
| [SPARK-24601](https://issues.apache.org/jira/browse/SPARK-24601) | Bump Jackson version to 2.9.6 |  Major | Spark Core | Fokko Driesprong |  |
| [SPARK-17952](https://issues.apache.org/jira/browse/SPARK-17952) | SparkSession createDataFrame method throws exception for nested JavaBeans |  Major | . | Amit Baghel | Michal Šenkýř |
| [SPARK-25655](https://issues.apache.org/jira/browse/SPARK-25655) | Add Pspark-ganglia-lgpl to the scala style check |  Major | Build | Xiao Li | Xiao Li |
| [SPARK-25600](https://issues.apache.org/jira/browse/SPARK-25600) | Make use of TypeCoercion.findTightestCommonType while inferring CSV schema |  Minor | SQL | Dilip Biswal | Dilip Biswal |
| [SPARK-25461](https://issues.apache.org/jira/browse/SPARK-25461) | PySpark Pandas UDF outputs incorrect results when input columns contain None |  Major | PySpark | Chongyuan Xiang | Liang-Chi Hsieh |
| [SPARK-25677](https://issues.apache.org/jira/browse/SPARK-25677) | Configuring zstd compression in JDBC throwing IllegalArgumentException Exception |  Major | Spark Core | ABHISHEK KUMAR GUPTA | Shivu Sondur |
| [SPARK-25669](https://issues.apache.org/jira/browse/SPARK-25669) | Check CSV header only when it exists |  Minor | SQL | Maxim Gekk | Maxim Gekk |
| [SPARK-25535](https://issues.apache.org/jira/browse/SPARK-25535) | Work around bad error checking in commons-crypto |  Major | Spark Core | Marcelo Vanzin | Marcelo Vanzin |
| [SPARK-25636](https://issues.apache.org/jira/browse/SPARK-25636) | spark-submit swallows the failure reason when there is an error connecting to master |  Minor | Spark Core | Devaraj K | Devaraj K |
| [SPARK-25674](https://issues.apache.org/jira/browse/SPARK-25674) | If the records are incremented by more than 1 at a time,the number of bytes might rarely ever get updated |  Minor | SQL | liuxian | liuxian |
| [SPARK-25388](https://issues.apache.org/jira/browse/SPARK-25388) | checkEvaluation may miss incorrect nullable of DataType in the result |  Minor | Tests | Kazuaki Ishizaki | Kazuaki Ishizaki |
| [SPARK-25567](https://issues.apache.org/jira/browse/SPARK-25567) | [Spark Job History] Table listing in SQL Tab not display Sort Icon |  Minor | Web UI | ABHISHEK KUMAR GUPTA | shahid |
| [SPARK-25685](https://issues.apache.org/jira/browse/SPARK-25685) | Allow running tests in Jenkins in enterprise Git repository |  Minor | Build, Tests | Lantao Jin | Lantao Jin |
| [SPARK-19287](https://issues.apache.org/jira/browse/SPARK-19287) | JavaPairRDD flatMapValues requires function returning Iterable, not Iterator |  Minor | Java API | Sean Owen | Sean Owen |
| [SPARK-25710](https://issues.apache.org/jira/browse/SPARK-25710) | range should report metrics correctly |  Major | SQL | Wenchen Fan | Wenchen Fan |
| [SPARK-25579](https://issues.apache.org/jira/browse/SPARK-25579) | Use quoted attribute names if needed in pushed ORC predicates |  Blocker | SQL | Dongjoon Hyun | Dongjoon Hyun |
| [SPARK-25680](https://issues.apache.org/jira/browse/SPARK-25680) | SQL execution listener shouldn't happen on execution thread |  Major | SQL | Wenchen Fan | Wenchen Fan |
| [SPARK-25735](https://issues.apache.org/jira/browse/SPARK-25735) | Improve start-thriftserver.sh: print clean usage and exit with code 1 |  Minor | Spark Core | Gengliang Wang | Gengliang Wang |
| [SPARK-25003](https://issues.apache.org/jira/browse/SPARK-25003) | Pyspark Does not use Spark Sql Extensions |  Major | PySpark | Russell Spitzer | Russell Spitzer |
| [SPARK-25682](https://issues.apache.org/jira/browse/SPARK-25682) | Docker images generated from dev build and from dist tarball are different |  Minor | Kubernetes | Marcelo Vanzin | Marcelo Vanzin |
| [SPARK-25493](https://issues.apache.org/jira/browse/SPARK-25493) | CRLF Line Separators don't work in multiline CSVs |  Major | SQL | Justin Uang | Justin Uang |
| [SPARK-25745](https://issues.apache.org/jira/browse/SPARK-25745) | docker-image-tool.sh ignores errors from Docker |  Major | Deploy, Kubernetes | Rob Vesse | Rob Vesse |


### TESTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [SPARK-25414](https://issues.apache.org/jira/browse/SPARK-25414) | make it clear that the numRows metrics should be counted for each scan of the source |  Major | Structured Streaming | Wenchen Fan | Wenchen Fan |
| [SPARK-25673](https://issues.apache.org/jira/browse/SPARK-25673) | Remove Travis CI which enables Java lint check |  Minor | Build | Hyukjin Kwon | Hyukjin Kwon |
| [SPARK-25659](https://issues.apache.org/jira/browse/SPARK-25659) | Test type inference specification for createDataFrame in PySpark |  Minor | PySpark | Hyukjin Kwon | Hyukjin Kwon |
| [SPARK-25670](https://issues.apache.org/jira/browse/SPARK-25670) | Speed up JsonExpressionsSuite |  Trivial | SQL | Maxim Gekk | Maxim Gekk |
| [SPARK-25736](https://issues.apache.org/jira/browse/SPARK-25736) | add tests to verify the behavior of multi-column count |  Minor | SQL | Wenchen Fan | Wenchen Fan |
| [SPARK-25763](https://issues.apache.org/jira/browse/SPARK-25763) | Use more \`@contextmanager\` to ensure clean-up each test. |  Major | PySpark, SQL | Takuya Ueshin | Takuya Ueshin |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [SPARK-24882](https://issues.apache.org/jira/browse/SPARK-24882) | data source v2 API improvement |  Major | SQL | Wenchen Fan | Wenchen Fan |
| [SPARK-23429](https://issues.apache.org/jira/browse/SPARK-23429) | Add executor memory metrics to heartbeat and expose in executors REST API |  Major | Spark Core | Edwina Lu |  |
| [SPARK-25358](https://issues.apache.org/jira/browse/SPARK-25358) | MutableProjection supports fallback to an interpreted mode |  Major | SQL | Takeshi Yamamuro | Takeshi Yamamuro |
| [SPARK-23648](https://issues.apache.org/jira/browse/SPARK-23648) | extend hint syntax to support any expression for R |  Major | SparkR, SQL | Dylan Guedes | Huaxin Gao |
| [SPARK-25339](https://issues.apache.org/jira/browse/SPARK-25339) | Refactor FilterPushdownBenchmark to use main method |  Major | SQL | Yuming Wang | Yuming Wang |
| [SPARK-25487](https://issues.apache.org/jira/browse/SPARK-25487) | Refactor PrimitiveArrayBenchmark |  Major | SQL | Chenxiao Mao | Chenxiao Mao |
| [SPARK-25499](https://issues.apache.org/jira/browse/SPARK-25499) | Refactor BenchmarkBase and Benchmark |  Major | SQL | Gengliang Wang | Gengliang Wang |
| [SPARK-25489](https://issues.apache.org/jira/browse/SPARK-25489) | Refactor UDTSerializationBenchmark |  Major | MLlib | Chenxiao Mao | Chenxiao Mao |
| [SPARK-25478](https://issues.apache.org/jira/browse/SPARK-25478) | Refactor CompressionSchemeBenchmark to use main method |  Major | Tests | Yuming Wang | Yuming Wang |
| [SPARK-25486](https://issues.apache.org/jira/browse/SPARK-25486) | Refactor SortBenchmark to use main method |  Major | SQL | yucai | yucai |
| [SPARK-25379](https://issues.apache.org/jira/browse/SPARK-25379) | Improve ColumnPruning performance |  Major | Optimizer, SQL | Marco Gaido | Marco Gaido |
| [SPARK-25481](https://issues.apache.org/jira/browse/SPARK-25481) | Refactor ColumnarBatchBenchmark to use main method |  Major | SQL | yucai | yucai |
| [SPARK-25485](https://issues.apache.org/jira/browse/SPARK-25485) | Refactor UnsafeProjectionBenchmark to use main method |  Major | SQL | yucai | yucai |
| [SPARK-25559](https://issues.apache.org/jira/browse/SPARK-25559) | Just remove the unsupported predicates in Parquet |  Major | SQL | DB Tsai | DB Tsai |
| [SPARK-25508](https://issues.apache.org/jira/browse/SPARK-25508) | Refactor OrcReadBenchmark to use main method |  Major | SQL | yucai | yucai |
| [SPARK-25476](https://issues.apache.org/jira/browse/SPARK-25476) | Refactor AggregateBenchmark to use main method |  Major | Tests | Yuming Wang | Yuming Wang |
| [SPARK-25510](https://issues.apache.org/jira/browse/SPARK-25510) |  Create a new trait SqlBasedBenchmark |  Major | Tests | Yuming Wang | Yuming Wang |
| [SPARK-25483](https://issues.apache.org/jira/browse/SPARK-25483) | Refactor UnsafeArrayDataBenchmark to use main method |  Major | Tests | Yuming Wang | Yuming Wang |
| [SPARK-25601](https://issues.apache.org/jira/browse/SPARK-25601) | Register Grouped aggregate UDF Vectorized UDFs for SQL Statement |  Major | PySpark, SQL | Hyukjin Kwon | Hyukjin Kwon |
| [SPARK-25479](https://issues.apache.org/jira/browse/SPARK-25479) | Refactor DatasetBenchmark to use main method |  Major | Tests | Yuming Wang | Yuming Wang |
| [SPARK-25606](https://issues.apache.org/jira/browse/SPARK-25606) | DateExpressionsSuite: Hour 1 min |  Major | Tests | Xiao Li | Yuming Wang |
| [SPARK-25609](https://issues.apache.org/jira/browse/SPARK-25609) | DataFrameSuite: SPARK-22226: splitExpressions should not generate codes beyond 64KB 49 seconds |  Major | Tests | Xiao Li | Marco Gaido |
| [SPARK-25605](https://issues.apache.org/jira/browse/SPARK-25605) | CastSuite: cast string to timestamp 2 mins 31 sec |  Major | Tests | Xiao Li | Marco Gaido |
| [SPARK-25626](https://issues.apache.org/jira/browse/SPARK-25626) | HiveClientSuites: getPartitionsByFilter returns all partitions when hive.metastore.try.direct.sql=false 46 sec |  Major | Tests | Xiao Li | Dilip Biswal |
| [SPARK-25610](https://issues.apache.org/jira/browse/SPARK-25610) | DatasetCacheSuite: cache UDF result correctly 25 seconds |  Major | Tests | Xiao Li | Dilip Biswal |
| [SPARK-25621](https://issues.apache.org/jira/browse/SPARK-25621) | BucketedReadWithHiveSupportSuite: read partitioning bucketed tables having composite filters	 45 sec |  Major | Tests | Xiao Li | Gengliang Wang |
| [SPARK-25622](https://issues.apache.org/jira/browse/SPARK-25622) | BucketedReadWithHiveSupportSuite: read partitioning bucketed tables with bucket pruning filters - 42 seconds |  Major | Tests | Xiao Li | Apache Spark |
| [SPARK-25488](https://issues.apache.org/jira/browse/SPARK-25488) | Refactor MiscBenchmark to use main method |  Major | Tests | Yuming Wang | Yuming Wang |
| [SPARK-25658](https://issues.apache.org/jira/browse/SPARK-25658) | Refactor HashByteArrayBenchmark to use main method |  Major | Tests | Yuming Wang | Yuming Wang |
| [SPARK-25657](https://issues.apache.org/jira/browse/SPARK-25657) | Refactor HashBenchmark to use main method |  Major | Tests | Yuming Wang | Yuming Wang |
| [SPARK-25630](https://issues.apache.org/jira/browse/SPARK-25630) | HiveOrcHadoopFsRelationSuite: SPARK-8406: Avoids name collision while writing files 21 sec |  Major | Tests | Xiao Li | Gengliang Wang |
| [SPARK-25623](https://issues.apache.org/jira/browse/SPARK-25623) | LogisticRegressionSuite: multinomial logistic regression with intercept with L1 regularization 1 min 10 sec |  Major | Tests | Xiao Li | shahid |
| [SPARK-25624](https://issues.apache.org/jira/browse/SPARK-25624) | LogisticRegressionSuite.multinomial logistic regression with intercept with elasticnet regularization 56 seconds |  Major | Tests | Xiao Li | shahid |
| [SPARK-25625](https://issues.apache.org/jira/browse/SPARK-25625) | LogisticRegressionSuite.binary logistic regression with intercept with ElasticNet regularization - 33 sec |  Major | Tests | Xiao Li | shahid |
| [SPARK-25611](https://issues.apache.org/jira/browse/SPARK-25611) | CompressionCodecSuite: both table-level and session-level compression are set 2 min 20 sec |  Major | Tests | Xiao Li | Dilip Biswal |
| [SPARK-25612](https://issues.apache.org/jira/browse/SPARK-25612) | CompressionCodecSuite: table-level compression is not set but session-level compressions 47 seconds |  Major | Tests | Xiao Li | Dilip Biswal |
| [SPARK-25700](https://issues.apache.org/jira/browse/SPARK-25700) | Avoid to create a readsupport at write path in Data Source V2 |  Critical | SQL | Hyukjin Kwon | Hyukjin Kwon |
| [SPARK-25662](https://issues.apache.org/jira/browse/SPARK-25662) | Refactor DataSourceReadBenchmark to use main method |  Major | Tests | Yuming Wang | Peter Toth |
| [SPARK-25615](https://issues.apache.org/jira/browse/SPARK-25615) | KafkaSinkSuite: streaming - write to non-existing topic 1 min |  Major | Tests | Xiao Li | Dilip Biswal |
| [SPARK-25664](https://issues.apache.org/jira/browse/SPARK-25664) | Refactor JoinBenchmark to use main method |  Major | Tests | Yuming Wang | Yuming Wang |
| [SPARK-25629](https://issues.apache.org/jira/browse/SPARK-25629) | ParquetFilterSuite: filter pushdown - decimal 16 sec |  Major | Tests | Xiao Li | Yuming Wang |
| [SPARK-25631](https://issues.apache.org/jira/browse/SPARK-25631) | KafkaRDDSuite: basic usage 	2 min 4 sec |  Major | Tests | Xiao Li | Dilip Biswal |
| [SPARK-25632](https://issues.apache.org/jira/browse/SPARK-25632) | KafkaRDDSuite: compacted topic 2 min 5 sec. |  Major | Tests | Xiao Li | Dilip Biswal |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [SPARK-25666](https://issues.apache.org/jira/browse/SPARK-25666) | Internally document type conversion between Python data and SQL types in UDFs |  Minor | PySpark | Hyukjin Kwon | Hyukjin Kwon |
| [SPARK-23257](https://issues.apache.org/jira/browse/SPARK-23257) | Implement Kerberos Support in Kubernetes resource manager |  Major | Kubernetes | Rob Keevil | Ilan Filonenko |
| [SPARK-25551](https://issues.apache.org/jira/browse/SPARK-25551) | Remove unused InSubquery expression |  Trivial | SQL | Marco Gaido | Marco Gaido |
| [SPARK-25598](https://issues.apache.org/jira/browse/SPARK-25598) | Remove flume connector in Spark 3 |  Major | DStreams | Marcelo Vanzin | Sean Owen |
| [SPARK-25705](https://issues.apache.org/jira/browse/SPARK-25705) | Remove Kafka 0.8 support in Spark 3.0.0 |  Major | Build, Structured Streaming | Sean Owen | Sean Owen |


