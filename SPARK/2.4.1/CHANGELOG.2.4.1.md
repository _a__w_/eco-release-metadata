
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Spark Changelog

## Release 2.4.1 - Unreleased (as of 2018-10-20)



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [SPARK-25639](https://issues.apache.org/jira/browse/SPARK-25639) | Add documentation on foreachBatch, and multiple watermark policy |  Blocker | Documentation | Tathagata Das | Tathagata Das |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [SPARK-25677](https://issues.apache.org/jira/browse/SPARK-25677) | Configuring zstd compression in JDBC throwing IllegalArgumentException Exception |  Major | Spark Core | ABHISHEK KUMAR GUPTA | Shivu Sondur |
| [SPARK-25636](https://issues.apache.org/jira/browse/SPARK-25636) | spark-submit swallows the failure reason when there is an error connecting to master |  Minor | Spark Core | Devaraj K | Devaraj K |
| [SPARK-25674](https://issues.apache.org/jira/browse/SPARK-25674) | If the records are incremented by more than 1 at a time,the number of bytes might rarely ever get updated |  Minor | SQL | liuxian | liuxian |
| [SPARK-25697](https://issues.apache.org/jira/browse/SPARK-25697) | When zstd compression enabled in progress application is throwing Error in UI |  Major | Spark Core | ABHISHEK KUMAR GUPTA | shahid |


