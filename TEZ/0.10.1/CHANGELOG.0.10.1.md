
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Tez Changelog

## Release 0.10.1 - Unreleased (as of 2018-10-20)



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [TEZ-3975](https://issues.apache.org/jira/browse/TEZ-3975) | Please add OWASP Dependency Check to the build (pom.xml) |  Major | . | Albert Baker | Jonathan Eagles |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [TEZ-3989](https://issues.apache.org/jira/browse/TEZ-3989) | Fix by-laws related to emeritus clause |  Major | . | Hitesh Shah | Hitesh Shah |
| [TEZ-3972](https://issues.apache.org/jira/browse/TEZ-3972) | Tez DAG can hang when a single task fails to fetch |  Major | . | Kuhu Shukla | Kuhu Shukla |
| [TEZ-3982](https://issues.apache.org/jira/browse/TEZ-3982) | DAGAppMaster and tasks should not report negative or invalid progress |  Major | . | Kuhu Shukla | Kuhu Shukla |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [TEZ-3994](https://issues.apache.org/jira/browse/TEZ-3994) | Upgrade maven-surefire-plugin to 0.21.0 to support yetus |  Major | . | Jonathan Eagles | Jonathan Eagles |


### OTHER:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [TEZ-3988](https://issues.apache.org/jira/browse/TEZ-3988) | Update snapshot version in master to 0.10.1-SNAPSHOT |  Major | . | Eric Wohlstadter | Eric Wohlstadter |


