
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Sentry Changelog

## Release 2.2.0 - Unreleased (as of 2018-10-20)



### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [SENTRY-2413](https://issues.apache.org/jira/browse/SENTRY-2413) | Provide a configuration option to permit specific DB privileges to be granted explicitly |  Major | Sentry | Sergio Peña | Sergio Peña |
| [SENTRY-2371](https://issues.apache.org/jira/browse/SENTRY-2371) | Add a new thrift API for getting all privileges a user has |  Major | . | Hao Hao | Hao Hao |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [SENTRY-2403](https://issues.apache.org/jira/browse/SENTRY-2403) | Incorrect naming in RollingFileWithoutDeleteAppender |  Major | . | Peter Somogyi | Peter Somogyi |
| [SENTRY-2417](https://issues.apache.org/jira/browse/SENTRY-2417) | LocalGroupMappingService class docs do not accurately reflect required INI format |  Major | Docs | Dan Burkert | Dan Burkert |
| [SENTRY-2409](https://issues.apache.org/jira/browse/SENTRY-2409) | ALTER TABLE SET OWNER does not allow to change the table if using only the table name |  Major | Sentry | Na Li | Na Li |
| [SENTRY-2429](https://issues.apache.org/jira/browse/SENTRY-2429) | Transfer database owner drops table owner |  Major | Sentry | Na Li | Na Li |
| [SENTRY-2428](https://issues.apache.org/jira/browse/SENTRY-2428) | Skip null partitions or partitions with null sds entries |  Major | Sentry | Arjun Mishra | Arjun Mishra |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [SENTRY-2423](https://issues.apache.org/jira/browse/SENTRY-2423) | Increase the allocation size for auto-increment of id's for Snapshot tables. |  Major | Sentry | kalyan kumar kalvagadda | kalyan kumar kalvagadda |


