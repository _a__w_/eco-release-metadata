
<!---
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->
# Apache Orc Changelog

## Release 1.6.0 - Unreleased (as of 2018-10-20)



### NEW FEATURES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [ORC-383](https://issues.apache.org/jira/browse/ORC-383) | Parallel builds fails with ConcurrentModificationException |  Major | . | Prasanth Jayachandran | Prasanth Jayachandran |
| [ORC-397](https://issues.apache.org/jira/browse/ORC-397) | ORC should allow selectively disabling dictionary-encoding on specified columns |  Major | . | Mithun Radhakrishnan | Mithun Radhakrishnan |


### IMPROVEMENTS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [ORC-364](https://issues.apache.org/jira/browse/ORC-364) | Minor fixups on javadoc. |  Major | . | Owen O'Malley | Owen O'Malley |
| [ORC-116](https://issues.apache.org/jira/browse/ORC-116) | Create submodule with example java code from documentation |  Major | . | Owen O'Malley | Owen O'Malley |
| [ORC-334](https://issues.apache.org/jira/browse/ORC-334) | Add AppVeyor support for integration on windows |  Major | . | Deepak Majeti | Renat Valiullin |
| [ORC-366](https://issues.apache.org/jira/browse/ORC-366) | Improve TZDIR setup for WIN32 |  Major | . | Renat Valiullin | Renat Valiullin |
| [ORC-372](https://issues.apache.org/jira/browse/ORC-372) | Enable valgrind for C++ travis-ci tests |  Major | . | Deepak Majeti | Deepak Majeti |
| [ORC-376](https://issues.apache.org/jira/browse/ORC-376) | Add Ubuntu18 docker file |  Major | . | Owen O'Malley | Owen O'Malley |
| [ORC-386](https://issues.apache.org/jira/browse/ORC-386) | Add new spark file format benchmark |  Major | . | Owen O'Malley | Owen O'Malley |
| [ORC-393](https://issues.apache.org/jira/browse/ORC-393) | Add ORC snapcraft definition |  Major | . | Owen O'Malley | Owen O'Malley |
| [ORC-399](https://issues.apache.org/jira/browse/ORC-399) | Move Java compiler to version 8. |  Major | . | Owen O'Malley | Owen O'Malley |
| [ORC-385](https://issues.apache.org/jira/browse/ORC-385) | Change RecordReader from AutoCloseable to Closeable |  Major | . | Owen O'Malley | Owen O'Malley |
| [ORC-411](https://issues.apache.org/jira/browse/ORC-411) | Update build to work with Java 10. |  Major | . | Owen O'Malley | Owen O'Malley |


### BUG FIXES:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [ORC-367](https://issues.apache.org/jira/browse/ORC-367) | Boolean columns are read incorrectly when using seek. |  Major | . | Jesus Camacho Rodriguez | Owen O'Malley |
| [ORC-373](https://issues.apache.org/jira/browse/ORC-373) | If "orc.dictionary.key.threshold" is set to 0, don't try dictionary encoding. |  Major | . | Prasanth Jayachandran | Prasanth Jayachandran |
| [ORC-379](https://issues.apache.org/jira/browse/ORC-379) | ConversionTreeReaders should handle Decimal64 |  Major | . | Prasanth Jayachandran | Prasanth Jayachandran |
| [ORC-380](https://issues.apache.org/jira/browse/ORC-380) | Add isOnlyImplicitConversion boolean function to SchemaEvolution |  Critical | ORCv2 | Matt McCline | Matt McCline |
| [ORC-365](https://issues.apache.org/jira/browse/ORC-365) | Only print min and max for timestamp stats once |  Minor | . | Jesus Camacho Rodriguez | Jesus Camacho Rodriguez |
| [ORC-382](https://issues.apache.org/jira/browse/ORC-382) | Apache rat exclusions + add rat check to travis |  Major | . | Prasanth Jayachandran | Prasanth Jayachandran |
| [ORC-371](https://issues.apache.org/jira/browse/ORC-371) | [C++] Disable Libhdfspp build when Cyrus SASL is not found |  Major | . | Deepak Majeti | Anatoli Shein |
| [ORC-384](https://issues.apache.org/jira/browse/ORC-384) | C++ Reader leaks memory when reading a non-ORC file |  Minor | C++ | Martin Rupp | Martin Rupp |
| [ORC-396](https://issues.apache.org/jira/browse/ORC-396) | ORC build fails to find LZ4 lib directory on some platforms |  Major | C++ | Tim Armstrong | Tim Armstrong |
| [ORC-203](https://issues.apache.org/jira/browse/ORC-203) | Modify the StringStatistics to trim minimum and maximum values |  Major | . | Owen O'Malley | Sandeep More |
| [ORC-401](https://issues.apache.org/jira/browse/ORC-401) | Typing error found in document and site |  Trivial | documentation, site | xuchuanyin | xuchuanyin |
| [ORC-375](https://issues.apache.org/jira/browse/ORC-375) | v1.5.1 install from source fails under GCC 7.3.0 |  Major | build, C++ | Serge Fein | James Clampffer |
| [ORC-403](https://issues.apache.org/jira/browse/ORC-403) | Should check offsets got from protobuf Objects |  Major | C++ | Quanlong Huang | Quanlong Huang |
| [ORC-406](https://issues.apache.org/jira/browse/ORC-406) | ORC: Char(n) and Varchar(n) writers truncate to n bytes & corrupts multi-byte data |  Major | . | Gopal V | Gopal V |
| [ORC-391](https://issues.apache.org/jira/browse/ORC-391) | [C++]  parseType does not accept underscore in the field name |  Minor | . | Zherui Cao | Zherui Cao |
| [ORC-410](https://issues.apache.org/jira/browse/ORC-410) | Fix a locale-dependent test in TestCsvReader |  Major | . | Kotaro Terada | Kotaro Terada |
| [ORC-409](https://issues.apache.org/jira/browse/ORC-409) | Changes for extending MemoryManagerImpl |  Major | . | Prasanth Jayachandran | Prasanth Jayachandran |
| [ORC-237](https://issues.apache.org/jira/browse/ORC-237) | OrcFile.mergeFiles Specified block size is less than configured minimum value |  Major | . | EbCead |  |
| [ORC-407](https://issues.apache.org/jira/browse/ORC-407) | JasonFileDump class needs to be updated to take into account string truncations |  Major | . | Sandeep More | Sandeep More |
| [ORC-416](https://issues.apache.org/jira/browse/ORC-416) | Avoid opening data reader when there is no stripe |  Major | Java | Dongjoon Hyun | Dongjoon Hyun |
| [ORC-419](https://issues.apache.org/jira/browse/ORC-419) | Ensure to call \`close\` at RecordReaderImpl constructor exception |  Critical | Java | Dongjoon Hyun | Dongjoon Hyun |


### SUB-TASKS:

| JIRA | Summary | Priority | Component | Reporter | Contributor |
|:---- |:---- | :--- |:---- |:---- |:---- |
| [ORC-251](https://issues.apache.org/jira/browse/ORC-251) | Modify InStream and OutStream to optionally encrypt data |  Major | . | Owen O'Malley | Owen O'Malley |


